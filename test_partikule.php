<?php
/*
 * Partikule Batch / Test
 *
 */

/*
- Get existing order increment ID for example = 145000038
- comment out 'sales_order_place_after' tags in
    app/code/core/Mage/SalesRule/etc/config.xml and
    app/code/core/Mage/Sales/etc/config.xml and
    app/code/core/Enterprise/PageCache/etc/config.xml (if you run EE) to disable existing sales_order_place_after observers.

- And don't forget to clear cache.

*/

include('app/Mage.php');

Mage::app();

/*
 * Order Numbers :
 *
 * contact@michel-ange.fr : 100023352
    0X8U8LD0AH
    NMDWB9NDQR
    0IHTV10O5D
    1AQSC9LXN0
    0DYBEAKIGO
    XEHXRVZNNS
 */

$order = Mage::getModel('sales/order')->loadByIncrementId('100023352');
Mage::dispatchEvent('sales_order_save_after', array('order'=>$order));



/*
 * Coupons : Batch modify
 *
 *
 *
 */
/*
$rules = Mage::getModel('salesrule/rule')
    ->getCollection()
    ->addFieldToFilter('is_active', 1)
    ->addFieldToFilter('description', array('like' =>'Nose Samples Order Coupon'))
    //  ->addFieldToFilter('code', array('eq' =>'18GCD1IZAI'))

    ->addFieldToFilter('coupon_type', array('eq' => 2))
    ->addFieldToFilter('name', array('like' => 'Samples_%'))
    //    ->getFirstItem()
;

$all_times = 0;
$all_unused = 0;

$new_condition = Mage::getModel('salesrule/rule_condition_address')
    ->setType('salesrule/rule_condition_address')
    ->setAttribute('base_subtotal')
    ->setOperator('>=')
    ->setValue(80);


foreach($rules as $rule)
{
    $_rule = Mage::getModel('salesrule/rule')->load($rule->getId());


    if ( ! empty($rule->getCode()) && substr($_rule->getName(), 0, 8) === 'Samples_')
    {
        $conditions = unserialize($_rule->getConditionsSerialized());

        if (empty($conditions['conditions'][1]))
        {
            $_rule->getConditions()->addCondition($new_condition);

            $_rule->save();

            Mage::log(print_r('Processed : ' . $_rule->getName(), true), null, 'partikule.log', true);
        }
        else
        {
        //    Mage::log(print_r('NOT Processed : ' . $_rule->getName(), true), null, 'partikule.log', true);
        }
    }


    /*
     * Number of uses per coupons.
     *
     *
        $coupon = Mage::getModel('salesrule/coupon');
        $collection = $coupon->getCollection();
        $collection->addFieldToFilter('code', array('in' => $rule->getCode()));


        $times = 0;

        foreach($collection as $coupon)
        {
            if ($coupon->getId())
            {
                $times += $coupon->getTimesUsed();
                $all_times += $coupon->getTimesUsed();

                if ($coupon->getTimesUsed() == 0)
                {
                    $all_unused += 1;

                    // Mage::log(print_r($coupon->getData(), true), null, 'partikule.log', true);


                   print_r('<pre>' . $coupon->getCode() . ';'.$rule->getCreatedAt().'</pre>');
                }
            }
        }

}
*/

