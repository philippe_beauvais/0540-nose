<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Bundle
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order item render block
 *
 * @category    Mage
 * @package     Mage_Bundle
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Bundle_Block_Sales_Order_Items_Renderer extends Mage_Sales_Block_Order_Item_Renderer_Default
{
    public function isShipmentSeparately($item = NULL)
    {
        if ($item) {
            if ($item->getOrderItem()) {
                $item = $item->getOrderItem();
            }
            if ($parentItem = $item->getParentItem()) {
                if ($options = $parentItem->getProductOptions()) {
                    if (isset($options['shipment_type']) && $options['shipment_type'] == Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                }
            } else {
                if ($options = $item->getProductOptions()) {
                    if (isset($options['shipment_type']) && $options['shipment_type'] == Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                }
            }
        }

        if ($options = $this->getOrderItem()->getProductOptions()) {
            if (isset($options['shipment_type']) && $options['shipment_type'] == Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function isChildCalculated($item = NULL)
    {
        if ($item) {
            if ($item->getOrderItem()) {
                $item = $item->getOrderItem();
            }
            if ($parentItem = $item->getParentItem()) {
                if ($options = $parentItem->getProductOptions()) {
                    if (isset($options['product_calculations']) && $options['product_calculations'] == Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                }
            } else {
                if ($options = $item->getProductOptions()) {
                    if (isset($options['product_calculations']) && $options['product_calculations'] == Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                }
            }
        }

        if ($options = $this->getOrderItem()->getProductOptions()) {
            if (isset($options['product_calculations'])
                && $options['product_calculations'] == Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
                return TRUE;
            }
        }
        return FALSE;
    }

	/**
	 * Partikule :
	 * For Nose : Do not display Selection attributes
	 *
	 * @param $item
	 *
	 * @return null
	 */
	public function getSelectionAttributes($item)
	{
		/*
        if ($item instanceof Mage_Sales_Model_Order_Item) {
            $options = $item->getProductOptions();
        } else {
            $options = $item->getOrderItem()->getProductOptions();
        }

		Mage::log($options['bundle_selection_attributes'], null, 'partikule.log');

		if (isset($options['bundle_selection_attributes'])) {
            return unserialize($options['bundle_selection_attributes']);
        }
		*/
        return NULL;
    }

	/**
	 * Partikule : Modify the name output
	 * - Add the category if one is found.
	 * - Remove the price : in case of Nose bundle, we don't care.
	 *
	 * @param $item
	 *
	 * @return string
	 */
	public function getValueHtml($item)
    {
		// Partikule : Add the category if found
		$name = $item->getName();

		$product = Mage::getModel('catalog/product')->load($item->getProductId());
		$product_category = Mage::helper('nose/Product')->getProductFirstCategory($product);
		if ($product_category)
			$name = '<strong>' . $product_category->getName() . '</strong>, ' . $item->getName();

		return $name;
    }

    /**
     * Getting all available childs for Invoice, Shipmen or Creditmemo item
     *
     * @param Varien_Object $item
     * @return array
     */
    public function getChilds($item)
    {
        $_itemsArray = array();

        if ($item instanceof Mage_Sales_Model_Order_Invoice_Item) {
            $_items = $item->getInvoice()->getAllItems();
        } else if ($item instanceof Mage_Sales_Model_Order_Shipment_Item) {
            $_items = $item->getShipment()->getAllItems();
        } else if ($item instanceof Mage_Sales_Model_Order_Creditmemo_Item) {
            $_items = $item->getCreditmemo()->getAllItems();
        }

        if ($_items) {
            foreach ($_items as $_item) {
                if ($parentItem = $_item->getOrderItem()->getParentItem()) {
                    $_itemsArray[$parentItem->getId()][$_item->getOrderItemId()] = $_item;
                } else {
                    $_itemsArray[$_item->getOrderItem()->getId()][$_item->getOrderItemId()] = $_item;
                }
            }
        }

        if (isset($_itemsArray[$item->getOrderItem()->getId()])) {
            return $_itemsArray[$item->getOrderItem()->getId()];
        } else {
            return NULL;
        }
    }

    public function canShowPriceInfo($item)
    {
        if (($item->getOrderItem()->getParentItem() && $this->isChildCalculated())
                || (!$item->getOrderItem()->getParentItem() && !$this->isChildCalculated())) {
            return TRUE;
        }
        return FALSE;
    }


	/**
	 * Partikule : Copied here to mod the
	 * Accept option value and return its formatted view
	 *
	 * @param mixed $optionValue
	 * Method works well with these $optionValue format:
	 *      1. String
	 *      2. Indexed array e.g. array(val1, val2, ...)
	 *      3. Associative array, containing additional option info, including option value, e.g.
	 *          array
	 *          (
	 *              [label] => ...,
	 *              [value] => ...,
	 *              [print_value] => ...,
	 *              [option_id] => ...,
	 *              [option_type] => ...,
	 *              [custom_view] =>...,
	 *          )
	 *
	 * @return array
	 */
	public function getFormatedOptionValue($optionValue)
	{
// Mage::log('Call of getFormatedOptionValue()', NULL, 'partikule.log');
		$optionInfo = array();

		// define input data format
		if (is_array($optionValue)) {
			if (isset($optionValue['option_id'])) {
				$optionInfo = $optionValue;
				if (isset($optionInfo['value'])) {
					$optionValue = $optionInfo['value'];
				}
			} elseif (isset($optionValue['value'])) {
				$optionValue = $optionValue['value'];
			}
		}

		// render customized option view
		if (isset($optionInfo['custom_view']) && $optionInfo['custom_view']) {
			$_default = array('value' => $optionValue);
			if (isset($optionInfo['option_type'])) {
				try {
					$group = Mage::getModel('catalog/product_option')->groupFactory($optionInfo['option_type']);
					return array('value' => $group->getCustomizedView($optionInfo));
				} catch (Exception $e) {
					return $_default;
				}
			}
			return $_default;
		}

		// truncate standard view
		$result = array();
		if (is_array($optionValue)) {
			$_truncatedValue = implode("\n", $optionValue);
			$_truncatedValue = nl2br($_truncatedValue);
			return array('value' => $_truncatedValue);
		} else {
			$_truncatedValue = Mage::helper('core/string')->truncate($optionValue, 55, '');
			$_truncatedValue = nl2br($_truncatedValue);
		}

		$result = array('value' => $_truncatedValue);

		if (Mage::helper('core/string')->strlen($optionValue) > 55) {
			$result['value'] = $result['value'] . ' <a href="#" class="dots" onclick="return false">...</a>';
			$optionValue = nl2br($optionValue);
			$result = array_merge($result, array('full_view' => $optionValue));
		}

		return $result;
	}



}
