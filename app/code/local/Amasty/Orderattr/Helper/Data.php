<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Orderattr
*/
class Amasty_Orderattr_Helper_Data extends Mage_Core_Helper_Abstract
{
	// Added by Partikule
	protected $_formElements = array();
	
    public function fields($step)
    {
        return Mage::app()->getLayout()->createBlock('amorderattr/fields')->setStep($step)->toHtml();
    }
    
    /**
     * Added by Partikule
     * Returns one given field 
     *
     */
    public function getField($attributeCode, $step)
    {
    	if ( ! isset($this->_formElements[$step]))
    	{
        	$this->_formElements[$step] = Mage::app()->getLayout()->createBlock('amorderattr/fields')->setStep($step)->getFormElements();
        }
        
		$fieldset = NULL;
		foreach ($this->_formElements[$step] as $el)
		{
			$fieldset = $el;
			break;
		}

		if ( ! is_null($fieldset))
		{
			$elements = $fieldset->getSortedElements();

			foreach($elements as $element)
			{
				if ($element->getId() == $attributeCode)
					return $element;
			}
		}
		
		return FALSE;
    }
    
    
    public function clearCache()
    {
        $cacheDir = Mage::getBaseDir('var') . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
        $this->_clearDir($cacheDir);
        Mage::app()->cleanCache();
        Mage::getConfig()->reinit();
    }
    
    protected function _clearDir($dir = '')
    {
        if($dir) 
        {
            if (is_dir($dir)) 
            {
                if ($handle = @opendir($dir)) 
                {
                    while (($file = readdir($handle)) !== false) 
                    {
                        if ($file != "." && $file != "..") 
                        {
                            $fullpath = $dir . '/' . $file;
                            if (is_dir($fullpath)) 
                            {
                                $this->_clearDir($fullpath);
                                @rmdir($fullpath);
                            }
                            else 
                            {
                                @unlink($fullpath);
                            }
                        }
                    }
                    closedir($handle);
                }
            }
        }
    }
}