<?php

class Partikule_Boutique_IndexController extends Mage_Core_Controller_Front_Action
{
	
	/**
	 * Redirect to the pseudo category controller
	 *
	 */
	public function indexAction()
	{
		// get the attribute acting as root filter
		$mainAttributeCode = Mage::getStoreConfig('boutique/settings/attribute');

		// Get the first attribute option and redirect to the category controller
		$options = Mage::helper('attribute/data')->getAttributeOptions($mainAttributeCode);
		
		if ( ! empty($options))
		{
			$optionValue = strtolower($options[0]['default_value']);
			
			$this->getResponse()
				->setRedirect(Mage::getBaseUrl().$this->getRequest()->getModuleName() . '/category/display/' . $optionValue, 301);
		}
	}

}