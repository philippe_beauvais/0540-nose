<?php

class Partikule_Boutique_ProductController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Just do nothing
	 *
	 */
	public function indexAction()
	{
		echo('');
	}


	/**
	 * Returns one attribute name / value filtered product list
	 *
	 * Called through XHR
	 *  
	 * $this->loadLayout() loads "root" defined in "nose/default/layout/boutique.xml" through : attribute_product_getlist
	 *
	 * Gets through POST : 
	 *
	 * - category_id : 		Category ID
	 * - attribute : 		Attribute name
	 * - attribute_value : 	Attribute Value
	 * - ... : 				All other args 
	 *
	 * @usage : In templates : 
	 *			 	new Ajax.Updater(
	 *					'productList', 
	 *					'boutique/product/getproducts', 
	 *					{
	 *						'method': 'post',
	 *						'parameters': {
	 *							'category_id': 	'<category_id>',
	 *							'attribute': 	'<attribute code>',
	 *							'value': 		'<attribute value>'
	 *						} 
	 *					}
	 *				);
	 *
	 *
	 */
	public function getProductsAction()
	{
		$this->_processXhrPost();
	}
	

	/**
	 * Generic function to process POST data from XHR
	 * Loads the layout linked to the caller function
	 * and register post data
	 *
	 */
	protected function _processXhrPost()
	{
		if ($this->getRequest()->isPost())
		{
			// get the attribute acting as root filter
			$main_attribute = Mage::getStoreConfig('boutique/settings/attribute');

			
			Mage::register('main_attribute', $main_attribute);

			// Only register post value to make them available for the template
			foreach($this->getRequest()->getPost() as $key=>$value)
			{
				Mage::register($key, $value);
			}
			
			// Renders the layout defined in "attribute.xml" for this method
			$this->loadLayout()->renderLayout();
		}
	}

}