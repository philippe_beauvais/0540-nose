<?php
/**
 * Boutique Category Controller
 *
 * The boutique has a attribute's layered navigation
 * What we call "Category" is in fact one main product classification attribute.
 *
 * This attribute is defined in : config.xml : boutique/settings/attribute
 *
 *
 */
class Partikule_Boutique_CategoryController extends Mage_Core_Controller_Front_Action
{

/*
 * Tried to make one "universal" dispatcher for the Category Controller : 
 * 
 * Idea : 	http://nose.home/fr/boutique/category/perfumes
 * 			should be redirected to
 *			http://nose.home/fr/boutique/category/display/perfumes
 * 
	public function preDispatch()
	{
		$this->getRequest()->setModuleName('boutique');
		$this->getRequest()->setControllerName('category');
		$this->getRequest()->setActionName('display');
		$this->getRequest()->setControllerModule('boutique');

		parent::preDispatch();
		$option_value = Mage::helper('boutique/Url')->getLastUriSegment($this);

		// Check if this Attribute option exists...
		// ...
		$attributeCode = Mage::getStoreConfig('boutique/settings/attribute');
		
		if ($this->_hasAttributeOption($attributeCode, $option_value))
		{
			
//			$this->display($option_value);
			
		}
	}
*/
	


	/**
	 *
	 */
	public function displayAction()
	{
		// Get the option to display page
		$option_value = Mage::helper('boutique/Url')->getLastUriSegment($this);
		
		// Get the attribute (code) acting as root filter
		$mainAttributeCode = Mage::getStoreConfig('boutique/settings/attribute');

		// Register the base attribute code
		Mage::register('main_attribute', $mainAttributeCode);

		
		$helper = Mage::helper('boutique/Attribute');
		
		// Register "main_attribute_option" : Array
		if ($helper->hasAttributeOption($mainAttributeCode, $option_value))
			Mage::register('main_attribute_option', $helper->getOptionFromAttribute($mainAttributeCode, $option_value));

		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('boutique');

		$this->renderLayout();
	}
	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	private function _getOptionFromName($attributeCode, $optionName)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
	}
	
	

	
/*
    protected function _prepareLayout()
    {
		$this->loadLayout();

		$this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('boutique');
		
		$this->renderLayout();


        $this->getLayout()->createBlock('catalog/breadcrumbs');

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $category = $this->getCurrentCategory();
            if ($title = $category->getMetaTitle()) {
                $headBlock->setTitle($title);
            }
            if ($description = $category->getMetaDescription()) {
                $headBlock->setDescription($description);
            }
            if ($keywords = $category->getMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            if ($this->helper('catalog/category')->canUseCanonicalTag()) {
                $headBlock->addLinkRel('canonical', $category->getUrl());
            }
        }
        return $this;
    }
*/



}