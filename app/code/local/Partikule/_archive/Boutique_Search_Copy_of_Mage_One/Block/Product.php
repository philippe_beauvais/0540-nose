<?php

/**
 *
 
 Important improvment : 
 - Check for stocks ! 

$collection->getSelect()->joinLeft(      
       array('stock'=>'cataloginventory_stock_item'),     
       'stock.product_id = e.entity_id',      
       array('stock.qty', 'stock.is_in_stock')      
     );
$products->addAttributeToFilter('is_in_stock', 1)

// Get categories from product


$all_cats = $product_model->getCategoryIds($_product);
 
*/

class Partikule_Boutique_Block_Product extends Mage_Core_Block_Template
{
	/*
	 * Current attribute
	 *
	 */
	private $_attribute = NULL;
	


	public function getProductCollection($attributes, $limit_to_configurable = FALSE)
	{
		// Get the attribute (code) acting as root filter
		$mainAttributeCode = Mage::getStoreConfig('boutique/settings/attribute');

		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('catalog/product')->getCollection()
		->setStoreId( $storeId );
		
		Mage::getModel('catalog/layer')->prepareProductCollection($collection);

		
		$collection->addAttributeToSelect("*");
		
		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$collection->addAttributeToFilter($code, $value);
		}
		
        // Join to perfume_detail to get the perfume details
        $collection->getSelect()->joinLeft(
			array('pd' => 'perfume_detail'),
			"pd.nose_sku = e.sku and pd.lang='". Mage::helper('attribute/core')->getCurrentStoreCode() ."'",
			array('*')
		);


		// Get the category url_path attribute table and id
		// $collection->joinTable('catalog/category_product', 'product_id=entity_id', array('category_id' => 'category_id'));
		$res = Mage::getSingleton('core/resource');
		$eav = Mage::getModel('eav/config');
		$nameattr = $eav->getAttribute('catalog_category', 'url_path');
		
		$nametable = $res->getTableName('catalog/category') . '_' . $nameattr->getBackendType();
		$nameattrid = $nameattr->getAttributeId();

		$collection->joinTable('catalog/category_product', 'product_id=entity_id', array('single_category_id' => 'category_id'), null, 'left')
			->groupByAttribute('entity_id')
			->joinTable(
				$nametable,
				'entity_id=single_category_id', 
				array('category_url' => 'value'),
				array('attribute_id'=> $nameattrid, 'store_id' => $storeId), 
				'left'
			)
			->getSelect()->columns(array('category_urls' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`$nametable`.`value` SEPARATOR '; '), '')")))
		;

//		Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'sql.log');

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

		
		return $collection;


		foreach($collection as &$product)
		{
			trace($product->getData());
		}
	}
	
	
	/**
	 * Returns the product collection filtered by attribute
	 * If no attribute is given, ...
	 * 
	 * @param		String   		Attribute Name
	 * @param		String			
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	public function getNewProductsCollection($limit_to_configurable = FALSE)
	{
		// Get the main Attribute. Registered by : Partikule_Boutique_CategoryController
		$main_attribute = Mage::registry('main_attribute');
		$main_attribute_option = Mage::registry('main_attribute_option');

		$collection = Mage::getModel('catalog/product')->getCollection();
		Mage::getModel('catalog/layer')->prepareProductCollection($collection);
		
		$collection->addAttributeToSelect("*")
			->addAttributeToFilter($main_attribute , $main_attribute_option['option_id'])	// 'boutique_product_type' 
			->addAttributeToFilter($this->getAttribute_code() , 1)							// 'boutique_new' is set to "yes" in the backend
		;

        // Join to perfume_detail to get the perfume details
        $collection->getSelect()->joinLeft(
			array('pd' => 'perfume_detail'),
			"pd.nose_sku = e.sku and pd.lang='". Mage::helper('attribute/core')->getCurrentStoreCode() ."'",
			array('*')
		);

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
		{
			$collection->addAttributeToFilter('type_id', 'configurable');
		}

		// trace("{$collection->getSelect()}".PHP_EOL);

		return $collection;
	}
	 */
	
	
	/**
	 * Return the current attribute
	 * attribute_code was set through its code by boutique.xml
	 *
	 * @return 	string		bla
	 *
	 *
	 */
	public function getAttribute()
	{
		return Mage::helper('attribute/data')->getAttribute($this->getAttribute_code());
	}


	
	public function getAttributesOptions($attributeCode)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
		return $options;
	}



	
}