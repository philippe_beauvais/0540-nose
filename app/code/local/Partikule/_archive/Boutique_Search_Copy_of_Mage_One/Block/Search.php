<?php

/**
 *
 * http://nose.home/fr/catalogsearch/advanced/
 *
 */

class Partikule_Boutique_Block_Search extends Mage_CatalogSearch_Block_Advanced_Form
{
	
	private $_attributes = NULL;
	
	
	/**
	 * Gets one attribute by code
	 *
	 * @return 	Attribute instance
	 *
	 */
	public function getAttributeByCode($attribute_code)
	{
		if ( is_null($this->_attributes))
			$this->_attributes = $this->getSearchableAttributes();
		
		foreach($this->_attributes as $attribute)
		{
			if ($attribute->getAttributeCode() == $attribute_code)
				return $attribute;
		}
		
		return FALSE;
	}
	
	
    /**
     * Retrieve advanced search model object
     *
     * @return Mage_CatalogSearch_Model_Advanced
     */
    public function getModel()
    {
        return Mage::getSingleton('boutique/catalogsearch_advanced');
    }

	
	/**
	 * Returns the specifi search URL
	 *
	 */
    public function getSearchPostUrl()
    {
        return $this->getUrl('*/search/result');
    }
	
	
	/**
	 * Returns all Categories which are child of the Brands parent category defined in boutique.xml
	 * used in the search form to search by categories (brands)
	 *
	 * @return	Categories Collection
	 *
	 */
	public function getBrandsCategories()
    {
        $helper = Mage::helper('boutique/category');
        return $helper->getBrandsCategories();
    }


}