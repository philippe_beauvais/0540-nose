<?php
/**
 * URL Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Boutique_Helper_Attribute extends Mage_Core_Helper_Abstract
{

	public function getOptionFromAttribute($attributeCode, $optionName)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
		foreach($options as $option)
		{
			if ( strtolower($optionName) == strtolower($option['default_value']) )
			{
				return $option;
			}
		}
		
		return FALSE;
	}


	/**
	 * Checks the option agains the attribute
	 *
	 * @param 	string 		Attribute code
	 * @param 	string 		Attribute's default value (admin name)
	 *
	 * @return 	boolean		TRUE if the attribute has the option
	 *						FALSE if not.
	 *
	 */
	public function hasAttributeOption($attributeCode, $option_value)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
		foreach($options as $option)
		{
			if ( $option_value == strtolower($option['default_value']) )
			{
				return TRUE;
			}
		}
		
		return FALSE;
	}


	
}