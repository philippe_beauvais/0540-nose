<?php
/**
 * Category Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Boutique_Helper_Category extends Mage_Core_Helper_Abstract
{


	/**
	 * Returns Childs of the category ID defined in boutique.xml
	 *
	 * @return 	Category Collection
	 *
	 */
	public function getBrandsCategories()
	{
		// Parent Brands category ID : All childs are brands
		$brands_category_id = Mage::getStoreConfig('boutique/settings/brands_category_id');
		
		if ($brands_category_id)
		{
			$parent_category = Mage::getModel('boutique/category')->load($brands_category_id);
		
			return $parent_category->getChildrenCollection();
		}
		
		return FALSE;
	}




}