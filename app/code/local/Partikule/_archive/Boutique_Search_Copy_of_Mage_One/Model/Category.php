<?php

/**
 * Catalog category
 *
 */
class Partikule_Boutique_Model_Category extends Mage_Catalog_Model_Category
{
	/**
	 * Gets one category childrens and order them by one attrobute.
	 * http://www.netismine.com/magento/get-sort-category-children-as-collection
	 *
	 * @return Categories Collection
	 *
	 */
	public function getChildrenCollection($sort=true, $order='name') 
	{
		$childrenIdArray = explode(',',$this->getChildren());
		
		if ($key = array_search($this->getId(),$childrenIdArray)) {
			unset($childrenIdArray[$key]);
		}
		
		$collection = $this->getCollection()
			->addAttributeToFilter('entity_id', array('in' => $childrenIdArray))
			->addAttributeToSelect('*');
	 
		if ($sort) {
			return $collection->addOrderField($order);
		}
		
		return $collection;			
	}


}
