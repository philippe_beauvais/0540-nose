<?php

Mage::log('Installing Partikule_Cust Module 0.1.5', null, 'partikule.log');

$installer = $this;

$installer->startSetup();

$installer->installEntities();

// $installer->removeAttribute('customer', 'id_origin');
// $installer->removeAttribute('customer', 'customer_status');

/* 
 * Put into customer_form_attribute table so field will available :
 * - in admin,
 * - in customer account edit
 * - in customer account create
 * - optional : checkout_register
 *
 */
Mage::getSingleton('eav/config')
	->getAttribute('customer', 'my_country')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->setData('sort_order', 69)
	->save();


Mage::getSingleton('eav/config')
	->getAttribute('customer', 'my_city')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->setData('sort_order', 70)
	->save();

Mage::getSingleton('eav/config')
	->getAttribute('customer', 'id_origin')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->setData('sort_order', 70)
	->save();


Mage::getSingleton('eav/config')
	->getAttribute('customer', 'customer_status')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->save();

$installer->endSetup();


?>