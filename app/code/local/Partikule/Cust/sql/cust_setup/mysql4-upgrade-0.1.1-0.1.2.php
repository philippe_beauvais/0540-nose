<?php

Mage::log('Upgrading Partikule_Cust Module to 0.1.2', null, 'partikule.log');


$installer = $this;

$installer->startSetup();

$installer->installEntities();

/* 
 * Put into customer_form_attribute table so field will available :
 * - in admin,
 * - in customer account edit
 * - in customer account create
 * - optional : checkout_register
 *
 */
Mage::getSingleton('eav/config')
	->getAttribute('customer', 'my_zip')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->setData('sort_order', 68)
	->save();

$installer->endSetup();


?>