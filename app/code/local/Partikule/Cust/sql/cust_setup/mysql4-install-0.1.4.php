<?php

Mage::log('Installing Partikule_Cust Module 0.1.4', null, 'partikule.log');

$installer = $this;

$installer->startSetup();

$installer->installEntities();

/* 
 * Put into customer_form_attribute table so field will available :
 * - in admin,
 * - in customer account edit
 * - in customer account create
 * - optional : checkout_register
 *
 */
Mage::getSingleton('eav/config')
	->getAttribute('customer', 'id_origin')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->save();


Mage::getSingleton('eav/config')
	->getAttribute('customer', 'customer_status')
	->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit', 'customer_account_create'))
	->save();

$installer->endSetup();


?>