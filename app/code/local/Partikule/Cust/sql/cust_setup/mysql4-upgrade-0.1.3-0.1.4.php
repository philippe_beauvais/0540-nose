<?php

Mage::log('Upgrading Partikule_Cust Module to 0.1.4', null, 'partikule.log');

$installer = $this;

$installer->startSetup();

// Add customer Status and Customer Origin
Mage::getSingleton('eav/config')
	->getAttribute('customer', 'id_origin')
	->save();


Mage::getSingleton('eav/config')
	->getAttribute('customer', 'customer_status')
	->save();

$installer->endSetup();


?>