<?php
/**
 * Customer resource setup model
 *
 */
class Partikule_Cust_Model_Resource_Eav_Mysql4_Setup extends Mage_Eav_Model_Entity_Setup
{
	public function getDefaultEntities()
    {
        return array(
            'customer' => array(
                'entity_model'          =>'customer/customer',
                'table'                 => 'customer/entity',
                'increment_model'       => 'eav/entity_increment_numeric',
                'increment_per_store'   => false,
 				'attribute_model' 		=> 'customer/attribute',
            	'additional_attribute_table' => 'customer/eav_attribute',
            	'entity_attribute_collection' => 'customer/attribute_collection',
                'attributes' => array(
                    'my_country' => array(
                        'type'          => 'varchar',
                        'input'         => 'select',
                        'source'        => 'customer/entity_address_attribute_source_country',
                        'label'         => 'Country',
                        'visible'       => true,
                        'required'      => false,
                        'position'      => 69,
                    ),
                    'my_city' => array(
                        'type'          => 'varchar',
                        'input'         => 'text',
                        'label'         => 'City',
                        'visible'       => true,
                        'required'      => false,
                        'position'      => 69,
                    ),
					'my_zip' => array(
						'type'          => 'varchar',
						'input'         => 'text',
						'label'         => 'Postal Code',
						'visible'       => true,
						'required'      => false,
						'position'      => 68,
					),
					'id_origin' => array(
						'label'         => 'ID Origin',
						'visible'       => true,
						'required'      => false,
						'position'      => 72,
					),
					'customer_status' => array(
						'label'         => 'Customer status',
						'visible'       => true,
						'required'      => false,
						'position'      => 73,
					),
				),
            ),
        );
    }
}


