<?php


/**
 Add Expression To Collection Select
 *
 */
$collection = Mage::getResourceModel('sales/order_collection')
            ->addAttributeToSelect('*')
            ->joinAttribute('group', 'customer/group_id', 'customer_id', null, 'left')
            ->joinAttribute('billing_firstname', 'order_address/firstname', 'billing_address_id', null, 'left')
            ->joinAttribute('billing_lastname', 'order_address/lastname', 'billing_address_id', null, 'left')
            ->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
            ->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
            ->addExpressionAttributeToSelect('billing_name',
                'CONCAT({{billing_firstname}}, " ", {{billing_lastname}})',
                array('billing_firstname', 'billing_lastname'))
            ->addExpressionAttributeToSelect('shipping_name',
                'CONCAT({{shipping_firstname}}, " ", {{shipping_lastname}})',
                array('shipping_firstname', 'shipping_lastname')); 

$groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  Mage::helper('sales')->__('Customer Group'),
            'width'     =>  '100',
            'index'     =>  'group',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));
        
/* Joint the stock 
*/

$collection->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
->setStore(Mage::app()->getStore())
->addUrlRewrite();

