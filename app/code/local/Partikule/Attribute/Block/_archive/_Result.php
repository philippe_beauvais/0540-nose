<?php


class Partikule_Attribute_Block_Result extends Mage_Catalog_Block_Product_List
{
	protected function _getProductCollection()
	{
		if (is_null($this->_productCollection)) {
			$collection = Mage::getResourceModel('catalog/product_collection');
			Mage::getModel('catalog/layer')->prepareProductCollection($collection);

			$colorId = $this->getRequest()->getParam('filterColor', 0);
			$categoryId = $this->getRequest()->getParam('filterCategory', 0);

			if (!$colorId && !$categoryId) {
				$this->_redirect('*/*/index');
			}

			if ($colorId) {
				$collection->addAttributeToFilter('color', $colorId);
			}

			if ($categoryId) {
				$category = Mage::getModel('catalog/category')->load($categoryId);
				$collection->addCategoryFilter($category, true);
			}

			$this->_productCollection = $collection;
		}

		return $this->_productCollection;
	}
}