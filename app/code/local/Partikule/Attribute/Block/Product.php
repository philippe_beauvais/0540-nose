<?php


class Partikule_Attribute_Block_Product extends Mage_Catalog_Block_Product_Abstract
{

	/**
	 * Returns the product collection of one category, filtered by attribute
	 * If no attribute is given, returns the complete product list from the category
	 * 
	 * @param		array   	Array of attributes to filter on
	 * @param		boolean		Limit to configurables. Default TRUE
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getProductCollection($attributes = array(), $limit_to_configurable = TRUE)
	{
		$category_id = Mage::registry('category_id');

		if ( ! is_null($category_id))
		{
			$category = Mage::getModel('catalog/category')->load($category_id);
			$collection = $category->getProductCollection();
		}
		else
		{
			$storeId = Mage::app()->getStore()->getId();
			$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
			Mage::getModel('catalog/layer')->prepareProductCollection( $collection );
		}
		

		$collection->addAttributeToSelect('*')
			->addAttributeToFilter('status', 1)
			->addAttributeToFilter('visibility', 4)
			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'M-%')))
		;

        // Join to perfume_detail to get the perfume details
        // NOTICE : Specific to eNose project
        //
		// $collection = Mage::getModel('nose/Perfume')->joinToPerfumeDetail($collection);

		foreach($attributes as $key => $value)
		{
			// Mage::log($key .'::' .$value , null, 'partikule.log');
			$collection->addAttributeToFilter($key, $value);
		}

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

		// Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');
		// Mage::log($collection->getData(), null, 'partikule.log');
				
		return $collection;
	}
	
	
	
	/**
	 * Returns the cheapest simple product child of the configurable
	 *
	 */
/*
	public function getCheapestSimple($product)
	{
		return Mage::helper('boutique/Product')->getCheapestSimple($product);
	}
*/
	


/*
 * Code archive 
 *
 */
	
	
/*
		if ($attributeName != '' && $attributeValue != '')
		{
			$collection->addFieldToFilter(array(
					array('attribute'=>$attributeName,'like' => '%'.$attributeValue.'%')
				)
			);
			
			// "get attribute" dynamic method
			$getAttributeMethod = 'get'.$attributeName;
			
			// Validate the filter
			foreach($collection as $product)
			{
				// Get array of product attributes
				$attributeValues = $product->$getAttributeMethod();
				$attributeValues = explode(',', $attributeValues);
				
				// Remove products which have not the asked attribute
				if ( ! in_array($attributeValue, $attributeValues))
					$collection->removeItemByKey($product->getId());
			}
		}
*/
		
	
	
	
	
}