<?php


class Partikule_Attribute_Block_Navigation extends Mage_Core_Block_Template
{

	/**
	 * Navigation by Attribute values as items
	 * Returns array of attribute values for a given attribute
	 *
	 * If the category is given, only returns the attribute values which are set to the category production collection
	 *
	 * @param	String   												Attribute Code
	 * @param	Mixed : Category ID or Mage_Catalog_Model_Category		Optional. Category to get the attributes values from.
	 *
	 * @return	Array
	 *
	 */
	public function getNavigationItems($attributeCode = '', $category = NULL)
	{
		// Array of attribute values to return
		$returned_options = array();

		if ($attributeCode != '')
		{
			$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);

			// Returns all the options if the category isn't set
			if ( is_null($category))
			{
				$returned_options = $options;
			}
			else
			{
				// Loads the category if the ID is given
				if (is_int($category))
					$category = Mage::getModel('catalog/category')->load($category);

				// Gets the category products collection, in order to limit the returned attributes options items
				// Other method : $collection = Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($category);
				$collection = $category->getProductCollection()
					->addAttributeToSelect('*')
					->addAttributeToFilter('status', 1)
					->addAttributeToFilter('visibility', 4)
				;				


				// Product instance get method name to retrieve the Attribute value
				$getAttributeValue = 'get' . ucfirst($attributeCode);

				// Array of found attributes values IDs
				$found_values = array();
				
				// Check if products have each value of the attribute option
				// If not, the attribute isn't returned as menu item.
				foreach($options as $key => $opt)
				{
					foreach($collection as $product)
					{
						// Get product attribute value. Can be an coma separated list of values
						$attributeValues = $product->$getAttributeValue();
						$attributeValues = explode(',', $attributeValues);

						foreach($attributeValues as $val)
						{
							if ($opt['option_id'] == $val && ! in_array($opt, $returned_options))
								$returned_options[] = $opt;
						}
					}
				}
			}
		}

		return $returned_options;
	}
	
	
/*
	public function getAttributesOptions($attributeCode)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
		return $options;
	}
*/
	
	
	
	
	
	
	/**
	 * NOT USED !!!!
	 *
	 * @returns 	Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	protected function _getProductCollection()
	{
		if (is_null($this->_productCollection)) {
			$collection = Mage::getResourceModel('catalog/product_collection');
			Mage::getModel('catalog/layer')->prepareProductCollection($collection);

			$colorId = $this->getRequest()->getParam('filterColor', 0);
			$categoryId = $this->getRequest()->getParam('filterCategory', 0);

			if (!$colorId && !$categoryId) {
				$this->_redirect('*/*/index');
			}

			if ($colorId) {
				$collection->addAttributeToFilter('color', $colorId);
			}

			if ($categoryId) {
				$category = Mage::getModel('catalog/category')->load($categoryId);
				$collection->addCategoryFilter($category, true);
			}

			$this->_productCollection = $collection;
		}

		return $this->_productCollection;
	}

}