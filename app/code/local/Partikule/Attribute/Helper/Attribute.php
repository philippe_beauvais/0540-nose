<?php
/**
 * Attribute Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Attribute_Helper_Attribute extends Mage_Core_Helper_Abstract
{

	/**
	 * Returns one attribute's option backend value
	 *
	 * @param 	string		Attribute's code
	 * @param 	int			Option ID
	 *
	 * @return 	string		Admin's side option value
	 *
	 * @usage	bla
	 *
	 */
	public function getOptionBackendValue($attribute_code, $option_id)
	{
		$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_code);
		$options = $attribute->getSource()->getAllOptions(true, true);
		$label = '';
		
		foreach($options as $option)
		{
			if ($option['value'] == $option_id)
			{
				$label = $option['label'];
				break;
			}
		}
		
		return $label;
	}

	
	/**
	 * Used by Product view : category template, to display the Range attribute option
	 *
	 */
	public function getAttributeOption($attribute_code, $option_id)
	{
		$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_code);
		$options = $attribute->getSource()->getAllOptions(true, true);
		$label = '';

		foreach($options as $option)
		{
			if ($option['value'] == $option_id)
			{
				return $option;
				break;
			}
		}
		
		return FALSE;

//		Mage::getModel('catalog/product')->getResource()->getAttribute("color");	
	}


}
