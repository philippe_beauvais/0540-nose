<?php

/**
 * Filtering functions
 * Inspired by : http://magedev.com/2009/07/05/creating-customizable-product-list/
 *
 */
class Partikule_Attribute_Helper_Data
{
    /**
     * Store Attributes cache
     *
     * @var array
     */
    protected $_storeAttrOptions = array();

    protected $_storeAttr = array();



	
	/**
	 * Gets one attribute by its code
	 *
	 */
	public function getAttribute($attribute_code)
	{
    	// Local cache
        $cacheKey   = 'attr_'.$attribute_code;
		
		// Loads the attribute
        if (isset($this->_storeAttr[$cacheKey]))
            return $this->_storeAttr[$cacheKey];
        else
        	$this->_storeAttr[$cacheKey] = Mage::getModel('catalog/entity_attribute')->setStoreId(Mage::app()->getStore()->getId())->loadByCode('catalog_product', $attribute_code);

		return $this->_storeAttr[$cacheKey];
	}

	
	
	/**
	 * Returns one attribute type based on its code
	 *
	 * @param	string		Attribute code
	 * @return	string		Attribute type: 'boolean', 'select', 'multiselect', 'text', 'textarea', 'media_image', 'gallery', 'price', 'hidden'
	 *						
	 */
	public function getAttributeType($attribute_code)
	{
		$attribute = $this->getAttribute($attribute_code);

		if ($attribute)
			return $attribute->getFrontend_input();
		
		return FALSE;
	}
	
	
	/**
	 * Returns the array of attributes options from one attribute code
	 * 
	 * @param	String 		Attribute code
	 *
	 * @return	Mage_Eav_Model_Mysql4_Entity_Attribute_Option_Collection		Collection of attributes
	 *
	 */
	public function getAttributeOptions($attribute_code)
	{
    	// Local cache
        $cacheKey   = 'attr_'.$attribute_code;

        if (isset($this->_storeAttrOptions[$cacheKey]))
            return $this->_storeAttrOptions[$cacheKey];

		$attribute = Mage::getModel('catalog/entity_attribute');
		
		$attribute->setStoreId(Mage::app()->getStore()->getId())->loadByCode('catalog_product', $attribute_code);

		$options = array();
		
		if ($attribute->getData('is_visible'))
		{
			$options = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
				->setAttributeFilter($attribute->getId())
				->setStoreFilter()
				->load()
				->getData()
//				->toOptionArray()
			;
			// trace("{$options->getSelect()}".PHP_EOL);
		}

        $this->_storeAttrOptions[$cacheKey] = $options;

		return $options;
	}
	
	
	/**
	 * Returns the array of attributes options from one attribute code
	 * 
	 * @param	Array			Associative array containing data of currently processed category
	 * @param	String			string added as a prefix of each level of category tree
	 * @param	Int				integer defining level from which category tree elements should be added to options. By default is set to 2 in order to omit root category (at level 1). 	 *
	 *
	 * @return	Array
	 *
	 */
	protected function _addCategoryOption($categoryData, $levelPrefix = '&raquo;', $startAtLevel = 2)
	{
		if ($categoryData['level'] >= $startAtLevel)
		{
			return array(
				'value' => $categoryData['entity_id'],
				'label' => str_repeat($levelPrefix, $categoryData['level'] - $startAtLevel) . $categoryData['name']
			);
		}

		return null;
	}
	
	
	/**
	 * Returns the array of options from Category
	 * 
	 * @return	Array		Array of options
	 *
	 */
	public function getCategoryOptions()
	{
		$options = array();
		
		$categoryCollection = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId())->getCollection()->addAttributeToSelect('name');
		
		foreach ($categoryCollection as $categoryData)
		{
			if ($option = $this->_addCategoryOption($categoryData))
			{
				$options[] = $option;
			}
		}

		return $options;
	}
	

	public function getProductFilteredAttributeOptions($attribute_code, $collection, $discard = NULL, $maxByOption=20)
	{
		// Array of attribute values to return
		$returned_options = array();

		if ( ! is_null($discard))
			$discard = (is_array($discard)) ? $discard : array($discard);

        $options = $this->getAttributeOptions($attribute_code);

        if ( ! empty($options))
        {
            // Product instance get method name to retrieve the Attribute value
            $getAttributeValue = 'get' . ucfirst($attribute_code);

            // Check if products have each value of the attribute option
            // If not, the attribute isn't returned as menu item.
            foreach($options as $key => $opt)
            {
                foreach($collection as $product)
                {
                    // Get product attribute value. Can be an coma separated list of values
                    $attributeValues = $product->$getAttributeValue();
                    $attributeValues = explode(',', $attributeValues);

                    foreach($attributeValues as $val)
                    {
                        if ($opt['option_id'] == $val)
                        {
                            if ( ! array_key_exists($opt['option_id'], $returned_options))
                            {
                                $returned_options[$opt['option_id']] = $opt;
                                $returned_options[$opt['option_id']]['nb_added'] = 0;
                                $returned_options[$opt['option_id']]['nb_total'] = 0;
                                $returned_options[$opt['option_id']]['products'] = array();
                            }

                    //        if ($returned_options[$opt['option_id']]['nb_added'] < $maxByOption)
                    //        {
                                $returned_options[$opt['option_id']]['products'][] = $product;
                                $returned_options[$opt['option_id']]['nb_added'] += 1;
                    //        }

                            $returned_options[$opt['option_id']]['nb_total'] += 1;
                        }
                    }
                }
            }

            // Removes discarded options value if any
            if ( ! is_null($discard))
            {
                $discard = (is_array($discard)) ? $discard : array($discard);

                foreach($returned_options as $key => $option)
                {
                    foreach($discard as $option_value)

                    if (strtolower($option['default_value']) == strtolower($option_value))
                    {
                        unset($returned_options[$key]);
                    }
                }
            }
        }
		return $returned_options;
	}	
}