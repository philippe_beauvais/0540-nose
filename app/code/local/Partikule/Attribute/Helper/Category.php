<?php

/**
 * Partikule Attribute Category Helper
 *
 */
class Partikule_Attribute_Helper_Category
{
	/**
	 * Store categories local cache
	 *
	 * @var array
	 */
	protected $_storeCategories = array();


	/**
	 * Returns one category based on its ID
	 *
	 * @param  	int   							Category ID
	 *
	 * @return  Mage_Catalog_Model_Category 
	 *
	 */
	public function getCategory($category_id)
	{
		// Loads the category if the ID is given
		if (is_int($category_id))
		{
			// Local cache
			$cacheKey   = 'category_'.$category_id;
			
			// Return cached category if any
			if (isset($this->_storeCategories[$cacheKey]))
				return $this->_storeCategories[$cacheKey];
			
			// Load the category from its ID
			$category = Mage::getModel('catalog/category')->load($category_id);
			
			// Store category
			$this->_storeCategories[$cacheKey] = $category;
			
			return $category;
		}
		
		return new Mage_Catalog_Model_Category();
	}
}