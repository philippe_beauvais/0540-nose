<?php

/**
 *
 */
class Partikule_Attribute_Helper_Label
{
	/**
	 * Retrieves what should be possible simply with Magento...
	 * Just the translated storeLabel of one attribute...
	 *
	 * They are 2 options : 
	 * 1. I'm just stupid
	 * 2. Magento & Varien likes make us loose our time...
	 *
	 */
	public function getStoreLabel($attribute)
	{
		if($attribute->getStoreLabel()) 
		{
		    return $attribute->getStoreLabel();
		}
		else 
		{
		    $storeLabels = $attribute->getStoreLabels(); 
		    if ( array_key_exists(Mage::app()->getStore()->getStoreId(), $storeLabels) )
		    {
		        return $storeLabels[Mage::app()->getStore()->getStoreId()];
		    } 
		    else
		    {
		        return $attribute->getFrontendLabel();
		    }
		}
	}
	
}