<?php
/**
 * Nose URL Helper
 * 
 *
 */
class Partikule_Lifecycle_Helper_Url extends Mage_Core_Helper_Abstract
{
	public function getApiUrl()
	{
		$env = Mage::getStoreConfig('lifecycle/settings/environment');
		$url = Mage::getStoreConfig('lifecycle/settings/'.$env.'/api_url');

		return $url;
	}
}
