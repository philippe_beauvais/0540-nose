<?php
/**
 * Lifecycle Helper
 *
 * @category    Partikule
 * @package     Partikule_Lifecycle
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Lifecycle_Helper_Data extends Mage_Core_Helper_Abstract {


	/**
	 *
	 * @param $action_code
	 * @param $id_user		Can be one user ID
	 *                    	or one user array
	 * @param $data      	Array. Additional data. Can be anything
	 *
	 */
	public function action($action_code, $id_user=NULL, $data=array())
	{
		// Log
		// $user_email = isset($id_user['email']) ? $id_user['email'] : '';
		// Mage::log('Action "' . $action_code .'" : ' . $user_email, null, 'partikule.log');

        $magento_id = NULL;

		if ( ! empty($id_user) && is_array($id_user) && ! empty($id_user['id_user'])) $id_user = $id_user['id_user'];

		if ( ! empty($data) && ! empty($data['entity_id'])) $magento_id = $data['entity_id'];

		// Lifecycle Server API URL
		$api_url = Mage::helper('lifecycle/url')->getApiUrl().'action/';

		// Mage::log('Partikule_Lifecycle_Helper_Data->action() : ' . $action_code, null, 'partikule.log');

		// eNoseMe Store ID : Different from Magento Store ID. "1" for the website
		$store_id = Mage::getStoreConfig('mynose/settings/store_id');

		$post = array(
			'action' => $action_code,
			'store_id' => $store_id,
			'id_user' => $id_user,
			'data' => $data
		);

		if ( ! empty($magento_id))
		    $post['magento_id'] = $magento_id;

		// Log
		// Mage::log('url : ' . $api_url, null, 'partikule.log', TRUE);
		// Mage::log($post, null, 'partikule.log', TRUE);

		Mage::helper('lifecycle/Request')->curl($api_url, $post);
	}
}
