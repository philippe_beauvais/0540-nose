<?php
/**
 * Lifecycle Request Helper
 *
 * @category    Partikule
 * @package     Partikule_Lifecycle
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */
class Partikule_Lifecycle_Helper_Request extends Mage_Core_Helper_Abstract
{
	public function curl($url, $post = null, $retries = 3)
	{
		$env = Mage::getStoreConfig('lifecycle/settings/environment');

		$api_key = Mage::getStoreConfig('lifecycle/settings/'.$env.'/api_key');

		$curl = curl_init($url);
	
	    if (is_resource($curl) === true)
	    {
	        curl_setopt($curl, CURLOPT_FAILONERROR, true);
	        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	
	        if (isset($post) === true)
	        {
	            curl_setopt($curl, CURLOPT_POST, true);
	            curl_setopt($curl, CURLOPT_POSTFIELDS, (is_array($post) === true) ? http_build_query($post, '', '&') : $post);
	        }

			// API key
			curl_setopt($curl,CURLOPT_HTTPHEADER,array('X-API-KEY: ' . $api_key));

			// .htaccess ?
			// $http_user = Mage::getStoreConfig('lifecycle/settings/'.$env.'/htuser');
			$http_user = Mage::getConfig()->getNode('default/lifecycle/settings/'.$env)->htuser;
			if ($http_user)
			{
				// $http_pass = Mage::getStoreConfig('lifecycle/settings/'.$env.'/htpassword');
				$http_pass = Mage::getConfig()->getNode('default/lifecycle/settings/'.$env)->htpassword;
				curl_setopt($curl, CURLOPT_USERPWD, "$http_user:$http_pass");
			}
	
	        $result = false;
	
	        while (($result === false) && (--$retries > 0))
	        {
	            $result = curl_exec($curl);
	        }

			if ( ! $result)
			{
				Mage::log('ERROR : Partikule_Lifecycle_Helper_Request->curl() : ' . $url. ' : '. curl_error($curl), null, 'partikule.log');
			}

	        curl_close($curl);
	    }
	
	    return $result;
	}
}