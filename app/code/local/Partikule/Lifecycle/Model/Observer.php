<?php
/**
 * Lifecycle
 *
 * @category    Partikule
 * @package     Partikule_Lifecycle
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */
class Partikule_Lifecycle_Model_Observer extends Mage_Core_Model_Abstract
{
	public function onInvitationAccepted($observer)
	{
		// $sponsor = $observer->getSponsor();
		// $sponsored = $observer->getSponsored();

		// Do stuff here...
	}
}
