<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Model_Url extends Mage_Core_Model_Url
{

	public function getUrl($routePath=null, $routeParams=null)
	{
		if (isset($routeParams['_autologin']))
		{
			$customer = ! empty($routeParams['_autologin_customer']) ? $routeParams['_autologin_customer'] : NULL;

			if (is_string($customer))
			{
				$validator = new Zend_Validate_EmailAddress();
				if($validator->isValid($customer))
					$customer = Mage::getModel('customer/customer')->loadByEmail($customer);
			}

			// Got one customer ?
			if ( ! is_null($customer) && !empty($customer) && $customer->hasData())
			{
				$login_hash = $customer->getLoginHash();
				$var = Mage::helper('autologin')->get_url_param_segment();

				if (isset($routeParams['_query']))
				{
					$q = $routeParams['_query'];

					if (is_string($q) && strlen($q) > 0)
						$routeParams['_query'] .= '&'.$var.'='.$login_hash;
					else if (is_array($q))
						$routeParams['_query'][$var] = $login_hash;
					else
						$routeParams['_query'] = array($var => $login_hash);
				}
				else
				{
					$routeParams['_query'][$var] = $login_hash;
				}
			}
			unset($routeParams['_autologin']);
			unset($routeParams['_autologin_customer']);
		}
		return parent::getUrl($routePath, $routeParams);
	}
}