<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */
class Partikule_Autologin_Model_Observer extends Mage_Core_Model_Abstract
{
	var $_updated = 0;

	/**
	 * Do the Autologin at frontend
	 *
	 */
	public function doAutologin()
	{
		// Mage::log('Partikule_Autologin_Model_Observer > doAutologin()', null, 'partikule.log');

		if (Mage::helper('autologin')->is_module_enabled())
		{
			$session = Mage::getSingleton('customer/session');
			$request = Mage::app()->getRequest();

			$hash = $request->getParam(Mage::helper('autologin')->get_url_param_segment());

			if ( ! empty($hash))
			{
				$csv = Mage::helper('autologin')->decrypt($hash);
				$_customerinfo = explode(';', $csv);

				$customer = Mage::getModel('customer/customer')->load((int)$_customerinfo[0]);

				if ( ! $customer->getId() || $customer->getEmail() != trim($_customerinfo[1]))
				{
					// $session->addError('User invalid or does not exist');
					$session->logout();
				}
				else{
					// Logout
					$session->logout();

					// Login
					$session->setCustomer($customer);
				}
			}
		}
	}

	public function updateCustomer($id)
	{
		try
		{
			$_customer = Mage::getModel('customer/customer')->load($id);

			$hash = Mage::helper('autologin')->encrypt($_customer);
			$_customer->setData('login_hash', $hash)->getResource()->saveAttribute($_customer, 'login_hash');
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}


	/**
	 * Updates all customers hashes
	 * Uses not more than 2M...
	 *
	 * @return int
	 */
	public function updateAllCustomers()
	{
		// Mage::log('Partikule_Autologin_Model_Observer > updateAllCustomers()', null, 'partikule.log');
		$start_memory = memory_get_usage();

		$customersCollection = Mage::getModel('customer/customer')->getCollection();

		Mage::getSingleton('core/resource_iterator')->walk(
			$customersCollection->getSelect(),
			array(array($this, 'customerCallback')),
			array('start_memory' => $start_memory)
		);

		// $mem = memory_get_usage() - $start_memory;
		// Mage::log('Autologin Init Memory Use (End) : ' . $this->_updated . ' users : ' . $mem, null, 'partikule.log');

		return $this->_updated;
	}


	/**
	 * Callback for customers hash update
	 *
	 * @param $args
	 */
	public function customerCallback($args)
	{
		$_customer = Mage::getModel('customer/customer');
		$_customer->setData($args['row']);
		// $start_memory = $args['start_memory'];

		$hash = Mage::helper('autologin')->encrypt($_customer);
		$_customer->setData('login_hash', $hash)->getResource()->saveAttribute($_customer, 'login_hash');

		$this->_updated += 1;

		// $mem = memory_get_usage() - $start_memory;
		// Mage::log('Autologin Init Memory Use : ' . $mem, null, 'partikule.log');
	}

	public function saveCustomer($observer)
	{
		// Mage::log('Partikule_Autologin_Model_Observer > saveCustomer()', null, 'partikule.log');

		$customer = $observer->getCustomer();

		$hash = Mage::helper('autologin')->encrypt($customer);

		$customer->setData('login_hash', $hash)->getResource()->saveAttribute($customer, 'login_hash');
	}
}
