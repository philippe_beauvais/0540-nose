<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Model_Api extends Mage_Api_Model_Resource_Abstract
{
	var $_items = array();


	/**
	 * Get customers list (email, id, hash)
	 *
	 * @param $filters
	 *
	 * @return array
	 */
	public function items($filters)
	{
		$this->_items = array();

		// $start_memory = memory_get_usage();

		$collection = Mage::getModel('customer/customer')->getCollection()
			->addAttributeToFilter('login_hash', array('neq' => 'NULL' ));

		if (is_array($filters))
		{
			try {
				foreach ($filters as $field => $value)
				{
					$collection->addFieldToFilter($field, $value);
				}
			}
			catch (Mage_Core_Exception $e)
			{
				$this->_fault('filters_invalid', $e->getMessage());
			}
		}

		Mage::getSingleton('core/resource_iterator')->walk(
			$collection->getSelect(),
			array(array($this, '_customerCallback'))
			/*
			,array(
				'start_memory' => $start_memory,
			)
			*/
		);

		return $this->_items;
	}


	public function url($customerId, $uri=NULL)
	{
		$url = '';
		$helper = Mage::helper('autologin');

		$var = $helper->get_url_param_segment();

		$customer = Mage::getModel('customer/customer')->load($customerId);

		if ($customer->hasData())
		{
			if ( is_null($uri))
				$uri = $uri = trim(Mage::getStoreConfig('customer/autologin/fronturl'), '/');

			if (empty($uri))
				$uri = 'checkout/cart';

			$login_hash = $customer->getLoginHash();

			$url = Mage::app()->getStore($customer->getStoreId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

			$url .= $uri . '/?' . $var. '=' . $login_hash;
		}
		return $url;
	}


	public function hash($customerId)
	{
		$hash = '';

		$customer = Mage::getModel('customer/customer')->load($customerId);

		if ($customer->hasData())
		{
			$hash = $customer->getLoginHash();
		}

		return $hash;
	}


	public function update($customerId)
	{
		try
		{
			Mage::getModel('autologin/observer')->updateCustomer($customerId);
		}
		catch (Mage_Core_Exception $e)
		{
			$this->_fault('data_invalid', $e->getMessage());
		}
		catch (Exception $e) {
			$this->_fault('data_invalid', $e->getMessage());
		}
		return $customerId;
	}


	/**
	 * Callback for customers hash update
	 *
	 * @param $args
	 */
	public function _customerCallback($args)
	{
		$_customer = $args['row'];

		$this->_items[] = array(
			'entity_id' => $_customer['entity_id'],
			'email' => $_customer['email'],
			'login_hash' => $_customer['login_hash'],
		);
	}

}