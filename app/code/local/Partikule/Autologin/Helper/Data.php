<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Helper_Data extends Mage_Core_Helper_Abstract {

	public function encrypt($customer)
	{
		$email = $id = NULL;
		if ( ! is_array($customer))
		{
			$id = $customer->getId();
			$email = $customer->getEmail();
		}
		else
		{
			if (isset($customer['entity_id']) && isset($customer['email']))
			{
				$id = $customer['entity_id'];
				$email = $customer['email'];
			}
		}

		if ( ! is_null($id) && !is_null($email))
			return base64_encode(Mage::helper('core')->encrypt($id . ';' . $email));

		return '#empty#';
	}

	public function decrypt($hash)
	{
		return Mage::helper('core')->decrypt(base64_decode($hash));
	}


	/**
	 * Returns TRUE if the module is enabled
	 *
	 * @return boolean
	 */
	public function is_module_enabled()
	{
		return  Mage::getStoreConfig('customer/autologin/enabled');
	}


	/**
	 * Returns the autologin URL var name, as set in the settings
	 *
	 * @return string
	 */
	public function get_url_param_segment()
	{
		return  Mage::getStoreConfig('customer/autologin/urlparam');
	}


	/**
	 * Returns the first Frontend client URL
	 *
	 * @param $customer
	 *
	 * @return string
	 */
	public function get_main_autologin_url($customer)
	{
		return $this->_get_autologin_front_url($customer, 'fronturl');
	}

	/**
	 * Returns the second Frontend client URL
	 *
	 * @param $customer
	 *
	 * @return string
	 */
	public function get_custom_autologin_url($customer)
	{
		return $this->_get_autologin_front_url($customer, 'fronturl2');
	}


	public function get_autologin_front_url($customer, $key='fronturl')
	{
		$url = '';

		$var = Mage::getStoreConfig('customer/autologin/urlparam');
		$uri = trim(Mage::getStoreConfig('customer/autologin/'.$key), '/');
		$login_hash = $customer->getLoginHash();

		if (!empty($var) && !empty($uri))
		{
			$url = Mage::app()->getStore($customer->getStoreId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

			$url .= $uri . '/?' . $var. '=' . $login_hash;
		}

		return $url;
	}


	public function get_autologin_front_url_label($key='fronturl')
	{
		return Mage::getStoreConfig('customer/autologin/'.$key.'label');
	}
}
