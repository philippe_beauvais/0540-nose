<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Adminhtml_ListController extends Mage_Adminhtml_Controller_Action
{
	private $_export_fields = array(
		'entity_id',
		'name',
		'firstname',
		'lastname',
		'email',
		'login_hash'
	);

	private $_csv_separator = ';';

	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('customer/autologin_list');
		return $this;
	}


	/**
	 * Get Customers list with autologin hash
	 *
	 */
	public function indexAction()
	{
		$this->_initAction()->_addContent($this->getLayout()->createBlock('autologin/adminhtml_autologin'));
		$this->renderLayout();
	}

	/**
	 * Export customer grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'customers_with_login_hash.csv';

		$path = Mage::getBaseDir('var') . DS . 'export' . DS;
		$name = md5(microtime());
		$file = $path . DS . $name . '.csv';

		$io = new Varien_Io_File();
		$io->setAllowCreateFolders(true);
		$io->open(array('path' => $path));
		$io->streamOpen($file, 'w+');
		$io->streamLock(true);
		$io->streamWriteCsv($this->_getExportCsvHeaders(), $this->_csv_separator);

		$customersCollection = Mage::getModel('customer/customer')->getCollection()
			->addNameToSelect()
			->addAttributeToFilter('login_hash', array('neq' => 'NULL' ));

		Mage::getSingleton('core/resource_iterator')->walk(
			$customersCollection->getSelect(),
			array(array($this, '_exportCsv')),
			array('io' => $io)
		);

		$io->streamUnlock();
		$io->streamClose();

		$content = array(
			'type'  => 'filename',
			'value' => $file,
			'rm'    => true // can delete file after use
		);

		$this->_prepareDownloadResponse($fileName, $content);
	}


	public function _getExportCsvHeaders()
	{
		$row = array();
		foreach($this->_export_fields as $field)
		{
			$row[] = $field;
		}
		return $row;
	}


	public function _exportCsv($args)
	{
		$io = $args['io'];

		$data = $args['row'];
		$row = array();

		foreach($this->_export_fields as $field)
		{
			if (isset($data[$field]))
				$row[] = $data[$field];
			else
				$row[] = '';
		}
		$io->streamWriteCsv($row, $this->_csv_separator);
	}
}
