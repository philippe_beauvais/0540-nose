<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Adminhtml_AutologinController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Initialize all customers hashes
	 * Not done during installation because 2 visitors request had launched 2 times the
	 * init.
	 *
	 */
	public function initAction()
	{
		try
		{
			$nb_updated = Mage::getModel('autologin/observer')->updateAllCustomers();

			$message = $this->__('Operation done<br/>' . $nb_updated . ' customers updated.');

			Mage::getSingleton('adminhtml/session')->addSuccess($message);
		}
		catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}

		$this->_redirectUrl($this->_getRefererUrl());
	}


	/**
	 * Autologin from the Admin Panel (Customer Grid)
	 *
	 */
	public function loginAction()
	{
		$id = (int) trim($this->getRequest()->getParam('id'));

		$session = $this->_getSession();

		$helper = Mage::helper('autologin');

		$url = '';

		if ( ! $helper->is_module_enabled())
		{
			$message = $this->__('The Autologin module is not enabled');
			$session->addError($message);
			$this->_redirect('customer/account/login');
		}
		else
		{
			try
			{
				if($id)
				{
					$customer = Mage::getModel('customer/customer')->load($id);

					/*
					$session->setCustomerAsLoggedIn($customer);
					$message = $this->__('You are now logged in as %s', $customer->getName());
					$session->addNotice($message);
					*/

					$url = $helper->get_autologin_front_url($customer, 'fronturl');
				}
				else{
					throw new Exception ($this->__('The login attempt was unsuccessful. Some parameter is missing'));
				}
			}
			catch (Exception $e)
			{
				$session->addError($e->getMessage());
			}

			if ( ! empty($url))
			{
				$this->_redirectUrl($url);
			}
			else
			{
				$message = $this->__('The Autologin module is not enabled');
				$session->addError($message);
				$this->_redirect('customer/account');
			}
		}
	}


}