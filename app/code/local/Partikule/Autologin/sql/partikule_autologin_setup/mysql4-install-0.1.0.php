<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

$installer = $this;
$installer->startSetup();

$custEntityTypeId = $installer->getEntityTypeId('customer');
$custAttributeSetId = $installer->getDefaultAttributeSetId($custEntityTypeId);
$custAttributeGroupId = $installer->getDefaultAttributeGroupId($custEntityTypeId, $custAttributeSetId);

$installer->removeAttribute('customer', 'login_hash' );

$installer->addAttribute(
	$custEntityTypeId,
	'login_hash',
	array(
		'backend' => '',
		'frontend' => '',
		'label' => 'Login Hash',
		'input' => 'text',
		'type'  => 'varchar',
		'required' => 0,
		'user_defined' => 0,
		'unique' => 0,
	)
);
$installer->addAttributeToGroup($custEntityTypeId, $custAttributeSetId, $custAttributeGroupId, 'login_hash', 0);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'login_hash');
$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();

$installer->endSetup();
