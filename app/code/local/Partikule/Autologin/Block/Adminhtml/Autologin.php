<?php
class Partikule_Autologin_Block_Adminhtml_Autologin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		//where is the controller
		$this->_controller = 'adminhtml_autologin';
		$this->_blockGroup = 'autologin';
		//text in the admin header
		$this->_headerText = Mage::helper('autologin')->__('Customer Autologin List');

		//value of the add button
//		$this->_addButtonLabel = 'Add a contact';

		parent::__construct();

		$this->removeButton('add');
	}
}