<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Block_Adminhtml_Autologin_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('autologinGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('customer/customer')->getCollection()
			->addNameToSelect()
			->addAttributeToSelect('email')
			->addAttributeToFilter('login_hash', array('neq' => 'NULL' ));

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('entity_id', array(
			'header'    => Mage::helper('customer')->__('ID'),
			'width'     => '50px',
			'index'     => 'entity_id',
		));
		$this->addColumn('name', array(
			'header'    => Mage::helper('customer')->__('Name'),
			'index'     => 'name'
		));
		$this->addColumn('email',
			array(
				'header' => $this->__('Email'),
				'align' =>'left',
				'index' => 'email',
			));
		$this->addColumn('login_hash', array(
			'header' => Mage::helper('autologin')->__('Login Hash'),
			'align' =>'left',
			'index' => 'login_hash',
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('customer')->__('CSV'));
		// $this->addExportType('*/*/exportXml', Mage::helper('customer')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/customer/edit', array('id' => $row->getId()));
	}

}