<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */
class Partikule_Autologin_Block_Adminhtml_System_Config_Initbutton extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	/**
	 * Set template to itself
	 */
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		if (!$this->getTemplate()) {
			$this->setTemplate('partikule/autologin/system/config/initbutton.phtml');
		}
		return $this;
	}


	/**
	 * Get the button and scripts contents
	 *
	 * @param Varien_Data_Form_Element_Abstract $element
	 * @return string
	 */
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$originalData = $element->getOriginalData();
		$this->addData(array(
			'button_label' => $originalData['button_label'],
			'button_url'   => $this->getUrl($originalData['button_url']),
			'html_id' => $element->getHtmlId(),
		));
		return $this->_toHtml();
	}
}
