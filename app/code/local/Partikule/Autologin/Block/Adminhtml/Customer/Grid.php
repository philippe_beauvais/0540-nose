<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid
{
	protected function _prepareColumns()
	{
		parent::_prepareColumns();

		$this->addColumnAfter('login',
			array(
				'header'    =>  Mage::helper('customer')->__('Login'),
				'width'     => '50',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('customer')->__('Cart'),
						'url' => array('base' => 'autologin/adminhtml_autologin/login'),
						'field'     => 'id',
						'target'=>'_blank',
					),
				),
				'filter'    => false,
				'sortable'  => false,
				'index'     => 'stores',
				'is_system' => true,
			),
			'action'
		);

		return $this;
	}
}