<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Autologin
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Autologin_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Customer_Edit
{
	public function  __construct()
	{
		if ($this->getCustomerId())
		{
			$helper = Mage::helper('autologin');

			$customer = Mage::getModel('customer/customer')->load($this->getCustomerId());

			$url = $helper->get_autologin_front_url($customer, 'fronturl');
			$label = $helper->get_autologin_front_url_label('fronturl');

			if ( ! empty($url) && ! empty($label))
			{
				$this->_addButton('button_al_main', array(
					'label'     => $label,
					'onclick' => "popWin('".$url."', '_blank')",
					'class'     => 'go'
				), 0, 0, 'header', 'header');
			}

			$url = $helper->get_autologin_front_url($customer, 'fronturl2');
			$label = $helper->get_autologin_front_url_label('fronturl2');

			if ( ! empty($url) && ! empty($label))
			{
				$this->_addButton('button_al_custom', array(
					'label'     => $label,
					'onclick' => "popWin('".$url."', '_blank')",
					'class'     => 'go'
				), 0, 1, 'header', 'header');
			}
		}

		parent::__construct();
	}
}