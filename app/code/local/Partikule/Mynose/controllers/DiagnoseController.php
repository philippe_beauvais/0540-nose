<?php

class Partikule_Mynose_DiagnoseController extends Partikule_Mynose_Controller_Abstract
{
	
	protected $_contactModel = NULL;
	
	
    public function preDispatch()
    {
        parent::preDispatch();

        $action = $this->getRequest()->getActionName();

        if (!$this->getRequest()->isDispatched())
        {
            return;
        }

		// Pattern set in parent : Partikule_Mynose_Controller_Abstract
        $pattern = self::DISPATCH_PATTERN;

		// If not one one this pattern, and user not logged in
        if (!preg_match($pattern, $action))
        {
            if ( ! $this->_getSession()->authenticate($this))
            {
            	/* Get the invited eNose user and puts its ID in session
            	 * Only if invited hasn't a Magento account
            	 * This is done to avoid eNose users table update problem when
            	 * the user hasn't one Magento ID
            	 * @see : 	Partikule_Mynose_Model_User->updateNoseAccount()
            	 *			and Partikule_Mynose_Model_User->getNoseUser()
            	 *
            	 */
            	if ($action == 'do')
            	{
					// Facebook potential user
					$origin = Mage::app()->getRequest()->getParam('o');
			
					if ($origin == 'fb')
					{
						// Facebook UID
						$facebook_id = Mage::app()->getRequest()->getParam('u');
						
						if ($facebook_id)
						{
							$user = $this->_getContactModel()->getUserFrom(array('facebook_id' => $facebook_id));
							
							if ($user)
							{
								$hash = md5($user['facebook_id'] . 'fb' . $user['id_user'] );
								
								if ($hash == Mage::app()->getRequest()->getParam('h'))
								{
									Mage::getSingleton('core/session')->setInvitedUserNoseId($user['id_user']);
								}
							}
						}
					}
				}
                $this->setFlag('', 'no-dispatch', true);
            }
            else
            {
                $this->_getSession()->setNoReferer(true);
            }
        }
        else
        {
			$this->_getSession()->setNoReferer(true);
        }
	}
	
		
	/**
	 * Displays the Welcome screen
	 *
	 *
	 */
	public function indexAction()
	{
		// Redirect to "do"
		$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getDiagnoseUrl());
	}
	
	
	/**
	 * Displays the Diagnostic form
	 *
	 *
	 */
	public function doAction()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}
	
	
	/**
	 * Displays the Diagnostic form
	 *
	 *
	 */
	public function createAction()
	{
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*');
            return;
        }

		// Redirect to the Result after registration
		if (Mage::helper('mynose/diagnostic')->hasInputsInSession() == TRUE)
		{
			Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('mynose/url')->getFormActionURL());
		}
		// Redirect to the Diagnose Form after registration
		else
		{
			Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('mynose/url')->getDiagnoseUrl());
		}

		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}
	
	
	private function _getContactModel()
	{
		if ( is_null($this->_contactModel))
			$this->_contactModel = Mage::getModel('mynose/contact');
		
		return $this->_contactModel;
	}

	
}