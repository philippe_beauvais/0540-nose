<?php

/**
 * Brand Controller
 * Used by XHR to get the brand list for the diagnose form
 *
 */
class Partikule_Mynose_BrandController extends Partikule_Mynose_Controller_Abstract
{
	/**
	 * Nothing
	 *
	 */
	public function indexAction()
	{

	}
	
	/**
	 *
	 *
	 */
	public function autocompleteAction()
	{
		// eNose URL
		$url = Mage::helper('mynose/url')->getEnoseUrl().'core/autocomplete/';
		
		
		// Pseudo post data, send through POST to the Diagnose application, 
		// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
		// These data are send back in the JSON answer
		$post = array (
			'base_url' => Mage::getBaseUrl(),
			'what' => 'brand',
			'search' => $this->getRequest()->getPost('search')
		);

		// Request to eNose
		$json = Mage::helper('mynose/Request')->curl($url, $post);
		
		echo($json);
		die();
	}

}