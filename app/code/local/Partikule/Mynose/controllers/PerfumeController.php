<?php

/**
 * Perfume Controller
 * Used by XHR to get the perfume list for the diagnose form
 *
 */
class Partikule_Mynose_PerfumeController extends Partikule_Mynose_Controller_Abstract
{
	/**
	 * Index
	 *
	 * If Ionize defines a Magento Template for the Home page, use it.
	 * Else, use the default Magento Home page
	 *
	 * This does not alter the Magento behavior regarding the home page.
	 *
	 */
	public function indexAction()
	{
	
	}
	
	
	/**
	 * Returns the Perfume Select HTML
	 *
	 * @return 	string		HTML
	 *
	 */
	public function perfumeselectAction()
	{
		$url = $this->_getEnoseUrl().'core/get_perfume_select/';
		
		$user = Mage::getSingleton('customer/session')->getCustomer();
		
		// Pseudo post data, send through POST to the Diagnose application, 
		// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
		// These data are send back in the JSON answer
		$post = array (
//			'base_url' => Mage::getBaseUrl(),
			'brand_id' => $this->getRequest()->getPost('brand_id'),
			'select_id' => $this->getRequest()->getPost('select_id'),
			'include_all' => $this->getRequest()->getPost('include_all'),
			'selected' => $this->getRequest()->getPost('selected'),
			'sex' => $user->getGender()
		);

		// Request to eNose
		$html = Mage::helper('mynose/Request')->curl($url, $post);

		echo($html);
		die();
	}

}