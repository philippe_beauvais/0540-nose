<?php

class Partikule_Mynose_VoucherController extends Partikule_Mynose_Controller_Abstract
{

    // Online
    const STORE_ID = 1;
    const DISTRIBUTOR_ID = 1;


    /**
     * Displays the current Evaluation List
     *
     */
    public function indexAction()
    {
        $this->loadLayout();

        // Set the current Ionize's page URL active
        // $this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

        $this->renderLayout();
    }

}
