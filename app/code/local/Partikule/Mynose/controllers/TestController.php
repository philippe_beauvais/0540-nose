<?php

class Partikule_Mynose_TestController extends Partikule_Mynose_Controller_Abstract
{

	/**
	 *
	 */
	public function indexAction()
	{
		// Set the Authentication back URL.
		// Mandatory for the Login form
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());

		$this->loadLayout();
		
		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->getLayout();
		$this->renderLayout();
	}
	
	/**
	 * Evaluation Open Test
	 *
	 */
	public function evalAction()
	{
	
		$testModel = Mage::getModel('mynose/test');
		
		$testModel->openEvaluation();
				
	
	
	}
	
	/**
	 * Facebook Connect Test
	 *
	 */
	public function fbAction()
	{
		trace('hop');
	}



	/**
	 * Sitemap Test
	 *
	 */
	public function smAction()
	{
		// $model = Mage::getModel('sitemap/sitemap');
		// $model->generateXml();
	}







	/*
 public function beforeLoadLayout($observer)
	 {
		 $loggedIn = Mage::getSingleton('customer/session')->isLoggedIn();

		 trace('youp');
		 $observer->getEvent()->getLayout()->getUpdate()
			->addHandle('diagnose_logged_'.($loggedIn?'in':'out'));

 }
 */

	/**
	 * Gets the Diagnose form data from eNose and displays the Diagnose Form
	 *
	 * @Note : Removed the 16.12.2012 because of change of the register approach
	 *
	 */
/*
	public function formAction()
	{
		// eNose Diagnose controller's URL
		// http://enose.tld/diagnose
		$url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'formdata';

		// Get the user
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$post['magento_id'] = $user->getId();

		// Pseudo post data, send through POST to the Diagnose application, 
		// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
		// These data are send back in the JSON answer
		$post = array (
			'base_url' => Mage::getBaseUrl(),
			'module_url' => Mage::getBaseUrl(). 'mynose',
			'autocomplete_url' => Mage::getBaseUrl(). 'mynose/brand/autocomplete/',
			'perfume_select_url' => Mage::getBaseUrl(). 'mynose/perfume/perfumeselect/',
			'form_action_url' => Mage::getBaseUrl(). 'mynose/result/',
			'antispam_hash' =>	Mage::getStoreConfig('mynose/settings/antispam_hash'),
			'magento_id' => $user->getId()
		);
	
		// Request to eNose
		$json = Mage::helper('mynose/Request')->curl($url, $post);
		$result = json_decode($json);

//Mage::log(print_r($result, true), null, 'mynose.log');		
		
		// Register all data from eNose ( + post data, sent by eNose)
		$this->_registerData($result);

Mage::log($result, null, 'partikule.log');

		$this->loadLayout();
		$this->renderLayout();
		
//		$this->_renderLayout('mynose/diagnose/diagnose_form.phtml');
		
	}
*/


	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	public function getPictureAction($perfume_id)
	{
		// eNose Server controller's URL
		$url = $this->_getEnoseUrl().'core/get_picture';
		
		$post = array (
			'perfume_id' => $perfume_id
		);

		// Request to eNose
		$pic = Mage::helper('mynose/Request')->curl($url, $post);
		
		echo($pic);
		
	}
	 */


	
	/**
	 * Get the Magento Post data in order to send then to Ionize
	 *
	 * @return		array		Magento Post data
	 *
	private function _getPostData($pageId)
	{
		$post = $this->getRequest()->getPost();

		//Get the info we need about the user, to pass to silverstripe
		$customerSession = Mage::getModel('customer/session');
		$isLogged = $customerSession->isLoggedIn();

		$post['magentoSession'] = $customerSession->getSessionId();
		$post['isUserLogged'] = $isLogged;
		$post['mageUserId'] = $customerSession->getCustomerId();
		
		return $post;
	}
	
	protected function _prepareLayout()
	{
		trace('Index Controller Prepare Layout');
	}

	 */
	
	
	
	
	
	

}