<?php

/**
 * Open Diag Controller
 * Used by XHR to get the brand list for the diagnose form
 * without the user check
 *
 */
class Partikule_Mynose_OpenController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Nothing
	 *
	 */
	public function indexAction()
	{
		die();
	}


	public function brandsAction()
	{
		$brandSelectHtml = Mage::helper('mynose/diagnostic')->getBrandAutocomplete($this->getRequest());

		echo $brandSelectHtml;
		die();
	}


	public function perfumesAction()
	{
		$perfumeSelectHtml = Mage::helper('mynose/diagnostic')->getPerfumeSelect($this->getRequest());

		echo $perfumeSelectHtml;
		die();
	}

	/**
	 * Same than Partikule_Mynose_ResultController->index(), but without the need to be logged in.
	 * Stores the perfumes in session
	 *
	 */
	public function resultAction()
	{
		$post = $this->getRequest()->getPost();

		$session = Mage::getSingleton('core/session');
		$session->setData('formPerfumes', $post);

		// Redirect to the diagnose result
		$this->getResponse()
			->setRedirect(Mage::helper('ionize/core')->getBaseUrl(TRUE) . 'mynose/result/');
	}


}