<?php

class Partikule_Mynose_WelcomeController extends Partikule_Mynose_Controller_Abstract
{

	/**
	 * Displays the MyNose Welcome
	 * as defined in mynose.xml/mynose_index_index : mynose.phtml
	 *
	 */
	public function indexAction()
	{
		// Set the Authentication back URL.
		// Mandatory for the Login form
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());

		$this->loadLayout();
		
		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->getLayout();
		$this->renderLayout();
	}
	
	
}