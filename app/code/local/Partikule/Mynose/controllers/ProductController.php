<?php
require_once 'Mage/Catalog/controllers/ProductController.php';

class Partikule_Mynose_ProductController extends Mage_Core_Controller_Front_Action // Mage_Catalog_ProductController
{
	
	
	/**
	 * Looks for one product regarding the perfume ID
	 * Called by XHR
	 *
	 * @see diagnostic.phtml
	 *
	 */
	public function indexAction()
	{
		// Master Product SKU = 'M-' + Perfume ID
		$sku = 'M-' . $this->getRequest()->getPost('pid');

		// Get the product from its SKU
		/*
		$product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
			->loadByAttribute('sku', $sku);
		*/

		$product_id = Mage::getModel('catalog/product')->getIdBySku($sku);

		// Because loading by Attribute doesn't returns the others attributes... WTF !
		$product = Mage::getModel('catalog/product')->load($product_id);

		Mage::register('product', $product);

		$this->loadLayout()->renderLayout();
	}


	/**
	 * Looks for one product regarding the perfume ID
	 * Called by XHR by the evaluation page
	 *
	 * @see evaluation.phtml
	 *
	 */
	public function evalAction()
	{
		$this->indexAction();
	}







    //Copy of parent _initProduct but changes visibility checks.
    //Reproducing functionality like this is far from great for future compatibilty
    //but at the moment I don't see a better alternative.
    protected function _initProduct()
    {
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id');
        $parentId   = (int) $this->getRequest()->getParam('pid');

        if (!$productId || !$parentId) {
            return false;
        }

        $parent = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($parentId);

        if (!Mage::helper('catalog/product')->canShow($parent)) {
            return false;
        }

        $childIds = $parent->getTypeInstance()->getUsedProductIds();
        if (!is_array($childIds) || !in_array($productId, $childIds)) {
            return false;
        }

        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);
        // @var $product Mage_Catalog_Model_Product
        if (!$product->getId()) {
            return false;
        }
        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            Mage::register('current_category', $category);
        }
        $product->setCpid($parentId);
        Mage::register('current_product', $product);
        Mage::register('product', $product);
        return $product;
    }
	
}