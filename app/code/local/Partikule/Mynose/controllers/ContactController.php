<?php

class Partikule_Mynose_ContactController extends Partikule_Mynose_Controller_Abstract
{
	const RELATION_STATUS_INVITED = 1;
	const RELATION_STATUS_ACCEPTED = 2;
	const RELATION_STATUS_REFUSED = 3;
	const RELATION_STATUS_DELETED = 4;

	const ORIGIN_ONLINE = 1;
	const ORIGIN_BOUTIQUE = 2;
	const ORIGIN_INVITATION = 3;
	const ORIGIN_PROSPECTION = 4;
	
	// Friend. See realtion_type table
	const DEFAULT_RELATION_TYPE_ID = 10;
	
	
	// Codes of unique relation : The user cannot have more than 1 kind of these relation types
	protected $_UNIQUE_RELATION_TYPES = array(1);

	protected $_contactModel = NULL;


    public function preDispatch()
    {
        parent::preDispatch();

        $action = $this->getRequest()->getActionName();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }
		
		// Pattern set in parent : Partikule_Mynose_Controller_Abstract
        $pattern = self::DISPATCH_PATTERN;
        
		// If not one one this pattern, and user not logged in
        if (!preg_match($pattern, $action))
        {
            if ( ! $this->_getSession()->authenticate($this))
            {
            	/* Get the invited eNose user and puts its ID in session
            	 * Only if invited hasn't a Magento account
            	 * This is done to avoid eNose users table update problem when
            	 * the user hasn't one Magento ID
            	 * @see : 	Partikule_Mynose_Model_User->updateNoseAccount()
            	 *			and Partikule_Mynose_Model_User->getNoseUser()
            	 *
            	 */
            	if ($action == 'acceptEmailInvitation' OR $action == 'rejectEmailInvitation')
            	{
					$senderId = Mage::app()->getRequest()->getParam('s');
					
					// Invited ID : Me ! 
					$invitedId = Mage::app()->getRequest()->getParam('i');
					$hash = Mage::app()->getRequest()->getParam('h');

					// User's Nose infos
					$invitedNoseUser = $this->_getContactNoseUser($invitedId);
					$senderNoseUser = $this->_getContactNoseUser($senderId);

					if ( ! empty($senderNoseUser) && ! empty($invitedNoseUser))
					{
						$session = Mage::getSingleton('core/session');

						$has_invitation_relation = $this->_getContactModel()->getRelation($senderNoseUser, $invitedNoseUser, self::RELATION_STATUS_INVITED, $hash);
	
						if ($has_invitation_relation)
						{
							if ($action == 'rejectEmailInvitation')
							{
								$result = $this->_getContactModel()->removeRelation($senderNoseUser, $invitedNoseUser, $hash, 'refuse');
		
								if ($result)
								{
									// Send Email
									// Mage::helper('mynose/email')->sendInvitationRefusedMail($senderNoseUser, $invitedNoseUser);

									// Lifecycle : Invitation Rejected
									Mage::helper('lifecycle')->action('invitation_rejected', $invitedNoseUser);

									$session->addSuccess($this->__("You refused the invitation from ") . $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname']);
								}
								// Clean Invitation session
								$this->_getContactModel()->cleanInvitationSession();
								
								// Idea : Have a dedicated screen to confirm deletion.
								// Not done, lack of time.
								// $action = $this->getRequest()->setActionName('rejectEmailConfirmation');
								// $this->rejectEmailConfirmationAction();
							}
							else
							{
								$session->setInvitedUserNoseId($invitedId);
								$session->setSenderUserNoseId($senderId);
								$session->setInvitationHash($hash);
								$session->setRelation($has_invitation_relation);

								// Fire Event
								Mage::dispatchEvent("contact_invitation_accepted_email", array(
									'sender' => $senderNoseUser,
									'invited' => $invitedNoseUser,
									'origin' => 'email'
								));
							}
						}
						else
						{
							$session->addError($this->__('This link has already be used and is not available anymore'));	
						}
		           	}
            	}
            	else if ( $action == 'saveFbRequests')
            	{
            		// Redirect in case the user isn't connected
            		echo "<script type='text/javascript'>top.location.href = '".Mage::helper('mynose/url')->getContactAddUrl()."';</script>";
					die();
            	}
            	
                $this->setFlag('', 'no-dispatch', true);
            }
        }
        else
        {
            $this->_getSession()->setNoReferer(true);
        }
    }


	public function indexAction()
	{
		$this->_renderContactLayout();
	}


	public function calendarAction()
	{
		$this->_renderContactLayout();
	}



	public function addAction()
	{
		$this->_renderContactLayout();
	}
	
	
	public function bookAction()
	{
		$this->_renderContactLayout();
	}


	public function inviteAction()
	{
		$this->_renderContactLayout();
	}


	/**
	 * Returns Customer Addresses for one contact
	 * Used through Ajax by the OPC billing page
	 * @DEPRECATED
	 *
	 */
	public function addressesAction()
	{
		$customer_id = $this->getRequest()->getPost('id');

		$customer = Mage::getModel('customer/customer')->load($customer_id);
		
		$data = array();
		foreach ($customer->getAddresses() as $address)
		{
			$data[] = $address->toArray();
		}
		
		echo json_encode($data);
		die();
	}
	
	
	/**
	 * Displays one contact's diagnostic
	 *
	 *
	 */
	public function diagnosticAction()
	{
		$id_contact = $this->getRequest()->getPost('id');
		$from = $this->getRequest()->getPost('from');

		// Current user and supposed Friend : From eNose DB
		$currentNoseUser = $this->_getCurrentNoseUser();
		$contactNoseUser = $this->_getContactNoseUser($id_contact);
		
		if ($this->_getContactModel()->getRelation($currentNoseUser, $contactNoseUser, self::RELATION_STATUS_ACCEPTED))
		{
			Mage::register('id_user', $contactNoseUser['id_user']);
	
			$this->loadLayout();

			// Set the current Ionize's page URL active
			$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

			$this->getLayout()->getBlock('head')->setTitle($this->__('Diagnostic of:') . ' ' . $contactNoseUser['firstname'] . ' ' . $contactNoseUser['lastname']);
	
			$this->renderLayout();
		}
		else
		{
			// No POST : Redirect to MyNose Home
			$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getMyNoseUrl());
		}
	}


	/**
	 *
	 * Saves the Facebook requests as Nose relations
	 * Called by XHR
     *
     * @note    Removed on 2017/11/08
     *          Not possible anymore.
	 *
	 */
	/*
	public function saveFbRequestsAction()
	{
        $post = $this->getRequest()->getPost();
        $currentNoseUser = $this->_getCurrentNoseUser();

            $friends = $facebook->call($args);

			// 3. Create one relation, if no one exists
			foreach($friends as $friend)
			{
				$is_new_user = FALSE;

				$facebook_id = $this->_getContactModel()->getFacebookUserId($friend);
				$requested_user = $this->_getContactModel()->getUserFrom(array('facebook_id' => $facebook_id));

				// Creates the user from his Facebook ID if he doesn't exists
				if ( ! $requested_user )
				{
					$is_new_user = TRUE;

					// Add the sponsor
					$friend['id_sponsor'] = $currentNoseUser['id_user'];
					$requested_user = $this->_getContactModel()->createUserFromFacebookFriend($friend);
				}

				// Create one relation in eNose between the 2 users
				if ($requested_user)
				{
					// Try to get the relation
					$relation = $this->_getContactModel()->getRelation($currentNoseUser, $requested_user);

					// No relation : Create
					if ( ! $relation)
					{
						$this->_getContactModel()->createRelation($currentNoseUser, $requested_user);

						if ($is_new_user)
						{
							Mage::dispatchEvent("contact_invitation_facebook_sent_to_new_user", array(
								'sender' => $currentNoseUser,
								'invited' => $requested_user,
								'origin' => 'facebook'
							));
						}
						else
						{
							Mage::dispatchEvent("contact_invitation_facebook_sent", array(
								'sender' => $currentNoseUser,
								'invited' => $requested_user,
								'origin' => 'facebook'
							));

						}
					}
					else
					{
						// Mage::log('Relation already exists : ' , null, 'partikule.log');
						// Mage::log($relation, null, 'partikule.log');
					}
				}
			}
		}
		// echo('done');

		$this->sendXhrResponse('');
	}
	*/


	
	/**
	 *
	 * Creates the invitation
	 * - The relation
	 * Validates the contact form
	 *
	 */
	public function createAction()
	{
		Mage::getSingleton('customer/session')->setCustomerFormData($this->getRequest()->getPost());

		$session = Mage::getSingleton('core/session');

		// Current Nose user's infos
		$currentNoseUser = $this->_getCurrentNoseUser();
		
		// 5 contacts can be added
	//	for($i=0; $i<5; $i++)
	//	{
			if (
				FALSE != $this->getRequest()->getPost('firstname') &&
				FALSE != $this->getRequest()->getPost('lastname') &&
				FALSE != $this->getRequest()->getPost('email')
			)
			{
				$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));
		
				if ( ! empty($invitedNoseUser) && $invitedNoseUser['email'] == $currentNoseUser['email'])
				{
					$session->addError($this->__('You cannot add yourself as contact'));
				}
				else
				{
					// Try to get the relation
					$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);
					
					// No relation : Create
					if ( ! $relation)
					{
						$is_new_user = FALSE;

						// Create User if he doesn't exists
						if ( empty($invitedNoseUser) OR ! $invitedNoseUser['id_user'])
						{
							// Create the pseudo _POST array
							$post = array(
								'firstname' => $this->getRequest()->getPost('firstname'),
								'lastname' => $this->getRequest()->getPost('lastname'),
								'email' => $this->getRequest()->getPost('email'),
								'id_sponsor' => $currentNoseUser['id_user'],
								'origin' => self::ORIGIN_INVITATION,
							);

							// Create the user
							$this->_getContactModel()->createUser($post);
							
							// Get again to feed the user ID
							$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));

							$is_new_user = TRUE;
						}
		
						// User exists : Create relation
						if ( ! empty($invitedNoseUser))
						{
							// Wait! :
							// First check if the invited user hasn't already a lover.
							// It is very bad to steal the lover from one another... pas beau, bouh !
							if (
								$this->_getContactModel()->isLoveRelationId($this->getRequest()->getPost('link'))
								&& $this->_getContactModel()->hasAlreadyLover($invitedNoseUser, $currentNoseUser)
							)
							{
								$session->addError($invitedNoseUser['firstname'] . $this->__(" has already one lover. You cannot invite him / her as lover."));	
							}
							else
							{
								$result = $this->_getContactModel()->createRelation($currentNoseUser, $invitedNoseUser, $this->getRequest()->getPost('link'));
								
								if ($result)
								{
									$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);
									
									// Set the Hash to the curent user
									$currentNoseUser['hash'] = $relation['hash'];

									// Fire Event
									if ($is_new_user)
									{
										Mage::dispatchEvent("contact_invitation_sent_to_new_user", array(
											'sender' => $currentNoseUser,
											'invited' => $invitedNoseUser,
											'relation' => $relation,
											'origin' => 'invitation'
										));
									}
									else
									{
										Mage::dispatchEvent("contact_invitation_sent", array(
											'sender' => $currentNoseUser,
											'invited' => $invitedNoseUser,
											'origin' => 'invitation'
										));
									}

									// Send Invitation Email
									// Mage::helper('mynose/email')->sendInvitationMail($currentNoseUser, $invitedNoseUser);
			
									$session->addSuccess($this->__('One invitation to be your friend has been sent to ') . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
								}
								else
								{
									$session->addSuccess($this->__('One problem happens during connexion with this user'));				
								}
							}
						}
					}
					else
					{
						if ($relation['status'] == self::RELATION_STATUS_INVITED)
							$session->addSuccess($this->__("You already invited ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
						else
							$session->addSuccess($this->__("You're already friend with ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
					}
				}
			}
	//	}

		$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}
	
	
	/**
	 *
	 * Remove one contact
	 *
	 */
	public function removeAction()
	{
		$session = Mage::getSingleton('core/session');

		// My friend : From eNose DB
		$contactNoseUser = $this->_getContactNoseUser($this->getRequest()->getPost('id'));
		
		if ( ! empty($contactNoseUser))
		{
			// Current Nose user's infos
			$currentNoseUser = $this->_getCurrentNoseUser();
			$result = $this->_getContactModel()->removeRelation($currentNoseUser, $contactNoseUser, NULL, 'delete');
			
			if ($result)
				$session->addSuccess($contactNoseUser['firstname'] . ' ' . $contactNoseUser['lastname'] . ' ' . $this->__("was removed from your contacts"));
			else
				$session->addError($this->__('You seem not linked to this user'));	
		}
		else
		{
			// $session->addError($this->__('You cannot be linked to this user'));	
		}
		
		// Redirect to book
		// $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactBookUrl());
        $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactAddUrl());

    }
	
	
	/**
	 *
	 * Reject one invitation
	 *
	 */
	public function rejectInvitationAction()
	{
		$session = Mage::getSingleton('core/session');

		// Invit. sender's : From eNose DB
		$senderNoseUser = $this->_getContactNoseUser($this->getRequest()->getPost('id'));
		
		if ( ! empty($senderNoseUser))
		{
			// Current Nose user's infos : Me !
			$currentNoseUser = $this->_getCurrentNoseUser();
			$result = $this->_getContactModel()->removeRelation($currentNoseUser, $senderNoseUser, NULL, 'refuse');
			
			if ($result)
			{
				// Lifecycle : Invitation Rejected
				Mage::helper('lifecycle')->action('invitation_rejected', $currentNoseUser);

				// Fire Event
				Mage::dispatchEvent("contact_invitation_rejected", array(
					'sender' => $senderNoseUser,
					'invited' => $currentNoseUser,
					'origin' => 'website'
				));

				// Send Email
				// Mage::helper('mynose/email')->sendInvitationRefusedMail($senderNoseUser, $currentNoseUser);
			
				$session->addSuccess($this->__("You rejected invitation from ") . $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname']);
			}
			else
				$session->addError($this->__('You seem not linked to this user'));	
		}
		else
		{
			// $session->addError($this->__('You cannot be linked to this user'));	
		}
		
		// Redirect to book
		// $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactBookUrl());
        $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}
	

	/**
	 *
	 * Accepts one invitation
	 *
	 */
	public function acceptInvitationAction()
	{
		$session = Mage::getSingleton('core/session');

		// Sender : The id of the passed user
		$senderNoseUser = $this->_getContactNoseUser($this->getRequest()->getPost('id'));

		if ( ! empty($senderNoseUser))
		{
			// Invited : It's me !
			$invitedNoseUser = $this->_getCurrentNoseUser();
			
			// Relation ?
			$relation = $this->_getContactModel()->getRelation($senderNoseUser, $invitedNoseUser);
			
			// Set relation to "Friend" if the invited user / sender has already one lover relation
			if ( 
				$this->_getContactModel()->isLoveRelationId($relation['id_relation_type'])
				&&
				(
					$this->_getContactModel()->hasAlreadyLover($invitedNoseUser, $senderNoseUser) OR
					$this->_getContactModel()->hasAlreadyLover($senderNoseUser, $invitedNoseUser)
				)
			)
			{
				$this->_getContactModel()->setRelationType($invitedNoseUser, $senderNoseUser, self::DEFAULT_RELATION_TYPE_ID);
			}

			$result = $this->_getContactModel()->validateRelation($senderNoseUser, $invitedNoseUser);

			if ($result)
			{
				// Fire Event
				Mage::dispatchEvent("contact_invitation_accepted", array(
					'sender' => $senderNoseUser,
					'invited' => $invitedNoseUser,
					'origin' => 'website'
				));

				// If needed, call the Love Machine Diagnostic Creation service
				if ($this->_getContactModel()->isLoveRelationId($relation['id_relation_type']) )
					Mage::helper('mynose/diagnostic')->callLoverDiagnosticSave();

				// Send Email
				// Mage::helper('mynose/email')->sendInvitationAcceptedMail($senderNoseUser, $invitedNoseUser);
				
				$session->addSuccess($this->__("You're now linked with ") . $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname']);
			}
			else
			{
				$session->addError($this->__('You cannot be linked to this user'));	
			}
		}
		else
		{
			$session->addError($this->__('You cannot be linked to this user'));	
		}

		// Clean Invitation session
		$this->_getContactModel()->cleanInvitationSession();
		
		// Redirect to book
		// $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactBookUrl());
        $this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}


	


	/**
	 *
	 * Accepts one invitation sent by Mail
	 * Used only by Nose known users
	 *
	 */
	public function acceptEmailInvitationAction()
	{
		$session = Mage::getSingleton('core/session');

		// Sender ID
		$senderId = Mage::app()->getRequest()->getParam('s');
		
		// Invited ID : Me ! 
		$invitedId = Mage::app()->getRequest()->getParam('i');
		
		// Hash : to check
		$hash = Mage::app()->getRequest()->getParam('h');
		
		if ( ! empty($senderId) && ! empty($invitedId) && ! empty($hash) )
		{
			// User's Nose infos
			$senderNoseUser = $this->_getContactNoseUser($senderId);
			$invitedNoseUser = $this->_getContactNoseUser($invitedId);

			$currentUser = Mage::getModel('customer/session')->getCustomer();

			if ( ! empty($senderNoseUser) && ! empty($invitedNoseUser) && $currentUser->getId() == $invitedNoseUser['magento_id'])
			{
				// Relation
				$relation = $this->_getContactModel()->getRelation($senderNoseUser, $invitedNoseUser);

				// Set relation to "Friend" if the invited user / sender has already one lover relation
				if ( 
					$this->_getContactModel()->isLoveRelationId($relation['id_relation_type'])
					&&
					(
						$this->_getContactModel()->hasAlreadyLover($invitedNoseUser, $senderNoseUser) OR
						$this->_getContactModel()->hasAlreadyLover($senderNoseUser, $invitedNoseUser)
					)
				)
				{
					$this->_getContactModel()->setRelationType($invitedNoseUser, $senderNoseUser, self::DEFAULT_RELATION_TYPE_ID);
				}
			
				$result = $this->_getContactModel()->validateRelation($senderNoseUser, $invitedNoseUser, $hash);
				
				if ($result)
				{
					// Fire Event
					Mage::dispatchEvent("contact_invitation_accepted_email", array(
						'sender' => $senderNoseUser,
						'invited' => $invitedNoseUser,
						'origin' => 'email'
					));

					// If needed, call the Love Machine Diagnostic Creation service
					if ($this->_getContactModel()->isLoveRelationId($relation['id_relation_type']) )
						Mage::helper('mynose/diagnostic')->callLoverDiagnosticSave();

					// Send Email
					// Mage::helper('mynose/email')->sendInvitationAcceptedMail($senderNoseUser, $invitedNoseUser);
					
					$session->addSuccess($this->__("You're now linked with ") . $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname']);
				}
				else
				{
					$session->addError($this->__('You cannot be linked to this user'));	
				}
			}
			else
			{
				$session->addError($this->__('You cannot be linked to this user or the link is invalid'));	
			}
		}
		else
		{
			$session->addError($this->__('Already used or invalid link'));	
		}
		
		// Clean Invitation session
		$this->_getContactModel()->cleanInvitationSession();
		
		// Redirect to book
		$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}
	
	
	/**
	 *
	 * Refuse one invitation sent by Mail
	 *
	 */
	public function rejectEmailInvitationAction()
	{
		$session = Mage::getSingleton('core/session');

		// Sender ID
		$senderId = Mage::app()->getRequest()->getParam('s');
		
		// Invited ID : It's me ! 
		$invitedId = Mage::app()->getRequest()->getParam('i');
		
		// Hash : to check
		$hash = Mage::app()->getRequest()->getParam('h');
		
		if ( ! empty($senderId) && ! empty($invitedId) && ! empty($hash) )
		{
			// User's Nose infos
			$senderNoseUser = $this->_getContactNoseUser($senderId);
			$invitedNoseUser = $this->_getContactNoseUser($invitedId);
			
			if ( ! empty($senderNoseUser) && ! empty($invitedNoseUser))
			{
				$result = $this->_getContactModel()->removeRelation($senderNoseUser, $invitedNoseUser, $hash, 'refuse');

				if ($result)
				{
					// Fire Event
					Mage::dispatchEvent("contact_invitation_rejected", array(
						'sender' => $senderNoseUser,
						'invited' => $invitedNoseUser,
						'origin' => 'email'
					));

					// Send Email
					// Mage::helper('mynose/email')->sendInvitationRefusedMail($senderNoseUser, $invitedNoseUser);
					
					$session->addSuccess($this->__("You refused the invitation from ") . $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname']);
				}
				else
				{
					$session->addError($this->__('You cannot be linked to this user'));	
				}
			}
			else
			{
				$session->addError($this->__('You cannot be linked to this user or the link is invalid'));	
			}
		}
		else
		{
			$session->addError($this->__('Already used or invalid link'));	
		}

		// Clean Invitation session
		$this->_getContactModel()->cleanInvitationSession();
		
		// Redirect to book
		$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}
	
	
	

	
	/**
	 * Change One link between the current user and one contact
	 * 
	 */
	public function changelinkPostAction()
	{
		$session = Mage::getSingleton('core/session');

		// Current Nose user's infos
		$currentNoseUser = $this->_getCurrentNoseUser();
		$invitedNoseUser = $this->_getContactNoseUser($this->getRequest()->getPost('id'));
		$id_relation_type = $this->getRequest()->getPost('id_relation_type');
		
		// Check the relation : Only one "lover" relation possible
		if ( 
			$this->_getContactModel()->isLoveRelationId($id_relation_type)
			&&
			$this->_getContactModel()->hasAlreadyLover($currentNoseUser, $invitedNoseUser)
		)
		{
			$relationType = $this->_getContactModel()->getRelationType($id_relation_type);

			$session->addError($this->__('You cannot have 2 relations of this type'));
		}
		else
		{
			// Check the old relation : If "lover", remove the lover's diag
			$old_relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);

			if ($old_relation)
			{
				$was_love_relation = $this->_getContactModel()->isLoveRelationId($old_relation['id_relation_type']);
				
				if ($was_love_relation && $old_relation['status'] == self::RELATION_STATUS_ACCEPTED)
				{
					Mage::helper('mynose/diagnostic')->callLoverDiagnosticErase();
				}
			}
			
			// Change the relation type
			$result = $this->_getContactModel()->changeRelationLink($currentNoseUser, $invitedNoseUser, $id_relation_type);
			
			// Get again the new relation
			$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);

			// If needed, call the Love Machine Diagnostic Creation service
			if ($this->_getContactModel()->isLoveRelationId($id_relation_type) && $relation['status'] == self::RELATION_STATUS_ACCEPTED)
				Mage::helper('mynose/diagnostic')->callLoverDiagnosticSave();

			if ($result)
				$session->addSuccess($this->__("Relation type updated"));
			else		
				$session->addError($this->__('One problem happens during connexion with this user'));	
		}
		// Redirect to book
		$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactUrl());
	}
	
	
	
	/**
	 * Returns current Nose User
	 *
	 *
	 */
	private function _getCurrentNoseUser()
	{
		$contactModel = Mage::getModel('mynose/contact');

		// Current Magento user
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = $contactModel->getUserFromEmail($currentUser->getEmail());
		
		return $currentNoseUser;
	}
	
	
	/**
	 * Returns one contact from his ID
	 *
	 */
	private function _getContactNoseUser($id)
	{
		return $this->_getContactModel()->getUserFromId($id);
	}
	
	
	private function _getContactModel()
	{
		if ( is_null($this->_contactModel))
			$this->_contactModel = Mage::getModel('mynose/contact');
		
		return $this->_contactModel;
	}


	protected function _renderContactLayout()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}
}