<?php

class Partikule_Mynose_ResultController extends Partikule_Mynose_Controller_Abstract
{

    public function preDispatch()
    {
		parent::preDispatch();

		$action = $this->getRequest()->getActionName();

		if (!$this->getRequest()->isDispatched()) {
			return;
		}

		// Pattern set in parent : Partikule_Mynose_Controller_Abstract
		$pattern = self::DISPATCH_PATTERN;

		// If not one one this pattern, and user not logged in
        if (!preg_match($pattern, $action))
        {
			if ( ! $this->_getSession()->authenticate($this))
			{
				// If Diag comes from Home page : Redirect to Create an account
				/*
				 * TODO : Redirect to Create one account if Session data
				 */
				if ($action == 'index')
				{
					if (Mage::helper('mynose/diagnostic')->hasInputsInSession() == TRUE)
					{
						// Redirect to Create Account
						$this->getResponse()->setRedirect(
							Mage::getUrl('mynose/diagnose/create')
						);
					}
				}
				$this->setFlag('', 'no-dispatch', true);
			}
		}
		else
		{
			$this->_getSession()->setNoReferer(true);
		}
	}



	/**
	 * Generate the result
	 * Get the Diagnose form data and process them.
	 *
	 * If you ask you where is done the display
	 * Have a look at :
	 * - layout/mynose.xml
	 * - template/mynose/diagnose/xhr_result.phtml
	 * - template/mynose/diagnose/diagnostic.phtml
	 * - Mynose/Block/Diagnostic.php
	 *
	 */
	public function indexAction()
	{
		// Get the selection from form
		$post = $this->getRequest()->getPost();

		// If the user has filled out the Home diagnostic form : Data in session
		if (Mage::helper('mynose/diagnostic')->hasInputsInSession() == TRUE)
		{
			$inputs = Mage::helper('mynose/diagnostic')->getFormSelectionFromSession();

			if ( ! is_null($inputs))
			{
				$post = $inputs;

				// Clear the session
				Mage::helper('mynose/diagnostic')->clearSession();
			}
		}

		Mage::helper('mynose/diagnostic')->callDiagnosticResults($post);
		
		// Redirect to the diagnose result
		$this->getResponse()
			->setRedirect(Mage::helper('ionize/core')->getBaseUrl(TRUE) . 'mynose/result/mydiagnose');

		/*
		 * This was by Ajax
		 * Now the controller simply redirect
		 *
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
		
		// Module URL : For template JS
		// $this->getLayout()->getBlock('root')->assign('module_url', Mage::helper('mynose/url')->getModuleUrl());
			
		$this->renderLayout();
		*/
	}
	
	
	/**
	 * Displays the current user's Diag
	 * Used :
	 * 	- After the Diag
	 *	- To display the current diag through the menu
	 *
	 */
	public function mydiagnoseAction()
	{
		// Set the Authentication back URL
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());

		$this->loadLayout();
		
		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}
	
	
	/**
	 * Displays the current user's Diag Recommandation list
	 * Used :
	 * 	- After the Diag
	 *	- To display the current diag through the menu
	 *
	 */
	public function myrecoAction()
	{
		// Set the Authentication back URL
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());

		$this->loadLayout();
		
		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}
	
}