<?php

class Partikule_Mynose_EvalController extends Partikule_Mynose_Controller_Abstract
{

	// Online
	const STORE_ID = 		1;
	const DISTRIBUTOR_ID = 	1;


	/**
	 * Displays the current Evaluation List
	 *
	 */
	public function indexAction()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
		
		$this->renderLayout();
	}
	
	
	/**
	 * Displays the favorites List
	 *
	 */
	public function favoriteAction()
	{
		$this->indexAction();
	}
	
	
	/**
	 * Evaluation : Ranks one perfume
	 * Called through XHR by the evaluation template
	 *
	 * @TODO : Add on 
	 *
	 */
	public function rankAction()
	{
		// Not logged in : Redirect to login form (simply by reloading the current page
		if( ! Mage::getModel('customer/session')->isLoggedIn() )
		{
			header("HTTP/1.0 401 Unauthorized");
    		die();
		}
		else
		{
	        // eNose Diagnose Result URL
			// http://enose.tld/diagnose/result
			$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'evaluate';

			// Add Current logged User data to post (email needed for the Diag)
			$user = Mage::getSingleton('customer/session')->getCustomer();

			/*
			 * output_id : Perfume ID
			 * evaluation : Rank given by the user
			 *
			 */
			$post = array(
//				'id_diag' => $this->getRequest()->getPost('dig'),
				'output_id' => $this->getRequest()->getPost('pid'),
				'evaluation' => $this->getRequest()->getPost('rank'),
				'magento_id' => $user->getId()
			);
			
			// CURL request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
		}
	}
	
	
	/**
	 * Saves the Evaluations
	 *
	 * Calls the user diag (this action looks for unsaved evals and saves them if mandatory)
	 *
	 * This is the same than Partikule_Mynose_Block_Diagnostic->getDiagnostic(), 
	 * except that it redirects to the evaluations
	 * 
	 *
	 */
	public function saveAction()
	{
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';
		$need_save_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'need_evaluations_save';

		$user = Mage::getSingleton('customer/session')->getCustomer();
		$session = Mage::getSingleton('core/session');

		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId(),
				'distributor_id' => self::DISTRIBUTOR_ID,
				'store_id' => self::STORE_ID
			);

			$json = Mage::helper('mynose/Request')->curl($need_save_url, $post);
			$result = json_decode($json);
			
			if ($result)
			{
				// Mage::log($result, null, 'partikule.log');
			
				// Request to eNose
				$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
				$result = json_decode($json);
				
				// Register the diag : Needed for Block : 'mynose/email_items'
				Mage::register('diag', $result);
	
				/*
				// Mail the Diag if it wasn't emailed before
				// Done by LifeCycle
				if ( ! empty($result->diag_perfume) OR ! empty($result->diag_home))
				{
					if ($result->diag_perfume->emailed == 0 OR $result->diag_home->emailed == 0)
					{
						$resultBlock = $this->getLayout()->createBlock('mynose/email_items')->toHtml();
						
						if ($resultBlock != '')
						{
							// Send the Diagnostic Result mail
							Mage::helper('mynose/Email')->sendDiagnosticResult($user, $resultBlock);
							
							// Set 'emailed' to 1 to this diag
							Mage::getModel('mynose/diagnostic')->setEmailed($result->diag_perfume->id_diag, 1);
							Mage::getModel('mynose/diagnostic')->setEmailed($result->diag_home->id_diag, 1);
						}
					}
				}
				*/
				$session->addSuccess($this->__("Your evaluations have been successfully saved."));
			}
		}
		
		$this->getResponse()
			->setRedirect(Mage::helper('mynose/url')->getEvalUrl());
	}
	
	
	
}