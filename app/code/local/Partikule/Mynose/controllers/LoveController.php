<?php

class Partikule_Mynose_LoveController extends Partikule_Mynose_Controller_Abstract
{
	const RELATION_STATUS_INVITED = 1;
	const RELATION_STATUS_ACCEPTED = 2;
	const RELATION_STATUS_REFUSED = 3;
	const RELATION_STATUS_DELETED = 4;
	
	const ORIGIN_ONLINE = 1;
	const ORIGIN_BOUTIQUE = 2;
	const ORIGIN_INVITATION = 3;
	const ORIGIN_PROSPECTION = 4;

	protected $_contactModel = NULL;

	/**
	 * 
	 *
	 *
	 */
	public function indexAction()
	{
		// Set the Authentication back URL
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());

		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
		
		$this->renderLayout();
	}


	/**
	 * Displays the current user's LM Diag
	 *
	 */
	public function mydiagnosticAction()
	{
		// Get the lover's
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$currentNoseUser = Mage::getModel('mynose/contact')->getUserFromEmail($user->getEmail());
		$lover = Mage::getModel('mynose/contact')->getLover($currentNoseUser);
		
		/*
		 * The user can have one lover but the corresponding diag can comes from one "old" lover.
		 * In this case, the check of the diag will be done by the template, wehn the diag was received
		 * from the eNose service
		 *
		 */
		if ($lover)
		{
			// Register lover for the Diagnostic Block
			Mage::register('lover', $lover);
			Mage::register('title', $this->__('My Love Machine diagnostic with ') . $lover['firstname'] . ' ' . $lover['lastname']);

			$this->loadLayout();
	
			// Set the current Ionize's page URL active
			$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
	
			$this->getLayout()->getBlock('head')->setTitle($this->__('My Love Machine diagnostic') );
		
			$this->renderLayout();
		}
		else
		{
			$session = Mage::getSingleton('core/session');
			$session->addError($this->__("You don't have any lover : Please first invite one lover."));	

			// Redirect to Love Machine : Invite
			$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
		}
	}


	/**
	 * Displays the current user lover's LM Diag
	 *
	 */
	public function loverDiagnosticAction()
	{
		// Get the lover's
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$currentNoseUser = Mage::getModel('mynose/contact')->getUserFromEmail($user->getEmail());
		$lover = Mage::getModel('mynose/contact')->getLover($currentNoseUser);

		if ($lover)
		{
			// Register lover for the Diagnostic Block
			Mage::register('lover', $lover);
			Mage::register('title', $this->__("Love Machine Diagnostic from ") . ' ' . $lover['firstname'] . ' ' . $lover['lastname']);

			$this->loadLayout();
	
			// Set the current Ionize's page URL active
			$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
	
			$this->getLayout()->getBlock('head')->setTitle($this->__("Love Machine Diagnostic from ") . ' ' . $lover['firstname'] . ' ' . $lover['lastname']);
	
			$this->renderLayout();
		}
		else
		{
			$session = Mage::getSingleton('core/session');
			$session->addError($this->__("You don't have any lover : Please first invite one lover."));	

			// Redirect to Love Machine : Invite
			$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
		}
	}


	/**
	 *
	 * Displays the Invitation form
	 *
	 *
	 */
	public function inviteAction()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}


	/**
	 *
	 * Sends the surprise
	 * Validates the surprise form
	 *
	 * @see Partikule_Mynose_ContactController->createAction for comments
	 *
	 */
	public function sendAction()
	{
		$post = $this->getRequest()->getPost();
		$session = Mage::getSingleton('core/session');
		Mage::getSingleton('customer/session')->setCustomerFormData($post);
		
		// Potential existing user : From eNose DB
		$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));
		
		// Current Nose user's infos
		$currentNoseUser = $this->_getCurrentNoseUser();

		// Same Email : Case of one stupid user	
		if ( ! empty($invitedNoseUser) && $invitedNoseUser['email'] == $currentNoseUser['email'])
		{
			$session->addError($this->__('You cannot invite yourself'));
			
			// Form populating
			$session->setSurpriseFormData($this->getRequest()->getPost());
			
			// Redirect to Love Machin
			$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
		}
		else
		{
			// Try to get the relation
			$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);
			
			// No relation : Create
			if ( ! $relation)
			{
				// Create User if he doesn't exists
				if ( empty($invitedNoseUser) OR ! $invitedNoseUser['id_user'])
				{
					// User's origin
					$post['origin'] = self::ORIGIN_INVITATION;
				
					$invitedNoseUser = $this->_getContactModel()->createUser($post);
					
					// Get again to feed the user ID
					$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));
				}

				// User exists : Create relation
				if ( ! empty($invitedNoseUser))
				{
					// Wait! :
					// First check if the invited user hasn't already a lover.
					// It is very bad to steal the lover from one another... pas beau, bouh !
					if ( $this->_getContactModel()->hasAlreadyLover($invitedNoseUser, $currentNoseUser))
					{
						$session->addError($invitedNoseUser['firstname'] . $this->__(" has already one lover. You cannot invite him / her as lover."));	
						
						// Redirect to Love Machine Invitation form
						$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
						
					}
					else
					{
						$result = $this->_getContactModel()->createRelation($currentNoseUser, $invitedNoseUser, $this->getRequest()->getPost('link'));
						
						if ($result)
						{
							$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);
							
							// Set the Hash to the curent user
							$currentNoseUser['hash'] = $relation['hash'];
							
							// Send Invitation Email
							Mage::helper('mynose/email')->sendLoveMachineInvitationMail($currentNoseUser, $invitedNoseUser, $post);
	
							$session->addSuccess($this->__('One invitation has been sent to ') . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
							
							// Redirect to book
							$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactBookUrl());
						}
						else
						{
							$session->addSuccess($this->__('One problem happens during connexion with this user'));				
	
							// Redirect to Add
							$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
						}
					}
				}
			}
			else
			{
				if ($relation['status'] == self::RELATION_STATUS_INVITED)
					$session->addSuccess($this->__("You already invited ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
				else
					$session->addSuccess($this->__("You're already friend with ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
				
				// Redirect to Love Machine Invitation form
				$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getLoveInvitationUrl());
			}
		}
	}


	private function _getContactModel()
	{
		if ( is_null($this->_contactModel))
			$this->_contactModel = Mage::getModel('mynose/contact');
		
		return $this->_contactModel;
	}

	/**
	 * Returns current Nose User
	 *
	 *
	 */
	private function _getCurrentNoseUser()
	{
		$contactModel = Mage::getModel('mynose/contact');

		// Current Magento user
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = $contactModel->getUserFromEmail($currentUser->getEmail());
		
		return $currentNoseUser;
	}
	
	
	/**
	 * Returns one contact from his ID
	 *
	 */
	private function _getContactNoseUser($id)
	{
		return $this->_getContactModel()->getUserFromId($id);
	}




}