<?php
/**
 * Class Partikule_Mynose_SponsorController
 *
 * Renders the Sponsor screen in MyNose.
 * Sponsor actions are handled by : /app/code/local/Partikule/Sponsor/controllers/SponsorController.php
 *
 */
class Partikule_Mynose_SponsorController extends Partikule_Mynose_Controller_Abstract
{
	public function creditAction()
	{
		$this->_renderContactLayout();
	}

	protected function _renderContactLayout()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}

}