<?php


class Partikule_Mynose_Block_Calendar extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	
	/**
	 * Returns next user's calendars events
	 *
	 */
	public function getCalendarEvents()
	{
		$calendar = Mage::getModel('mynose/calendar')->getNextCalendarEvents();

		return $calendar;
	}
	
	/**
	 *
	 */
	public function getContacts()
	{
		$contacts = Mage::getModel('mynose/contact')->getContacts();

		return $contacts;
	}



}