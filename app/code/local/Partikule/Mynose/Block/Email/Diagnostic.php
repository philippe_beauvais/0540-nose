<?php
/**
 * Partikule
 * 23.11.2012
 *
 * Copied from Items.php bu slightly modified :
 * - Does not get Diag from the current user but from any user
 * - Can get the diag / user
 * 		- From Registry
 * 		- From one passed order
 *
 */


/**
 * Diagnostic result items block
 *
 * @category   Partikule
 * @package    Partikule_MyNose
 * @author     Partikule
 *
 */
class Partikule_Mynose_Block_Email_Diagnostic extends  Mage_Catalog_Block_Product_Abstract
{

    /**
     * Product Items Collection
     *
     * @var Mage_Wishlist_Model_Mysql4_Item_Collection
     */
    protected $_perfume_collection = NULL;
    protected $_home_collection = NULL;

    protected $_template = 'mynose/diagnose/email/items.phtml';
    
    
	public function __construct()
    {
        parent::__construct();
    }
    
    
	/**
	 * $diag && $user must be set in the Registry
	 *
	 * @return Mage_Wishlist_Model_Mysql4_Item_Collection|null
	 *
	 */
	public function getPerfumeCollection()
    {
    	if ( is_null($this->_perfume_collection))
    	{
	    	$diag = Mage::registry('diag');
	    	$user = Mage::registry('user');

			// Case of internal Diag from order
			if (!$diag && !$user)
			{
				$order = $this->getData('order');
				$user = Mage::getModel('customer/customer')->load($order->getCustomerId());
				$diag = Mage::helper('mynose/diagnostic')->getCustomerDiagnostic($user);
			}

	    	if ($diag && $user)
	    	{
				$master_ids = $this->_getMasterIds($diag->perfumes);
				
				$this->_perfume_collection = $this->getDiagProductCollection($user, $master_ids);
	    	}
		}    	
    	return $this->_perfume_collection;
    }
	
	
    /**
     *
     */
    public function getHomeCollection()
    {
    	if ( is_null($this->_home_collection))
    	{
	    	$diag = Mage::registry('diag');
			$user = Mage::registry('user');

			// Case of internal Diag from order
			if (!$diag && !$user)
			{
				$order = $this->getData('order');
				$user = Mage::getModel('customer/customer')->load($order->getCustomerId());
				$diag = Mage::helper('mynose/diagnostic')->getCustomerDiagnostic($user);
			}

			if ($diag && $user)
	    	{
				$master_ids = $this->_getMasterIds($diag->homefragrances);
				
				$this->_home_collection = $this->getDiagProductCollection($user, $master_ids);
	    	}
		}    	
    	return $this->_home_collection;
    }


	/**
	 * @param $user
	 * @param $master_ids
	 *
	 * @return mixed
	 */
	public function getDiagProductCollection($user, $master_ids)
    {
		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('catalog/product')->getCollection()
			->setStoreId( $storeId )
			->addAttributeToSelect("*")
			->addAttributeToFilter('master_id', array('in' =>  $master_ids))
			->addAttributeToFilter('type_id', 'configurable')
		;
		
		// ORDER BY FIELD : Keep the Diag order
		$collection->getSelect()->order(new Zend_Db_Expr("FIELD(master_id, '" . implode("','", $master_ids) . "')"));
		
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

		return $collection;
    }


	/**
	 * @param $items
	 *
	 * @return array
	 */
	protected function _getMasterIds($items)
    {
    	$master_ids = array();
    	
		// get master products from diagnostic results
		if ( ! empty($items))
		{
			foreach ($items as $item)
				$master_ids[] = $item->master_id;
		}
		
		return $master_ids;
    }
}