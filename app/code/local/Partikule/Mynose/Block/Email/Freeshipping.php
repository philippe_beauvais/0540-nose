<?php
/**
 * Partikule
 * 26.11.2012
 *
 */


/**
 * Free Shipping block
 *
 * @category   Partikule
 * @package    Partikule_MyNose
 * @author     Partikule
 *
 */
class Partikule_Mynose_Block_Email_Freeshipping extends  Mage_Core_Block_Template
{
	protected $_template = 'mynose/diagnose/email/free_shipping.phtml';
}
