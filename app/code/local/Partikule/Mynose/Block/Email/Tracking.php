<?php
	/**
	 * Partikule
	 * 23.11.2012
	 *
	 * Copied from Items.php bu slightly modified :
	 * - Does not get Diag from the current user but from any user
	 *
	 *
	 */


	/**
	 * Diagnostic result items block
	 *
	 * @category   Partikule
	 * @package    Partikule_MyNose
	 * @author     Partikule
	 *
	 */
class Partikule_Mynose_Block_Email_Tracking extends Mage_Core_Block_Template
{
//	protected $_template = 'mynose/diagnose/email/tracking.phtml';


	public function __construct()
	{
		parent::__construct();
		$this->setData('area','frontend');
		$this->setTemplate('mynose/diagnose/email/tracking.phtml');
	}


	public function getOrderTrackingUrl()
	{
		$order = $this->getData('order');

		$url = Mage::helper('nose/shipping')->getOrderTrackingUrl($order);

		return $url;
	}


}
