<?php
/**
 * Partikule
 *
 */


/**
 * Diagnostic result items block
 *
 * @category   Partikule
 * @package    Partikule_MyNose
 * @author     Partikule
 *
 */
class Partikule_Mynose_Block_Email_Items extends  Mage_Catalog_Block_Product_Abstract // Mage_Core_Block_Template //
{

    /**
     * Product Items Collection
     *
     * @var Mage_Wishlist_Model_Mysql4_Item_Collection
     */
    protected $_perfume_collection = NULL;
    protected $_home_collection = NULL;

    protected $_template = 'mynose/diagnose/email/items.phtml';
    
    
	public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     *
     */
    public function getPerfumeCollection()
    {
    	if ( is_null($this->_perfume_collection))
    	{
	    	$diag = Mage::registry('diag');

	    	if ($diag)
	    	{
				$user = Mage::getSingleton('customer/session')->getCustomer();
				
				$master_ids = $this->_getMasterIds($diag->perfumes);
				
				$this->_perfume_collection = $this->getDiagProductCollection($user, $master_ids);
	    	}
		}    	
    	return $this->_perfume_collection;
    }
	
	
    /**
     *
     */
    public function getHomeCollection()
    {
    	if ( is_null($this->_home_collection))
    	{
	    	$diag = Mage::registry('diag');

	    	if ($diag)
	    	{
				$user = Mage::getSingleton('customer/session')->getCustomer();
				
				$master_ids = $this->_getMasterIds($diag->homefragrances);
				
				$this->_home_collection = $this->getDiagProductCollection($user, $master_ids);
	    	}
		}    	
    	return $this->_home_collection;
    }
	
	
    /**
     *
     */
    public function getDiagProductCollection($user, $master_ids)
    {
		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('catalog/product')->getCollection()
			->setStoreId( $storeId )
			->addAttributeToSelect("*")
			->addAttributeToFilter('master_id', array('in' =>  $master_ids))
			->addAttributeToFilter('type_id', 'configurable')
		;
		
		// ORDER BY FIELD : Keep the Diag order
		$collection->getSelect()->order(new Zend_Db_Expr("FIELD(master_id, '" . implode("','", $master_ids) . "')"));
		
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

		return $collection;
    }
    
    
	/**
     *
     */
    protected function _getMasterIds($items)
    {
    	$master_ids = array();
    	
		// get master products from diagnostic results
		if ( ! empty($items))
		{
			foreach ($items as $item)
				$master_ids[] = $item->master_id;
		}
		
		return $master_ids;

    }
}