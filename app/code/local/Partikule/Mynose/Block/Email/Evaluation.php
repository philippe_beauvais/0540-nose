<?php
/**
 * Partikule
 * 26.11.2012
 *
 */


/**
 * Evaluation block
 *
 * @category   Partikule
 * @package    Partikule_MyNose
 * @author     Partikule
 *
 */
class Partikule_Mynose_Block_Email_Evaluation extends  Mage_Core_Block_Template
{
	protected $_template = 'mynose/diagnose/email/evaluation.phtml';
}
