<?php
	/**
	 * Partikule
	 * 23.11.2012
	 *
	 * Copied from Items.php bu slightly modified :
	 * - Does not get Diag from the current user but from any user
	 *
	 *
	 */


	/**
	 * Diagnostic result items block
	 *
	 * @category   Partikule
	 * @package    Partikule_MyNose
	 * @author     Partikule
	 *
	 */
class Partikule_Mynose_Block_Email_Coupon extends Mage_Core_Block_Template
{
	protected $_template = 'mynose/diagnose/email/coupon.phtml';


	public function __construct()
	{
		parent::__construct();
	}


	public function getCouponCode()
	{
		$coupon_code = NULL;
		$order = $this->getData('order');

		if ($order)
		{
			$orderId = $order->getId();

			$coupon_code = Mage::helper('nose/coupon')->getCouponCodeFromOrderId($orderId);
		}

		return $coupon_code;
	}
}
