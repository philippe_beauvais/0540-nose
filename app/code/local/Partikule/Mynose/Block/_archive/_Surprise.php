<?php


class Partikule_Mynose_Block_Surprise extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	
	/**
	 * Returns the Diagnostic Form data
	 * 
	 * @TODO : Send the user Info and return existing Diag data to feed the form
	 *
	 */
	public function getInvitationForm()
	{
	}
	
	
	/**
	 * Returns the current user's Diagnostic results (Recommandations)
	 *
	 *
	 */
	public function getDiagnostic()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';

		$user = Mage::getSingleton('customer/session')->getCustomer();
		
		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId()
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);
			
			// Returns the Diag only if perfumes or Homefragrances
			if ( ! empty($result->perfumes) OR ! empty($result->homefragrances))
				return $result;
		}
		
		return FALSE;
	}
	
	
	/**
	 * Returns the current user's Inputs (last diag he done)
	 *
	 * @param	int		Model ID.
	 *					1: Perfume diag (classic one)
	 *					2: Home fragrance
	 *					3: Love Machine
	 *
	 */
	public function getInputs($model_id = 1)
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'inputs';

		$user = Mage::getSingleton('customer/session')->getCustomer();
		$store_code = Mage::app()->getStore()->getCode();
		
		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId(),
				'model_id' => $model_id,
				'lang_code' => $store_code
			);

			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);
			// Mage::log($result, null, 'partikule.log');
						
			if ( ! empty($result->perfumes) OR ! empty($result->season_families))
				return $result;
		}
		
		return FALSE;
	}
	
	
	
	/**
	 * Returns the current user's evaluations
	 *
	 * @return	Array	Array of Evaluations
	 *
	 */
	public function getEvaluationList($status = FALSE, $limit = FALSE)
	{
		// Returned results
		$result = array();
		
        // eNose Diagnose Result URL
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'evaluations';
		
		// Add Current logged User data to post (email needed for the Diag)
		$user = Mage::getSingleton('customer/session')->getCustomer();

		$post = array(
			'magento_id' => $user->getId(),
			'limit' => $limit,
			'status' => $status
		);

		// CURL request to eNose
		$json = Mage::helper('mynose/Request')->curl($enose_url, $post);

		// Pure assoc. array from json
		$result = json_decode($json);
		
/*
		if ($status != FALSE)
		{
			foreach($jsonResult as $eval)
			{
				if ($eval['status'] == $status)
					$result
			}
			$w_result = $result;
		}
*/

		return $result;
	}
	
	
	/**
	 * Returns a product Collection from one perfume collection
	 *
	 * @param 	Collection 				Perfumes, Homefragrances, Bears...
	 *
	 * @return 	Product Collection		Corresponding Samples
	 *
	 *
	 */
	public function getSamplesFromDiagnosticItems($items)
	{
		if ($items)
		{
			$items_ids = array();
			
			foreach($items as $item)
				$master_ids[] = $item->master_id;

			$products = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('*')
				->addAttributeToFilter('master_id', array('in' => $master_ids))
				->addAttributeToFilter('sku', array('like' => 'S-%'))
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				->addFinalPrice()
			;
			
			// Order as perfume list
//			$products->getSelect()->order(new Zend_Db_Expr("FIELD(e.sku, '" . implode("','", $sample_ids) . "')"));
//			$products->getSelect()->order(new Zend_Db_Expr("FIELD(perfume_id, '" . implode("','", $parfumes_ids) . "')"));
			$products->getSelect()->order(new Zend_Db_Expr("FIELD(master_id, '" . implode("','", $master_ids) . "')"));
			
			// Add Master ID to each product : Needed in template to make a ref.
			foreach($products as $product)
				foreach($items as $item)
					if (substr($product->getSku(),2) == $item->master_id)
						$product->setMasterId($item->master_id);

			return $products;
		}
		return FALSE;	
	}
	
	
	/**
	 * Returns a product Collection from one perfume collection
	 *
	 * @param 	Collection 				Perfumes
	 *
	 * @return 	Product Collection		Corresponding Master Products
	 *
	 *
	 */
	public function getProductsFromDiagnosticItems($items)
	{
		if ($items)
		{
			// Mage::log($items, null,  'partikule.log');

			$sample_ids = array();
			
			foreach($items as $item)
				$sample_ids[] = 'M-' . $item->master_id;
			
			$products = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToFilter('sku', array('in' => $sample_ids))
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				->addFinalPrice()	
			;
			
			$products->getSelect()->order(new Zend_Db_Expr("FIELD(e.sku, '" . implode("','", $sample_ids) . "')"));
			
			// Add Perfume ID to each product : Needed in template to make a ref.
			foreach($products as $product)
				foreach($items as $item)
					if (substr($product->getSku(),2) == $item->master_id)
						$product->setMasterId($item->master_id);
			
			return $products;
		}
		return FALSE;	
	}
	
	
	
	
	/**
	 * Returns Diagnostics List for the current user
	 *
	 *
	 */
	public function getDiagnosticHistory()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'history';
		
		$user = Mage::getSingleton('customer/session')->getCustomer();

		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId()
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			return $result;
		}
		
		return FALSE;
		
/*
		if ($product_id)
		{
			$productModel = Mage::getModel('catalog/product');
			$product = $productModel->load($product_id);
			
			return $product;
		}
		
		return FALSE;
*/
	}
	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getOrders()
	{
		$user = Mage::getSingleton('customer/session')->getCustomer();

		$orderCollection = Mage::getModel('sales/order')->getCollection()
							->addfilter('customer_id', $user->getId());

/* 		$orderCollection->getSelect()->where('e.customer_id = ?', $user->getId()); */
		
		return $orderCollection;
		
		
	}

	public function _toHtml()
	{
        $this->getLayout()->getBlock('head')->setTitle($this->__('Youpi'));

        return parent::_toHtml();
	
//		return 'Nose Diagnose';

	}


    public function getAddToCartUrl($product, $additional = array())
    {
	   if ($product->getTypeInstance(true)->hasRequiredOptions($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }
            if (!isset($additional['_query'])) {
                $additional['_query'] = array();
            }
            $additional['_query']['options'] = 'cart';

            return $this->getProductUrl($product, $additional);
        }
        return $this->helper('checkout/cart')->getAddUrl($product, $additional);

    }

    /**
     * Retrieve Product URL using UrlDataObject
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $additional the route params
     * @return string
     */
    public function getProductUrl($product, $additional = array())
    {
        if ($this->hasProductUrl($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }
            return $product->getUrlModel()->getUrl($product, $additional);
        }

        return '#';
    }


}