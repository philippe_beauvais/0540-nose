<?php


class Partikule_Mynose_Block_Contact extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Gets the FB token or NULL
     *
     * @return null|String
     *
     */
    public function getFacebookToken()
    {
        $info = Mage::getModel('inchoo_socialconnect/facebook_info_user');

        $info->load();

        try
        {
            $token = $info->getAccessToken();

            if (empty($token) || empty($token->access_token))
                return NULL;

            return $token->access_token;
        }
        catch(Exception $e)
        {
            // Not logged to Facebook
            return NULL;
        }
    }


    public function getFacebookLoginUrl()
    {
        $client = Mage::getSingleton('inchoo_socialconnect/facebook_oauth2_client');

        $userInfo = Mage::registry('inchoo_socialconnect_facebook_userinfo');

        if(is_null($userInfo) || ! $userInfo->hasData())
        {
            return $client->createAuthUrl();
        }

        return NULL;
    }


	/**
	 * Returns the Diagnostic page title
	 * Useful because different when the Diagnostic results comes from friends
	 *
	 *
	 */
	public function getDiagnosticTitle()
	{
		$title = '';
		
		// "contact" registered by : Partikule_Mynose_Block_Diagnostic->getContactDiagnostic()
		$contact = Mage::registry('contact');
		
		if ($contact)
			$title =  $this->__('Diagnostic from ') . $contact['firstname'] . ' ' . $contact['lastname'];
		
		return($title);
	}
	
	/**
	 * returns TRUE if the current user has already one lover
	 * Lazely based onn the relation type ID, but...
	 * ... when you got delays....
	 *
	 */
	public function hasAlreadyLover()
	{
		// Current Magento user
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = Mage::getModel('mynose/contact')->getUserFromEmail($currentUser->getEmail());
		
		$has = Mage::getModel('mynose/contact')->hasAlreadyLover($currentNoseUser);
		
		return $has;
	}
	
	
	/**
	 *
	 */
	public function getRelationTypes()
	{
		// Used to return only allowed relation types
		$currentNoseUser = $this->getCurrentNoseUser();
	
		$relationTypes = Mage::getSingleton('mynose/contact')->getRelationTypes($currentNoseUser);

		return $relationTypes;
	}
	
	
	/**
	 *
	 */
	public function getAllRelationTypes()
	{
		$relationTypes = Mage::getSingleton('mynose/contact')->getRelationTypes();

		return $relationTypes;
	}
	
	
	/**
	 * Returns next user's calendars events
	 *
	 */
	public function getCalendarEvents()
	{
		$calendar = Mage::getModel('mynose/calendar')->getNextCalendarEvents();

		return $calendar;
	}
	
	
	public function getContacts()
	{
		$contacts = Mage::getModel('mynose/contact')->getContacts();

		return $contacts;
	}
	
	
	public function _toHtml()
	{
        $this->getLayout()->getBlock('head')->setTitle($this->__('Youpi'));

        return parent::_toHtml();
	}


    /**
     * Retrieve form data
     *
     * @return Varien_Object
     *
     */
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if (is_null($data)) {
            $formData = Mage::getSingleton('customer/session')->getCustomerFormData(true);
            $data = new Varien_Object();
            if ($formData) {
                $data->addData($formData);
                $data->setCustomerData(1);
            }

            $this->setData('form_data', $data);
        }
        return $data;
    }

	/**
	 * Returns current Nose User
	 *
	 *
	 */
	public function getCurrentNoseUser()
	{
		$contactModel = Mage::getModel('mynose/contact');

		// Current Magento user
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = $contactModel->getUserFromEmail($currentUser->getEmail());
		
		return $currentNoseUser;
	}


	public function getNbCredits()
    {
        return Mage::helper('sponsor/credit')->getCredits();
    }


    public function getCouponAmount()
    {
        $amount = 0;

        // Current Magento user
        $customer = Mage::getModel('customer/session')->getCustomer();

        $currentUser = Mage::getModel('mynose/user')->getNoseUserFromMagentoId($customer->getId());

        $sponsor_coupons = Mage::getModel('mynose/user')->getNoseUserCoupons($currentUser['id_user']);

        foreach($sponsor_coupons as $_coupon)
        {
            $coupon = Mage::getModel('salesrule/coupon')->load($_coupon['coupon'], 'code');

            if ($coupon->getTimesUsed() == 0)
            {
                // $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());

                $amount += $_coupon['amount'];
            }
        }

        return $amount;
    }
}