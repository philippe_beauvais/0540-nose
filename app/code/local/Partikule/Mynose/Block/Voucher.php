<?php


class Partikule_Mynose_Block_Voucher extends Mage_Core_Block_Template
{
    protected $_voucher_reasons = array(
        'godson_order' => 'Godson Order',
        'godson_kit_order' => 'Godson Kit Order',
        'kit_order' => 'Diagnosis Kit Order',
    );


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }


    /**
     * Gets coupons saved in eNose
     * Check them against usage and returns only unused one.
     *
     * @return array
     */
    public function getVouchers()
    {
        $coupons = array();

        $customer = Mage::getModel('customer/session')->getCustomer();

        $_coupons = Mage::getModel('mynose/user')->getNoseUserCouponsFromCustomer($customer);

        $_codes = array();

        foreach($_coupons as $_coupon)
        {
            $_codes[] = $_coupon['coupon'];
        }

        $collection = Mage::getModel('salesrule/coupon')->getCollection();
        $collection->addFieldToFilter('code', array('in' => $_codes));

        foreach($collection as $coupon)
        {
            if ($coupon->getId() && $coupon->getTimesUsed() < 1)
            {
                foreach($_coupons as $_coupon)
                {
                    if ($_coupon['coupon'] == $coupon->getCode())
                        $coupons[] = $_coupon;
                }
            }
        }

        return $coupons;
    }


    public function getValue($voucher)
    {
        if ( ! empty($voucher['amount']))
            return $voucher['amount'];
        else
        {
            $amount = 0;

            $collection = Mage::getModel('salesrule/coupon')->getCollection();
            $collection->addFieldToFilter('code', array('in' => $voucher['coupon']));

            foreach($collection as $coupon)
            {
                if ($coupon->getId())
                {
                    $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());

                    $amount= $rule->getDiscountAmount();
                }
            }

            return $amount;
        }
    }


    public function getReason($voucher)
    {
        $reason = '';

        if ( ! empty($voucher['source']) && ! empty($this->_voucher_reasons[$voucher['source']]))
        {
            $reason = $this->__($this->_voucher_reasons[$voucher['source']]);
        }

        return $reason;
    }
}