<?php
/**
 * Partikule
 *
 * Extended to change the template
 *
 */

/**
 * Block to render customer's gender attribute
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Partikule_Mynose_Block_Widget_Gender extends Mage_Customer_Block_Widget_Gender
{
    /**
     * Initialize block
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('mynose/widget/gender.phtml');
    }

}
