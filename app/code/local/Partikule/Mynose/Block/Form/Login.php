<?php
/**
 * Partikule
 *
 * Extended because the very flexible Magento sets the title of the page in hard in the _prepareLayout() method.
 * Thank you alot Magento, I really love you !
 */

/**
 * Customer login form block
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Partikule_Mynose_Block_Form_Login  extends Mage_Customer_Block_Form_Login
{

    protected function _prepareLayout()
    {
        $return = parent::_prepareLayout();
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('MyNose Login'));
        return $return;
    }

}
