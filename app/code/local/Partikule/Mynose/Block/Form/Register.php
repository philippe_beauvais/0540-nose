<?php
/**
 * Partikule
 *
 * Extended because the very flexible Magento sets the title of the page in hard in the _prepareLayout() method.
 * Thank you alot Magento, I really love you !
 *
 */

/**
 * Customer register form block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Partikule_Mynose_Block_Form_Register extends Mage_Customer_Block_Form_Register
{

    protected function _prepareLayout()
    {
    	$return = parent::_prepareLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('MyNose Register'));
        return $return;
    }

}
