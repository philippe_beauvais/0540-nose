<?php


class Partikule_Mynose_Block_Surprise extends Partikule_Mynose_Block_Contact
{

	/**
	 * Returns the Diagnostic page title
	 * Useful because different when the Diagnostic results comes from friends
	 *
	 *
	 */
	public function getDiagnosticTitle()
	{
		$title = '';
		
		// "contact" registered by : Partikule_Mynose_Block_Diagnostic->getContactDiagnostic()
		$contact = Mage::registry('contact');
		
		if ($contact)
			$title =  $this->__('Diagnostic from ') . $contact['firstname'] . ' ' . $contact['lastname'];
		
		return($title);
	}
	

}