<?php


class Partikule_Mynose_Block_Love extends Partikule_Mynose_Block_Contact
{

	/**
	 * Returns the Diagnostic page title
	 * Useful because different when the Diagnostic results comes from friends
	 *
	 *
	 */
	public function getDiagnosticTitle()
	{
		$title = '';
		
		// "lover" registered by : Partikule_Mynose_LoveController->loverDiagnosticAction()
		$lover = Mage::registry('lover');
		
		if ($lover)
		{
			// Registered by LoveController->loverDiagnosticAction()
			// and LoveController->mydiagnosticAction()
			$title = Mage::registry('title');
			// $title =  $this->__('My Love Machine diagnostic with ') . $lover['firstname'] . ' ' . $lover['lastname'];
		}
		else
			$title =  $this->__('My Love Machine diagnostic');
		
		return($title);
	}
	
	
	public function getLoveRelationTypes()
	{
		$relationTypes = Mage::getSingleton('mynose/contact')->getLoveRelationTypes();

		return $relationTypes;
	}
	


	/**
	 * Returns the User's lover
	 */
	public function getLover()
	{
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$currentNoseUser = Mage::getModel('mynose/contact')->getUserFromEmail($user->getEmail());
		$lover = Mage::getModel('mynose/contact')->getLover($currentNoseUser);

		return $lover;
	}


}