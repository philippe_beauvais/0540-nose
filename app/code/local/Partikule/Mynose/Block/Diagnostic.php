<?php
/**
 * Diagnostic Block
 *
 * Do / Get the diagnostic
 *
 *
 */

class Partikule_Mynose_Block_Diagnostic extends Mage_Core_Block_Template
{
	/*
	 * Evaluations status
	 *
	 */
	const EVAL_STATUS_CLOSED = 		0;		// Evaluation not yet opened (no order on samples)
	const EVAL_STATUS_PENDING = 	1;		// Order on sample, but not confirmed
	const EVAL_STATUS_WAITING = 	2;		// Eval opened, waiting for user input
	const EVAL_STATUS_DONE = 		3;		// Eval done by user
	const EVAL_STATUS_CANCELED = 	4;		// Eval canceled, not used for the moment.
	
	/*
	 * Errors codes
	 *
	 */
	const ERROR_NO_MAGENTO_USER = 	1;		// Returned by eNose, not used here
	const ERROR_NO_USER_GENDER =	2;		// Returned by eNose, not used here
	const ERROR_DIAGNOSTIC =		3;		// Diagnostic URL not reachable

	/*
	 * Diagnostic models
	 * @see : IDs in eNose table "model"
	 *
	 */
	const MODEL_PERFUME = 	1;
	const MODEL_HOME = 		2;
	const MODEL_LOVE = 		3;
	
	// Online
	const STORE_ID = 		1;
	const DISTRIBUTOR_ID = 	1;

	protected $_diag = NULL;


	protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	
	/**
	 * Returns the Diagnostic Form data
	 * 
	 * @TODO : Send the user Info and return existing Diag data to feed the form
	 *
	 */
	public function getDiagnoseForm()
	{
		// eNose Diagnose controller's URL
		// http://enose.tld/diagnose
		$url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'formdata';

		// Get the user
		$user = Mage::getSingleton('customer/session')->getCustomer();

		if ($user)
		{
			$user->setEnoseStoreId(self::STORE_ID);
			$user->setEnoseDistributorId(self::DISTRIBUTOR_ID);

			$store_code = Mage::app()->getStore()->getCode();
			$post['magento_id'] = $user->getId();
	
			// Pseudo post data, send through POST to the Diagnose application, 
			// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
			// These data are send back in the JSON answer
			$post = array (
				'base_url' => Mage::getBaseUrl(),
				'module_url' => Mage::getBaseUrl(). 'mynose',
				'autocomplete_url' => Mage::getBaseUrl(). 'mynose/brand/autocomplete/',
				'perfume_select_url' => Mage::getBaseUrl(). 'mynose/perfume/perfumeselect/',
				'form_action_url' => Mage::getBaseUrl(). 'mynose/result/',
				'antispam_hash' =>	Mage::getStoreConfig('mynose/settings/antispam_hash'),
				'magento_id' => $user->getId(),
				'lang_code' => $store_code
			);

			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($url, $post);
			$result = json_decode($json);

			// Create one Error object
			if (empty($result))
			{
				$result = new stdClass();
				$result->error = self::ERROR_DIAGNOSTIC;
				$result->message = 'Diagnostic URL not reachable';
			}
			
			// Log the error
			if (isset($result->error))
			{
				// No user : Create one !
				if (isset($result->code) && $result->code == 1)
				{
					$eNoseUser = Mage::getModel('mynose/user')->getNoseUser($user);

					// Update the eNose User with Data (and custom attributes) from Nose users
					if ( ! empty($eNoseUser))
					{
						Mage::getModel('mynose/user')->updateNoseUser($user, $eNoseUser);
					}
					else
					{
						$user->setEnoseStoreId(self::STORE_ID);
						$user->setEnoseDistributorId(self::DISTRIBUTOR_ID);
						Mage::getModel('mynose/user')->createNoseUser($user);
					}

					Mage::app()->getResponse()->setRedirect(Mage::helper('mynose/url')->getDiagnoseUrl());
				}
			}
			
			// Returns all data from eNose ( + $post data, sent by eNose)
			return $result;
		}

		return FALSE;
	}


	/**
	 * Returns the current user's Diagnostic results (Recommandations)
	 *
	 *
	 */
	public function getDiagnostic()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';

		$customerSession = Mage::getSingleton('customer/session');
		
		if ($customerSession->isLoggedIn())
		{
            $user = Mage::getSingleton('customer/session')->getCustomer();

			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId(),
				'store_id' => self::STORE_ID,
				'distributor_id' => self::DISTRIBUTOR_ID,
				'lang_code' => Mage::app()->getStore()->getCode()
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			// Register the diag : Needed for Block : 'mynose/email_items'
			Mage::register('diag', $result);

			// Mail the Diag if it wasn't emailed before
			// Replaced by LifeCycle
			/*
			if ( ! empty($result->diag_perfume) OR ! empty($result->diag_home))
			{
				if ($result->diag_perfume->emailed == 0 OR $result->diag_home->emailed == 0)
				{
					$resultBlock = $this->getLayout()->createBlock('mynose/email_items')->toHtml();
					
					if ($resultBlock != '')
					{
						// Send the Diagnostic Result mail
						Mage::helper('mynose/Email')->sendDiagnosticResult($user, $resultBlock);

						// Set 'emailed' to 1 to this diag
						Mage::getModel('mynose/diagnostic')->setEmailed($result->diag_perfume->id_diag, 1);
						Mage::getModel('mynose/diagnostic')->setEmailed($result->diag_home->id_diag, 1);
					}
				}
			}
			*/

			// Returns the Diag only if perfumes or Homefragrances
			if ( ! empty($result->perfumes) OR ! empty($result->homefragrances))
				return $result;
		}
		
		return FALSE;
	}
	
	
	/**
	 * Returns the Diagnostic page title
	 * Useful because different when the Diagnostic results comes from friends
	 *
	 *
	 */
	public function getDiagnosticTitle($context = 'default')
	{
		$title = '';
		$contact = Mage::registry('contact');
		if ($contact)
			$context = 'contact';

		switch ($context)
		{
			case 'default':
				$title = $this->__('Your diagnostic');
				break;
			case 'contact' :
				$contact = Mage::registry('contact');
				$title =  $this->__('Diagnostic of ') . $contact['firstname'] . ' ' . $contact['lastname'];
				break;
		}
		return $title;
	}

	public function getRecoTitle($context = 'default')
	{
		$title = '';
		$contact = Mage::registry('contact');
		if ($contact)
			$context = 'contact';

		switch ($context)
		{
			case 'default':
				$title = $this->__('Nose recommandations');
				break;
			case 'contact' :
				$contact = Mage::registry('contact');
				$title =  $this->__('Recommandations for ') . $contact['firstname'] . ' ' . $contact['lastname'];
				break;
		}
		return $title;
	}


	/**
	 * Returns one contact's (friend)  Diagnostic results (Recommandations)
	 *
	 *
	 */
	public function getContactDiagnostic()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';
		
		// ID of the contact : Registered by ContactController
		$id_contact = Mage::registry('id_user');

		$contactNoseUser = Mage::getModel('mynose/contact')->getUserFromId($id_contact);
		
		if ($contactNoseUser)
		{
			// Register Contact, for sub-blocks usage
			Mage::register('contact', $contactNoseUser);

			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $contactNoseUser['magento_id'],
				'lang_code' => Mage::app()->getStore()->getCode()
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			// Returns the Diag only if perfumes or Homefragrances
			if ( ! empty($result->perfumes) OR ! empty($result->homefragrances))
				return $result;
		}
		
		return FALSE;
	}	


	/**
	 *
	 * Returns LM reco for current user
	 *
	 *
	 */
	public function getMyLoveMachineDiagnostic()
	{
		if (is_null($this->_diag))
		{
			// eNose Diagnose controller's URL :
			// http://enose.tld/diagnose/history
			$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';
			
			// Current user
			$user = Mage::getSingleton('customer/session')->getCustomer();
			
			// Lover : Registered by Partikule_Mynose_LoveController
			// $lover = Mage::registry('lover');
			if ($user)
			{
				// Current User : Needs to be posted to eNose
				$post = array (
					'magento_id' => $user->getId(),
					'model_id' => self::MODEL_LOVE,
					'lang_code' => Mage::app()->getStore()->getCode(),
					'distributor_id' => self::DISTRIBUTOR_ID,
					'store_id' => self::STORE_ID,
				);

				// Request to eNose
				$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
				
				$result = json_decode($json);

				// Returns the Diag only if perfumes
				// if ( ! empty($result->perfumes) )
					$this->_diag = $result;
			}
			
			$this->_diag = FALSE;
		}
		return $this->_diag;
	}
	
	
	/**
	 *
	 * Returns LM reco for current user's lover
	 *
	 *
	 */
	public function getLoverLoveMachineDiagnostic()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';
		
		$user = Mage::getSingleton('customer/session')->getCustomer();

		// Lover : Registered by Partikule_Mynose_LoveController
		$lover = Mage::registry('lover');

		if ( ! empty($lover))
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $lover['magento_id'],
				'model_id' => self::MODEL_LOVE,
				'lang_code' => Mage::app()->getStore()->getCode(),
				'distributor_id' => self::DISTRIBUTOR_ID,
				'store_id' => self::STORE_ID,
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			// Returns the Diag only if perfumes
			if ( ! empty($result->perfumes) )
				return $result;
		}
		
		return FALSE;
	}	
	
	
	/**
	 * Returns the current user's Inputs (last diag he done)
	 *
	 * @param	int		Model ID.
	 *					1: Perfume diag (classic one)
	 *					2: Home fragrance
	 *					3: Love Machine
	 *
	 */
	public function getInputs($model_id = 1)
	{
		// Session data : First
		if (Mage::helper('mynose/diagnostic')->hasInputsInSession() == TRUE)
		{
			$inputs = Mage::helper('mynose/diagnostic')->getInputsFromSession();

			if ( ! is_null($inputs))
			{
				return $inputs;
			}
		}

		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'inputs';

		$user = Mage::getSingleton('customer/session')->getCustomer();
		$store_code = Mage::app()->getStore()->getCode();

		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId(),
				'model_id' => $model_id,
				'lang_code' => $store_code,
				'distributor_id' => self::DISTRIBUTOR_ID,
				'store_id' => self::STORE_ID,
			);

			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			if ( ! empty($result->perfumes) OR ! empty($result->style) OR ! empty($result->notes))
				return $result;
		}
		
		return FALSE;
	}



	
	
	/**
	 * Returns the current user's evaluations
	 *
	 * @return	Array	Array of Evaluations
	 *
	 */
	public function getEvaluationList($status = FALSE, $limit = FALSE)
	{
        // eNose Diagnose Result URL
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'evaluations';
		
		// Add Current logged User data to post (email needed for the Diag)
		$user = Mage::getSingleton('customer/session')->getCustomer();

		$post = array(
			'magento_id' => $user->getId(),
			'limit' => $limit,
			'status' => $status
		);

		// CURL request to eNose
		$json = Mage::helper('mynose/Request')->curl($enose_url, $post);

		// Pure assoc. array from json
		$result = json_decode($json);

		return $result;
	}
	
	
	/**
	 * Returns the current user's temp evaluations list 
	 * (not saved)
	 *
	 * @return	Array	Array of Evaluations
	 *
	 */
	public function getTempEvaluationList($status = FALSE, $limit = FALSE)
	{
		// Returned results
		$result = array();
		
        // eNose Diagnose Result URL
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'evaluations';
		
		// Add Current logged User data to post (email needed for the Diag)
		$user = Mage::getSingleton('customer/session')->getCustomer();

		$post = array(
			'magento_id' => $user->getId(),
			'limit' => $limit,
			'status' => $status
		);

		// CURL request to eNose
		$json = Mage::helper('mynose/Request')->curl($enose_url, $post);

		// Pure assoc. array from json
		$result = json_decode($json);
		
		return $result;
	}
	
	
	
	
	/**
	 * Returns a product Collection from one perfume collection
	 *
	 * @param 	Collection 				Perfumes, Homefragrances, Bears...
	 *
	 * @return 	Product Collection		Corresponding Samples
	 *
	 *
	 */
	public function getSamplesFromDiagnosticItems($items)
	{
		if ($items)
		{
			$items_ids = array();
			
			foreach($items as $item)
				$master_ids[] = $item->master_id;

// Mage::log($master_ids, null, 'partikule.log');

			$products = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('*')
				->addAttributeToFilter('master_id', array('in' => $master_ids))
				->addAttributeToFilter('sku', array('like' => 'S-%'))
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				->addFinalPrice()
			;
			
			// Order as perfume list
//			$products->getSelect()->order(new Zend_Db_Expr("FIELD(e.sku, '" . implode("','", $sample_ids) . "')"));
//			$products->getSelect()->order(new Zend_Db_Expr("FIELD(perfume_id, '" . implode("','", $parfumes_ids) . "')"));
			$products->getSelect()->order(new Zend_Db_Expr("FIELD(master_id, '" . implode("','", $master_ids) . "')"));
			
			// Add Master ID to each product : Needed in template to make a ref.
/*
			foreach($products as $product)
				foreach($items as $item)
					if (substr($product->getSku(),2) == $item->master_id)
						$product->setMasterId($item->master_id);
*/

			return $products;
		}
		return FALSE;
	}
	
	
	/**
	 * Returns one sample from one Diagnostic item
	 * Based on Master IDs and first char of product SKU
	 * Much consistent if the sync between eNose and Magento wasn't done correctly
	 * (means if the master ID Mage attribute isn't updated)
	 * but weird...
	 *
	 */
	public function getSampleFromDiagnosticItem($item)
	{
		return Mage::getModel('mynose/diagnostic')->getSampleFromDiagnosticItem($item);
	}
	
	
	
	/**
	 * Returns a product Collection from one perfume collection
	 *
	 * @param 	Collection 				Perfumes
	 *
	 * @return 	Product Collection		Corresponding Master Products
	 *
	 *
	 */
	public function getProductsFromDiagnosticItems($items)
	{
		if ($items)
		{
			$sample_ids = array();
			
			foreach($items as $item)
				$sample_ids[] = 'M-' . $item->master_id;
			
			$products = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToFilter('sku', array('in' => $sample_ids))
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				->addAttributeToSelect('master_id')
				->addFinalPrice()	
			;
			
			$products->getSelect()->order(new Zend_Db_Expr("FIELD(e.sku, '" . implode("','", $sample_ids) . "')"));
			
			// Add Perfume ID to each product : Needed in template to make a ref.
			// Just kept here in case the synch wasn't correct....
			// kind of lazy "safe mode"
			foreach($products as $product)
			{
				if ($product->getMasterId() == '')
				{
					foreach($items as $item)
					{
						if (substr($product->getSku(),2) == $item->master_id)
						{
							$product->setMasterId($item->master_id);
							break;
						}
					}
				}
			}
			
			return $products;
		}
		return FALSE;
	}
	
	
	
	
	/**
	 * Returns Diagnostics List for the current user
	 *
	 *
	 */
	public function getDiagnosticHistory()
	{
		// eNose Diagnose controller's URL :
		// http://enose.tld/diagnose/history
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'history';
		
		$user = Mage::getSingleton('customer/session')->getCustomer();

		if ($user)
		{
			// Current User : Needs to be posted to eNose
			$post = array (
				'magento_id' => $user->getId(),
				'lang_code' => Mage::app()->getStore()->getCode()
			);
			
			// Request to eNose
			$json = Mage::helper('mynose/Request')->curl($enose_url, $post);
			
			$result = json_decode($json);

			return $result;
		}
		
		return FALSE;
		
/*
		if ($product_id)
		{
			$productModel = Mage::getModel('catalog/product');
			$product = $productModel->load($product_id);
			
			return $product;
		}
		
		return FALSE;
*/
	}
	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getOrders()
	{
		$user = Mage::getSingleton('customer/session')->getCustomer();

		$orderCollection = Mage::getModel('sales/order')->getCollection()
							->addfilter('customer_id', $user->getId());

/* 		$orderCollection->getSelect()->where('e.customer_id = ?', $user->getId()); */
		
		return $orderCollection;
		
		
	}

	public function _toHtml()
	{
//        $this->getLayout()->getBlock('head')->setTitle($this->__('Youpi'));

        return parent::_toHtml();
	
//		return 'Nose Diagnose';

	}


    public function getAddToCartUrl($product, $additional = array())
    {
	   if ($product->getTypeInstance(TRUE)->hasRequiredOptions($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = TRUE;
            }
            if (!isset($additional['_query'])) {
                $additional['_query'] = array();
            }
            $additional['_query']['options'] = 'cart';

            return $this->getProductUrl($product, $additional);
        }
        return $this->helper('checkout/cart')->getAddUrl($product, $additional);

    }

    /**
     * Retrieve Product URL using UrlDataObject
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $additional the route params
     * @return string
     */
    public function getProductUrl($product, $additional = array())
    {
        if ($this->hasProductUrl($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = TRUE;
            }
            return $product->getUrlModel()->getUrl($product, $additional);
        }

        return '#';
    }
	
	
	/**
	 * Returns the current Customer
	 * 
	 */
	public function getCustomer()
	{
		return Mage::getSingleton('customer/session')->getCustomer();
	}


}