<?php

class Partikule_Mynose_Model_Observer extends Partikule_Mynose_Model_Abstract
{
	// Output types
	const OUTPUT_TYPE_PERFUME = 	7;
	const OUTPUT_TYPE_HOME = 		9;
	const OUTPUT_TYPE_COSMETIC = 	10;			// Not currently used

	/**
	 * Magento Source is always 1
	 *
	 * @var int
	 */
	private static $_SOURCE_ID = 1;

	private static $_LOG_SALES_TABLE = 'log_sales';


	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');
		
		$this->evaluations_table = Mage::getSingleton('core/resource')->getTableName('mynose/evaluations');
		$this->perfume_table = Mage::getSingleton('core/resource')->getTableName('mynose/perfume');
		$this->cosmetic_table = Mage::getSingleton('core/resource')->getTableName('mynose/cosmetic');
		$this->homefragrance_table = Mage::getSingleton('core/resource')->getTableName('mynose/homefragrance');
	}


	public function onLogin($observer)
    {
        $event = $observer->getEvent();

        $customer = $event->getCustomer();

        $data = $customer->getData();

        // The action will receive the Magento ID in $data.
        // The API can handle it.
        Mage::helper('lifecycle')->action('nose_fr_user_login', NULL, $data);
    }


    /**
	 * Opens eNose evaluations
	 * when the order is complete (after invoice creation
	 *
	 * @event : sales_order_save_commit_after
	 *
	 * eNose Evaluation statuses :
	 *
	 * Status : 0 Closed, no order is placed.
	 *			1 One order is placed on this sample
	 *			2 Open and awaiting values from user
	 *			3 Done
	 *			4 Canceled
	 *
	 *
	 *
	 */
	public function syncPerfumeEvaluationOnOrderSave($observer)
	{
	    $order = $observer->getOrder();

		// Array of master IDs
		$master_ids = array();
		$enose_status = NULL;

	    // New order : Set the Evaluations to status 
	    if( $order->getState() == Mage_Sales_Model_Order::STATE_NEW || $order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
	    {
			$items = $order->getItemsCollection();
	    
			foreach($items as $item)
			{
				// Load the product to get the Master ID
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				if ($product->getMasterId() != '')
					$master_ids[] = $product->getMasterId();
			}
			
			if ( ! empty($master_ids) )
			{
				// Get the user's info
				$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
			
				// Get the user from eNose. Assoc. Array, no object
				$user = Mage::getModel('mynose/user')->getNoseUser($customer);
		
				// Order Complete
				// Status 1 : Sample ordered, listed in evaluation list
				// Status 2 : Awaiting user's Eval
				$eval_status = ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) ? 1 : 2;
				
		   		$connection = $this->getWriteConnection();

				// Update Data
				$now = date('Y-m-d H:i:s');
				$update_data = array(
					'evaluation_status' => $eval_status,
					'commande_dt' => $now
				);
				
				
				
				// User's Evaluation output IDs
				$query =	' SELECT group_concat(output_id) as ids FROM ' . $this->evaluations_table
							. ' WHERE id_user = ' . $user['id_user']
				;
		
				$results = $connection->fetchOne($query);				
				
				$evaluation_ids = ( ! empty($results)) ? explode(',', $results) : array();
				
				
				// Order's master ids
				$query =	' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_PERFUME . ' as output_type FROM ' . $this->perfume_table . 
							' WHERE master_id in (' . implode(",", $master_ids) . ')' .
							' union ' .
							' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_HOME . ' as output_type FROM ' . $this->homefragrance_table . 
							' WHERE master_id in (' . implode(",", $master_ids) . ')' .
							' union ' .
							' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_COSMETIC . ' as output_type FROM ' . $this->cosmetic_table . 
							' WHERE master_id in (' . implode(",", $master_ids) . ')'
						;
		
				$masters = $connection->fetchAll($query);				
				
				// Compare with order's master IDs array
				foreach($masters as $master)
				{
					// UPDATE : Is in Eval IDs
					if (in_array($master['output_id'], $evaluation_ids))
					{
						$where = " id_user = " . $user['id_user'] . 
								 " AND output_id = " . $master['output_id'] .
								 " AND evaluation_status < 2
								 ";
						$connection->update($this->evaluations_table, $update_data, $where);
						$connection->commit();
					}
					// INSERT : Is not in eval IDs
					else
					{
						$insertData = array(
							'id_user' => 	$user['id_user'],
							'output_id' =>	$master['output_id'],
							'id_diag' => 0,
							'evaluation_status' => $eval_status,
							'commande_dt' => $now,
							'output_type' => $master['output_type']
						);
						$connection->insert($this->evaluations_table, $insertData);
						$connection->commit();
					}
				}
			}
		}
	}
	
	
	/**
	 * Creates and saves the coupon
	 * Called on : 	sales_order_save_commit_after
	 * See : 		code/local/Partikule/Mynose/etc/config.xml
	 *
	 */
	public function createCouponOnSampleOrder($observer)
	{
	    $order = $observer->getOrder();

	    if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
	    {
			$settings_product_id = Mage::getStoreConfig('nose/general/bundle_product_id');
			$attributes = Mage::getModel('amorderattr/attribute');
			$attributes->load($order->getId(), 'order_id');
			$existing_coupon = $attributes->getData('coupon_code');

			// If no coupon for this order : Create one
			if ( ! $existing_coupon)
			{
				$found = FALSE;
				$items = $order->getItemsCollection();

				// Try to find the Bundle ID
				foreach($items as $item)
				{
					if ($item->getProductId() == $settings_product_id)
					{
						$found = TRUE;
						break;
					}
				}

				if ($found == TRUE)
				{
					$bundle = Mage::getModel('catalog/product')->load($settings_product_id);
					$bundlePrice = $bundle->getPrice();

					// Get the user's info
					$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

					// Coupon code
					$rulePrefix = 'Samples_' . $customer->getId() . '_' . $order->getId() . '_';
					$couponCode = $this->createUniqueCouponFromMasterRule('_Master_Samples', $rulePrefix, $bundlePrice);

					if ($couponCode)
					{
						// Save Coupon code into Amasty custom attributes
						$attributes->setData('coupon_code', $couponCode);
						$attributes->save();

						// Save the Coupon in eNose DB (in users table, brand pld way)
						$data = array(
							'coupon_code' => 	$couponCode
						);
						$enoseUser = Mage::getModel('mynose/user')->getNoseUserFromCustomer($customer);

						Mage::getModel('mynose/user')->updateNoseUserData($enoseUser, $data);

                        // New way (2017)
                        Mage::getModel('mynose/user')->insertNoseUserCouponFromOrder(
                            $enoseUser['id_user'],
                            $couponCode,
                            $bundlePrice,
                            'kit_order',
                            $order->getId()
                        );
					}
				}
			}




			/*
			$amount = 0;
			$nbSamples = 0;
			$items = $order->getItemsCollection();

			foreach($items as $item)
			{
				// Only get samples from order
				if (substr($item->getSku(), 0, 1) == 'S')
				{
					$amount += $item->getOriginalPrice();
					$nbSamples += 1;
				}
			}
			
			// Get the Amasty Module custom attributes
            $attributes = Mage::getModel('amorderattr/attribute');
			$attributes->load($order->getId(), 'order_id');
			$existing_coupon = $attributes->getData('coupon_code');
			
			if ($nbSamples > 0 && ! $existing_coupon)
			{

				// Get the user's info
				$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
				
				$rulePrefix = 'Samples_' . $customer->getId() . '_' . $order->getId() . '_';
				$couponCode = $this->createUniqueCouponFromMasterRule('_Master_Samples', $rulePrefix, $amount);
				
				if ($couponCode)
				{
					// Mage::log('Create coupon : ' . $couponCode, null, 'partikule.log');
					// Save Coupon code into Amasty custom attributes
	        		$attributes->setData('coupon_code', $couponCode);
			        $attributes->save();

					// Send Email
					// Removed on 2012.11.27
					// The coupon is sent with the Summary Email
					// Mage::helper('mynose/email')->sendSampleCouponMail($customer, $order, $couponCode, $amount, $nbSamples);
				}
			}
			*/
		}

		return $this;
	}


	/**
	 * Sends the order to the eNose DB :
	 * table : log_sale
	 *
	 * @param $observer
	 *
	 */
	public function logSale($observer)
	{
		$order = $observer->getOrder();

		if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
		{
			$data = array();

			$keys = array
			(
				// Magento => Local : Source
				// Be sure the "local" DB fields exist
				// Prefix with 'order' : reset from order
				// Order
				'entity_id' => 				'entity_id',						// Order : Internal ID
				'item_id' =>				'item_id',							// Item : Internal ID
				'increment_id' => 			'order_number',						// Order : Logical ID (eg. 100000092)
				'customer_id' => 			'order_customer_id',				// Order

				// Order : Subtotal
				'order_discount_invoiced' => 'order_discount_ttc',
				'subtotal_invoiced' =>		'order_subtotal_ht',				// Order : Subtotal HT without shipping
				'order_tax_invoiced' =>		'order_tax_invoiced',			// Order : VAT on subtotal
				'subtotal_incl_tax' =>		'order_subtotal_ttc',				// Order : Subtotal TTC without shipping

				// Order : Coupon
				'coupon_code' => 			'order_coupon_code',				// Order

				// Item
				'created_at' =>				'item_date_creation',				// Item
				'updated_at' =>				'item_date_update',					// Item
				'product_id' =>				'item_product_id',					// Item
				'sku' =>					'item_product_sku',					// Item
				'name' =>					'item_product_name',				// Item

				// Item Price
				'original_price' =>			'item_original_price_ttc',			// Item : Original price
				'price_incl_tax' =>			'item_price_ttc',					// Item : Price

				// Item taxes
				'tax_percent' =>			'item_tax_percent',					// Item
				'tax_invoiced' =>			'item_tax_invoiced',				// Item

				// Item : Quantity
				'qty_ordered' =>			'item_qty_ordered',					// Item
				'qty_invoiced' =>			'item_qty_invoiced',				// Item
				'qty_shipped' =>			'item_qty_shipped',					// Item
				'qty_refunded' =>			'item_qty_refunded',				// Item
				'qty_canceled' =>			'item_qty_canceled',				// Item

				// Discount
//				'discount_amount' =>		'item_discount_ttc',				// Item
				'discount_invoiced' =>		'item_discount_invoiced_ttc',		// Item

				// Item : Row price
				'row_total_incl_tax' =>		'item_total_ttc',					// Item
			);

			$magento_keys = array_keys($keys);
			$local_keys = array_values($keys);

			// Get array from Magento objects
			$order_data = $order->getData();
			$items = $order->getAllVisibleItems();

			// Mage::log($order->getData(), null, 'partikule.log');

			// Create the data array
			foreach ($items as $item)
			{
				// Mage::log($item->getData(), null, 'partikule.log');

				$data[] = array_merge
				(
					$order_data,
					$item->getData(),
					// Order specific data (overwritten by $item and restored)
					array(
						'order_tax_invoiced' => $order['tax_invoiced'],
						// Discount
						'order_base_discount_amount' => $order['base_discount_amount'],
						'order_base_discount_invoiced' => $order['base_discount_invoiced'],
						'order_discount_amount' => $order['discount_amount'],
						'order_discount_invoiced' => $order['discount_invoiced'],
					)
				);
			}

			foreach($data as $row)
			{
				$local_row = array('source' => self::$_SOURCE_ID);

				foreach($local_keys as $idx => $key)
				{
					$local_row[$key] = addslashes($row[$magento_keys[$idx]]);
				}

				$local_data[] = $local_row;
			}

			$local_sql_data = array();
			foreach($local_data as $row)
			{
				$local_sql_data[] = "('" . implode("','", $row) . "')";
			}

			// Add "source" at the beginning of the array;
			array_unshift($local_keys, 'source');

			$sql_vals = implode(',', $local_sql_data);
			$sql_keys = implode(',', $local_keys);

			$sql = "
				insert ignore into " . self::$_LOG_SALES_TABLE . "
				(" . $sql_keys . ")
				values
				" . $sql_vals . "
			";

			$connection = $this->getWriteConnection();

			$connection->query($sql);
		}
	}



	/**
	 * Sends the global Summary Email
	 * Called on : 	sales_order_save_commit_after
	 * See : 		code/local/Partikule/Mynose/etc/config.xml
	 *
	 */
	public function sendSummaryEmailAfterShipping($observer)
	{
		$order = $observer->getOrder();

		$shipment = NULL;
		foreach($order->getShipmentsCollection() as $ship)
		{
			$shipment = $ship;
		}

		// $shipment = $observer->getEvent()->getShipment();
		// $order = $shipment->getOrder();

		if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
		{
			$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

			// Send Email
			Mage::helper('mynose/Email')->sendSummaryEmailAfterShipping($customer, $order, $shipment);
		}

		return $this;
	}


	/**
	 * Creates one coupon based on one master rule name
	 *
	 * @param $masterName	Rule master name
	 * @param $rulePrefix	Rule name prefix
	 * @param $amount		Amount
	 *
	 * @return bool|string
	 */
	public function createUniqueCouponFromMasterRule($masterName, $rulePrefix, $amount)
	{
		$uniqueId = $this->_generateUniqueCouponCode(10);

		// Only executed if Master Rule found
		$rule = Mage::getModel('salesrule/rule')->load($masterName, 'name');
        if (!$rule || ! $rule->getId() || ! $rule->getIsActive() )
            return FALSE;

        $rule->setId(null);
   		$rule->setName($rulePrefix . $uniqueId);
		$rule->setDescription('Nose Samples Order Coupon');
		$rule->setFromDate(date('Y-m-d'));
		$rule->setCouponCode($uniqueId);
		$rule->setUsesPerCoupon(1);
		$rule->setUsesPerCustomer(1);
		// by_percent - 	Percent of product price discount
		// by_fixed - 		Fixed amount discount
		// cart_fixed - 	Fixed amount discount for whole cart
		// buy_x_get_y - 	Buy X get Y free (discount amount is Y)
		// $rule->setSimpleAction('cart_fixed');
		$rule->setDiscountAmount($amount);

		// the conditions are for 'if all of these conditions are true'
		// for if any one of the conditions is true set 'aggregator' to 'any'
		// for if all of the conditions are false set 'value' to 0.
		// for if any one of the conditions is false set 'aggregator' to 'any' and 'value' to 0
		$conditions = array();
		$conditions[1] = array(
			'type' => 'salesrule/rule_condition_combine',
			'aggregator' => 'all',
			'value' => 1,
			'new_child' => ''
		);

		$rule->setData('conditions',$conditions);
//		$rule->loadPost($rule->getData());
		$rule->setCouponType(2);
		
		/*
		$labels = array();
		$labels[0] = 'Default store label';//default store label
		$labels[1] = 'Label for store with id 1';
		//....
		$labels[n] = 'Label for store with id n';
		$rule->setStoreLabels($labels);
		//add one line for each store view you have. The key is the store view ID
		*/
		
		$rule->save();

		return $uniqueId;	
	}
	
	
	/**
	 * Creates User's unique coupons based on their sample order
	 *
	 * @param	$amount int		Value of the coupon
	 * @return	int				ID of the coupon
	 *
	 */
	public function createCoupon($amount)
	{	
		$uniqueId = $this->_generateUniqueCouponCode(10);
		$rule = Mage::getModel('salesrule/rule');
		
		$rule->setName($uniqueId);
		$rule->setDescription('Nose Sample Coupon');
		$rule->setFromDate(date('Y-m-d'));
		// $rule->setToDate('2011-01-01');
		$rule->setCouponCode($uniqueId);
		$rule->setUsesPerCoupon(1);
		$rule->setUsesPerCustomer(1);
		$rule->setCustomerGroupIds($this->_getAllCustomerGroups());//if you want only certain groups replace getAllCustomerGroups() with an array of desired ids
		$rule->setIsActive(1);
		$rule->setStopRulesProcessing(0);	// Set to 1 if you want all other rules after this to not be processed
		$rule->setIsRss(0);					// Set to 1 if you want this rule to be public in rss
		$rule->setIsAdvanced(1);			// ?
		$rule->setProductIds('');
		$rule->setSortOrder(0);				// order 
		
		// all available discount types
		// by_percent - 	Percent of product price discount
		// by_fixed - 		Fixed amount discount
		// cart_fixed - 	Fixed amount discount for whole cart
		// buy_x_get_y - 	Buy X get Y free (discount amount is Y)
		$rule->setSimpleAction('by_fixed');
 
		$rule->setDiscountAmount($amount);			//the discount amount/percent. if SimpleAction is by_percent this value must be <= 100
		$rule->setDiscountQty(1);				//Maximum Qty Discount is Applied to
		$rule->setDiscountStep(0);				//used for buy_x_get_y; This is X
		$rule->setSimpleFreeShipping(0);		//set to 1 for Free shipping
		$rule->setApplyToShipping(0);			//set to 0 if you don't want the rule to be applied to shipping
		$rule->setWebsiteIds($this->_getAllWbsites());	//if you want only certain websites replace getAllWbsites() with an array of desired ids
 
		$conditions = array();

		// the conditions are for 'if all of these conditions are true'
		// for if any one of the conditions is true set 'aggregator' to 'any'
		// for if all of the conditions are false set 'value' to 0.
		// for if any one of the conditions is false set 'aggregator' to 'any' and 'value' to 0
		$conditions[1] = array(
			'type' => 'salesrule/rule_condition_combine',
			'aggregator' => 'all',
			'value' => 1,
			'new_child' => ''
		);

		$conditions['1--1'] = Array
		(
			'type' => 'salesrule/rule_condition_address',
			'attribute' => 'base_subtotal',
			'operator' => '>=',
			'value' => 200
		);
		
		//to add an other constraint on product attributes (not cart attributes like above) uncomment and change the following:
		/*
		$conditions['1--2'] = array
		(
			'type' => 'salesrule/rule_condition_product_found',//-> means 'if all of the following are true' - same rules as above for 'aggregator' and 'value'
			//other values for type: 'salesrule/rule_condition_product_subselect' 'salesrule/rule_condition_combine'
			'value' => 1,
			'aggregator' => 'all',
			'new_child' => '',
		);
		 
		$conditions['1--2--1'] = array
		(
			'type' => 'salesrule/rule_condition_product',
			'attribute' => 'sku',
			'operator' => '==',
			'value' => '12',
		);
		*/
		//$conditions['1--2--1'] means sku equals 12. For other constraints change 'attribute', 'operator'(see list above), 'value'
		 
		$rule->setData('conditions',$conditions);
		$rule->loadPost($rule->getData());
		$rule->setCouponType(2);
		
		/*
		$labels = array();
		$labels[0] = 'Default store label';//default store label
		$labels[1] = 'Label for store with id 1';
		//....
		$labels[n] = 'Label for store with id n';
		$rule->setStoreLabels($labels);
		//add one line for each store view you have. The key is the store view ID
		*/
		
		$rule->save();
		
		return $uniqueId;	
				
	}


	/**
	 * Called on : 	sales_order_save_commit_after
	 *
	 */
	public function runPurchaseLifecycleAction($observer)
	{
		$order = $observer->getOrder();

		if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
		{
			$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

			$eNoseUser = Mage::getModel('mynose/user')->getNoseUser($customer);

			if ($eNoseUser)
			{
				// Sample Kit ID
				$sample_kit_product_id = Mage::getStoreConfig('nose/general/bundle_product_id');

				// Giftcards SKUs array
				$giftcard_skus = Mage::getStoreConfig('nose/giftcard_skus/skus');
				$giftcard_skus = str_replace(' ', '', $giftcard_skus);
				$giftcard_skus = explode(',', $giftcard_skus);


				// Mage::log('Sample Kit ID : '.$sample_kit_product_id, null, 'partikule.log');
				$sample_kit_found = FALSE;
				$product_found = FALSE;
				$giftcard_found = FALSE;
				$sample_found = FALSE;

				$items = $order->getItemsCollection();

				// Try to find the Bundle ID
				foreach($items as $item)
				{
					// Mage::log($item->getData(), null, 'partikule.log');

					if ($item->getProductId() == $sample_kit_product_id)
					{
						$sample_kit_found = TRUE;
					}
					else
					{
						$sku = $item->getSku();

						// Sample
						if (substr($sku,0,2) == 'S-')
						{
							$sample_found = TRUE;
						}
						else
						{
							// GiftCard
							if (in_array($sku, $giftcard_skus))
							{
								$giftcard_found = TRUE;
							}
							// Product
							else
							{
								$product_found = TRUE;
							}
						}
					}
				}

				// Actions
				if ($product_found)
				{
					Mage::helper('lifecycle')->action('product_buy_online', $eNoseUser);
				}
				else
				{
					if ($sample_kit_found)
					{
						Mage::helper('lifecycle')->action('sample_kit_buy', $eNoseUser);
					}
					else if($giftcard_found)
					{
						Mage::helper('lifecycle')->action('giftcard_buy', $eNoseUser);
					}
					else if ($sample_found)
					{
						Mage::helper('lifecycle')->action('sample_buy', $eNoseUser);
					}
				}
			}
		}

		return $this;
	}


	/*
	 * -------------------------------------------------
	 * Private methods
	 * -------------------------------------------------
	 */



	private function _getAllWbsites()
	{
		$websites = Mage::getModel('core/website')->getCollection();
		$websiteIds = array();
		foreach ($websites as $website){
			$websiteIds[] = $website->getId();
		}
		return $websiteIds;
	}


	
	private function _getAllCustomerGroups()
	{
		//get all customer groups
		$customerGroups = Mage::getModel('customer/group')->getCollection();
		$groups = array();
		foreach ($customerGroups as $group){
			$groups[] = $group->getId();
		}
		return $groups;
	}
	
	private function _generateUniqueCouponCode($length = null)
	{
		$rndId = crypt(uniqid(rand(),1));
		$rndId = strip_tags(stripslashes($rndId));
		$rndId = str_replace(array(".", "$"),"",$rndId);
		$rndId = strrev(str_replace("/","",$rndId));
		if (!is_null($rndId)){
			$rndId = strtoupper(substr($rndId, 0, $length));
		}
		else {
			$rndId = strtoupper($rndId);
		}
		
		if ( FALSE == $this->_isCouponCodeUnique($rndId))
			return $this->_generateUniqueCouponCode($length);
		
		return $rndId;
	}

	/**
	 * Returns TRUE if no coupon with this code was found
	 *
	 */
	private function _isCouponCodeUnique($couponCode)
	{
		$coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');

        if ( $coupon->hasData() )
        	return FALSE;
        
        return TRUE;
	}

}