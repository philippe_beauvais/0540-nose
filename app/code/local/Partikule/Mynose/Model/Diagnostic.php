<?php

/**
 * Disgnostic Model
 *
 *
 */
class Partikule_Mynose_Model_Diagnostic extends Partikule_Mynose_Model_Abstract
{
	const MODEL_PERFUME = 	1;
	const MODEL_HOME = 		2;
	const MODEL_LOVE = 		3;

	/*
	 * Array of current Diags IDs
	 * key is the model ID
	 *
	 */
	private $_current_diag_id = array();
	private $_current_customer_diag_id = array();


	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');

		$this->users_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
		$this->diagnostic_table =  Mage::getSingleton('core/resource')->getTableName('mynose/diagnostic');
	}

	public function setEmailed($id_diag, $value = 0)
	{
		$connection = $this->getWriteConnection();
	
		$where = "id_diag = " . $id_diag;
		$data = array('emailed' => $value);

		try
		{
			$connection->beginTransaction();
			$connection->update($this->diagnostic_table, $data, $where);
			$connection->commit();
			
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}
	
	
	
	/**
	 * Return directly the diag of the current user
	 * Direct call to the eNose DB
	 *
	 * @notice : 	DO NOT USE.
	 * 				USE the cURL method instead.
	 * 				btw, the method getCurrentDiagId() isn't correct, it should be based on the magento_id
	 *
	 *
	 */
	public function getCurrentUserDiagnostic()
	{
		$user = Mage::getSingleton('customer/session');
		
		$diag = array();
		
		if ($user->getId())
		{
			$id_diag = $this->getCurrentDiagId($user);
			
			if ($id_diag)
			{
			
				$select = $this->getReadConnection()->select()
					->from( array('d' => $this->diagnostic_table))
					->where('id_diag = ?', $id_diag)
				;
				$query = $select->query();
				$diag = $query->fetch();
			}
		}
		return $diag;
	}


	public function getCurrentDiagId($user, $model_id = self::MODEL_PERFUME)
	{
		if( ! empty($this->_current_diag_id[$user->getId().'.'.$model_id]))
			return $this->_current_diag_id[$user->getId().'.'.$model_id];
		
		$sql = "
			SELECT MAX(id_diag) as id_diag FROM diagnostic WHERE id_user='" .$user->getId() ."'
			and model_id = '" . $model_id . "'
		";

		$select = $this->getReadConnection()->query($sql);
		$max = $select->fetch();
		
		return $max['id_diag'];
	}



	public function getCustomerCurrentDiagId($customer, $model_id = self::MODEL_PERFUME)
	{
		if( ! empty($this->_current_customer_diag_id[$customer->getId().'.'.$model_id]))
			return $this->_current_customer_diag_id[$customer->getId().'.'.$model_id];

		$sql = "
			SELECT MAX(id_diag) as id_diag FROM diagnostic
			join users on users.id_user = diagnostic.id_user
			WHERE users.magento_id='" .$customer->getId() ."'
			and model_id = '" . $model_id . "'
		";

		$select = $this->getReadConnection()->query($sql);
		$max = $select->fetch();

		return $max['id_diag'];
	}

	/**
	 * Returns one Magento customer's masters list from the diag
	 * @param     $customer
	 * @param int $model_id
	 *
	 * @return array
	 */
	public function getCustomerMasterDiag($customer, $model_id = self::MODEL_PERFUME)
	{
		$diag = array();
		$id_diag = $this->getCustomerCurrentDiagId($customer, $model_id);

		if ( ! empty($id_diag))
		{
			$sql = "
				select
					output_histo.rank,
					master.*,
					brand.brand_name
				from master
				join output_histo on (output_histo.id_diag=".$id_diag." and output_histo.output_id = master.master_id and output_histo.output_type=7)
				join brand on brand.brand_id = master.brand_id
				order by output_histo.rank
				limit 5
				;
			";
			$select = $this->getReadConnection()->query($sql);
			$diag = $select->fetchall();
		}

		return $diag;
	}



	/**
	 * Get the Diagnostic from one user
	 * @TODO : Rewrite, doesn't work as it is.
	 *
	 * @param $user
	 *
	 * @return array|mixed
	 */
	public function getCustomerDiagnostic($user)
	{
		$diag = array();

		if ($user && $user->getId())
		{
			$id_diag = $this->getCurrentDiagId($user);

			if ($id_diag)
			{
				$select = $this->getReadConnection()->select()
					->from( array('d' => $this->diagnostic_table))
					->where('id_diag = ?', $id_diag)
				;
				$query = $select->query();
				$diag = $query->fetch();
			}
		}
		return $diag;
	}


	/**
	 * Get the Diagnostic from one user
	 * @TODO : Rewrite, doesn't work as it is.
	 *
	 * @param $user
	 *
	 * @return array|mixed
	 */
	public function getUserDiagnostic($user)
	{
		$diag = array();

		if ($user && $user->getId())
		{
			$id_diag = $this->getCurrentDiagId($user);

			if ($id_diag)
			{
				$select = $this->getReadConnection()->select()
					->from( array('d' => $this->diagnostic_table))
					->where('id_diag = ?', $id_diag)
				;
				$query = $select->query();
				$diag = $query->fetch();
			}
		}
		return $diag;
	}

	/**
	 * Returns one sample from one Diagnostic item
	 * Based on Master IDs and first char of product SKU
	 * Much consistent if the sync between eNose and Magento wasn't done correctly
	 * (means if the master ID Mage attribute isn't updated)
	 * but weird...
	 *
	 */
	public function getSampleFromDiagnosticItem($item, $is_array = FALSE)
	{
		$product = NULL;
		$master_id = NULL;

		if (is_array($item))
			$master_id = ! empty($item['master_id']) ? $item['master_id'] : NULL;
		else
			$master_id = $item->master_id;

		if ( ! is_null($master_id))
		{

			$products = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('master_id')
				->addAttributeToFilter('sku', array('like' => 'S-'. $master_id .'-%'))
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				//	->addFinalPrice()
			;

			// Mage::log($products->getSelect()->__toString(), null, 'partikule.log');

			$product = $products->getFirstItem();


		}
		return $product;
	}

	/*
	 * Deprecated on 2016.03.15
	 * Reason : Not used
	 *
	public function getCustomerDiagCount($customer, $model_id = self::MODEL_PERFUME)
	{
		$sql = "
			SELECT count(id_diag) as nb_diag
			FROM diagnostic
			join users on users.id_user = diagnostic.id_user
			WHERE users.magento_id='" .$customer->getId() ."'
			and model_id = '" . $model_id . "'
		";

		$select = $this->getReadConnection()->query($sql);
		$result = $select->fetch();

		$nb = $result['nb_diag'];

		return $nb;
	}
	*/


	public function getCustomerAlsoLiked($customer)
	{
		$sql = "
			select
				distinct (ais.sale_perfume_id) as master_id,
				p.perfume_name,
				b.brand_name,
				sum(ais.score) as score
			from
				input_attributes_current iac
				join users u on u.id_user = iac.id_user and u.magento_id=".$customer->getId()."
				join asso_input_sales ais on ais.input_perfume_id = iac.input_id and ais.nose = 1
				join perfume p on p.perfume_id = ais.sale_perfume_id
				join brand b on b.brand_id = p.brand_id
			where
				iac.input_type = 7
				and iac.model_id = 1
				and iac.flag_input_user = 1
			group by ais.sale_perfume_id
			order by ais.score DESC
			limit 5
		";

		$select = $this->getReadConnection()->query($sql);
		$result = $select->fetchAll();

		return $result;
	}

	public function hasAlreadyOneDiagnostic($customer)
	{
		$sql = "
			select count(d.id_diag) as nb
			from diagnostic d
			join users u on u.id_user = d.id_user and u.magento_id = ".$customer->getID().";
		";

		$select = $this->getReadConnection()->query($sql);
		$result = $select->fetch();

		$nb = $result['nb'];

		return $nb;
	}

}