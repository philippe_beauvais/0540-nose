<?php

/**
 *
 * 
 * 
 *
 */
class Partikule_Mynose_Model_Contact extends Partikule_Mynose_Model_Abstract
{

	const RELATION_STATUS_INVITED = 1;
	const RELATION_STATUS_ACCEPTED = 2;
	const RELATION_STATUS_REFUSED = 3;
	const RELATION_STATUS_DELETED = 4;

	const FACEBOOK_JOIN_FROM = 4;


	protected $_unique_relation_type_ids = NULL;
	
	// Default user's group used when creating one user from this module
	protected $default_user_group = 6;
	
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');
		
		$this->relation_type_table = Mage::getSingleton('core/resource')->getTableName('mynose/relation_type');
		$this->relation_type_lang_table = Mage::getSingleton('core/resource')->getTableName('mynose/relation_type_lang');
		$this->users_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
		$this->relation_table = Mage::getSingleton('core/resource')->getTableName('mynose/relation');
		$this->relation_histo_table = Mage::getSingleton('core/resource')->getTableName('mynose/relation_histo');
		$this->iac_table = Mage::getSingleton('core/resource')->getTableName('mynose/input_attributes_current');
		$this->diagnostic_table = Mage::getSingleton('core/resource')->getTableName('mynose/diagnostic');
	}


/*	public function onSponsorValidated($event)
	{
		$sender = $event->getSponsor();
		$invited = $event->getSponsored();

		// Relation ?
		$relation = $this->getRelation($sender, $invited);

		// Set relation to "Friend" if the invited user / sender has already one lover relation
		if (
			$this->isLoveRelationId($relation['id_relation_type'])
			&&
			(
				$this->hasAlreadyLover($invited, $sender) OR
				$this->hasAlreadyLover($sender, $invited)
			)
		)
		{
			$this->setRelationType($invited, $sender, self::DEFAULT_RELATION_TYPE_ID);
		}

		$this->validateRelation($sender, $invited);
	}*/


	
	/**
	 * Returns all relation types
	 *
	 * @param	Array	Current Nose User
	 *					If not NULL, the returned relation types will exclude the unique one that the user already has.
	 *
	 */
	public function getRelationTypes($currentNoseUser = NULL)
	{
		$userUniqueRelationsTypes = FALSE;
		
		if ($currentNoseUser != NULL)
		{
			// 1. check if the users are potentially linked
			$select = $this->getReadConnection()->select()
				->from( array('r' => $this->relation_table))
				->where('id_user1 = ?', $currentNoseUser['id_user'])
				->where('r.id_relation_type IN (?)', explode(',', $this->_getUniqueRelationIds()));
			;
			$query = $select->query();
			$userUniqueRelationsTypes = $query->fetchall();
		}
		
		$select = $this->getReadConnection()->select()
			->from(
				array('rt' => $this->relation_type_table),
				array(
					'id_relation_type' => 'id_relation_type',
					'relation_code' => 'relation_code',
					'uniq', 'uniq'
				)
			)
			->join(
				array('rtl' => $this->relation_type_lang_table),
				"(rt.id_relation_type = rtl.id_relation_type AND rtl.lang = '" . Mage::app()->getStore()->getCode() . "')",
				array(
					'relation_name' => 'relation_name'
				)
			);

		// If the user has one unique relation, filter on non-unique one.
		if ( FALSE != $userUniqueRelationsTypes)
		{
			$select->where('rt.id_relation_type NOT IN (?)', explode(',', $this->_getUniqueRelationIds()));
		}
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');

		$query = $select->query();

		$relationsTypes = $query->fetchall();
		return $relationsTypes;
	}
	
	
	/**
	 * Returns LOVE relation types
	 *
	 */
	public function getLoveRelationTypes()
	{
		$select = $this->getReadConnection()->select()
			->from( array('rt' => $this->relation_type_table))
			->join(
				array('rtl' => $this->relation_type_lang_table),
				"(rt.id_relation_type = rtl.id_relation_type AND rtl.lang = '" . Mage::app()->getStore()->getCode() . "')",
				array(
					'relation_name' => 'relation_name'
				)
			)
			->where('rt.id_relation_type IN (?)', explode(',', $this->_getUniqueRelationIds()))
		;
		
		$query = $select->query();

		$relationsTypes = $query->fetchall();

		return $relationsTypes;		
	}	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function isLoveRelationId($id_relation_type)
	{
		$love_relation_type_ids = explode(',', $this->_getUniqueRelationIds());
		
		return in_array($id_relation_type, $love_relation_type_ids);
	}
	
	public function getRelationType($id_relation_type)
	{
		$select = $this->getReadConnection()->select()
			->from( array('rt' => $this->relation_type_table))
			->join(array('rtl' => $this->relation_type_lang_table),
				"(rt.id_relation_type = rtl.id_relation_type AND rtl.lang = '" . Mage::app()->getStore()->getCode() . "')"
			)
			->where('rt.id_relation_type = ?', $id_relation_type);
		;	

		// trace("{$select}".PHP_EOL);
		
		$query = $select->query();

		$relationsType = $query->fetch();

		return $relationsType;		
	}
	
	
	
	
	public function getUserFromEmail($email)
	{
		$select = $this->getReadConnection()->select()
			->from( array('u' => $this->users_table))
			->where('email = ?', $email);
		;
		
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');		
		
		$query = $select->query();

		$user = $query->fetch();
		
		return $user;
	}
	
	public function getUserFromId($id)
	{
		$select = $this->getReadConnection()->select()
			->from( array('u' => $this->users_table))
			->where('id_user = ?', $id);
		;
		
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');		
		
		$query = $select->query();

		$user = $query->fetch();
		
		return $user;
	}

	public function getUserFrom($cond)
	{
		$user = FALSE;
		
		if (is_array($cond))
		{
			$select = $this->getReadConnection()->select()
				->from( array('u' => $this->users_table));
			
			foreach($cond as $key => $value)
			{
				$select->where($key . ' = ?', $value);
			}
			
			$query = $select->query();

			$user = $query->fetch();
		}
		return $user;		
	}
		
	
	/**
	 * Return the existing relation between 2 users
	 *
	 * @param	array	User's info
	 * @param	array	User's info
	 * @return	mixed	Array of the relation or FALSE if no relation is found
	 *
	 */
	public function getRelation($user1, $user2, $status = NULL)
	{
		return $this->_getRelation($user1, $user2, $status);
	}
	
	
	/**
	 * Sets the relation type betwen 2 users
	 * @param	array	Currently connected user
	 *
	 */
	public function setRelationType($user1, $user2, $relation_type_id)
	{
		$connection = $this->getWriteConnection();

		$data1 = array(
			'id_user1' => $user1['id_user'],
			'id_user2' => $user2['id_user'],
			'id_relation_type' => $relation_type_id
		);

		$data2 = array(
			'id_user1' => $user2['id_user'],
			'id_user2' => $user1['id_user'],
			'id_relation_type' => $this->getMirrorTypeId($relation_type_id)
		);

		$where1 = "id_user1 = " . $user1['id_user'] . " and id_user2 = " . $user2['id_user'];
		$where2 = "id_user1 = " . $user2['id_user'] . " and id_user2 = " . $user1['id_user'];

		try
		{
			$connection->beginTransaction();

			$connection->update($this->relation_table, $data1, $where1);
			$connection->update($this->relation_table, $data2, $where2);

			$connection->commit();
			
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}
	
	
	/**
	 * Return all the relations from one user
	 *
	 * @param	array	User's info
	 * @param	array	User's info
	 * @return	mixed	Array of the relation or FALSE if no relation is found
	 *
	 */
	public function getRelations($user1, $status = NULL, $excluded_user = FALSE)
	{
		$relations = FALSE;
		
		if ($user1['id_user'])
		{
			// 1. check if the users are potentially linked
			$select = $this->getReadConnection()->select()
				->from( array('r' => $this->relation_table))
				->where('id_user1 = ?', $user1['id_user'])
			;
	
			if ( $excluded_user != FALSE)
				$select->where('id_user2 != ?', $excluded_user['id_user']);
			
			if ( !is_null($status))
			{
				if ( ! is_array($status))
				{
					$select->where('status = ?', $status);
				}
				else
				{
					foreach($status as $key => $stat)
					{
						if ($key == 0)
							$select->where('status = ?', $stat);
						else
							$select->orwhere('status = ?', $stat);
					}
				}
			}
			// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');			
			$query = $select->query();
			$relations = $query->fetchall();
		}
		
		return $relations;
	}
	
	
	/**
	 * Returns the lover from one Nose user
	 * take care : one Nose user, not one Magento user ! 
	 *
	 */
	public function getLover($user)
	{
		$relation_type_ids = $this->_getUniqueRelationIds();

		$select = $this->getReadConnection()->select()
			->from( array('r' => $this->relation_table))
			->join(array('u' => $this->users_table),
					'u.id_user = r.id_user1'
				)

			->where('id_user2 = ?', $user['id_user'])
			->where('r.id_relation_type IN (?)', explode(',', $relation_type_ids));
		;
		
		$query = $select->query();
		$lover = $query->fetch();
	
		return $lover;
	}
	
	
	public function hasAlreadyLover($user1, $excluded_user = FALSE)
	{
		$has = FALSE;
		
		$relation_type_ids = $this->_getUniqueRelationIds();
		
		if ($user1['id_user'] && !empty($relation_type_ids))
		{
			// 1. check if the users are potentially linked
			$select = $this->getReadConnection()->select()
				->from( array('r' => $this->relation_table))
				->where('id_user1 = ?', $user1['id_user'])
				->where('r.id_relation_type IN (?)', explode(',', $relation_type_ids));
			;
			
			if ( $excluded_user != FALSE)
				$select->where('id_user2 != ?', $excluded_user['id_user']);
			
//			 Mage::log("{$select}".PHP_EOL, null, 'partikule.log');			
			$query = $select->query();
			$relations = $query->fetchall();
			
			if ($relations && count($relations) > 0)
				$has = TRUE;
		}
		return $has;
	}
	
	
	/**
	 * Creates one relation
	 *
	 * @param 	array		User 1
	 * @param 	array		User 2
	 *
	 *
	 */
	public function createRelation($user1, $user2, $id_relation_type=0)
	{
		$connection = $this->getWriteConnection();

		$date_creation = date("Y-m-d H:i:s");
	
		$data1 = array(
			'id_user1' => $user1['id_user'],
			'id_user2' => $user2['id_user'],
			'direction' => 1,
			'status' => 1,
			'id_relation_type' => $id_relation_type,
			'date_creation' => $date_creation,
			'date_answer' => '0000-00-00',
			'date_delete' => '0000-00-00',
			'user_delete' => NULL,
			'id_relation_message' => NULL,
			'hash' => $this->_getHash($user1,$user2,$date_creation)
		);

		$data2 = $data1;
		$data2['id_user1'] = $user2['id_user'];
		$data2['id_user2'] = $user1['id_user'];
		$data2['direction'] = 2;
		$data2['id_relation_type'] = $this->getMirrorTypeId($id_relation_type);

		try
		{
			$connection->beginTransaction();
	
			$connection->insert($this->relation_table, $data1);
			$connection->insert($this->relation_table, $data2);
			$connection->commit();
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}
	

	/**
	 * Validates one relation between 2 users
	 * Returns TRUE if the relation exists and was validated
	 *
	 * @param 	array		User 1 : Accepting the invitation
	 * @param 	array		User 2 :
	 *
	 */
	public function validateRelation($user1, $user2, $hash = NULL)
	{
		// 1. check if relation
		if ( ! $this->_getRelation($user1, $user2, self::RELATION_STATUS_INVITED, $hash))
			return FALSE;
				
		$connection = $this->getWriteConnection();

		// 2. Validate the relation		
		try
		{
			$where1 = "id_user1 = " . $user2['id_user'] . " and id_user2 = " . $user1['id_user'];
			if ( !is_null($hash))
				$where1 .= " and hash = '" . $hash . "'";
			
			$data1 = array(
				'status' => 2,
				'date_answer' => date("Y-m-d H:i:s")
			);
	
			$where2 = "id_user1 = " . $user1['id_user'] . " and id_user2 = " . $user2['id_user'];
			if ( !is_null($hash))
				$where2 .= " and hash= '" . $hash . "'";

			$data2 = array(
				'status' => 2,
				'date_answer' => date("Y-m-d H:i:s")
			);
	
			$connection->beginTransaction();
	
			$connection->update($this->relation_table, $data1, $where1);
			$connection->update($this->relation_table, $data2, $where2);
	
			$connection->commit();
	
			return TRUE;
		}
		catch (Exception $e)
		{
		    Mage::log(print_r($e->getMessage(), true), null, 'partikule.log', true);


			$connection->rollBack();
			return FALSE;
		}
	}
	
	
	public function removeRelation($user1, $user2, $hash = NULL, $action = NULL)
	{
		// 1. check if relation
		$relation = $this->_getRelation($user1, $user2, array(self::RELATION_STATUS_INVITED, self::RELATION_STATUS_ACCEPTED));
		if ( ! $relation)
			return FALSE;

		$connection = $this->getWriteConnection();
		
		// 2. Remove the relation		
		try
		{
			// Historize the previous relation
			$this->historizeRelation($user1, $user2, $relation, $action);
			
			// Delete the relation
			$where1 = "id_user1 = " . $user2['id_user'] . " and id_user2 = " . $user1['id_user'];
			if ( !is_null($hash))
				$where1 .= " and hash = '" . $hash . "'";
			
			$where2 = "id_user1 = " . $user1['id_user'] . " and id_user2 = " . $user2['id_user'];
			if ( !is_null($hash))
				$where2 .= " and hash= '" . $hash . "'";

			$connection->beginTransaction();
	
			$connection->delete($this->relation_table, $where1);
			$connection->delete($this->relation_table, $where2);
	
			$connection->commit();
	
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
		
	}
	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function changeRelationLink($user1, $user2, $id_relation_type)
	{
		// 1. check if relation
		$relation = $this->_getRelation($user1, $user2, array(self::RELATION_STATUS_INVITED, self::RELATION_STATUS_ACCEPTED));
		if ( ! $relation)
			return FALSE;

		$connection = $this->getWriteConnection();

		// 2. Change the relation type		
		try
		{
			// Historize the previous relation
			$this->historizeRelation($user1, $user2, $relation);
		
			// Mage::log('Pour ' . $user1['firstname'] .', '. $user2['lastname'] .' est : ' . $id_relation_type, null, 'partikule.log');

			$where1 = "id_user1 = " . $user1['id_user'] . " and id_user2 = " . $user2['id_user'];
			$data1 = array(
				'id_relation_type' => $id_relation_type
			);

			$where2 = "id_user1 = " . $user2['id_user'] . " and id_user2 = " . $user1['id_user'];
			$data2 = array(
				'id_relation_type' => $this->getMirrorTypeId($id_relation_type)
			);

			$connection->beginTransaction();

			$connection->update($this->relation_table, $data1, $where1);
			$connection->update($this->relation_table, $data2, $where2);

			$connection->commit();
			
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}


	/**
	 * Save the relation before changing / deleting to the relation_histo table
	 *
	 * @param 	array 		User 1
	 * @param 	array 		User 2
	 * @param 	array 		Relation
	 * @param 	string 		'delete', 'refuse'
	 *
	 * @return 	boolean		TRUE / FALSE
	 *
	 */
	public function historizeRelation($user1, $user2, $relation, $action = NULL)
	{
		$connection = $this->getWriteConnection();

		$date_delete = date("Y-m-d H:i:s");
		
		// Only 2 actions for the moment
		$status = $relation['status'];
		if ( ! is_null($action))
			$status = ($action == 'refuse') ? 3 : 4;
	
		$data1 = array(
			'id_user1' => $user1['id_user'],
			'id_user2' => $user2['id_user'],
			'direction' => 1,
			'status' => $status,
			'id_relation_type' => $relation['id_relation_type'],
			'date_creation' => $relation['date_creation'],
			'date_answer' => $relation['date_answer'],
			'date_delete' => $date_delete,
			'user_delete' => $user1['id_user'],
			'id_relation_message' => NULL,
			'hash' => $relation['hash']
		);

		$data2 = $data1;
		$data2['id_user1'] = $user2['id_user'];
		$data2['id_user2'] = $user1['id_user'];
		$data2['direction'] = 2;
		$data2['id_relation_type'] = $this->getMirrorTypeId($relation['id_relation_type']);

		try
		{
			$connection->beginTransaction();
	
			$connection->insert($this->relation_histo_table, $data1);
			$connection->insert($this->relation_histo_table, $data2);
			$connection->commit();
			
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}

	
	
	
	/**
	 * Gets the mirror relation type ID
	 * 
	 * @param 	int 		Relation type ID
	 *
	 * @return 	int		Mirror relation type or the same relation type ID if no one is found
	 *
	 */
	public function getMirrorTypeId($id_relation_type)
	{
		$select = $this->getReadConnection()->select()
			->from( array('rt' => $this->relation_type_table))
			->where('id_relation_type = ?', $id_relation_type);
		;

		$query = $select->query();
		$rt = $query->fetch();
		
		if ( ! is_null($rt['id_mirror']))
			return $rt['id_mirror'];
		
		return $id_relation_type;
	}
	
	
	/**
	 * Creates one user
	 *
	 * @param 	array		POST data
	 *
	 */
	public function createUser($post)
	{
		if ( empty($post['email']))
			return FALSE;
		
		if ( ! $this->getUserFromEmail($post['email']))
		{
			$connection = $this->getWriteConnection();
		
			$data = array(
				'id_user' => '',
				'id_group' => 6,
				'customer_status' => 1,
				'join_date' => date("Y-m-d H:i:s"),
				'firstname' => $post['firstname'],
				'lastname' => $post['lastname'],
				'email' => $post['email'],
				'gender' => ( ! empty($post['gender'])) ? $post['gender'] : '',
				'id_sponsor' => ( ! empty($post['id_sponsor'])) ? $post['id_sponsor'] : NULL,
			);
			
			if ( ! empty($post['origin']))
				$data['origin'] = $post['origin'];

			$connection->beginTransaction();
	
			$connection->insert($this->users_table, $data);
			$connection->commit();
			
			return $data;
		}
		return FALSE;
	}
	
	
	/**
	 * Creates one user from one Facebook invitation
	 * If the user already exists, returns the existing user
	 *
	 * @param 	array		Friend data
	 *
	 * @return 	array		Friend's array
	 *
	 */
	public function createUserFromFacebookFriend($friend)
	{
		// Get the facebook ID from DB
		$facebook_id = $this->getFacebookUserId($friend);

		$user = $this->getUserFrom(array('facebook_id' => $facebook_id));

		// Creates the user if he doesn't exists
		if ( ! $user )
			$user = $this->_insertFacebookUser($friend);
		
		return $user;
	}
	
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getContacts()
	{
		$contacts = array();
		
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = $this->getUserFromEmail($currentUser->getEmail());

		if ($currentNoseUser)
		{
			/*
			 * Get contacts list.
			 * We suppose each contact has 0 or 1 diagnostic in input_attributes_current
			 * but... this can also return more than one line for each contact
			 * so we use GROUP CONCAT as the important thing is only to know if the user has / has not 
			 * one diag.
			 * The diag ID doesn't mater in this case.
			 *
			 */
			$select = $this->getReadConnection()->select()
				->distinct()
				->from(array('u' => $this->users_table))
				->join(array('r' => $this->relation_table),
					'u.id_user = r.id_user2'
				)
	
				->joinLeft(array('d' => $this->diagnostic_table),
					'u.id_user = d.id_user AND d.model_id = 1',
					array('id_diag' => new Zend_Db_Expr('max(d.id_diag)')) 
				)
				->joinLeft(array('d2' => $this->diagnostic_table),
					'u.id_user = d2.id_user AND d2.model_id = 3',
					array('id_love_diag' => new Zend_Db_Expr('max(d2.id_diag)')) 
				)
	
				/*
				->joinLeft(array('iac' => $this->iac_table),
					'iac.id_user = r.id_user2 AND iac.model_id = 1',
					array('id_diag' => new Zend_Db_Expr('GROUP_CONCAT( distinct iac.id_diag)')) 
				)
				*/
				->where('r.id_user1 = ?', $currentNoseUser['id_user'])
				// ->where('r.status < ?', '4')
				->group('u.id_user')
				->order('u.firstname')
			;
			// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');		
			// trace("{$select}".PHP_EOL);
	
			$query = $select->query();
			$contacts = $query->fetchall();
		}
		
		return $contacts;
	}
	
	
    /**
     * Returns contacts who have at least one address
     * Returns limited array
     *
     */
    public function getContactsWithAddress()
    {
    	// Array of returned contacts
    	$contactList = array();
    	
    	$contacts = $this->getContacts();
    	
    	foreach($contacts as $contact)
    	{
			$customer_id = $contact['magento_id'];
			$customer = Mage::getModel('customer/customer')->load($customer_id);
			
			foreach ($customer->getAddresses() as $address)
			{
				$ad = $address->toArray();

				if ($ad['is_active'] == 1)
				{
					/*
					$c = array(
						'id_user' => $contact['id_user'],
						'firstname' => $contact['firstname'],
						'lastname' => $contact['lastname'],
						'magento_id' => $contact['magento_id'],
						'email' => $contact['email'],
						'address'=> $ad
					);
					*/
					
					// Even it is encoded
					unset($contact['password']);

					// Correct street to fit the Form elements ID (street1, street2, etc.)
					$streets = explode("\n", $ad['street']);
					foreach($streets as $k=>$v)
					{
						$ad['street' . ($k+1)] = $v;
					}
					
					$contact['address'] = $ad;
					$contactList[] = $contact;
					
					break;
				}
			}
    	}

		return $contactList;
    }
	
	
	
	// Safely clear the Invitation session
	public function cleanInvitationSession()
	{
		Mage::getSingleton('core/session')->unsInvitedUserNoseId();
		Mage::getSingleton('core/session')->unsSenderUserNoseId();
		Mage::getSingleton('core/session')->unsInvitationHash();
		Mage::getSingleton('core/session')->unsRelation();
	}

	
	/**
	 * Returns on Facebook user's ID
	 *
	 */
	public function getFacebookUserId($user)
	{
		$facebook_id = ( ! empty($user['uid'])) ? $user['uid'] : $user['id'];

		return $facebook_id;	
	}
	
	protected function _getFacebookUserGender($user)
	{
		$gender = '';
		
		$idx = ( isset($user['gender']) ) ? 'gender' : 'sex';
		
		if ( ! empty($user[$idx]))
			$gender = ($user[$idx] == 'male') ? 1 : 2;
		
		return $gender;	
	}
	
	protected function _getFacebookUserBirthday($user)
	{
		$birthday = '';
		
		if (isset($user['birthday']))
		{
			$a = explode('/', $user['birthday']);
			$birthday = $a[2] . '-' . $a[0] . '-' . $a[1];
		}		
		return $birthday;	
	}
	
	/**
	 * Inserts one Facebook user into the Ionize users table
	 *
	 */
	protected function _insertFacebookUser($facebook_user)
	{
		$data = array(
			'id_group' => $this->default_user_group,
			'join_date' => date("Y-m-d H:i:s"),
			'firstname' => $facebook_user['first_name'],	
			'lastname' => $facebook_user['last_name'],	
			'email' => $facebook_user['email'],	
			'facebook_id' => $this->_getFacebookUserId($facebook_user),
			'gender' => $this->getFacebookUserId($facebook_user),
			'origin' => self::FACEBOOK_JOIN_FROM,
			'id_sponsor' => ! empty($facebook_user['id_sponsor']) ? $facebook_user['id_sponsor'] : NULL
		);

		$connection = $this->getWriteConnection();
		$connection->beginTransaction();

		$connection->insert($this->users_table, $data);
		$connection->commit();

		$data['id_user'] = $connection->lastInsertId();

		if ($data['id_user'])
			return $data;
		
		return FALSE;
	}
	
	
	/**
	 * Gets one relation between 2 users
	 *
	 * @param	mixed	Status code to filter on or array of status to filter on
	 * @return	mixed	Array of the relation / FALSE if tno matching relation found.
	 *
	 */
	protected function _getRelation($user1, $user2, $status = NULL, $hash = NULL)
	{
		$relation = FALSE;
		
		if ($user1['id_user'] && $user2['id_user'])
		{
			// 1. check if the users are potentially linked
			$select = $this->getReadConnection()->select()
				->from( array('r' => $this->relation_table))
				->where('id_user1 = ?', $user1['id_user'])
				->where('id_user2 = ?', $user2['id_user'])
			;
	
			if ( !is_null($hash))
				$select->where('hash = ?', $hash);
			
			if ( !is_null($status))
			{
				if ( ! is_array($status))
				{
					$select->where('status = ?', $status);
				}
				else
				{
					foreach($status as $key => $stat)
					{
						if ($key == 0)
							$select->where('status = ?', $stat);
						else
							$select->orwhere('status = ?', $stat);
					}
				}
			}
			// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');			
			$query = $select->query();
			$relation = $query->fetch();
		}
		
		return $relation;
	}
	
	

	protected function _getHash($user1, $user2, $date = NULL)
	{
		if (is_null($date))
			$date = $user1['date_creation'];
		
		return md5($user1['email'].$user2['email'].$date);
	}


	/**
	 *
	 * Returns list of uniq relations IDs
	 *
	 * @usage
	 *
	 */
	public function _getUniqueRelationIds()
	{
		if (is_null($this->_unique_relation_type_ids))
		{
			$select = $this->getReadConnection()->select()
				->from( array('rt' => $this->relation_type_table),
						array('ids' => new Zend_Db_Expr('group_concat(id_relation_type)'))
					  )
				->where('rt.uniq = (?)', '1')
			;
			
			$query = $select->query();
			$ids = $query->fetch();
	
			if ( ! empty($ids))
				$this->_unique_relation_type_ids = $ids['ids'];
		}
		
		return $this->_unique_relation_type_ids;
	}


	
/*
	public function getUser($conds)
	{
		$select = $this->getReadConnection()->select()
			->from( array('u' => $this->users_table))
		;
		
		foreach ($conds as $field => $value)
		{
			$select->where($field . ' = ?', $value);
		}
		$query = $select->query();

		$user = $query->fetch();
		
		
	}
*/
	
}