<?php

/**
 *
 * Creates / Updates Users account in Nose
 * 
 *
 */
class Partikule_Mynose_Model_User extends Partikule_Mynose_Model_Abstract
{
	// ENose "Guest" Group
	protected $_group = '6';
	
	// Nose Users cache
	protected $_noseUsers = array();
	
	protected $_contactModel = NULL;


	const STORE_ID = 		1;
	const DISTRIBUTOR_ID = 	1;


	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');
		
		$this->table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
		$this->users_coupons_table = Mage::getSingleton('core/resource')->getTableName('mynose/users_coupons');
	}

	
	
	/**
	 * Called on User save
	 * See : PArtikule/MyNose/etc/config.xml
	 *
	 * @param	Object	Magento User
	 *
	 */
	public function updateNoseAccount($user)
	{
		// Get User from eNose DB
		$eNoseUser = $this->getNoseUser($user);

		// Update
		if ( ! empty($eNoseUser))
		{
			$this->updateNoseUser($user, $eNoseUser);
			
			/*
			 * Only in case the user is new and was invited by a registered user
			 * Means : 
			 * 1. The user has one eNose account by not Magento ID
			 *
			 */
			$invitedNoseId = Mage::getSingleton('core/session')->getInvitedUserNoseId();
			$senderNoseId = Mage::getSingleton('core/session')->getSenderUserNoseId();
			$hash = Mage::getSingleton('core/session')->getInvitationHash();
			
			if ($invitedNoseId && $senderNoseId && $hash)
			{
				$invitedNoseUser = $this->_getContactModel()->getUserFromId($invitedNoseId);
				$senderNoseUser = $this->_getContactModel()->getUserFromId($senderNoseId);
				
				// Check if the invited user is really the same than the Magento user
				if ($invitedNoseUser['magento_id'] == $user->getId())
				{
					$result = $this->_getContactModel()->validateRelation($senderNoseUser, $invitedNoseUser, $hash);
					if ($result)
					{
						// Mage::helper('mynose/email')->sendInvitationAcceptedMail($senderNoseUser, $invitedNoseUser);
					}
				}
			}

			// Clean Invitation session
			Mage::getModel('mynose/contact')->cleanInvitationSession();
		}
		// Create
		else
		{
			$id_user = $this->createNoseUser($user);
			// Mage::log('$id_user : ' . $id_user, null, 'partikule.log');
		}
	}
	
	
	/**
	 * Creates one user in the ENose DB
	 * @param $user
	 *
	 * @return string	Inserted User ID
	 */
	public function createNoseUser($user)
	{
		$connection = $this->getWriteConnection();

		// Set default store_id and distributor_id.
		// Do we have the choice ?
		if ( ! $user->getEnoseStoreId())
			$user->setEnoseStoreId(self::STORE_ID);

		if ( ! $user->getEnoseDistributorId())
			$user->setEnoseDistributorId(self::DISTRIBUTOR_ID);

		if ( ! $user->getIdOrigin())
			$user->setIdOrigin(1);

		$birth_date = ($user->getDob()) ? $user->getDob() : '0000-00-00';

		// Address from user
		$address = array(
			'country_iso2' => $user->getMy_country(),
			'city' => $user->getCity(),
			'postcode' => $user->getPostcode(),
			'street' => $user->getStreet(),
		);

		// Address from Billing Address
		$billingAddress = $user->getDefaultBillingAddress();
		if ($billingAddress)
		{
			$address = array(
				'country_iso2' => $billingAddress->getCountryId(),
				'city' => $billingAddress->getCity(),
				'postcode' => $billingAddress->getPostcode(),
				'street' => implode("\n", $billingAddress->getStreet()),
			);
		}

		$data = array_merge(array(
			'id_group' => $this->_group,
			'username' => $user->getEmail(),
			'email' => $user->getEmail(),
			'join_date' => $user->getCreated_at(),
			'firstname' => $user->getFirstname(),
			'lastname' => $user->getLastname(),
			'gender' => $user->getGender(),
			'birth_date' => $birth_date,
			'age' => $this->getAge($birth_date),
			'country_iso2' => $user->getMy_country(),
			'postcode' => $user->getMy_zip(),
			'city' => $user->getMy_city(),
			'origin' => $user->getIdOrigin(),
			'customer_status' => $user->getCustomerStatus(),
			'magento_id' => $user->getId(),
			'store_id' => $user->getEnoseStoreId(),
			'distributor_id' => $user->getEnoseDistributorId(),
			'favorite_lang' => Mage::app()->getStore($user->getStoreId())->getCode(),
			'login_hash' => $user->getLoginHash(),
		), $address);

		// Mage::log(print_r($user->getData(), TRUE), NULL, 'partikule.log');
		// Mage::log($data, NULL, 'partikule.log');

		$connection->beginTransaction();
		$connection->insert($this->table, $data);
		$connection->commit();

		// Returns 0...
		$id_inserted = $connection->lastInsertId();

		// Link the user to Store and Distributor
		// Get User from eNose DB
		$eNoseUser = $this->getNoseUser($user);

		if ( ! empty($eNoseUser))
		{
			if ( FALSE == $this->isLinkedToDistributor($eNoseUser))
				$this->linkToDistributor($eNoseUser);

			if ( FALSE == $this->isLinkedToStore($eNoseUser))
				$this->linkToStore($eNoseUser);

			return $eNoseUser['id_user'];
		}

		return $id_inserted;
	}


	/**
	 * Updates custom data of the eNose DB User
	 *
	 * @param $eNoseUser
	 * @param $data
	 *
	 */
	public function updateNoseUserData($eNoseUser, $data=array())
	{
		$connection = $this->getWriteConnection();

		if ( !empty($data))
		{
			$connection->beginTransaction();

			// Update based on external ID
			$where = "id_user = " . $eNoseUser['id_user'];

			$connection->update($this->table, $data, $where);
			$connection->commit();
		}
	}



	
	/**
	 * Updates one user in the ENose DB
	 *
	 */
	public function updateNoseUser($user, $eNoseUser)
	{
		if( $this->validateUserEmail($user) == TRUE )
		{
			$connection = $this->getWriteConnection();
		
			$birth_date = ($user->getDob()) ? $user->getDob() : '0000-00-00';

			$address = array();

			// Address from Billing Address
			$billingAddress = $user->getDefaultBillingAddress();
			if ($billingAddress)
			{
				$address = array(
					'country_iso2' => $billingAddress->getCountryId(),
					'city' => $billingAddress->getCity(),
					'postcode' => $billingAddress->getPostcode(),
					'street' => implode("\n", $billingAddress->getStreet()),
				);
			}

			$data = array_merge(array(
				'email' => $user->getEmail(),
//				'username' => $user->getEmail(),
				'firstname' => $user->getFirstname(),
				'lastname' => $user->getLastname(),
				'gender' => $user->getGender(),
				'age' => $this->getAge($birth_date),
				'country_iso2' => $user->getMy_country(),
				'postcode' => $user->getMy_zip(),
				'city' => $user->getMy_city(),
				'birth_date' => $birth_date,
				'customer_status' => $user->getCustomerStatus(),
				'magento_id' => $user->getId(),
				'favorite_lang' => Mage::app()->getStore($user->getStoreId())->getCode(),
				'login_hash' => $user->getLoginHash(),
			), $address);

			$connection->beginTransaction();

			// Update based on external ID
			$where = "id_user = " . $eNoseUser['id_user'];

			$connection->update($this->table, $data, $where);
			$connection->commit();
		}
	}
	
	
	/**
	 * Checks if another user doesn't exists in Magento with the same new Email
	 *
	 * @param $user
	 *
	 * @return bool		TRUE if the email is valid, FALSE if another user already has the same email
	 *
	 */
	public function validateUserEmail($user)
	{
		$mageUser = Mage::getModel('customer/Customer')->loadByEmail($user->getEmail());
		
		if ($mageUser->hasData() && ($mageUser->getId() != $user->getId()))
		{
			return FALSE;
		}
		
		return TRUE;
	}


	public function isLinkedToDistributor($eNoseUser)
	{
		if ( ! empty($eNoseUser))
		{
			$select = $this->getReadConnection()->select()
				->from( array('lk' => 'lk_user_distributor'))
				->where('id_user = ?', $eNoseUser['id_user'])
				->where('distributor_id = ?', $eNoseUser['distributor_id'])
			;

			$query = $select->query();
			$db_user = $query->fetch();

			// If we don't find the user based on its Magento ID, we try with the email :
			if ( ! empty($db_user))
				return TRUE;
		}

		return FALSE;
	}


	public function isLinkedToStore($eNoseUser)
	{
		if ( ! empty($eNoseUser))
		{
			$select = $this->getReadConnection()->select()
				->from( array('lk' => 'lk_user_store'))
				->where('id_user = ?', $eNoseUser['id_user'])
				->where('store_id = ?', $eNoseUser['store_id'])
			;

			$query = $select->query();
			$db_user = $query->fetch();

			// If we don't find the user based on its Magento ID, we try with the email :
			if ( ! empty($db_user))
				return TRUE;
		}

		return FALSE;
	}


	public function linkToDistributor($eNoseUser)
	{
		$connection = $this->getWriteConnection();

		$data = array(
			'id_user' => $eNoseUser['id_user'],
			'distributor_id' => $eNoseUser['distributor_id'],
			'join_date' => date('Y-m-d H:i:s'),
		);

		$connection->beginTransaction();
		$connection->insert('lk_user_distributor', $data);
		$connection->commit();
	}

	public function linkToStore($eNoseUser)
	{
		$connection = $this->getWriteConnection();

		$data = array(
			'id_user' => $eNoseUser['id_user'],
			'store_id' => $eNoseUser['store_id'],
			'join_date' => date('Y-m-d H:i:s'),
		);

		$connection->beginTransaction();
		$connection->insert('lk_user_store', $data);
		$connection->commit();
	}


	/**
	 * Gets one user from the eNose DB
	 * @param $user		Object. Magento Customer
	 *
	 * @return array|mixed
	 */
	public function getNoseUser($user)
	{
		$db_user = array();

		if ($user->hasData())
		{
			$select = NULL;
		
			// If one Nose User ID is saved in session, get 
			$tempNoseId = Mage::getSingleton('core/session')->getInvitedUserNoseId();
			if ($tempNoseId)
			{
				// Mage::log('getNoseUser() : NOSE ID : ' . $tempNoseId, null, 'partikule.log');
				$select = $this->getReadConnection()->select()
					->from( array('u' => $this->table))
					->where('id_user = ?', $tempNoseId)
				;	
			}
			else
			{
				$select = $this->getReadConnection()->select()
					->from( array('u' => $this->table))
					->where('magento_id = ?', $user->getID())
				;	
			}

			// Mage::log("{$select}".PHP_EOL, 'partikule.log');

			$query = $select->query();
			$db_user = $query->fetch();
			
			// If we don't find the user based on its Magento ID, we try with the email :
			if ( ! $db_user)
			{
				$select = $this->getReadConnection()->select()
					->from( array('u' => $this->table))
					->where('email = ?', $user->getEmail())
				;			

				$query = $select->query();
				$db_user = $query->fetch();
			}
		}
		
		return $db_user;		
	}


	public function getNoseUserFromCustomer($user)
	{
		$db_user = array();

		if ($user->hasData())
		{
			$select = $this->getReadConnection()->select()
				->from( array('u' => $this->table))
				->where('magento_id = ?', $user->getID())
			;

			$query = $select->query();
			$db_user = $query->fetch();

			// If we don't find the user based on its Magento ID, we try with the email :
			if ( ! $db_user)
			{
				$select = $this->getReadConnection()->select()
					->from( array('u' => $this->table))
					->where('email = ?', $user->getEmail())
				;

				$query = $select->query();
				$db_user = $query->fetch();
			}
		}

		return $db_user;
	}

	
	/**
	 * Gets one user from the eNose DB
	 *
	 * @param	int		Nose user ID
	 *
	 */
	public function getNoseUserFromId($id_user)
	{
		if (empty($this->_noseUsers[$id_user]))
		{
			$select = $this->getReadConnection()->select()
				->from( array('u' => $this->table))
				->where('id_user = ?', $id_user);
			;
			
			// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');		
			
			$query = $select->query();
			$this->_noseUsers[$id_user] = $query->fetch();
		}

		return $this->_noseUsers[$id_user];
	}
	

	/**
	 * Gets one user from the eNose DB
	 * @param $customer_id		int. Magento customer ID
	 *
	 * @return mixed
	 */
	public function getNoseUserFromMagentoId($customer_id)
	{
		$select = $this->getReadConnection()->select()
			->from( array('u' => $this->table))
			->where('magento_id = ?', $customer_id);
		;

		$query = $select->query();
		$result = $query->fetch();

		return $result;
	}


    public function getNoseUserCouponsFromCustomer($customer)
    {
        $result = array();

        $noseUser = $this->getNoseUserFromCustomer($customer);

        if (! empty($noseUser))
        {
            return $this->getNoseUserCoupons($noseUser['id_user']);
        }

        return $result;
    }


	public function getNoseUserCoupons($id_user)
    {
        $select = $this->getReadConnection()->select()
            ->from( array('u' => $this->users_coupons_table))
            ->where('id_user = ?', $id_user);
        ;

        $query = $select->query();
        $result = $query->fetchAll();

        return $result;
    }


    public function insertNoseUserCoupon($data = array())
    {
        $connection = $this->getWriteConnection();

        if ( !empty($data))
        {
            $connection->beginTransaction();

            $connection->insert($this->users_coupons_table, $data);
            $connection->commit();
        }
    }

    public function insertNoseUserCouponFromOrder($id_user, $coupon_code, $amount = 0, $source=NULL, $id_source=NULL, $id_user_source=NULL)
    {
        $nose_user_coupon = array(
            'id_user' => $id_user,
            'id_user_source' => $id_user_source,
            'dt_creation' => date('Y-m-d H:i:s'),
            'source' => $source,
            'id_source' => $id_source,
            'coupon' => $coupon_code,
            'amount' => $amount,
        );

        $this->insertNoseUserCoupon($nose_user_coupon);
    }


	/**
	 * Returns user's age attributes from 'tranche_age' table
	 *
	 * @param 	User 		
	 *
	 *
	 */
	public function getAgeAttributeData($user)
	{
		$age = $this->getAge($user->getDob());
		
		$select = $this->getReadConnection()->select()
			->from( array('t' => 'tranche_age'))
			->where('age_min <= ?', $age)
			->where('age_max >= ?', $age)
		;	

		$query = $select->query();

		$result = $query->fetchAll();

		return $result;
	}
	
	
	public function getAge($birthdate)
	{
		list($Y,$m,$d)    = explode("-",$birthdate);
		return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}


	/**
	 * Get the city size
	 * @TODO: Implement this. Not deifned at this time
	 *
	 */
	public function getCitySize()
	{
		return 0;		
	}


	public function getBillingAddress($user)
	{
		// $user->getId();
		$addresses = $user->getAddressesCollection();

		foreach($addresses as $address)
		{
			// Mage::log($address->getData(), null, 'partikule.log');
		}

	}


	private function _getContactModel()
	{
		if ( is_null($this->_contactModel))
			$this->_contactModel = Mage::getModel('mynose/contact');
		
		return $this->_contactModel;
	}


}