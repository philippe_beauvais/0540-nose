<?php

/**
 * Customer Model
 *
 *
 */
class Partikule_Mynose_Model_Customer extends Partikule_Mynose_Model_Abstract
{
	private static $_DIAGNOSTIC_PACK_SKU = 'D-Diagnostic-Pack';

	private static $_diag = NULL;

	private static $_BUNDLE_KIT_ID = NULL;

	public function __construct()
	{
		parent::__construct();

		self::$_BUNDLE_KIT_ID =  Mage::getStoreConfig('nose/general/bundle_product_id');

		$this->setConnection('mynose_read', 'mynose_write');

		$this->users_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
		$this->diagnostic_table =  Mage::getSingleton('core/resource')->getTableName('mynose/diagnostic');
	}

	/**
	 * Returns TRUE if the customer has already bought his last diag Sample Kit
	 *
	 * @param $customer
	 * @return bool
	 */
	public function hasBoughtLastDiagSampleKit($customer)
	{
		$has_bought = FALSE;

		$diag = $this->_get_current_diag($customer);

		$masterIds = array_unique(array_map(
			function($diag) {
				return $diag['master_id'];
			},
			$diag
		));

		// Customer's Order IDs
		$orders = Mage::getResourceModel('sales/order_collection')
			->addFieldToSelect('*')
			->addFieldToFilter('customer_id', $customer->getId())
			->addAttributeToSort('created_at', 'DESC')
			->toArray(array('entity_id'));
		;

		$orderIds = array_unique(array_map(
			function($orderItem) {
				return $orderItem['entity_id'];
			},
			$orders['items']
		));

		// Contains one order one bundle ?
		// Get orders ID from Orders items
		$orders = array();
		$collection = Mage::getResourceModel('sales/order_item_collection')
			->addAttributeToFilter('sku', array('eq' => self::$_DIAGNOSTIC_PACK_SKU))
			->addFieldToFilter('order_id', array('in' => array(implode(',', $orderIds))))
			->load();

		// Each Diagnostic Sample Kit item
		foreach($collection as $orderItem)
		{
			$orders[$orderItem->getOrder()->getIncrementId()] = $orderItem->getOrder();
		}

		if ( ! empty($orders))
		{
			foreach($orders as $order)
			{
				foreach($order->getAllItems() as $item)
				{
					$bundle_master_ids = $this->_get_bundle_kit_master_ids($item);

					$diff = array_diff($masterIds, $bundle_master_ids);

					if (empty($diff))
						$has_bought = TRUE;
				}
			}
		}
		return $has_bought;
	}


	/**
	 * @todo : Rewrite, does not work using _get_bundle_kit_master_ids()
	 * @param $customer
	 *
	 * @return bool
	 */
	public function isLastDiagSampleKitInCart($customer)
	{
		$is_in_cart = FALSE;

		// Current Diag Master IDs
		$diag = $this->_get_current_diag($customer);

		$masterIds = array_unique(array_map(
			function($diag) {
				return $diag['master_id'];
			},
			$diag
		));

		// Cart
		$quote = Mage::helper("checkout/cart")->getQuote();
		// $_cart_items = $quote->getAllItems();
		$items = $quote->getAllItems();

		if ($items)
		{
			foreach($items as $item)
			{
				$_product = $item->getProduct();

				if ($_product && $_product->getId() == self::$_BUNDLE_KIT_ID)
				{
					$bundle_master_ids = $this->_get_bundle_kit_master_ids($item);

					$diff = array_diff($masterIds, $bundle_master_ids);

					if (empty($diff))
						$is_in_cart = TRUE;

					break;
				}
			}
		}

		return $is_in_cart;
	}

	private function _get_current_diag($customer)
	{
		if (is_null(self::$_diag))
		{
			self::$_diag = Mage::getModel('mynose/diagnostic')->getCustomerMasterDiag($customer);
		}
		return self::$_diag;
	}

	private function _get_bundle_kit_master_ids($product)
	{
		$master_ids = array();
		$options = $product->getProductOptions();
		$optionIds = array_keys($options['info_buyRequest']['bundle_option']);

		$types = Mage_Catalog_Model_Product_Type::getTypes();
		$typemodel = Mage::getSingleton($types[Mage_Catalog_Model_Product_Type::TYPE_BUNDLE]['model']);
		$typemodel->setConfig($types[Mage_Catalog_Model_Product_Type::TYPE_BUNDLE]);
		$selections = $typemodel->getSelectionsCollection($optionIds, $product);
		$selection_map = array();
		foreach($selections->getData() as $selection) {
			if(!isset($selection_map[$selection['option_id']])) {
				$selection_map[$selection['option_id']] = array();
			}
			$selection_map[$selection['option_id']][$selection['selection_id']] = $selection;
		}

		// Use of SKUs to get the master IDs
		$chosen_skus = array();
		foreach($options['info_buyRequest']['bundle_option'] as $op => $sel) {
			$chosen_skus[] = $selection_map[$op][$sel]['sku'];
		}

		foreach($chosen_skus as $sku)
		{
			$arr = explode('-', $sku);
			$master_ids[] = $arr[1];
		}

		return $master_ids;
	}
}