<?php

/**
 *
 * 
 * 
 *
 */
class Partikule_Mynose_Model_Calendar extends Partikule_Mynose_Model_Abstract
{
	const RELATION_STATUS_ACCEPTED = 2;

	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');
		
		$this->event_table = Mage::getSingleton('core/resource')->getTableName('mynose/event');
		$this->event_lang_table = Mage::getSingleton('core/resource')->getTableName('mynose/event_lang');
		$this->users_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
		$this->relation_table = Mage::getSingleton('core/resource')->getTableName('mynose/relation');
		$this->diagnostic_table = Mage::getSingleton('core/resource')->getTableName('mynose/diagnostic');
	}


	/**
	 * Returns the next calendar's events
	 *
	 * Mix of "official" events and "birthdays"
	 *
	 */
	public function getNextCalendarEvents()
	{
		$result = FALSE;
		
		// String of contact IDs, coma separated
		$contacts_ids = $this->getUserContactsIds();

		$sql = "
			SELECT 
				'official' as 'event_type',
				e.event_code, 
				e.event_name, 
				NULL as 'user_name',
				e.event_date as 'date',
				NULL as age,
				NULL as id_diag,
				NULL as diag_date,
				NULL as id_user,
				sex_id,
				ids_relation_type,
				FLOOR((UNIX_TIMESTAMP(CONCAT(((RIGHT(e.event_date, 5) < RIGHT(CURRENT_DATE, 5)) + YEAR(CURRENT_DATE)), RIGHT(e.event_date, 6))) - UNIX_TIMESTAMP(CURRENT_DATE)) / 86400) AS upcoming_days
			from event e
			where year(e.event_date) >= year(CURRENT_DATE) OR e.year = '0000'
		";				
		
		if ($contacts_ids)
		{
			$sql .= " union
				SELECT 
					'personal' as 'event_type',
					'birthday' as 'event_code',
					'Birthday' as 'event_name',
					concat(u.firstname, ' ', u.lastname) as 'user_name', 
					u.birth_date as 'date',
					(YEAR(CURDATE())-YEAR(birth_date)) - (RIGHT(CURDATE(),5)<RIGHT(birth_date,5)) +1 AS age,
					d.id_diag AS id_diag,
					d.diag_dt AS diag_date,
					u.id_user,
					NULL as sex_id,
					NULL as ids_relation_type,
					FLOOR((UNIX_TIMESTAMP(CONCAT(((RIGHT(u.birth_date, 5) < RIGHT(CURRENT_DATE, 5)) + YEAR(CURRENT_DATE)), RIGHT(u.birth_date, 6))) - UNIX_TIMESTAMP(CURRENT_DATE)) / 86400) AS upcoming_days
				from users u
				left join (
					select d2.id_user, d2.id_diag , d2.diag_dt
					from diagnostic d2
					join
					(
						select d3.id_user, max(d3.id_diag) as id_diag
						from diagnostic d3
						group by d3.id_user
					) as d3 on (d3.id_user = d2.id_user and d2.id_diag = d3.id_diag)
				) as d on (d.id_user = u.id_user)
				where u.id_user in (". $contacts_ids .")
				AND (DAYOFYEAR(curdate()) <= dayofyear(u.birth_date) 
				AND DAYOFYEAR(curdate()) +350 >= dayofyear(u.birth_date) OR DAYOFYEAR(curdate()) <= dayofyear(u.birth_date)+365 
				AND DAYOFYEAR(curdate()) +350 >= dayofyear(u.birth_date)+365)
				order by upcoming_days ASC
			";
			
			
		}

		// Mage::log($sql, null, 'partikule.log');

		$query =  $this->getReadConnection()->query($sql);

		$result = $query->fetchall();

		// Mage::log($result, null, 'partikule.log');		
		
		return $result;
	}
	
	
	/**
	 * Return the string of user's contacts
	 *
	 * SELECT GROUP_CONCAT( distinct u.id_user) AS `ids` FROM `users` AS `u`
 	 * 	INNER JOIN `relation` AS `r` ON u.id_user = r.id_user2 WHERE (r.id_user1 = '1')
	 *
	 * @return 	string		IDs of contacts or FALSE if no contacts found
	 *
	 */
	public function getUserContactsIds()
	{
		$user = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$noseUser = Mage::getModel('mynose/contact')->getUserFromEmail($user->getEmail());

		$select = $this->getReadConnection()->select()
			->from(array('u' => $this->users_table),
				array('ids' => new Zend_Db_Expr('GROUP_CONCAT( distinct u.id_user)'))
			)
			->join(array('r' => $this->relation_table),
				'u.id_user = r.id_user2',
				array()
			)
			->where('r.status = ?', self::RELATION_STATUS_ACCEPTED)
			->where('r.id_user1 = ?', $noseUser['id_user'])
		;
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');		

		$query = $select->query();
		$result = $query->fetch();
		
		if ( ! empty($result))
			return $result['ids'];
		
		return FALSE;
	}
	
}