<?php

class Partikule_Mynose_Model_Test extends Partikule_Mynose_Model_Abstract
{
	// Output types
	const OUTPUT_TYPE_PERFUME = 	7;
	const OUTPUT_TYPE_HOME = 		9;
	const OUTPUT_TYPE_COSMETIC = 	10;

	
	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');
		
		$this->evaluations_table = Mage::getSingleton('core/resource')->getTableName('mynose/evaluations');
		$this->perfume_table = Mage::getSingleton('core/resource')->getTableName('mynose/perfume');
		$this->cosmetic_table = Mage::getSingleton('core/resource')->getTableName('mynose/cosmetic');
		$this->homefragrance_table = Mage::getSingleton('core/resource')->getTableName('mynose/homefragrance');
	}
	
	
	/**
	 * Test for Evaluation Open
	 *
	 */
	public function openEvaluation()
	{
		$eval_status = 2;
		$now = date('Y-m-d H:i:s');
		$connection = $this->getWriteConnection();
		$customer = Mage::getModel('customer/session');
		$customerId = $customer->getCustomerId();
		$user = Mage::getModel('mynose/user')->getNoseUser($customer);
		


		// Byredo Beaudelaire : 510
		// Vapo : 4397
		$master_ids = array('510', '1917', '4397');
		
		trace("<h1>Order's ID</h1>");
		trace($master_ids);
		
		
		$update_data = array(
			'evaluation_status' => $eval_status,
			'commande_dt' => $now
		);
		
		
		// User's Evaluation output IDs
		$query =	' SELECT group_concat(output_id) as ids FROM ' . $this->evaluations_table
					. ' WHERE id_user = ' . $user['id_user']
//					. ' AND evaluation_status < 2'
		;

		$results = $connection->fetchOne($query);				
		
		$evaluation_ids = ( ! empty($results)) ? explode(',', $results) : array();

		trace('<h1>In evaluations</h1>');
		trace($evaluation_ids);
		
		
		// Order's master ids
		$query =	' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_PERFUME . ' as output_type FROM ' . $this->perfume_table . 
					' WHERE master_id in (' . implode(",", $master_ids) . ')' .
					' union ' .
					' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_HOME . ' as output_type FROM ' . $this->homefragrance_table . 
					' WHERE master_id in (' . implode(",", $master_ids) . ')' .
					' union ' .
					' SELECT master_id as output_id, ' . self::OUTPUT_TYPE_COSMETIC . ' as output_type FROM ' . $this->cosmetic_table . 
					' WHERE master_id in (' . implode(",", $master_ids) . ')'
				;

		$masters = $connection->fetchAll($query);				
		
		
		// Compare with order's master IDs array
		foreach($masters as $master)
		{
			// UPDATE : Is in Eval IDs
			if (in_array($master['output_id'], $evaluation_ids))
			{
				$where = " id_user = " . $user['id_user'] . 
						 " AND output_id = " . $master['output_id'] .
						 " AND evaluation_status < 2
						 ";
				trace('<h1>update</h1>');
				trace($where);
				$connection->update($this->evaluations_table, $update_data, $where);
				$connection->commit();
			}
			// INSERT : Is not in eval IDs
			else
			{
				$insertData = array(
					'id_user' => 	$user['id_user'],
					'output_id' =>	$master['output_id'],
					'id_diag' => 0,
					'evaluation_status' => $eval_status,
					'commande_dt' => $now,
					'output_type' => $master['output_type']
				);
				$connection->insert($this->evaluations_table, $insertData);
				$connection->commit();
				
				trace('<h1>insert</h1>');
				trace($insertData);
				
			}
		}
		
		
		

		
		// Mage::log('$where : ' . $where, null, 'partikule.log');
		//$connection->update($this->evaluations_table, $data, $where);
		//$connection->commit();

		
	}
	
	
	
	
}