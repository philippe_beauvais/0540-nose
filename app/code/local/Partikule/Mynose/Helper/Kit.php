<?php
/**
 * Sample Kit Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Mynose_Helper_Kit extends Mage_Core_Helper_Abstract
{
	public function getOrderSampleKitUrl($customer)
	{
		$sample_url = NULL;

		$diag_bundle_id = Mage::getStoreConfig('nose/general/bundle_product_id');

		$diag = Mage::getModel('mynose/diagnostic')->getCustomerMasterDiag($customer);

		if ( ! empty($diag))
		{
			$samples = array();
			foreach($diag as $perfume)
			{
				$samples[] =  Mage::getModel('mynose/diagnostic')->getSampleFromDiagnosticItem($perfume, TRUE);
			}

			$sample_ids = array();

			foreach($samples as $sample)
				$sample_ids[] = $sample->getId();

			$sample_url = implode(',', $sample_ids);


			$sample_url = Mage::helper('checkout/cart')->getCartUrl()
				. 'bundle/bundle/' . $diag_bundle_id .'/products/' . $sample_url;
		}

		return $sample_url;
	}
}