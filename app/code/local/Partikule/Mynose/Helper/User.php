<?php
/**
 * Nose User Helper
 * 
 *
 */
class Partikule_Mynose_Helper_User extends Mage_Core_Helper_Abstract
{

	/**
	 * Fired when one user saves his account
	 *
	 */
	static function onUserSave($observer)
	{
		$event = $observer->getEvent(); 
		$user = $event->getCustomer();

		// Mage::log('onUserSave() ID : ' . $user->getId(), null, 'partikule.log');

		// User Update / Create
		if ( $user->getId() )
		{
			Mage::getModel('mynose/User')->updateNoseAccount($user);
		}
	}
	
	
	static function onUserSaveAfter($observer)
	{
		$event = $observer->getEvent();
		$user = $event->getCustomer();

		// Mage::log('onUserSaveAfter() ID : ' . $user->getId(), null, 'partikule.log');

		// User Update / Create
		if ( $user->getId() )
		{
			Mage::getModel('mynose/User')->updateNoseAccount($user);
		}
	}
	
	
	/**
	 * Fired when one user is saved on the Admin side
	 *
	 */
	static function onAdminUserSave($observer)
	{
		$event = $observer->getEvent(); 
		
		$request = $observer->getRequest();
		
		
//		$customer = $event->getCustomer();
//		Mage::log(get_class_methods($observer), null, 'adminusersaveEvent.log');
//		Mage::log(get_class_methods($event->getData()), null, 'onAdminUserSave.log');
//		Mage::log($request->getData(), null, 'adminusersaveEvent.log');
		
	}
	
	
	
}