<?php
/**
 * Email Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Mynose_Helper_Email extends Mage_Core_Helper_Abstract
{

	// const XML_PATH_EMAIL_IDENTITY               = 'sales_email/shipment/identity';
	const XML_PATH_EMAIL_IDENTITY               = 'trans_email/ident_general';

	/**
	 * Email : Coupon for Sample buy : Yeeeee !
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 * @param	Array	$_POST data.
	 *
	 */
	public function sendSampleCouponMail($customer, $order, $couponCode, $couponValue, $nbSamples)
	{
		// Template vars
		$vars = array(
			'customer_name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
			'coupon_code' => $couponCode,
			'coupon_value' => $couponValue,
			'nb_samples' => $nbSamples
		);
		$to = $customer->getEmail();
		
		return $this->sendMail($to, $vars['customer_name'], 'nose_sample_coupon', $vars);
	}
	
	/**
	 * Email : Invitation to one Surprise
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 * @param	Array	$_POST data.
	 *
	 */
	public function sendSurpriseInvitationMail($senderNoseUser, $invitedNoseUser, $post = array())
	{
		// Template vars
		$vars = array(
			'sender_name' => $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname'],
			'invited_name' => $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname'],
			'invited_email' => $invitedNoseUser['email'],
			'hash' => $senderNoseUser['hash'],
			'sender_id_user' => $senderNoseUser['id_user'],
			'invited_id_user' => $invitedNoseUser['id_user'],
			'rejectUrl' => Mage::helper('mynose/url')->getContactRejectEmailInvitationUrl(),
			'acceptUrl' => Mage::helper('mynose/url')->getContactAcceptEmailInvitationUrl(),
			'message' => $post['message']
		);
		$to = $invitedNoseUser['email'];
		
		return $this->sendMail($to, $vars['invited_name'], 'nose_surprise_invitation', $vars);
	}
	
	/**
	 * Email : Invitation to Love Machine
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 * @param	Array	$_POST data.
	 *
	 */
	public function sendLoveMachineInvitationMail($senderNoseUser, $invitedNoseUser, $post = array())
	{
		// Template vars
		$vars = array(
			'sender_name' => $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname'],
			'invited_name' => $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname'],
			'invited_email' => $invitedNoseUser['email'],
			'hash' => $senderNoseUser['hash'],
			'sender_id_user' => $senderNoseUser['id_user'],
			'invited_id_user' => $invitedNoseUser['id_user'],
			'rejectUrl' => Mage::helper('mynose/url')->getContactRejectEmailInvitationUrl(),
			'acceptUrl' => Mage::helper('mynose/url')->getContactAcceptEmailInvitationUrl(),
			'message' => $post['message']
		);
		$to = $invitedNoseUser['email'];
		
		return $this->sendMail($to, $vars['invited_name'], 'nose_love_machine_invitation', $vars);
	}
	
	




	/**
	 * Email : Invitation
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 *
	 * @return bool
	 */
	public function sendInvitationMail($senderNoseUser, $invitedNoseUser)
	{
		// Template vars
		$vars = array(
			'sender_name' => $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname'],
			'invited_name' => $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname'],
			'invited_email' => $invitedNoseUser['email'],
			'hash' => $senderNoseUser['hash'],
			'sender_id_user' => $senderNoseUser['id_user'],
			'invited_id_user' => $invitedNoseUser['id_user'],
			'rejectUrl' => Mage::helper('mynose/url')->getContactRejectEmailInvitationUrl(),
			'acceptUrl' => Mage::helper('mynose/url')->getContactAcceptEmailInvitationUrl()
		);
		$to = $invitedNoseUser['email'];

		// Accept URL
		/*
  		$url = Mage::helper('mynose/url')->getContactAcceptEmailInvitationUrl();
		$url .= '/s/'.$senderNoseUser['id_user'];
		$url .= '/i/'.$invitedNoseUser['id_user'];
		$url .= '/h/'.$senderNoseUser['hash'];

		Mage::log('Accept URL : ' . $url, null, 'partikule.log');
		*/

		return $this->sendMail($to, $vars['invited_name'], 'nose_contact_invitation', $vars);
	}
	
	
	/**
	 * Email : Invitation accepted
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 *
	 * @return bool
	 */
	public function sendInvitationAcceptedMail($senderNoseUser, $invitedNoseUser)
	{
		// Template vars
		$vars = array(
			'sender_id_user' => $senderNoseUser['id_user'],
			'sender_name' => $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname'],
			'invited_id_user' => $invitedNoseUser['id_user'],
			'invited_name' => $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname'],
			'invited_email' => $invitedNoseUser['email'],
			'contactBookUrl' => Mage::helper('mynose/url')->getContactBookUrl()
		);
		
		$to = $senderNoseUser['email'];
		
		return $this->sendMail($to, $vars['invited_name'], 'nose_contact_invitation_accepted', $vars);
	}
	
	
	/**
	 * Email : Invitation refused
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 *
	 * @return bool
	 */
	public function sendInvitationRefusedMail($senderNoseUser, $invitedNoseUser)
	{
		// Template vars
		$vars = array(
			'sender_name' => $senderNoseUser['firstname'] . ' ' . $senderNoseUser['lastname'],
			'invited_name' => $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname'],
			'invited_email' => $invitedNoseUser['email'],
			'sender_id_user' => $senderNoseUser['id_user'],
			'invited_id_user' => $invitedNoseUser['id_user']
		);
		
		$to = $senderNoseUser['email'];
		
		return $this->sendMail($to, $vars['invited_name'], 'nose_contact_invitation_refused', $vars);
	}
	
	
	/**
	 * Email : Diagnostic Result Email
	 *
	 * called by : 
	 * 1. 	Partikule_Mynose_Block_Diagnostic->getDiagnostic()
	 * 		if $diag['emailed'] = 0
	 *
	 * 2. 	Partikule_Mynose_Helper_Diagnostic->sendDiagnosticResult()
	 *		Builds the product collection
	 *
	 * @param	Array	Sender of the invit.
	 * @param	Array	Invited.
	 *
	 * @return bool
	 */
	public function sendDiagnosticResult($user, $diagBlock)
	{
		// Template vars
		$vars = array(
			'user' => $user,
			'diagnostic_url' => Mage::helper('mynose/url')->getDiagnoseResultUrl(),
			'items' => $diagBlock,
			'user_name' => $user->getFirstname() . ' ' . $user->getLastname(),			
		);
		
		$to = $user->getEmail();
		
		return $this->sendMail($to, $user->getFirstname() .' '. $user->getLastname() , 'nose_diagnostic_result', $vars);
	}


	/**
	 * Sends the Order Summary Email
	 * Called by Partikule/Mynose/Model/Observer->sendSummaryEmailAfterShipping()
	 *
	 * @param        $customer
	 * @param string $tracking_block
	 * @param string $evaluation_block
	 * @param string $coupon_block
	 * @param string $free_shipping_block
	 * @param string $diag_block
	 *
	 * @return bool
	 */
	public function sendSummaryEmailAfterShipping($customer, $order, $shipment)
	{
		// Order's Store ID : To send order's language email
		$storeId = $order->getStoreId();

		// Mage::log('sendSummaryEmailAfterShipping() Store ID:'.$storeId, null, 'partikule.log');
		// Start store emulation process
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

		/*
		$defaultLocale = Mage::app()->getLocale()->getLocaleCode();
		$currentStore   = Mage::app()->getStore()->getCode();
		$currentLocale  = Mage::getModel('core/locale')->getLocaleCode();
		Mage::log('$currentStore : ' . $currentStore, null, 'partikule.log');
		Mage::log('$currentLocale : ' . $currentLocale, null, 'partikule.log');
		*/

		$gender = $customer->getGender();
		$prefix = ($gender) ? Mage::helper('core')->__('email_prefix_' . $gender) : Mage::helper('core')->__('Hello');


		$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);


		// Template vars
		$vars = array(
			'user' => $customer,
			'user_prefix' => $prefix,
			'user_name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
			'order' => $order,
			'shipment' => $shipment,
			'diagnostic_url' => Mage::helper('mynose/url')->getDiagnoseResultUrl(),
		);

		$to = $customer->getEmail();

		// Template in : app/locale/fr_fr/template/email/
		return $this->sendMail(
			$to,
			$customer->getFirstname() .' '. $customer->getLastname() ,
			'nose_order_summary',
			$vars,
			$storeId
		);
	}
	
	
	public function sendMail($to, $name, $template_id, $vars, $storeId = NULL)
	{
		if (is_null($storeId))
			$storeId = Mage::app()->getStore()->getId();

		$mailer = Mage::getModel('core/email_template_mailer');

		$emailInfo = Mage::getModel('core/email_info');
		$emailInfo->addTo($to, $name);
		$mailer->addEmailInfo($emailInfo);

		$mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
		$mailer->setStoreId($storeId);
		$mailer->setTemplateId($template_id);

		$mailer->setTemplateParams($vars);
		$result = $mailer->send();


		/*
		 * Method with Transactional :
		 * Not working when sending Email from Admin side.
		 * Layout aren't rendered
		 * See : Mage_Sales_Model_Order_Shipment->sendEmail()
		 *
		// Mage::log('sendMail StoreID : '.$storeId, null, 'partikule.log');
		// Set the StoreID to the given store, so emails can be send in english from another store.
		// Mage::app()->setCurrentStore($storeId);
		// Mage::getDesign()->setPackageName("nose")->setTheme("nose");

				$emailTemplate  = Mage::getModel('core/email_template');

				//fetch sender data from Admin > System > Configuration > Store Email Addresses > General Contact
				$sender = array(
					'name'  => Mage::getStoreConfig('trans_email/ident_general/name'),
					'email' => Mage::getStoreConfig('trans_email/ident_general/email')
				);

				$model = $emailTemplate->setReplyTo($sender['email']);
				$result = $model->sendTransactional($template_id, $sender, $to, $name, $vars, $storeId);

		*/
		if ( ! $result)
		{
			Mage::log('ERROR : sendMail()"'.$template_id.'" to : '.$to, null, 'partikule.log');
			return FALSE;
		}

		return TRUE;
	}
}
