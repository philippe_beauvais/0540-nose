<?php
/**
 * Nose User Helper
 * 
 *
 */
class Partikule_Mynose_Helper_Request extends Mage_Core_Helper_Abstract
{
	/**
	 * CURL method
	 *
	 *
	 */
	public function curl($url, $post = null, $retries = 3)
	{
		$result = false;

		$curl = curl_init($url);

	    if (is_resource($curl) === true)
	    {
	        curl_setopt($curl, CURLOPT_FAILONERROR, true);
	        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            if (! empty($post))
            {
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, (is_array($post) === true) ? http_build_query($post, '', '&') : $post);
            }

			// .htaccess ?
			$http_user = Mage::getConfig()->getNode('default/mynose/settings')->htuser;

			if ($http_user)
			{
				$http_pass = Mage::getConfig()->getNode('default/mynose/settings')->htpassword;

				curl_setopt($curl, CURLOPT_USERPWD, "$http_user:$http_pass");
			}
	
	        while (($result === false) && (--$retries > 0))
	        {
	            $result = curl_exec($curl);
	        }

			if ( ! $result)
			{
				$msg = curl_error($curl);
				if ( ! empty($msg))
					Mage::log('ERROR : Partikule_Mynose_Helper_Request->curl() : ' . $url. ' : '. $msg, null, 'partikule.log');
			}

	        curl_close($curl);
	    }
	
	    return $result;
	}

}