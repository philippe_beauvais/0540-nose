<?php
/**
 * Diagnostic Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Mynose_Helper_Diagnostic extends Mage_Core_Helper_Abstract
{
	private $_inputSessionIndex = 'formPerfumes';

	/*
	 * Array of allowed $_POST vars
	 *
	 */
	private $_allowed_diag_keys = array(
		'style_id',
		'note_id',
		'note_id_1',
		'note_id_2',
		'note_id_label_1',
		'note_id_label_2',
		'period_1',
		'period_2',
		'period_3',
		'brand_1',
		'brand_2',
		'brand_3',
		'search_brand_1',
		'search_brand_2',
		'search_brand_3',
		'perfume_1',
		'perfume_2',
		'perfume_3',
	);
	
	
	/**
	 * Try to get the diagnostic result from the eNose server
	 * The remote service creates the diagnostics if needed.
	 *
	 * @param	$post	$_POST data
	 *
	 * @return	void	Dos not returns anything : Just calls the service
	 *					The Result display panel checks if one diag exists or not.
	 *
	 * @dispatchEvent	mynose_diagnosis_call
	 *                	if the Curl call was successful
	 */
	public function callDiagnosticResults($post)
    {
		$customer = Mage::getSingleton('customer/session')->getCustomer();

		// eNose Diagnose Result URL (remote service)
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'result';
		
		$session = Mage::getSingleton('core/session');

		// First Diag
		// Use by Google Tracking
		$has_already_one_diagnostic = Mage::getModel('mynose/diagnostic')->hasAlreadyOneDiagnostic($customer);
		if ($has_already_one_diagnostic > 0)
			$session->setData('firstDiag', FALSE);
		else
			$session->setData('firstDiag', TRUE);

		// Put each POST data in Session
		foreach($post as $k => $p)
		{
			if ( in_array($k, $this->_allowed_diag_keys))
				$session->setData($k, $p);			
		}

		// Add Current logged User data to post (email needed for the Diag)
		$post['email'] = $customer->getEmail();
		$post['magento_id'] = $customer->getId();
		
		// Store ID : 1 = 'Online'
		$post['store_id'] = Mage::getStoreConfig('mynose/settings/store_id');
		$post['distributor_id'] = Mage::getStoreConfig('mynose/settings/distributor_id');

		// Mage::log($enose_url, null, 'partikule.log');


		// CURL request to eNose : Posts the Diag Data
		$result = Mage::helper('mynose/Request')->curl($enose_url, $post);

		if ($result !== FALSE)
		{
			Mage::dispatchEvent("mynose_diagnosis_done", array(
				'user' => $customer,
				'distributor_id' => $post['distributor_id'],
				'store_id' => $post['store_id']
			));
		}
		else
		{
			Mage::log('callDiagnosticResults() : ERROR ', null, 'partikule.log');
			Mage::log($post, null, 'partikule.log');
		}
    }
    
    
    /**
     * Call the Lovers Diagnostic saving service
     * Only saves the lovers diag if needed.
     *
     * Called on relation type change, by the ContactController
     *
     */
    public function callLoverDiagnosticSave()
    {
        // eNose Diagnose Result URL (remote service)
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'saveloversdiag';
		
		$session = Mage::getSingleton('core/session');

		// Add Current logged User data to post (email needed for the Diag)
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$post = array(
			'magento_id' => $user->getId()
		);

		// CURL request to eNose : Posts the Diag Data
		$result = Mage::helper('mynose/Request')->curl($enose_url, $post);
    }
    
    
    /**
     * Call the Lovers Diagnostic erase service
     * Called on relation type change
     *
     */
    public function callLoverDiagnosticErase()
    {
        // eNose Diagnose Result URL (remote service)
		// http://enose.tld/diagnose/result
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'eraseloversdiags';
		
		$session = Mage::getSingleton('core/session');
		
		// Add Current logged User data to post (email needed for the Diag)
		$user = Mage::getSingleton('customer/session')->getCustomer();
		$post = array(
			'magento_id' => $user->getId()
		);
		
		// CURL request to eNose : Posts the Diag Data
		$result = Mage::helper('mynose/Request')->curl($enose_url, $post);
    }


	/**
	 * Returns the HTML formatted Brands autocomplete select
	 *
	 * @param $request
	 *
	 * @return mixed
	 */
	public function getBrandAutocomplete($request)
	{
		// eNose URL
		$url = Mage::helper('mynose/url')->getEnoseUrl().'core/autocomplete/';

		// Pseudo post data, send through POST to the Diagnose application,
		// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
		// These data are send back in the JSON answer
		$post = array (
			'base_url' => Mage::getBaseUrl(),
			'what' => 'brand',
			'search' => $request->getPost('search')
		);

		// Request to eNose
		$html = Mage::helper('mynose/Request')->curl($url, $post);
		return $html;
	}


	public function getPerfumeSelect($request)
	{
		$url = Mage::helper('mynose/url')->getEnoseUrl().'core/get_perfume_select/';

		// Post data, send through POST to the Diagnose application,
		// needed to the Diagnose to define the proxy URL to call (for Autocompleter, for example)
		// These data are send back in the JSON answer
		$post = array (
			'brand_id' => $request->getPost('brand_id'),
			'select_id' => $request->getPost('select_id'),
			'include_all' => $request->getPost('include_all'),
			'selected' => $request->getPost('selected'),
			// Not send anymore
			// 'sex' => $user->getGender()
		);

		// Request to eNose
		$html = Mage::helper('mynose/Request')->curl($url, $post);
		return $html;

	}



	/**
 	 * Sends the Disgnostic Result to the user
 	 *
 	 * @param	User
 	 * @param	Object		Diagnostic result object
 	 *
 	 *
 	 */
	public function sendDiagnosticResult($user, $diag)
	{
		// Master array
		$master_ids = array();
		
		// get master products from diagnostic results
		if ( ! empty($diag->perfumes))
		{
			foreach ($diag->perfumes as $perfume)
				$master_ids[] = $perfume->master_id;
				// Mage::getModel('catalog/product')->loadByAttribute('master_id', $perfume->master_id);
		}

		$storeId = Mage::app()->getStore()->getId();

		$perfumeCollection = Mage::getModel('catalog/product')->getCollection()
			->setStoreId( $storeId )
			->addAttributeToSelect("*")
			->addAttributeToFilter('master_id', array('in' =>  $master_ids))
			->addAttributeToFilter('type_id', 'configurable')
		;
		
		$perfumeCollection = Mage::getModel('boutique/Category')->setCategoryData($perfumeCollection);
		

// $wishlistBlock = $this->getLayout()->createBlock('wishlist/share_email_items')->toHtml();

		// Send the mail
		Mage::helper('mynose/email')->sendDiagnosticResult($user, $perfumeCollection);
		

	}


	/**
	 * Returns the current user Diag.
	 * Alert : By query directly the eNose DB
	 *
	 * @deprecated
	 * @TODO : Replace by cURL callt
	 * @notice : 	Should be used by : design/frontend/nose/default/template/nose/article/career.phtml
	 * 				Doesn't returns the complete diag.
	 *
	 * @return mixed
	 */
	public function getCurrentUserDiagnostic()
	{
		return Mage::getModel('mynose/diagnostic')->getCurrentUserDiagnostic();
	}


	/**
	 * Get one user's Diagnostic
	 *
	 * @TODO : Rewrite, not correct, base on customer ID, should be based on user_id
	 *
	 * @param $user
	 *
	 * @return mixed
	 */
	public function getUserDiagnostic($user)
	{
		if ($user && $user->getId())
		{
			return Mage::getModel('mynose/diagnostic')->getUserDiagnostic($user);
		}

		return NULL;
	}


	/**
	 * Get the complete Diagnotic of one Magento user
	 * @created 2012.11.26
	 *
	 * @param 	$customer	Magento Customer
	 * @return	array		Diagnostic Recommandations array
	 *
	 */
	public function getCustomerDiagnostic($customer)
	{
		$enose_url = Mage::helper('mynose/url')->getEnoseDiagnoseUrl().'diagnostic';

		// Current User : Needs to be posted to eNose
		$post = array (
			'magento_id' => $customer->getId(),
//			'lang_code' => Mage::app()->getStore()->getCode()
		);

		// Request to eNose
		$json = Mage::helper('mynose/Request')->curl($enose_url, $post);

		$diag = json_decode($json);

		return $diag;
	}

	/**
	 * @return bool
	 * Partikule : Added 2012.12.06
	 *
	 */
	public function hasInputsInSession()
	{
		$session = Mage::getSingleton('core/session');
		$sessionPerfumes = $session->getData($this->_inputSessionIndex);

		if ($sessionPerfumes)
			return TRUE;

		return FALSE;
	}


	/**
	 * Returns the diagnose form selected perfumes
	 * as they was fill in the form
	 *
	 * @return mixed|null
	 */
	public function getFormSelectionFromSession()
	{
		$session = Mage::getSingleton('core/session');
		$sessionPerfumes = $session->getData($this->_inputSessionIndex);

		if ($sessionPerfumes)
			return $sessionPerfumes;

		return NULL;
	}


	/**
	 *
	 * Returns one inputs object as awaited from Diag preference form,
	 * but from session
	 *
	 * Partikule : Added 2012.12.06
	 *
	 */
	public function getInputsFromSession()
	{
		$session = Mage::getSingleton('core/session');
		$sessionPerfumes = $session->getData($this->_inputSessionIndex);

		$inputArray = array();

		for ($i=1; $i<4; $i++)
		{
			if (isset($sessionPerfumes['perfume_'.$i]) && isset($sessionPerfumes['brand_'.$i]))
			{
				$inputArray[] = array(
					'perfume_id' => $sessionPerfumes['perfume_'.$i],
					'brand_id' => $sessionPerfumes['brand_'.$i],
					'brand_name' => $sessionPerfumes['search_brand_'.$i],
					'when_month' =>$sessionPerfumes['period_'.$i],
				);
			}
		}

		if ( ! empty($inputArray))
		{
			$inputArray = array('perfumes'=>$inputArray);
			$inputObject = json_decode(json_encode($inputArray));
			return $inputObject;
		}

		return NULL;
	}


	/**
	 *
	 * @return mixed
	 */
	public function getCustomerAlsoLiked()
	{
		$customer = Mage::getSingleton('customer/session')->getCustomer();

		$data = Mage::getModel('mynose/diagnostic')->getCustomerAlsoLiked($customer);

		return $data;
	}



	public function clearSession()
	{
		$session = Mage::getSingleton('core/session');
		$session->getData($this->_inputSessionIndex, TRUE);
	}

}