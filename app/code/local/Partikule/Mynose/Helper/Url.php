<?php
/**
 * Nose URL Helper
 * 
 *
 */
class Partikule_Mynose_Helper_Url extends Mage_Core_Helper_Abstract
{

	public function getModuleUrl() { return Mage::getBaseUrl(). 'mynose'; }
	
	
	public function getEnoseUrl()
	{
		return Mage::getStoreConfig('mynose/settings/url');
	}
	
	
	public function getEnoseDiagnoseUrl()
	{
		$lang = Mage::helper('mynose/Core')->getCurrentStoreCode();
		
		return $this->getEnoseUrl().$lang.'/'.Mage::getStoreConfig('mynose/settings/diagnose_uri');
		
//		return Mage::getStoreConfig('mynose/settings/diagnose_url');
	}

	public function getBrandAutocompleteURL() { return Mage::getBaseUrl(). 'mynose/brand/autocomplete/'; }


	public function getPerfumeSelectURL() { return Mage::getBaseUrl(). 'mynose/perfume/perfumeselect/'; }
	public function getFormActionURL() { return Mage::getBaseUrl(). 'mynose/result/'; }


	public function getMyNoseUrl() { return Mage::getBaseUrl(). 'mynose'; }
	public function getMyNoseWelcomeUrl() { return Mage::getBaseUrl(). 'mynose/welcome'; }


	public function getProductUrl()	{ return Mage::getBaseUrl(). 'mynose/product'; }
	public function getEvalProductUrl() { return Mage::getBaseUrl(). 'mynose/product/eval'; }
	public function getFavoriteUrl() { return Mage::getBaseUrl(). 'mynose/eval/favorite'; }

	public function getEvalUrl() { return Mage::getBaseUrl(). 'mynose/eval'; }
	public function getEvalSaveUrl() { return Mage::getBaseUrl(). 'mynose/eval/save'; }


	public function getDiagnoseRegisterUrl() { return Mage::getBaseUrl(). 'mynose/diagnose/create'; }
	public function getDiagnoseUrl() { return Mage::getBaseUrl(). 'mynose/diagnose/do'; }
	public function getDiagnoseResultUrl() { return Mage::getBaseUrl(). 'mynose/result/mydiagnose'; }
	public function getDiagnoseRecoUrl() { return Mage::getBaseUrl(). 'mynose/result/myreco'; }
	public function getEvaluationFormUrl(){ return Mage::getBaseUrl(). 'mynose/eval/update';}
	
	// Surprise
	public function getSurpriseUrl(){ return Mage::getBaseUrl(). 'mynose/surprise';}
	public function getSurpriseInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/surprise/invite';}
	public function getSurpriseFormUrl(){ return Mage::getBaseUrl(). 'mynose/surprise/send';}
	public function getSurpriseFriendUrl(){ return Mage::getBaseUrl(). 'mynose/surprise/friend';}
	public function getSurpriseCalendarUrl(){ return Mage::getBaseUrl(). 'mynose/surprise/calendar';}

	public function getGiftCardUrl(){ return Mage::getBaseUrl(). 'customer/giftcards/balance';}




	// Contact
	public function getContactUrl(){ return Mage::getBaseUrl(). 'mynose/contact';}
	public function getContactInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/contact/invite';}
	public function getContactCreditUrl(){ return Mage::getBaseUrl(). 'mynose/contact/credit';}

//	public function getContactCalendarUrl(){ return Mage::getBaseUrl(). 'mynose/contact/calendar';}

	public function getContactFormUrl(){ return Mage::getBaseUrl(). 'mynose/contact/create';}
	
	public function getContactChangeLinkUrl(){ return Mage::getBaseUrl(). 'mynose/contact/changelink';}
	public function getContactChangeLinkPostUrl(){ return Mage::getBaseUrl(). 'mynose/contact/changelinkpost';}

	public function getContactRemoveUrl(){ return Mage::getBaseUrl(). 'mynose/contact/remove';}
	public function getContactAddUrl(){ return Mage::getBaseUrl(). 'mynose/contact/add';}
	public function getContactBookUrl(){ return Mage::getBaseUrl(). 'mynose/contact/book';}
	public function getContactAcceptInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/contact/acceptInvitation';}
	public function getContactRejectInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/contact/rejectInvitation';}
	public function getContactDiagnosticUrl(){ return Mage::getBaseUrl(). 'mynose/contact/diagnostic';}
	
	public function getContactAddressesUrl(){ return Mage::getBaseUrl(). 'mynose/contact/addresses';}

	public function getFbRequestsSaveUrl(){ return Mage::getBaseUrl(). 'mynose/contact/saveFbRequests';}
	
	
	// Love Machin, love bidule
	public function getLoveUrl(){ return Mage::getBaseUrl(). 'mynose/love';}
	public function getLoveInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/love/invite';}
	public function getLoveFormUrl(){ return Mage::getBaseUrl(). 'mynose/love/send';}

	// HERE
	public function getLoveMyDiagnosticUrl(){ return Mage::getBaseUrl(). 'mynose/love/mydiagnostic';}
	public function getLoveLoverDiagnosticUrl(){ return Mage::getBaseUrl(). 'mynose/love/loverdiagnostic';}

	
	
	// Mail invitations
	public function getContactAcceptEmailInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/contact/acceptEmailInvitation';}
	public function getContactRejectEmailInvitationUrl(){ return Mage::getBaseUrl(). 'mynose/contact/rejectEmailInvitation';}
	

	// Open URLs : No need to be logged
	// Brand autocomplete URL
	public function getOpenBrandsURL() { return Mage::getBaseUrl(). 'mynose/open/brands/'; }
	public function getOpenPerfumesURL() { return Mage::getBaseUrl(). 'mynose/open/perfumes/'; }
	public function getOpenFormActionURL() { return Mage::getBaseUrl(). 'mynose/open/result/'; }



}
