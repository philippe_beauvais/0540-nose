<?php
/**
 * Contact Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Mynose_Helper_Contact extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Return the contact array
	 * Used by OPC billing page to display the select of contacts.
	 *
	 */
    public function getContacts()
    {
    	return Mage::getModel('mynose/contact')->getContacts();
    }
    
    /**
     * Returns contacts who have at least one address
     *
     *
     */
    public function getContactsWithAddress()
    {
    	$contacts = Mage::getModel('mynose/contact')->getContactsWithAddress();
        return $contacts;
    }

}