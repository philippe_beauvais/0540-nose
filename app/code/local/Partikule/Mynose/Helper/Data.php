<?php
/**
 * Data Helper
 *
 * Needs to be there to make the Translator work well.
 *
 * The Translator is used in evaluation.phtml for example, to make the translations available fter Ajax calls.
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Mynose_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Returns one JSON encoded array 
	 *
	 */
	public function setJsonMessage($type, $message)
	{
		$message = array(
			'type' => $type,
			'message' => $message
		);
		
		return json_encode($message);
	}

	/**
	 * Returns one JSON encoded array 
	 *
	 */
	public function setJsonSuccessMessage($message)
	{
		return $this->setJsonMessage('success', $message);
	}

	/**
	 * Returns one JSON encoded array 
	 *
	 */
	public function setJsonErrorMessage($message)
	{
		return $this->setJsonMessage('error', $message);
	}

}
