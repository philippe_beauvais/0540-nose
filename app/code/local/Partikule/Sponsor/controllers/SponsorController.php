<?php

class Partikule_Sponsor_SponsorController extends Mage_Core_Controller_Front_Action
{
	public function useCreditAction()
	{
		/**
		 * No reason continue with empty shopping cart
		 */
		if (!$this->_getCart()->getQuote()->getItemsCount()) {
			$this->_goBack();
			return;
		}

		$removeCreditUsage =  $this->getRequest()->getParam('remove');

		if ($removeCreditUsage)
		{
			Mage::dispatchEvent("cancel_cart_credit", array(
				'cart' => $this->_getCart()
			));
		}
		else
		{
			Mage::dispatchEvent("apply_cart_credit", array(
				'cart' => $this->_getCart()
			));
		}

		// Recalculate Quote total
		// $quote->setTotalsCollectedFlag(false)->collectTotals();

		$this->_goBack();

	}


	protected function _renderContactLayout()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}




	/**
	 * Retrieve shopping cart model object
	 *
	 * @return Mage_Checkout_Model_Cart
	 */
	protected function _getCart()
	{
		return Mage::getSingleton('checkout/cart');
	}

	/**
	 * Get checkout session model instance
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	protected function _getSession()
	{
		return Mage::getSingleton('checkout/session');
	}

	/**
	 * Get current active quote instance
	 *
	 * @return Mage_Sales_Model_Quote
	 */
	protected function _getQuote()
	{
		return $this->_getCart()->getQuote();
	}

	/**
	 * Set back redirect url to response
	 *
	 * @return Mage_Checkout_CartController
	 */
	protected function _goBack()
	{
		$returnUrl = $this->getRequest()->getParam('return_url');
		if ($returnUrl) {
			// clear layout messages in case of external url redirect
			if ($this->_isUrlInternal($returnUrl)) {
				$this->_getSession()->getMessages(true);
			}
			$this->getResponse()->setRedirect($returnUrl);
		} elseif (!Mage::getStoreConfig('checkout/cart/redirect_to_cart')
			&& !$this->getRequest()->getParam('in_cart')
			&& $backUrl = $this->_getRefererUrl()
		) {
			$this->getResponse()->setRedirect($backUrl);
		} else {
			if (($this->getRequest()->getActionName() == 'add') && !$this->getRequest()->getParam('in_cart')) {
				$this->_getSession()->setContinueShoppingUrl($this->_getRefererUrl());
			}
			$this->_redirect('checkout/cart');
		}
		return $this;
	}


}