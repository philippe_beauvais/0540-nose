<?php

class Partikule_Sponsor_InviteController extends Partikule_Sponsor_Controller_Abstract
{
	const RELATION_STATUS_INVITED = 1;
	const RELATION_STATUS_ACCEPTED = 2;
	const RELATION_STATUS_REFUSED = 3;
	const RELATION_STATUS_DELETED = 4;

	protected $_contactModel = NULL;

	/**
	 * 
	 *
	 *
	 */
	public function indexAction()
	{
		$this->loadLayout();

		// Define the template the root block must use.
//		$this->getLayout()->getBlock('root')->setTemplate('ionize/page/' . $this->_page->getMagentoTemplate());

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');
		
		$this->renderLayout();
	}

	/**
	 *
	 * Displays the Invitation form
	 *
	 *
	 */
	public function inviteAction()
	{
		$this->loadLayout();

		// Set the current Ionize's page URL active
		$this->getLayout()->getBlock('catalog.topnav')->setActive('mynose');

		$this->renderLayout();
	}


	/**
	 *
	 * Sends the surprise
	 * Validates the surprise form
	 *
	 * @see Partikule_Mynose_ContactController->createAction for comments
	 *
	 */
	public function sendAction()
	{
		$post = $this->getRequest()->getPost();
		$session = Mage::getSingleton('core/session');
		Mage::getSingleton('customer/session')->setCustomerFormData($post);
		
		// Potential existing user : From eNose DB
		$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));
		
		// Current Nose user's infos
		$currentNoseUser = $this->_getCurrentNoseUser();

		// Same Email : Case of one stupid user	
		if ( ! empty($invitedNoseUser) && $invitedNoseUser['email'] == $currentNoseUser['email'])
		{
			$session->addError($this->__('You cannot invite yourself'));
			
			// Form populating
			$session->setSurpriseFormData($this->getRequest()->getPost());
			
			// Redirect to Surprise
			$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getSurpriseInvitationUrl());
		}
		else
		{
			// Try to get the relation
			$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);

			// No relation : Create
			if ( ! $relation)
			{
				// Is the invited user a new one ?
				$new_invited_user = FALSE;

				// Create User if he doesn't exists
				if ( empty($invitedNoseUser) OR ! $invitedNoseUser['id_user'])
				{
					// User's origine
					$post['origin'] = self::ORIGIN_INVITATION;

					$invitedNoseUser = $this->_getContactModel()->createUser($post);

					// Get again to feed the user ID
					$invitedNoseUser = $this->_getContactModel()->getUserFromEmail($this->getRequest()->getPost('email'));

					$new_invited_user = TRUE;
				}

				// User exists : Create relation
				if ( ! empty($invitedNoseUser))
				{
					// Wait! :
					// First check if the invited user hasn't already a lover.
					// It is very bad to steal the lover from one another... pas beau, bouh !
					if (
						$this->_getContactModel()->isLoveRelationId($this->getRequest()->getPost('link'))
						&& $this->_getContactModel()->hasAlreadyLover($invitedNoseUser, $currentNoseUser)
					)
					{
						$session->addError($invitedNoseUser['firstname'] . $this->__(" has already one lover. You cannot invite him / her as lover."));	
						
						// Redirect to Love Machine Invitation form
						$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getSurpriseInvitationUrl());
					}
					else
					{
						$result = $this->_getContactModel()->createRelation($currentNoseUser, $invitedNoseUser, $this->getRequest()->getPost('link'));
						
						if ($result)
						{
							$relation = $this->_getContactModel()->getRelation($currentNoseUser, $invitedNoseUser);
							
							// Set the Hash to the curent user
							$currentNoseUser['hash'] = $relation['hash'];
							
							// Send Invitation Email
							Mage::helper('mynose/email')->sendSurpriseInvitationMail($currentNoseUser, $invitedNoseUser, $post);

							// Lifecycle : Invitation Sent / Received
							Mage::helper('lifecycle')->action('invitation_online_sent', $currentNoseUser);
							Mage::helper('lifecycle')->action('invitation_reception', $invitedNoseUser);

							// @TODO :
							// Fire Event for the sponsor
							Mage::dispatchEvent("lifecycle_invitation_accepted", array(
								'sponsor' => $currentNoseUser,
								'sponsored' => $invitedNoseUser
							));


							$session->addSuccess($this->__('One invitation has been sent to ') . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);

							// Redirect to book
							$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getContactBookUrl());
						}
						else
						{
							$session->addSuccess($this->__('One problem happens during connexion with this user'));				
	
							// Redirect to Add
							$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getSurpriseInvitationUrl());
						}
					}
				}
			}
			else
			{
				if ($relation['status'] == self::RELATION_STATUS_INVITED)
					$session->addSuccess($this->__("You already invited ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
				else
					$session->addSuccess($this->__("You're already friend with ") . $invitedNoseUser['firstname'] . ' ' . $invitedNoseUser['lastname']);
				
				// Redirect to Surprise
				$this->getResponse()->setRedirect(Mage::helper('mynose/url')->getSurpriseInvitationUrl());
			}
		}
	}


	private function _getContactModel()
	{
		if ( is_null($this->_contactModel))
			$this->_contactModel = Mage::getModel('mynose/contact');
		
		return $this->_contactModel;
	}

	/**
	 * Returns current Nose User
	 *
	 *
	 */
	private function _getCurrentNoseUser()
	{
		$contactModel = Mage::getModel('mynose/contact');

		// Current Magento user
		$currentUser = Mage::getModel('customer/session')->getCustomer();

		// Current Nose user's infos
		$currentNoseUser = $contactModel->getUserFromEmail($currentUser->getEmail());
		
		return $currentNoseUser;
	}
	
	
	/**
	 * Returns one contact from his ID
	 *
	 */
	private function _getContactNoseUser($id)
	{
		return $this->_getContactModel()->getUserFromId($id);
	}




}