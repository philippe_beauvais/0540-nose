<?php

/**
 *
 *
 * Actions
 * 'get' :    	the user got credits, usually from sponsorship validation
 * 'use' : 		the user uses credits
 * 'admin' : 	the admin sets credits to user
 */

class Partikule_Sponsor_Model_Sponsor extends Partikule_Sponsor_Model_Abstract
{
	public static $_CREDIT_USE = 'use';
	public static $_CREDIT_CANCEL = 'cancel';
	public static $_CREDIT_GET = 'get';
	public static $_CREDIT_ADMIN = 'admin';

	public static $_SPONSOR_NOT_VALIDATED = 0;
	public static $_SPONSOR_VALIDATED_EMAIL = 1;
	public static $_SPONSOR_VALIDATED_DIAG = 2;
	public static $_SPONSOR_VALIDATED = 3;


	public function __construct()
	{
		parent::__construct();

		// eNose user's table
		$this->users_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');

		// Magento Sponsor tables
		$this->partikule_sponsorship = Mage::getSingleton('core/resource')->getTableName('sponsor/partikule_sponsorship');
		$this->partikule_sponsorship_data = Mage::getSingleton('core/resource')->getTableName('sponsor/partikule_sponsorship_data');

	}


	/**
	 * Callback for the event "mynose_diagnosis_done"
	 * dispatched by : Partikule_Mynose_Helper_Diagnostic->callDiagnosticResults()
	 *
	 */
	public function checkSponsorStatusAfterDiag($event)
	{
		$customer = $event->getUser();
		$sponsoredNoseUser = Mage::getModel('mynose/user')->getNoseUser($customer);
		$status = $sponsoredNoseUser['sponsor_validated'];

		// Mage::log('checkSponsorStatusAfterDiag() : ' . $sponsoredNoseUser['email'], null, 'partikule.log');

		// If the user is sponsored : Can be validated
		if (
			$status != self::$_SPONSOR_VALIDATED
			&& ! empty($sponsoredNoseUser['id_sponsor'])
		)
		{
			if ($status == self::$_SPONSOR_VALIDATED_EMAIL)
			{
				// Mage::log($sponsoredNoseUser['email'] . ' from ' . $status . ' to ' . self::$_SPONSOR_VALIDATED, null, 'partikule.log');
				$result = $this->setSponsorStatus($sponsoredNoseUser, self::$_SPONSOR_VALIDATED);
				$this->validateSponsor($customer);
			}
			else
			{
				// Mage::log($sponsoredNoseUser['email'] . ' from ' . $status . ' to ' . self::$_SPONSOR_VALIDATED_EMAIL, null, 'partikule.log');
				$result = $this->setSponsorStatus($sponsoredNoseUser, self::$_SPONSOR_VALIDATED_DIAG);
			}
		}
	}


	/**
	 * Callback for the event "contact_invitation_accepted_email"
	 * dispatched by : Partikule_Mynose_ContactController->acceptEmailInvitationAction()
	 *
	 */
	public function checkSponsorStatusAfterEmail($event)
	{
		$sponsorNoseUser = $event->getSender();
		$sponsoredNoseUser = $event->getInvited();
		$status = $sponsoredNoseUser['sponsor_validated'];

		// Mage::log('checkSponsorStatusAfterEmail() : ' . $sponsoredNoseUser['email'], null, 'partikule.log');

		// Magento User
		$sponsor = Mage::getModel('customer/customer')->load($sponsorNoseUser['magento_id']);

		// If the user is sponsored : Can be validated
		if (
			$status != self::$_SPONSOR_VALIDATED
			&& ! empty($sponsoredNoseUser['id_sponsor'])
		)
		{
			if ($status == self::$_SPONSOR_VALIDATED_DIAG)
			{
				// Mage::log($sponsoredNoseUser['email'] . ' from ' . $status . ' to ' . self::$_SPONSOR_VALIDATED, null, 'partikule.log');
				$result = $this->setSponsorStatus($sponsoredNoseUser, self::$_SPONSOR_VALIDATED);

				$this->validateSponsor($sponsor);
			}
			else
			{
				// Mage::log($sponsoredNoseUser['email'] . ' from ' . $status . ' to ' . self::$_SPONSOR_VALIDATED_EMAIL, null, 'partikule.log');
				$result = $this->setSponsorStatus($sponsoredNoseUser, self::$_SPONSOR_VALIDATED_EMAIL);
			}
		}
	}


	public function validateSponsor($customer)
	{
		// eNose Users
		$sponsoredNoseUser = Mage::getModel('mynose/user')->getNoseUser($customer);
		$sponsorNoseUser = Mage::getModel('mynose/user')->getNoseUserFromId($sponsoredNoseUser['id_sponsor']);

		if ( ! empty($sponsorNoseUser) && ! empty($sponsorNoseUser['magento_id']))
		{
			// Magento Sponsor User
			$sponsorUser = Mage::getModel('customer/customer')->load($sponsorNoseUser['magento_id']);

			// Add 1 credit to the sponsor
			$this->addSponsoringCredit($sponsorUser, $customer, 1);

			// Dispatch Event : sponsor validated
			Mage::dispatchEvent("sponsor_validated", array(
				'sponsor' => $sponsorNoseUser,
				'sponsored' => $sponsoredNoseUser
			));

			// Mage::log('Sponsor Validated : ' . $sponsorNoseUser['email'] . ' SPONSOR OF : '.$sponsoredNoseUser['email'], null, 'partikule.log');
		}
	}


	/**
	 * Sets the user's sponsor status
	 * Write the field 'sponsor_validated' in eNose DB : users table
	 *
	 * @param $noseUser		Nose User (the Sponsored one)
	 * @param $status
	 *
	 * @return bool
	 */
	public function setSponsorStatus($noseUser, $status = 0)
	{
		// Use eNose DB
		$this->setConnection('mynose_read', 'mynose_write');

		$connection = $this->getWriteConnection();

		$data = array(
			'sponsor_validated' => $status,
		);

		$where = "id_user = " . $noseUser['id_user'];

		try
		{
			$connection->beginTransaction();
			$connection->update($this->users_table, $data, $where);
			$connection->commit();
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}


	/**
	 * Set the user's credit to the given absolute number
	 * should never be used
	 *
	 * @param     $sponsor
	 * @param int $nb
	 */
	public function setAdminCredit($sponsor, $nb=0)
	{
		$data = array(
			'action' => self::$_CREDIT_ADMIN,
			'customer_id' => $sponsor->getId(),
			'credit_change' => $nb,
		);

		$result = $this->writeSponsorshipData($data);

		// Absolute Credit update
		if ($result)
		{
			$this->updateSponsorCredits($sponsor, $nb, FALSE);
		}
	}


	/**
	 * Adds Credits from Admin
	 * Relative to the existing number of credits
	 *
	 * @param     $sponsor		Magento user
	 * @param int $nb
	 *
	 * @return bool
	 */
	public function addAdminCredit($sponsor, $nb=0)
	{
		$data = array(
			'action' => self::$_CREDIT_ADMIN,
			'customer_id' => $sponsor->getId(),
			'credit_change' => $nb,
		);

		// Mage::log($data, null, 'partikule.log');

		$result = $this->writeSponsorshipData($data);

		// Relative Credit update
		if ($result)
		{
			$this->updateSponsorCredits($sponsor, $nb);
		}
	}


	/**
	 * Add Credit from Sponsoring validation
	 * Relative to the existing number of credit of the existing user
	 *
	 * @param     $sponsor		Object. Magento user
	 * @param     $sponsored	Object. Magento user
	 * @param int $nb
	 *
	 * @return bool
	 */
	public function addSponsoringCredit($sponsor, $sponsored, $nb=1)
	{
		if ( ! Mage::getModel('sponsor/credit')->isSponsorAboveCreditLimit($sponsor))
		{
			$data = array(
				'action' => self::$_CREDIT_GET,
				'customer_id' => $sponsor->getId(),
				'sponsored_id' => $sponsored->getId(),
				'credit_change' => $nb,
			);

			$result =  $this->writeSponsorshipData($data);

			// Relative Credit update
			if ($result)
			{
				$this->updateSponsorCredits($sponsor, $nb);
			}
		}
	}


	/**
	 * Updates the number of credit of one user
	 *
	 * @param $sponsor
	 * @param $nb			int. 	Number credits
	 * @param $relative		bool. 	If TRUE, set the credit relative to the existing credits.
	 *                       		Default : TRUE
	 *
	 * @return bool
	 */
	public function updateSponsorCredits($sponsor, $nb, $relative=TRUE)
	{
		// Use Magento Connexion
		$this->setConnection('core_read', 'core_write');
		$connection = $this->getWriteConnection();

		$nose_connexion = Mage::getSingleton('core/resource')->getConnection('mynose_write');
		$mage_connexion = Mage::getSingleton('core/resource')->getConnection('core_write');

		$sponsorNoseUser = Mage::getModel('mynose/user')->getNoseUserFromCustomer($sponsor);

		// Not sponsor ?
		$existing_data = $this->_isSponsor($sponsor);

		if ( ! $existing_data)
		{
			$mage_data = array(
				'customer_id' => $sponsor->getId(),
				'credits' => $nb,
				'created_date' => date("Y-m-d H:i:s"),
			);
			$nose_where = "id_user = " . $sponsorNoseUser['id_user'];
			$nose_data = array('nb_credits' => $nb);
			try
			{
				$mage_connexion->beginTransaction();
				$mage_connexion->insert($this->partikule_sponsorship, $mage_data);
				$mage_connexion->commit();

				// Update eNose
				$nose_connexion->update($this->users_table, $nose_data, $nose_where);
				$nose_connexion->commit();

				return TRUE;
			}
			catch (Exception $e)
			{
				$connection->rollBack();
				return FALSE;
			}
		}
		else
		{
			if ($relative)
				$nb_credits = intval($existing_data['credits']) + intval($nb);
			else
				$nb_credits = $nb;

			// Protect against negative values
			if ($nb_credits < 0) $nb_credits = 0;

			$mage_data = array(
				'credits' => $nb_credits,
				'updated_date' => date("Y-m-d H:i:s"),
			);
			$mage_where = "customer_id = " . $sponsor->getId();

			$nose_where = "id_user = " . $sponsorNoseUser['id_user'];
			$nose_data = array('nb_credits' => $nb_credits);

			try
			{
				$connection->beginTransaction();
				$connection->update($this->partikule_sponsorship, $mage_data, $mage_where);
				$connection->commit();

				// Update eNose
				$nose_connexion->update($this->users_table, $nose_data, $nose_where);
				$nose_connexion->commit();

				return TRUE;
			}
			catch (Exception $e)
			{
				$connection->rollBack();
				return FALSE;
			}
		}
	}


	/**
	 * Returns the current sponsor credits array if he is already one sponsor
	 *
	 * @param $user
	 *
	 * @return mixed
	 */
	private function _isSponsor($user)
	{
		// Use Magento Connexion
		$this->setConnection('core_read', 'core_write');

		$select = $this->getReadConnection()->select()
			->from( array('sponsorship' => $this->partikule_sponsorship))
			->where('sponsorship.customer_id = ?', $user->getId()
		);

		// trace("{$select}".PHP_EOL);
		$query = $select->query();
		$result = $query->fetch();
		return $result;
	}


	/**
	 * Writes sponsorship data
	 *
	 * @param $data
	 *
	 * @return bool
	 */
	public function writeSponsorshipData($data)
	{
		// Use Magento Connexion
		$this->setConnection('core_read', 'core_write');
		$connection = $this->getWriteConnection();

		$data['ref_date'] = date("Y-m-d H:i:s");

		try
		{
			$connection->beginTransaction();
			$connection->insert($this->partikule_sponsorship_data, $data);
			$connection->commit();
			return TRUE;
		}
		catch (Exception $e)
		{
			$connection->rollBack();
			return FALSE;
		}
	}
}