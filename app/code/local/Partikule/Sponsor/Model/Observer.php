<?php

class Partikule_Sponsor_Model_Observer extends Mage_Core_Model_Abstract
{

    const SPONSOR_MAX_COUPONS_AMOUNT = 		30;

    const REFERRAL_MIN_ORDER_AMOUNT = 80;

    private static $_SPONSOR_COUPONS_SOURCES = array('godson_order', 'godson_kit');


    /**
	 * Admin Cusotmer save event observer
	 * Saves credits set by Admin (relative to the current number of user's credits
	 *
	 * @param $observer
	 */
	public function adminSaveCustomer($observer)
	{
		$customer = $observer->getEvent()->getCustomer();

		try
		{
			$nb_credits =  Mage::app()->getRequest()->getPost('add_credits');

			// Mage::log('adminSaveCustomer() : ' . $nb_credits , null, 'partikule.log');
			// Mage::log($customer->getEmail(), null, 'partikule.log');

			if ( ! empty($nb_credits) )
			{
				Mage::getModel('sponsor/sponsor')->addAdminCredit($customer, $nb_credits);
			}

		}
		catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}
	}


	/**
	 * Remove Usage of Credits on one quote
	 *
	 * @param $observer
	 */
	public function cancelCartCredit($observer)
	{
		$cart = $observer->getEvent()->getCart();
		$quote = $cart->getQuote();

		// Resets Quote Credit (Only for sponsor table and quote "credit_used" attribute
		// Removed 2015.01.28 : Credits are removed
		// Mage::getModel('sponsor/credit')->cancelQuoteCredits($quote);

		$items = $quote->getAllVisibleItems();

		$nb_credit_to_restore = 0;

		// Remove Credits in Cart's Products
		foreach ($items as $item)
		{
			// Be sure we have the definitive item
			$item = $item->getParentItem() ? $item->getParentItem() : $item ;

			if ($this->_can_credit_be_removed($item))
			{
				$nb_credit_to_restore += $this->_remove_item_credit_use($item, $cart);
			}
			else
			{
				Mage::log($item->getSku() .' not elligible', null, 'partikule.log');
			}
		}

		$quote->save();
	}


	/**
	 * Called by the event "apply_cart_credit"
	 *
	 * @param $observer
	 *
	 * @return mixed
	 */
	public function applyCartCredit($observer)
	{
		$cart = $observer->getEvent()->getCart();
		$quote = $cart->getQuote();
		$nb_used_credits = 0;

		// Resets Quote Credit (Only for sponsor table and quote "credit_used" attribute
		Mage::getModel('sponsor/credit')->cancelQuoteCredits($quote);

		// Customer credits
		$nb_available_credits = Mage::getModel('sponsor/credit')->getCustomerNbCredit($quote->getCustomer());

		$items = $quote->getAllVisibleItems();

		foreach ($items as $item)
		{
			// Be sure we have the definitive item
			$item = $item->getParentItem() ? $item->getParentItem() : $item ;

			if ($this->_can_credit_be_added($item))
			{
				$nb_items = $item->getQty();

				for($i=0; $i<$nb_items; $i++)
				{
					if ($nb_available_credits > 0)
					{
						$p = Mage::getModel('catalog/product')->load($item->getProductId());

						$p->addCustomOption('credit_used', serialize('1'));

						$item_added = $quote->addProduct($p, 1);
						$item_added->setCustomPrice(0);
						$item_added->setOriginalCustomPrice(0);
						$item_added->getProduct()->setIsSuperMode(true);

						if( $item->getQty() == 1 )
						{
							$cart->removeItem($item->getItemId());
							$quote->save();
						}
						else
						{
							$item->setQty($item->getQty() - 1);
							$quote->save();
						}

						$nb_used_credits++;
						$nb_available_credits--;
					}
				}
			}
		}

		if ($nb_used_credits > 0)
		{
			Mage::getModel('sponsor/credit')->useCreditsInQuote($quote, $nb_used_credits);

			$cart->save();
		}

		return $observer;
	}


	public function cancelQuoteItemCredit($observer)
	{
		$item = $observer->getQuoteItem();

		if ($item && $this->_can_credit_be_removed($item))
		{
			$qty = $item->getQty();

			Mage::getModel('sponsor/credit')->cancelQuoteCredits($item->getQuote(), $qty);
		}
	}


    /**
     * Creates potentially one coupon code for the godfather
     *
     * Event : sales_order_place_after
     * Set in : app/code/local/Partikule/Sponsor/etc/config.xml
     *
     * @param $observer
     *
     */
    public function createPotentialSponsorCoupon($observer)
    {
        $order = $observer->getOrder();

        if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE)
        {
            $noseUser = Mage::getModel('mynose/user')->getNoseUserFromMagentoId($order->getCustomerId());

            if ( ! empty($noseUser) && !empty($noseUser['id_sponsor']))
            {
                $sponsor = Mage::getModel('mynose/user')->getNoseUserFromId($noseUser['id_sponsor']);

                if ( ! empty($sponsor))
                {
                    $amount = 0;
                    $order_coupon_exists = FALSE;

                    $sponsor_coupons = Mage::getModel('mynose/user')->getNoseUserCoupons($sponsor['id_user']);

                    foreach($sponsor_coupons as $coupon)
                    {
                        if (in_array($coupon['source'], self::$_SPONSOR_COUPONS_SOURCES))
                        {
                            $amount += $coupon['amount'];
                        }

                        // If one coupon has already be generated for this order (no matter what kind of coupon)
                        if ($coupon['id_source'] == $order->getId())
                            $order_coupon_exists = TRUE;
                    }


                    if ($amount < self::SPONSOR_MAX_COUPONS_AMOUNT && $order_coupon_exists == FALSE)
                    {
                        $couponCode = NULL;
                        $source = 'godson_order';

                        if ($order->getSubtotal() >= self::REFERRAL_MIN_ORDER_AMOUNT)
                        {
                            $couponCode = $this->createReferralCoupon(
                                '_Master_Godson_Order',
                                'Godson_Order_' . $order->getId() . '_'
                            );
                        }
                        else
                        {
                            $found = FALSE;

                            // Try to find the Bundle ID
                            foreach($order->getItemsCollection() as $item)
                            {
                                if ($item->getProductId() == Mage::getStoreConfig('nose/general/bundle_product_id'))
                                {
                                    $found = TRUE;
                                    break;
                                }
                            }

                            if ($found == TRUE)
                            {
                                $couponCode = $this->createReferralCoupon(
                                    '_Master_Godson_Kit',
                                    'Godson_Order_' . $order->getId() . '_'
                                );

                                $source = 'godson_kit';
                            }
                        }

                        if ( ! is_null($couponCode))
                        {
                            $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
                            $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());

                            Mage::getModel('mynose/user')->insertNoseUserCouponFromOrder(
                                $sponsor['id_user'],
                                $couponCode,
                                $rule->getDiscountAmount(),
                                $source,
                                $order->getId(),
                                $noseUser['id_user']
                            );
                        }
                    }
                }
            }
        }
    }

    public function createReferralCoupon($masterName, $rulePrefix)
    {
        $uniqueId = $this->_generateUniqueCouponCode(10);

        // Only executed if Master Rule found
        $rule = Mage::getModel('salesrule/rule')->load($masterName, 'name');
        if (!$rule || ! $rule->getId())
            return FALSE;

        $rule->setId(null);
        $rule->setName($rulePrefix . $uniqueId);
        $rule->setDescription($masterName);
        $rule->setFromDate(date('Y-m-d'));
        $rule->setCouponCode($uniqueId);
        $rule->setUsesPerCoupon(1);
        $rule->setUsesPerCustomer(1);
        $rule->setCouponType(2);
        $rule->setIsActive(1);

        $rule->save();

        return $uniqueId;
    }


    /**
     * @param null $length
     * @return mixed|string
     */
    private function _generateUniqueCouponCode($length = null)
    {
        $rndId = crypt(uniqid(rand(),1));
        $rndId = strip_tags(stripslashes($rndId));
        $rndId = str_replace(array(".", "$"),"",$rndId);
        $rndId = strrev(str_replace("/","",$rndId));
        if (!is_null($rndId)){
            $rndId = strtoupper(substr($rndId, 0, $length));
        }
        else {
            $rndId = strtoupper($rndId);
        }

        if ( FALSE == $this->_isCouponCodeUnique($rndId))
            return $this->_generateUniqueCouponCode($length);

        return $rndId;
    }


    /**
     * Returns TRUE if no coupon with this code was found
     *
     * @param $couponCode
     * @return bool
     */
    private function _isCouponCodeUnique($couponCode)
    {
        $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');

        if ( $coupon->hasData() )
            return FALSE;

        return TRUE;
    }






    /**
	 * Removes credits used by one product in the cart
	 *
	 * @param $item
	 * @param $cart
	 *
	 * @return int		Nb of credits to restore fot this item
	 *
	 */
	private function _remove_item_credit_use($item, $cart=NULL)
	{
		$nb_credit_to_restore = 0;

		if (is_null($cart))
			$cart = Mage::getSingleton('checkout/cart');

		$quote = $cart->getQuote();
		$item_product = $item->getProduct();
		$option = $item_product->getCustomOption('credit_used');

		if ( ! empty($option))
		{
			$nb_items = $item->getQty();

			for($i=0; $i<$nb_items; $i++)
			{
				$p = Mage::getModel('catalog/product')->load($item->getProductId());
				$item_added = $quote->addProduct($p, 1);

				if( $item->getQty() == 1 ){
					$cart->removeItem($item->getItemId());
					$cart->save();
				}
				else
				{
					$item->setQty($item->getQty() - 1);
					$cart->save();
				}

				$nb_credit_to_restore++;
			}
		}

		return $nb_credit_to_restore;
	}


	private function _can_credit_be_removed($item)
	{
		$sku = $item->getSku();
		$item_product = $item->getProduct();
		$additionalOption = $item_product->getCustomOption('credit_used');

		if (substr($sku, 0, 2) == 'S-' && ! empty($additionalOption))
		{
			$value = unserialize($additionalOption->getValue());

			if ($value == 1) return TRUE;
		}

		return FALSE;
	}


	private function _can_credit_be_added($item)
	{
		$sku = $item->getSku();
		$item_product = $item->getProduct();
		$additionalOption = $item_product->getCustomOption('credit_used');

		if (substr($sku, 0, 2) == 'S-' && empty($additionalOption))
		{
			return TRUE;
		}

		return FALSE;
	}
}