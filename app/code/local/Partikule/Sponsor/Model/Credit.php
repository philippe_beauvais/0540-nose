<?php

/**
 *
 *
 * Actions
 * 'get' :    	the user got credits, usually from sponsorship validation
 * 'use' : 		the user uses credits
 * 'admin' : 	the admin sets credits to user
 */

class Partikule_Sponsor_Model_Credit extends Partikule_Sponsor_Model_Abstract
{
	public static $_CREDIT_USE = 'use';
	public static $_CREDIT_CANCEL = 'cancel';
	public static $_CREDIT_GET = 'get';
	public static $_CREDIT_ADMIN = 'admin';

	public static $_MAX_CREDITS_YEAR = 0;

	public function __construct()
	{
		parent::__construct();

		// Magento Sponsor tables
		$this->partikule_sponsorship = Mage::getSingleton('core/resource')->getTableName('sponsor/partikule_sponsorship');
		$this->partikule_sponsorship_data = Mage::getSingleton('core/resource')->getTableName('sponsor/partikule_sponsorship_data');

		// Get Customer Table from customer
		$this->customer_table = Mage::getSingleton('core/resource')->getTableName('customer/entity');

		$max_credits = Mage::getStoreConfig('sponsor/default/partikule_sponsor_max_credit', Mage::app()->getStore()->getId());

		if (!empty($max_credits))
			self::$_MAX_CREDITS_YEAR = $max_credits;
	}


	/**
	 * REturns FALSE is not, TRUE if YES
	 * @param $sponsor
	 *
	 * @return bool
	 */
	public function isSponsorAboveCreditLimit($sponsor)
	{
		if ($this->getCustomerNbCredit($sponsor) > self::$_MAX_CREDITS_YEAR)
			return TRUE;

		$this->setConnection('core_read', 'core_write');
		$connection = $this->getReadConnection();

		$sql = "
			select
				sum(credit_change) as nb,
				DATE(min(ref_date)) as ref_date
			from partikule_sponsorship_data
			WHERE customer_id = ".$sponsor->getId()."
			and ref_date >= (
				select
				date_add(
					DATE(min(ref_date)),
					INTERVAL (YEAR(CURRENT_DATE)-YEAR(DATE(min(ref_date)))) - (RIGHT(CURRENT_DATE,5)<RIGHT(DATE(min(ref_date)),5)) YEAR
				)
				from partikule_sponsorship_data
				WHERE customer_id = ".$sponsor->getId()."
			)
			group by customer_id
		";

		$query = $connection->query($sql);
		$result = $query->fetch();

		return ($result['nb'] > self::$_MAX_CREDITS_YEAR);
	}


	/**
	 * Returns the number of credits one user has
	 *
	 * @param $user
	 *
	 * @return int
	 */
	public function getCustomerNbCredit($user)
	{
		$nb = 0;

		// Use Magento Connexion
		$this->setConnection('core_read', 'core_write');

		$select = $this->getReadConnection()->select()
			->from( array('sponsorship' => $this->partikule_sponsorship))
			->where('sponsorship.customer_id = ?', $user->getId());
		;

		$query = $select->query();

		$result = $query->fetch();

		if ( ! empty($result['credits']))
			$nb = $result['credits'];

		return $nb;
	}


	/**
	 * Ge the whole history for one customer
	 *
	 * @param $user		Object. Magento customer
	 *
	 * @return array
	 */
	public function getCreditHistory($user)
	{
		// Use Magento Connexion
		$this->setConnection('core_read', 'core_write');

		$firstnameAttr = Mage::getResourceSingleton('customer/customer')->getAttribute('firstname');
		$lastnameAttr  = Mage::getResourceSingleton('customer/customer')->getAttribute('lastname');

		$select = $this->getReadConnection()->select()
			->from( array('s' => $this->partikule_sponsorship_data))
			->joinLeft(array('c' => $this->customer_table),
				"(c.entity_id = s.sponsored_id)"
			)
			->joinLeft(array('ce1' => 'customer_entity_varchar'),
				'(ce1.entity_id = c.entity_id AND ce1.attribute_id = '.$firstnameAttr->getAttributeId().')',
				array('firstname' => 'value'))
			->joinLeft(array('ce2' => 'customer_entity_varchar'),
				'(ce2.entity_id = c.entity_id AND ce2.attribute_id='.$lastnameAttr->getAttributeId().')',
				array('lastname' => 'value'))
			->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS fullname"))
			->where('s.customer_id = ?', $user->getId())
			->order('s.ref_date DESC');
		;

		$query = $select->query();
		$result = $query->fetchall();

		foreach($result as $key => $credit)
		{
			$result[$key]['reason'] = $this->_getCreditReason($credit);
			$result[$key]['variation'] = $this->_getCreditVariation($credit);
		}

		// Variation
		return $result;
	}

	private function _getCreditReason($credit)
	{
		// Reason
		$reason = '';
		if ($credit['action'] == 'get')
			$reason = Mage::helper('sponsor')->__('Sponsorship accepted') . ' : <b>'. $credit['fullname']. '<b>';

		if ($credit['action'] == 'admin')
			$reason = Mage::helper('sponsor')->__('The Nose Team set your credits');

		if ($credit['action'] == 'use')
		{
			if ( ! empty($credit['order_id']))
				$reason = Mage::helper('sponsor')->__('Used in order n° ') . $credit['order_id'];

			if ( ! empty($credit['quote_id']))
				$reason = Mage::helper('sponsor')->__('Used in Cart');
		}
		if ($credit['action'] == 'cancel')
		{
			$reason = Mage::helper('sponsor')->__('Usage canceled');
		}

		return $reason;
	}

	private function _getCreditVariation($credit)
	{
		if ($credit['credit_change'] < 0)
			$var = $credit['credit_change'];
		else
			$var = '+' . $credit['credit_change'];

		return $var;
	}


	/**
	 * Cancels Quote Credits
	 * @notice : Only on the Sponsor side. See Observers to cancel them in the quote
	 *
	 * @param $quote
	 * @param $nb_credit
	 */
	public function cancelQuoteCredits($quote, $nb_credit=NULL)
	{
		if (is_null($nb_credit))
			$nb_credit = $this->getNbCreditUsedInQuote($quote);

		// Mage::log('Nb Credits : ' . $nb_credit, null, 'partikule.log');

		if ($nb_credit > 0)
		{
			$customer = $quote->getCustomer();

			$nb = (abs($nb_credit));

			$data = array(
				'action' => self::$_CREDIT_CANCEL,
				'customer_id' => $customer->getId(),
				'credit_change' => $nb,
				'quote_id' => $quote->getId(),
			);

			$result =  Mage::getModel('sponsor/sponsor')->writeSponsorshipData($data);

			// Relative Credit update
			if ($result)
			{
				Mage::getModel('sponsor/sponsor')->updateSponsorCredits($customer, $nb);
			}
		}
	}


	/**
	 * Use of credits in one Quote
	 *
	 * @param $quote
	 * @param $nb_credits
	 *
	 */
	public function useCreditsInQuote($quote, $nb_credits)
	{
		$nb = -(abs($nb_credits));

		$customer = $quote->getCustomer();

		$data = array(
			'action' => self::$_CREDIT_USE,
			'customer_id' => $customer->getId(),
			'credit_change' => $nb,
			'quote_id' => $quote->getId(),
		);

		$result =   Mage::getModel('sponsor/sponsor')->writeSponsorshipData($data);

		// Relative Credit update
		if ($result)
		{
			Mage::getModel('sponsor/sponsor')->updateSponsorCredits($customer, $nb);
		}
	}


	/**
	 * Returns TRUE if quote has products with credits used on it.
	 * FALSE if not
	 *
	 * @param $quote
	 *
	 * @return bool
	 */
	public function areCreditUsedInQuote($quote)
	{
		$items = $quote->getAllVisibleItems();

		foreach ($items as $item)
		{
			// Be sure we have the definitive item
			$item = $item->getParentItem() ? $item->getParentItem() : $item ;
			$item_product = $item->getProduct();
			$additionalOption = $item_product->getCustomOption('credit_used');

			if (! empty($additionalOption))
			{
				$value = unserialize($additionalOption->getValue());

				if ($value == 1) return TRUE;
			}
		}
		return FALSE;
	}


	public function getNbCreditUsedInQuote($quote)
	{
		$result = 0;

		$items = $quote->getAllVisibleItems();

		foreach ($items as $item)
		{
			// Be sure we have the definitive item
			$item = $item->getParentItem() ? $item->getParentItem() : $item ;
			$item_product = $item->getProduct();
			$additionalOption = $item_product->getCustomOption('credit_used');

			if ( ! empty($additionalOption))
			{
				$value = unserialize($additionalOption->getValue());

				if ($value == 1)
				{
					$nb_items = $item->getQty();

					for($i=0; $i<$nb_items; $i++)
					{
						$result++;
					}
				}
			}
		}
		return $result;
	}
}
