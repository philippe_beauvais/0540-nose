<?php

/**
 * Adminhtml customer sponsor tab
 *
 */
class Partikule_Sponsor_Block_Adminhtml_Customer_Edit_Tab_Sponsor
	extends Mage_Adminhtml_Block_Template
	implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

	public function __construct()
	{
		$this->setTemplate('partikule/sponsor/customer/edit/sponsor.phtml');

	}

	public function getCredits()
	{
		$customer = Mage::registry('current_customer');

		return Mage::helper('sponsor/credit')->getCredits($customer);
	}

	public function getHistory()
	{
		$customer = Mage::registry('current_customer');

		$credits = Mage::getModel('sponsor/credit')->getCreditHistory($customer);

		// Add the diag of each grandson
		foreach($credits as $key => $credit)
		{
			if ( ! empty($credit['sponsored_id']))
			{
				$credits[$key]['has_bought'] = FALSE;

				$godson = Mage::getModel('customer/customer')->load($credit['sponsored_id']);
				$credits[$key]['diag'] = Mage::getModel('mynose/diagnostic')->getCustomerMasterDiag($godson);
				$has_bought = Mage::getModel('mynose/customer')->hasBoughtLastDiagSampleKit($godson);

				$credits[$key]['has_bought'] = $has_bought;
			}
		}


		return $credits;
	}

	private function _getCreditReason($credit)
	{
		// Reason
		$reason = '';
		if ($credit['action'] == 'get')
			$reason = $this->__('Sponsorship of %s was accept', $credit['fullname']);

		if ($credit['action'] == 'admin')
			$reason = $this->__('The Nose Team set your credits');

		if ($credit['action'] == 'use')
		{
			if ( ! empty($credit['order_id']))
				$reason = $this->__('Used in order n° ') . $credit['order_id'];

			if ( ! empty($credit['quote_id']))
				$reason = $this->__('Used in Cart');
		}
		if ($credit['action'] == 'cancel')
		{
			$reason = $this->__('Usage canceled');
		}
		return $reason;

	}


	// public function
/*	public function getCustomtabInfo()
	{
		$customer = Mage::registry('current_customer');
		$customtab='My Custom tab Action Contents Here';
		return $customtab;
	}*/

	/**
	 * Return Tab label
	 *
	 * @return string
	 */
	public function getTabLabel()
	{
		return $this->__('Sponsor');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 */
	public function getTabTitle()
	{
		return $this->__('Action Tab');
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 */
	public function canShowTab()
	{
		$customer = Mage::registry('current_customer');
		return (bool)$customer->getId();
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 */
	public function isHidden()
	{
		return false;
	}

	/**
	 * Defines after which tab, this tab should be rendered
	 *
	 * @return string
	 */
	public function getAfter()
	{
		return 'tags';
	}

}
