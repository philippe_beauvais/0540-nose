<?php


class Partikule_Sponsor_Block_Credit extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

	/**
	 * Returns the number of credits of the current customer
	 *
	 * @return mixed
	 */
	public function getNbCredits()
	{
		return Mage::helper('sponsor/credit')->getCredits();
	}


	/**
	 * Returns Array of credits history for the current customer
	 *
	 * @return mixed
	 */
	public function getCreditHistory()
	{
		// Current Magento user
		$customer = Mage::getModel('customer/session')->getCustomer();

		$credits = Mage::getModel('sponsor/credit')->getCreditHistory($customer);

		return $credits;
	}


	public function areCreditUsedInQuote()
	{
		$quote = Mage::getSingleton('checkout/cart')->getQuote();

		return Mage::getModel('sponsor/credit')->areCreditUsedInQuote($quote);
	}

}