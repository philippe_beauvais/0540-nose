<?php
/**
 * Partikule Sponsor URL Helper
 *
 *
 */
class Partikule_Sponsor_Helper_Url extends Mage_Core_Helper_Abstract
{
	public function getSponsorUrl(){ return Mage::getBaseUrl(). 'sponsor';}

	public function getSponsorInvitationUrl(){ return Mage::getBaseUrl(). 'sponsor/invite';}

}

