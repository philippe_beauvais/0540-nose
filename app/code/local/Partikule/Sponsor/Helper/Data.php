<?php
/**
 * Autologin
 *
 * @category    Partikule
 * @package     Partikule_Sponsor
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

class Partikule_Sponsor_Helper_Data extends Mage_Core_Helper_Abstract {


	/**
	 * Returns TRUE if the module is enabled
	 *
	 * @return boolean
	 */
	public function is_module_enabled()
	{
		return  Mage::getStoreConfig('customer/sponsor/enabled');
	}


}
