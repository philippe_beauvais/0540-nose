<?php
/**
 * Partikule Sponsor Credit Helper
 *
 *
 */
class Partikule_Sponsor_Helper_Credit extends Mage_Core_Helper_Abstract
{
	public function getCredits($customer = NULL)
	{
		if (is_null($customer))
			$customer = Mage::getModel('customer/session')->getCustomer();

		return Mage::getModel('sponsor/credit')->getCustomerNbCredit($customer);
	}
}

