<?php
/**
 * Sponsor Module (draft)
 *
 * @category    Partikule
 * @package     Partikule_Sponsor
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

$installer = $this;
$installer->startSetup();

$installer->removeAttribute('customer', 'sponsor_resolved' );
$installer->removeAttribute('customer', 'sponsor_id' );



$installer->endSetup();
