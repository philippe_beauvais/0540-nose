<?php
/**
 * Partikule Sponsor
 *
 * @category    Partikule
 * @package     Partikule_Sponsor
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

$installer = $this;
$installer->startSetup();

/**
 * Sponsorship tables
 *
 */
$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('partikule_sponsorship')} (
  customer_id int(11) unsigned NOT NULL,
  credits int(11) unsigned NOT NULL default 0,
  created_date datetime NULL,
  updated_date datetime NULL,
  PRIMARY KEY (customer_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('partikule_sponsorship_data')} (
  customer_id int(11) unsigned NOT NULL,
  sponsored_id int(11) unsigned NULL,
  ref_date datetime NULL,
  credit_change int(11) NOT NULL default 0,
  action enum('use','cancel','get', 'admin') DEFAULT 'use',
  quote_id int(11) unsigned NULL,
  order_id int(11) unsigned NULL,
  comment varchar(255) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");


/**
 * Customer attributes
 * Define if the sponsorship is resolved or not.
 *
 */
$custEntityTypeId = $installer->getEntityTypeId('customer');
$custAttributeSetId = $installer->getDefaultAttributeSetId($custEntityTypeId);
$custAttributeGroupId = $installer->getDefaultAttributeGroupId($custEntityTypeId, $custAttributeSetId);

$installer->removeAttribute('customer', 'sponsor_resolved' );
$installer->removeAttribute('customer', 'sponsor_id' );

$installer->addAttribute(
	$custEntityTypeId,
	'sponsor_resolved',
	array(
		'backend' => '',
		'frontend' => '',
		'label' => 'Sponsorship Resolved',
		'input' => 'text',
		'type'  => 'varchar',
		'required' => 0,
		'user_defined' => 0,
		'unique' => 0,
	)
);
$installer->addAttributeToGroup($custEntityTypeId, $custAttributeSetId, $custAttributeGroupId, 'sponsor_resolved', 0);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'sponsor_resolved');
//$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();


$installer->addAttribute(
	$custEntityTypeId,
	'sponsor_id',
	array(
		'backend' => '',
		'frontend' => '',
		'label' => 'Sponsor ID',
		'input' => 'text',
		'type'  => 'varchar',
		'required' => 0,
		'user_defined' => 0,
		'unique' => 0,
	)
);
$installer->addAttributeToGroup($custEntityTypeId, $custAttributeSetId, $custAttributeGroupId, 'sponsor_id', 0);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'sponsor_id');
//$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();


$installer->endSetup();