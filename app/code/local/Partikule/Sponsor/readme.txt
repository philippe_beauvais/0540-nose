-----------------------------------
Partikule Sponsor module
-----------------------------------

// Check if the module is installed
select * from core_resource where code = 'partikule_sponsor_setup';

// Check if the customers attributes are installed
select * from eav_attribute where attribute_code = 'sponsor_id';
select * from eav_attribute where attribute_code = 'sponsor_resolved';
