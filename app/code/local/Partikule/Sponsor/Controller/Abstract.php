<?php

abstract class Partikule_Sponsor_Controller_Abstract extends Mage_Core_Controller_Front_Action
{
	
	const DISPATCH_PATTERN = '/^(create|login|logoutSuccess|forgotpassword|forgotpasswordpost|confirm|confirmation)/i';
	
    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     *
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     *
     * http://stackoverflow.com/questions/3774900/magento-extending-customer-account-controller-to-add-actions-to-the-forgot-pass
     */
	
    public function preDispatch()
    {
        // a brute-force protection here would be nice

        parent::preDispatch();

        $action = $this->getRequest()->getActionName();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $pattern = self::DISPATCH_PATTERN;
        
		// If not one one this pattern, and user not logged in
        if (!preg_match($pattern, $action))
        {
            if ( ! $this->_getSession()->authenticate($this))
            {
                $this->setFlag('', 'no-dispatch', true);
            }
        }
        else
        {
            $this->_getSession()->setNoReferer(true);
        }
    }

    /**
     * Action postdispatch
     *
     * Remove No-referer flag from customer session after each action
     */
    public function postDispatch()
    {
        parent::postDispatch();
    }

	
	public function sendXhrResponse($response)
	{
		if (is_array($response))
			$response = json_encode($response);
		
		echo($response);
		die();
	}
	
	
	/**
	 * Returns the current Magento Installation "Nose Store ID"
	 * Each online webstore sending data to eNose MUST have its own store ID
	 *
	 */
	protected function _getStoreId()
	{
		return Mage::getStoreConfig('mynose/settings/nose_store_id');
	}


	/**
	 * Registers all passed data
	 *
	 * @param 	Mixed 		Array or collection of data to register
	 *
	 */
	protected function _registerData($data = array())
	{
		if ( ! empty($data))
		{
			// Only register post value to make them available for the template
			foreach($data as $key => $value)
			{
				Mage::register($key, $value);
			}
		}
	}
	


	/**
	 * Registers all passed data and
	 * Renders the template defined in diagnose.xml for the called controller
	 *
	 */
	protected function _renderLayout($layout = NULL)
	{
		if ( ! is_null($layout))
		{
			$this->loadLayout();
			$this->getLayout()->getBlock('root')->setTemplate($layout);
			$this->renderLayout();
		}			
	}
	

	protected function _checkAntispam($post)
	{
		return (Mage::getStoreConfig('mynose/settings/antispam_hash') == $post[Mage::getStoreConfig('mynose/settings/antispam_field')]);
	}


}

