<?php

class Partikule_Boutique_ProductController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Just do nothing
	 *
	 */
	public function indexAction()
	{
		echo('');
	}


	/**
	 * Returns one attribute name / value filtered product list
	 *
	 * Called through XHR
	 *  
	 * $this->loadLayout() loads "root" defined in "nose/default/layout/boutique.xml" through : attribute_product_getlist
	 *
	 * Gets through POST : 
	 *
	 * - category_id : 		Category ID
	 * - attribute : 		Attribute name
	 * - attribute_value : 	Attribute Value
	 * - ... : 				All other args 
	 *
	 * @usage : In templates : 
	 *			 	new Ajax.Updater(
	 *					'productList', 
	 *					'boutique/product/getproducts', 
	 *					{
	 *						'method': 'post',
	 *						'parameters': {
	 *							'category_id': 	'<category_id>',
	 *							'attribute': 	'<attribute code>',
	 *							'value': 		'<attribute value>'
	 *						} 
	 *					}
	 *				);
	 *
	 *
	 */
	public function getProductsAction()
	{
		$this->_processXhrPost();
	}
	
	public function getLowestInfosAction()
	{
		//$start = microtime();

		if ($this->getRequest()->isPost())
		{
			// Mage::log($this->getRequest()->getPost(), null, 'partikule.log');
			$pids = $this->getRequest()->getPost('pids');

			if (is_array($pids))
			{
				$collection = Mage::getModel('boutique/product')->getCheapestSimples($pids);

				$products = array();
				foreach($pids as $master_id)
				{
					$products[$master_id] = array(
						'id' => '',
						'parentId' => $master_id,
						'sku' => '',
						'price' => '',
						'color' => ''
					);
				}

				foreach($collection as $product)
				{
					$product->load($product->getId());
					$info = array(
						'id' => $product->getId(),
						'parentId' => $product->getParentId(),
						'sku' => $product->getSku(),
						'price' => $product->getFormatedPrice(),
						'color' => $product->getAttributeText('LIGHTSPEED_PRODUCT_COLOR'),
						'size' => $product->getAttributeText('LIGHTSPEED_PRODUCT_SIZE')
					);
					$products[$product->getParentId()] = $info;
				}
				echo (json_encode(array_values($products)));
				die();
			}
			else
			{
				$page = Mage::registry('page');
				if ($page->getTitle())
				{
					Mage::log('Partikule_Boutique_ProductController : No PIDS sent for page :'.$page->getTitle() , null, 'partikule.log');
				}
			}

		}
		// $end = microtime();
		// Mage::log(getDiff($start, $end), null, 'partikule.log');
	}
	

	public function getBestsellerAction()
	{
		$this->_processXhrPost();
	}
	
	public function getMostviewedAction()
	{
		$this->_processXhrPost();
	}
	

	/**
	 * Generic function to process POST data from XHR
	 * Loads the layout linked to the caller function
	 * and register post data
	 *
	 */
	protected function _processXhrPost()
	{
		if ($this->getRequest()->isPost())
		{
			// get the attribute acting as root filter
			$main_attribute = Mage::getStoreConfig('boutique/settings/attribute');

			Mage::register('main_attribute', $main_attribute);

			// Mage::log($this->getRequest()->getPost(), null, 'partikule.log');

			// Only register post value to make them available for the template
			foreach($this->getRequest()->getPost() as $key=>$value)
			{
				Mage::register($key, $value);
			}
			
			// Renders the layout defined in "attribute.xml" for this method
			$this->loadLayout()->renderLayout();
		}
	}
}