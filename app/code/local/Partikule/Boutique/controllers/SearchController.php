<?php
/**
 * Magento
 *
 */

/**
 * Catalog Search Controller
 *
 */
class Partikule_Boutique_SearchController extends Mage_Core_Controller_Front_Action
{

    public function resultAction()
    {
        $this->loadLayout();
        
        try
        {
// Boutique's internal search engine
// Extended form standard one, but doesn't work
//
//          Mage::getSingleton('boutique/catalogsearch_advanced')->addFilters($this->getRequest()->getQuery());
//          Mage::getSingleton('catalogsearch/advanced')->addFilters($this->getRequest()->getQuery());
            Mage::getSingleton('boutique/search')->addFilters($this->getRequest()->getQuery());
        }
        catch (Mage_Core_Exception $e)
        {
            Mage::getSingleton('catalogsearch/session')->addError($e->getMessage());
            $this->_redirectError(Mage::getURL('*/*/noresult'));
        }
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
		

    
//        $this->getResponse()->setBody($this->getLayout()->createBlock('boutique/result')->toHtml());
    	
    }

    

    public function noresultAction()
    {
        $this->loadLayout();
        $this->getLayout();
        $this->renderLayout();
	}	

}
