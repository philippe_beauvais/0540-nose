<?php
/**
 * Product Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Boutique_Helper_Product extends Mage_Core_Helper_Abstract
{
	protected $productsCount = 10;
	

	/**
	 * Returns the cheapest simple product child of one configurable
	 *
	 */
	public function getCheapestSimple($product)
	{
		$cheapest = NULL;
		
		if ($product->getTypeId() == "simple")
		{
			if (substr($product->getSku(), 0,1) != 'S')
				return $product;
		}
		
		if($product->getTypeId() == "configurable")
		{

			$simpleProducts = $product->getTypeInstance()->getUsedProducts();
			
			if($simpleProducts)
			{
				foreach($simpleProducts as $simple)
				{
					if ( (is_null($cheapest) OR $simple->getPrice() < $cheapest->getPrice()) AND substr($simple->getSku(), 0,1) != 'S' )
						$cheapest = $simple;
				}
			}
		}

		return $cheapest;
	}
	
	

	/**
	 * Returns the new products collection of one category
	 * If no category is given, returns the complete product list from the category
	 * 
	 * @param		Mixed : Category ID or Mage_Catalog_Model_Category			Optional. Category to get the attributes values from.
	 * @param		Boolean			Return only configurable products
	 * @param		int				Number of product to return
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
/*
	public function getNewProductCollection($category = NULL, $limit_to_configurable = FALSE, $nbProducts = 10)
	{
		if ( ! is_null($category) && intval($category) != 0)
		{
			$category = Mage::getModel('catalog/category')->load($category);
			$collection = $category->getProductCollection();
		}
		else
		{
			$storeId = Mage::app()->getStore()->getId();
			$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
			Mage::getModel('catalog/layer')->prepareProductCollection( $collection );
		}
		
		$collection->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', 4);
		;

		// Takken from Mage_Catalog_Block_Product_New
		$todayDate	= Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

		$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
		$collection = $this->_addProductAttributesAndPrices($collection)
			->addStoreFilter()
			->addAttributeToFilter('news_from_date', array('or'=> array(
				0 => array('date' => true, 'to' => $todayDate),
				1 => array('is' => new Zend_Db_Expr('null')))
			), 'left')
			->addAttributeToFilter('news_to_date', array('or'=> array(
				0 => array('date' => true, 'from' => $todayDate),
				1 => array('is' => new Zend_Db_Expr('null')))
			), 'left')
			->addAttributeToFilter(
				array(
					array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
					array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
					)
			  )
			->addAttributeToFilter(array(array('attribute'=>'sku', 'nlike'=>'S-%')))
			->addAttributeToSort('news_from_date', 'desc')
			->setPageSize($nbProducts)
			->setCurPage(1)
		;
		// end.

        // Join to perfume_detail to get the perfume details
        // Not used for the moment, but keep it here
        // 
		// $collection = Mage::getModel('nose/Perfume')->joinToPerfumeDetail($collection);

		// Get the category url_path attribute
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);
		
		// Mage::log("{$productCollection->getSelect()}".PHP_EOL, null, 'sql.log');

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

		return $collection;
	}
*/
	
	
	/**
	 * Returns the new products collection, filterded by attributes
	 * 
	 * @param		Array		Array of attributes to filter on.
	 * @param		Mixed : 	Category ID or Mage_Catalog_Model_Category			Optional. Category to get the attributes values from.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
/*
	public function getFilteredProductCollection($attributes, $category = NULL, $limit_to_configurable = FALSE, $nbProducts=10)
	{
		$productCollection = $this->getProductCollection($category, $limit_to_configurable, $nbProducts);

		foreach($attributes as $key => $value)
		{
			$productCollection->addAttributeToFilter($key, $value);
		}
		
		// Mage::log("{$productCollection->getSelect()}".PHP_EOL, null, 'partikule.log');
		
		return $productCollection;
	}
*/
	
	
}