<?php
/**
 * URL Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Boutique_Helper_Url extends Mage_Core_Helper_Abstract
{


	/**
	 * Returns the last URL's segment
	 *
	 * @param	int		Mage_Core_Controller_Front_Action
	 * @return 	String
	 *
	 */
	public function getLastUriSegment(Mage_Core_Controller_Front_Action $action)
	{
		$uri = $action->getRequest()->getOriginalPathInfo();
		
		$uri = trim($uri,'/');
		
		$segments = explode('/', $uri);
		
		$segment = array_pop($segments);
		
		return $segment;
	}
}