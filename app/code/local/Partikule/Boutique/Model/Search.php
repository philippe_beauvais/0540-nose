<?php

/**
 * Search Model
 * Simple search model, developped because extending the advanced standard one doesn't work and we need category filtering !
 *
 */
class Partikule_Boutique_Model_Search extends Mage_Core_Model_Abstract
{
	
	private $_productCollection = NULL;


	/**
	 * Retrieve advanced search product collection
	 *
	 * @return Mage_CatalogSearch_Model_Mysql4_Advanced_Collection
	 */
	public function getProductCollection($first = 'nop')
	{
		if (is_null($this->_productCollection))
		{
			$storeId = Mage::app()->getStore()->getId();

			$collection = Mage::getModel('catalog/product')->getCollection()
				->setStoreId( $storeId );
	
			Mage::getModel('catalog/layer')->prepareProductCollection($collection);

			$collection->addAttributeToSelect("*");


	        // Join to perfume_detail to get the perfume details
	        $collection->getSelect()->joinLeft(
				array('pd' => 'perfume_detail'),
				"pd.nose_sku = e.sku and pd.lang='". Mage::helper('attribute/core')->getCurrentStoreCode() ."'",
				array('*')
			);

			
			if(isset($_GET['category']) && is_numeric($_GET['category']))
				$collection->addCategoryFilter(Mage::getModel('catalog/category')->load($_GET['category']),true);


			if ( ! $collection) {
				return $collection;
			}
			$this->_productCollection = $collection;
		}

		return $this->_productCollection;
	}


    /**
     * Add advanced search filters to product collection
     *
     * @param   array $values
     * @return  Mage_CatalogSearch_Model_Advanced
     */
    public function addFilters($values)
    {
        $attributes     = $this->getAttributes();
        $hasConditions  = false;
        $allConditions  = array();

        foreach ($attributes as $attribute)
        {
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            if ( ! isset($values[$attribute->getAttributeCode()])) {
                continue;
            }
            
            $value = $values[$attribute->getAttributeCode()];
			
			if ( ! is_array($value))
				$value = array($value);
			

            if ($attribute->getAttributeCode() != 'price')
            {
            	if (count($value > 1))
					$this->getProductCollection('yes')->addAttributeToFilter($attribute->getAttributeCode(), array("in" => array($value)) );
				else            		
					$this->getProductCollection('yes')->addAttributeToFilter($attribute->getAttributeCode(), $value);
	        }
	        
			// Mage::log("{$this->getProductCollection()->getSelect()}".PHP_EOL, null, 'sql.log');
        }

        return $this;
    }
	
	
    /**
     * Retrieve array of attributes used in advanced search
     *
     * @return array
     */
    public function getAttributes()
    {
        /* @var $attributes Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Collection */
        $attributes = $this->getData('attributes');
        
        if (is_null($attributes))
        {
            $product = Mage::getModel('catalog/product');
            $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addHasOptionsFilter()
                ->addDisplayInAdvancedSearchFilter()
                ->addStoreLabel(Mage::app()->getStore()->getId())
                ->setOrder('main_table.attribute_id', 'asc')
                ->load();
            foreach ($attributes as $attribute) {
                $attribute->setEntity($product->getResource());
            }
            $this->setData('attributes', $attributes);
        }
        return $attributes;
    }



}
