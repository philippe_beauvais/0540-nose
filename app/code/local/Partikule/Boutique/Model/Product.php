<?php
/**
 * Product Model
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Boutique_Model_Product extends Mage_Core_Model_Abstract
{

	/**
	 * Returns array of simples from one configurable ID array
	 * Used by Boutique to get cheapest infos through XHR
	 * See template/boutique/product_list.phtml for more infos
	 *
	 * @param	Array	Array of configurable IDs
	 *
	 *
	 */
	public function getCheapestSimples($productIds)
	{
		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
		$collection->getSelect()
			->where('e.entity_id IN (?)', $productIds);
		
		// Adds 0.04 ms to the process		
		// Mage::getModel('catalog/layer')->prepareProductCollection($collection);
		
		$simplesIds = array();
		$simples = array();
		foreach($collection as $product)
		{
			$childProduct = NULL;

			if ($product->getTypeId() == "simple")
			{
				if (substr($product->getSku(), 0,1) != 'S')
					$simples[] = $product;
			}
			else
			{
				/*
				 * Better way, but doesn't returns the correct product 
				 *
				 *
				$childProduct = $this->getChildProductWithLowestPrice($product, "finalPrice");
				
				if ($childProduct)
					$simples[] = $childProduct;
				*/
			
				$simpleProducts = $product->getTypeInstance()->getUsedProducts();
				
				if($simpleProducts)
				{
					foreach($simpleProducts as $simple)
					{
                        $simple->load($simple->getId());
                        if ($simple->getStatus() == 2) {
                            continue;
                        }
						if ( (is_null($childProduct) OR $simple->getPrice() < $childProduct->getPrice()) AND substr($simple->getSku(), 0,1) != 'S' )
						{
							// Save the Parent ID : Will be used by ProductController
							$simple->setParentId($product->getId());
							$childProduct = $simple;
						}
					}
				}
				if ( ! is_null($childProduct))
					$simples[] = $childProduct;
			}
		}
			
		return $simples;
	}
	
	
    public function getChildProducts($product, $checkSalable=true)
    {
        static $childrenCache = array();
        $cacheKey = $product->getId() . ':' . $checkSalable;

        if (isset($childrenCache[$cacheKey])) {
            return $childrenCache[$cacheKey];
        }

        // This returns also the simple which are not linked to the configurable, like samples : 
        // $childProducts = $product->getTypeInstance(true)->getUsedProductCollection($product);
        
		// This returns the correct childs products :
		// but this is slow...
        $childProducts = $product->getTypeInstance()->getUsedProducts();

        if ($checkSalable) {
            $salableChildProducts = array();
            foreach($childProducts as $childProduct) {
                if($childProduct->isSalable()) {
                    $salableChildProducts[] = $childProduct;
                }
            }
            $childProducts = $salableChildProducts;
        }

        $childrenCache[$cacheKey] = $childProducts;
        return $childProducts;
    }


    public function getChildProductWithLowestPrice($product, $priceType, $checkSalable=true)
    {
        $childProducts = $this->getChildProducts($product, $checkSalable);
        if (count($childProducts) == 0) {
            return false;
        }
        $minPrice = PHP_INT_MAX;
        $minProd = false;
        foreach($childProducts as $childProduct) {
            if ($priceType == "finalPrice") {
                $thisPrice = $childProduct->getFinalPrice();
            } else {
                $thisPrice = $childProduct->getPrice();
            }
            if($thisPrice < $minPrice) {
                $minPrice = $thisPrice;
                $minProd = $childProduct;
            }
        }
        return $minProd;
    }


}