<?php

/**
 * Catalog category
 *
 */
class Partikule_Boutique_Model_Category extends Mage_Catalog_Model_Category
{
	/**
	 * Gets one category childrens and order them by one attribute.
	 * http://www.netismine.com/magento/get-sort-category-children-as-collection
	 *
	 * @return Categories Collection
	 *
	 */
	public function getChildrenCollection($sort=true, $order='name') 
	{
		$childrenIdArray = explode(',',$this->getChildren());
		
		if ($key = array_search($this->getId(),$childrenIdArray)) {
			unset($childrenIdArray[$key]);
		}
		
		$collection = $this->getCollection()
			->addAttributeToFilter('entity_id', array('in' => $childrenIdArray))
			->addAttributeToSelect('*');
	 
		if ($sort) {
			return $collection->addOrderField($order);
		}
		
		return $collection;			
	}
	
	
	/**
	 * Adds the Category URL & Name to one product collection
	 *
	 *
	 * @param 	Product Collection
	 *
	 * @return 	Product Collection
	 *
	 * @usage	In blocks : 		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);
	 *
	 */
	public function setCategoryData($collection)
	{
		$storeId = Mage::app()->getStore()->getId();

		$res = Mage::getSingleton('core/resource');
		$eav = Mage::getModel('eav/config');

		$nameattr = $eav->getAttribute('catalog_category', 'url_path');
		$nametable = $res->getTableName('catalog/category') . '_' . $nameattr->getBackendType();
		$nameattrid = $nameattr->getAttributeId();
		
		// Name is also in the nametable : catalog_category_entity_varchar
		$nameattr2 = $eav->getAttribute('catalog_category', 'name');
		$nameattrid2 = $nameattr2->getAttributeId();

/*
		$collection->joinTable('catalog/category_product', 'product_id=entity_id', array('single_category_id' => 'category_id'), null, 'left')
			->groupByAttribute('entity_id')
			->joinTable(
				$nametable,
				'entity_id=single_category_id', 
				array('category_url' => 'value'),
				array('attribute_id'=> $nameattrid, 'store_id' => $storeId), 
				'left'
			)
			->getSelect()->columns(array('category_urls' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`$nametable`.`value` SEPARATOR '; '), '')")))
		;
*/
		$collection->joinTable('catalog/category_product', 'product_id=entity_id', array('single_category_id' => 'category_id'), null, 'left')
			->groupByAttribute('entity_id')
			->joinTable(
				array('category_url_table' => $nametable),
				'entity_id=single_category_id', 
				array('category_url' => 'value'),
				array('attribute_id'=> $nameattrid, 'store_id' => $storeId), 
				'left'
			)
			->joinTable(
				array('category_name_table' => $nametable),
				'entity_id=single_category_id', 
				array('category_name' => 'value'),
				array('attribute_id'=> $nameattrid2), 
				'left'
			)
			->getSelect()->columns(
				array
				(
					'category_urls' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`category_url_table`.`value` SEPARATOR '; '), '')"),
					'category_names' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`category_name_table`.`value` SEPARATOR '; '), '')")
				)
			)
		;
		
		return $collection;
		
	}
	
	
	/**
	 * Adds the Category Names to one product collection
	 *
	 *
	 * @param 	Product Collection
	 *
	 * @return 	Product Collection
	 *
	 * @usage	In blocks : 		$collection = Mage::getModel('boutique/Category')->setCategoryName($collection);
	 *
	 */
	public function setCategoryName($collection)
	{
		return $collection;
		
		$storeId = Mage::app()->getStore()->getId();

		$res = Mage::getSingleton('core/resource');
		$eav = Mage::getModel('eav/config');
		$nameattr = $eav->getAttribute('catalog_category', 'name');
		
		$nametable = $res->getTableName('catalog/category') . '_' . $nameattr->getBackendType();
		$nameattrid = $nameattr->getAttributeId();

		$collection->joinTable('catalog/category_product', 'product_id=entity_id', array('name_category_id' => 'category_id'), null, 'left')
		    ->groupByAttribute('entity_id')
		    ->joinTable(
		    	$nametable,
		    	"entity_id=name_category_id", 
		    	array('category_name' => 'value'),
		    	"attribute_id = $nameattrid", 'left'
		    )
		    ->getSelect()->columns(array('category_names' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`$nametable`.`value` SEPARATOR '; '), '')")))
		;
		
		return $collection;
		
	}


	/**
	 * Returns one product first category
	 *
	 * @param $product
	 *
	 * @return Mage_Core_Model_Abstract|null
	 */
	public function getProductCategory($product)
	{
		$_category = NULL;

		if ( $product)
		{
			$categoryIds = $product->getCategoryIds();

			if(count($categoryIds) )
			{
				$firstCategoryId = $categoryIds[0];
				$_category = Mage::getModel('catalog/category')->load($firstCategoryId);
			}
		}
		return $_category;
	}




	

}
