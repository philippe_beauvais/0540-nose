<?php

/**
 *
 */

class Partikule_Boutique_Block_Bestseller extends Partikule_Boutique_Block_Product
{
	/*
	 * Current attribute
	 *
	 */
	// private $_attribute = NULL;
	
	protected function _construct()
	{
		$this->addData(array(
			'cache_lifetime'	=> 120,
			'cache_tags'		=> array(Mage_Catalog_Model_Product::CACHE_TAG . "_bestsellers"),
			'cache_key'			=> 'boutique_bestsellers'
		));
	}  

	
	/**
	 * Get filtered product collection
	 * Used by the "Boutique" to filter on 2 attributes levels the products
	 *
	 * @usage	Example : 
	 *			The template "boutique/product_nav.phtml" adds : 
	 *			- the main attribute (Boutique/etc/config.xml)
	 *			- The second attribute
	 *			to the $attributes array
	 *
	 */
	public function getProductCollection($attributes = array(), $limit_to_configurable = TRUE)
	{
		// Mage::log($attributes, null, 'partikule.log');

		$storeId = Mage::app()->getStore()->getId();

		$simpleCollection = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
			->addAttributeToFilter(array(array('attribute'=>'sku', 'nlike'=>'S-%')))
            ->setOrder('ordered_qty', 'desc');

		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$simpleCollection->addAttributeToFilter($code, $value);
		}

		// Mage::log("{$simpleCollection->getSelect()}".PHP_EOL, null, 'partikule.log');

		// Now, get the corresponding configurable products
		$parentIdsArray = array();
		foreach ($simpleCollection as $simple)
		{
			$parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
			                  ->getParentIdsByChild($simple->getId());
			
			$parentIdsArray[] = $parentIds[0];
		}

		$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
		Mage::getModel('catalog/layer')->prepareProductCollection($collection);

		$collection
			->addAttributeToFilter('entity_id', array('in' => $parentIdsArray))
            ->addAttributeToSelect('*')
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
		;

		// Adds the category URL to each item of the collection
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

		return $collection;
	}
	
	
	/**
	 * Returns the most viewed product collection
	 * If no category is registered, returns the whole new product list
	 *
	 * @note		Filters products on SKU starting with "M-"
	 *
	 * @param		Int			Number of products to return.
	 * @param		Arrray		Array of attributes to filter on.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getMostViewedProductCollection($nbProducts = 10, $attributes = array())
	{
		// Mage::log($attributes, null, 'partikule.log');

		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getResourceModel('reports/product_collection')
			->addAttributeToSelect('*')
//			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'M-%')))
			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'M-%')))
			->addAttributeToFilter('type_id', 'configurable')

			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
			->setStoreId($storeId)
			->addStoreFilter($storeId)
			->addViewsCount()
			->setPageSize($nbProducts)
		;
		
		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$collection->addAttributeToFilter($code, $value);
		}

		return $collection;
	}
	
	
}