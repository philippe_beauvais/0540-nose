<?php

/**
 *
 
 Important improvment : 
 - Check for stocks ! 

$collection->getSelect()->joinLeft(      
       array('stock'=>'cataloginventory_stock_item'),     
       'stock.product_id = e.entity_id',      
       array('stock.qty', 'stock.is_in_stock')      
     );
$products->addAttributeToFilter('is_in_stock', 1)

// Get categories from product


$all_cats = $product_model->getCategoryIds($_product);
 
*/

class Partikule_Boutique_Block_Product extends Mage_Catalog_Block_Product_Abstract
{
	/*
	 * Current attribute
	 *
	 */
	private $_attribute = NULL;


	protected function _construct()
	{
/*
		Mage::log('CACHE !!! ', null, 'partikule.log');
		$this->addData(array(
			'cache_lifetime'	=> 120,
//			'cache_tags'		=> array(Mage_Catalog_Model_Product::CACHE_TAG . "_" . $this->getAttribute_code()),
			'cache_tags'		=> array(Mage_Catalog_Model_Product::CACHE_TAG . "_" ),
//			'cache_key'			=> 'boutique' . $this->getAttribute_code(),
			'cache_key'			=> 'boutique',
		));
*/
	}  

/*
	public function getCacheKey()
	{
		Mage::log('getCacheKey() called', null, 'partikule.log');
		if (!$this->hasData('cache_key')) {
			$cacheKey = get_class($this).'_STORE'.Mage::app()->getStore()->getId().$this->getAttribute_code();
			$this->setCacheKey($cacheKey);
		}
		return $this->getData('cache_key');
	}
	public function getCacheLifetime(){return 9999999999;}
	public function getCacheTags(){ return array();}
*/
	

	/**
	 * Get filtered product collection
	 * Used by the "Boutique" to filter on 2 attributes levels the products
	 *
	 * @usage	Example : 
	 *			The template "boutique/product_nav.phtml" adds : 
	 *			- the main attribute (Boutique/etc/config.xml)
	 *			- The second attribute
	 *			to the $attributes array
	 *
	 */
	public function getProductCollection($attributes = array(), $limit_to_configurable = TRUE)
	{
		// Get the attribute (code) acting as root filter
//		$mainAttributeCode = Mage::getStoreConfig('boutique/settings/attribute');

		$storeId = Mage::app()->getStore()->getId();
		$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
		Mage::getModel('catalog/layer')->prepareProductCollection($collection);

		$collection
			->addAttributeToSelect("*")
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
		;

		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$collection->addAttributeToFilter($code, $value);
		}

		// Remove Samples (échantillons)
        $collection->addAttributeToFilter(array(array('attribute'=>'sku', 'nlike'=>'S-%')));

		
        // Join to perfume_detail to get the perfume details
        // Not used for the moment, but keep it here
        // 
		// $collection = Mage::getModel('nose/Perfume')->joinToPerfumeDetail($collection);


		// Adds the category URL to each item of the collection
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

//	$collection->addMinimalPrice();

		// Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'sql.log');

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

		
		return $collection;
	}
	
	
	/**
	 * Return the current attribute
	 * attribute_code was set through its code by boutique.xml
	 *
	 * @return 	string		bla
	 *
	 *
	 */
	public function getAttribute()
	{
		return Mage::helper('attribute/data')->getAttribute($this->getAttribute_code());
	}


	
	public function getAttributesOptions($attributeCode)
	{
		$options = Mage::helper('attribute/data')->getAttributeOptions($attributeCode);
		
		return $options;
	}
	
	
	
/*
 * Moved to Attribute/Helper/Data
 *
 *
	public function getProductFilteredAttributeOptions($attribute_code, $collection, $discard = NULL)
	{
		// Array of attribute values to return
		$returned_options = array();

//		$cache = Mage::getSingleton('core/cache');
		$cache_key = "filtered_" . $attribute_code;	
//		$data_from_cache = $cache->load($cache_key);
		$data_from_cache = FALSE;
		
		if ( $data_from_cache )
		{
			$returned_options = $data_from_cache;
			Mage::log('from cache', null, 'partikule.log');
		}
		else
		{
	
Mage::log('getProductFilteredAttributeOptions() : ' . $attribute_code, null, 'partikule.log');
	
			$options = $this->getAttributeOptions($attribute_code);
			
			if ( ! empty($options))
			{
				// Product instance get method name to retrieve the Attribute value
				$getAttributeValue = 'get' . ucfirst($attribute_code);
		
				// Check if products have each value of the attribute option
				// If not, the attribute isn't returned as menu item.
				foreach($options as $key => $opt)
				{
					foreach($collection as $product)
					{
						// Get product attribute value. Can be an coma separated list of values
						$attributeValues = $product->$getAttributeValue();
						$attributeValues = explode(',', $attributeValues);
		
						foreach($attributeValues as $val)
						{
							if ($opt['option_id'] == $val && ! in_array($opt, $returned_options))
								$returned_options[] = $opt;
						}
					}
				}
				
				// Removes discarded options value if any
				if ( ! is_null($discard))
				{
					$discard = (is_array($discard)) ? $discard : array($discard);
					
					foreach($returned_options as $key => $option)
					{
						foreach($discard as $option_value)
						
						if (strtolower($option['default_value']) == strtolower($option_value))
						{
							unset($returned_options[$key]);
						}
					}
				}
			}

			// $cache->save($returned_options, $cache_key, array($cache_key), 120);
		}			
		return $returned_options;
	}
*/
	
}