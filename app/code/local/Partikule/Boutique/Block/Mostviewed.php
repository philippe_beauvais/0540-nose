<?php

/**
 *
 */

class Partikule_Boutique_Block_Mostviewed extends Partikule_Boutique_Block_Product
{
	
	protected $productCount = 20;
	
	/**
	 * Get filtered product collection
	 * Used by the "Boutique" to filter on 2 attributes levels the products
	 *
	 * @usage	Example : 
	 *			The template "boutique/product_nav.phtml" adds : 
	 *			- the main attribute (Boutique/etc/config.xml)
	 *			- The second attribute
	 *			to the $attributes array
	 *
	 */
	public function getProductCollection($attributes, $limit_to_configurable = FALSE)
	{
		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
			->addAttributeToFilter('type_id', 'configurable')

			->addViewsCount()
            ->setPageSize($this->getProductCount())
            // ->addCategoryFilter(Mage::registry('current_category'))
        ;

		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$collection->addAttributeToFilter($code, $value);
		}

		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

		// Mage::log($collection->getData(), null, 'partikule.log');
		
		return $collection;
	}


	/**
	 * Return limit
	 *
	 */
	public function getProductCount()
	{
		return $this->productCount;
	}
	




}


