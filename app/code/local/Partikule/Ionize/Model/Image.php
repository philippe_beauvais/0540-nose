<?php
/**
 *
 */

/**
 * Ionize Image model
 *
 * @category   Partikule
 * @package	   Partikule_Ionize
 * @author	   Partikule Studio
 *
 */
class Partikule_Ionize_Model_Image extends Mage_Core_Model_Abstract
{
	protected $_width;
	protected $_height;
	protected $_quality = 90;
	protected $_cropX = 'left';
	protected $_cropY = 'top';
	

	protected $_keepAspectRatio	 = true;
	protected $_keepFrame		 = true;
	protected $_keepTransparency = true;
	protected $_constrainOnly	 = true;
//	  protected $_backgroundColor  = array(255, 255, 255);
	protected $_backgroundColor	 = array(0,0,0);
	
	protected $_fitResize = FALSE;

	protected $_baseFile;
	protected $_isBaseFilePlaceholder;
	protected $_newFile;
	protected $_processor;
	protected $_destinationSubdir;


	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setWidth($width)
	{
		$this->_width = $width;
		return $this;
	}

	public function getWidth()
	{
		return $this->_width;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setHeight($height)
	{
		$this->_height = $height;
		return $this;
	}

	public function getHeight()
	{
		return $this->_height;
	}
	
	
	public function setFitResize($fit)
	{
		$this->_fitResize = $fit;
		return $this;
	}
	
	public function getFitResize()
	{
		return $this->_fitResize;
	}
	
	public function setCropX($cropX)
	{
		$this->_cropX = $cropX;
		return $this;
	}
	
	public function getCropX()
	{
		return $this->_cropX;
	}
	
	public function setCropY($cropY)
	{
		$this->_cropY = $cropY;
		return $this;
	}
	
	public function getCropY()
	{
		return $this->_cropY;
	}
	
	/**
	 * Set image quality, values in percentage from 0 to 100
	 *
	 * @param int $quality
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setQuality($quality)
	{
		$this->_quality = $quality;
		return $this;
	}

	/**
	 * Get image quality
	 *
	 * @return int
	 */
	public function getQuality()
	{
		return $this->_quality;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setKeepAspectRatio($keep)
	{
		$this->_keepAspectRatio = (bool)$keep;
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setKeepFrame($keep)
	{
		$this->_keepFrame = (bool)$keep;
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setKeepTransparency($keep)
	{
		$this->_keepTransparency = (bool)$keep;
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setConstrainOnly($flag)
	{
		$this->_constrainOnly = (bool)$flag;
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setBackgroundColor(array $rgbArray)
	{
		$this->_backgroundColor = $rgbArray;
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setSize($size)
	{
		// determine width and height from string
		list($width, $height) = explode('x', strtolower($size), 2);
		foreach (array('width', 'height') as $wh) {
			$$wh  = (int)$$wh;
			if (empty($$wh))
				$$wh = null;
		}

		// set sizes
		$this->setWidth($width)->setHeight($height);

		return $this;
	}

	protected function _checkMemory($file = null)
	{
		return $this->_getMemoryLimit() > ($this->_getMemoryUsage() + $this->_getNeedMemoryForFile($file)) || $this->_getMemoryLimit() == -1;
	}

	protected function _getMemoryLimit()
	{
		$memoryLimit = trim(strtoupper(ini_get('memory_limit')));

		if (!isSet($memoryLimit[0])){
			$memoryLimit = "128M";
		}

		if (substr($memoryLimit, -1) == 'K') {
			return substr($memoryLimit, 0, -1) * 1024;
		}
		if (substr($memoryLimit, -1) == 'M') {
			return substr($memoryLimit, 0, -1) * 1024 * 1024;
		}
		if (substr($memoryLimit, -1) == 'G') {
			return substr($memoryLimit, 0, -1) * 1024 * 1024 * 1024;
		}
		return $memoryLimit;
	}

	protected function _getMemoryUsage()
	{
		if (function_exists('memory_get_usage')) {
			return memory_get_usage();
		}
		return 0;
	}

	protected function _getNeedMemoryForFile($file = null)
	{
		$file = is_null($file) ? $this->getBaseFile() : $file;
		if (!$file) {
			return 0;
		}

		if (!file_exists($file) || !is_file($file)) {
			return 0;
		}

		$imageInfo = getimagesize($file);

		if (!isset($imageInfo[0]) || !isset($imageInfo[1])) {
			return 0;
		}
		if (!isset($imageInfo['channels'])) {
			// if there is no info about this parameter lets set it for maximum
			$imageInfo['channels'] = 4;
		}
		if (!isset($imageInfo['bits'])) {
			// if there is no info about this parameter lets set it for maximum
			$imageInfo['bits'] = 8;
		}
		return round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 1.65);
	}

	/**
	 * Convert array of 3 items (decimal r, g, b) to string of their hex values
	 *
	 * @param array $rgbArray
	 * @return string
	 */
	protected function _rgbToString($rgbArray)
	{
		$result = array();
		foreach ($rgbArray as $value) {
			if (null === $value) {
				$result[] = 'null';
			}
			else {
				$result[] = sprintf('%02s', dechex($value));
			}
		}
		return implode($result);
	}

	/**
	 * Set filenames for base file and new file
	 *
	 * @param string $file
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setBaseFile($file)
	{
		$this->_isBaseFilePlaceholder = false;

		if (($file) && (0 !== strpos($file, '/', 0))) {
			$file = '/' . $file;
		}
		$baseDir = $this->getBaseMediaPath();

		if ('/no_selection' == $file) {
			$file = null;
		}
		if ($file) {


			if ((!$this->_fileExists($baseDir . $file)) || !$this->_checkMemory($baseDir . $file)) {
				$file = null;
			}
		}
		if (!$file) {
			// check if placeholder defined in config
			$isConfigPlaceholder = Mage::getStoreConfig("catalog/placeholder/{$this->getDestinationSubdir()}_placeholder");
			$configPlaceholder	 = '/placeholder/' . $isConfigPlaceholder;
			if ($isConfigPlaceholder && $this->_fileExists($baseDir . $configPlaceholder)) {
				$file = $configPlaceholder;
			}
			else {
				// replace file with skin or default skin placeholder
				$skinBaseDir	 = Mage::getDesign()->getSkinBaseDir();
				$skinPlaceholder = "/images/catalog/product/placeholder/{$this->getDestinationSubdir()}.jpg";
				$file = $skinPlaceholder;
				if (file_exists($skinBaseDir . $file)) {
					$baseDir = $skinBaseDir;
				}
				else {
					$baseDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default'));
					if (!file_exists($baseDir . $file)) {
						$baseDir = Mage::getDesign()->getSkinBaseDir(array('_theme' => 'default', '_package' => 'base'));
					}
				}
			}
			$this->_isBaseFilePlaceholder = true;
		}

		$baseFile = $baseDir . $file;

		if ((!$file) || (!file_exists($baseFile))) {
			throw new Exception(Mage::helper('catalog')->__('Image file was not found.'));
		}

		$this->_baseFile = $baseFile;

		// build new filename (most important params)
		$path = array(
			$this->getBaseMediaPath().'/files',
			'.thumbs',
			Mage::app()->getStore()->getId()
		);

		if((!empty($this->_width)) || (!empty($this->_height)))
			$path[] = "{$this->_width}x{$this->_height}";

		// add misk params as a hash
		$miscParams = array(
				($this->_keepAspectRatio  ? '' : 'non') . 'proportional',
				($this->_keepFrame		  ? '' : 'no')	. 'frame',
				($this->_keepTransparency ? '' : 'no')	. 'transparency',
				($this->_constrainOnly ? 'do' : 'not')	. 'constrainonly',
				$this->_rgbToString($this->_backgroundColor),
				'quality' . $this->_quality
		);


//		  $path[] = md5(implode('_', $miscParams));

		// append prepared filename
		$this->_newFile = implode('/', $path) . $file; // the $file contains heading slash

		return $this;
	}
	
	/**
	 * Returns the base media path
	 * Could use Mage::getStoreConfig('ionize/settings/files_path');
	 * but it doesn't yet...
	 *
	 */
	public function getBaseMediaPath()
	{
		return Mage::getBaseDir();
	}


	public function getBaseFile()
	{
		return $this->_baseFile;
	}

	public function getNewFile()
	{
		return $this->_newFile;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setImageProcessor($processor)
	{
		$this->_processor = $processor;
		return $this;
	}

	/**
	 * @return Varien_Image
	 */
	public function getImageProcessor()
	{
		if( !$this->_processor ) {
			$this->_processor = new Varien_Image($this->getBaseFile());
		}
		$this->_processor->keepAspectRatio($this->_keepAspectRatio);
		$this->_processor->keepFrame($this->_keepFrame);
		$this->_processor->keepTransparency($this->_keepTransparency);
		$this->_processor->constrainOnly($this->_constrainOnly);
		$this->_processor->backgroundColor($this->_backgroundColor);
		$this->_processor->quality($this->_quality);
		return $this->_processor;
	}

	/**
	 * @see Varien_Image_Adapter_Abstract
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function resize()
	{
		if (is_null($this->getWidth()) && is_null($this->getHeight())) {
			return $this;
		}
		
		$w = $this->_width;
		$h = $this->_height;
		
		$processor = $this->getImageProcessor();
		
		if ($this->getFitResize() == TRUE)
		{
			if ($processor->getOriginalWidth() > $processor->getOriginalHeight())
				$w = NULL;
			else
				$h = NULL;
		}
		$processor->resize($w, $h);
		
		return $this;
	}

	/**
	 * Makes one adaptative resize, but only if the method exists in the adapter
	 * @see http://2ammedia.co.uk/web-design/magento-adaptive-resize-resize-to-best-fit
	 *
	 */
	public function adaptiveResize()
	{
		if (is_null($this->getWidth()) && is_null($this->getHeight())) {
			return $this;
		}
		if (method_exists($this->getImageProcessor(), 'adaptiveResize'))
			$this->getImageProcessor()->adaptiveResize($this->_width, $this->_height, $this->_cropX, $this->_cropY);
		
		return $this;
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function saveFile()
	{
		$filename = $this->getNewFile();
		$this->getImageProcessor()->save($filename);
//		  Mage::helper('core/file_storage_database')->saveFile($filename);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		$baseDir = Mage::getBaseDir();
		$path = str_replace($baseDir . DS, "", $this->_newFile);
		return Mage::getStoreConfig('web/unsecure/base_url') . str_replace(DS, '/', $path);
//		  return Mage::getBaseUrl() . str_replace(DS, '/', $path);
	}

	public function push()
	{
		$this->getImageProcessor()->display();
	}

	/**
	 * @return Mage_Catalog_Model_Product_Image
	 */
	public function setDestinationSubdir($dir)
	{
		$this->_destinationSubdir = $dir;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDestinationSubdir()
	{
		return $this->_destinationSubdir;
	}

	public function isCached()
	{
		return $this->_fileExists($this->_newFile);
	}

/*
	public function clearCache()
	{
		$directory = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.DS.'cache'.DS;
		$io = new Varien_Io_File();
		$io->rmdir($directory, true);

		Mage::helper('core/file_storage_database')->deleteFolder($directory);
	}
*/

	/**
	 * First check this file on FS
	 *
	 * @param string $filename
	 * @return bool
	 */
	protected function _fileExists($filename)
	{
		if (file_exists($filename)) {
			return true;
		} 
		return FALSE;
	}
}
