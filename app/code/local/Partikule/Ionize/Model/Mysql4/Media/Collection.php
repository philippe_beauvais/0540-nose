<?php

class Partikule_Ionize_Model_Mysql4_Media_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	// Lang table : config.xml
	protected $_langTable;
	
	
	/**
	 * Contructor
	 *
	 *
	 */
	public function _construct()
	{
		parent::_construct();

		$this->_init('ionize/media');
		
		// Inits the lang table name from config.xml
		$this->_langTable = $this->getResource()->getTable('ionize/media_lang');
	}
	
	
	/**
	 * Public function to join the Page table to the Page_lang table
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinLang()
	 *
	 */
	public function joinLang()
	{
		return $this->_joinLang();
	}
	
	
	/**
	 * Joins to the article_media table
	 *
	 */
	public function joinArticle()
	{
		$this->getSelect()->joinLeft(
			array('article_media' => 'article_media'),
			'main_table.id_media = article_media.id_media',
			array('*')
		);

		// Order by Ionize ordering
		$this->getSelect()->order('article_media.ordering', 'ASC');


		return $this;
	}
	
	
	/**
	 * Joins to the page_media table
	 *
	 */
	public function joinPage()
	{
		$this->getSelect()->joinLeft(
			array('page_media' => 'page_media'),
			'main_table.id_media = page_media.id_media',
			array('*')
		);

		// Order by Ionize ordering
		$this->getSelect()->order('page_media.ordering', 'ASC');
		
		// Mage::log("{$this->getSelect()}".PHP_EOL, null, 'sql.log');

		return $this;
	}
	
	
	/**
	 * Joins the Page table to the Page_lang table
	 * Called by $this->joinLang()
	 *
	 */
	protected function _joinLang()
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_media = ' . $this->_langTable . '.id_media AND ';
		$conditions .= $this->_langTable . ".lang = '" . Mage::helper('ionize/core')->getCurrentStoreCode() ."'";
		
		$this->getSelect()->joinLeft(
			array('media_lang' => $this->_langTable),
			$conditions,
			array('*')
		);

		return $this;
	}
}