<?php

class Partikule_Ionize_Model_Mysql4_Media extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Set to "id", as the media collection only exists in the context of on page
		// Means the medias are never got outside of article or page context
		$this->_init('ionize/media', 'id');
	}
	
}
