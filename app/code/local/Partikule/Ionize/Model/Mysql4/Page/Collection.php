<?php

class Partikule_Ionize_Model_Mysql4_Page_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	// Page Lang table : config.xml
	protected $_pageLangTable;
	
	// Ionize Magento Template table definition : config.xml
	protected $_magentoTemplateTable;
	
	protected $_storeCode = NULL;


	/**
	 * Contructor
	 *
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/page');
		
		// Inits the lang table name from config.xml
		$this->_initTables();
	}
	
	
	/**
	 * Adds medias to the page collection
	 *
	 */
	public function withMedias()
	{
		$collection = Mage::getModel('ionize/Media')->addToCollection($this, 'page');

		return $collection;
	}
	
	
	/**
	 * Public function to join the Page table to the type table
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinType()
	 *
	 */
	public function joinType($storeCode = NULL)
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_type = ' . $this->_typeTable . '.id_type';

		$this->getSelect()->joinLeft(
			array('type' => $this->getTable('ionize/type')),
			$conditions,
			array('type_code' => 'code')
		);

		// trace("{$this->getSelect()}".PHP_EOL);

		return $this;
	}
	
	/**
	 * Public function to join the Page table to the Page_lang table
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinLang()
	 *
	 */
	public function joinLang($storeCode = NULL)
	{
		if (is_null($storeCode))
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();
		
		return $this->_joinLang($storeCode);
	}
	
	
	
	/**
	 * Public function to join the Page table to the URL table
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinUrl()
	 *
	 */
	public function joinUrl($storeCode = NULL)
	{
		if (is_null($storeCode))
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();
		
		return $this->_joinUrl($storeCode);
	}
	
	
	/**
	 * Public function to join the Page table to the Ionize's Magento module template table
	 * Adds the 'template' field to the collection items
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinTemplate()
	 *
	 */
	public function joinTemplate()
	{
		return $this->_joinTemplate();
	}
	
	
	/**
	 * Init protected vars $_pageTable @ $_pageLangTable
	 * Get the tables names from config.xml
	 * Nice, scalable, but not so good regarding performances...
	 * 
	 * Anyway, after seen what Magento does, is it so important ?
	 *
	 */
	protected function _initTables()
	{
		$this->_pageLangTable = $this->getResource()->getTable('ionize/pagelang');
		$this->_urlTable = $this->getResource()->getTable('ionize/url');
		$this->_magentoTemplateTable = $this->getResource()->getTable('ionize/magento_template');
		$this->_typeTable = $this->getResource()->getTable('ionize/type');
	}
	
	
	/**
	 * Joins the Page table to the Page_lang table
	 * Called by $this->joinLang()
	 *
	 */
	protected function _joinLang($storeCode)
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_page = ' . $this->_pageLangTable . '.id_page AND ';
		$conditions .= $this->_pageLangTable . ".lang = '" . $storeCode ."'";

		$this->getSelect()->joinLeft(
			array('page_lang' => $this->getTable('ionize/pagelang')),
			$conditions,
			array('*')
		);

		// trace("{$select}".PHP_EOL);

		return $this;
	}
	
	
	/**
	 * Joins the Page table to the Url table
	 * Called by $this->joinUrl()
	 *
	 */
	protected function _joinUrl($storeCode)
	{

		$this->getSelect()->joinLeft(
			array('url' => $this->getTable('ionize/url')),
			'main_table.id_page = ' . $this->_urlTable . '.id_entity AND ' .
			$this->_urlTable . ".lang = '" . $storeCode ."' AND " . 
			$this->_urlTable . ".type='page' AND " .
			$this->_urlTable . ".active='1'",
			array('url.path')
		);
		// trace("{$this->getSelect()}".PHP_EOL);

		return $this;
	}
	
	
	/**
	 * Join to the Ionize's Magento Template table
	 * in order to add the magento template file to the result
	 *
	 */
	protected function _joinTemplate()
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_page = ' . $this->_magentoTemplateTable . '.id_page';

		$this->getSelect()->joinLeft(
			array($this->_magentoTemplateTable => $this->getTable('ionize/magento_template')),
			$conditions,
			array('magento_template')
		);

		return $this;
	}
}