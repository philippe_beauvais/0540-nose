<?php

/**
 * Extend field definition model
 *
 * The Ionize table "extend_field" contains the definition and default values for each extend field
 *
 */
class Partikule_Ionize_Model_Mysql4_Extend_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	// Table of extends definition
	protected $_table;
	
	// Lang table of extends definition 
	protected $_langTable;
	

	/**
	 * Contructor
	 *
	 *
	 */
	public function _construct()
	{
		parent::_construct();

		$this->_init('ionize/extend');
		
		// Inits the lang table name from config.xml
		$this->_table = $this->getResource()->getTable('ionize/extend_field');
		$this->_langTable = $this->getResource()->getTable('ionize/extend_field_lang');
	}
	
	
	/**
	 * Join to the Extend Field lang table (definition of each extend field)
	 *
	 * @return 	Collection		this collection
	 *
	 */
	public function joinLang()
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_extend_field = ' . $this->_langTable . '.id_extend_field AND ';
		$conditions .= $this->_langTable . ".lang = '" . Mage::app()->getStore()->getCode() ."'";
		
		$this->getSelect()->joinLeft(
			array('extend_field_lang' => $this->_langTable),
			$conditions,
			array('*')
		);

		return $this;
	}
	
	

}