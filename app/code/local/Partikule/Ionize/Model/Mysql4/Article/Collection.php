<?php

class Partikule_Ionize_Model_Mysql4_Article_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	// Lang table
	protected $_langTable = 	'article_lang';
	
	// Context table : Links one article to 0..n pages
	protected $_contextTable = 	'page_article';
	
	// Type table : Links one type to 0..n articles, depending on the context table type data
	// Each can have a type in the context of one page.
	// No type is a type
	protected $_typeTable = 	'article_type';
	
	
	/**
	 * Contructor
	 *
	 *
	 */
	public function _construct()
	{
		parent::_construct();

		$this->_init('ionize/article');
		
		// Inits the lang table name from config.xml
		$this->_initTables();
	}
	
	
	/**
	 * Public function to join the Article table to the Article_lang table
	 *
	 * @usage	In Helper, Controller : 
	 *			$articles = $article_model->getCollection()->joinLang()
	 *
	 */
	public function joinLang($storeCode = NULL)
	{
		if (is_null($storeCode))
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();

		return $this->_joinLang($storeCode);
	}
	
	
	/**
	 * Public function to join the Page_article table
	 * In Ionize, each article is linked to 0->n pages through this table
	 *
	 */
	public function joinPage($id_page = NULL)
	{
		return $this->_joinPage($id_page);
	}
	
	
	/**
	 * Public function to join the URL table
	 *
	 */
	public function joinUrl($storeCode = NULL)
	{
		if (is_null($storeCode))
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();

		return $this->_joinUrl($storeCode);
	}
	
	
	
	
	/**
	 * Add conditions to the Collection query
	 *
	 * @param 	array 		Array of conditions
	 *
	 */
	public function setConditions($conditions)
	{
		// Get all table join informations
		$fromPart = $this->getSelect()->getPart(Zend_Db_Select::FROM);

		foreach($conditions as $cond => $value)
		{
			switch($cond)
			{
				// If the join with page_article doesn't exists, do it
				// @TODO : Rewrite for Ionize 0.9.9
				case 'type' :
				
					if (!isset($fromPart[$this->_contextTable]))
					{
						$this->getSelect()->join(
							array($this->_contextTable => $this->getTable('ionize/' . $this->_contextTable)),
							'main_table.id_article = ' . $this->_contextTable . '.id_article',
							array('*')
						);
					}
	
					// Join on article_type
					if (!isset($fromPart[$this->_typeTable]))
					{
						$this->getSelect()->join(
							array(
								$this->_typeTable => $this->getTable('ionize/' . $this->_typeTable)		
							),
							$this->_contextTable . '.id_type = ' . $this->_typeTable . '.id_type',
							array('*')
						);
					}
					
					// '' or NULL : same same
					if ($value == '' or $value == 'NULL')
						$this->getSelect()->where($this->_typeTable . ".type = '' OR ". $this->_typeTable . ".type IS NULL");
					else
						$this->getSelect()->where($this->_typeTable . ".type = '" . $value ."'");
					
					break;
					
				case 'limit' :
					$this->setCurPage(1);
					$this->setPageSize($value);
					break;
					
				default:
					$this->addFilter($cond, $value);
			}
		}

		return $this;		
	}
	
	
	/**
	 * Init protected vars $_langTable
	 * Get the tables names from config.xml
	 * Nice, scalable, but perhaps not so good regarding performances...
	 * 
	 * Anyway, after seen what Magento does like this, is it so important ?
	 *
	 */
	protected function _initTables()
	{
		$this->_langTable = $this->getResource()->getTable('ionize/article_lang');
		$this->_urlTable = $this->getResource()->getTable('ionize/url');
	}
	
	
	/**
	 * Joins the Article table to the Page_article table
	 * Called by $this->joinPage()
	 *
	 * @param	int		ID of the parent page
	 *
	 */
	protected function _joinPage($id_page)
	{
		// Existing Joins
		$fromPart = $this->getSelect()->getPart(Zend_Db_Select::FROM);
		
		// Create Join with page_article only if it doesn't exists
        if ( ! isset($fromPart[$this->_contextTable]))
        {
        	$this->getSelect()->join(
				array($this->_contextTable => $this->getTable('ionize/' . $this->_contextTable)),
				'main_table.id_article = ' . $this->_contextTable . '.id_article ',
				array('*')
			);
        }
		
		// Get the Articles Types (in the Ionize article_type table)
		$this->getSelect()->joinLeft(
			array(
				$this->_typeTable => $this->getTable('ionize/' . $this->_typeTable)		
			),
			$this->_contextTable . '.id_type = ' . $this->_typeTable . '.id_type',
			array('*')
		);
		
		
		// Limit to the asked page
		$this->getSelect()->where($this->_contextTable . '.id_page = ' . $id_page);
		
		// Order by Ionize ordering
		$this->getSelect()->order($this->_contextTable . '.ordering', 'ASC');

		// trace("{$select}".PHP_EOL);

		return $this;
	}
	
	
	/**
	 * Joins the Article table to the Article_lang table
	 * Called by $this->joinLang()
	 *
	 * @protected
	 *
	 */
	protected function _joinLang($storeCode)
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_article = ' . $this->_langTable . '.id_article AND ';
		$conditions .= $this->_langTable . ".lang = '" . $storeCode ."'";
		
		$select = $this->getSelect()->joinLeft(
			array($this->_langTable => $this->getTable('ionize/' . $this->_langTable)),
			$conditions,
			array('*')
		);

		// trace("{$select}".PHP_EOL);

		return $this;
	}
	
	
	/**
	 * Joins the Article table to the URL table
	 * Called by $this->joinUrl()
	 *
	 * @protected
	 *
	 */
	protected function _joinUrl($storeCode)
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_article = ' . $this->_urlTable . '.id_entity AND ';
		$conditions .= $this->_urlTable . ".lang = '" . $storeCode ."' AND ";
		$conditions .= $this->_urlTable . ".active = '1' AND ";
		$conditions .= $this->_urlTable . ".type = 'article'";
		
		$select = $this->getSelect()->joinLeft(
			array($this->_urlTable => $this->_urlTable),
			$conditions,
			array('path')
		);

		// trace("{$select}".PHP_EOL);

		return $this;
	}
}