<?php

class Partikule_Ionize_Model_Mysql4_Pagelang_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_pageTable;
	
	// Ionize Magento Template table definition : config.xml
	protected $_magentoTemplateTable;
	

	/**
	 * Contructor
	 *
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/pagelang');
		
		// Inits the lang table name from config.xml
		$this->_pageTable = $this->getResource()->getTable('ionize/page');
		$this->_magentoTemplateTable = $this->getResource()->getTable('ionize/magento_template');
	}
	
	
	/**
	 * Public function to join the Page table to the Ionize's Magento module template table
	 * Adds the 'template' field to the collection items
	 *
	 * @usage	In Helper, Controller : 
	 *			$pages = $page_model->getCollection()->joinTemplate()
	 *
	 */
	public function joinTemplate()
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = $this->_pageTable.'.id_page = ' . $this->_magentoTemplateTable . '.id_page';

		$this->getSelect()->joinLeft(
			array($this->_magentoTemplateTable => $this->getTable('ionize/magento_template')),
			$conditions,
			array('magento_template')
		);

		return $this;
	}
	
	
	/**
	 * Joins the Page table to the Page_lang table
	 * Called by $this->joinLang()
	 *
	 */
	public function joinPage()
	{
		// Join to page_lang on id_page AND current lang code (2 chars)
		$conditions  = 'main_table.id_page = ' . $this->_pageTable . '.id_page';

		$this->getSelect()->joinLeft(
			array('page' => $this->getTable('ionize/page')),
			$conditions,
			array('*')
		);

		return $this;
	}
/*	
	public function limit($n)
	{
		$this->getSelect()->limit($n);
		
		return $this;
	
	}
*/
}