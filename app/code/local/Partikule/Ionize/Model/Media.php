<?php

class Partikule_Ionize_Model_Media extends Partikule_Ionize_Model_Abstract
{
	// Object Type. Mandatory
	protected $_objectType = 'media';


	// Cache tag : Used by Block/Slider to cache results
	const CACHE_TAG				= 'ionize_media';	


	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/media');
	}
	
	
	/**
	 * Returns collection of medias linked to a parent list
	 * 
	 * @param	Items Collection		Pages, Articles, etc.
	 * @param	String					Type of the parent : 'article', 'page'
	 *
	 * @return	Collection				Media collection
	 *
	 */
	public function getFromParents($parentCollection, $parent_type)
	{
		$collection = $this->getCollection()
			->joinLang()
		;

		// Get the parent collection IDs
		$parent_ids = array();
		foreach($parentCollection as $item)
			$parent_ids[] = $item->getId();

		if ( ! empty($parent_ids))
		{
			// Join to the collection type media table
			switch($parent_type)
			{
				case 'article':
					$collection->joinArticle();
					break;
				
				case 'page':
					$collection->joinPage();
					break;
			}
			
			// Get medias in $parent_ids
			$collection->addFieldToFilter(
				$parent_type.'_media.'.'id_'.$parent_type, array('in' => $parent_ids)
			);

//			trace("{$collection->getSelect()}".PHP_EOL);
//			Mage::log('SQL : ' . "{$collection->getSelect()}", null, 'page.log');
			
			// Add Extends fields to each Media
			$collection = $this->addExtendsToCollection($collection);
		}

		return $collection;
	}
	
	/**
	 * Adds medias collections linked to one item (page or article)
	 * 
	 * @param	Item				Page, Article, etc.
	 * @param	String				Type of the parent : 'article', 'page'
	 *
	 * @return	Collection			Media collection
	 *
	 */
	public function addToItem($item, $parent_type)
	{
		$collection_id = 'id_'.$parent_type;
		$collection = $this->getCollection()
			->joinLang()
		;

		// Join to the collection type media table
		switch($parent_type)
		{
			case 'article':
				$collection->joinArticle();
				break;
			
			case 'page':
				$collection->joinPage();
				break;
		}
		
		// Get medias in $parent_ids
		$collection->addFieldToFilter(
			$parent_type.'_media.'.'id_'.$parent_type, $item->getId()
		);

//			trace("{$collection->getSelect()}".PHP_EOL);
//			Mage::log('SQL : ' . "{$collection->getSelect()}", null, 'page.log');
		
		// Add Extends fields to each Media
		$collection = $this->addExtendsToCollection($collection);

		// Set the 'media' attribute
		$item->setMedias(new Partikule_Ionize_Model_Collection());
		
		foreach($collection as $media)
		{
			if($media->getData($collection_id) == $item->getId())
			{
				if ( ! $item->getMedias()->getItemById($item->getId()))
					$item->getMedias()->addItem($media);
			}
		}

		return $item;
	}
	
	
	
	/**
	 * Return one Varien_Data_Collection containing each collection item
	 * with the attribute "medias" set to the Varien_Data_Collection of medias linked to.
	 * 
	 * @param 	Mixed		Parent items model collection. Can be a collection of pages, a collection of articles :
	 *						- Partikule_Ionize_Model_Mysql4_Page_Collection
	 *						- Partikule_Ionize_Model_Mysql4_Article_Collection
	 *
	 * @param 	String		Parent type. 'article', 'page'
	 * @param 	Array		Array of parent to limit the media DB query
	 * @param 	Array		Optional filters. 
	 *						example : array('type'=>'file')
	 *
	 * @return 	Varien_Data_Collection		with the attribute 'medias' set.
	 *
	 */
	public function addToCollection($collection, $parent_type, $filters = array())
	{
		$collection_id = 'id_'.$parent_type;
		
		// Get the medias collection
		$mediaCollection = $this->getFromParents($collection, $parent_type);

		// Add media to each item of the collection
		foreach($collection as $item)
		{
			// Set the 'media' attribute
			$item->setMedias(new Partikule_Ionize_Model_Collection());
			
			foreach($mediaCollection as $media)
			{
				if($media->getData($collection_id) == $item->getId())
				{
					if ( ! $item->getMedias()->getItemById($item->getId()))
						$item->getMedias()->addItem($media);
				}
			}
		}

		return $collection;
	}
	
	
	/**
	 * Adds Extends fields to the media collection
	 *
	 * @param 	Collection 		Media Collection
	 *
	 * @return 	Collection		Media Collection, with potential new fields
	 *
	 */
/*
	private function _addExtendsToCollection($mediaCollection)
	{
		return Mage::getModel('ionize/extends')->addToCollection($mediaCollection, 'media');
	}
*/
	
/*
	public function getId()
	{
		return $this->getIdMedia();
	}
*/
	
	
	/**
	 * Returns the source path to the media regarding the thumb folder
	 *
	 * @param	String		Name of the thumb folder, as defined in Ionize
	 * @param	String		Extension to return. Useful for video, attaching one file get other extension file.
	 *						Ex : The .mp4 is attached to the article, but in certain vcase, we could need the .ogg file.
	 * @return	String		Complete URL to the media
	 *
	 * @usage	<img src="<?php echo getSource('120'); ?>"
	 *			will return the path to the thumb_120 media folder.
	 *
	 */
	public function getSource($folder = '', $extension = NULL)
	{
		if ($folder != '')
			$folder = 'thumb_'.$folder.'/';
		
		$file_name = $this->getFile_name();
		if ( ! is_null($extension))
		{
			$extension = ($extension[0] != '.') ? '.'.$extension : $extension;
			$file_name = explode('.', $file_name);
			array_pop($file_name);
			$file_name = implode('.', $file_name) . $extension;
		}
		
		// $folder = Mage::getStoreConfig('web/unsecure/base_url') . 'ion/' . $this->getBase_path() . $folder . $file_name;
		$folder = Mage::getStoreConfig('web/unsecure/base_url') . $this->getBase_path() . $folder . $file_name;
		return $folder;
	}
	
	
	public function resize($width = NULL, $height = NULL)
	{
	}
	
	
	/**
	 * Returns TRUE if the ressource path is external, FALSE if internal
	 * A little bit hacky but...
	 *
	 */
	public function isExternal()
	{
		$path = $this->getPath();
		
		if ($path)
		{
			$loc_arr = explode('/', $path);
			$location = array_shift($loc_arr);

			if (in_array($location, array('http:', 'https:')))
				return TRUE;
		}
				
		return FALSE;
	}
	
}
