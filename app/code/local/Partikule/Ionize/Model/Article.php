<?php

class Partikule_Ionize_Model_Article extends Partikule_Ionize_Model_Abstract
{
	// Cache tag : Used by Block/Slider to cache results
	const CACHE_TAG = 'ionize_article';	
	
	// Object Type. Mandatory
	protected $_objectType = 'article';
	
	// Context table
	protected $_typeTable = 'article_type';

	// Current store code (lang code)
	protected $_storeCode = NULL;

	
	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/article');
	}
	
	
	
	/**
	 * Retrieves Articles collection based on given conditions
	 * 
	 * @param	Partikule_Ionize_Model_Page		Page model instance
	 * @param	Array							Array of conditions
	 *
	 * @return	Varien_Data_Collection			Article collection
	 *
	 */
	public function getArticles($page, $conditions = array())
	{
        $articles = $this->getCollection()
        	->addFilter('article_lang.online', '1')
        	->addFilter('page_article.online', '1')
        	->joinPage($page->getId_page())
        	->joinLang()
			->joinUrl()
        	->setConditions($conditions)
        ;
		
		// trace("{$articles->getSelect()}".PHP_EOL);
		
		// Add medias collections to each article : reachable with : $article->getMedias()
		$articles = Mage::getModel('ionize/media')->addToCollection($articles, 'article');

		// Get the target URL (link) foreach article
		foreach($articles as &$article)
		{
			$this->addTargetUrlToArticle($article);
		}

		// Add Extends fields to collection items
		$articles = $this->addExtendsToCollection($articles, 'article');

		return $articles;
	}
	
	
	/**
	 * Retrives one Article from its URL.
	 * If the page isn't given, returns the article from current page
	 * 
	 * @param	String									URI Part of the Article
	 * @param	Partikule_Ionize_Model_Page				Optional, Page from which get this article
	 *
	 * @returns Partikule_Ionize_Model_Article			Article
	 *
	 */
	public function getArticleByUrl($url, $page = NULL)
	{
		if ( is_null($page))
		{
			$page = Mage::helper('ionize/page')->getRegisteredPage();
		}
	
        $articles = $this->getCollection()
        	->addFilter('article_lang.online', '1')
        	->addFilter('article_lang.url', $url)
        	->joinPage( $page->getId_page() )
        	->joinLang( $this->getStoreCode() );


		// Add medias collections to each article
		// Will be get by getMedia()
		$articles = Mage::getModel('ionize/media')->addToCollection($articles, 'article');
		
		if ( $articles->getFirstItem()->hasData() )
			$article = $articles->getFirstItem()->addExtends();
		else
			$article = $articles->getFirstItem();
		
		return $article;
	}
	
	
	/**
	 * Retrives one Article from its ID
	 * 
	 * @param	Int									Article ID
	 *
	 * @returns Partikule_Ionize_Model_Article		Article
	 *
	 */
	public function getArticle($id_article)
	{
        $articles = $this->getCollection()
        	->addFilter('main_table.id_article', $id_article)
        	->addFilter('article_lang.online', '1')
        	->joinUrl()
        	->joinLang( $this->getStoreCode() );

		// Add medias collections to each article
		// Will be get by getMedia()
		$articles = Mage::getModel('ionize/media')->addToCollection($articles, 'article');
		
		// trace("{$articles->getSelect()}".PHP_EOL);

		if ( $articles->getFirstItem()->hasData() )
			$article = $articles->getFirstItem()->addExtends();
		else
			$article = $articles->getFirstItem();

		return $article;
	}
	
	
	
	
	/**
	 * Returns one media based on the conditions array
	 * First checks conditions against the Ionize core fields, then against the potential extends fields
	 *
	 * @param 	array 		array of conditions
	 *
	 * @return	mixed		Partikule_Ionize_Model_Media or NULL if no media found
	 *
HERE
HERE
HERE
	 */
	public function getMedia($conditions = array(), $operator= 'and')
	{
		$returned_media = NULL;
		
		$medias = $this->getMedias();
		
		foreach($conditions as $key => $value)
		{
			$medias->addFilter($key, $value, $operator);
		}
		
		foreach($this->getMedias() as $media)
		{
			trace($media->getPath());
		}
	}
	
	/**
	 * Returns true if one media of the given type is linked to the article
	 *
	 * @param	String	type. Can be 'picture', 'video', 'music', 'file'
	 *
	 */
	public function hasMediaType($type = 'picture', $fromCurrentStoreCode = FALSE)
	{
		$medias = $this->getMedias();
		
		if ($fromCurrentStoreCode)
			$medias =  $medias->fromCurrentStore();

		$medias = $medias->filterOnLangDisplayed();

		foreach($medias as $media)
		{
			if ($media->getType() == $type)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getFirstType($type = 'picture', $fromCurrentStoreCode = FALSE)
	{
		$medias = $this->getMedias();

		// filters on the Extend Field called "magento-store"
		// Means the type media must have this Extend field
		// Better to filter directly on "lang_display"
		if ($fromCurrentStoreCode)
		{
			$medias = $medias->fromCurrentStore();
		}
		$medias = $medias->filterOnLangDisplayed();

		foreach($medias as $media)
		{
			if ($media->getType() == $type)
			{
				return $media;
			}
		}
		return FALSE;	
	}
	
	
	/**
	 * Set the correct path (URL target) to the article
	 *
	 * @param 	Article
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function addTargetUrlToArticle($article)
	{
		if ($article->getLink_type() != '')
		{
			// Link to another page : get this other page
			if ($article->getLink_type() == 'page')
			{
				$page = Mage::getModel('ionize/page')->getPageById($article->getLink_id());
				
				$article->setTargetUrl($page->getPath());
			}

			// Mage::log($article->getData(), null, 'article.log');
			
			// Link to another article : get this other article (should be improved)
			if ($article->getLink_type() == 'article')
			{
				// Link to one article looks like pp.aa
				// pp : page ID
				// aa : article ID
				//
				// TODO : Correct this and see how to mix the concept of "article in its page context" and "one article has only one min parent"...
				// Hmmm....
				//
				$rel = explode('.', $article->getLink_id());
	
				if (isset($rel[1]))
					$target_article = $this->getArticle($rel[1]);
				
				$article->setTargetUrl(Mage::getBaseUrl() . $target_article->getPath());
			}
			
			// External link
			if ($article->getLink_type() == 'external')
			{
				$article->setTargetUrl($article->getLink());
			}

		}
		return $article;
	}
	
	
	/**
	 * Returns the Article's date
	 *
	 * @return 	string		bla
	 *
	 *
	 */
	public function getDate($format = 'd F')
	{
		$date = $this->getLogical_date();
		
		if ($date == '0000-00-00 00:00:00')
		{
			if ($this->getPublish_on() != '0000-00-00 00:00:00')
				$date = $this->getPublish_on();
			else
			{
				if ($this->getUpdated() != '0000-00-00 00:00:00')
					$date = $this->getUpdated();
				else
					$date = $this->getPublish_on();			
			}
		}
		
//		$format = 'l d F Y \a\t H\hi';

		$tmp = date($format, strtotime($date) );
				
		return $tmp;
	
		//     Mage::app()->getLocale()->date(strtotime($Model->getCreatedAt()), null, null, false)->toString('dd/MM/yyyy')
	}
	
	
	/**
	 * Set the store code
	 * So the model can retrieve pages from another store (language)
	 *
	 */
    public function setStoreCode($storeCode)
    {
        $this->_storeCode = $storeCode;

        return $this;
    }
	
	/**
	 * Returns the current set store code or the Mage store code if
	 * the local one isn't set.
	 *
	 */
    public function getStoreCode()
    {
    	if (is_null($this->_storeCode))
    		return Mage::helper('ionize/core')->getCurrentStoreCode();
    	
        return $this->_storeCode;
    }

}
