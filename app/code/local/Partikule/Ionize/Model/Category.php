<?php

class Partikule_Ionize_Model_Category extends Partikule_Ionize_Model_Abstract
{
	
	/**
	 * Returns the array of products iD linked to one page
	 * 
	 * @param	int		Page ID
	 *
	 * @return	array	Array of IDs
	 *
	 */
	public function getCategoryIdFromPage($id_page)
	{
		$result = array();
		
		// Get direct connection resource to Ionize table
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('ionize_read');
		$categoriesTable = $resource->getTableName('ionize/magento_categories');
		
		$select = $read->select()
			->from($categoriesTable)
			->where('parent = ?', 'page')
			->where('id_parent = ?', $id_page)
			->order(array('ordering ASC'));

		$query = $select->query();
		
		$categories = $query->fetchAll();
		
		foreach($categories as $category)
		{
			$result[] = $category['entity_id'];
		}
		
		return $result;
	}

}