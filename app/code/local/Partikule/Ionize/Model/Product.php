<?php

class Partikule_Ionize_Model_Product extends Partikule_Ionize_Model_Abstract
{
	
	/**
	 * Returns the array of products iD linked to one page
	 * 
	 * @param	int		Page ID
	 *
	 * @return	array	Array of IDs
	 *
	 */
	public function getProductIdFromPage($id_page)
	{
		return $this->_getProductIdFromParent($id_page, 'page');
	}
	
	
	/**
	 * Returns the array of products iD linked to one article
	 * 
	 * @param	int		Article ID
	 *
	 * @return	array	Array of IDs
	 *
	 */
	public function getProductIdFromArticle($id_article)
	{
		return $this->_getProductIdFromParent($id_article, 'article');
	}
	
	
	/**
	 * Returns products collection from on parent
	 *
	 */
	public function getProductCollectionFrom($parent = 'page', $id_parent)
	{
		$product_ids = $this->_getProductIdFromParent($id_parent, $parent);

		$storeId = Mage::app()->getStore()->getId();
		
		$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect("*")
			->setStoreId( $storeId )
			->addAttributeToFilter('entity_id', array('in' => $product_ids))
			->addAttributeToFilter('type_id', 'configurable')
		;
		
		// ORDER BY FIELD : Keep the user's product order
		$collection->getSelect()->order(new Zend_Db_Expr("FIELD(e.entity_id, '" . implode("','", $product_ids) . "')"));

		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);
		
		return $collection;
		
	}
	
	
	
	/**
	 * Returns the array of products ID linked to one parent
	 * 
	 * @param	int		parent ID
	 * @param	string		parent type. can be 'article', 'page'
	 *
	 * @return	array	Array of IDs
	 *
	 */
	protected function _getProductIdFromParent($id, $parent_type)
	{
		$result = array();
		
		// Get direct connection resource to Ionize table
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('ionize_read');
		$productsTable = $resource->getTableName('ionize/magento_products');
		
		$select = $read->select()
			->from($productsTable)
			->where('parent = ?', $parent_type)
			->where('id_parent = ?', $id)
			->order(array('ordering ASC'));

		$query = $select->query();
		
		$products = $query->fetchAll();
		
		foreach($products as $product)
		{
			$result[] = $product['entity_id'];
		}
		
		return $result;
	}
	
	
	

}