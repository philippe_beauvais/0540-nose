<?php
/**
 * Extend definition model
 *
 */
class Partikule_Ionize_Model_Extend extends Mage_Core_Model_Abstract
{
	// Local cache for definitions
	protected $_definitions = array();


	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/extend');
	}
	
	/**
	 * Returns the collection of parent's linked extends definitions
	 * Useful to get the default values of each extend field
	 *
	 * @param 	string 		Parent type. 'page', 'article', 'media'
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getDefinitions($parent)
	{
		if ( ! isset($this->_definitions[$parent]))
		{
			$collection = $this->getCollection()
				->addFilter('parent', $parent)
			;
			
			$this->_definitions[$parent] =  $collection;
		}
		
		return $this->_definitions[$parent];
	}
	
	
}
