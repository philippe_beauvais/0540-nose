<?php

class Partikule_Ionize_Model_Url extends Partikule_Ionize_Model_Abstract
{

	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/url');
	}
	
	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getPageIdFromUrl($url)
	{
        $pages = $this->getCollection()
        	->addFilter('path', $url)
        	->addFilter('type', 'page')
        ;
		
		// At this step, we don't care if the Path is active or not,
		// what's interesting us is the page ID, that's all, we don't care about the URL.
		$page = $pages->getFirstItem();	
        
        if ($page->hasData())
        	return $page->getId_entity();
        
        return FALSE;
	}
	
	
	/**
	 * Returns one element from URL table regarding its path
	 *
	 * @param 	string 		Element's path
	 *
	 */
	public function getElementFromPath($path)
	{
        $elements = $this->getCollection()
        	->addFilter('path', $path)
        	->addFilter('lang', Mage::helper('ionize/core')->getCurrentStoreCode())
        ;

		$element = $elements->getFirstItem();	

		return $element;
	}
	
	
	/**
	 * Returns one element from URL table regarding the given conditions
	 *
	 * @param 	array 		Conditions
	 *
	 */
	public function getElement($conditions)
	{
        $elements = $this->getCollection();

		foreach($conditions as $key => $value)
		{
        	$elements->addFilter($key, $value);
		}

		$element = $elements->getFirstItem();	

		return $element;
	}


	public function getAllCategoryUrlsFromUrl($url)
	{
		$urls = $this->getCollection()
			->addFilter('path', $url)
			->addFilter('active', '1')
			->addFilter('type', 'page');

		$url = $urls->getFirstItem();

		if ($url)
		{
			$urls = $this->getCollection()
				->addFilter('id_entity', $url->getIdEntity())
				->addFilter('active', '1')
				->addFilter('type', 'page');
		}

		return $urls;
	}
	
	
	/**
	 * Retrieves one Page based on its URL
	 * 
	 * @param	String		Page URL (lang depending, eg. "brand" retrieves the same page as "marque", but with translated data)
	 *
	 * @return	Object		Page Object
	 *
	 */
/*
	public function getPageByUrl($url)
	{
        $pages = $this->getCollection()
        	->addFilter('page.online', '1')
        	->addFilter('path', $url)
        	->addFilter('active', '1')
        	->addFilter('type', 'page')
        	->joinPage()
        	->joinTemplate();

		$page = $pages->getFirstItem();	
		
		// If more than one page has the same URL, get the store code one (means the code in URL)
		if($pages->count() > 1)
		{
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();
			$page = $pages->getItemByColumnValue('lang', $storeCode);
		}

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			return $this->getPageById($page->getLink_id());
		}

		return $page;
	}
	
*/
	
	/**
	 * Retrieves one Page based on its ID
	 * 
	 * @param	Int			Page ID
	 *
	 * @return	Object		Page Object
	 *
	public function getPageById($id)
	{
        $pages = $this->getCollection()
        	->addFilter('page.online', '1')
        	->addFilter('id_entity', $id)
        	->addFilter('type', 'page')
        	->joinTemplate();

		$page = $pages->getFirstItem();

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			return $this->getPageById($page->getLink_id());
		}

		return $page;
	}
	 */



}
