<?php

abstract class Partikule_Ionize_Model_Abstract extends Mage_Core_Model_Abstract
{
	// Object Type. Mandatory
	protected $_objectType = NULL;
	
	/**
	 * Simply return the name of the object type
	 * Used by Extends to get the type of the object
	 *
	 * $_objectType must be set in Page, Article and Media
	 *
	 */
	public function getObjectType()
    {
    	return $this->_objectType;
    }
	
	
	/**
	 * Returns one Extend field instance
	 *
	 * @param 	string 		Name of the Extend Field, like defined in Ionize (internal Extend code)
	 * @param 	boolean 	Returns the default extend value if no value is found
	 *
	 * @return 	Object		One Extend Field Model Object or one empty Varien Object if no
	 *						extend is found
	 *
	 */
	public function getExtend($name, $return_default = FALSE)
	{
		$extend =  $this->getExtends()->getItemByColumnValue('name', $name);

		if ($extend && $extend->hasData())
		{
			return $extend;
		}

		if ($return_default == TRUE)
		{
			$extend = $this->getExtendsDefinitions()->getItemByColumnValue('name', $name);
			
			// Set the content to the default value
			$extend->setContent($extend->getDefaultValue());
			
			return $extend;
		}

		return new Varien_Object();
	}
	
	
	/**
	 * Adds Extends fields to one collection
	 *
	 * @param 	Collection 		Items Collection (pages, articles, medias, )
	 * @param 	String 			Type of collection ('page', 'article', 'media')
	 *
	 * @return 	Collection		Media Collection, with potential new fields
	 *
	 */
	protected function addExtendsToCollection($collection, $type = NULL)
	{
		if (is_null($type))
			$type = $this->getObjectType();

		return Mage::getModel('ionize/extends')->addToCollection($collection, $type);
	}
	
	
	/**
	 * Add extends to one object.
	 * Can be 'page', 'article', 'media'...
	 *
	 */
	protected function addExtends($type = NULL)
	{
		if (is_null($type))
			$type = $this->getObjectType();
		
		return Mage::getModel('ionize/extends')->addToObject($this, $type);
	}
	
	
	/**
	 * Returns the Extends definition for the current object
	 *
	 */
	protected function getExtendsDefinitions($type = NULL)
	{
		if (is_null($type))
			$type = $this->getObjectType();

		return Mage::getSingleton('ionize/extend')->getDefinitions($type);
	}
	
}
