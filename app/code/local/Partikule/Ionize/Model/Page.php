<?php

class Partikule_Ionize_Model_Page extends Partikule_Ionize_Model_Abstract
{
	// Object Type. Mandatory
	protected $_objectType = 'page';

	// Cache tag : UIsed by Block/Navigation to cache page result
	const CACHE_TAG				= 'ionize_page';

	protected $_storeCode = NULL;
	
	// Array of categories linked to the page
	protected $_categoryIds = NULL;
	
	// First category linked to the page
	protected $_firstCategory = NULL;
	
	// Pages linked to category
	protected $_pageCategory = NULL;

	protected $_langs = array(
		'fr' => 'fr-fr',
		'en' => 'en-gb'
	);

	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/page');
	}
	
	
	
	/**
	 * Retrieves pages Collection regarding the current language code
	 * (the collection join the table "page" to the table "page_lang"
	 *
	 * @param	int							Ionize Menu ID. Default to 1, the main menu.
	 * @param	int							Ionize parent page ID.
	 *
	 * @return	Mysql4_Page_Collection 		Collection of pages
	 *
	 */ 
	public function getPages($id_menu = 1, $id_parent = NULL)
	{
        $pages = $this->getCollection();
       	
        $pages->addFilter('main_table.online', '1')
        	->addFilter('id_menu', $id_menu)
        	->setOrder('ordering', 'ASC')
        	->joinLang($this->getStoreCode())
        	->joinUrl($this->getStoreCode())
        	->joinType()
        ;
        
        if ( ! is_null($id_parent))
        {
			$pages->addFilter('main_table.id_parent', $id_parent);
		}
		// Mage::log("{$pages->getSelect()}".PHP_EOL, null, 'partikule.log');		

        return $pages;
	}


	/**
	 * Retrieves visible pages Collection regarding the current language code
	 * (the collection join the table "page" to the table "page_lang"
	 *
	 * @param	int							Ionize Menu ID. Defualt to 1, the main menu.
	 * @param	int							Ionize parent page ID.
	 *										(not implemented yet)
	 *
	 * @return	Mysql4_Page_Collection 		Collection of pages
	 *
	 */ 
	public function getVisiblePages($id_menu = 1, $id_parent = NULL)
	{
        $pages = $this->getCollection();
        
        $pages->addFilter('main_table.online', '1')
        	->addFilter('id_menu', $id_menu)
        	->addFilter('appears', 1)
        	->setOrder('ordering', 'ASC')
        	->joinLang($this->getStoreCode())
        	->joinUrl($this->getStoreCode())
        	->joinType()
        ;
        	
        if ( ! is_null($id_parent))
        {
			$pages->addFilter('main_table.id_parent', $id_parent);
		}

        return $pages;
	}


	/**
	 * Retrieves one Page based on its URL
	 * 
	 * @param	String		Page URL (lang depending, eg. "brand" retrieves the same page as "marque", but with translated data)
	 *
	 * @return	Object		Page Object
	 *
	 */
	public function getPageByUrl($url)
	{
        $pages = $this->getCollection()
        	->joinTemplate()
        	->joinLang( $this->getStoreCode() )
        	->joinUrl( $this->getStoreCode() )
        	->joinType()
        	->addFilter('main_table.online', '1')
        	->addFilter('url.path', $url)
		;
		
		$page = $pages->getFirstItem();
		
		if ($page->hasData())
			$page = $this->prepareUrl($page);

		// Mage::log("{$pages->getSelect()}".PHP_EOL, null, 'page.log');		

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			// Modified 2013.12.04
			return $this->getPageById($page->getLink_id());
			// $other_page = $this->getPageById($page->getLink_id());
			// $page->setUrl($other_page->getUrl());
		}

		// Add medias collections to page : reachable with : $page->getMedias()
		if ($page->hasData())
			$page = Mage::getModel('ionize/media')->addToItem($page, 'page');


		return $page;
	}
	
	/**
	 * Retrieves on page based on its name (not translated)
	 * in Ionize, the name is the 'name' field of the page table.
	 *
	 * This function is usefull to get indiv. page which doesn't have an URL path declared in the table "URL"
	 * 
	 * @param	String		Page name
	 * @return	Page object
	 *
	 */
	public function getPageByName($name)
	{
        $pages = $this->getCollection()
        	->addFilter('main_table.online', '1')
        	->addFilter('main_table.name', $name)
        	->joinTemplate()
        	->joinType()
        	->joinLang( $this->getStoreCode())
        	->joinUrl($this->getStoreCode())
		;
		
		//trace("{$this->getCollection()->getSelect()}".PHP_EOL);

		$page = $pages->getFirstItem();

		if ($page->hasData())
			$page = $this->prepareUrl($page)->addExtends();
		
		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			// Modified 2013.12.04
			return $this->getPageById($page->getLink_id());

			// Should be like that, with one "redirect" field in ionize
			// $other_page = $this->getPageById($page->getLink_id());
			// $page->setUrl($other_page->getUrl());
		}

		return $page;
	}
	
	
	/**
	 * Retrieves one Page based on its ID
	 * 
	 * @param	Int			Page ID
	 *
	 * @return	Object		Page Object
	 *
	 */
	public function getPageById($id)
	{
        $pages = $this->getCollection()
        	->addFilter('main_table.online', '1')
        	->addFilter('main_table.id_page', $id)
        	->joinTemplate()
        	->joinLang($this->getStoreCode())
        	->joinUrl($this->getStoreCode())
        	->joinType()
        ;

		$page = $pages->getFirstItem();

		if ($page->hasData())
		{
			$page = $this->prepareUrl($page)->addExtends();

			// $page = $this->add_alternate_pages_to_page($page);

			// Add Extend fields to this page
			// $page = $this->addExtends($page, 'page');
		}
		// Mage::log("{$pages->getSelect()}".PHP_EOL, null, 'partikule.log');

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			// Modified 2013.12.04
			return $this->getPageById($page->getLink_id());
			//$other_page = $this->getPageById($page->getLink_id());
			//$page->setUrl($other_page->getUrl());
		}

		// Add medias collections to page : reachable with : $page->getMedias()
		if ($page->hasData())
			$page = Mage::getModel('ionize/media')->addToItem($page, 'page');

		return $page;
	}

	
	/**
	 * Retrieves the Home page
	 * 
	 * @return	Object		Page Object
	 *
	 */
	public function getHomePage()
	{
        $pages = $this->getCollection()
        	->addFilter('main_table.online', '1')
        	->addFilter('home', 1)
        	->joinTemplate()
        	->joinLang($this->getStoreCode())
        	->joinType()
        ;

		$page = $pages->getFirstItem()->addExtends();

		// $page = $this->add_alternate_pages_to_page($page);

		// Mage::log($page->getData(), null, 'partikule.log');


		return $page;
	}
	
	
	
	
	/**
	 * Returns the Ionize page linked to one category
	 * 
	 * @param	int		Category ID
	 *
	 * @return	array	Page data
	 *
	 */
	public function getPageLinkedToCategory($category_id)
	{
		if ( ! is_null($this->_pageCategory))
			return $this->_pageCategory;

		// Get direct connection resource to Ionize table
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('ionize_read');
		$categoriesTable = $resource->getTableName('ionize/magento_categories');

		$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();
		
		$select = $read->select()
			->from($categoriesTable)
			->join(
				array('page_lang' => 'page_lang'),
				$categoriesTable . ".id_parent = page_lang.id_page AND page_lang.lang = '".$storeCode."'",
				array('*')
			)
			->joinLeft(
				array('url' => 'url'),
				"url.id_entity = page_lang.id_page AND url.lang = '".$storeCode."' AND url.active='1'",
				array('path')
			)
			->where('parent = ?', 'page')
			->where('entity_id = ?', $category_id)
			->where('master = ?', '1')
			->order(array('ordering ASC')
		);

		$this->_pageCategory = $select->query()->fetchAll();
		
		// trace("{$select}".PHP_EOL);
		
		return $this->_pageCategory;
	}


	public function add_alternate_pages_to_page($page)
	{
		$base_url = Mage::helper('ionize/core')->getBaseUrl();
		$urls = $this->get_alternate_pages($page);
		$alternates = array();

		foreach($urls as $url)
		{
			if ($url->getLang() != $page->getLang())
			{
				if ($page->getHome() == '1')
				{
					$alternates[] = array(
						'url' => $base_url . $url->getLang() . '/',
						'lang' => $this->_langs[$url->getLang()]
					);
				}
				else
				{
					$alternates[] = array(
						'url' => $base_url . $url->getLang() . '/' . $url->getPath(),
						'lang' => $this->_langs[$url->getLang()]
					);
				}
			}

			$page->setAlternates($alternates);
		}

		return $page;
	}

	/**
	 * @param $page
	 * @return array		Plain old PHP array.
	 */
	public function get_alternate_pages($page)
	{
		$alternates = array();

		if ($page)
		{
			$base_url = Mage::helper('ionize/core')->getBaseUrl();

			$urls = Mage::getModel('ionize/url')->getCollection()
				->addFilter('type', 'page')
				->addFilter('active', '1')
				->addFilter('id_entity', $page->getId());

			foreach ($urls as $url) {
				if ($url->getLang() != $page->getLang()) {
					if ($page->getHome() == '1') {
						$alternates[] = array(
							'url' => $base_url . $url->getLang() . '/',
							'lang' => $this->_langs[$url->getLang()]
						);
					} else {
						$alternates[] = array(
							'url' => $base_url . $url->getLang() . '/' . $url->getPath(),
							'lang' => $this->_langs[$url->getLang()]
						);
					}
				}
			}
		}

		return $alternates;
	}


	public function build_alternates_urls($urls)
	{
		$base_url = Mage::helper('ionize/core')->getBaseUrl();
		$lang = Mage::app()->getStore()->getCode();

        $alternates = array();

		foreach ($urls as $url)
		{
			if ($url->getLang() != $lang)
			{
				$alternates[] = array(
					'url' => $base_url . $url->getLang() . '/' . $url->getPath(),
					'lang' => $this->_langs[$url->getLang()]
				);
			}
		}

		return $alternates;
	}


	/**
	 * Returns the Categories ID array
	 *
	 * @return 	array		Simple array of categories ID
	 *
	 */
	public function getCategoryIds()
	{
		if ( ! is_null($this->_categoryIds))
			return $this->_categoryIds;

		$result = array();
		
		// Get direct connection resource to Ionize table
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('ionize_read');
		$categoriesTable = $resource->getTableName('ionize/magento_categories');
		
		$select = $read->select()
			->from($categoriesTable)
			->where('parent = ?', 'page')
			->where('id_parent = ?', $this->getId_page())
			->order(array('ordering ASC'))
		;

		$query = $select->query();
		
		$categories = $query->fetchAll();
		
		foreach($categories as $category)
		{
			$result[] = $category['entity_id'];
		}
		
		$this->_categoryIds = $result;
		
		return $result;
	}
	
	
	/**
	 * Returns the first category ID
	 *
	 * @return 	int		Category ID
	 *
	 */
	public function getFirstCategoryId()
	{
		$categories_ids = $this->getCategoryIds();

		if ( ! empty($categories_ids))
			return $categories_ids[0];

		return FALSE;
	}


	/**
	 * Returns the first category
	 *
	 * @return 	Mage_Catalog_Model_Category		Category
	 *
	 */
	public function getFirstCategory()
	{
		if ( ! is_null($this->_firstCategory))
			return $this->_firstCategory;
		
		$category_id = $this->getFirstCategoryId();
		
		if ( FALSE !== $category_id)
			$category = Mage::getModel('catalog/category')->load($category_id);
		else
			$category = new Mage_Catalog_Model_Category();
		
		$this->_firstCategory = $category;
		
		return $category;
	}


	/**
	 * Returns true if one media of the given type is linked to the article
	 * @param string $type		type. Can be 'picture', 'video', 'music', 'file'
	 * @param bool   $fromCurrentStoreCode
	 *
	 * @return bool
	 */
	public function hasMediaType($type = 'picture', $fromCurrentStoreCode = FALSE)
	{
		$medias = $this->getMedias();
		
		if ($fromCurrentStoreCode)
			$medias->fromCurrentStore();

		$medias = $medias->filterOnLangDisplayed();

		foreach($medias as $media)
		{
			if ($media->getType() == $type)
				return TRUE;
		}
		return FALSE;
	}
	
	public function getFirstType($type = 'picture', $fromCurrentStoreCode = FALSE)
	{
		$medias = $this->getMedias();

		if ($fromCurrentStoreCode)
			$medias->fromCurrentStore();

		$medias = $medias->filterOnLangDisplayed();

		foreach($medias as $media)
		{
			if ($media->getType() == $type)
				return $media;
		}
		return FALSE;	
	}

	
	/**
	 * Returns on Extend from the page
	 *
	 * @param 	string 								name of the extend to get
	 *
	 * @usage	Partikule_Ionize_Model_Extends		
	 *
	 */
/*
	public function getExtend($name)
	{
		return $this->getExtends()->getItemByColumnValue('name', $name);
	}
*/

	
	/**
	 * Set the store code
	 * So the model can retrieve pages from another store (language)
	 *
	 */
    public function setStoreCode($storeCode)
    {
        $this->_storeCode = $storeCode;

        return $this;
    }
	
	/**
	 * Returns the current set store code or the Mage store code if
	 * the local one isn't set.
	 *
	 */
    public function getStoreCode()
    {
    	if (is_null($this->_storeCode))
    		return Mage::helper('ionize/core')->getCurrentStoreCode();
    	
        return $this->_storeCode;
    }
    
    
    /**
     * Prepare the URL of one page
     *
     * @param 	array		Page array
     *
     * @return 	string		bla
     *
     * @usage	bla
     *
     */
	public function prepareUrl($page)
	{
		if (strpos($page->getPath(), 'http://') === FALSE)
			$page->setPath(Mage::getBaseUrl() .$page->getPath());

		return $page;
	}
	
	
	/**
	 * Adds Extends fields to the media collection
	 *
	 * @param 	Collection 		Media Collection
	 *
	 * @return 	Collection		Media Collection, with potential new fields
	 *
	 */
/*
	private function _addExtendsToCollection($pageCollection)
	{
		return Mage::getModel('ionize/extends')->addToCollection($pageCollection, 'page');
	}
*/
	
	
	/**
	 * Adds Extends fields to the media collection
	 *
	 * @param 	Collection 		Media Collection
	 *
	 * @return 	Collection		Media Collection, with potential new fields
	 *
	 */
/*
	private function _addExtends($page)
	{
		return Mage::getModel('ionize/extends')->addToObject($page, 'page');
	}
*/
	
	
}
