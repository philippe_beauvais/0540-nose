<?php

class Partikule_Ionize_Model_Pagelang extends Partikule_Ionize_Model_Abstract
{
	// Cache tag : UIsed by Block/Navigation to cache page result
//	const CACHE_TAG				= 'ionize_pagelang';

	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/pagelang');
	}
	
	/**
	 * Retrieves one Page based on its URL
	 * 
	 * @param	String		Page URL (lang depending, eg. "brand" retrieves the same page as "marque", but with translated data)
	 *
	 * @return	Object		Page Object
	 *
	 */
	public function getPageByUrl($url)
	{
        $pages = $this->getCollection()
        	->addFilter('main_table.online', '1')
        	->addFilter('url', $url)
        	->joinPage()
        	->joinTemplate();
		
		$page = $pages->getFirstItem();	
		
		// If more than one page has the same URL, get the store code one

		if($pages->count() > 1)
		{
			$storeCode = Mage::helper('ionize/core')->getCurrentStoreCode();
			$page = $pages->getItemByColumnValue('lang', $storeCode);
		}

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			return $this->getPageById($page->getLink_id());
		}

		return $page;
	}
	
	
	/**
	 * Retrieves one Page based on its ID
	 * 
	 * @param	Int			Page ID
	 *
	 * @return	Object		Page Object
	 *
	 */
	public function getPageById($id)
	{
        $pages = $this->getCollection()
        	->addFilter('main_table.online', '1')
        	->addFilter('main_table.id_page', $id)
        	->joinTemplate();

		$page = $pages->getFirstItem();

		// Link to another page : get this other page
		if ($page->getLink_type() == 'page')
		{
			return $this->getPageById($page->getLink_id());
		}

		return $page;
	}
}
