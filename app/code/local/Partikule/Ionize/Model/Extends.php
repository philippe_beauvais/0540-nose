<?php

class Partikule_Ionize_Model_Extends extends Mage_Core_Model_Abstract
{
	/**
	 * Pseudo Constructor
	 *
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('ionize/extends');
	}
	
	/**
	 * Returns Collection of Extends linked to one parent
	 *
	 * @param 	Int 					Parent ID
	 * @param	String					Type of the parent : 'article', 'page', 'media'
	 *
	 * @return	Collection				Extends collection
	 *
	 */
	public function getFromParent($parent_id, $parent_type)
	{
		$collection = $this->getCollection();

		$collection
			->joinLang()
			->joinExtend()
			->addFilter('extend_field.parent', $parent_type);

		$collection->addFieldToFilter(
			'main_table.id_parent', $parent_id
		);

		$collection->getSelect()
			->where("(main_table.lang = '".Mage::app()->getStore()->getCode()."' OR main_table.lang = '' )");

		return $collection;
	}
	
	
	/**
	 * Returns collection of extends linked to a parent list
	 * 
	 * @param	Items Collection		Pages, Articles, Medias, etc.
	 * @param	String					Type of the parent : 'article', 'page', 'media'
	 *
	 * @return	Collection				Extends collection
	 *
	 */
	public function getFromParents($parentCollection, $parent_type)
	{
		$collection = $this->getCollection();

		// Array of parent IDs
		$parent_ids = array();
		foreach($parentCollection as $item)
		{
			$id = $item->{'getId_'.$parent_type}();
			$parent_ids[] = $id;
		}

		if ( ! empty($parent_ids))
		{
			$collection
				->joinLang()
				->joinExtend()
				->addFilter('extend_field.parent', $parent_type)
			;

			$collection->addFieldToFilter(
				'main_table.id_parent', 
				array('in' => $parent_ids)
			);

			$collection->getSelect()
				->where("(main_table.lang = '".Mage::app()->getStore()->getCode()."' OR main_table.lang = '' )");
		}
		
		// Debug
		/*
		if ($parent_type == 'article')
				Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');
		*/
		// trace("{$collection->getSelect()}".PHP_EOL);
		
		return $collection;
	}
	

	/**
	 * Add Extends to one object (Page, Article, Media)
	 *
	 * @param 	Object			Page, Article, Media
	 * @param 	string			type : 'page', 'article, 'media'
	 *
	 * @return 	Object			Object with the attribute 'extends' set.
	 *
	 */
	public function addToObject($object, $type)
	{
		$extendsCollection = $this->getFromParent($object->getId(), $type);

		$object->setExtends(new Partikule_Ionize_Model_Collection());
		
		foreach($extendsCollection as $extend)
		{
			$object->getExtends()->addItem($extend);
		}

		return $object;
	}

	
	/**
	 * Add Extends Collection to a parent collection of items
	 *
	 * @param 	Collection		Collection of parent elements. Can be Collection of pages, articles, medias.
	 * @param 	string			Parent type : 'page', 'article, 'media'
	 *
	 * @return 	Collection		Collection with the attribute 'extends' set.
	 *
	 */
	public function addToCollection($collection, $parent_type)
	{
		$extendsCollection = $this->getFromParents($collection, $parent_type);

		// Add media to each item of the collection
		foreach($collection as $item)
		{
			// Debug
			// Mage::log('id : ' . $item->{'getId_'.$parent_type}(), null, 'partikule.log');
			
			// Set the 'media' attribute
			$item->setExtends(new Partikule_Ionize_Model_Collection());
			
			foreach($extendsCollection as $extend)
			{
				if($extend->getData('id_parent') == $item->{'getId_'.$parent_type}())
				{
					if ( ! $item->getExtends()->getItemById($item->{'getId_'.$parent_type}()))
					{
						$item->getExtends()->addItem($extend);
						// Mage::log($extend, null, 'partikule.log');
					}
				}
			}
		}

		return $collection;
	}
}
