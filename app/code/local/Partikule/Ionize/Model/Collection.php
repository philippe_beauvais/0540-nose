<?php

/**
 * Ionize Collection
 * Extension of Varien_data_Collection
 *
 * Used by models, when creating filtered collections, like medias collection when added to each item of one article or page collection
 * 
 *
 */
class Partikule_Ionize_Model_Collection extends Varien_Data_Collection
{
	/**
	 * Adds post filtering capabilities on loaded collections.
	 * Used on sub-collections of element, like medias, articles, etc.
	 *
	 * @param	array		Array of conditions
	 * @param	boolean		Includes or not includes extends fields in condition keys search
	 * @param	string		Not implemented
	 *
	 */
	public function getFiltered($conditions = array(), $extends = TRUE, $operator = 'and')
	{
		// Will return an 
		$filtered_collection = new Partikule_Ionize_Model_Collection();
		
		if ( ! empty($conditions))
		{
			foreach($this->getItems() as $item)
			{
				$data = $item->getData();
				
				// Extends fields filtering
				if ($extends == TRUE && $item->getExtends())
				{
					$extends = $item->getExtends()->getItems();
	
					foreach($extends as $extend)
					{
						foreach($conditions as $key => $value)
						{
							if ($extend->getName() == $key && $extend->getContent() == $value)
							{
								if ( ! $filtered_collection->getItemById($item->getId()))
									$filtered_collection->addItem($item);
							}
						}
					}
				}
				
				// Core fields filtering
				foreach($conditions as $key => $value)
				{
					if (isset($data[$key]) && $data[$key] == $value)
					{
						if ( ! $filtered_collection->getItemById($item->getId()))
							$filtered_collection->addItem($item);
					}
				}
			}
		}
		else
		{
			$filtered_collection = $this;
		}
		// Mage::log($filtered_collection->getItems(), null, 'partikule.log');

		return $filtered_collection;		
	}
	
	
	/**
	 * Filters One collection on the current store code
	 * The Element in Ionize must have one extend field with the code (name) : "magento-store"
	 * 
	 * @usage : $article->getMedias()->fromCurrentStore()
	 *
	 */
	public function fromCurrentStore()
	{
		$conditions = array('magento-store' => Mage::helper('ionize/core')->getCurrentStoreCode());
		
		return $this->getFiltered($conditions, TRUE);
	}


	/**
	 * Filters One Media collection on lang_display
	 * The Element in Ionize must have one extend field with the code (name) : "magento-store"
	 *
	 * @usage : $article->getMedias()->fromCurrentStore()
	 *
	 */
	public function filterOnLangDisplayed()
	{
		// Will return an
		$filtered_collection = new Partikule_Ionize_Model_Collection();

		$current_store_code = Mage::helper('ionize/core')->getCurrentStoreCode();

		foreach($this->getItems() as $item)
		{

			if ( ! $item->getLangDisplay() OR $item->getLangDisplay() == $current_store_code)
			{
				if ( ! $filtered_collection->getItemById($item->getId()))
					$filtered_collection->addItem($item);
			}
		}
		return $filtered_collection;
	}

}