<?php
/**
 * Page Edition Form Container
 *
 */
class Partikule_Ionize_Block_Adminhtml_Page_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{

	public function __construct()
	{
		parent::__construct();
		
		$this->_objectId = 'id';
		
		// blockGroup : Module !
		$this->_blockGroup = 'ionize';
		
		// Controleur : Celui qui possède la méthode editAction()
		$this->_controller = 'adminhtml_page';
		
		//on definit les labels pour les boutons save et les boutons delete
		$this->_updateButton('save', 'label','Save Page');
		$this->_updateButton('delete', 'label', 'Delete Page');
	}

	/* Ici,  on regarde si on a transmit un objet au formulaire,
            afin de mettre le bon texte dans le  header (Editer ou
             Ajouter) */


	/**
	 * @TODO : 	Checker Mage::registry('ionize_data')
	 *			Aussi dans Tab/form.php
	 *			pas sûr que le nom soit bon.
	 *
	 */
	public function getHeaderText()
	{
		if( Mage::registry('page_data') && Mage::registry('page_data')->getId())
		{
			return 'Edition de la Page : "'.$this->htmlEscape(
				Mage::registry('page_data')->getName()
			).'"';
		}
		else
		{
			return 'Ajouter une page';
		}
	}
}
