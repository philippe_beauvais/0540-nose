<?php

class Partikule_Ionize_Block_Adminhtml_Page_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('pageGrid');
		$this->setDefaultSort('id_page');
		$this->setDefaultDir('ASC');
		
		$this->setSaveParametersInSession(true);
	}
	
	
	/**
	 * Collection a utiliser pour interagir avec la base de donnée
	 *
	 */
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('ionize/page')->getCollection();
		
		$this->setCollection($collection);
		
		return parent::_prepareCollection();
	}
	
	
	/**
	 * Liste des colonnes à afficher
	 *
	 */
	protected function _prepareColumns()
	{
		$this->addColumn('id_page',
			array(
				'header' => 'ID',
				'align' =>'right',
				'width' => '50px',
				'index' => 'id_page',
			));

		$this->addColumn('name',
			array(
				'header' => 'Name',			// Title ?
				'align' =>'left',
				'index' => 'name',
			));


		return parent::_prepareColumns();
	}
	
	
	/**
	 * Edition d'une ligen ?
	 *
	 */
	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}
