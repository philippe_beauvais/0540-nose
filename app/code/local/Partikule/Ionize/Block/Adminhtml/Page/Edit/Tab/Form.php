<?php
class Partikule_Ionize_Block_Adminhtml_Page_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();

		$this->setForm($form);
		
		$fieldset = $form->addFieldset('page_form',
			array('legend'=>'ref information'));
		
		$fieldset->addField('name', 'text',
			array(
				'label' => 'Nom',
				'class' => 'required-entry',
				'required' => true,
				'name' => 'name'
			));

		if ( Mage::registry('page_data') )
		{
			$form->setValues(Mage::registry('page_data')->getData());
		}

		$this->setDestElementId('edit_form');

		return parent::_prepareForm();
	}
}
