<?php

class Partikule_Ionize_Block_Adminhtml_Page_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('page_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('Information sur la page');
	}

	protected function _beforeToHtml()
	{
		$this->addTab('form_section', array(
				'label' => 'Page Information',
				'title' => 'Page Information',
				'content' => $this->getLayout()
				->createBlock('ionize/adminhtml_page_edit_tab_form')
				->toHtml()
			));
		return parent::_beforeToHtml();
	}
}
