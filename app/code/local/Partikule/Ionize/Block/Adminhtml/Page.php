<?php

class Partikule_Ionize_Block_Adminhtml_Page extends Mage_Adminhtml_Block_Widget_Grid_Container
{

	public function __construct()
	{
		// Emplacement du controller : dans Adminhtml/Page
/*		
		$this->_blockGroup = 'page';
		$this->_controller = 'adminhtml_page';
*/
		// Nom du module
		$this->_blockGroup = 'ionize';
		
		// Emplacement du controller Grid
		$this->_controller = 'adminhtml_page';
		

		// Texte du header qui s’affichera dans l’admin
		$this->_headerText = 'Gestion des pages';
		
		// Nom du bouton pour ajouter une page
//		$this->_addButtonLabel = 'Ajouter une page';
		
		parent::__construct();

        $this->_removeButton('add');

	}
}