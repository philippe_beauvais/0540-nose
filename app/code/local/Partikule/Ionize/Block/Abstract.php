<?php


abstract class Partikule_Ionize_Block_Abstract extends Mage_Core_Block_Template
{
	
	/**
	 * Returns the current Page
	 *
	 */
	public function getPage()
	{
		return Mage::helper('ionize/page')->getRegisteredPage();
	}

}
