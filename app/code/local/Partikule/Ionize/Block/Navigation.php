<?php

class Partikule_Ionize_Block_Navigation extends Mage_Core_Block_Template
{
	/*
	 * Array of actives pages IDs
	 *
	 */
	protected $_active_page_ids = array();

	protected $_baseUrl = NULL;

	// Ionize breacrumbs
	protected $_breadcrumbs = array();

	protected $_uri_segments = array();


	protected function _construct()
	{
		// To get the current menu, also on Magento standard pages, like product view.
		$this->_breadcrumbs = Mage::helper('ionize/Breadcrumbs')->getBreadcrumbs();

		$request = $this->getRequest();
		$this->_uri_segments = explode('/', $request->getRequestString());


	/*
		$this->addData(array(
			'cache_lifetime'	=> false,
			'cache_tags'		=> array(Partikule_Ionize_Model_Page::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG),
		));
	*/
	}


	/**
	 * Get pages from Ionize
	 * Use of the helper to get the cached Pages
	 *
	 */
	public function getPages($id_menu = 1, $id_parent = NULL, $with_invisible = FALSE)
	{
    	$pages = array();

		if ( $with_invisible == FALSE)
			$pages = Mage::helper('ionize/navigation')->getVisiblePages($id_menu, $id_parent);
		else
			$pages = Mage::helper('ionize/navigation')->getPages($id_menu, $id_parent);

		return $pages;
	}


	/**
	 * Returns the current page's parent page.
	 *
	 *
	 */
	public function getParentPage($without_url = FALSE)
	{
		return Mage::helper('ionize/page')->getParentPage($without_url);
	}


	/**
	 * Returns the current page's parent page ID.
	 *
	 * More optimized if only the ID is needed : No need to make one DB request.
	 *
	 */
	public function getParentPageID($without_url = FALSE)
	{
		return Mage::helper('ionize/page')->getParentPageId($without_url);
	}





	/**
	 * Render Navigation menu in HTML
	 *
	 * @param 	int 		ID of the Ionize's menu to get the navigation menu from
	 * @param 	int 		Level number for list item class to start from
	 * @param 	int 		Max level to display
	 *
	 * @return 	string		HTML of the Navigation menu
	 *
	 * @usage	$this->renderNavigationMenuHtml(1,0,1);
	 *
	 */
	public function renderNavigationMenuHtml($id_menu = 1, $asked_level = 0, $maxLevel = 9999, $with_invisible = FALSE)
	{
		$html = '';

		// Get Pages from the Page Helper
		// $pages = $this->getPages($id_menu)->createUrls();
		$pages = $this->getPages($id_menu, NULL, $with_invisible);

		// Pages array
		$pages_array = $pages->getData();

		$id_parent = 0;
		if ($asked_level > 0)
		{
			$parent_page = $this->_getNavigationParentPage($asked_level, $pages_array);

			if ( ! empty($parent_page) && $parent_page['level'] == ($asked_level -1))
				$id_parent = $parent_page['id_page'];
		}

		// Security : Do not return the root menu for asked level > 0
		if ($asked_level > $id_parent && $id_parent == 0 )
			return $html;

		// Pages URLs
		$pages_array = Mage::helper('ionize/navigation')->prepareUrls($pages_array);

		// Set the active pages
		$this->_getActivePages();

		// Get array of pages tree
		$pages_tree = array();
		$this->_buildPagesTree($pages_array, $pages_tree, $id_parent);

		$pages_array_count = count($pages_tree);

		foreach($pages_tree as $index => $page)
		{
			$isFirst = ($index == 0) ? TRUE : FALSE;
			$isLast = ($index + 1 == $pages_array_count) ? TRUE : FALSE;

            $html .= $this->_renderNavigationMenuItemHtml($page, $asked_level, $maxLevel, $isFirst, $isLast);
		}

		return $html;
	}





	/**
	 * Returns the asked page childs as array
	 * Useful for building selects or ul / li elements in templates
	 *
	 * @param	int			Menu ID
	 * @param	int			Parent page ID
	 * @param	boolean		Includee pages which aren't displayed in navigation menu ?
	 *
	 */
	public function getChildPagesData($id_menu = 1, $id_parent = 0, $with_invisible = FALSE)
	{
		$pages_tree = array();

		// Get Pages from the Page Helper
		$pages = NULL;
		if ( $with_invisible == FALSE)
			$pages = Mage::helper('ionize/navigation')->getVisiblePages($id_menu, $id_parent);
		else
			$pages = Mage::helper('ionize/navigation')->getPages($id_menu, $id_parent);

		// Pages array
		$pages_array = $pages->getData();

		if ( ! empty($pages_array))
		{
			// Pages URLs
			$pages_array = Mage::helper('ionize/navigation')->prepareUrls($pages_array);

			// Set the active pages
			$this->_getActivePages();

			$this->_buildPagesTree($pages_array, $pages_tree, $id_parent);
		}
		return $pages_tree;

	}


	/**
	 * Sets one page active
	 * called through Partikule_Ionize_Helper_Navigation->setActive()
	 *
	 *
	 */
	public function setActive($url)
	{
		// Get Pages from the Page Helper
		$pages = $this->getPages();

		$active_page_ids = array();

		foreach($pages as $p)
		{
			if ($p['url'] == $url )
			{
				$active_page_ids[] = $p['id_page'];
			}
		}
		$this->_active_page_ids = array_merge($this->_active_page_ids, $active_page_ids);
	}






	/**
	 * Returns the Navigation pagination page
	 * By default, the parent page is returned
	 * If the user sets another page as Sub Navigation in Ionize, this page is returned
	 *
	 * @param	array	All pages array
	 *
	 * @return	array	Page data array
	 *
	 */
	private function _getNavigationParentPage($asked_level = 0, $pages_array)
	{
		$nav_parent_page = array();

		$page = Mage::helper('ionize/page')->getRegisteredPage();

		$page_level = $page['level'];

		$id_parent = ($page['id_subnav'] != $page['id_page'] && $page['id_subnav'] !=0 ) ? $page['id_subnav'] : $page['id_parent'];

		foreach($pages_array as $parent)
		{
			if ($parent['id_page'] == $id_parent)
			{
				$nav_parent_page = $parent;
				break;
			}
		}

		// Correct the $nav_parent_page regarding to the asked level
		while ($page_level >= $asked_level && $page_level > 0)
		{
			$potential_parent_page = array();

			foreach($pages_array as $p)
			{
				if( ! empty($nav_parent_page)
					&& $p['id_page'] == $nav_parent_page['id_parent']
					&& $p['level'] == ($asked_level-1)
				)
				{
					$potential_parent_page = $p;
					break;
				}
			}

			if ( ! empty($potential_parent_page))
			{
				$nav_parent_page = $potential_parent_page;
				$page_level = $nav_parent_page['level'];
			}
			else
			{
				$page_level--;
			}
		}

		return ($nav_parent_page);
	}


	/**
	 * Renders the options list
	 *
	 * @param 	array 		Page data
	 * @return 	string		html
	 *
	 */
	private function _renderSelectItemHtml($page)
	{
        $html = '<option value="' . $page['url'] . '">';

        $html .= $page['title'];

        $html .= '</option>';

        return $html;
	}


	/**
	 * Returns one navigation element's HTML
	 * Inspired from /app/code/core/Mage/Catalog/Block/Navigation.php
	 * and adapted to Ionize's data
	 *
	 * @param	array		Page array
	 * @param	int			Level of the element
	 * @param	int			Max level to return
	 * @param	boolean		Is the element the first of the array ?
	 * @param	boolean		Is the element the last of the array ?
	 *
	 * @return	String		HTML string of the element
	 *
	 */
	protected function _renderNavigationMenuItemHtml($page, $level, $maxLevel, $isFirst = FALSE, $isLast = FALSE)
	{
        $html = '<li';

		$classes = array();

		// Active CSS Class
        if (in_array($page['id_page'], $this->_active_page_ids)) $classes[] = 'active';

		// Second pass for possible active menu item
		if (empty($classes))
		{
		}

		if ($isFirst) $classes[] = 'first';
		if ($isLast) $classes[] = 'last';

        if ( ! empty($classes))
	        $html.= ' class="' . implode(' ', $classes) . '"';

	   //     $html.= ' id="nav-' . $page['name'] . '"';


		// Mage::helper('ionize/core')->getStoreUrl()
        $html.= '>'."\n";
        $html.= '<a href="' . $page['path'] . '"><span>' . $page['title'] . '</span></a>'."\n";

        // Only display childrens considering the max level
        if ( ! empty($page['children']) && $page['level'] < $maxLevel)
        {
            $html .= '<ul class="level' . ($level + 1) . '">';

        	$children_count = count($page['children']);

        	foreach ($page['children'] as $index => $child_page)
        	{
				$isFirst = ($index == 0) ? TRUE : FALSE;
				$isLast = ($index + 1 == $children_count) ? TRUE : FALSE;

            	$html .= $this->_renderNavigationMenuItemHtml($child_page, $maxLevel, $level + 1, $isFirst, $isLast);
            }

            $html .= '</ul>';
        }

        $html.= '</li>';

        return $html;
	}


	/**
	 * Builds the pages tree
	 *
	 * @returns		Array		Array of pages
	 *
	 */
	protected function _buildPagesTree($data, &$arr, $id_parent=0, $startDepth=0, $maxDepth=-1)
	{
		$index = 0;
		$startDepth++;

		$children = array();

		foreach($data as $d)
		{
			if ($d['id_parent'] == $id_parent)
				$children[] = $d;
		}

		foreach ($children as $child)
		{
			$arr[$index] = $child;

			$this->_buildPagesTree($data, $arr[$index]['children'], $child['id_page'], $startDepth, $maxDepth);
			$index++;
		}
	}


	/**
	 * Gets the array of active pages
	 *
	 * @param	Array	Array of pages
	 *
	 * @param	mixed	ID of the starting page
	 *
	 */
	protected function _getActivePagesArray($pages, $id_page)
	{
		$active_pages = array();


		// Page data
		// $page = array_values(array_filter($pages, create_function('$row','return $row["id_page"] == "'. $id_page .'";') ));
		$page = array();
		foreach($pages as $p)
		{
			if ($p['id_page'] == $id_page)
				$page = $p;
		}

		if ( ! empty($page))
		{
			if ($page['id_parent'] != '0')
			{
				$active_pages += $this->_getActivePagesArray($pages, $page['id_parent']);
			}

			$active_pages[] = $id_page;
		}

		return $active_pages;
	}


	/**
	 * Returns the array of active pages IDs
	 *
	 * @param	Array	Array of pages
	 *
	 * @return 	Array	Array of pages IDs
	 *
	 */
	protected function _getActivePages()
	{
		// Returned result : Can be empty
		$active_pages = array();

		$pages = Mage::helper('ionize/navigation')->getPages();

		$page = Mage::helper('ionize/page')->getRegisteredPage();

		if ($page)
		{
			$level = $page->getLevel();
			$active_pages[] = $page->getId();

			$page = $page->getData();

			while ($level > -1)
			{
				foreach($pages as $p)
				{
					if ($page['id_parent'] == $p['id_page'] )
					{
						$page = $p;
						$active_pages[] = $p['id_page'];
						break;
					}
				}
				$level--;
			}
		}
		else
		{
			foreach($pages as $page)
			{
				foreach($this->_breadcrumbs as $manger => $petit_bout_de_pain)
				{
					if ($page['url'] == $petit_bout_de_pain['uri'])
					{
						$active_pages[] = $page['id_page'];
					}
				}
			}
		}

		$this->_active_page_ids = array_merge($this->_active_page_ids, $active_pages);
	}


	/**
	 * Returns the current page URI segment
	 *
	 * @param	int		optional. Segment index to return
	 * @return 	String
	 *
	 */
	protected function _getPageUriSegment($segment = 0)
	{
		$uri = $this->getRequest()->getOriginalPathInfo();

		$segments = explode('/', $uri);

		// Remove the first empty segment
		array_shift($segments);

		return $segments[$segment];
	}


	protected function _getBaseUrl()
	{
		if ( ! is_null($this->_baseUrl))
			return $this->_baseUrl;

		$this->_baseUrl = Mage::helper('ionize/core')->getBaseUrl();

		return $this->_baseUrl;
	}

}