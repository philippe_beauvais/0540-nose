<?php

class Partikule_Ionize_Block_Page extends Partikule_Ionize_Block_Abstract
{
	// Window Title
	var $_windowTitle = NULL;
	
	
	protected function _construct()
	{
	}


    protected function _prepareLayout()
    {
    	if ( is_null($this->_windowTitle) )
    	{
	    	$this->setWindowTitle();	
    	}

        return parent::_prepareLayout();
    }

	
	/**
	 * Returns the current page's title
	 *
	 */
	public function getPageTitle()
	{
		$page = Mage::helper('ionize/page')->getRegisteredPage();

		if ($page)
			return $page->getTitle();
	}
	
	
	/**
	 * Returns the current page's subtitle
	 *
	 */
	public function getPageSubtitle()
	{
		$page = Mage::helper('ionize/page')->getRegisteredPage();

		if ($page)
		{
			return nl2br($page->getSubtitle());
		}
	}
	
	
	/**
	 * Returns collection of subpages of the given page ID or, by default, from the current visited page
	 *
	 */
	public function getSubPages($id_page = NULL, $conditions=array())
	{
		$fromPage = NULL;

		// From which page ?
		if ( is_null($id_page))
		{
			$fromPage = Mage::helper('ionize/page')->getRegisteredPage();
		}
		else
		{
			$fromPage = $this->getPageById($id_page);
		}

		// Sub pages : See if Use of one helper to get pages from cache
		$pages  = Mage::getModel('ionize/page')->getPages()->addFilter('id_parent', $fromPage->getId());


		foreach ($conditions as $key=>$value)
		{
			$pages->addFilter($key, $value);
		}

		return $pages;

	}
	
	
	
	
	/**
	 * Returns one subpage
	 *
	 * @param	Array	Array of filters
	 *
	 */
	public function getSubPage($id_page=NULL, $conditions = array())
	{
		if ( is_null($id_page))
		{
			$fromPage = Mage::helper('ionize/page')->getRegisteredPage();
		}
		else
		{
			$fromPage = $this->getPageById($id_page);
		}
		
		$pages  = Mage::getModel('ionize/page')->getPages()->addFilter('id_parent', $fromPage->getId());
		
		foreach ($conditions as $key=>$value)
		{
			$pages->addFilter($key, $value);
		}
		
		return $pages->getFirstItem();
		
	}
	
	
	/**
	 * Returns Articles from a given page or, by default, from the current visited page
	 *
	 * @param 	int 		parent page ID
	 * @param 	array 		Array of conditions to send to the model
	 *
	 * @return 	Varien_Data_Collection		Collection of Articles
	 *
	 */
	public function getArticles($id_page = NULL, $conditions=array())
	{
		$fromPage = NULL;
		
		// Instanciate basic collection
		$articles = new Varien_Data_Collection();

		// From which page ?
		if ( is_null($id_page))
		{
			$fromPage = Mage::helper('ionize/page')->getRegisteredPage();
		}
		else
		{
			$fromPage = $this->getPageById($id_page);
		}

		$articles = Mage::getModel('ionize/article')->getArticles($fromPage, $conditions);

// HERE
// Get, for each article, the target URL
// 		
//		

		return $articles;
	}
	
	
	/**
	 * Return one Page based on its ID
	 *
	 * @param 	int 		ID page
	 *
	 * @return 	Mixed		Page Object
	 *						NULL if nothing found
	 *
	 */
	public function getPageById($id_page = NULL)
	{
		if ( ! is_null($id_page) )
		{
			$pages  = Mage::helper('ionize/navigation')->getPages();
			
			foreach($pages as $page)
			{
				if ($page->getId() == $id_page)
					return $page;
			}
		}
		
		return NULL;
	}
	
	
	/**
	 * Sets the window title
	 * Used by Block/Page/Html/Head to display the window title
	 *
	 */
	public function setWindowTitle($title = NULL)
	{
        if ($headBlock = $this->getLayout()->getBlock('head'))
        {
			$page = Mage::helper('ionize/page')->getRegisteredPage();
    
            if ($page && $page->hasData())
            {
            	$windowTitle = $page->getMeta_title();
            	
            	if ($windowTitle == '')
            		$windowTitle = $page->getTitle();
            	
            	$this->_windowTitle = $windowTitle;
            	
	            $headBlock->setTitle( $windowTitle );
	        }
        }
		
	}
	
}