<?php
 
class Partikule_Ionize_Block_Page_Html_Breadcrumbs extends Mage_Core_Block_Template
{
    protected $_crumbs = null;	
	
	
	protected function _construct()
	{
		$this->addData(array(
			'cache_lifetime'	=> false,
			'cache_tags'		=> array('ionize_breadcrumbs', Mage_Core_Model_Store_Group::CACHE_TAG),
		));
	}
	
	
    protected function _prepareLayout()
    {
    	$this->_crumbs = Mage::helper('ionize/Breadcrumbs')->getBreadcrumbs();

	    return parent::_prepareLayout();
    }
	
	
	
    protected function _toHtml()
    {
    	// As the Ionize Breadcrumb always countains the link to Home, only display if not only "Home" is present
        if (is_array($this->_crumbs)) {
            reset($this->_crumbs);
            $this->_crumbs[key($this->_crumbs)]['first'] = true;
            end($this->_crumbs);
            $this->_crumbs[key($this->_crumbs)]['last'] = true;
        }
		
		if (count($this->_crumbs) < 2)
		{
			array_pop($this->_crumbs);
        }

       	$this->assign('crumbs', $this->_crumbs);
        return parent::_toHtml();
    }



}