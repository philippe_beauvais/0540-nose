<?php


class Partikule_Ionize_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{

    /**
     * Set title element text
     *
     * @param string $title
     * @return Mage_Page_Block_Html_Head
     */
    public function setTitle($title)
    {
        $this->_data['title'] = Mage::getStoreConfig('design/head/title_prefix') . ' | ' . $title
            . ' ' . Mage::getStoreConfig('design/head/title_suffix');
        return $this;
    }

    /**
     * Retrieve title element text (encoded)
     *
     * @return string
     */
    public function getTitle()
    {
    	
        if (empty($this->_data['title'])) {
            $this->_data['title'] = $this->getDefaultTitle();
        }
        return htmlspecialchars(html_entity_decode(trim($this->_data['title']), ENT_QUOTES, 'UTF-8'));
    }


	/**
	 * From excellent module : https://github.com/jreinke/magento-suffix-static-files
	 * Integrated to Ionize Module :
	 *
	 * 1. Because the original one rewrites page_html_head block, and Ionize do so also
	 * 2. To bundle this module to the Ionize one.
	 *
	 * @param string $format
	 * @param array  $staticItems
	 * @param array  $skinItems
	 * @param null   $mergeCallback
	 *
	 * @return string
	 */
	protected function &_prepareStaticAndSkinElements($format, array $staticItems, array $skinItems, $mergeCallback = NULL)
	{
		$designPackage = Mage::getDesign();
		$baseJsUrl = Mage::getBaseUrl('js');
		$items = array();
		if ($mergeCallback && !is_callable($mergeCallback)) {
			$mergeCallback = NULL;
		}

		// get static files from the js folder, no need in lookups
		foreach ($staticItems as $params => $rows) {
			foreach ($rows as $name) {
				$items[$params][] = $mergeCallback ? Mage::getBaseDir() . DS . 'js' . DS . $name : $baseJsUrl . $name;
			}
		}

		// lookup each file basing on current theme configuration
		foreach ($skinItems as $params => $rows) {
			foreach ($rows as $name) {
				$items[$params][] = $mergeCallback ? $designPackage->getFilename($name, array('_type' => 'skin'))
					: $designPackage->getSkinUrl($name, array());
			}
		}

		$html = '';
		foreach ($items as $params => $rows) {
			// attempt to merge
			$mergedUrl = FALSE;
			if ($mergeCallback) {
				$mergedUrl = call_user_func($mergeCallback, $rows);
			}
			// render elements
			$params = trim($params);
			$params = $params ? ' ' . $params : '';
			if ($mergedUrl) {
				if ($this->isUrlSuffixEnabled()) {
					$mergedUrl .= '?q=' . urlencode($this->getUrlSuffix());
				}
				$html .= sprintf($format, $mergedUrl, $params);
			} else {
				foreach ($rows as $src) {
					if ($this->isUrlSuffixEnabled()) {
						$src .= '?q=' . urlencode($this->getUrlSuffix());
					}
					$html .= sprintf($format, $src, $params);
				}
			}
		}
		return $html;
	}

	public function isUrlSuffixEnabled()
	{
		return Mage::getStoreConfigFlag('dev/suffix_js_css/enable');
	}

	public function getUrlSuffix()
	{
		return Mage::getStoreConfig('dev/suffix_js_css/suffix');
	}



}