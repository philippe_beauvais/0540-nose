<?php
 
class Partikule_Ionize_Block_Articles extends Partikule_Ionize_Block_Abstract
{
	
	protected function _construct()
	{
	}
	
	/**
	 * Return one article, from current visited Page
	 * We suppose in this case the URL looks like this : 
	 * http://the_domain/the_page/the_page2/the_article
	 *
	 * @param 	array 			Conditions array
	 * @return 	Article
	 *
	 * @usage	In template : $article = $this->getArticle();
	 *
	 *
	 * @TODO :		REPLACE ALL OCCURENCES OF CALL TO THIS METHOD BY CALL TO  : getCurrentArticle()
	 *				NO TIME TO DO THIS NOW...
	 *
	 */
	public function getArticle($conditions = array())
	{
		$fromPage = Mage::helper('ionize/page')->getRegisteredPage();

		$articleUrl = $this->_getArticleUrl();

		$article = Mage::getModel('ionize/article')->getArticleByUrl($articleUrl, $fromPage);
		
		Mage::helper('ionize/article')->setRegisteredArticle($article);

		return $article;
	}	
	
	/**
	 * Return one article, from current visited Page
	 * We suppose in this case the URL looks like this : 
	 * http://the_domain/the_page/the_page2/the_article
	 *
	 * @return 	Article
	 *
	 * @usage	In template : $article = $this->getArticle();
	 *
	 */
	public function getCurrentArticle()
	{
		$fromPage = Mage::helper('ionize/page')->getRegisteredPage();

		$articleUrl = $this->_getArticleUrl();

		$article = Mage::getModel('ionize/article')->getArticleByUrl($articleUrl, $fromPage);

		Mage::helper('ionize/article')->setRegisteredArticle($article);

		return $article;
	}	
	
	
	public function getPageFirstArticle($conditions = array())
	{
		$fromPage = Mage::helper('ionize/page')->getRegisteredPage();
		
		$article = Mage::getModel('ionize/article')->getArticles($fromPage, $conditions)->getFirstItem();

		Mage::helper('ionize/article')->setRegisteredArticle($article);

		return $article;
	}
	

	/**
	 * Return articles collection, from current visited Page
	 *
	 * @param 	array 			Conditions array
	 * @return 	Collection
	 *
	 * @usage	In template : $slider = $this->getArticles(array('limit' => 2, 'type' => the_type_name));
	 *
	 */
	public function getArticles($conditions = array())
	{
		// Instanciate basic collection
		$articles = new Varien_Data_Collection();;
		
		// Get the page model
		// Registered by the controller: Partikule_Ionize_IndexController
		
		// Get the current Magento registered Page
		$page = $this->getPage();

		// Articles Collection
		if ( $page->hasData() )
			$articles = Mage::getModel('ionize/article')->getArticles($page, $conditions);

		return $articles;
	}	
	
	
	/**
	 * Returns articles from a given page
	 *
	 * @param 	string 			Page name
	 * @param 	array 			Conditions array
	 * @return 	Collection
	 *
	 */
	public function getArticlesFromPage($page_name, $conditions=array())
	{
		// Instanciate basic collection
		$articles = new Varien_Data_Collection();;

        $page = Mage::getModel('ionize/page')->getPageByName($page_name);
        
		// Articles Collection
		if ( $page->hasData() )
			$articles = Mage::getModel('ionize/article')->getArticles($page, $conditions);
		
		return $articles;
	}


	/**
	 * In case Articles is used to display Footer articles.
	 * This maintains compatibility
	 * Not sure...
	 *
	 */
    public function getCopyright()
    {
        if (!$this->_copyright) {
            $this->_copyright = Mage::getStoreConfig('design/footer/copyright');
        }

        return $this->_copyright;
    }
	
	
	/**
	 * Get the URI of the current displayed article
	 *
	 * @return 	string		URI part of the article
	 *
	 *
	 */
	private function _getArticleUrl()
	{
		$_segments = explode('/', $this->helper('core/url')->getCurrentUrl());
		$_uri = array_pop($_segments);

		// Prevent URI args to be included in article URI segment
		$_uri =  explode('?', $_uri);
		$_uri = array_shift($_uri);

		return $_uri;
	}
	
	

    
    
}