<?php
 
/**
 * Block of Products linked to one article
 *
 */
class Partikule_Ionize_Block_Catalog_Product_List_Article extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    protected $_productIdList;


    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     *
     */
    public function getProductCollection()
    {
		if (is_null($this->_productCollection))
		{
			$_article = Mage::registry('article');

			if($_article && $_article->hasData())
			{
				// Get Products IDs linked to this page through Ionize
				$product_ids = Mage::getModel('ionize/product')->getProductIdFromArticle($_article->getId());

				$storeId = Mage::app()->getStore()->getId();
				$collection = Mage::getModel('catalog/product')->getCollection()
					->addAttributeToSelect("*")
					->setStoreId( $storeId )
					->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
					->addAttributeToFilter('entity_id', array('in' => $product_ids))
					->addAttributeToFilter('type_id', 'configurable')
				;
				
				// ORDER BY FIELD : Keep the user's product order
				$collection->getSelect()->order(new Zend_Db_Expr("FIELD(e.entity_id, '" . implode("','", $product_ids) . "')"));

				$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

				// Join to perfume_detail to get the perfume details
/*
		        $productCollection->getSelect()->joinLeft(
					array('pd' => 'perfume_detail'),
					"pd.nose_sku = e.sku and pd.lang='". Mage::helper('attribute/core')->getCurrentStoreCode() ."'",
					array('*')
				);
				// Mage::log("{$productCollection->getSelect()}".PHP_EOL, null, 'sql.log');

				// Get the category url_path attribute table and id
				// $collection->joinTable('catalog/category_product', 'product_id=entity_id', array('category_id' => 'category_id'));
				$res = Mage::getSingleton('core/resource');
				$eav = Mage::getModel('eav/config');
				$nameattr = $eav->getAttribute('catalog_category', 'url_path');
				
				$nametable = $res->getTableName('catalog/category') . '_' . $nameattr->getBackendType();
				$nameattrid = $nameattr->getAttributeId();
		
				$productCollection->joinTable('catalog/category_product', 'product_id=entity_id', array('single_category_id' => 'category_id'), null, 'left')
					->groupByAttribute('entity_id')
					->joinTable(
						$nametable,
						'entity_id=single_category_id', 
						array('category_url' => 'value'),
						array('attribute_id'=> $nameattrid, 'store_id' => $storeId), 
						'left'
					)
					->getSelect()->columns(array('category_urls' => new Zend_Db_Expr("IFNULL(GROUP_CONCAT(`$nametable`.`value` SEPARATOR '; '), '')")))
				;
*/
				
				
				$this->_productCollection = $collection;
			}
		}
		return $this->_productCollection;
    }
	
    /**
     * Retrieve products IDs array
     *
     * @return array 	Array of products IDs
     *
     */
    public function getProductIdList()
    {
		if (is_null($this->_productIdList))
		{
			$_page = Mage::registry('page');
			
			if($_page->hasData())
			{
				// Get Products IDs linked to this page through Ionize
				$product_ids = Mage::getModel('ionize/product')->getProductIdFromPage($_page->getId());

				$this->_productIdList = $product_ids;
			}		
		}
		return $this->_productIdList;
    }
	
	

	/**
	 * Returns the cheapest simple product child of the configurable
	 *
	 * Uses the Boutique Helper
	 *
	 */
/*
	public function getCheapestSimple($product)
	{
		return Mage::helper('boutique/Product')->getCheapestSimple($product);
	}
*/

	
}	
