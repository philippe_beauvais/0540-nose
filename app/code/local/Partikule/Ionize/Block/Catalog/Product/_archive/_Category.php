<?php
 
/**
 * List of Products from one category linked to a page
 *
 */
class Partikule_Ionize_Block_Catalog_Product_Category extends Mage_Catalog_Block_Product_List
{
    /**
     * List of categories Id (array)
     *
     */
    protected $_categoryIdList;



	public function getProductsFromCategory()
	{
		$category_id = $this->getFirstCategoryId();
	
		if ( ! is_null($category_id))
		{
			$productCollection = Mage::getModel('catalog/category')->load($category_id)->getProductCollection()
//				->addAttributeToSelect('*')
				->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
//				->addAttributeToFilter('visibility', 4)
				;
			
	$attribute_options = Mage::helper('ionize/data')->getAttributeOptions('Classification');		

trace($attribute_options);

$category_options = Mage::helper('ionize/data')->getCategoryOptions();

trace($category_options);
			
			
			foreach($productCollection as $p)
			{
			
	trace($p->getData());
// Get the Attribute Set

//$default_attribute = $p->getDefaultAttributeSetId();

// $attributes = $p->getAttributes();

//$attributes = $p->getReservedAttributes();

/*
$res = Mage::getSingleton('core/resource');
$eav = Mage::getModel('eav/config');

$nameattr = $eav->getAttribute('catalog_category', 'name');
$nametable = $res->getTableName('catalog/category') . '_' . $nameattr->getBackendType();
$nameattrid = $nameattr->getAttributeId();

*/


//				trace( $attributes );
			}
			
			
			return $productCollection;
			/*
			$productCollection = Mage::getResourceModel('catalog/product_collection')
    	        ->addCategoryFilter($category);
			*/
			
		}
		else
		{
			return new Varien_Data_Collection();
		}

	}






	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getProductsFromCategory_backup()
	{
		$category_id = $this->getFirstCategoryId();

		if ( ! is_null($category_id))
		{
			$productCollection = Mage::getModel('catalog/category')->load($category_id)->getProductCollection()
				->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
//				->addAttributeToFilter('visibility', 4)
				;
			
			return $productCollection;
			/*
			$productCollection = Mage::getResourceModel('catalog/product_collection')
    	        ->addCategoryFilter($category);
			*/
			
		}
		else
		{
			return new Varien_Data_Collection();
		}
	}









	/**
	 * Returns the first category ID of one category list linked to a page
	 *
	 * @return 	int		Category ID
	 *
	 */
	public function getFirstCategoryId()
	{
		$category_id = NULL;
		
		$categories_ids = $this->getCategoryIdList();
		
		if ( ! empty($categories_ids))
			$category_id = $categories_ids[0];
		
		return $category_id;
	}


    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     *
     */
    public function getCategoryIdList()
    {
        return $this->_getCategoryIdList();
    }
	
	
	protected function _getCategoryIdList()
	{
		if (is_null($this->_categoryIdList))
		{
			$_page = Mage::registry('page');
			
			if($_page->hasData())
			{
				// Get Products IDs linked to this page through Ionize
				$category_ids = Mage::getModel('ionize/category')->getCategoryIdFromPage($_page->getId());

				$this->_categoryIdList = $category_ids;
			}		
		}
		return $this->_categoryIdList;
	}

	
	/**
	 * Returns the current Page
	 *
	 */
	public function getPage()
	{
		return Mage::helper('ionize/page')->getRegisteredPage();
	}


}	
