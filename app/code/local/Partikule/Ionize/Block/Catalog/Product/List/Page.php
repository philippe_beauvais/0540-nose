<?php
 
/**
 * Block of Products linked to a page
 *
 */
class Partikule_Ionize_Block_Catalog_Product_List_Page extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productIdList;

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 7200,
            'cache_tags' => array(Mage_Catalog_Model_Product::CACHE_TAG),
        ));
    }

    public function getCacheKeyInfo()
    {
        return array(
            'name_in_layout' => $this->getNameInLayout(),
            'store' => Mage::app()->getStore()->getId(),
            'design_package' => Mage::getDesign()->getPackageName(),
            'design_theme' => Mage::getDesign()->getTheme('template'),
        );
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     *
     */
    /*public function getPageProductCollection()
    {
		$_page = Mage::registry('page');

		if($_page->hasData())
		{
			// Get Products IDs linked to this page through Ionize
			$product_ids = Mage::getModel('ionize/product')->getProductIdFromPage($_page->getId());

			$storeId = Mage::app()->getStore()->getId();

			$collection = Mage::getModel('catalog/product')->getCollection()
				->setStoreId( $storeId )
				->addAttributeToSelect("*")
				->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
				->addAttributeToFilter('entity_id', array('in' => $product_ids))
				->addAttributeToFilter('type_id', 'configurable')
			;

			$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);
			
			// Mage::getModel('catalog/layer')->prepareProductCollection($collection);
			
			// ORDER BY FIELD : Keep the user's choosen product order
			$collection->getSelect()->order(new Zend_Db_Expr("FIELD(e.entity_id, '" . implode("','", $product_ids) . "')"));

			return $collection;
		}

		return FALSE;
    }*/

    /**
     * Fix à supprimer après la production pour les nouveautés sur la homepage
     */
    public function getPageProductCollection()
    {
        $idHomeIonize = 327;

        $product_ids = Mage::getModel('ionize/product')->getProductIdFromPage($idHomeIonize);

        $storeId = Mage::app()->getStore()->getId();

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->setStoreId( $storeId )
            ->addAttributeToSelect("*")
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('entity_id', array('in' => $product_ids))
            ->addAttributeToFilter('type_id', 'configurable')
        ;

        $collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

        // Mage::getModel('catalog/layer')->prepareProductCollection($collection);

        // ORDER BY FIELD : Keep the user's choosen product order
        $collection->getSelect()->order(new Zend_Db_Expr("FIELD(e.entity_id, '" . implode("','", $product_ids) . "')"));

        return $collection;
    }
	
	
	/**
	 * Returns products grouped by category in one array
	 * 
	 *
	 *
	 */
    public function getPageCategoryProductCollection()
    {
    	$collection = $this->getPageProductCollection();
    	
    	// Category array to return
    	$data = array();
    	
    	foreach($collection as $_product)
    	{
    		$id_category = $_product->getSingleCategoryId();
    		
    		// Only returns products which has one category
    		if ($id_category)
    		{
	    		if ( ! isset($data[$id_category]))
	    		{
	    			$data[$id_category] = array(
	    				'id' => $id_category,
	    				'name' => $_product->getcategory_name(),
	    				'products' => array()
	    			);
	    		}
	    		$data[$id_category]['products'][] = $_product;
	    	}
    	}

    	return $data;
    }
	
	
	
	
	
    /**
     * Retrieve the new products collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     *
     */
    public function getPageNewProductCollection()
    {
    	$collection = $this->getPageProductCollection();
    	
    	if ($collection)
    	{
			// Takken from Mage_Catalog_Block_Product_New
			$todayDate	= Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
	
			$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

			// New One
			$collection->addStoreFilter()
				->addAttributeToFilter('news_from_date', array('or'=> array(
					0 => array('date' => true, 'to' => $todayDate),
					1 => array('is' => new Zend_Db_Expr('null')))
				), 'left')
				->addAttributeToFilter('news_to_date', array('or'=> array(
					0 => array('date' => true, 'from' => $todayDate),
					1 => array('is' => new Zend_Db_Expr('null')))
				), 'left')
				->addAttributeToFilter(
					array(
						array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
						array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
						)
				  )
				->addAttributeToFilter(array(array('attribute'=>'sku', 'nlike'=>'S-%')))
				->addAttributeToSort('news_from_date', 'desc')

				->setCurPage(1)
			;
		}
		return $collection;				
	}
	
	
	
	
    /**
     * Retrieve products IDs array
     *
     * @return array 	Array of products IDs
     *
     */
    public function getProductIdList()
    {
        return $this->_getProductIdList();
    }
	
	
	protected function _getProductIdList()
	{
		if (is_null($this->_productIdList))
		{
			$_page = Mage::registry('page');
			
			if($_page->hasData())
			{
				// Get Products IDs linked to this page through Ionize
				$product_ids = Mage::getModel('ionize/product')->getProductIdFromPage($_page->getId());

				$this->_productIdList = $product_ids;
			}		
		}
		return $this->_productIdList;
	}

	
	/**
	 * Returns the cheapest simple product child of the configurable
	 *
	 * Uses the Boutique Helper
	 *
	 */
	public function getCheapestSimple($product)
	{
		return Mage::helper('boutique/Product')->getCheapestSimple($product);
	}

	
    /**
     * Initialize product collection
     * 
     * Adapted from Mage_Catalog_Model_Layer
     * Returns the input collection intead one Mage_Catalog_Model_Layer 
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     *
    public function prepareProductCollection($collection)
    {
        $collection
//            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents();
            //->addStoreFilter()
            //->addUrlRewrite($this->getCurrentCategory()->getId());

//        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
//        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $collection;
    }
     */


}	
