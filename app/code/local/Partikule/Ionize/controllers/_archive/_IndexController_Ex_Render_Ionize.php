<?php
// Controllers must manually be included in order to extend them, so include Mage_Cms_IndexController
// require_once Mage::getModuleDir('controllers', 'Mage_Cms').DS.'IndexController.php';
// N'importe quoi lui ! 

class Partikule_Ionize_IndexController extends Mage_Cms_IndexController
{
	// Page data. FALSE by default or Page Model
	protected $_page = FALSE;


	public function preDispatch()
	{
		parent::preDispatch();

		// $this->getRequest()->setRouteName('cms');

		//  $navigation_helper = Mage::helper('ionize/navigation');

		//  $this->_uri = $navigation_helper->getPageUriSegment($this);
	}


	/**
	 * Render CMS 404 Not found page
	 *
	 * @param string $coreRoute
	 *
	 *
	 */
	public function noRouteAction($coreRoute = null)
		// public function ionizeRouteAction($coreRoute = null)
		{
		$pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);

		// returns 'no-route'
		// trace('XML_PATH_NO_ROUTE_PAGE : ' . $pageId);

		// Return "cms"
		// trace($this->getRequest()->getRouteName());

		// Returns the path : /marques/marque/etc
		// trace($this->getRequest()->getOriginalPathInfo());


		if ( ! Mage::helper('cms/page')->renderPage($this, $pageId))
		{
			// If Ionize Route exists (means one page is found)
			// Could be done by one helper, but as this action will not be used anywhere else...
			//formerly :  if ( ! $response = $this->renderPage($pageId))
			
			if ( ! $this->_page = $this->_getIonizePage() ) 			
			{
				$this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
				$this->getResponse()->setHeader('Status','404 File not found');

				$this->_forward('defaultNoRoute');
			}
			else
			{			
				/*
				 * TODO : Look if this kind of answer is consistent.
				 * Better to return something else, like false
				 * and not to check this
				 *
				 */
				if(is_int($this->_page))
				{
					switch($this->_page)
					{
					case 404:
						$this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
						$this->getResponse()->setHeader('Status','404 File not found');

						$this->_forward('defaultNoRoute');
						break;

					case 500:
						$this->getResponse()->setHeader('HTTP/1.1','500 Internal Server Error');
						$this->getResponse()->setHeader('Status','500 Internal Server Error');

						$this->_forward('defaultNoRoute');
						break;

					case 403:
						$this->getResponse()->setHeader("Location", Mage::getBaseUrl()."customer/account/login")->sendHeaders();

						break;
					default:
						print_r($response);
					}
				}
				else
				{
//					$this->_forward('ionizeRoute');
					$this->ionizeRouteAction();

					/*
					 * 1. Ceci fonctionne mais bof :
					 */
					//     echo $response;
					//     die();


					/*
					 * Passer par la création d'un bloc : Oui, mais quel intérêt ?
					 */

					// Register $response to be able to get it in the block
					//     Zend_Registry::set('ionizeResponse', $response);

					/*
					$test = $this->getLayout()
						->createBlock('ionize/page')
//						->setBlockId('ionize_page')
						->setTemplate('ionize/articles_dynamic.phtml')
						->toHtml();
*/

					// Reprise de Cms/Helper/Page.php

					//       $page = Mage::getSingleton('cms/page');
					//  $page->setStoreId(Mage::app()->getStore()->getId());

					/*
        $this->getLayout()->getUpdate()
           ->addHandle('default')
           ->addHandle('cms_page');

        $this->addActionLayoutHandles();
*/
					/*
		$this->getLayout()->helper('page/layout')
			->applyTemplate('ionize_articles');
*/
					//$this->loadLayout();
					//$this->getLayout()->getBlock('root')->setTemplate('ionize/articles_dynamic.phtml');
					//$this->renderLayout();

					// trace($page->getId());

					/*
					$test = $this->getLayout();
					$test->createBlock('ionize/page');
					trace($test);
*/
					/*
						->createBlock('ionize/page')
						->setData(array('content'=>$response))
						->setTemplate('ionize/articles_dynamic.phtml')
						->toHtml();

					Mage::getSingleton('core/layout')->createBlock('catalog/product_list');
					*/








					//     $this->getResponse()->setBody($test);
					//     $this->renderLayout();
					// trace($test);


					/* Ceci fonctionne bien et présente l'avantage de préserver les entêtes Magento. :
					*/
					//     $this->getResponse()->setBody($response);
					//     $this->renderLayout();


				}
			}
		}

	}



	public function ionizeRouteAction()
	{
		trace('ionizeRouteAction()');

/*
		$page = Mage::getSingleton('ionize/page');
		$page->setData($this->_page);
*/

		$this->loadLayout();
		
		
		// Send page variable to the root block
		// Had a look there : http://stackoverflow.com/questions/4006183/magento-passing-data-between-a-controller-and-a-block
		// Nothing worked but this.
		$this->getLayout()->getBlock('root')->assign('page', $this->_page);
		
		
trace($this->_page->getData());
		
		
		// Define the template the root block must use.
		$this->getLayout()->getBlock('root')->setTemplate('ionize/articles_dynamic.phtml');
		
		
		
		$this->renderLayout();

	}

	/**
	 * Default no route page action
	 * Used if no route page don't configure or available
	 * Overwriting to remove automatically setting 404 response headers
	 */
	public function defaultNoRouteAction()
	{
		trace('<h1>defaultNoRouteAction() : Ionize 404</h1>');

		$this->loadLayout();
		$this->renderLayout();

	}
	
	
	public function renderPage($pageId = null)
	{
		// method to retrieve content from Ionize
		$response = $this->_getIonizeContent();

		// Check the response code to see if an error code was returned, or a request object (this is setup in Ionize to return expected codes
		if(is_int($valid = $this->_validateIonizeResponse($response)))
		{
			return $valid;
		}
		return $response->getRawBody();
	}
	
	
	/**
	 * Retrieves one Ionize Page
	 *
	 * @return		mixed		Object, Page object if page object data aren't empty
	 *							FALSE,	if no page was found
	 *
	 */
	private function _getIonizePage()
	{
		// Page URI, eg "brands", "marques", ...
		$uri = $this->_getIonizeResourceUri();
		
        $page = Mage::getModel('ionize/page')->getPageByUrl($uri);
		
		$page_data = $page->getData();
		
		if ( ! empty($page_data))
			return $page;
			
		return FALSE;
	}
	
	
	/**
	 * Returns the Ionize's URL process output
	 * @notice : 	Not so good, as data are processed by Ionize.
	 *				Ionize SHOULD NOT process any data !!!
	 *
	 */
	private function _getIonizeContent()
	{
		// Magento POST data
		$magento_post = $this->_getPostData();

		// Set our request method
		$method = ( count($magento_post) == 0 ) ? 'GET' : 'POST';

		// Make curl request to Ionize

		$client = new Varien_Http_Client($this->_getIonizeContentUrl());

		$client->setCookieJar(); // Need this to persist the session across multiple requests
		$client->setParameterPost($magento_post);
		$client->setHeaders(array('Accept-encoding' => '',
				'Cookie' => array(
					'PHPSESSID='.$magento_post['magentoSession']
				)
			));

		$response = $client->request($method);

		return $response;
	}

	/**
	 * Retrieves one Ionize resource URI
	 *
	 * @return  string  Ionize resource URI, eg. "brands"
	 *
	 */
	private function _getIonizeResourceUri()
	{
		$navigation_helper = Mage::helper('ionize/navigation');

		// Page segment URI. "marques" or "byredo"
		$uri = $navigation_helper->getPageUriSegment($this);
		
		return $uri;
	}


	/**
	 * Returns the Ionize URL to the content
	 *
	 * @return  string  Complete URL, eg. http://domain.tld/ion/fr/brands
	 *
	 */
	private function _getIonizeContentUrl()
	{
		$navigation_helper = Mage::helper('ionize/navigation');

		// Page segment URI. "marques" or "byredo"
		$uri = $navigation_helper->getPageUriSegment($this);

		// Real base URL, without store code
		$base_url = Mage::getStoreConfig('web/unsecure/base_url');

		$url = Mage::getStoreConfig('web/unsecure/base_url') . 'ion/' . Mage::app()->getStore()->getCode() . $this->getRequest()->getRequestString();

		return $url;
	}
	
	
	/**
	 * Get the Magento Post data in order to send then to Ionize
	 *
	 * @return		array		Magento Post data
	 *
	 */
	private function _getPostData($pageId)
	{
		$post = $this->getRequest()->getPost();

		//Get the info we need about the user, to pass to silverstripe
		$customerSession = Mage::getModel('customer/session');
		$isLogged = $customerSession->isLoggedIn();

		$post['magentoSession'] = $customerSession->getSessionId();
		$post['isUserLogged'] = $isLogged;
		$post['mageUserId'] = $customerSession->getCustomerId();
		
		return $post;
	}

	
	
	
	
	
	
	
	

	/**
	 * Admin part : Not done yet
	 *
	 */
	public function ionizeAdmin()
	{
		$response = $this->_getIonizeAdmin();

		// Check the response code to see if an error code was returned, or a request object (this is setup in Ionize to return expected codes
		//  if(is_int($valid = $this->_validateIonizeResponse(NULL, $response)))
		//  {
		//   return $valid;
		//  }
		return $response->getRawBody();

	}

	private function _getIonizeAdmin()
	{
		// Build our uri, notice we are assuming that Ionize was installed inside Magento, in a folder named 'ion'
		$uri = Mage::getBaseUrl(). "ion/admin";
		$uri = str_replace('index.php/', '', $uri);

		$client = new Varien_Http_Client($uri);
		$client->setCookieJar(); // Need this to persist the session across multiple requests

		$response = $client->request('GET');

		return $response;
	}


	private function _validateIonizeResponse($response)
	{
		if($response->isError())
		{
			//Return just the error code
			return $response->getStatus();

		}
		return $response;
	}






}