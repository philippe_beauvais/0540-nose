<?php

class Partikule_Ionize_IndexController extends Mage_Cms_IndexController
{
	// Page data. FALSE by default or Page Model
	protected $_page = FALSE;


	public function preDispatch()
	{
		parent::preDispatch();

//		Change the handler to cms...
//		$this->getRequest()->setRouteName('cms');

		// Try to get one Ionize page matching the URL
		$this->_page =  Mage::helper('ionize/page')->getIonizePage($this);

		// Mage::log('Mage Store code : ' . Mage::helper('ionize/core')->getCurrentStoreCode(), null, 'page.log');
		
		// Removed for the moment
		// See bellow...
		// $this->_checkPageRoute();

		// Register the page model : Blocks will be able to get the page
		Mage::register('page', $this->_page, TRUE);
	}
	
	
	
	/**
	 * Checks the route by comparing the URL store code and the current page store code
	 * Redirects to the correct page if they are different.
	 *
	 *
	 */
	private function _checkPageRoute()
	{
		// Store Code form URL request
		$storeCode = $this->getRequest()->getStoreCodeFromPath();

		// The store code switched !
		/*
		 * We NEVER go in this code
		 * This was originally to put away the very beautiful : ?___from_store=en
		 * from URL when switching language
		 * As ?___from_store=en is used by Mage for the product page lang switch, 
		 * we do not change that fo rthe moment.
		 *
		 */
		if ($storeCode != $this->_page->getLang() && $this->_page->getName())
		{
			// Get the correct asked page
			$page = Mage::getModel('ionize/page')->getPageById($this->_page->getId_page());

			$base_url = Mage::helper('ionize/core')->getBaseUrl();

			$this->getResponse()
				->setRedirect($page->getPath(), 301);
//				->setRedirect($base_url . $storeCode.'/'.$page->getUrl(), 301);
		}
	}

	
	
	/**
	 * Home page
	 *
	 * If Ionize defines a Magento Template for the Home page, use it.
	 * Else, use the default Magento Home page
	 *
	 * This does not alter the Magento behavior regarding the home page.
	 *
	 */
	/*public function indexAction($coreRoute = null)
	{
		// Check if the Ionize Home page has a defined Magento template
		if ($this->_page->hasData() && $this->_page->getMagento_template() != '')
		{
			$this->ionizeRouteAction();
		}
		else
		{
			 $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);

			 if ( ! Mage::helper('cms/page')->renderPage($this, $pageId))
			 {
				 $this->_forward('defaultIndex');
			 }
		}
	}*/


	/**
	 * Uses the "CMS No Route Page" to potentialy use the Ionize page
	 *
	 * @param string $coreRoute
	 *
	 *
	 */
	public function noRouteAction($coreRoute = null)
	{
		// No data for the Ionize page 
		if ( $this->_page->hasData() && $this->_page->getMagento_template() != '') 			
		{
			$this->ionizeRouteAction();
		}
		else
		{
			$pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);

			// Mage::log('Forward route !!! ' . $pageId, null, 'page.log');

			// If the render of the template "no-route" isn't possible
			if ( ! Mage::helper('cms/page')->renderPage($this, $pageId))
			{
				// Render the default Magento NoRoute path
				$this->_forward('defaultNoRoute');
			}
		}
	}


	/**
	 * Sets the Magento root template (defined from Ionize) to use
	 * and renders the layout
	 *
	 */
	public function ionizeRouteAction()
	{
		$this->loadLayout();

		// Sets the header Title
		$this->getLayout()->getBlock('header')->setHeaderTitle($this->_page->getMetaTitle());
		
		// Define the template the root block must use.
		$this->getLayout()->getBlock('root')->setTemplate('ionize/page/' . $this->_page->getMagentoTemplate());
		
		$this->renderLayout();
	}



	
	/**
	 * Get the Magento Post data in order to send then to Ionize
	 *
	 * @return		array		Magento Post data
	 *
	private function _getPostData($pageId)
	{
		$post = $this->getRequest()->getPost();

		//Get the info we need about the user, to pass to silverstripe
		$customerSession = Mage::getModel('customer/session');
		$isLogged = $customerSession->isLoggedIn();

		$post['magentoSession'] = $customerSession->getSessionId();
		$post['isUserLogged'] = $isLogged;
		$post['mageUserId'] = $customerSession->getCustomerId();
		
		return $post;
	}
	 */
	
	protected function _prepareLayout()
	{
		trace('Index Controller Prepare Layout');
	}

	
	
	
	
	
	// ------------------------------------------------------------------------

	
	
	
	

	/**
	 * Admin part : Not done yet
	 *
	 */
	public function ionizeAdmin()
	{
		$response = $this->_getIonizeAdmin();

		// Check the response code to see if an error code was returned, or a request object (this is setup in Ionize to return expected codes
		//  if(is_int($valid = $this->_validateIonizeResponse(NULL, $response)))
		//  {
		//   return $valid;
		//  }
		return $response->getRawBody();

	}

	private function _getIonizeAdmin()
	{
		// Build our uri, notice we are assuming that Ionize was installed inside Magento, in a folder named 'ion'
		$uri = Mage::getBaseUrl(). "ion/admin";
		$uri = str_replace('index.php/', '', $uri);

		$client = new Varien_Http_Client($uri);
		$client->setCookieJar(); // Need this to persist the session across multiple requests

		$response = $client->request('GET');

		return $response;
	}


	private function _validateIonizeResponse($response)
	{
		if($response->isError())
		{
			//Return just the error code
			return $response->getStatus();

		}
		return $response;
	}

	
	// ------------------------------------------------------------------------

	
	/**
	 * Renders the Ionize output as a raw page
	 * Could be used to render Ionize blocks, but it is not a good practice
	 *
	 * @DEPRECATED FOR THE MOMENT 
	 *
	public function renderPage($pageId = null)
	{
		// method to retrieve content from Ionize
		$response = $this->_getIonizeContent();

		// Check the response code to see if an error code was returned, or a request object (this is setup in Ionize to return expected codes
		if(is_int($valid = $this->_validateIonizeResponse($response)))
		{
			return $valid;
		}
		return $response->getRawBody();
	}
	 */
	
	
	
	/**
	 * Returns the Ionize's URL process output
	 *
	 * @DEPRECATED FOR THE MOMENT 
	 *
	 * @notice : 	Not so good, as data are processed by Ionize.
	 *				Ionize SHOULD NOT process any data !!!
	 *
	private function _getIonizeContent()
	{
		// Magento POST data
		$magento_post = $this->_getPostData();

		// Set our request method
		$method = ( count($magento_post) == 0 ) ? 'GET' : 'POST';

		// Make curl request to Ionize

		$client = new Varien_Http_Client($this->_getIonizeContentUrl());

		$client->setCookieJar(); // Need this to persist the session across multiple requests
		$client->setParameterPost($magento_post);
		$client->setHeaders(array('Accept-encoding' => '',
				'Cookie' => array(
					'PHPSESSID='.$magento_post['magentoSession']
				)
			));

		$response = $client->request($method);

		return $response;
	}
	 */

	/**
	 * Returns the Ionize URL to the content
	 *
	 * @return	 string	 Complete URL, eg. http://domain.tld/ion/fr/brands
	 *
	private function _getIonizeContentUrl()
	{
		$navigation_helper = Mage::helper('ionize/navigation');

		// Page segment URI. "marques" or "byredo"
		$uri = $navigation_helper->getPageUriSegment($this);

		// Real base URL, without store code
		$base_url = Mage::getStoreConfig('web/unsecure/base_url');

		$url = Mage::getStoreConfig('web/unsecure/base_url') . 'ion/' . Mage::app()->getStore()->getCode() . $this->getRequest()->getRequestString();

		return $url;
	}
	 */
	

}