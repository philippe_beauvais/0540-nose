<?php

class Partikule_Ionize_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

	/*
	 * Default Action
	 * @TODO : Regarder fonctionnement breadcrumb
	 *
	 */
	public function indexAction()
	{
//		Mage::log('Ionize Module', Zend_Log::DEBUG, 'ionize.log');
		
		// URL parts
		$params = $this->getRequest()->getParams();


		// $this->loadLayout()->_setActiveMenu('ionize/set_time')->_addBreadcrumb('Test Page','Test Page');
		$this->loadLayout()->_setActiveMenu('ionize/pages');

		$this->renderLayout();
		
		

		// $this->loadLayout()->renderLayout();
	}

	public function editAction()
	{
		$pageId = $this->getRequest()->getParam('id');
		
		$pageModel = Mage::getModel('ionize/page')->load($pageId);

// trace($pageModel->getId());

		if ($pageModel->getId() || $pageModel == 0)
		{
			Mage::register('page_data', $pageModel);
			
//trace(Mage::get_config());
			
			$this->loadLayout();
			$this->_setActiveMenu('ionize/pages');
			$this->_addBreadcrumb('test Manager', 'test Manager');
			$this->_addBreadcrumb('Test Description', 'Test Description');
			
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
			// Creates the blocks : adminhtml/page/edit
			//						adminhtml/page/edit/tabs
			$this->_addContent($this->getLayout()->createBlock('ionize/adminhtml_page_edit'))
				->_addLeft($this->getLayout()->createBlock('ionize/adminhtml_page_edit_tabs')
			);
			
			$this->renderLayout();
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError('Page does not exist');
			$this->_redirect('*/*/');
		}
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	public function saveAction()
	{
		if ($this->getRequest()->getPost())
		{
			try {
				$postData = $this->getRequest()->getPost();
				$testModel = Mage::getModel('ionize/page');

				if( $this->getRequest()->getParam('id') <= 0 )
					$testModel->setCreatedTime(
						Mage::getSingleton('core/date')
						->gmtDate()
					);
				$testModel
				->addData($postData)
				->setUpdateTime(
					Mage::getSingleton('core/date')
					->gmtDate())
				->setId($this->getRequest()->getParam('id'))
				->save();

				Mage::getSingleton('adminhtml/session')
				->addSuccess('successfully saved');

				Mage::getSingleton('adminhtml/session')
				->settestData(false);

				$this->_redirect('*/*/');
				return;
			} catch (Exception $e){
				Mage::getSingleton('adminhtml/session')
				->addError($e->getMessage());

				Mage::getSingleton('adminhtml/session')
				->settestData($this->getRequest()
					->getPost()
				);

				$this->_redirect('*/*/edit',
					array('id' => $this->getRequest()
						->getParam('id')));
				return;
			}
		}
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		if($this->getRequest()->getParam('id') > 0)
		{
			try
			{
				$testModel = Mage::getModel('ionize/page');
				$testModel->setId($this->getRequest()
					->getParam('id'))
				->delete();
				Mage::getSingleton('adminhtml/session')
				->addSuccess('successfully deleted');
				$this->_redirect('*/*/');
			}
			catch (Exception $e)
			{
				Mage::getSingleton('adminhtml/session')
				->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
}