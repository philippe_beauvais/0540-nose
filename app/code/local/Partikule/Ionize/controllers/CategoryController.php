<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category	Mage
 * @package		Mage_Catalog
 * @copyright	Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license		http://opensource.org/licenses/osl-3.0.php	Open Software License (OSL 3.0)
 */

/**
 * Category controller
 *
 * @category   Mage
 * @package	   Mage_Catalog
 * @author	   Magento Core Team <core@magentocommerce.com>
 */
class Partikule_Ionize_CategoryController extends Mage_Catalog_CategoryController
{
	public function preDispatch()
	{
		parent::preDispatch();

		// Try to get one Ionize page matching the URL
		$this->_page =  Mage::helper('ionize/page')->getIonizePage($this);
		
		$this->_checkPageRoute();
		
		// Register the page model : Blocks will be able to get the page
		Mage::register('page', $this->_page, TRUE);
	}
	
	
	
	/**
	 * Category view action
	 * Rewritten to display Ionize page if the category URL has one corresponding Ionize URL
	 *
	 * @see Mage_Catalog_CategoryController
	 *
	 */
	public function viewAction()
	{
		
		// Check if the Ionize Home page has a defined Magento template
		if ($this->_page->hasData() && $this->_page->getMagento_template() != '')
		{
			$this->ionizeRouteAction();
		}
		else
		{
			$pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);
			
			if ( ! Mage::helper('cms/page')->renderPage($this, $pageId))
			{
				$this->_forward('defaultIndex');
			}
		}

	}
	
	
	// -------------------------------------------------------------------------------------


	/**
	 * Checks the route by comparing the URL store code and the current page store code
	 * Redirects to the correct page if they are different.
	 *
	 *
	 */
	private function _checkPageRoute()
	{
		// Store Code form URL request
		$storeCode = $this->getRequest()->getStoreCodeFromPath();
		
		if ($storeCode != $this->_page->getLang())
		{
			// Get the correct asked page
			$page = Mage::getModel('ionize/page')->getPageById($this->_page->getId_page());

			$base_url = Mage::helper('ionize/core')->getBaseUrl();

			$this->getResponse()
				->setRedirect($base_url . $storeCode.'/'.$page->getUrl(), 301);
		}
	}

	
	/**
	 * Sets the Magento root template (defined from Ionize) to use
	 * and renders the layout
	 *
	 */
	public function ionizeRouteAction()
	{
		$this->loadLayout();

		// Define the template the root block must use.
		$this->getLayout()->getBlock('root')->setTemplate('ionize/page/' . $this->_page->getMagentoTemplate());
		
		$this->renderLayout();
	}

	
	
}
