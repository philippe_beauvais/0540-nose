<?php
/**
 * Page Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Ionize_Helper_Page extends Mage_Core_Helper_Abstract
{
	
	public function setRegisteredPage($page)
	{
		if ( ! $this->getRegisteredPage())
			Mage::register('page', $page);
	}

	
	/**
	 * Return the current registered Page
	 *
	 */
	public function getRegisteredPage()
	{
		return Mage::registry('page');
	}


	/**
	 * Returns the current page parent's page
	 *
	 * @param	boolean		Including pages without URL
	 * @param	boolean		Returns only the IF of the parent page
	 *
	 * @return 	mixed		Page / Int
	 *
	 */
	public function getParentPage($include_without_url = FALSE, $only_id = FALSE)
	{
		$_page = $this->getRegisteredPage();

		if ($_page)
		{
			$pageId = $_page->getIdParent();
			
			if ($pageId != 0)
			{
				$parent_page_id = FALSE;
				$store_code = Mage::helper('ionize/core')->getCurrentStoreCode();
	
				// URL element, to get the asked page ID from path / full_path
				$url = Mage::getModel('ionize/url')->getElement(array('type' => 'page', 'id_entity' => $_page->getIdPage(), 'lang' => $store_code, 'active' => 1));
				
				$segments = array();
				if ($include_without_url == FALSE)
					$segments = explode('/', $url->getPathIds());
				else
					$segments = explode('/', $url->getFullPathIds());
				
				array_pop($segments);
				$parent_page_id = array_pop($segments);
				
				if ( ! is_null($parent_page_id))
				{
					if ($only_id)
						return $parent_page_id;
					else
						return Mage::getModel('ionize/page')->getPageById($parent_page_id);
				}
			}
		}
		
		return FALSE;
	}

	
	/**
	 * Return the parent's page ID
	 *
	 */
	public function getParentPageId($without_url = FALSE)
	{
		return $this->getParentPage($without_url, TRUE);
	}
	
	
	/**
	 * Returns the current page's title
	 *
	 */
	public function getPageTitle()
	{
		$page = $this->getRegisteredPage();

		if ($page)
			return $page->getTitle();
	}
	
	
	/**
	 * Returns the current page
	 *
	 */
	public function getPage()
	{
		return $this->getRegisteredPage();
	}
	
	
	/**
	 * Return the product Ionize parent page (Category)
	 *
	 * @notice : 	Only works on product page, if one Ionize page exists with the same category URL segment
	 * 				Example : 
	 *				Ionize URL : 			http://mydomain/brands/the-brand
	 *				Magento product URL : 	http://mydomain/brands/the-brand/the-product
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getCategoryPage()
	{
		$category = Mage::registry('current_category');
		
		$current_url = Mage::helper('core/url')->getCurrentUrl();
		$segments = explode('/', $current_url);
		
		// We expect that the product is the last part of the URL : put it out
		array_pop($segments);
		
		// Segments will contains only the full store path to the category
		$domain = $_SERVER['HTTP_HOST'];
		$start_key = array_search($domain, $segments);
		$segments = array_slice($segments, $start_key+1);
		
		// Lang : First array index
		$store_code = array_shift($segments);
		$page_potential_path = implode('/', $segments);
		
		// Now starts the fun : The page potential URI should exists in the URL table of Ionize
		$page = Mage::getModel('ionize/page')->getPageByUrl($page_potential_path);
		
		if ($page)
		{
			return $page;
		}
		return FALSE;
	}


	public function getAlternatePagesUrls($page)
	{
		$alternates = array();

		// ionize page
		if ($page)
			$alternates = Mage::getModel('ionize/page')->get_alternate_pages($page);
		// Magento page
		else
		{
			// Product page
			$_product = Mage::registry('current_product');

			if ($_product && $_product->hasData())
			{
				// Get the alternates
				$_category = Mage::helper('nose/product')->getProductFirstCategory($_product);

				if ($_category)
				{
					$urls = Mage::getModel('ionize/url')->getAllCategoryUrlsFromUrl($_category->getUrlPath());

					$alternates =  Mage::getModel('ionize/page')->build_alternates_urls($urls);

					foreach($alternates as &$alternate)
					{
						$alternate['url'] .= '/' . $_product->getUrlKey();
					}
				}
			}
		}

		return $alternates;
	}

	
	/**
	 * Retrieves one Ionize Page
	 *
	 * @param		Controller
	 * @return		Partikule_Ionize_Model_Page		Page object
	 *							
	 *
	 */
	public function getIonizePage($controller)
	{
		// Element URI, from URL
		$uri = Mage::helper('ionize/navigation')->getElementUri($controller);

		$page = new Varien_Object();

		// Home URI : Empty String
		if ($uri == '')
			$page = Mage::getModel('ionize/page')->getHomePage();

		// Other Page
		else
		{
			// $store_code = $controller->getRequest()->getStoreCodeFromPath();
			$store_code = Mage::helper('ionize/core')->getCurrentStoreCode();

			// Element that match the path, the lang & which is active
			$element = Mage::getModel('ionize/url')->getElement(array('path' => $uri, 'lang' => $store_code, 'active' => 1));

			// Nothing found : try Element that match the path & the lang
			if ( ! $element->hasData())
				$element = Mage::getModel('ionize/url')->getElement(array('path' => $uri, 'lang' => $store_code));
			
			// Nothing found : try element which match only the path
			if ( ! $element->hasData())
				$element = Mage::getModel('ionize/url')->getElement(array('path' => $uri));

			
			if ($element->hasData())
			{
				// Element not active : Redirect 301 to the good one !
				if ($element->getActive() == 0 OR $store_code != $element->getLang())
				{
					/*
					Mage::log('getPageUri : ' . $uri, null, 'partikule.log');
					Mage::log('$store_code : ' . $store_code, null, 'partikule.log');
					Mage::log('$element->getLang() : ' . $element->getLang(), null, 'partikule.log');
					*/
					$conditions = array(
						'active' => '1',
						'type' => $element->getType(),
						'id_entity' => $element->getIdEntity(),
						'lang' => $store_code
					);
					
					$element = Mage::getModel('ionize/url')->getElement($conditions);
					
					if ($element->hasData())
					{
						$controller->getResponse()
							->setRedirect(Mage::helper('ionize/core')->getBaseUrl(TRUE) . $element->getPath(), 301);
					}
				}

				// Get the page regarding the link between the article and its parent page
				if ($element->getType() == 'article')
				{
					$full_path_ids = explode('/', $element->getFullPathIds());

					// Remove the article ID first, the get the page ID
					array_pop($full_path_ids);
					$id_page = array_pop($full_path_ids);

					// Mage::log($element->getFullPathIds(), null, 'partikule.log');

					$page = Mage::getModel('ionize/page')->getPageById($id_page);
				}
				 
				if ($element->getType() == 'page')
				{
					$page = Mage::getModel('ionize/page')->getPageById($element->getId_entity());	
				}
			}
		}

		return $page;
	}
	
}