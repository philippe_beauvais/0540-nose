<?php
/**
 * Article Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Ionize_Helper_Article extends Mage_Core_Helper_Abstract
{

	public function setRegisteredArticle($article)
	{
		if ($this->getRegisteredArticle())
			Mage::unregister('article');

		Mage::register('article', $article);
	}

	/**
	 * Return the current registered Article
	 *
	 */
	public function getRegisteredArticle()
	{
		return Mage::registry('article');
	}
	
	/**
	 * Returns product collection from one article's ID
	 *
	 */
	public function getProductCollectionFromId($id)
	{
		return Mage::getModel('ionize/product')->getProductCollectionFrom('article', $id);
	}
	
}