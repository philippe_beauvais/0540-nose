<?php
/**
 * Navigation Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Ionize_Helper_Navigation extends Mage_Core_Helper_Abstract
{
    /**
     * Store pages cache
     *
     * @var array
     */
    protected $_storePages = array();

    /**
     * Store Pages linked to category
     *
     * @var array
     */
	protected $_pageCategory = NULL;
	
	


    /**
     * Retrieve visible pages
     * Checked in Ionize as "Visible in Menu". Attribute "appears"
     *
     * @param   int 	$id_parent		The starting parent id.
     * @param   null 	$id_parent
     * @return  mixed	Varien_Data_Tree_Node_Collection|Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection|array
     *
     * @todo	Add id_parent filter
     *			public function getPages($id_menu = 1, $id_parent = 0)
     *
     */
	public function getVisiblePages($id_menu = 1, $id_parent = NULL)
    {
    	// Local cache
        $cacheKey   = 'pages_visible_menu_'.$id_menu;
        if ( ! is_null($id_parent)) $cacheKey .= '_'.$id_parent;

        if (isset($this->_storePages[$cacheKey]))
            return $this->_storePages[$cacheKey];

        /* @var $page Partikule_Ionize_Model_Page */
        $pages = Mage::getModel('ionize/page')->getVisiblePages($id_menu, $id_parent);

        $this->_storePages[$cacheKey] = $pages;
        
        return $pages;
    }
	
    public function getPages($id_menu = 1, $id_parent = NULL)
    {
    	// Local cache
        $cacheKey   = 'pages_menu_'.$id_menu;
		if ( ! is_null($id_parent))	$cacheKey .= '_'.$id_parent;
		
        if (isset($this->_storePages[$cacheKey]))
            return $this->_storePages[$cacheKey];

        /* @var $page Partikule_Ionize_Model_Page */
        $pages = Mage::getModel('ionize/page')->getPages($id_menu, $id_parent);

        $this->_storePages[$cacheKey] = $pages;
        
        return $pages;
	}
	

	public function getCurrentPage($pages, $page_uri)
	{
		return $this->_getCurrentPage($pages, $page_uri);
	}

	
	/**
	 * Returns the pages linked to one given category
	 *
	 */
	public function getPageLinkedToCategory($category_id)
	{
		if ( ! is_null($this->_pageCategory))
			return $this->_pageCategory;

		$this->_pageCategory = Mage::getModel('ionize/page')->getPageLinkedToCategory($category_id);
		
		return $this->_pageCategory;
	}
	
	
	/**
	 * Returns the current element URI
	 *
	 * @param	Mage_Core_Controller_Front_Action		Deprecated. optional. Segment index to return
	 * @return 	String
	 *
	 */
	public function getElementUri(Mage_Core_Controller_Front_Action $action)
	{
		$uri = $action->getRequest()->getOriginalPathInfo();
		
		$uri = trim($uri,'/');
		
		return $uri;
	}
	
	

	
	/**
	 * Returns the current page URI segment
	 *
	 * @param	Mage_Core_Controller_Front_Action		Deprecated. optional. Segment index to return
	 * @return 	String
	 *
	 */
	public function getPageUriSegment(Mage_Core_Controller_Front_Action $action, $segment = '')
	{
		$uri = $action->getRequest()->getOriginalPathInfo();

		$uri = trim($uri,'/');
		
		$segments = explode('/', $uri);
		// Remove the first empty segment
		// array_shift($segments);
		
		$segment = array_pop($segments);
		
		return $segment;
	}
	
	
	/**
	 * Returns the current page URI segments
	 *
	 * @param	Mage_Core_Controller_Front_Action
	 * @return 	String
	 *
	 */
	public function getPageUriSegments(Mage_Core_Controller_Front_Action $action)
	{
		$uri = $action->getRequest()->getOriginalPathInfo();
		
		$uri = trim($uri,'/');
		
		$segments = explode('/', $uri);
		// Remove the first empty segment
		// array_shift($segments);
		
		return $segments;
	}

	
	/**
	 * Prepares the pages URL
	 * Change each URL for the redirected page if mandatory
	 *
	 * @param	Array	Array of pages
	 * 
	 * @return	Array	Array of pages
	 *
	 */
	public function prepareUrls($pages)
	{
		// Pass 1 : get the Url
		foreach($pages as &$page)
		{
			// Link to a page : Get the target page URL
			$page['path'] = $this->_getPageUrl($page, $pages);
		}
		
		// Pass 2 : Add the Base Url prefix
		foreach($pages as &$page)
		{
			if (strpos($page['path'], 'http://') === FALSE)
			{
				$page['path'] = Mage::getBaseUrl() . $page['path'];
			}

if ($page['id_page'] == 38)
	$page['path'] = '';

		}


		return $pages;
	}
	
	
	
	/**
	 * Sets one menu item active
	 * Used by modules controllers 
	 *
	public function setActiveMenu($controller, $uri)
	{
		// Set the current Ionize's page URL active
		$controller->getLayout()->getBlock('catalog.topnav')->setActive($uri);
	}
	 */


	/**
	 * Find the URL for one page
	 *
	 * @param 	array 		Page array.
	 * @param 	array 		Pages array.
	 *
	 * @return 	void
	 *
	 */
	protected function _getPageUrl($page, $pages)
	{
		if ($page['link_type'] == 'page')
		{
			foreach($pages as $p)
			{
				if ($p['id_page'] == $page['link_id'])
				{
					return $this->_getPageUrl($p, $pages);
				}
			}
		}

		return $page['path'];
	}




	/** 
	 * Get the current page, based on its URI segment
	 * 
	 * @param	Array			Array of pages
	 * @param	string			Page URI segment
	 *
	 * @return	array			Array of the page data. Can be empty.
	 *
	 */
	protected function _getCurrentPage($pages, $page_uri)
	{
		// Ignore the page named 'page' and get the home page
		if ($page_uri == '')
		{
			return $this->_getHomePage($pages);
		}
		else
		{
			return $this->_getPage($pages, $page_uri);
		}
	}
	
	
	/**
	 * Get the website Home page
	 * The Home page is the first page from the main menu (ID : 1)
	 * 
	 * @param	FTL_Context		FTL_ArrayContext array object
	 *
	 * @return	Array			Home page data array or an empty array if no home page is found
	 *
	 */
	protected function _getHomePage($pages)
	{
		if( ! empty($pages))
		{
			foreach($pages as $page)
			{
				if ($page['home'] == 1)
				{
					return $page;
				}
			}
			
			// No Home page found : Return the first page of the menu 1
			foreach($pages as $p)
			{
				if ($p['id_menu'] == 1)
					return $p;
			}
		}

		return array();
	}

	
	/**
	 * Get one page regarding to its name
	 * 
	 * @param	array	pages array
	 * @param	string	Page name
	 * @return	array	Page data array
	 */
	protected function _getPage($pages, $page_name)
	{
		foreach($pages as $p)
		{
			if ($p['url'] == $page_name)
				return $p;
		}
	
		return array();	
	}

	
}