<?php
/**
 * Breadcrumbs Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Ionize_Helper_Breadcrumbs extends Mage_Core_Helper_Abstract
{

    protected $_crumbs = null;	


	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getBreadcrumbs()
	{
		// Home Page
		$this->addCrumb('home', 
			array(
				'label'=>Mage::helper('catalogsearch')->__('Home'),
				'title'=>Mage::helper('catalogsearch')->__('Go to Home Page'),
				'link'=>Mage::getBaseUrl()
			)
		);

    	// Only get pages if the visited page comes from Ionize
    	if ($current_page = Mage::helper('ionize/page')->getRegisteredPage())
		{
	    	// Get all pages, visible in menu or not
			$pages  = Mage::helper('ionize/navigation')->getPages();

			// Other Ionize's pages
			$this->_getPagesBreadcrumbs($current_page);
		}

		// Displays the category breadcrumb : On the Product page for example		
		$category = Mage::registry('current_category');

		if ($category)
		{
			$this->_getCategoryBreadcrumbs($category);
		}
		
		return $this->_crumbs;
	}
	


	/**
	 * Adds one breadcrumb array to the breadcrumbs array
	 * Same method as Magento one
	 * 
	 */
    public function addCrumb($crumbName, $crumbInfo, $after = false)
    {
        $this->_prepareArray($crumbInfo, array('uri', 'label', 'title', 'link', 'first', 'last', 'readonly'));

        if ((!isset($this->_crumbs[$crumbName])) || (!$this->_crumbs[$crumbName]['readonly'])) {
           $this->_crumbs[$crumbName] = $crumbInfo;
        }
        return $this;
    }


	/**
	 *
	 * Get the Ionize page breadcrumbs regarding the current stored category
	 * If one Ionize page is linked to this category, get the breadcrumb to this page
	 *
	 */
	private function _getCategoryBreadcrumbs($category)
	{
		// From Category ID, get the page in the table 
		$pages = Mage::helper('ionize/navigation')->getPageLinkedToCategory($category->getId());
	
		// Get only the firt found page.
		// Ionize sets only one link page <> magento category to master, so normally, we should have only one page
		$page = array_shift($pages);

		// Add the Ionize pages to the breadcrumb
		$this->_getPagesBreadcrumbs($page);
		
		// Get the current catalog breadcrumb array. See : Mage_Catalog_Block_Breadcrumbs
		$path  = Mage::helper('catalog')->getBreadcrumbPath();

		// Add the product name to the path
		if ( ! empty($path['product']))
			$this->addCrumb($path['product']['label'], $path['product']);		
	}
	
	


	/**
	 * Adds all the pages breadcrumbs to the current breadcrumbs array
	 * 
	 * @param	Array / Page
	 *
	 */
	private function _getPagesBreadcrumbs($current_page = NULL)
	{
		$pages = Mage::helper('ionize/navigation')->getPages()->getData();
		
		$pages = Mage::helper('ionize/navigation')->prepareUrls($pages);

		if ($current_page != NULL) 
		{
			$page = $current_page;
			
			if (is_object($current_page))
				$page = $current_page->getData();

			$breadcrumbs = self::_getBreadcrumbArray($page, $pages);

			foreach($breadcrumbs as $breadcrumb)
			{
				// Only display the pages which are "displayed in menu"
				if (isset($breadcrumb['has_url']) && $breadcrumb['has_url'] == '1')
				{
					$this->addCrumb($breadcrumb['name'], 
						array(
							'uri' => $breadcrumb['url'],
							'label' => $breadcrumb['title'],
							'title' => $breadcrumb['title'],
							'link' => (isset($breadcrumb['path'])) ? $breadcrumb['path'] : ''
						)
					);
				}
			}
		}
	}


	/**
	 * Returns the breadcrumb data array
	 *
	 * @param	Array	The starting page
	 * @param	Array	All the pages
	 * @param	Array	Returned data array, self feeded
	 *
	 * @return	Array	Array of pages name (in the current language)
	 *
	 */
	private function _getBreadcrumbArray($page, $pages, $data = array())
	{
		$parent = NULL;
		
		if (isset($page['id_parent']) )
		{
			// Find the parent
			for($i=0; $i < count($pages) ; $i++)
			{
				if ($pages[$i]['id_page'] == $page['id_parent'])
				{
					$parent = $pages[$i];
					$data = self::_getBreadcrumbArray($parent, $pages, $data);
					break;
				}
			}
			
			$data[] = $page;
		}
		return $data;
	}
	
    /**
     * Set required array elements
     *
     * @param   array $arr
     * @param   array $elements
     * @return  array
     */
    protected function _prepareArray(&$arr, array $elements=array())
    {
        foreach ($elements as $element) {
            if (!isset($arr[$element])) {
                $arr[$element] = null;
            }
        }
        return $arr;
    }

	

	
}		
