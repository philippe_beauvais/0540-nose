<?php
/**
 * Core Helper
 *
 * Centralize most of the Ionize / Magento core data retrieve.
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Ionize_Helper_Date extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Returns a wel formatted date from one Ionize Date
	 *
	 * @param	String		MySQL Ionize date
	 * @param	String		Mage_Core_Model_Locale format. see Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
	 *						'full', 'long', 'medium', 'short'
	 *						OR
	 *						PHP data format. see http://php.net/manual/en/function.date.php
	 *
	 */
	public function getFormatedDate($date, $format = 'short')
	{
		$phpdate = strtotime( $date );
		
		if ( in_array($format, array('full', 'long', 'medium', 'short')))
			$format = str_replace('%', '', Mage::app()->getLocale()->getDateStrFormat($format));

		return date($format, $phpdate);
	}
	
}