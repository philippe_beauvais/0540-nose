<?php
class Partikule_Nose_Model_Api extends Mage_Api_Model_Resource_Abstract
{
	
	public function test($productSku)
	{
		// Mage::log($productSku, null, 'partikule.log');
		
		// Must return something, to avoid NIL error on the XMLRPC lib side.
		return FALSE;
	}
	
	
	/**
	 * Creates all given attributes options
	 *
	 * @param	string		Attribute code
	 * @param	array		Array of external ID => Attribute option code
	 * @param	array		Array of store code => external ID => Attribute option code
	 *
	 */
	public function createAttributeOptions($attributeCode, $options = array(), $store_options = array())
	{
		// Get the attribute
        $attribute = Mage::getModel('catalog/product')
            ->setStoreId(0)
            ->getResource()
            ->getAttribute($attributeCode);

		// Other possibility to get the attribute : 
		// $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attributeCode);

		// Fault if not exists
        if ( ! $attribute->getId()) {
            $this->_fault('attribute_does_not_exist');
        }
		
		// Returned array of ID => Magento ID
		$returned_ids = array();
		
		// Stores
		$stores = Mage::app()->getStores();

		// Start Order
		$order = 0;
		
		// Try to create each attribute option
		foreach($options as $option_id => $new_option)
		{
			$exists = FALSE;
			$db_option = NULL;

			foreach ($attribute->getSource(true)->getAllOptions() as $existing_option)
			{
				if ($new_option == $existing_option['label'])
				{
					$exists = TRUE;
					$db_option = $existing_option;
				}
			}
			
			$option = array();
			$option['attribute_id'] = $attribute->getId(); 
			$id_option = 'newOption';
			$order += 10;
			
			if ($exists)
				$id_option = $db_option['value'];

			$option['order'][$id_option] = $order;			
			$option['value'][$id_option][0] = $new_option;

			foreach($stores as $store)
			{
				if ( ! empty($store_options[$store['code']][$option_id]))
					$option['value'][$id_option][$store['store_id']] = $store_options[$store['code']][$option_id];
			}

			$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
			$installer->startSetup();
			$installer->addAttributeOption($option);
			$installer->endSetup();
			
			// Old method : More quick, but generates errors... see system.log
			// Mage::getModel('eav/entity_setup')->addAttributeOption($option);
			// $attribute->save();

			$collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
				->addFieldToFilter('attribute_id', $attribute->getId())
				->join('attribute_option_value', 'main_table.option_id = attribute_option_value.option_id')
				->addFieldToFilter('value', $new_option);
			
			// Mage::log("{$collection->getSelect()}", null, 'partikule.log');
			
			$data =  $collection->getData();
			$firstOption = array_shift($data);
			
			if ( $firstOption )
				$returned_ids[$option_id] = $firstOption['option_id'];
		}

		return $returned_ids;
	}
	
	
	/**
	 * Not implemented.
	 * The idea is to create one option and return its code, to update the Ionize attribute table with the 
	 * Mage ID of each option
	 *
	 */
	public function createAttributeOption($attributeCode, $option = array(), $store_option = array())
	{
		// Get the attribute
        $attribute = Mage::getModel('catalog/product')
            ->setStoreId(0)
            ->getResource()
            ->getAttribute($attributeCode);

		// Other possibility to get the attribute : 
		// $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attributeCode);

		// Fault if not exists
        if ( ! $attribute->getId()) {
            $this->_fault('attribute_does_not_exist');
        }

		// Stores
		$stores = Mage::app()->getStores();


        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->addFieldToFilter('attribute_id',$attributeId)
        	->join('attribute_option_value','main_table.option_id=attribute_option_value.option_id')
            ->addFieldToFilter('value',$option_label);
		$new_option = $collection->getFirstItem();
		
		return $new_option->getId();
	}

}
?>
