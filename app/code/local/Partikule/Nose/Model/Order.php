<?php
/*
 *
 *	trace("{$select}".PHP_EOL);	
 *
 */


class Partikule_Nose_Model_Order extends Partikule_Nose_Model_Abstract
{
    /**
     * Local cache
     *
     * @var array
     */
    protected $_orders = array();
	

	public function __construct()
	{
		parent::__construct();
		
		$this->setConnection('mynose_read', 'mynose_write');

		$this->invoice_table = Mage::getSingleton('core/resource')->getTableName('nose/invoice');
		$this->invoice_line_table =  Mage::getSingleton('core/resource')->getTableName('nose/invoice_line');
	}
	
	
	/**
	 * Returns 	The customer's "eal life" shop orders
	 *
	 * @return	array	Array of orders
	 *
	 */
	public function getOrdersFromCustomer()
	{
		// Get the user
		$user = Mage::getSingleton('customer/session')->getCustomer();
		
		$email = $user->getEmail();
		
		// Get Orders from eNose
		$select = $this->getReadConnection()->select()
			->from( array('i' => $this->invoice_table))

			// Previously based on email, but since the magento_id is copied to the pos_invoice table...
			//	->where('i.customer_email = ?', $user->getEmail())
			->where('i.magento_id = ?', $user->getId())
			->order(array('creation_date DESC'));
		
		// trace("{$select}".PHP_EOL);
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');

		$query = $select->query();

		$orders = $query->fetchAll();

		return $orders;
	}
	
	public function getOrder($orderId)
	{
		$select = $this->getReadConnection()->select()
			->from( array('i' => $this->invoice_table))
			->where('i.pos_invoice_id = ?', $orderId)
		;
		
		// trace("{$select}".PHP_EOL);
		// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');

		$query = $select->query();
		$order = $query->fetch();
		
		if ( ! empty($order))
		{
			$select = $this->getReadConnection()->select()
				->from( array('i' => $this->invoice_line_table))
				->where('i.pos_invoice_id = ?', $orderId)
			;
			$query = $select->query();
			$lines = $query->fetchAll();
			
			$order['lines'] = $lines;
		}

		return $order;
		
	}
	
	
	
}