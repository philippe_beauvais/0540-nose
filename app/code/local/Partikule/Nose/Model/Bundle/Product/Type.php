<?php
class Partikule_Nose_Model_Bundle_Product_Type extends Mage_Bundle_Model_Product_Type
{

	/**
	 * Prepare additional options/information for order item which will be
	 * created from this product.
	 *
	 * Partikule : The way the name of the product is stored doesn't give category info
	 *
	 * @param Mage_Catalog_Model_Product $product
	 * @return array
	 */

	public function getOrderOptions($product = null)
	{
		$optionArr = parent::getOrderOptions($product);

		$bundleOptions = array();

		$product = $this->getProduct($product);

		if ($product->hasCustomOptions()) {
			$customOption = $product->getCustomOption('bundle_option_ids');
			$optionIds = unserialize($customOption->getValue());
			$options = $this->getOptionsByIds($optionIds, $product);
			$customOption = $product->getCustomOption('bundle_selection_ids');
			$selectionIds = unserialize($customOption->getValue());
			$selections = $this->getSelectionsByIds($selectionIds, $product);

			foreach ($selections->getItems() as $selection) {
				if ($selection->isSalable()) {
					$selectionQty = $product->getCustomOption('selection_qty_' . $selection->getSelectionId());
					if ($selectionQty) {
						$price = $product->getPriceModel()->getSelectionPrice(
							$product,
							$selection,
							$selectionQty->getValue()
						);

						$option = $options->getItemById($selection->getOptionId());
						if (!isset($bundleOptions[$option->getId()]))
						{
							$bundleOptions[$option->getId()] = array(
								'option_id' => $option->getId(),
								'label' => $option->getTitle(),
								'value' => array()
							);
						}

						// Partikule : Add the category if found
						$product = Mage::getModel('catalog/product')->load($selection->getProductId());
						$product_category = Mage::helper('nose/Product')->getProductFirstCategory($product);
						if ($product_category)
							$selection->setName($product_category->getName() . ', ' . $selection->getName());

						$bundleOptions[$option->getId()]['value'][] = array(
							'title' => $selection->getName(),
							'qty'   => $selectionQty->getValue(),
							'price' => Mage::app()->getStore()->convertPrice($price)
						);

					}
				}
			}
		}

		$optionArr['bundle_options'] = $bundleOptions;

		/**
		 * Product Prices calculations save
		 */
		if ($product->getPriceType()) {
			$optionArr['product_calculations'] = self::CALCULATE_PARENT;
		} else {
			$optionArr['product_calculations'] = self::CALCULATE_CHILD;
		}

		$optionArr['shipment_type'] = $product->getShipmentType();

		return $optionArr;
	}


}



