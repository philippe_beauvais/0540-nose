<?php

class Partikule_Nose_Model_Observer extends Mage_Core_Model_Abstract
{

    /**
     * On customer register, update the eNose user "term_of_use" field.
     * @date    2017/11/19
     *
     * @param $observer
     */
    public function onCustomerRegisterSuccess($observer)
    {
        $accountController = $observer->getAccountController();
        $customer = $observer->getCustomer();

        $post = $accountController->getRequest()->getPost();

        if ( ! empty($post['term_of_use']) && $post['term_of_use'] == 1)
        {
            $userModel = Mage::getModel('mynose/user');
            $user = $userModel->getNoseUserFromMagentoId($customer->getId());

            if ( ! empty($user))
            {
                $userModel->updateNoseUserData($user, array('term_of_use' => 1));
            }
        }
    }


	/**
	 * Updates the Magento local 'perfume_detail' table
	 * Called by a cron defined in config.xml
	 *
	 * Writes the file /var/log/cronPerfumeTableUpdate.log
	 *
	 */
	public function cronPerfumeTableUpdate()
	{
		$time_start = microtime(true);

		// Get Perfume Detail from eNose view : perfume_detail
		$resource = Mage::getSingleton('core/resource');
		$read = 	$resource->getConnection('nose_read');
		$write = 	$resource->getConnection('core/write');
		
		// The local perfume_table has the same name than the view in eNose : get it one time only
		$perfumeTable = $resource->getTableName('nose/perfume_detail');
		
		// Get Perfumes from and prepare perfume data
		$select = $read->select()->from( array('p' => $perfumeTable));
		$query = $select->query();
		$perfumes = $query->fetchAll();
		$perfs = array();
		
		foreach($perfumes as $p)
		{
			$str = implode("#§#", array_values($p));
			$str = str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $str);
			$str = str_replace("#§#", "','", $str);
			$perfs[] = "('" . $str . "')";
		}

		$perfs = implode(',', $perfs);

		
		// SQL Create
		$sqlCreate = 'CREATE TABLE perfume_detail (
						lang varchar(3),
           				perfume_id INT(11),
           				nose_sku varchar(14),
           				nose_brand varchar(800),
           				nose_sex smallint(6),
           				millesime int(11),
           				inspiration varchar(1000),
           				description longtext,
           				creation varchar(3000),
           				style varchar(3000),
           				family varchar(800),
           				subfamily varchar(800),
           				specifer varchar(800),
           				notes_head varchar(500),
           				notes_heart varchar(500),
           				notes_base varchar(500),
           				notes_main varchar(500)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci';

		// Drop table
		$sqlTruncate = 'DROP TABLE IF EXISTS perfume_detail';
		$result = $write->query($sqlTruncate);
		$result = $write->query($sqlCreate);

		
		// Prepare data
		$sql = "INSERT INTO perfume_detail (
					lang,
					perfume_id,
					nose_sku,
					nose_brand,
					nose_sex,
					millesime,
					inspiration,
					description,
					creation,
					style,
					family,
					subfamily,
					specifer,
					notes_head,
					notes_heart,
					notes_base,
					notes_main
				) VALUES ";

		
		$sql .= $perfs;
		$stmt = $write->prepare($sql);
		$stmt->execute();
	
		$time_end = microtime(true);
		$time = $time_end - $time_start;
	
		$msg = date('d m Y H:i') . ' - Table perfume_detail updated : ' . count($perfumes) . ' inserted. ' . $time . ' seconds.';

		// Mage::log($msg, null, 'cronPerfumeTableUpdate.log');
	}
	
}