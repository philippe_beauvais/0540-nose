<?php
/**
 * Partikule
 *
 *
 * @category    Nose
 * @package     Nose_Product
 * @copyright   Copyright (c) 2012 Partikule. (http://www.partikule.net)
 *
 */

/**
 * Catalog product api
 *
 * @category   Nose
 * @package    Nose_Product
 * @author     Partikule
 *
 */
class Partikule_Nose_Model_Product_Api extends Mage_Catalog_Model_Product_Api
{
	/*
	 * Non store based attributes to reset by store
	 */
	private $_attrToReset = array('news_from_date', 'news_to_date');


    /**
     * Retrieve list of products with basic info (id, sku, type, set, name)
     *
     * Extend the standard Mage_Catalog_Model_Product_Api->item()
     * to add more flexibility in filtering
     *
     * @param array $filters
     * @param string|int $store
     * @return array
     *
     */
    public function items($filters = null, $store = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($this->_getStoreId($store))
            ->addAttributeToSelect('name');

		/*
			// Debug
		
			Mage::log(print_r($filters ,true), null, 'partikule.log');
			$filters = array(
					array('attribute'=>'sku','like'=>'P-114-%')
			);
		*/

        if (is_array($filters))
        {
			$collection->addFieldToFilter($filters);
		}

        $result = array();

		// Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');

        foreach ($collection as $product)
        {
            $result[] = array(
            	// Basic product data
                'product_id' => $product->getId(),
                'sku'        => $product->getSku(),
                'name'       => $product->getName(),
                'set'        => $product->getAttributeSetId(),
                'type'       => $product->getTypeId(),
                'category_ids'       => $product->getCategoryIds()
            );
        }

        return $result;
    }


	public function getAllProducts($filters = null)
	{
		$result = array();

		$resource = Mage::getSingleton('core/resource');

		$readConnection = $resource->getConnection('core_read');

		$query = "
		SELECT ccp.product_id,
                b.brand_name,
                p.mag_name,
                cpe.sku AS mag_sku,
                p.mag_price,
                p.mag_image,
                p.mag_small_image,
                p.mag_thumbnail,
                p.mag_news_from_date,
                p.mag_status,
                p.mag_url_key,
                p.mag_url_path,
                cpe.created_at AS mag_created_at,
                cpe.updated_at AS mag_updated_at,
                p.enose_master_id,
                b.brand_url_key,
                b.brand_url_path,
                csi.qty,
                css.stock_status
			FROM
			 catalog_product_entity cpe INNER JOIN
			(
			SELECT entity_id AS product_id,
			MAX(CASE WHEN attribute_id=60 AND store_id=0 THEN value ELSE NULL END) AS mag_name,
			MAX(CASE WHEN attribute_id=64 AND store_id=0 THEN value ELSE NULL END) AS mag_price,
			MAX(CASE WHEN attribute_id=168 AND store_id=0 THEN value ELSE NULL END) AS enose_master_id,
			MAX(CASE WHEN attribute_id=74 AND store_id=0 THEN value ELSE NULL END) AS mag_image,
			MAX(CASE WHEN attribute_id=75 AND store_id=0 THEN value ELSE NULL END) AS mag_small_image,
			MAX(CASE WHEN attribute_id=76 AND store_id=1 THEN value ELSE NULL END) AS mag_thumbnail,
			MAX(CASE WHEN attribute_id=81 AND store_id=0 THEN value ELSE NULL END) AS mag_news_from_date,
			MAX(CASE WHEN attribute_id=84 AND store_id=0 THEN value ELSE NULL END) AS mag_status,
			MAX(CASE WHEN attribute_id=86 AND store_id=0 THEN value ELSE NULL END) AS mag_url_key,
			MAX(CASE WHEN attribute_id=87 AND store_id=0 THEN value ELSE NULL END) AS mag_url_path,
			MAX(CASE WHEN attribute_id=103 AND store_id=0 THEN value ELSE NULL END) AS mag_created_at,
			MAX(CASE WHEN attribute_id=104 AND store_id=0 THEN value ELSE NULL END) AS mag_updated_at
			FROM (SELECT entity_id, attribute_id, store_id, value FROM
			catalog_product_entity_varchar
			UNION
			SELECT entity_id, attribute_id, store_id, value FROM catalog_product_entity_datetime
			WHERE attribute_id IN (60, 63, 64, 168, 74, 75, 76, 81, 84, 86, 87)
			UNION
			SELECT entity_id, attribute_id, store_id, value FROM catalog_product_entity_decimal
			WHERE attribute_id IN (60, 63, 64, 168, 74, 75, 76, 81, 84, 86, 87)
			UNION
			SELECT entity_id, attribute_id, store_id, value FROM catalog_product_entity_int
			WHERE attribute_id IN (60, 63, 64, 168, 74, 75, 76, 81, 84, 86, 87)
			UNION
			SELECT entity_id, attribute_id, store_id, value FROM catalog_product_entity_text
			WHERE attribute_id IN (60, 63, 64, 168, 74, 75, 76, 81, 84, 86, 87)
			UNION
			SELECT entity_id, attribute_id, store_id, value FROM catalog_product_entity_varchar
			WHERE attribute_id IN (60, 63, 64, 168, 74, 75, 76, 81, 84, 86, 87)) p
			GROUP BY entity_id) p
			ON p.product_id=cpe.entity_id
			INNER JOIN
			catalog_category_product ccp
			ON ccp.product_id = p.product_id
			INNER JOIN
			(SELECT cce.entity_id,
							MAX(CASE WHEN attribute_id=33 THEN value ELSE NULL END)  AS brand_name,
							MAX(CASE WHEN attribute_id=35 THEN value ELSE NULL END)  AS brand_url_key,
							MAX(CASE WHEN attribute_id=49 THEN value ELSE NULL END)  AS brand_url_path
			FROM catalog_category_entity cce
			INNER JOIN catalog_category_entity_varchar ccev
			ON cce.entity_id=ccev.entity_id
			WHERE cce.parent_id=587
			GROUP BY cce.entity_id) b
			ON ccp.category_id=b.entity_id
			LEFT JOIN cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id AND csi.stock_id=1
			LEFT JOIN cataloginventory_stock_status css ON cpe.entity_id = css.product_id AND css.stock_id=1
		";

		$entities = $readConnection->fetchAll($query);

		// Be able to transmit thousands of lines without memory exhausting
		foreach($entities as $idx => $entity)
		{
			if ($idx === 0)
			{
				$result['keys'] = array_keys($entity);
			}

			$result['entities'] .= implode(";;;", $entity).'|';
		}


		return $result;
	}

	
	
    /**
     * Updates one configurable and all its linked simples
     * Updates only the attributes wich are in the product's attribute set
     *
     * @param int|string			Configurable product ID
     * @param array 				Product data
     * @param string|int			Store ID
     *
     * @return boolean
     *
     */
    public function updateConfigurableAndSimple($productId, $productData, $store = null, $identifierType = null)
    {
		$store_id = ( ! is_null($store)) ? $store : Mage_Core_Model_App::ADMIN_STORE_ID;

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		// Mage::log($productData, null,  'partikule.log');

        $product = $this->_getProduct($productId, $store_id, $identifierType);

        if ( ! $product->getId()) {
            $this->_fault('not_exists');
        }

		// Product pseudo ID
		$product_sku_id = explode('-', $productId);
		if (isset($product_sku_id[1]))
			$product_sku_id = $product_sku_id[1];
		else
			$this->_fault('not_exists');

		// Get products with the same ID in SKU
		$products = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToSelect('*')
			->addAttributeToFilter('sku', array('like' => '%-'.$product_sku_id.'-%'))
		;


		// Add the master to the list of products to update		
		$products->addItem($product);

		foreach($products as $product)
		{
			// Mage::log('store : ' . $store_id, null,  'partikule.log');
			// Mage::log($product->getSku(), null, 'partikule.log');
			// Product load, to get all attributes, like url_key
			$_product = $this->_getProduct($product->getId(), $store, $identifierType);

	        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
	            $_product->setWebsiteIds($productData['website_ids']);
	        }

	        foreach ($_product->getTypeInstance(true)->getEditableAttributes($_product) as $attribute) 
	        {
	            if ($this->_isAllowedAttribute($attribute) && isset($productData[$attribute->getAttributeCode()]))
	            {
	            	$option_value = $productData[$attribute->getAttributeCode()];

					// Find the option ID from its Admin label
	            	if ( $attribute->getBackendType() == 'int' &&  (int)$option_value == 0 )
	            	{
						foreach ($attribute->getSource(true)->getAllOptions() as $existing_option)
						{
							if ($existing_option['label'] == $option_value)
							{
								$option_value = $existing_option['value'];
								break;
							}
						}
	            	}

					if (in_array($attribute->getAttributeCode(), $this->_attrToReset))
					{
						// $this->setUseDefault($_product, $attribute->getAttributeCode());
						$this->setForAllStores($_product, $attribute->getAttributeCode(), $option_value);

						// news_from_date_default
						// Mage::log( $attribute->getAttributeCode() . '::' .$option_value, null, 'partikule.log');
					}

					// Mage::log($store_id . "::" . $attribute->getAttributeCode() . '::' . $option_value, null, 'partikule.log');

					$_product->setData(
	                    $attribute->getAttributeCode(),
	                    $option_value
					);
	            }
	        }
	
	        $this->_prepareDataForSave($_product, $productData);
	
	        try {
	            /**
	             * @todo implement full validation process with errors returning which are ignoring now
	             * @todo see Mage_Catalog_Model_Product::validate()
	             */
	            if (is_array($errors = $_product->validate())) {
	                $strErrors = array();
	                foreach($errors as $code=>$error) {
	                    $strErrors[] = ($error === true)? Mage::helper('catalog')->__('Value for "%s" is invalid.', $code) : Mage::helper('catalog')->__('Value for "%s" is invalid: %s', $code, $error);
	                }
	                $this->_fault('data_invalid', implode("\n", $strErrors));
	            }

	            $_product->save();
	        } catch (Mage_Core_Exception $e) {
	            $this->_fault('data_invalid', $e->getMessage());
	        }
		}
        return true;
    }


	private function setForAllStores($product, $attr, $value)
	{
		  $stores = Mage::app()->getStores();

		  foreach($stores as $store)
		  {
			  $product->setStoreId($store['store_id'])
			  ->setData($attr, $value)
			  ->save();
		  }
	}


	public function feedTheBundle()
	{
		$nb_added_samples = 0;

		$settings_product_id = Mage::getStoreConfig('nose/general/bundle_product_id');

		// Load bundle
		$product = Mage::getModel('catalog/product');
		$product->load($settings_product_id);

		try
		{
			if ( ! ($product->getTypeId() == 'bundle'))
			{
				return FALSE;
			}
			else
			{
				// Because app/code/core/Mage/Bundle/Model/Selection.php->_beforeSave()
				// calls $storeId = Mage::registry('product')->getStoreId();
				Mage::register('product', $product);

				// Get all Samples
				$samples_collection = Mage::getModel('catalog/product')->getCollection()
					->addAttributeToSelect("*")
					->addAttributeToFilter('type_id', 'simple')
					->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'S-%')))
				;

				// $nb_samples = count($samples_collection);

				// Existing Options
				$optionCollection = $product->getTypeInstance(TRUE)->getOptionsCollection($product);

				// Already Linked products
				$selectionCollection = $product->getTypeInstance(TRUE)->getSelectionsCollection(
					$product->getTypeInstance(TRUE)->getOptionsIds($product),
					$product
				);

				// Add "Selections" as container for Collection of linked products
				$optionCollection->appendSelections($selectionCollection);

				$optionRawData = array();
				$selectionRawData = array();

				foreach($optionCollection as $option)
				{
					$selection_product_ids = array();

					$optionRawData[$option->getID()] = array(
						'option_id' => $option->getID(),
						'required' => $option->getData('required'),
						'position' => $option->getData('position'),
						'type' => $option->getData('type'),
						'title' => $option->getData('title')?$option->getData('title'):$option->getData('default_title'),
						'delete' => ''
					);

					foreach ($option->getSelections() as $selection)
					{
						// Array of products already in the option selection list
						$selection_product_ids[] = $selection->getProductId();

						$selectionRawData[$option->getID()][] = array(
							'selection_id' => $selection->getSelectionId(),
							'product_id' => $selection->getProductId(),
							'option_id' => $option->getID(),
							'position' => $selection->getPosition(),
							'is_default' => $selection->getIsDefault(),
							'selection_price_type' => $selection->getSelectionPriceType(),
							'selection_price_value' => $selection->getSelectionPriceValue(),
							'selection_qty' => $selection->getSelectionQty(),
							'selection_can_change_qty' => $selection->getSelectionCanChangeQty(),
							'delete' => ''
						);
					}

					foreach($samples_collection as $sample)
					{
						if ( ! in_array($sample->getId(), $selection_product_ids))
						{
							$nb_added_samples += 1;

							$selectionRawData[$option->getID()][] = array(
								'product_id' => $sample->getId(),
								'option_id' => $option->getId(),
								'position' => 0,
								'is_default' => 0,
								'selection_price_type' => 0,
								'selection_price_value' => 0,
								'selection_qty' => 1,
								'selection_can_change_qty' => 0,
								'delete' => ''
							);
						}
					}
				}

				$product->setCanSaveConfigurableAttributes(FALSE);
				$product->setCanSaveCustomOptions(TRUE);
				$product->setCanSaveBundleSelections(TRUE);
				$product->setAffectBundleProductSelections(TRUE);

				$product->setBundleOptionsData($optionRawData);
				$product->setBundleSelectionsData($selectionRawData);

				// Save
				$product->save();

				return array(
					'nb_added_samples' => $nb_added_samples,
					'memory_used' => memory_get_usage()
				);
			}
		}
		catch (Exception $e)
		{
			return array(
				'error' => TRUE,
				'message' =>  $e->getMessage()
			);
		}
	}






	private function setUseDefault($product, $attr)
	{
		$product->setData($attr, false)->save();

		// Stores
		/*
		  $stores = Mage::app()->getStores();

		  foreach($stores as $store)
		  {
			  $product->setStoreId($store['store_id'])
			  ->setData($attr, false)
			  ->save();
		  }
  */
	}

/*
    public function updateConfigurableAndSimple($productId, $productData, $store = null, $identifierType = null)
    {
		// Mage::log('store : ' . $store, null,  'partikule.log');
		// Mage::log($productData, null,  'partikule.log');

        $product = $this->_getProduct($productId, $store, $identifierType);

        if ( ! $product->getId()) {
            $this->_fault('not_exists');
        }

		// Get childs from configurable
		// Curiuously, returns also the childs which aren't linked to the configurable...
		// ... to see ...
		// $productTypeIns = $product->getTypeInstance(true);
		// $childIds = $productTypeIns->getChildrenIds($product->getId());
		//
		// Other possibilities, not studied deeper : 
		// $products  = Mage::getSingleton('catalog/product_type')->factory($product)->getUsedProductCollection();
		// $products = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);

		$products  = $product->getTypeInstance()->getUsedProductCollection();
		
		$products->addItem($product);

		foreach($products as $product)
		{
			Mage::log($product->getSku(), null, 'partikule.log');

			// Product load, to get all attributes, like url_key
			$_product = $this->_getProduct($product->getId(), $store, $identifierType);

	        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
	            $_product->setWebsiteIds($productData['website_ids']);
	        }

	        foreach ($_product->getTypeInstance(true)->getEditableAttributes($_product) as $attribute) 
	        {
	            if ($this->_isAllowedAttribute($attribute) && isset($productData[$attribute->getAttributeCode()]))
	            {
	            	$option_value = $productData[$attribute->getAttributeCode()];

	            	// Find the option ID from its Admin label
	            	if ( $attribute->getBackendType() == 'int' &&  (int)$option_value == 0 )
	            	{
						foreach ($attribute->getSource(true)->getAllOptions() as $existing_option)
						{
							if ($existing_option['label'] == $option_value)
							{
								$option_value = $existing_option['value'];
								break;
							}
						}
	            	}
	            	
	                $_product->setData(
	                    $attribute->getAttributeCode(),
	                    $option_value
					);
	            }
	        }
	
	        $this->_prepareDataForSave($_product, $productData);
	
	        try {
	            if (is_array($errors = $_product->validate())) {
	                $strErrors = array();
	                foreach($errors as $code=>$error) {
	                    $strErrors[] = ($error === true)? Mage::helper('catalog')->__('Value for "%s" is invalid.', $code) : Mage::helper('catalog')->__('Value for "%s" is invalid: %s', $code, $error);
	                }
	                $this->_fault('data_invalid', implode("\n", $strErrors));
	            }

	            $_product->save();
	        } catch (Mage_Core_Exception $e) {
	            $this->_fault('data_invalid', $e->getMessage());
	        }
		}
        return true;
    }
*/

	
}

