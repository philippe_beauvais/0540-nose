<?php
/*
 *
 *	trace("{$select}".PHP_EOL);	
 *
 */


class Partikule_Nose_Model_Creator extends Mage_Core_Model_Abstract
{
    /**
     * Store perfume cache
     *
     * @var array
     */
    protected $_storeCreators = array();
	
		
	/**
	 * Returns 	The perfume's creator array, from eNose
	 *
	 * NOTICE : Only works with master products
	 *			The perfume ID comes from the product SKU
	 *			Example : 
	 *			SKU =			M-1256
	 *			perfume_id = 	1256
	 *
	 * @param	product
	 *
	 * @return	array	Array of creators or blank array if no one found
	 *
	 */
	public function getCreatorArrayFromProduct($product)
	{
		$creators = array();

    	// Local cache
        $cacheKey   = 'creators_' . $product->getSku();
		
		// Return cache if any
        if (isset($this->_storeCreators[$cacheKey]))
            return $this->_storeCreators[$cacheKey];

		
		// Get the perfume ID from product SKU
		$perfume_id = substr($product->getSku(), 2);
		
		if (count($perfume_id))
		{
			// Get Creators List from eNose
			$resource = Mage::getSingleton('core/resource');
			$read = $resource->getConnection('nose_read');
			$creatorsTable = $resource->getTableName('nose/creator');
			
			$select = $read->select('c.creator_id')
				->from( array('c' => $creatorsTable))
				->join( array('clk' => 'lk_perfume_creator'), 'c.creator_id = clk.creator_id')
				->joinLeft(
					array('cl' => 'creator_lang'), 
					"c.creator_id = cl.creator_id and cl.lang = '".Mage::helper('nose/core')->getCurrentStoreCode()."'", 
					array('lang', 'description')
				)
				->where('perfume_id = ?', $perfume_id)
				->order(array('ordering ASC'));

			$query = $select->query();
		
			$creators = $query->fetchAll();
			
			// Foreach Creator, get the linked page
			if ( ! empty($creators))
			{
				$resource = Mage::getSingleton('core/resource');
				$read = $resource->getConnection('ionize_read');
				$pageTable = $resource->getTableName('nose/ionize_creator_page');
				
				$creator_ids = array();
				foreach($creators as $creator)
				{
					$creator_ids[] = $creator['creator_id'];
				}
				
				// Get all pages for these creators ID
				$select = $read->select()
					->from( array('p' => $pageTable))
					->joinLeft( array('pl' => 'page_lang'), "p.id_parent = pl.id_page AND pl.lang = '".Mage::helper('nose/core')->getCurrentStoreCode()."' and p.parent='page'")
					->joinLeft( array('url' => 'url'), "p.id_parent = url.id_entity AND url.type='page' AND url.lang = '".Mage::helper('nose/core')->getCurrentStoreCode()."'")
					->where('creator_id IN(?)', $creator_ids);					
				
				$query = $select->query();
		
				$pages = $query->fetchAll();

				// Links pages to creators
				if ( ! empty($pages))
				{
					foreach($creators as &$creator)
					{
						foreach($pages as $page)
						{
							if ($page['creator_id'] == $creator['creator_id'])
							{
								$creator['page'] = $page;
							}
						}
					}
				}
			}			
		}
		
		// Store in local cache
        $this->_storeCreators[$cacheKey] = $creators;

		return $creators;
	}
	
	
	/**
	 * Returns one Creator's product collection
	 * Connects to the eNose DB and gets the masters which :
	 * 1. Are sell by Nose
	 *
	 * @param 	int 		Creator's eNose ID.
	 *
	 */
	public function getProductCollection()
	{
		$creator_id = $this->getCreatorIdFromCurrentPage();

		if ($creator_id)
		{
			// String list of masters ids : '12,14,1245,147'
			$grouped_master_ids = $this->getGroupedMasterIds($creator_id);
		
			$storeId = Mage::app()->getStore()->getId();

			$collection = Mage::getModel('catalog/product')->getCollection()
				->setStoreId( $storeId )
				->addAttributeToSelect("*")
				->addAttributeToFilter('master_id', array('in' =>  explode(',', $grouped_master_ids)))
				->addAttributeToFilter('type_id', 'configurable')
			;
			
			$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

			// Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');

			return $collection;
		}
		return FALSE;
	}


	/**
	 * Returns products grouped by category in one array
	 *
	 * Used by Creator Block o display products from Creator, by brand
	 *
	 *
	 */
    public function getCategoryProductCollection()
    {
    	$collection = $this->getProductCollection();
    	
    	// Category array to return
    	$data = array();

		if ($collection)
		{
			foreach($collection as $_product)
			{
				$id_category = $_product->getSingleCategoryId();

				// Only returns products which has one category
				if ($id_category)
				{
					if ( ! isset($data[$id_category]))
					{
						$data[$id_category] = array(
							'id' => $id_category,
							'name' => $_product->getcategory_name(),
							'products' => array()
						);
					}
					$data[$id_category]['products'][] = $_product;
				}
			}
		}

    	return $data;
    }
	


	/**
	 * Bla
	 *
	 * @param 	int 		bla
	 *
	 * @return 	string		bla
	 *
	 * @usage	bla
	 *
	 */
	public function getCreatorIdFromCurrentPage()
	{
		$result = array();
		$page = Mage::helper('ionize/page')->getRegisteredPage();
		
		// Get direct connection resource to Ionize table
		$read = Mage::getSingleton('core/resource')->getConnection('ionize_read');
		
		$select = $read->select()
			->from('module_nose_creators')
			->where('parent = ?', 'page')
			->where('id_parent = ?', $page->getIdPage())
		;

		$query = $select->query();
		$result = $query->fetch();
		
		if ( ! empty($result))
			return $result['creator_id'];
		
		return FALSE;
	}



	/**
	 * Returns master's IDs linked to one creator in eNose
	 *
	 * @param 	int 		creator ID
	 *
	 * @return	String		String list of Master IDs
	 *
	 */
	public function getGroupedMasterIds($creator_id)
	{
		$read = Mage::getSingleton('core/resource')->getConnection('nose_read');

		$select = $read->select('clk.perfume_id')
			->from( array('clk' => 'lk_perfume_creator'),
				array('ids' => new Zend_Db_Expr('GROUP_CONCAT( distinct clk.perfume_id)'))
			)
			->join( array('p' => 'perfume'), 'p.perfume_id = clk.perfume_id AND p.nose=1 AND clk.creator_id = ' . $creator_id)
//			->order(array('ordering ASC'))
		;

		$query = $select->query();
		$result = $query->fetch();
		
		if ( ! empty($result))
			return $result['ids'];
		
		return FALSE;
				
	}


}