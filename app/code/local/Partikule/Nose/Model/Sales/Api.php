<?php
/**
 * Partikule
 *
 *
 * @category    Nose
 * @package     Nose_Sales
 * @copyright   Copyright (c) 2016 Partikule. (http://www.partikule.net)
 *
 */

/**
 * Sales API
 *
 * @category   Nose
 * @package    Nose_Sales
 * @author     Partikule
 *
 */
class Partikule_Nose_Model_Sales_Api extends Mage_Sales_Model_Order_Api
{

	/**
	 * @param $from_period
	 * @param $to_period
	 * @param $status
	 * @param array $order_keys
	 * @param array $item_keys
	 * @return array
	 */
	public function getPeriodOrders($from_period, $to_period, $status, $order_keys=array(), $item_keys = array())
	{
		$data = array();

		if ( ! empty($order_keys))
		{
			// From ?
			$from_date = date('Y-m-d', strtotime($from_period));
			$to_date = date('Y-m-d', strtotime($to_period));

			// Status
			$status_code = $this->get_status_code($status);

			// Mage::log(print_r($order_keys ,true), null, 'partikule.log');
			// Mage::log(print_r($item_keys ,true), null, 'partikule.log');
			// Mage::log(print_r($from_date ,true), null, 'partikule.log');
			// Mage::log(print_r($status_code ,true), null, 'partikule.log');

			$orders = Mage::getModel('sales/order')->getCollection()
				->addAttributeToFilter('created_at', array('from'  => $from_date, 'to' => $to_date))
				->addAttributeToFilter('status', array('eq' => $status_code));

			// $array_keys = array_fill_keys ( $keys, '' );

			$data = $this->_prepare_orders_data($orders, $order_keys, $item_keys);
		}

		return $data;
	}


	/**
	 * @param $past_period
	 * @param $status
	 * @param array $order_keys
	 * @param array $item_keys
	 * @return array
	 *
	 */
	public function getLastPeriodOrders($past_period, $status, $order_keys=array(), $item_keys = array())
	{
		$data = array();

		if ( ! empty($order_keys))
		{
			// From ?
			$from_date = date('Y-m-d', strtotime($past_period));

			// Status
			$status_code = $this->get_status_code($status);


			// Mage::log(print_r($order_keys ,true), null, 'partikule.log');
			// Mage::log(print_r($item_keys ,true), null, 'partikule.log');
			// Mage::log(print_r($status_code ,true), null, 'partikule.log');

			$orders = Mage::getModel('sales/order')->getCollection()
				->addAttributeToFilter('created_at', array('from'  => $from_date))
				->addAttributeToFilter('status', array('eq' => $status_code));

			// $array_keys = array_fill_keys ( $keys, '' );

			$data = $this->_prepare_orders_data($orders, $order_keys, $item_keys);
		}

		return $data;
	}


	private function _prepare_orders_data($orders, $order_keys=array(), $item_keys = array())
	{
		$data = array();

		foreach ($orders as $order)
		{
			// Get only visible items (bundles of samples for example)
			// $items = $order->getAllItems();
			$order_data = $order->getData();

			$order_exported_data = array();

			foreach($order_keys as $_internal_key => $_external_key)
			{
				if (isset($order_data[$_internal_key]))
					$order_exported_data[$_external_key] = $order_data[$_internal_key];
				else
					$order_exported_data[$_external_key] = NULL;
			}

			if ( ! empty($item_keys))
			{
				$items = $order->getAllVisibleItems();

				foreach ($items as $item)
				{
					$item_data = $item->getData();

					$item_exported_data = array();

					foreach($item_keys as $_internal_key => $_external_key)
					{
						if (isset($item_data[$_internal_key]))
							$item_exported_data[$_external_key] = $item_data[$_internal_key];
						else
							$item_exported_data[$_external_key] = NULL;
					}

					// Can be done, no common keys in both arrays
					$data[] = array_merge
					(
						$order_exported_data,
						$item_exported_data
					);
				}
			}
			else
			{
				$data[] = $order_exported_data;
			}
		}

		return $data;
	}


	private function get_status_code($status ='')
	{
		$status_code = 'complete';

		// Status
		switch($status)
		{
			case 'new':
				$status_code = Mage_Sales_Model_Order::STATE_NEW;
				break;

			case 'pending_payment':
				$status_code = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
				break;

			case 'processing':
				$status_code = Mage_Sales_Model_Order::STATE_PROCESSING;
				break;

			case 'complete':
				$status_code = Mage_Sales_Model_Order::STATE_COMPLETE;
				break;

			case 'closed':
				$status_code = Mage_Sales_Model_Order::STATE_CLOSED;
				break;

			case 'canceled':
				$status_code = Mage_Sales_Model_Order::STATE_CANCELED;
				break;

			case 'holded':
				$status_code = Mage_Sales_Model_Order::STATE_HOLDED;
				break;

			case 'payment_review':
				$status_code = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
				break;

			default:
				$status_code = Mage_Sales_Model_Order::STATE_COMPLETE;
		}

		return $status_code;
	}
}
