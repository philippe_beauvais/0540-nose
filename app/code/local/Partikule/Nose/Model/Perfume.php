<?php

class Partikule_Nose_Model_Perfume extends Mage_Core_Model_Abstract
{
    /**
     * Store perfume cache
     *
     * @var array
     */
    protected $_storePerfume = array();
	
	
	/**
	 * Returns ...
	 * NOTICE : Only works with master products
	 *			The perfume ID comes from the product SKU
	 *			Example : 
	 *			SKU =			M-1256
	 *			perfume_id = 	1256
	 *
	 * @param	
	 *
	 * @return	
	 *
	 */
	public function getPerfumeDataFromProduct($product)
	{
		$perfume = array();

    	// Local cache
        $cacheKey   = 'perfume_' . $product->getSku();
		
		// Return cache if any
        if (isset($this->_storePerfume[$cacheKey]))
            return $this->_storePerfume[$cacheKey];

		// Get the perfume ID from product SKU
		$perfume_id = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);

		if (count($perfume_id))
		{
			$resource = Mage::getSingleton('core/resource');
			
			// Get connection to eNose view called : perfume_detail
			// $read = 	$resource->getConnection('nose_read');

			// Get connection to Magento table called : perfume_detail
			$read = 	$resource->getConnection('core/read');
			$perfumeTable = $resource->getTableName('nose/perfume_detail');

			$select = $read->select()
				->from( array('p' => $perfumeTable))
				->where('perfume_id = ?', $perfume_id)
				->where('lang = ?', Mage::helper('nose/core')->getCurrentStoreCode());
	
			$query = $select->query();

			$perfume = $query->fetch();
		}

		// Store in local cache
        $this->_storePerfume[$cacheKey] = $perfume;

		return $perfume;
	}
	
	
	/**
	 * Join the given collection to perfume_detail 
	 *
	 * @param 	Product Collection
	 *
	 * @return 	Product Collection
	 *
	 * @usage	
	 *
	 */
	public function joinToPerfumeDetail($collection)
	{
        $collection->getSelect()->joinLeft(
			array('pd' => 'perfume_detail'),
			"pd.nose_sku = e.sku and pd.lang='". Mage::helper('attribute/core')->getCurrentStoreCode() ."'",
			array('*')
		);

		return $collection;
	}
	
}