<?php

class Partikule_Nose_Model_Newsletter_Observer extends Partikule_Nose_Model_Abstract
{

	public function __construct()
	{
		parent::__construct();

		$this->setConnection('mynose_read', 'mynose_write');

		// See : etc/config.xml : <global><models><nose_mysql4>
		$this->table = Mage::getSingleton('core/resource')->getTableName('nose/lk_user_distributor');
		$this->user_table = Mage::getSingleton('core/resource')->getTableName('mynose/users');
	}

	/**
	 * Observer
	 * Called after AW_Advancednewsletter_Model_Subscriber->subscribe()
	 * See :
	 * @param $event
	 */
	public function updateEnoseUserAfterSubscribe($event)
	{
		$connection = $this->getWriteConnection();

		$subscriber = $event->getSubscriber();

		$existing_user = $this->getNoseUserFromEmail($subscriber->getEmail());

		if ($existing_user)
		{
			$sql = "
				update lk_user_distributor, users
				set lk_user_distributor.fg_unsubscribed = 0
				where users.id_user = lk_user_distributor.id_user
				and users.email = '".$subscriber->getEmail()."'
			";
			$connection->query($sql);

		}
		else
		{
			$data = array(
				'email' => $subscriber->getEmail(),
				'origin' => '7',
				'join_date' => date('Y-m-d H:i:s'),
				'favorite_lang' => Mage::app()->getStore()->getCode()
			);
			$connection->insert($this->user_table, $data);
		}

		$connection->commit();
	}


	public function updateEnoseUserAfterUnsubscribe($event)
	{
		$connection = $this->getWriteConnection();

		$subscriber = $event->getSubscriber();

		$sql = "
			update lk_user_distributor, users
			set lk_user_distributor.fg_unsubscribed = 1
			where users.id_user = lk_user_distributor.id_user
			and users.email = '".$subscriber->getEmail()."'
		";

		$connection->query($sql);

		$connection->commit();
	}


	public function getNoseUserFromEmail($email)
	{
		$select = $this->getReadConnection()->select()
			->from( array('u' => $this->user_table))
			->where('email = ?', $email)
		;

		$query = $select->query();
		$db_user = $query->fetch();

		return $db_user;
	}

	public function runLifecycleAction($observer)
	{
		$event_name = $observer->getEvent()->getName();

		$subscriber = $observer->getSubscriber();

		$existing_user = $this->getNoseUserFromEmail($subscriber->getEmail());

		if ($existing_user)
		{
			switch($event_name)
			{
				case 'an_subscriber_subscribe':
					Mage::helper('lifecycle')->action('newsletter_subscribe', $existing_user);
					break;

				case 'an_subscriber_unsubscribe':
					Mage::helper('lifecycle')->action('newsletter_unsubscribe', $existing_user);
					break;
			}
		}

		return $this;
	}

}
