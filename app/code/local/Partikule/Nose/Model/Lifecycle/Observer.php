<?php
/**
 * Class Partikule_Nose_Model_Lifecycle_Observer
 *
 * Example of one Observer
 * This class should be in another module, dedicated to your business layer.
 *
 */
class Partikule_Nose_Model_Lifecycle_Observer
{
	public function onInvitationSent($observer)
	{
		$this->onInvitationSentWithNewUserCreation($observer);
	}

	/**
	 * Concerns the Sponsor
	 *
	 * @param $observer
	 */
	public function onInvitationSentWithNewUserCreation($observer)
	{
		$sender = $observer->getSender();
		$invited = $observer->getInvited();
		// $relation = $observer->getRelation();

		$url = Mage::helper('mynose/url')->getContactAcceptEmailInvitationUrl();
		$url .= '/s/'.$sender['id_user'];
		$url .= '/i/'.$invited['id_user'];
		$url .= '/h/'.$sender['hash'];

		// Action Cycle 1
		Mage::helper('lifecycle')->action('invitation_online_sent', $sender, array(
			'sender' => $sender,
			'invited' => $invited,
		//	'relation' => $relation,
			'invitation_accept_url' => $url,
			'sender_name' => $sender['firstname'] . ' ' . $sender['lastname']
		));

		Mage::helper('lifecycle')->action('invitation_reception', $invited);
	}


	/**
	 * Concerns the Sponsor
	 *
	 * @param $observer
	 */
	public function onInvitationFacebookSentWithNewUserCreation($observer)
	{
		$sender = $observer->getSender();
		$invited = $observer->getInvited();

		// Action Cycle 1
		Mage::helper('lifecycle')->action('invitation_online_sent', $sender);
		Mage::helper('lifecycle')->action('invitation_reception', $invited);
	}


	/**
	 * Concerns the Godson
	 *
	 * @param $observer
	 */
	public function onInvitationAccepted($observer)
	{
		$invited = $observer->getInvited();
		Mage::helper('lifecycle')->action('invitation_accepted', $invited);
	}


	/**
	 * Concerns the Godson
	 *
	 * @param $observer
	 */
	public function onInvitationRejected($observer)
	{
		$invited = $observer->getInvited();
		Mage::helper('lifecycle')->action('invitation_rejected', $invited);
	}


	/**
	 * Concerns the Sponsor
	 * godson sent as additional data
	 *
	 * @param $observer
	 */
	public function onSponsorValidated($observer)
	{
		$sponsor = $observer->getSponsor();

		// Add additional data to data sent to Lifecycle
		// So tasks can use them
		$data = array(
			'sponsored' => $observer->getSponsored()
		);

		Mage::helper('lifecycle')->action('sponsorship', $sponsor, $data);
	}
}
