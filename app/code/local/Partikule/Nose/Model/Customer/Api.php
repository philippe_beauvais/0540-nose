<?php
/**
 * Partikule
 *
 *
 * @category    Nose
 * @package     Nose_Customer
 * @copyright   Copyright (c) 2016 Partikule. (http://www.partikule.net)
 *
 */

/**
 * Customer API
 *
 * @category   Nose
 * @package    Nose_Customer
 * @author     Partikule
 *
 */
class Partikule_Nose_Model_Customer_Api extends Mage_Customer_Model_Customer_Api
{
	/**
	 * Returns entities and hashed, separated by pipes (|)
	 *
	 * @return 	array (
	 * 				entities => 12;VE5ZUlBu|13;OG4wcW9sVE9IcHJ
	 * 			)
	 */
	public function getCustomersWithHashes()
	{
		// Mage::log('getCustomersWithHashes()', null, 'partikule.log');

		$result = array(
			'entities' => ''
		);

		$select = Mage::getSingleton('core/resource')->getConnection()->select()
			->from( array('eav' => 'eav_attribute'))
			->where('eav.attribute_code = ?', 'login_hash')
		;
		$query = $select->query();
		$attribute = $query->fetch();

		if ( ! empty($attribute))
		{
			$attribute_id = $attribute['attribute_id'];

			// 1. check if the users are potentially linked
			$select = Mage::getSingleton('core/resource')->getConnection()->select()
				->from( array('cev' => 'customer_entity_varchar'))
				->where('cev.attribute_id = ?', $attribute_id)
			;

			// Mage::log("{$select}".PHP_EOL, null, 'partikule.log');

			$query = $select->query();

			$entities = $query->fetchall();
		}

		// Be able to transmit thousands of lines without memory exhausting
		foreach($entities as $entity)
		{
			$result['entities'] .= $entity['entity_id'].';'.$entity['value'].'|';
		}

		// Mage::log(print_r($result ,true), null, 'partikule.log');

		return $result;
	}


	public function getUnsubscribedList()
	{
		$result = array();

		$select = Mage::getSingleton('core/resource')->getConnection()->select()
			->from( array('adv' => 'aw_advancednewsletter_subscriber'))
			->where('adv.status = ?', '2')
		;

		$query = $select->query();

		$items = $query->fetchall();

		foreach($items as $item)
		{
			$result[] = array(
				'customer_id' => $item['customer_id'],
				'email' => $item['email'],
				'first_name' => $item['first_name'],
				'last_name' => $item['last_name']
			);
		}

		return $result;
	}


	public function unsubscribe($customer_id)
	{
		// Mage::log('unsubscribe() ' . $customer_id, null, 'partikule.log');

		if ( ! empty($customer_id))
		{
			$connection = Mage::getSingleton('core/resource')->getConnection();

			$sql = "
				update aw_advancednewsletter_subscriber adv
				set 
					adv.status = 2,
					adv.segments_codes = ''
				where 
				adv.customer_id = ".$customer_id."
			";

			$connection->query($sql);

			$connection->commit();

			return TRUE;
		}

		return FALSE;
	}


	public function setCustomerGroup($customer_id, $group_code=NULL)
	{
		$select = Mage::getSingleton('core/resource')->getConnection()->select()
			->from( array('cg' => 'customer_group'))
			->where('cg.customer_group_code = ?', $group_code)
		;
		$query = $select->query();

		$group = $query->fetch();

		// Mage::log(print_r($group ,true), null, 'partikule.log');

		if ( ! empty($group))
		{
			$connection = Mage::getSingleton('core/resource')->getConnection();

			$sql = "
				update customer_entity c
				set 
					c.group_id = ". $group['customer_group_id'] ."
				where 
					c.entity_id = " . $customer_id . "
			";

			$connection->query($sql);

			$connection->commit();

			return TRUE;
		}

		return FALSE;
	}


	public function test()
	{
		$data = array(
			'key_1' => 'Value 1',
			'key_2' => 'Value 2',
		);

		return $data;
	}
}