<?php

abstract class Partikule_Nose_Model_Abstract extends Mage_Core_Model_Abstract
{
	protected $_read;
	
	protected $_write;
	

    /**
     * Resource initialization
     */
    protected function _construct()
    {
    }
	
	

    /**
     * Set connections for entity operations
     *
     * @param Zend_Db_Adapter_Abstract|string $read
     * @param Zend_Db_Adapter_Abstract|string|null $write
     * @return Mage_Eav_Model_Entity_Abstract
     */
    public function setConnection($read, $write=null)
    {
        $this->_read = $read;
        $this->_write = $write ? $write : $read;
        return $this;
    }


    /**
     * Retrieve connection for read data
     *
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getReadAdapter()
    {
        if (is_string($this->_read)) {
            $this->_read = Mage::getSingleton('core/resource')->getConnection($this->_read);
        }
        return $this->_read;
    }

    /**
     * Retrieve connection for write data
     *
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getWriteAdapter()
    {
        if (is_string($this->_write)) {
            $this->_write = Mage::getSingleton('core/resource')->getConnection($this->_write);
        }
        return $this->_write;
    }

    /**
     * Retrieve read DB connection
     *
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    public function getReadConnection()
    {
        return $this->_getReadAdapter();
    }

    /**
     * Retrieve write DB connection
     *
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    public function getWriteConnection()
    {
        return $this->_getWriteAdapter();
    }
}
