<?php
/**
 */
# Controllers are not autoloaded so we will have to do it manually:
# require_once 'Mage/Checkout/controllers/CartController.php';
require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'CartController.php';
/**
 * Shopping cart controller Extension
 *
 */
class Partikule_Nose_Checkout_CartController extends Mage_Checkout_CartController
{

	/**
	 * Adds one Sample Kit to the cart
	 * Based on Master IDs
	 *
	 */
	public function addMasterKitAction()
	{
		$bundle_id = $this->getRequest()->getParam('bundle');
		$master_ids = trim($this->getRequest()->getParam('products'), ',');
		$master_ids = explode(',', $master_ids);
		$sample_ids = array();

		foreach($master_ids as $master_id)
		{
			$collection = Mage::getModel('catalog/product')->getCollection();
			$collection->addAttributeToSelect('master_id');
			$collection->addFieldToFilter(array(
				array('attribute'=>'master_id','eq'=>$master_id),
			));

			// Mage::log($collection->getData(), null, 'partikule.log');

			foreach($collection as $_product)
			{
				$sku = $_product->getSku();
				if (substr($sku, 0, 2) == 'S-')
				{
					$sample_ids[] = $_product->getId();
					break;
				}
			}
		}

		$cart_bundle_option = array();
		$bundle = new Mage_Catalog_Model_Product();;
		$bundle->load($bundle_id);

		try
		{
			// We can't continue
			if ($bundle->getTypeId() != 'bundle')
			{
				$this->_goBack();
				return;
			}
			else
			{
				$optionCollection = $bundle->getTypeInstance(TRUE)->getOptionsCollection($bundle);

				// Get all products linked to all options of the bundle
				$selectionCollection = $bundle->getTypeInstance(TRUE)->getSelectionsCollection(
					$bundle->getTypeInstance(TRUE)->getOptionsIds($bundle),
					$bundle
				);
				foreach($optionCollection as $option)
				{
					$diag_product_id = array_shift($sample_ids);

					$optionSelectionCollection = $selectionCollection->getItemsByColumnValue('option_id', $option->getId());

					// Loop into all Option products
					foreach($optionSelectionCollection as $option_product)
					{
						// If the asked sample is in the list : OK, add it
						if ($option_product->getId() == $diag_product_id)
						{
							$cart_bundle_option[$option->getId()] = $option_product->getSelectionId();
							break;
						}
					}
				}
				$params = array(
					'product' => $bundle_id,
					'related_product' => NULL,
					'bundle_option' => $cart_bundle_option,
					'qty' => 1,
				);

				$cart = Mage::getSingleton('checkout/cart');

				$cart->addProduct($bundle, $params);
				$cart->save();
				$this->_getSession()->setCartWasUpdated(true);

				Mage::dispatchEvent('checkout_cart_add_product_complete',
					array('product' => $bundle, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);

				$this->_redirect('checkout/cart');
			}
		}
		catch (Exception $e)
		{
			$this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
			Mage::logException($e);
			$this->_goBack();
		}

	}

	/**
	 * Add one Sample Kit to the cart
	 * Needs the products IDs as request param
	 *
	 */
	public function addKitAction()
	{
		$bundle_id = $this->getRequest()->getParam('bundle');
		$diag_product_ids = explode(',', $this->getRequest()->getParam('products'));
		$cart_bundle_option = array();

		// $bundle = Mage::getModel('catalog/product');
		$bundle = new Mage_Catalog_Model_Product();;
		$bundle->load($bundle_id);

		try
		{
			// We can't continue
			if ($bundle->getTypeId() != 'bundle')
			{
				$this->_goBack();
				return;
			}
			else
			{
				$optionCollection = $bundle->getTypeInstance(TRUE)->getOptionsCollection($bundle);

				// Get all products linked to all options of the bundle
				$selectionCollection = $bundle->getTypeInstance(TRUE)->getSelectionsCollection(
					$bundle->getTypeInstance(TRUE)->getOptionsIds($bundle),
					$bundle
				);
				foreach($optionCollection as $option)
				{
					// trace('<h2>' . $option->getId() . '</h2>');
					$diag_product_id = array_shift($diag_product_ids);

					$optionSelectionCollection = $selectionCollection->getItemsByColumnValue('option_id', $option->getId());

					// Loop into all Option products
					foreach($optionSelectionCollection as $option_product)
					{
						// If the asked sample is in the list : OK, add it
						if ($option_product->getId() == $diag_product_id)
						{
							$cart_bundle_option[$option->getId()] = $option_product->getSelectionId();
							break;
						}
					}
				}
				$params = array(
					'product' => $bundle_id,
					'related_product' => NULL,
					'bundle_option' => $cart_bundle_option,
					'qty' => 1,
				);

				$cart = Mage::getSingleton('checkout/cart');

				$cart->addProduct($bundle, $params);
				$cart->save();
				$this->_getSession()->setCartWasUpdated(true);


				/**
				 * @todo remove wishlist observer processAddToCart
				 */
				Mage::dispatchEvent('checkout_cart_add_product_complete',
					array('product' => $bundle, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);

				$this->_redirect('checkout/cart');
			}
		}
		catch (Exception $e)
		{
			$this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
			Mage::logException($e);
			$this->_goBack();
		}
	}
}
