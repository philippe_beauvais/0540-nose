<?php
/**
 *
 */
require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';


class Partikule_Nose_OnepageController extends Mage_Checkout_OnepageController
{


    /**
     * save checkout billing address
     */
    public function saveAddressesAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost())
        {

			// Mage::log($this->getRequest()->getPost(), null, 'partikule.log');

            $billing_data = $this->getRequest()->getPost('billing', array());
            $shipping_data = $this->getRequest()->getPost('shipping', array());
            
            // Customer Address ID
            $billingAddressId = $this->getRequest()->getPost('billing_address_id', false);
            $shippingAddressId = $this->getRequest()->getPost('shipping_address_id', false);

			// Mage::log('$billingAddressId : ' . $billingAddressId, null, 'partikule.log');
			// Mage::log('$shippingAddressId : ' . $shippingAddressId, null, 'partikule.log');

			// Set comments
//	        $this->getOnepage()->getQuote()->setComments($this->getRequest()->getPost('comments'));
//			$shipping_data['comments'] = $billing_data['comments'] = $this->getRequest()->getPost('comments');



            if (isset($billing_data['email'])) {
                $billing_data['email'] = trim($billing_data['email']);
            }
            
            if (
            	(isset($shipping_data['shipping_address']) && $shipping_data['shipping_address'] == 'same')
            	OR ($shipping_data['pickup'] == 1)
            
            )
            {
            	$billing_data['use_for_shipping'] = 1;
              	$billing_data['same_as_billing'] = 1;
            }

            $result = $this->getOnepage()->saveBilling($billing_data, $billingAddressId);

			
            if ( ! isset($result['error']))
            {
            	/*
            	 * Set or not the same_as_billing value
            	 *
            	 */
                if ( 
                	( isset($shipping_data['shipping_address']) && $shipping_data['shipping_address'] == 'same') 
                	OR ( $shipping_data['pickup'] == 1)
                )
                {
					$result = $this->getOnepage()->saveShipping($billing_data, $billingAddressId);
				}
                else
                {
					$result = $this->getOnepage()->saveShipping($shipping_data, $shippingAddressId);
				}
            	
           	
            	/*
            	 * Pickup in store
            	 *
            	 */
                if (
                	$this->getOnepage()->getQuote()->isVirtual()
                	OR ( isset($shipping_data['shipping_address']) && $shipping_data['shipping_address'] == 'pickup_store')
                	OR ( $shipping_data['pickup'] == 1 )
                )
                {
                	// Bypass "Méthode de livraison non valide" when saving shipping mehod from here with traditionnal approach :
                	// Problem comes from Mage_Checkout_Model_Type_Onepage->saveShippingMethod() : getShippingRateByCode()
					/*
					$result = $this->getOnepage()->saveShippingMethod('pickup_store');
            		if ( ! isset($result['error']))
            		{
	                    $result['goto_section'] = 'payment';
	                    $result['update_section'] = array(
	                        'name' => 'payment-method',
	                        'html' => $this->_getPaymentMethodsHtml()
	                    );
	                }
	                */
					$this->getOnepage()->getQuote()->getShippingAddress()->setShippingMethod('pickup_store');
					$this->getOnepage()->getQuote()->collectTotals()->save();
					
					$result = array(
	                    'goto_section' => 'payment',
	                    'update_section' => array(
	                        'name' => 'payment-method',
	                        'html' => $this->_getPaymentMethodsHtml()
	                    )
	                );
                }
                // Same adress than billing : Case of 
                else {
 
                    $result['goto_section'] = 'shipping_method';

                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

					$result['allow_sections'] = array('billing');
					$result['duplicateBillingInfo'] = 'true';
                }

				/*
								 * Comments
								 * Doesn't work. Have a look at Mage_Checkout_OnepageController->saveShippingMethodAction()
								 * where it should be retrievable...
								 * ... but where it is not..
								 */
				/*
				$this->getOnepage()->getQuote()->getShippingAddress()->setComments($this->getRequest()->getPost('comments'));
				Mage::log('Comments : ' . $this->getOnepage()->getQuote()->getShippingAddress()->getComments(), null, 'partikule.log');
				$this->getOnepage()->getQuote()->collectTotals()->save();
				*/
                
            }
			
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }

	}




}
