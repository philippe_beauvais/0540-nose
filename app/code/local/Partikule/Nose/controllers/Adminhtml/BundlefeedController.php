<?php

/**
 * Backend Settings form
 * Feeds the Diagnostic bundle selections with all available samples
 *
 */
class Partikule_Nose_Adminhtml_BundlefeedController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$settings_product_id = $this->getRequest()->getParam('bundle_id');

		// Load bundle
		$product = Mage::getModel('catalog/product');
		$product->load($settings_product_id);

		// Vital !
		try
		{
			if ( ! ($product->getTypeId() == 'bundle'))
			{
				Mage::throwException($this->__('This product is not one bundle. Please correct the product ID'));
			}
			else
			{
				$nb_linked = $this->_linkAllSamplesToBundle($product);

				$message = $this->__('The diagnostic Bundle has been successfully feeded.<br/>' . $nb_linked . ' products added.');
				Mage::getSingleton('adminhtml/session')->addSuccess($message);

			}

		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}



		$this->_redirectUrl($this->_getRefererUrl());
	}



	/**
	 *
	 * Links all Samples products to the Nose Diagnostic bundle
	 *
	 * @return int
	 */
	private function _linkAllSamplesToBundle($product)
	{
		$nb_added_samples = 0;

		Mage::register('product', $product);

		// Get all Samples
		$samples_collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect("*")
			->addAttributeToFilter('type_id', 'simple')
			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'S-%')))
		;

		// $nb_samples = count($samples_collection);

		// Existing Options
		$optionCollection = $product->getTypeInstance(TRUE)->getOptionsCollection($product);

		// Already Linked products
		$selectionCollection = $product->getTypeInstance(TRUE)->getSelectionsCollection(
			$product->getTypeInstance(TRUE)->getOptionsIds($product),
			$product
		);

		// Add "Selections" as container for Collection of linked products
		$optionCollection->appendSelections($selectionCollection);

		$optionRawData = array();
		$selectionRawData = array();

		foreach($optionCollection as $option)
		{
			$selection_product_ids = array();

			$optionRawData[$option->getID()] = array(
				'option_id' => $option->getID(),
				'required' => $option->getData('required'),
				'position' => $option->getData('position'),
				'type' => $option->getData('type'),
				'title' => $option->getData('title')?$option->getData('title'):$option->getData('default_title'),
				'delete' => ''
			);

			foreach ($option->getSelections() as $selection)
			{
				// Array of products already in the option selection list
				$selection_product_ids[] = $selection->getProductId();

				$selectionRawData[$option->getID()][] = array(
					'selection_id' => $selection->getSelectionId(),
					'product_id' => $selection->getProductId(),
					'option_id' => $option->getID(),
					'position' => $selection->getPosition(),
					'is_default' => $selection->getIsDefault(),
					'selection_price_type' => $selection->getSelectionPriceType(),
					'selection_price_value' => $selection->getSelectionPriceValue(),
					'selection_qty' => $selection->getSelectionQty(),
					'selection_can_change_qty' => $selection->getSelectionCanChangeQty(),
					'delete' => ''
				);
			}

			foreach($samples_collection as $sample)
			{
				if ( ! in_array($sample->getId(), $selection_product_ids))
				{
					$nb_added_samples += 1;

					$selectionRawData[$option->getID()][] = array(
						'product_id' => $sample->getId(),
						'option_id' => $option->getId(),
						'position' => 0,
						'is_default' => 0,
						'selection_price_type' => 0,
						'selection_price_value' => 0,
						'selection_qty' => 1,
						'selection_can_change_qty' => 0,
						'delete' => ''
					);
				}
			}
		}

		$product->setCanSaveConfigurableAttributes(FALSE);
		$product->setCanSaveCustomOptions(TRUE);
		$product->setCanSaveBundleSelections(TRUE);
		$product->setAffectBundleProductSelections(TRUE);

		$product->setBundleOptionsData($optionRawData);
		$product->setBundleSelectionsData($selectionRawData);

		// Save
		$product = $product->save();

		// trace($nb_samples . ' samples.');
		// trace($nb_added_samples . ' samples added to each Diagnostic Bundle option selection.');

		return $nb_added_samples;
	}

}