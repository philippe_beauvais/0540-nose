<?php

class Partikule_Nose_ProductController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Just do nothing
	 *
	 */
	public function indexAction()
	{
		echo('');
	}


	/**
	 * Loads "root" defined in : 	nose/default/layout/nose.xml
	 * Handler : 					nose_product_getbrandproducts
	 * Purpose : 					Return the attribute name / value filtered product list
	 * Called through XHR by : 		ionize/brand/product_nav.phtml
	 *  
	 * POST data send : 
	 *
	 * - category_id : 		Category ID
	 * - attribute : 		Attribute name
	 * - attribute_value : 	Attribute Value
	 * - ... : 				All other args 
	 *
	 * @usage : In templates : 
	 *			 	new Ajax.Updater(
	 *					'productList', 
	 *					'nose/product/getbrandproducts', 
	 *					{
	 *						'method': 'post',
	 *						'parameters': {
	 *							'category_id': 	'<category_id>',
	 *							'attribute': 	'<attribute name>',
	 *							'value': 		'<attribute value>'
	 *						} 
	 *					}
	 *				);
	 *
	 *
	 */
	public function getBrandProductsAction()
	{
		$this->_processXhrPost();
	}
	
	
	public function getNewProductsAction()
	{
		$this->_processXhrPost();
	}
	
	
	
	/**
	 * Loads "root" defined in : 	nose/default/layout/nose.xml
	 * Handler : 					nose_product_getcreatorproducts
	 * Purpose : 					Return the creator's linked product list
	 * Called through XHR by : 		ionize/brand/product_nav.phtml
	 *
	 *
	 */
	public function getCreatorProductsAction()
	{
		$this->_processXhrPost();
	}
	
	
	/**
	 * Generic function to process POST data from XHR
	 * Loads the layout linked to the caller function
	 * and register post data
	 *
	 */
	protected function _processXhrPost()
	{
		if ($this->getRequest()->isPost())
		{
			// Register $_POST values to make them available for the template
			foreach($this->getRequest()->getPost() as $key=>$value)
			{
				Mage::register($key, $value);
			}
			
			// Renders the layout defined in "attribute.xml" for this method
			$this->loadLayout()->renderLayout();
		}
	}

}