<?php
/**
 * Partikule Nose Order controller
 *
 */


class Partikule_Nose_OrderController extends Mage_Core_Controller_Front_Action
{

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Customer order history
     */
    public function historyAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Orders'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

	/**
	 * Reorder from one invoice
	 *
	 */
	public function reorderAction()
	{
		if (!$this->_loadValidOrder()) {
			return;
		}
		$order = Mage::registry('current_order');

		$cart = Mage::getSingleton('checkout/cart');


		foreach ($order['lines'] as $item)
		{
			// Get the corresponding product if it exists
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item['product_code']);

			if ($product)
			{
				try {
					$cart->addOrderItem($product);
				} catch (Mage_Core_Exception $e){
					if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
						Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
					}
					else {
						Mage::getSingleton('checkout/session')->addError($e->getMessage());
					}
					$this->_redirect('*/*/history');
				} catch (Exception $e) {
					Mage::getSingleton('checkout/session')->addException($e,
						Mage::helper('checkout')->__('Cannot add the item to shopping cart.')
					);
					$this->_redirect('checkout/cart');
				}
			}
		}

		$cart->save();
		$this->_redirect('checkout/cart');


	}
    
    /**
     * Order's detail
     *
     */
    public function viewAction()
    {
        if (!$this->_loadValidOrder()) {
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sales/order/history');
        }
        $this->renderLayout();
    }
    
    
    protected function _loadValidOrder($orderId = null)
    {
        if (null === $orderId) {
            $orderId = (int) $this->getRequest()->getParam('order_id');
        }
        if (!$orderId) {
            $this->_forward('noRoute');
            return false;
        }

        $order = Mage::getModel('nose/order')->getOrder($orderId);

        if ($this->_canViewOrder($order))
        {
            Mage::register('current_order', $order);
            return true;
        }
        else {
            $this->_redirect('*/*/history');
        }
        return false;
    }
    
    
    /**
     * Check order view availability
     *
     * @param   Mage_Sales_Model_Order $order
     * @return  bool
     */
    protected function _canViewOrder($order)
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();

        if ($order['magento_id'] && ($order['magento_id'] == $customerId))
        {
            return true;
        }
        return false;
    }


}
