<?php
/**
 * Nose URL Helper
 * 
 *
 */
class Partikule_Nose_Helper_Url extends Mage_Core_Helper_Abstract
{
	// eNose Orders link
	public function getOrderHistory() { return Mage::getBaseUrl(). 'nose/order/history'; }
	
	// Magento Orders link
	public function getSalesOrderHistory() { return Mage::getBaseUrl(). 'sales/order/history'; }
	
	


}
