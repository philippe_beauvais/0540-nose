<?php
/**
 * Nose Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Nose_Helper_Product extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Returns the list of products also bought by customers who bought the given product
	 * Limits to the given category if one is given
	 *
	 * @return 	string		bla
	 *
	 *
	 */
	public function getAlsoBoughtProductCollection($limit_to_category = FALSE, $limit = 25)
	{
		$storeId = Mage::app()->getStore()->getId();
		
		$product = Mage::registry('product');

		// Simples products linked to the master $product
		$product_ids = array();
		$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(NULL,$product);

		foreach($childProducts as $p)
			$product_ids[] = $p['entity_id'];
		
		// Orders containing asked products
		$orderCollection = Mage::getModel('sales/order_item')->getCollection()
			->addAttributeToSelect('order_id')
			->addAttributeToFilter('product_id', array('in' => $product_ids))
		;
		// Adds the customer IDs to the flat order items
		$orderCollection->getSelect()
				->joinLeft(
					array('order' => 'sales_flat_order'), 
					"order.entity_id = main_table.order_id", 
					array('customer_id')
				)
		;
		
		// Customer's IDs
		$customer_ids = array();
		foreach($orderCollection as $order)
			if ( ! in_array($order['customer_id'], $customer_ids)) { $customer_ids[] = $order['customer_id']; }

		// Orders from Users who ordered
		$orderCollection = Mage::getModel('sales/order_item')->getCollection()
			->distinct(TRUE)
			->addAttributeToSelect('product_id')
			->addAttributeToSelect('sku')
            ->addAttributeToFilter('sku', array('like'=>'P-%'))
            ->addExpressionFieldToSelect('customer', 'count(distinct(customer_id))', 'customer_id')
            ->addAttributeToFilter('customer_id', array('in' => $customer_ids))
		;

		$orderCollection->getSelect()
			->joinLeft(
				array('order' => 'sales_flat_order'), 
				"order.entity_id = main_table.order_id", 
				array('customer_id')
			)
			->group('product_id')
			->group('sku')
			->order('customer DESC')
			->limit($limit)
		;

		
		// Mage::log("{$orderProductCollection->getSelect()}".PHP_EOL, null, 'partikule.log');

		// Master products ID (perfume IDs in case of perfumes)
		$master_product_ids = array();

		foreach($orderCollection as $p)
		{
			$t = explode('-', $p->getSku());
			if (isset($t[1]))
			{
				$master_id = $t[1];
				if ( ! in_array($t[1], $master_product_ids) && ($t[1] != $product->getPerfumeId()) )
					$master_product_ids[] = $t[1];
			}
		}
		
		// Finally get the master product collection
		if ( $limit_to_category !== FALSE)
		{
			$category = Mage::registry('current_category');
			$collection = $category->getProductCollection();
		}
		else
		{
			$storeId = Mage::app()->getStore()->getId();
			$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
			Mage::getModel('catalog/layer')->prepareProductCollection( $collection );
		}
		
		$collection->addAttributeToFilter('type_id', 'configurable')
			->addAttributeToSelect('*')
			->addAttributeToFilter('perfume_id', array('in' => $master_product_ids))
		;
		
		// Adds the category URL to each item of the collection
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

        foreach ($collection as $_item) {
            if ($_item->getTypeId() == 'configurable'){
                $remove = true;
                $_products = $_item->getTypeInstance(true)->getUsedProducts(null, $_item);
                foreach ($_products as $_product) {
                    if(!$_product->isDisabled()) {
                        $remove = false;
                        break;
                    }

                }
                if ($remove) {
                    $collection->removeItemByKey($_item->getId());
                }
            }
        }

		return $collection;
	}
	
	
	/**
	 * Returns the new products collection of one category
	 * If no category is registered, returns the whole new product list
	 *
	 * @note		Filters products on SKU starting with "M-"
	 *
	 * @param		Int		Number of products to return.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getNewProductCollection($nbProducts = 10)
	{
		$category = Mage::registry('current_category');

		if ( $category)
		{
			$collection = $category->getProductCollection();
		}
		else
		{
			$storeId = Mage::app()->getStore()->getId();
			$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
		}
		
		$collection->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', 4);
		;

		// Takken from Mage_Catalog_Block_Product_New
		$todayDate	= Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		//Mage::log($todayDate, null, 'partikule.log');

		$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

		$collection
			->addStoreFilter()
			->addAttributeToFilter('news_from_date', array('or'=> array(
				0 => array('date' => TRUE, 'to' => $todayDate),
				1 => array('is' => new Zend_Db_Expr('null')))
			), 'left')
			->addAttributeToFilter('news_to_date', array('or'=> array(
				0 => array('date' => TRUE, 'from' => $todayDate),
				1 => array('is' => new Zend_Db_Expr('null')))
			), 'left')
			->addAttributeToFilter(
				array(
					array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
					array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
					)
			  )
			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'M-%')))
			->addAttributeToSort('news_from_date', 'desc')
        ;

        $collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

        foreach ($collection as $_item) {
            if ($_item->getTypeId() == 'configurable'){
                $remove = true;
                $_products = $_item->getTypeInstance(true)->getUsedProducts(null, $_item);
                foreach ($_products as $_product) {
                    if(!$_product->isDisabled()) {
                        $remove = false;
                        break;
                    }

                }
                if ($remove) {
                    $collection->removeItemByKey($_item->getId());
                }
            }
        }

        $collection
			->setPageSize($nbProducts)
			->setCurPage(1)
		;

		//Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');

		return $collection;
	}


	public function getAlsoLikedProductCollection($limit=NULL)
	{
		$storeId = Mage::app()->getStore()->getId();

		$product = Mage::registry('product');

		$perfume_id = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);

		if ($perfume_id)
		{
			$resource = Mage::getSingleton('core/resource');

			// Get connection to Magento table called : perfume_detail
			$read = $resource->getConnection('nose_read');

			$sql = "
				select
					aos.*
				from
					asso_input_sales aos
				where
					input_perfume_id = '".$perfume_id."'
					and nose = 1
				order by aos.score DESC
			";

			if ($limit != NULL)
				$sql .= " limit 10";

			$results = $read->fetchAll($sql);

			$ids = array();
			foreach($results as $r)
				$ids[] = $r['sale_perfume_id'];

			$collection = Mage::getModel('catalog/product')->getCollection()
				->setStoreId( $storeId )
				->addAttributeToSelect("*")
				->addAttributeToFilter('master_id', array('in' => $ids))
				->addAttributeToFilter('type_id', 'configurable')
			;

			$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

            foreach ($collection as $_item) {
                if ($_item->getTypeId() == 'configurable'){
                    $remove = true;
                    $_products = $_item->getTypeInstance(true)->getUsedProducts(null, $_item);
                    foreach ($_products as $_product) {
                        if(!$_product->isDisabled()) {
                            $remove = false;
                            break;
                        }

                    }
                    if ($remove) {
                        $collection->removeItemByKey($_item->getId());
                    }
                }
            }

			// foreach($collection as $_product)
			//	Mage::log($_product->getName(), null, 'partikule.log');

			return $collection;
		}

		return FALSE;
	}
	
	/**
	 * Returns the most viewed product collection
	 * If no category is registered, returns the whole new product list
	 *
	 * @note		Filters products on SKU starting with "M-"
	 *
	 * @param		Int			Number of products to return.
	 * @param		Arrray		Array of attributes to filter on.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	/*
	
	NOT HERE FOR THE MOMENT.
	SAME CODE IN : 			Partikule_Boutique_Block_Bestseller
	
	LET THIS CODE HERE FOR FUTURE USE
	
	public function getMostViewedProductCollection($nbProducts = 10, $attributes = array())
	{
		$collection = Mage::getResourceModel('reports/product_collection')
			->addAttributeToSelect('*')
			->addAttributeToFilter(array(array('attribute'=>'sku', 'like'=>'M-%')))
			->setStoreId($storeId)
			->addStoreFilter($storeId)
			->addViewsCount()
			->setPageSize($nbProducts)
		;


		foreach($attributes as $attribute)
		{
			list($code, $value) = each($attribute);
			$collection->addAttributeToFilter($code, $value);
		}
		
		
		
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
		
		return $collection;
	}
	*/

	
	
	
	/**
	 * Returns one product first category
	 * We expect that it returns the brand in the Nose case
	 *
	 */
	public function getProductFirstCategory($product)
	{
		if ($product)
		{
			$catIds = $product->getCategoryIds();
			
			if ( ! empty($catIds))
			{
				$relativeCatId  = $catIds[0];
				return Mage::getModel("catalog/category")->load($relativeCatId);
			}
		}
		
		return FALSE;
	}
	
	
	/**
	 * Returns one product's firt configurable parent.
	 *
	 *
	 */
	public function getParentProduct($product)
	{
		$objConfigurableProduct = Mage::getModel('catalog/product_type_configurable');
    	$arrConfigurableProductIds = $objConfigurableProduct->getParentIdsByChild($product->getId());
    	
    	if ( isset($arrConfigurableProductIds[0]))
    	{
    		$_product = Mage::getModel('catalog/product')->load($arrConfigurableProductIds[0]);
    		return $_product;
    	}
		
		return FALSE;
	}
	
	
	/**
	 * Get one Configurable product based on the child's SKU
	 * The product should have the SKU : M-XXX where XXX is the number got from the simple SKU
	 *
	 * @param 	string 		SKU
	 * @return bool
	 */
	public function getParentProductFromChildSku($product_sku)
	{
		$sku_segments = explode('-', $product_sku);

		$id = ( ! empty($sku_segments[1])) ? $sku_segments[1] : FALSE;
		
		if ($id !== FALSE)
		{
    		$_product = Mage::getModel('catalog/product')->loadByAttribute('sku','M-' . $id);
    		return $_product;
		}
		else
		{
			Mage::log('app/code/local/Partikule/Nose/Helper/Product.php : No Master product for product : ' . $product_sku, null, 'partikule.log');
		}
		
		return FALSE;
		
	}
	
	/**
	 * Returns one product's firt configurable parent.
	 *
	 *
	 */
	public function getParentProductFromChildId($product_id)
	{
		$objConfigurableProduct = Mage::getModel('catalog/product_type_configurable');
    	$arrConfigurableProductIds = $objConfigurableProduct->getParentIdsByChild($product_id);
    	
    	if ( isset($arrConfigurableProductIds[0]))
    	{
    		$_product = Mage::getModel('catalog/product')->load($arrConfigurableProductIds[0]);
    		return $_product;
    	}
		
		return FALSE;
	}


	/**
	 * Get the bundle child product array from one bundled product
	 * @param $bundle_product
	 *
	 * @return array
	 */
	public function getBundleChildProducts($bundle_product)
	{
		$child_collection = array();

		// Ordered options
		$order_options = Mage::getModel('bundle/product_type')->getOrderOptions($bundle_product);

		if ( ! empty($order_options['info_buyRequest']['bundle_option']))
		{
			// List of selected options
			$bundle_options = $order_options['info_buyRequest']['bundle_option'];

			$selectionCollection = $bundle_product->getTypeInstance(TRUE)->getSelectionsCollection(
				$bundle_product->getTypeInstance(TRUE)->getOptionsIds($bundle_product),
				$bundle_product
			);

			// Foreach selected option, get the product
			foreach ($bundle_options as $option_id => $selected_option_id)
			{
				$product = $selectionCollection->getItemByColumnValue('selection_id', $selected_option_id);

				// The product can not found if the selections have been updated
				if ($product)
				{
					$_parent_product = Mage::helper('nose/Product')->getParentProductFromChildSku($product->getSku());
					$_parent_product_category = Mage::helper('nose/Product')->getProductFirstCategory($_parent_product);

					if ( ! $_parent_product)
					{
						$_product_category = Mage::helper('nose/Product')->getProductFirstCategory($product);
						$product->setCategory($_product_category);
						$child_collection[] = $product;
					}
					else
					{
						$_parent_product->setCategory($_parent_product_category);
						$child_collection[] = $_parent_product;
					}
				}
			}
		}

		return $child_collection;
	}
	
	
}
