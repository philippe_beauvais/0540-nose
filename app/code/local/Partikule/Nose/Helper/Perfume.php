<?php
/**
 * Nose Helper
 * Get Noses data from eNose
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Nose_Helper_Perfume extends Mage_Core_Helper_Abstract
{
    /**
     * Store Noses cache
     *
     * @var array
     */
    protected $_storePerfume = array();
    
    
    /**
     * Retrieve one Perfume Data from a product
     *
     * @param   	Product Model
	 *
     * @return  	Array of perfume data
     *
     */
    public function getPerfumeDataFromProduct($product)
    {
    	$perfume = array();
    	
    	// Local cache
        $cacheKey   = 'creator_'.$product->getId();

        if (isset($this->_storePerfume[$cacheKey]))
            return $this->_storePerfume[$cacheKey];

		// Noses from eNose DB
        $perfume = Mage::getModel('nose/perfume')->getPerfumeDataFromProduct($product);

        $this->_storePerfume[$cacheKey] = $perfume;
        
        return $perfume;
    }
    
    
    /**
     * Returns the Master Product ID (eNose side) from one product SKU
     *
     * @param 	Product		Product instance
     *
     * @return 	Mixed		Master product ID / FALSE if no one is found
     *
     */
    public function getMasterIdFromSku($product)
    {
    	if ($product && $sku = $product->getSku())
    	{
            $arrSku = explode('-', $sku);
            $masterId = intval($arrSku[1]);
            if ($masterId) {
                return $masterId;
            }
    	}
    	
    	return false;
    }
    
}