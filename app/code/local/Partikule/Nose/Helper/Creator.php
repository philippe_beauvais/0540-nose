<?php
/**
 * Nose Helper
 * Get Noses data from eNose
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Nose_Helper_Creator extends Mage_Core_Helper_Abstract
{
    /**
     * Store Noses cache
     *
     * @var array
     */
    protected $_storeCreator = array();
    
    
    /**
     * Retrieve Noses Collection
     *
     * @param   Product Model
	 *
     * @return  
     *
     */
    public function getCreatorArrayFromProduct($product)
    {
    	$creators = array();
    	
    	// Local cache
        $cacheKey   = 'creator_'.$product->getId();

        if (isset($this->_storeCreator[$cacheKey]))
            return $this->_storeCreator[$cacheKey];

		// Noses from eNose DB
        $creators = Mage::getModel('nose/creator')->getCreatorArrayFromProduct($product);

        $this->_storeCreator[$cacheKey] = $creators;
        
        return $creators;
    }
    
    
    
}