<?php
/**
 * Nose User Helper
 *
 *
 */
class Partikule_Nose_Helper_Customer extends Mage_Core_Helper_Abstract
{
	/**
	 * Returns the current customer group
	 *
	 * @return null
	 */
	public function getCustomerGroup()
	{
		$customer = Mage::getSingleton('customer/session')->getCustomer();

		$collection = Mage::getModel('customer/group')->getCollection();

		foreach($collection as $_group)
		{
			if ($customer->getGroupId() == $_group->getCustomerGroupId())
			{
				return $_group;
			}
		}

		return NULL;
	}


	/**
	 * Set one customer OR the current logged in Customer to one group
	 * @param      $group_code
	 * @param null $customer
	 *
	 * @return bool		TRUE 	if the customer has been changed to the asked group
	 *               	FALSE	if the customer's group remains the old one
	 */
	public function setCustomerGroup($group_code, $customer=NULL)
	{
		if (is_null($customer))
			$customer = Mage::getSingleton('customer/session')->getCustomer();

		$set_group_id = NULL;

		if ($customer)
		{
			$collection = Mage::getModel('customer/group')->getCollection();

			foreach($collection as $_group)
			{
				if ($_group->getCode() == $group_code)
				{
					$set_group_id = $_group->getCustomerGroupId();

					if ($customer->getGroupId() != $_group->getCustomerGroupId())
					{
						$customer->setGroupId($_group->getCustomerGroupId())->save();
					}
					break;
				}
			}
		}

		if ($customer->getGroupId() == $set_group_id)
			return TRUE;

		return FALSE;

	}



}
