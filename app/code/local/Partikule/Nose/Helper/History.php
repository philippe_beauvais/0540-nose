<?php
	/**
	 * Nose Helper
	 * Get Boutique's Orders History data from eNose
	 *
	 * @author      Partikule <http://www.partikule.net>
	 *
	 */
class Partikule_Nose_Helper_History extends Mage_Core_Helper_Abstract
{
	/**
	 * @return array	Array of orders
	 */
	public function getOrders()
	{
		$orders = Mage::getModel('nose/order')->getOrdersFromCustomer();

		return $orders;
	}
}

