<?php
/**
 * Nose Helper
 * Coupon infos
 *
 * @author      Partikule <http://www.partikule.net>
 * @created		2012.11.26
 */
class Partikule_Nose_Helper_Coupon extends Mage_Core_Helper_Abstract
{

	/**
	 * Get the coupon code linked to the given order ID.
	 *
	 * @param $orderId
	 *
	 * @return mixed|null
	 */
	public function getCouponCodeFromOrderId($orderId)
	{
		$coupon_code = NULL;

		if ($orderId)
		{
			// Get the Amasty Module custom attributes
			// Get the Coupon code
			$attributes = Mage::getModel('amorderattr/attribute');
			$attributes->load($orderId, 'order_id');
			$_coupon_code = $attributes->getData('coupon_code');

			// Get the coupon usage : Used ? Not used ?
			$coupon = Mage::getModel('salesrule/coupon');
			$coupon->load($_coupon_code, 'code');

			if($coupon->getId())
			{
				$timesUsed = $coupon->getTimesUsed();
				if ($timesUsed == 0)
				{
					$coupon_code = $_coupon_code;
				}
			}
		}
		return $coupon_code;
	}

}

