<?php
/**
 * Cart Helper
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Nose_Helper_Cart extends Mage_Core_Helper_Abstract
{
	/**
	 * Return the real Cart subtotal (addition of full product prices)
	 *
	 */
	public function getCartSubtotal()
	{
		// Not good, the item has the HT price
		$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
		// $items = Mage::getModel("checkout/cart")->getItems();

		$price = 0;
		foreach($items as $_item)
		{
			$price += Mage::helper('checkout')->getPriceInclTax($_item) * $_item->getQty();
		}
		return $price;
	}


}