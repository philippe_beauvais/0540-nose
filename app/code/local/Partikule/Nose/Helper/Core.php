<?php
/**
 * Core Helper
 *
 * Centralize most of the Nose / Magento core data retrieve.
 *
 * @author      Partikule <http://www.partikule.net>
 *
 */
class Partikule_Nose_Helper_Core extends Mage_Core_Helper_Abstract
{

	/**
	 * Returns the Magento current store code
	 *
	 * @return 	string		Magento Store Code
	 *
	 */
	public function getCurrentStoreCode()
	{
		return Mage::app()->getStore()->getCode();
	}
	
	/**
	 * Returns the Magento base URL
	 *
	 * @param	boolean		Include Store Code, Default to FALSE
	 *
	 * @return 	string		URL. http://your_domain.com
	 *
	 */
	public function getBaseUrl($storeCode = FALSE)
	{
		if ($storeCode)
			return Mage::getBaseUrl();
		else
		{
			if (Mage::getStoreConfig('web/secure/use_in_frontend') == '0')
				return Mage::getStoreConfig('web/unsecure/base_url');
			else
				return Mage::getStoreConfig('web/secure/base_url');
		}	
	}
	
}