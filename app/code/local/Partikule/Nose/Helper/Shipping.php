<?php
/**
 * Nose Helper
 *
 * @author      Partikule <http://www.partikule.net>
 * @created		2012.11.26
 */
class Partikule_Nose_Helper_Shipping extends Mage_Core_Helper_Abstract
{

	/**
	 * Returns the shipping company real tracking URL
	 * from one given order
	 *
	 * @param $order
	 *
	 * @return null|string
	 */
	public function getOrderTrackingUrl($order)
	{
		$url = NULL;

		if ($order)
		{
			$tracks = $order->getTracksCollection();
			foreach ($tracks as $track)
			{
				$trackingInfo = $track->getNumberDetail();

				if ($trackingInfo)
				{
					$number = $trackingInfo->getTracking();
					$url = $trackingInfo->getStatus();
					if (preg_match('/href="([^"]*)"/i', $url , $regs))
					{
						$url = $regs[1];
						$url .= $number;
					}
				}
			}
		}
		return $url;
	}


	/**
	 * Returns the shipping company real tracking URL
	 * from one given order ID
	 *
	 * @param $orderId
	 *
	 * @return null|string
	 */
	public function getOrderTrackingUrlFromOrderId($orderId)
	{
		$order = Mage::getModel('sales/order')->load($orderId);

		return $this->getOrderTrackingUrl($order);
	}
}
