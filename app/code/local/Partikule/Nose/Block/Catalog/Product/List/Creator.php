<?php
/**
 *
 */


/**
 * Product list
 *
 */
class Partikule_Nose_Block_Catalog_Product_List_Creator extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     *
     */
    public function getCategoryProductCollection()
    {
    	$collection = Mage::getModel('nose/creator')->getCategoryProductCollection();

    	return $collection;
    }
}
