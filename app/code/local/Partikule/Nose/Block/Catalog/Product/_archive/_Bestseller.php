<?php
/**
 * Partikule
 *
 * Everything's takken from Mage_Catalog_Block_Product_New but the getProductCollection() method
 *
 * @layout : 	nose/default/layout/nose.xml
 * @handler : 	nose_product_getnewproducts
 * 
 * @usage :		template/nose/brand/product_new_list.phtml
 *
 */


class Partikule_Nose_Block_Catalog_Product_Bestseller extends Mage_Catalog_Block_Product_Abstract
{
	protected $productCount = 20;


	/**
	 * Returns the best sellers products collection of one category
	 * If no category is given, returns the complete product list from the category
	 * 
	 * @param		Mixed : Category ID or Mage_Catalog_Model_Category			Optional. Category to get the attributes values from.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getProductCollection($category = NULL, $limit_to_configurable = FALSE)
	{
		$storeId = Mage::app()->getStore()->getId();

		if ( ! is_null($category) && is_int(intval($category)))
		{
			$category = Mage::getModel('catalog/category')->load($category);
			
			$collection = $category->getProductCollection()->setStoreId( $storeId );
		}
		else
		{
			$collection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
			Mage::getModel('catalog/layer')->prepareProductCollection( $collection );
		}
		
		$collection->addAttributeToSelect('*')
				->addOrderedQty()
				->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', 4)
				->addAttributeToFilter(array(array('attribute'=>'sku', 'nlike'=>'S-%')))
				->addStoreFilter($storeId)
				->setOrder('ordered_qty', 'desc')
				->setPageSize($this->getProductsCount())
				->setCurPage(1)
		;
		
		// Adds the category URL to each item of the collection
		$collection = Mage::getModel('boutique/Category')->setCategoryData($collection);

        // Join to perfume_detail to get the perfume details
        // Not used for the moment, but keep it here
        // 
		// $collection = Mage::getModel('nose/Perfume')->joinToPerfumeDetail($collection);
		
		return $collection

/*
	Could be used ?
	Perhaps instead of "->addAttributeToFilter('visibility', 4)"
	but too much bored by the lack of Magr documentation to have a deeper look....
	
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
		$productCollection->setPageSize(6)->setCurPage(1);
*/

		
	}


	
	
	/**
	 * Returns the new products collection, filterded by attributes
	 * 
	 * @param		Array		Array of attributes to filter on.
	 * @param		Mixed : Category ID or Mage_Catalog_Model_Category			Optional. Category to get the attributes values from.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getFilteredProductCollection($attributes, $category = NULL, $limit_to_configurable = FALSE)
	{
		$productCollection = $this->getProductCollection($category, $limit_to_configurable);

		foreach($attributes as $key => $value)
		{
			$productCollection->addAttributeToFilter($key, $value);
		}
		
		// Mage::log("{$productCollection->getSelect()}".PHP_EOL, null, 'partikule.log');
		
		return $productCollection;
	}
	
	
	/**
	 * Return limit
	 *
	 */
	public function getProductCount()
	{
		return $this->productCount;
	}
	
	
}