<?php
/**
 * Partikule
 *
 * Everything's taken from Mage_Catalog_Block_Product_New but the getProductCollection() method
 *
 * @layout : 	nose/default/layout/nose.xml
 * @handler : 	nose_product_getnewproducts
 * 
 * @usage :		template/nose/brand/product_new_list.phtml
 *				template/boutique/product_new_list.phtml
 */

class Partikule_Nose_Block_Catalog_Product_New extends Mage_Catalog_Block_Product_New
{

	/**
	 * Returns the new products collection of one category
	 * If no category is given, returns the complete new product list
	 * 
	 * @param		Array		Attributes to filter on.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getProductCollection($attributes = array(), $limit_to_configurable = TRUE)
	{

		$collection = Mage::helper('nose/Product')->getNewProductCollection( $this->getProductsCount() );
		
		// Filter on attributes
		foreach($attributes as $key => $value)
			$collection->addAttributeToFilter($key, $value);

		// Limits to configurable
		if ($limit_to_configurable == TRUE)
			$collection->addAttributeToFilter('type_id', 'configurable');

//		Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'partikule.log');

		return $collection;
	}
	
	
	/**
	 * Returns the new products collection, filterded by attributes
	 * 
	 * @param		Array		Array of attributes to filter on.
	 *
	 * @return 		Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 *
	 */
	public function getFilteredProductCollection($attributes, $limit_to_configurable = FALSE)
	{
		$productCollection = $this->getProductCollection($limit_to_configurable);

		foreach($attributes as $key => $value)
		{
			$productCollection->addAttributeToFilter($key, $value);
		}
		
		// Mage::log("{$productCollection->getSelect()}".PHP_EOL, null, 'partikule.log');
		
		return $productCollection;
	}
	

	/**
	 * Partikule : 
	 * As the collectyion isn't got here, 
	 * Replace this method with one simplier one...
	 *
	 */
	protected function _beforeToHtml()
	{
        return $this;
	}

	
}