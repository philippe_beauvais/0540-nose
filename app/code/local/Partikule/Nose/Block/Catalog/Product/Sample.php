<?php

/**
 * Product Sample
 *
 * @author     Partikule
 */
class Partikule_Nose_Block_Catalog_Product_Sample extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

	
	
	/**
	 * Returns on Sample product if it exists
	 *
	 */
	public function getProduct()
	{
		// Master Product SKU = 'M-' + Perfume ID
		// $sku = 'S-' . $this->getRequest()->getPost('pid');

		$sample = NULL;
		$product = Mage::registry('product');
		
		// ID of the Master product (eNose side)
		// $master_id = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);

		$simpleProducts = $product->getTypeInstance()->getUsedProducts();

		if($simpleProducts)
		{
			foreach($simpleProducts as $simple)
			{
				if (substr($simple->getSku(), 0,1) == 'S')
				{
					// Mage::log('Got SKU : ' . $simple->getSku(), null, 'partikule.log');

					$sample = $simple;
					break;
				}
			}
		}

		// Mage::log($sample, null, 'partikule.log');

		// $sample_sku = 'S-' . $master_id;
		// $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sample_sku);

		return $sample;
	}

	/*
	 * Don't work with LS, as LS doesn't allow one other sample set than LIGHTPEED_ROOT...
	 * chic...
	 *
	public function getProduct()
	{
		// Sample (Echantillons) attribute SET
		$attribute_set_name = Mage::getStoreConfig('nose/settings/sample_products_attribute_set');

		$attributeSetId = Mage::getModel('eav/entity_attribute_set')
			->load($attribute_set_name, 'attribute_set_name')
			->getAttributeSetId();

		// Registered product
		$product = Mage::registry('product');

		$collection = Mage::getModel('catalog/product')
			->getCollection()
			->addAttributeToSelect('*')
			->addFieldToFilter('attribute_set_id', $attributeSetId)
			->addFieldToFilter('perfume_id', $product->getPerfumeId())
		;

		$_product = $collection->getFirstItem();
		
		return $_product;
	}
*/


	
}
