<?php

/**
 * Block to render Product's Creators List
 *
 * @usage		In template :
 *				echo $this->getLayout()
 * 						  ->createBlock('nose/catalog_product_widget_creator')
 * 						  ->setProduct( $_product )
 * 						  ->toHtml();
 *
 */
class Partikule_Nose_Block_Catalog_Product_Widget_Creator extends Mage_Customer_Block_Widget_Abstract
{
    /**
     * Initialize block
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('catalog/product/widget/creator.phtml');
    }
	
	
	/**
	 * Returns Creators List Array
	 * the "product" var is set by the view when the widget is called
	 *
	 * Called in the widget template
	 *
	 */
	public function getCreators()
	{
		return Mage::helper('nose/creator')->getCreatorArrayFromProduct($this->getProduct());
	}
	
	
	/**
	 * Returns the perfume linked to the product
	 * the "product" var is set by the view when the widget is called
	 *
	 * Called in the widget template
	 *
	 */
	public function getPerfume()
	{
		return Mage::helper('nose/perfume')->getPerfumeDataFromProduct($this->getProduct());
	}
}
