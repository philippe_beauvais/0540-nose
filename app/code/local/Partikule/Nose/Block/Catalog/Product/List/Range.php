<?php
/**
 */


/**
 * Catalog product range items block
 *
 * @author     Partikule Studio
 */
class Partikule_Nose_Block_Catalog_Product_List_Range extends Mage_Catalog_Block_Product_Abstract
{
    protected $_itemCollection;

    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */
		

/*
		if ($product->getProductRange())
		{
*/
			$storeId = Mage::app()->getStore()->getId();
			$this->_itemCollection = Mage::getModel('catalog/product')->getCollection()->setStoreId( $storeId );
			Mage::getModel('catalog/layer')->prepareProductCollection($this->_itemCollection);

			$this->_itemCollection->addAttributeToFilter('product_range', $product->getProductRange());

			// Adds the category URL to each item of the collection
			$this->_itemCollection = Mage::getModel('boutique/Category')->setCategoryData($this->_itemCollection);
	
			// Mage::log("{$collection->getSelect()}".PHP_EOL, null, 'sql.log');
	
			// Limits to configurable
			$this->_itemCollection->addAttributeToFilter('type_id', 'configurable');

	        $this->_itemCollection->load();

/* 		} */
			
		
        return $this;
    }

    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    public function getItems()
    {
        return $this->_itemCollection;
    }
}
