<?php
/**
 * Merges Billing and Shipping blocks in one unique block
 *
 */

/**
 * One page checkout status
 *
 * @category   Mage
 * @category   Mage
 * @package	   Mage_Checkout
 * @author		Magento Core Team <core@magentocommerce.com>
 */
class Partikule_Nose_Block_Checkout_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing
{
	/**
	 * Sales Qoute Billing Address instance
	 *
	 * @var Mage_Sales_Model_Quote_Address
	 */
	protected $_address;

    /**
     * Sales Qoute Shipping Address instance
     *
     * @var Mage_Sales_Model_Quote_Address
     */
    protected $_shipping_address = null;


	/**
	 * Initialize billing address step
	 *
	 */
	protected function _construct()
	{
		$this->getCheckout()->setStepData('billing', array(
			'label'		=> Mage::helper('checkout')->__('Addresses'),
			'is_show'	=> $this->isShow()
		));

		if ($this->isCustomerLoggedIn()) {
			$this->getCheckout()->setStepData('billing', 'allow', true);
		}
		parent::_construct();
	}
	
	
    /**
     * Return Sales Quote Address model (shipping address)
     *
     * Partikule : Copied from Shipping so Shipping address can be on Billing form
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getShippingAddress()
    {
        if (is_null($this->_shipping_address)) {
            if ($this->isCustomerLoggedIn()) {
                $this->_shipping_address = $this->getQuote()->getShippingAddress();
            } else {
                $this->_shipping_address = Mage::getModel('sales/quote_address');
            }
        }

        return $this->_shipping_address;
    }
	
	
	
	
}
