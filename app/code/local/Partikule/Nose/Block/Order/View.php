<?php
/**
 */

/**
 * Sales order View block
 *
 */

class Partikule_Nose_Block_Order_View extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
		
		$this->setTemplate('nose/sales/view.phtml');

        $this->setOrder(Mage::registry('current_order'));

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
    }

    protected function _prepareLayout()
    {

        parent::_prepareLayout();
        
        // Active submenu

		
		// Partikule : 
		// Add of  ->setTemplate('page/html/pager.phtml');
		// Because the text "Item" doesn't fit to the orders list...
		// Not beautiful you know...
		/*
        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setTemplate('sales/order/pager.phtml')												// Yeeeeh ! Partikule's mod !
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
        */
    }
    
    
    public function getReorderUrl($order)
    {
        return $this->getUrl('nose/order/reorder', array('order_id' => $order['pos_invoice_id']));
    }

	public function getBackUrl()
	{
		return $this->getUrl('nose/order/history/');
	}

}
