<?php
/**
 *
 *
 */
class Partikule_Nose_Block_Adminhtml_System_Config_Bundlefeed extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	/**
	 * Set template to itself
	 */
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		if (!$this->getTemplate()) {
			$this->setTemplate('nose/system/config/bundlefeed.phtml');
		}
		return $this;
	}

	/**
	 * Unset some non-related element parameters
	 *
	 * @param Varien_Data_Form_Element_Abstract $element
	 * @return string
	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
		return parent::render($element);
	}
	 */

	/**
	 * Get the button and scripts contents
	 *
	 * @param Varien_Data_Form_Element_Abstract $element
	 * @return string
	 */
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$originalData = $element->getOriginalData();
        $bundleId = Mage::getStoreConfig($originalData['bundle']);
		$this->addData(array(
			'button_label' => $originalData['button_label'],
			'button_url'   => $this->getUrl($originalData['button_url'], array('bundle_id' => $bundleId)),
			'html_id' => $element->getHtmlId(),
		));
		return $this->_toHtml();
	}
}
