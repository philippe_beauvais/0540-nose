<?php
/**
 * Partikule Nose
 *
 * @date        2017/11/19
 * @category    Partikule
 * @package     Partikule_Nose
 * @author      Partikule Dev Team <dev@partikule.net>
 * @copyright   Partikule (http://partikule.net)
 *
 */

$installer = $this;
$installer->startSetup();

$installer->removeAttribute('customer', 'term_of_use' );


/**
 * Customer attribute : term_of_use
 * If yes / no the customer has accepted the terms of use
 *
 */
$custEntityTypeId = $installer->getEntityTypeId('customer');
$custAttributeSetId = $installer->getDefaultAttributeSetId($custEntityTypeId);
$custAttributeGroupId = $installer->getDefaultAttributeGroupId($custEntityTypeId, $custAttributeSetId);


$installer->addAttribute(
    $custEntityTypeId,
    'term_of_use',
    array(
        'label' => 'Terms of Use',
        'input' => 'boolean',
        'type' => 'int',
        'required' => 0,
        'visible' => 1,
        'visible_on_front' => 1,
        'global' => 1,
        'user_defined' => 1,
        'sort_order' => 1001,
        'position' => 1001,
        'unique' => 0,
        'used_in_forms' => array(
            'adminhtml_customer',
        ),
        'comment' => 'Has the user accepted the Terms on Use of Nose.fr ? 0 = No, 1 = Yes.'
    )
);

$installer->addAttributeToGroup($custEntityTypeId, $custAttributeSetId, $custAttributeGroupId, 'term_of_use', 0);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'term_of_use');
$oAttribute->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create'));
$oAttribute->save();



$installer->endSetup();
