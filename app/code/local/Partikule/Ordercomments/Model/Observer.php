<?php


class Partikule_Ordercomments_Model_Observer
{
    public function setOrderComment(Varien_Event_Observer $observer)
    {
        $_order = $observer->getEvent()->getOrder();
        $_request = Mage::app()->getRequest();

        /*
         * Send by opcheckout.js : Review->save()
         *
         */
        $_comments = strip_tags($_request->getParam('comments'));

		/*
		Mage::log($quote->getData(), null, 'partikule.log');
		Mage::log($_request->getPost(), null, 'partikule.log');
		*/

/*
        if(!empty($_comments)){
            $_order->setComments($_comments);
        }
*/


        if(!empty($_comments)){
            $_order->setCustomerNote('<strong>Extra Instructions:</strong> ' .$_comments);            
        }  

        return $this;
    }
}

