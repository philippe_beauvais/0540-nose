<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Layerednavigation
 * @version    1.5.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Layerednavigation_Helper_Layer extends Mage_Core_Helper_Data
{
    const SOLR_FACET_LIMIT = 100;

    protected $_initEnterpriseSearchCollection = false;

    /**
     * @param Enterprise_Search_Model_Search_Layer $layer
     *
     */
    public function initEnterpriseSearchLayerProductCollection($layer)
    {
        if (!$this->_initEnterpriseSearchCollection) {
            $productCollection = $layer->getProductCollection();
            if ($productCollection instanceof Enterprise_Search_Model_Resource_Collection) {
                $storeId = $productCollection->getStoreId();
                $queryText = $productCollection->getExtendedSearchParams()['query_text'];
                $entityIds = $this->_getEntityIdsFromEnterpriseSearch($storeId, $queryText);
                $productCollection->getSelect()->where('e.entity_id IN (?)', $entityIds);
            }
            $this->_initEnterpriseSearchCollection = true;
        }
    }

    /**
     * @param mixed $storeId
     * @param mixed $queryText
     * @return array
     */
    protected function _getEntityIdsFromEnterpriseSearch($storeId, $queryText)
    {
        $entityIds = array();
        $_collection = $this->_getEnterpriseSearchProductCollection();
        $_collection->setStoreId($storeId)
            ->addSearchFilter($queryText);
        $_pages = ceil( $_collection->getSize() / self::SOLR_FACET_LIMIT );

        for ($i = 1; $i <= $_pages; $i++) {
            $_collection = $this->_getEnterpriseSearchProductCollection();
            $_collection->setStoreId($storeId)
                ->addSearchFilter($queryText)
                ->setPage($i, self::SOLR_FACET_LIMIT)
                ->load();
            $entityIds = array_merge($entityIds, $_collection->getLoadedIds());
        }
        return $entityIds;
    }

    /**
     * @param Enterprise_Search_Model_Resource_Collection $productCollection
     *
     * @return Enterprise_Search_Model_Resource_Collection
     */
    public function getRemovedPriceFilterEnterpriseSearchCollection($productCollection) {
        return $this->getRemovedFieldFilterEnterpriseSearchCollection($productCollection, 'price');
    }

    /**
     * @param Enterprise_Search_Model_Resource_Collection $productCollection
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return mixed
     */
    public function getRemovedFieldFilterEnterpriseSearchCollection($productCollection, $attribute) {
        $_collection = $this->_getEnterpriseSearchProductCollection();
        $_collection->setStoreId($productCollection->getStoreId());
        foreach ($productCollection->getExtendedSearchParams() as $param => $value) {
            if ($param == 'query_text') {
                $_collection->addSearchFilter($value);
            }
            else if ($param != Mage::getResourceSingleton('enterprise_search/engine')->getSearchEngineFieldName($attribute)) {
                $_collection->addFqFilter(array($param => $value));
            }
        }
        $_collection->getSelect()->setPart(
            Zend_Db_Select::FROM,
            $productCollection->getSelect()->getPart(Zend_Db_Select::FROM)
        );
        return $_collection->load();
    }

    protected function _getEnterpriseSearchProductCollection() {
        return Mage::helper('catalogsearch')->getEngine()->getResultCollection();
    }
}