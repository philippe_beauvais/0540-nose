<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Layerednavigation
 * @version    1.5.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Layerednavigation_Model_Filter_Type_Yesno_Attribute extends AW_Layerednavigation_Model_Filter_Type_Abstract
{
    protected $_customFiltersList = array(
        AW_Layerednavigation_Model_Source_Yesno::ON_SALE_CODE,
        AW_Layerednavigation_Model_Source_Yesno::IN_STOCK_CODE
    );

    public function apply(Zend_Controller_Request_Abstract $request)
    {
        $this->_currentValue = array();

        $value = $request->getParam($this->getFilter()->getCode(), null);
        if (null === $value) {
            return $this;
        }
        $value = explode(',', $value);

        $optionCollection = Mage::getModel('aw_layerednavigation/filter_option')->getCollection();
        $optionCollection->addFieldToFilter('option_id', array('in' => $value));
        $valueList = array();
        foreach ($optionCollection as $optionItem) {
            $valueList[] = $optionItem->getData('additional_data/value');
            $this->_currentValue[] = $optionItem;
        }

        if (count($valueList) <= 0) {
            return $this;
        }

        /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection */
        $collection = $this->getFilter()->getLayer()->getProductCollection();

        if (in_array($this->getFilter()->getCode(), $this->_customFiltersList)) {
            if ($this->getFilter()->getCode() == AW_Layerednavigation_Model_Source_Yesno::IN_STOCK_CODE) {
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            } else {
                $collection->getSelect()->where('price_index.final_price < price_index.price');
            }
        } else {
            $connection = Mage::getSingleton('core/resource')->getConnection('read');
            $attribute = Mage::getModel('catalog/resource_eav_attribute')
                ->loadByCode(
                    Mage_Catalog_Model_Product::ENTITY,
                    $this->getFilter()->getData('additional_data/attribute_code')
                );

            $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());

            $conditions = array(
                "{$tableAlias}.entity_id = e.entity_id",
                $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
                $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId()),
                $connection->quoteInto("{$tableAlias}.value IN (?)", $valueList)
            );

            $tableName = Mage::getSingleton('core/resource')->getTableName('aw_layerednavigation/filter_index_yesno');
            $collection->getSelect()->join(
                array($tableAlias => $tableName),
                implode(' AND ', $conditions),
                array()
            );
        }
        $collection->getSelect()->distinct();
        if ($collection instanceof Enterprise_Search_Model_Resource_Collection) {
            $collection->addFqFilter(array('id' => $collection->getAllIds()));
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        if ($this->_count === null) {
            $collection = $this->getFilter()->getLayer()->getProductCollection();
            if (method_exists($collection,'getFoundIds')) {
                $foundIds = $collection->getFoundIds();
                if (!empty($foundIds)) {
                    $collection->addIdFilter($foundIds);
                } else {
                    $collection->getSelect()->orWhere('FALSE');
                }
            }

            // clone select from collection with filters
            /** @var Zend_Db_Select $select */
            $select = clone $collection->getSelect();
            // reset columns, order and limitation conditions
            $select->reset(Zend_Db_Select::COLUMNS);
            $select->reset(Zend_Db_Select::ORDER);
            $select->reset(Zend_Db_Select::GROUP);
            $select->reset(Zend_Db_Select::LIMIT_COUNT);
            $select->reset(Zend_Db_Select::LIMIT_OFFSET);

            $connection = Mage::getSingleton('core/resource')->getConnection('read');
            if (in_array($this->getFilter()->getCode(), $this->_customFiltersList)) {
                $select = $this->_addCustomFiltersCount($this->getFilter()->getCode(), $select);
            } else {
                $attribute = Mage::getModel('catalog/resource_eav_attribute')
                    ->loadByCode(
                        Mage_Catalog_Model_Product::ENTITY,
                        $this->getFilter()->getData('additional_data/attribute_code')
                    );
                $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());

                $conditions = array(
                    "{$tableAlias}.entity_id = e.entity_id",
                    $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
                    $connection->quoteInto("{$tableAlias}.store_id = ?", $this->getFilter()->getStoreId()),
                );

                $fromPart = $select->getPart(Zend_Db_Select::FROM);
                if (array_key_exists($tableAlias, $fromPart)) {
                    unset($fromPart[$tableAlias]);
                    $select->setPart(Zend_Db_Select::FROM, $fromPart);
                }

                $tableName = Mage::getSingleton('core/resource')->getTableName('aw_layerednavigation/filter_index_yesno');
                $select
                    ->join(
                        array($tableAlias => $tableName),
                        join(' AND ', $conditions),
                        array(
                            'value',
                            'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")
                        )
                    )
                    ->group("{$tableAlias}.value");
            }
            $countList = $connection->fetchPairs($select);

            $optionCollection = $this->getFilter()->getOptionCollection()->addIsEnabledFilter();
            $result = array();
            foreach ($optionCollection as $optionItem) {
                $valueOptionId = $optionItem->getData('additional_data/value');
                if (array_key_exists($valueOptionId, $countList)) {
                    $result[$optionItem->getId()] = $countList[$valueOptionId];
                }
            }
            $this->_count = $result;
        }
        return $this->_count;
    }

    protected function _addCustomFiltersCount($code, $select)
    {
        if ($code == AW_Layerednavigation_Model_Source_Yesno::IN_STOCK_CODE) {
            $tableAlias = sprintf('%s_idx', AW_Layerednavigation_Model_Source_Yesno::IN_STOCK_CODE);

            $conditions = array(
                "{$tableAlias}.use_config_manage_stock = 0 AND {$tableAlias}.manage_stock=1 " .
                "AND {$tableAlias}.is_in_stock=1",
                "{$tableAlias}.use_config_manage_stock = 0 AND {$tableAlias}.manage_stock=0"
            );
            $manageStock = Mage::getStoreConfig(Mage_CatalogInventory_Model_Stock_Item::XML_PATH_MANAGE_STOCK);
            if ($manageStock) {
                $conditions[] = "{$tableAlias}.use_config_manage_stock = 1 AND {$tableAlias}.is_in_stock=1";
            } else {
                $conditions[] = "{$tableAlias}.use_config_manage_stock = 1";
            }

            $fromPart = $select->getPart(Zend_Db_Select::FROM);
            if (array_key_exists($tableAlias, $fromPart)) {
                unset($fromPart[$tableAlias]);
                $select->setPart(Zend_Db_Select::FROM, $fromPart);
            }

            $tableName = Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item');
            $countColumn = new Zend_Db_Expr(0);
            if (Mage::helper('aw_layerednavigation')->canDislapyStockFilter()) {
                $countColumn = new Zend_Db_Expr("COUNT({$tableAlias}.product_id)");
            }
            $select
                ->join(
                    array($tableAlias => $tableName),
                    "{$tableAlias}.product_id = e.entity_id AND (" . '(' . join(') OR (', $conditions) . '))',
                    array(
                        'in_stock_value' => new Zend_Db_Expr(1),
                        'count' => $countColumn
                    )
                )
                ->group('in_stock_value');
        } else {
            $select
                ->columns(array(
                    'on_sale_value' => new Zend_Db_Expr(1),
                    'count' => new Zend_Db_Expr('COUNT(e.entity_id)')
                ))
                ->where('price_index.final_price < price_index.price')
                ->group('on_sale_value');
            ;
        }

        return $select;
    }
}