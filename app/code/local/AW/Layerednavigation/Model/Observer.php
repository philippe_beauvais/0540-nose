<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Layerednavigation
 * @version    1.5.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Layerednavigation_Model_Observer
{
    const TRIGGER_REQUEST_PARAM = 'aw_layerednavigation';

    public function controllerFrontInitBefore($observer)
    {
        if (Mage::helper('aw_layerednavigation/config')->isEnabled() &&
            !((@class_exists('AW_Mobile2_Helper_Data') &&
                    (AW_Mobile2_Helper_Data::isIphoneTheme() || AW_Mobile2_Helper_Data::isIPadTheme()))
                || (@class_exists('AW_Mobile3_Helper_Data') &&
                    (AW_Mobile3_Helper_Data::isIphoneTheme() || AW_Mobile3_Helper_Data::isIPadTheme()))
            )
        ) {
            return $this;
        }
        $config = Mage::getConfig();

        $node = $config->getNode('global/blocks/catalog/rewrite');
        unset($node->layer_view);
        unset($node->category_view);
        $node = $config->getNode('global/blocks/catalogsearch/rewrite');
        unset($node->layer);
        $node = $config->getNode('global/blocks/enterprise_search/rewrite');
        unset($node->catalog_layer_view);
        $node = $config->getNode('global/blocks/enterprise_search/rewrite');
        unset($node->catalogsearch_layer);
        $node = $config->getNode('global/blocks/awadvancedsearch/rewrite');
        unset($node->catalogsearch_layer);
        $node = $config->getNode('global/blocks/awshopbybrand/rewrite');
        unset($node->layer_view);
        $node = $config->getNode('global/blocks/awshopbybrand/rewrite');
        unset($node->brand);
        $node = $config->getNode('global/models/catalog/rewrite');
        unset($node->config);

        return $this;
    }

    public function productListCollection($observer)
    {
        $request = Mage::app()->getFrontController()->getRequest();
        $sortOrder = $request->getParam('order');
        if (is_string($sortOrder) && $sortOrder == 'rating') {
            $collection = $observer->getCollection();
            $collection->joinField(
                'rating',
                'review/review_aggregate',
                'rating_summary',
                'entity_pk_value = entity_id',
                array(
                    'entity_type' => Mage_Review_Model_Review::ENTITY_PRODUCT,
                    'store_id' => Mage::app()->getStore()->getId()
                ),
                'left'
            );
            $dir = (strtoupper($request->getParam('dir')) == Zend_Db_Select::SQL_ASC)
                ? Zend_Db_Select::SQL_ASC
                : Zend_Db_Select::SQL_DESC
            ;

            $collection->getSelect()->order('rating ' . $dir);
        }

        return $this;
    }

    public function beforeRenderLayout($observer)
    {
        $request = Mage::app()->getFrontController()->getRequest();
        if (!$request->getParam(self::TRIGGER_REQUEST_PARAM, false)) {
            return $this;
        }
        $request->setParam(self::TRIGGER_REQUEST_PARAM, false);
        $requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = str_replace(self::TRIGGER_REQUEST_PARAM . '=1', '', $requestUri);
        $_SERVER['REQUEST_URI'] = substr_replace($requestUri, '', -1);

        $layout = Mage::app()->getFrontController()->getAction()->getLayout();
        $blockList = array(
            'success'   => true,
        );
        try {
            $blockList['block'] = array(
                'layer'     => $this->_getLayerHtml($layout),
                'catalog'   => $this->_getCatalogHtml($layout)
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $blockList['success'] = false;
        }

        $this->_disableRenderingPageNativeBlocks();

        $this->_sendResponse(
            Zend_Json::encode($blockList)
        );
        return $this;
    }

    public function coreSessionAddMessage()
    {
        if (Mage::getSingleton('adminhtml/session')->getData('aw_ln_index_bycron')) {
            $newCode = Mage::helper('aw_layerednavigation')->__(
                'Layered Navigation by aheadWorks index will be rebuilt upon cron execution.'
            );
            $messages = Mage::getSingleton('adminhtml/session')->getMessages();
            $items = $messages->getItems(Mage_Core_Model_Message::SUCCESS);
            foreach ($items as $message) {
                if (strpos($message->getCode(), 'Layered Navigation by aheadWorks') !== false) {
                    $message->setIdentifier('aw_ln_change');
                    $messages->deleteMessageByIdentifier('aw_ln_change');
                    $messages->add(
                        Mage::getSingleton('core/message')->warning($newCode)
                    );
                }
                if (strpos($message->getCode(), 'index(es) have reindexed data') !== false) {
                    $count = intval(substr($message->getCode(), 9, 2));
                    if ($count != 1) {
                        $fixedCount = $count - 1;
                        $fixedCode = str_replace((string)$count, (string)$fixedCount, $message->getCode());
                        $message->setCode($fixedCode);
                    } else {
                        $message->setIdentifier('aw_ln_mass_change');
                        $messages->deleteMessageByIdentifier('aw_ln_mass_change');
                    }
                    $messages->add(
                        Mage::getSingleton('core/message')->warning($newCode)
                    );
                }
            }
            Mage::getSingleton('adminhtml/session')->unsetData('aw_ln_index_bycron');
        }
        return $this;
    }

    protected function _getLayerHtml($layout)
    {
        $blockNames = array(
            'catalog.leftnav',
            'catalogsearch.leftnav',
            'enterprisecatalog.leftnav',
            'enterprisesearch.leftnav',
            'advancedsearch.leftnav',
        );
        foreach ($blockNames as $name) {
            $block = $layout->getBlock($name);
            if (!$block) {
                continue;
            }
            return $block->toHtml();
        }
        return '';
    }

    protected function _getCatalogHtml($layout)
    {
        $blockNames = array(
            'category.products',
            'search.result',
            'brand',
        );
        foreach ($blockNames as $name) {
            $block = $layout->getBlock($name);
            if (!$block) {
                continue;
            }
            return $block->toHtml();
        }
        return '';
    }

    private function _sendResponse($html)
    {
        $response = Mage::app()->getResponse();
        $response->clearBody();
        $response->setHttpResponseCode(200);
        //remove location header from response
        $headers = $response->getHeaders();
        $response->clearHeaders();
        foreach ($headers as $header) {
            if ($header['name'] !== 'Location') {
                $response->setHeader($header['name'], $header['value'], $header['replace']);
            }
        }
        $response->setHeader('Content-type', 'application/json');
        $response->setBody($html);
    }

    private function _disableRenderingPageNativeBlocks()
    {
        foreach (Mage::getSingleton('core/layout')->getAllBlocks() as $block) {
            Mage::getSingleton('core/layout')->removeOutputBlock($block->getNameInLayout());
        }
    }
}