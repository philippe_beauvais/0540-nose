<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Layerednavigation
 * @version    1.5.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Layerednavigation_Model_Source_Filter_State
{
    const EXPANDED_CODE   = 0;
    const EXPANDED_LABEL  = "Expanded";

    const COLLAPSED_CODE  = 1;
    const COLLAPSED_LABEL = "Collapsed";

    const FILTER_VALUE_CODE  = 2;
    const FILTER_VALUE_LABEL = "Use filter value";

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::EXPANDED_CODE,
                'label' => Mage::helper('aw_layerednavigation')->__(self::EXPANDED_LABEL)
            ),
            array(
                'value' => self::COLLAPSED_CODE,
                'label' => Mage::helper('aw_layerednavigation')->__(self::COLLAPSED_LABEL)
            ),
            array(
                'value' => self::FILTER_VALUE_CODE,
                'label' => Mage::helper('aw_layerednavigation')->__(self::FILTER_VALUE_LABEL)
            )
        );
    }

    public function toOptionArrayFilter()
    {
        return array(
            array(
                'value' => self::EXPANDED_CODE,
                'label' => Mage::helper('aw_layerednavigation')->__(self::EXPANDED_LABEL)
            ),
            array(
                'value' => self::COLLAPSED_CODE,
                'label' => Mage::helper('aw_layerednavigation')->__(self::COLLAPSED_LABEL)
            ),
        );
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            self::EXPANDED_CODE  => Mage::helper('aw_layerednavigation')->__(self::EXPANDED_LABEL),
            self::COLLAPSED_CODE => Mage::helper('aw_layerednavigation')->__(self::COLLAPSED_LABEL),
        );
    }
}