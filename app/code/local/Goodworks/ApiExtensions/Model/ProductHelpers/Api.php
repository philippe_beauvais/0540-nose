<?php
class Goodworks_ApiExtensions_Model_ProductHelpers_Api extends Mage_Api_Model_Resource_Abstract
{
     /**
     * method Name
     *
     * @param string $orderIncrementId
     * @return string
     */
    public function methodName( $arg )
    {
        Mage::log("Goodworks_ApiExtensions_Model_ProductHelpers_Api: methodName called");
        $result = "Goodworks?  More like Greatworks." . $arg;
        return $result;
    }
    
    /**
     * 
     * @param string $master_product_id
     * @param array $child_products
     *      product_id => array(
     *              index=> array(
     *                  'attribute_id'=>,
     *                  'label'=>,
     *                  'value_index'=>,
     *                  'is_percent'=>0,
     *                  'pricing_value'=>'')
     * 
     * @param array $attribute_ids_to_use
     */
    public function addProductOptions($master_product_id, $child_products, $attribute_ids_to_use) {
        Mage::log("Goodworks_ApiExtensions_Model_ProductHelpers_Api: setOptionsForProduct called");
        
        Mage::log("$child_products");
        
        return 1;
        
        $master_product = Mage::getModel('catalog/product')
                ->load($master_product_id)
                ->getData();
        
        #if attributes to use not specified on product, then add them
        if (is_null($master_product->getUsedProductAttributeIds()) || 
                empty($master_product->getUsedProductAttributeIds())) {
            
            $master_product->setUsedProductAttributeIds($attribute_ids_to_use);
        }
        
/*
 * $child_products = array(
 *     '5791'=>array(
 *          '0'=> array(
 *                  'attribute_id'=>'491',
 *                  'label'=>'vhs',
 *                  'value_index'=>'5',
 *                  'is_percent'=>0,
 *                  'pricing_value'=>''),
            '1'=> array(
 *                  'attribute_id'=>'500',
 *                  'label'=>'English',
 *                  'value_index'=>'9',
 *                  'is_percent'=>0,
 *                  'pricing_value'=>'')
        ),
       '5792'=>array(
 *          '0'=> array(
 *                  'attribute_id'=>'491',
 *                  'label'=>'dvd',
 *                  'value_index'=>'6',
 *                  'is_percent'=>0,
 *                  'pricing_value'=>''),
 *          '1'=> array(
 *                  'attribute_id'=>'500',
 *                  'label'=>'English',
 *                  'value_index'=>'9',
 *                  'is_percent'=>0,
 *                  'pricing_value'=>'')
        ),
       '5807'=>array('0'=>
                array('attribute_id'=>'491','label'=>'dvd','value_index'=>'6','is_percent'=>0,'pricing_value'=>''),
               '1'=>
                 array('attribute_id'=>'500','label'=>'Spanish','value_index'=>'8','is_percent'=>0,'pricing_value'=>'')
        ),
       '5808'=>array('0'=>
                array('attribute_id'=>'491','label'=>'vhs','value_index'=>'6','is_percent'=>0,'pricing_value'=>''),
               '1'=>
                 array('attribute_id'=>'500','label'=>'Spanish','value_index'=>'8','is_percent'=>0,'pricing_value'=>'')
        )
    );
 */
        
        /*
            $configurable_attributes = array('0'=>array('id'=>NULL,'label'=>'Media Format','position'=> NULL,
                   'values'=>array('0'=>
                                            array('value_index'=>5,'label'=>'vhs','is_percent'=>0,
                                                    'pricing_value'=>'0','attribute_id'=>'491'),
                                        '1'=>
                                            array('value_index'=>6,'label'=>'dvd',
			                            'is_percent'=>0,'pricing_value'=>'0','attribute_id'=>'491')
		    ),
                    'attribute_id'=>491,'attribute_code'=>'media_format','frontend_label'=>'Media Format',
		    'html_id'=>'config_super_product__attribute_0'),
                   '1'=>array('id'=>NULL,'label'=>'Language','position'=> NULL,
                   'values'=>array('0'=>
                                            array('value_index'=>8,'label'=>'Spanish','is_percent'=>0,
                                                    'pricing_value'=>'0','attribute_id'=>'500'),
                                        '1'=>
                                            array('value_index'=>9,'label'=>'English',
			                            'is_percent'=>0,'pricing_value'=>'0','attribute_id'=>'500')
		    ),
                    'attribute_id'=>500,'attribute_code'=>'media_format','frontend_label'=>'Language',
		    'html_id'=>'config_super_product__attribute_1')
           );
         */
        
        $configurable_product_data = $master_product->getConfigurableProductsData();
	$master_product->setConfigurableProductsData($child_products);

        #$master_product->setConfigurableAttributesData($attributeData);
	#$master_product->setCanSaveConfigurableAttributes(true);
         
        try {
            $master_product->save();
            Mage::log("Configurable product data saved");
            return 1;
        }
        catch (Exception $e) {
            Mage::log("Configurable product data was not saved");
            Mage::log("Exception: $e", Zend_Log::ERR);
            return 0;
	} 
    }
}
?>