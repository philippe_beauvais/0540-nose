<?php
class Goodworks_ApiExtensions_Model_ProductHelpers_Api_v2 extends Mage_Api_Model_Resource_Abstract
{
    private function attachProductToConfigurable($childProductId, $masterProduct) {
        $loader = Mage::getResourceModel('catalog/product_type_configurable')
                ->load($masterProduct, $masterProduct->getId());

        $ids = $masterProduct->getTypeInstance()->getUsedProductIds(); 
        $newids = array();
        foreach ( $ids as $id ) {
            $newids[$id] = 1;
        }

        $newids[$childProductId] = 1;
        $loader->saveProducts($masterProduct, array_keys($newids));
    }
    
    private function setUsedAttributeOptions($masterProduct, $attributeOptions) {
        #if attributes to use not specified on product, then add them
        $usedAttributeIds = $masterProduct->getUsedProductAttributeIds();
        if (is_null($usedAttributeIds) || !$usedAttributeIds) {
            if (is_null($attributeOptions) || !$attributeOptions) {
                Mage::log("attributeOptions not set", null, "accumula.log");
            }
            else {
                $data = array();
                
                foreach ($attributeOptions as $attributeOption) {
                    $optionValues = array();
                    $valueIndex = 0;
                    foreach ($attributeOption->values as $opt) {
                        $optionValues[] = array(
                            'value_index'=>$valueIndex,
                            'label'=>$opt,
                            'is_percent'=>0,
                            'pricing_value'=>'0',
                            'attribute_id'=>$attributeOption->id
                        );
                        $valueIndex++;
                    }
                    
                    $data[] = array(
                        'id'=>NULL,
                        'label'=>$attributeOption->label,
                        'position'=>NULL,
                        'attribute_id'=>$attributeOption->id,
                        'attribute_code'=>$attributeOption->code,
                        'frontend_label'=>$attributeOption->label,
                        'values'=>$optionValues
                    );
                }
                /*
                $data = array(
                    '0'=>array(
                        'id'=>NULL,
                        'label'=>'Color',
                        'position'=> NULL,
                        'values'=>array(
                            '0'=>array(
                                'value_index'=>0,
                                'label'=>'Blue',
                                'is_percent'=>0,
                                'pricing_value'=>'0',
                                'attribute_id'=>'147'),
                            '1'=>array(
                                'value_index'=>1,
                                'label'=>'Green',
                                'is_percent'=>0,
                                'pricing_value'=>'0',
                                'attribute_id'=>'147')
                        ),
                        'attribute_id'=>147,
                        'attribute_code'=>'channels_color',
                        'frontend_label'=>'Color'),
                    '1'=>array(
                        'id'=>NULL,
                        'label'=>'Size',
                        'position'=>NULL,
                        'values'=>array(
                            '0'=>array(
                                'value_index'=>0,
                                'label'=>'Large',
                                'is_percent'=>0,
                                'pricing_value'=>'0',
                                'attribute_id'=>'148')
                            ),
                        'attribute_id'=>148,
                        'attribute_code'=>'channels_size',
                        'frontend_label'=>'Size')
                    );*/
                
                $masterProduct->setConfigurableAttributesData($data);
                $masterProduct->setCanSaveConfigurableAttributes(1);
                
                try {
                    $masterProduct->save();
                    Mage::log("Master product used attribute ids saved", null, "accumula.log");
                }
                catch (Exception $e) {
                    Mage::log("Master product used attribute ids were not saved", null, "accumula.log");
                    Mage::log("Exception: $e", null, "accumula.log");
                }   
            }
        }
    }
    
    /**
     * 
     * @param string $master_product_id
     * @param array $child_product_ids
     * @param array $attributeOptions
     * 
     * array(
     *  object (
     *      (string)label,
     *      (string)id,
     *      (string)code,
     *      (array of string)values
     *  )
     * )
     */
    public function addProductOptions($master_product_id, $child_product_ids, $attributeOptions) {
        Mage::log("Goodworks_ApiExtensions_Model_ProductHelpers_Api: setOptionsForProduct called", null, "accumula.log");
        $m= new Mage;
        $version=$m->getVersion();
        
        Mage::log("Running magento version $version", null, "accumula.log");
        
        if (is_null($master_product_id)) {
            Mage::log("master_product_id not set", null, "accumula.log");
            return 0;
        }
        
        Mage::log("Starting to get master product details", null, "accumula.log");
        $masterProduct = Mage::getModel('catalog/product')->load($master_product_id);
        
        Mage::log("Starting to set used attribute ids", null, "accumula.log");
        $this->setUsedAttributeOptions($masterProduct, $attributeOptions);
        
        Mage::log("Starting to set configurable products", null, "accumula.log");
        $result = $this->setConfigurableProducts($masterProduct, $child_product_ids, $version);
        
        Mage::log("Starting to set masterProduct stock status", null, "accumula.log");
        $stockStatus = Mage::getModel('cataloginventory/stock_status');
        $stockStatus->assignProduct($masterProduct);
        $stockStatus->saveProductStatus($masterProduct->getId(), 1);
        
        Mage::log("Processing complete", null, "accumula.log");
        $masterProduct->unsetData();
        return $result;
    }

    private function setConfigurableProducts($masterProduct, $childProductIds, $version) {
        if (is_null($childProductIds) || !$childProductIds) {
            Mage::log("childProducts not set", null, "accumula.log");
            return 0;
        }
        
        try {
            // add new simple product to configurable product
            foreach ($childProductIds as $cp) {
                $this->attachProductToConfigurable($cp, $masterProduct, $version);
            }
            return 1;
        }
        catch (Exception $e) {
            Mage::log("Configurable product data was not saved", null, "accumula.log");
            
            Mage::log("Exception: $e", null, "accumula.log");
            
            return 0;
	}   
    }
    
    public function getOrderTaxInfo($order_id) {
        Mage::log("Goodworks_ApiExtensions_Model_ProductHelpers_Api: getOrderTaxInfo called", null, "accumula.log");
        $m= new Mage;
        $version=$m->getVersion();
        
        Mage::log("Running magento version $version", null, "accumula.log");
        
        if (is_null($order_id)) {
            Mage::log("order_id not set", null, "accumula.log");
            return 0;
        }
        
        Mage::log("Starting to get order details", null, "accumula.log");
        $order = Mage::getModel('sales/order')->load($order_id);
        
        if (is_null($order)) {
            Mage::log("No order found with id $order_id", null, "accumula.log");
            return 0;
        }
        
        Mage::log("Starting to get tax details", null, "accumula.log");
        $tax_info = $order->getFullTaxInfo();
        $tax_info_string = print_r($tax_info, true);
        
        Mage::log("Found tax details: $tax_info_string", null, "accumula.log");
        
        $result = array();
        
        if ($tax_info) {
            $result = array(
                "amount" => $tax_info[0]["amount"],
                "label" => $tax_info[0]["id"],
                "percent" => $tax_info[0]["percent"]
            );
        }
        else {
            $result = null;
        }

        return $result;
    }
}
?>