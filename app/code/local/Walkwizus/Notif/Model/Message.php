<?php

class Walkwizus_Notif_Model_Message extends Partikule_Mynose_Model_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->_init('notif/message');
        $this->setConnection('mynose_read', 'mynose_write');
    }

    public function getMessages($customer)
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('mynose_read');

        $noseUser = Mage::getModel('mynose/user')->getNoseUserFromCustomer($customer);
        $notificationTable = Mage::getSingleton('core/resource')->getTableName('notif/message');

        $select = $read->select()
            ->from(array('n' => $notificationTable))
            ->where('n.id_user = ?', $noseUser['id_user'])
            ->where(new Zend_Db_Expr('n.dt_published<sysdate() AND sysdate()<ifnull(n.dt_unpublished,sysdate())'))
            ->where('n.notification_status != "Deleted"')
            ->order(new Zend_Db_Expr('
                CASE 
                WHEN notification_status="Not seen" THEN 1 
                WHEN notification_status="Seen" THEN 2
                WHEN notification_status="Read" THEN 3
                WHEN notification_status="Clicked" THEN 4
                ELSE 5 END
            ')
        )->limit(Mage::helper('notif')->getLimitMessage());

        $query = $select->query();
        $result = $query->fetchAll();

        return $result;
    }
}