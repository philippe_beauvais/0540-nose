<?php

class Walkwizus_Notif_Model_Resource_Message extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('notif/message', 'id_notification');
    }
}