<?php

class Walkwizus_Notif_Model_Resource_Distributor extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('notif/distributor', 'id_user');
        $this->_isPkAutoIncrement = false;
    }
}