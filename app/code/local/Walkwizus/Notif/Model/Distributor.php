<?php

class Walkwizus_Notif_Model_Distributor extends Partikule_Mynose_Model_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->_init('notif/distributor');
        $this->setConnection('mynose_read', 'mynose_write');
    }

    public function turnOffNotification($customer)
    {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('mynose_write');

        $noseUser = Mage::getModel('mynose/user')->getNoseUserFromCustomer($customer);
        $distributorTable = Mage::getSingleton('core/resource')->getTableName('notif/distributor');

        $idUser = $noseUser['id_user'];

        $write->query("UPDATE {$distributorTable} SET notification_unsubscribed = 1 WHERE id_user = $idUser AND distributor_id = 1");
    }
}