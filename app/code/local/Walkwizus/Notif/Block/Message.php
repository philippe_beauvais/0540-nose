<?php

class Walkwizus_Notif_Block_Message extends Mage_Core_Block_Template
{
    public function getMessages()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return false;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return Mage::getModel('notif/message')->getMessages($customer);
    }

    public function getState($message)
    {
        return str_replace(' ', '-', strtolower($message['notification_status']));
    }
}