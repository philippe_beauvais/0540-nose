<?php

class Walkwizus_Notif_AjaxController extends Mage_Core_Controller_Front_Action
{
    private $_status = array(
        'not_seen' => 'Not seen',
        'seen' => 'Seen',
        'read' => 'Read',
        'clicked' => 'Clicked',
        'deleted' => 'Deleted',
    );

    protected function _renderHtmlContent()
    {

    }

    public function messagesAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    public function openMessagesAction()
    {
        $messages = $this->getRequest()->getParam('messages');
        if (is_array($messages)) {
            foreach ($messages as $message) {
                try {
                    $m = Mage::getModel('notif/message')->load($message);
                    if ($m->getNotificationStatus() == $this->_status['not_seen']) {
                        $m->setNotificationStatus($this->_status['seen']);
                        $m->setDtSeen(date('Y-m-d H:i:s'));
                        $m->save();
                    }
                } catch (Exception $e) {
                    $this->getResponse()->setBody($e->getMessage());
                }
            }
        }
    }

    public function markAllAsReadAction()
    {
        $messages = $this->getRequest()->getParam('messages');
        if (is_array($messages)) {
            foreach ($messages as $message) {
                try {
                    $m = Mage::getModel('notif/message')->load($message);
                    $currentStatus = $m->getNotificationStatus();
                    if ($currentStatus != $this->_status['read'] && $currentStatus != $this->_status['deleted']) {
                        $m->setNotificationStatus($this->_status['read']);
                        $m->setDtRead(date('Y-m-d H:i:s'));
                        $m->save();
                    }
                } catch (Exception $e) {
                    $this->getResponse()->setBody($e->getMessage());
                }
            }
        }
    }

    public function markAsReadAction()
    {
        $messageId = $this->getRequest()->getParam('messageId');

        try {
            $m = Mage::getModel('notif/message')->load($messageId);
            $currentStatus = $m->getNotificationStatus();

            if ($currentStatus != $this->_status['read'] && $currentStatus != $this->_status['deleted']) {
                $m->setNotificationStatus($this->_status['read']);
                $m->setDtRead(date('Y-m-d H:i:s'));
                $m->save();
            }
        } catch (Exception $e) {
            $this->getResponse()->setBody($e->getMessage());
        }
    }

    public function clickedAction()
    {
        $messageId = $this->getRequest()->getParam('id');
        $url = base64_decode($this->getRequest()->getParam('url'));

        try {
            $m = Mage::getModel('notif/message')->load($messageId);
            $currentStatus = $m->getNotificationStatus();

            if ($currentStatus != $this->_status['deleted'] && $currentStatus != $this->_status['clicked']) {
                $m->setNotificationStatus($this->_status['clicked']);
                $m->setDtClicked(date('Y-m-d H:i:s'));
                $m->save();
            }

            $this->_redirectUrl($url);
        } catch (Exception $e) {

        }
    }

    public function deleteMessageAction()
    {
        $messageId = $this->getRequest()->getParam('messageId');

        try {
            $message = Mage::getModel('notif/message')->load($messageId);
            $message->setNotificationStatus($this->_status['deleted']);
            $message->setDtDeleted(date('Y-m-d H:i:s'));
            $message->save();
        } catch (Exception $e) {

        }

        $this->_renderHtmlContent();
    }

    public function turnOffAction()
    {
        $customerSession = Mage::getSingleton('customer/session');
        if ($customerSession->isLoggedIn()) {
            try {
                Mage::getModel('notif/distributor')->turnOffNotification($customerSession->getCustomer());
            } catch (Exception $e) {
                $this->getResponse()->setBody($e->getMessage());
            }
        }
    }
}