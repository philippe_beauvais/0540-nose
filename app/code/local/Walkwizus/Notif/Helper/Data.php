<?php

class Walkwizus_Notif_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NOTIF_GENERAL_LIMIT = 'notif/general/limit';

    const XML_PATH_NOTIF_GENERAL_ID_CAMPAIGN = 'notif/general/id_campaign';

    public function getLimitMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_NOTIF_GENERAL_LIMIT);
    }

    public function getIdCampaign()
    {
        return Mage::getStoreConfig(self::XML_PATH_NOTIF_GENERAL_ID_CAMPAIGN);
    }

    private function _parseMessageContent($message, $id)
    {
        preg_match_all('#href="([^"]+)"#', $message, $matches);

        foreach ($matches[1] as $match) {
            $trackedUrl = Mage::getUrl('notif/ajax/clicked', array(
                'id' => $id,
                'url' => base64_encode($match)
            ));
            $message = str_replace('href="' . $match . '"', 'href="' . $trackedUrl . '"', $message);
        }

        return $message;
    }

    public function getMessageContent($message)
    {
        $storeCode = Mage::app()->getStore()->getCode();
        return $this->_parseMessageContent($message['content_' . $storeCode], $message['id_notification']);
    }
}