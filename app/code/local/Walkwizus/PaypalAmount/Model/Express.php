<?php

class Walkwizus_PaypalAmount_Model_Express extends Mage_Paypal_Model_Express
{
    private $_minAmount = 80;

    public function isAvailable($quote = null)
    {
        $total = $quote->getBaseGrandTotal();

        if (parent::isAvailable($quote) && $total > $this->_minAmount) {
            return true;
        }
        
        return false;
    }
}