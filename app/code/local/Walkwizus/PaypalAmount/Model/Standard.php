<?php

class Walkwizus_PaypalAmount_Model_Standard extends Mage_Paypal_Model_Standard
{
    private $_minAmount = 80;

    public function isAvailable($quote = null)
    {
        $total = $quote->getBaseGrandTotal();

        if ($this->getConfig()->isMethodAvailable() && parent::isAvailable($quote) && $total > $this->_minAmount) {
            return true;
        }

        return false;
    }
}