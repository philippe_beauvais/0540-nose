<?php

class Walkwizus_Cgu_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $redirect = base64_decode($this->getRequest()->getParam('current_url'));
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $userModel = Mage::getModel('mynose/user');
        $user = $userModel->getNoseUserFromMagentoId($customer->getId());

        $userModel->updateNoseUserData($user, array('term_of_use' => 1));

        $customer->setTermOfUse(1)->save();

        $this->_redirectUrl($redirect);
    }
}