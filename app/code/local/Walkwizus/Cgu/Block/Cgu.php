<?php

class Walkwizus_Cgu_Block_Cgu extends Mage_Core_Block_Template
{
    public function getFlagCgu($customer)
    {
        return Mage::getModel('mynose/user')->getNoseUserFromMagentoId($customer->getId());
    }
}