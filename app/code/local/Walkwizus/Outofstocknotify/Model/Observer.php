<?php

class Walkwizus_Outofstocknotify_Model_Observer
{
    public function onProductSave($observer)
    {
        $product = $observer->getEvent()->getProduct();

        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());

        if (!$stockItem->getIsInStock()) {
            $stockItem->setIsInStock(1);
            $stockItem->save();
        }
    }

    public function onStockItemSave($observer)
    {
        Mage::app()->getTranslator()->init('frontend', true);

        $item = $observer->getEvent()->getItem();

        if ($item->getIsInStock() && $item->getQty() > 0) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            $notifications = Mage::getModel('outofstocknotify/notification')->getNotificationsByProduct($product, Walkwizus_Outofstocknotify_Model_Notification::STATUS_PENDING);

            if ($notifications->count() > 0) {
                foreach ($notifications as $notification) {
                    $locale = Mage::getStoreConfig('general/locale/code', $notification->getStoreId());
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('outofstocknotify_notification_restocking', $locale);

                    $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $notification->getStoreId()));

                    $emailTemplateVariables = array();
                    $emailTemplateVariables['product'] = $product;

                    $customer = Mage::getModel('customer/customer')->loadByEmail($notification->getEmail());
                    if ($customer->getId()) {
                        $emailTemplateVariables['customer'] = $customer;
                    }

                    $messageId = Mage::helper('outofstocknotify')->sendEmail($notification->getEmail(), $emailTemplate, $emailTemplateVariables);

                    if (!is_null($messageId)) {
                        $notification->setData('message_restocking_id', $messageId);

                        $noseUser = Mage::helper('outofstocknotify')->getNoseUser($notification->getEmail());

                        Mage::getModel('reporting/message')
                            ->setMessageId($messageId)
                            ->setCampaignAlt('outofstocknotify_restocking')
                            ->setContactId($noseUser['id_user'])
                            ->setToEmail($notification->getEmail())
                            ->save();
                    }

                    $notification->setData('status', Walkwizus_Outofstocknotify_Model_Notification::STATUS_NOTIFIED);
                    $notification->setData('updated_at', date('Y-m-d H:i:s'));
                    $notification->save();
                }
            }
        }
    }
}