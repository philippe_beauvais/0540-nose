<?php

class Walkwizus_Outofstocknotify_Model_Resource_Notification_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('outofstocknotify/notification');
    }
}