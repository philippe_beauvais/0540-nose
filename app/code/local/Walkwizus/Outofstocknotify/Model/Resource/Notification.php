<?php

class Walkwizus_Outofstocknotify_Model_Resource_Notification extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('outofstocknotify/notification', 'entity_id');
    }
}