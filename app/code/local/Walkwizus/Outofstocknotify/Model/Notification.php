<?php

class Walkwizus_Outofstocknotify_Model_Notification extends Mage_Core_Model_Abstract
{
    const STATUS_NOTIFIED = 'notified';
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELED = 'canceled';

    public function _construct()
    {
        $this->_init('outofstocknotify/notification');
    }

    public function getStatusOptions()
    {
        return array(
            self::STATUS_PENDING => Mage::helper('outofstocknotify')->__('Pending'),
            self::STATUS_NOTIFIED => Mage::helper('outofstocknotify')->__('Notified'),
            self::STATUS_CANCELED => Mage::helper('outofstocknotify')->__('Canceled')
        );
    }

    public function isCustomerSubscribed($email, $productId)
    {
        $results = $this->getCollection()
            ->addFieldToFilter('email', $email)
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('status', self::STATUS_PENDING);
        return $results->count() == 1;
    }

    public function getNotificationsByCustomer($email, $status = null)
    {
        $notifications = $this->getCollection()
            ->addFieldToFilter('email', $email);
        if (!is_null($status)) {
            $notifications->addFieldToFilter('status', $status);
        }
        return $notifications;
    }

    public function getNotificationsByProduct($product, $status = null)
    {
        $notifications = $this->getCollection()
            ->addFieldToFilter('product_id', $product->getId());
        if (!is_null($status)) {
            $notifications->addFieldToFilter('status', $status);
        }
        return $notifications;
    }
}