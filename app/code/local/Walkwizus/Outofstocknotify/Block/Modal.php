<?php

class Walkwizus_Outofstocknotify_Block_Modal extends Mage_Core_Block_Template
{
    public function getOutofstockProducts()
    {
        $products = array();

        $product = Mage::registry('product');
        if ($product) {
            $_product = Mage::getModel('catalog/product')->load($product->getId());
            if($_product->getTypeId() == 'configurable') {
                $simpleProducts = $_product->getTypeInstance(true)->getUsedProducts(null, $_product);

                foreach ($simpleProducts as $simpleProduct) {
                    $inventory = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simpleProduct->getId());

                    if ($inventory->getQty() <= 0) {
                        $simpleProduct->load($simpleProduct->getId());

                        $isSubscribed = false;
                        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                            $customer = Mage::getSingleton('customer/session')->getCustomer();
                            $isSubscribed = Mage::getModel('outofstocknotify/notification')->isCustomerSubscribed($customer->getEmail(), $simpleProduct->getId());
                        }
                        $simpleProduct->setIsSubscribed($isSubscribed);
                        $products[$simpleProduct->getId()] = $simpleProduct;
                    }
                }
            }
        }

        return $products;
    }
}