<?php

class Walkwizus_Outofstocknotify_Block_Adminhtml_Notifications extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_notifications';
        $this->_blockGroup = 'outofstocknotify';

        $this->_headerText = $this->__('Notifications');
        parent::__construct();
        $this->_removeButton('add');
    }
}