<?php

class Walkwizus_Outofstocknotify_Block_Adminhtml_Notifications_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('notificationsGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('outofstocknotify/notification_collection');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('sku', array(
            'header' => $this->__('Sku'),
            'align' => 'left',
            'index' => 'sku'
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'name'
        ));

        $this->addColumn('email', array(
            'header' => $this->__('Email'),
            'align' => 'left',
            'index' => 'email'
        ));

        $this->addColumn('store_id', array(
            'header'    => Mage::helper('sales')->__('Store'),
            'index'     => 'store_id',
            'type'      => 'store',
            'store_view'=> true,
            'display_deleted' => false,
        ));

        $this->addColumn('created_at', array(
            'header' => $this->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'datetime'
        ));

        $this->addColumn('updated_at', array(
            'header' => $this->__('Updated At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'datetime'
        ));

        $this->addColumn('status', array(
            'header' => $this->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getModel('outofstocknotify/notification')->getStatusOptions(),
            'frame_callback' => array($this, 'decorateStatus')
        ));

        $this->addColumn('contacted', array(
            'header' => $this->__('Contacted'),
            'align' => 'left',
            'index' => 'contacted',
            'type' => 'options',
            'options' => array(
                Mage::helper('outofstocknotify')->__('No'),
                Mage::helper('outofstocknotify')->__('Yes')
            ),
            'frame_callback' => array($this, 'decorateContacted')
        ));

        return parent::_prepareColumns();
    }

    public function decorateStatus($value, $row, $column, $isExport)
    {
        $class = '';
        switch ($row->getStatus()) {
            case Walkwizus_Outofstocknotify_Model_Notification::STATUS_PENDING :
                $class = 'outofstocknotify-pending';
                break;
            case Walkwizus_Outofstocknotify_Model_Notification::STATUS_NOTIFIED :
                $class = 'outofstocknotify-notified';
                break;
            case Walkwizus_Outofstocknotify_Model_Notification::STATUS_CANCELED :
                $class = 'outofstocknotify-canceled';
                break;
        }
        return '<span class="'.$class.'">'.$value.'</span>';
    }

    public function decorateContacted($value, $row, $column, $isExport)
    {
        return $row->getContacted()
            ? '<span class="outofstocknotify-yes">'.Mage::helper('outofstocknotify')->__('Yes').'</span>'
            : '<span class="outofstocknotify-no">'.Mage::helper('outofstocknotify')->__('No').'</span>';
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('notification_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $statusOptions = Mage::getModel('outofstocknotify/notification')->getStatusOptions();

        $this->getMassactionBlock()->addItem('change_status', array(
            'label'         => Mage::helper('index')->__('Change Status'),
            'url'           => $this->getUrl('*/*/massChangeStatus'),
            'additional'    => array(
                'mode'      => array(
                    'name'      => 'notification_status',
                    'type'      => 'select',
                    'class'     => 'required-entry',
                    'label'     => Mage::helper('index')->__('Notification Status'),
                    'values'    => $statusOptions
                )
            )
        ));

        $this->getMassactionBlock()->addItem('change_contacted', array(
            'label'         => Mage::helper('index')->__('Change Contacted Status'),
            'url'           => $this->getUrl('*/*/massChangeContacted'),
            'additional'    => array(
                'mode'      => array(
                    'name'      => 'notification_contacted',
                    'type'      => 'select',
                    'class'     => 'required-entry',
                    'label'     => Mage::helper('index')->__('Contacted Status'),
                    'values'    => array(
                        Mage::helper('outofstocknotify')->__('No'),
                        Mage::helper('outofstocknotify')->__('Yes')
                    )
                )
            )
        ));

        return $this;
    }
}