<?php

class Walkwizus_Outofstocknotify_Block_Email extends Mage_Core_Block_Template
{
    public function getParentProduct()
    {
        $product = $this->getProduct();

        $parentProduct = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
        if (!empty($parentProduct)) {
            $product = Mage::getModel('catalog/product')->load($parentProduct[0]);
        }

        return $product;
    }

    public function getAddtocartUrl()
    {
        $product = $this->getProduct();

        $params = array('product' => $product->getId());

        $customer = $this->getCustomer();
        if ($customer) {
            $params[Mage::getStoreConfig('customer/autologin/urlparam')] = $customer->getLoginHash();
        }

        return $this->getUrl('outofstocknotify/email/addtocart', $params);
    }
}