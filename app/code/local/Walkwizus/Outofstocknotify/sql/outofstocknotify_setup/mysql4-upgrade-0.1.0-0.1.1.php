<?php

$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `sku` TEXT NOT NULL COMMENT 'Product Sku' AFTER `product_id`;
");

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `name` TEXT NOT NULL COMMENT 'Product Name' AFTER `sku`;
");

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `status` TEXT NOT NULL COMMENT 'Status' AFTER `store_id`;
");

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `updated_at` DATETIME NULL COMMENT 'Updated At' AFTER `created_at`;
");
$installer->endSetup();