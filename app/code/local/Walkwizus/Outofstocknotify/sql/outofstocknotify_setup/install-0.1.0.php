<?php

$installer = $this;

$table = $installer->getConnection()->newTable($installer->getTable('outofstocknotify/notification'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID'
);

$table->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Customer Email');

$table->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Product ID');

$table->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Store ID');

$table->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
    'nullable' => false,
), 'Created At');

$installer->getConnection()->createTable($table);

$installer->endSetup();

