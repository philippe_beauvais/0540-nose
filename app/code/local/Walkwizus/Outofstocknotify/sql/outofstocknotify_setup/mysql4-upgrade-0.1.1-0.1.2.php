<?php

$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `contacted` TINYINT NOT NULL DEFAULT 0 COMMENT 'Contacted' AFTER `status`;
");

$installer->endSetup();