<?php

$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `message_subscription_id` TEXT NULL COMMENT 'Message Subscription ID' AFTER `updated_at`;
");

$installer->run("
  ALTER TABLE {$this->getTable('outofstocknotify/notification')}
  ADD COLUMN `message_restocking_id` TEXT NULL COMMENT 'Message Restocking ID' AFTER `message_subscription_id`;
");

$installer->endSetup();