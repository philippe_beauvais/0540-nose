<?php

class Walkwizus_Outofstocknotify_ModalController extends Mage_Core_Controller_Front_Action
{
    public function notifyAction()
    {
        $email = $this->getRequest()->getPost('email');
        $productId = $this->getRequest()->getPost('product_id');

        $product = Mage::getModel('catalog/product')->load($productId);

        $notification = Mage::getModel('outofstocknotify/notification');

        if (!$notification->isCustomerSubscribed($email, $productId)) {
            $storeId = Mage::app()->getStore()->getId();

            Mage::app()->getTranslator()->init('frontend', true);

            $notification
                ->setData('email', $email)
                ->setData('product_id', $productId)
                ->setData('sku', $product->getSku())
                ->setData('name', $product->getName())
                ->setData('store_id', $storeId)
                ->setData('status', Walkwizus_Outofstocknotify_Model_Notification::STATUS_PENDING)
                ->setData('created_at', date('Y-m-d H:i:s'));

            $customer = Mage::getModel('customer/customer')->loadByEmail($notification->getEmail());

            if (!$customer->getId()) {
                $userId = Mage::helper('outofstocknotify')->addUserToEnose($notification);
            }
            else {
                $userModel = Mage::getModel('mynose/user');
                $noseUser = $userModel->getNoseUser($customer);
                $userId = $noseUser['id_user'];
            }

            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('outofstocknotify_notification_subscription');

            $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId));

            $emailTemplateVariables = array();
            $emailTemplateVariables['product'] = $product;

            if ($customer->getId()) {
                $emailTemplateVariables['customer'] = $customer;
                if ($customer->getGender() == 2) {
                    $emailTemplateVariables['female'] = true;
                }
            }

            $messageId = Mage::helper('outofstocknotify')->sendEmail($notification->getEmail(), $emailTemplate, $emailTemplateVariables);

            if (!is_null($messageId)) {
                $notification->setData('message_subscription_id', $messageId);

                Mage::getModel('reporting/message')
                    ->setMessageId($messageId)
                    ->setCampaignAlt('outofstocknotify_subscription')
                    ->setContactId($userId)
                    ->setToEmail($notification->getEmail())
                    ->save();
            }

            $notification->save();
        }

        Mage::getSingleton('core/session')->addSuccess($this->__('You will be notified when this product is back in stock: %s', $product->getName()));
        $this->_redirectReferer();
    }
}