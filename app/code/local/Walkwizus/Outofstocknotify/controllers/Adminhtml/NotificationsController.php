<?php

class Walkwizus_Outofstocknotify_Adminhtml_NotificationsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('customer');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('outofstocknotify/adminhtml_notifications'))
            ->renderLayout();
    }

    public function massChangeStatusAction()
    {
        $notificationsIds = $this->getRequest()->getParam('notification_ids');
        if (empty($notificationsIds) || !is_array($notificationsIds)) {
            $this->_getSession()->addError(Mage::helper('index')->__('Please select Notification(s)'));
        } else {
            try {
                $counter = 0;
                $status = $this->getRequest()->getParam('notification_status');
                foreach ($notificationsIds as $notificationId) {
                    $notification = Mage::getModel('outofstocknotify/notification')->load($notificationId);
                    if ($notification->getId()) {
                        $notification->setStatus($status);
                        $notification->setUpdatedAt(date('Y-m-d H:i:s'));
                        $notification->save();
                        $counter++;
                    }
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('index')->__('Total of %d notification(s) have changed status.', $counter)
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('index')->__('An error occurred.'));
            }
        }

        $this->_redirect('*/*/index');
    }

    public function massChangeContactedAction()
    {
        $notificationsIds = $this->getRequest()->getParam('notification_ids');
        if (empty($notificationsIds) || !is_array($notificationsIds)) {
            $this->_getSession()->addError(Mage::helper('index')->__('Please select Notification(s)'));
        } else {
            try {
                $counter = 0;
                $contacted = $this->getRequest()->getParam('notification_contacted');
                foreach ($notificationsIds as $notificationId) {
                    $notification = Mage::getModel('outofstocknotify/notification')->load($notificationId);
                    if ($notification->getId()) {
                        $notification->setContacted($contacted);
                        $notification->save();
                        $counter++;
                    }
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('index')->__('Total of %d notification(s) have changed contacted status.', $counter)
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('index')->__('An error occurred.'));
            }
        }

        $this->_redirect('*/*/index');
    }
}