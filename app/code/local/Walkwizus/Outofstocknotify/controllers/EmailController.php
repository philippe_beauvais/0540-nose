<?php

class Walkwizus_Outofstocknotify_EmailController extends Mage_Core_Controller_Front_Action
{
    public function addtocartAction()
    {
        $productId = $this->getRequest()->getParam('product');

        $product = Mage::getModel('catalog/product')->load($productId);
        $params = array('qty' => 1);

        $parentProduct = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
        if (!empty($parentProduct)) {
            $simpleProduct = $product;
            $product = Mage::getModel('catalog/product')->load($parentProduct[0]);

            $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
            $superAttribute = array();
            foreach ($productAttributeOptions as $productAttribute) {
                $superAttribute[$productAttribute['attribute_id']] = $simpleProduct->getData($productAttribute['attribute_code']);
            }

            $params['super_attribute'] = $superAttribute;
        }

        $cart = Mage::getModel('checkout/cart');
        $cart->init();
        $cart->addProduct($product, $params);
        $cart->save();

        Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

        $this->_redirect('checkout/cart');
    }
}