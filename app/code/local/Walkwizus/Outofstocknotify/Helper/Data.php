<?php

use \Mailjet\Resources;

class Walkwizus_Outofstocknotify_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function sendEmail($email, $emailTemplate, $emailTemplateVariables)
    {
        $messageId = null;

        if (Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_ENABLED)) {
            $mj = new \Mailjet\Client(
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_LOGIN),
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_PASSWORD)
            );

            $html = $emailTemplate->getProcessedTemplate($emailTemplateVariables, true);
            $subject = $emailTemplate->getProcessedTemplateSubject($emailTemplateVariables);

            $recipients = array(
                array('Email' => $email)
            );

            $bcc = Mage::getStoreConfig('nose/outofstock/email');
            if (!empty($bcc)) {
                $bcc = explode(',', $bcc);
                foreach ($bcc as $address) {
                    $recipients[] =  array('Email' => $address);
                }
            }

            $body = array(
                'FromEmail' => Mage::getStoreConfig('trans_email/ident_general/email'),
                'FromName' => Mage::getStoreConfig('trans_email/ident_general/name'),
                'Subject' => $subject,
                'Html-part' => $html,
                'Recipients' => $recipients
            );

            $response = $mj->post(Resources::$Email, array('body' => $body));
            $body = $response->getBody();

            foreach ($body['Sent'] as $message) {
                if ($message['Email'] == $email) {
                    $messageId = $message['MessageID'];
                    break;
                }
            }
        }
        else {
            $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
            $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
            $emailTemplate->setReplyTo(Mage::getStoreConfig('trans_email/ident_custom1/email'));

            $bcc = Mage::getStoreConfig('nose/outofstock/email');
            if (!empty($bcc)) {
                $bcc = explode(',', $bcc);
                $emailTemplate->addBcc($bcc);
            }

            $recipientEmail = $email;
            $recipientName = $email;

            $emailTemplate->send($recipientEmail, $recipientName, $emailTemplateVariables);
        }

        return $messageId;
    }

    public function getNoseUser($email)
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('mynose_read');

        $select = $read->select()
            ->from( array('u' => $resource->getTableName('mynose/users')))
            ->where('email = ?', $email);

        $query = $select->query();
        $user = $query->fetch();

        return $user;
    }

    public function addUserToEnose($notification)
    {
        $user = $this->getNoseUser($notification->getEmail());

        if (!empty($user)) {
            return $user['id_user'];
        }

        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('mynose_write');

        $joinDate = date('Y-m-d H:i:s');

        $locale = Mage::getStoreConfig('general/locale/code', $notification->getStoreId());
        $favoriteLang = array_shift(explode('_', $locale));

        $write->insert(
            $resource->getTableName('mynose/users'),
            array(
                'username' => $notification->getEmail(),
                'email' => $notification->getEmail(),
                'id_group' => 6,
                'join_date' => $joinDate,
                'origin' => 10,
                'favorite_lang' => $favoriteLang
            )
        );

        $userId = $write->lastInsertId();

        $write->insert(
            $resource->getTableName('nose/lk_user_distributor'),
            array(
                'id_user' => $userId,
                'distributor_id' => 1,
                'join_date' => $joinDate
            )
        );

        return $userId;
    }
}