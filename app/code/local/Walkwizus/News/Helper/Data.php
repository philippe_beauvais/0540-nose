<?php

class Walkwizus_News_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NEWS_POST_URL_PREFIX = 'news/post/url_prefix';

    const XML_PATH_NEWS_POST_URL_SUFFIX = 'news/post/url_suffix';

    const XML_PATH_NEWS_POST_URL_REWRITE_LIST = 'news/post/url_rewrite_list';

    public function getUrlPrefix()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWS_POST_URL_PREFIX);
    }

    public function getUrlSuffix()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWS_POST_URL_SUFFIX);
    }

    public function getUrlRewriteList()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWS_POST_URL_REWRITE_LIST);
    }

    public function getPostImage($post)
    {
        return Mage::getBaseUrl('media') . $post->getThumbnail();
    }
}