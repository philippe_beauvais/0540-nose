<?php

class Walkwizus_News_Block_Adminhtml_Post extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'news';

        parent::__construct();

        $this->_headerText = $this->__('Manage Posts');
        $this->_updateButton('add', 'label', $this->__('Add Post'));

        $this->setTemplate('news/grid.phtml');
    }
}