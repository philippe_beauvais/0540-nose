<?php

class Walkwizus_News_Block_Adminhtml_Post_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('post_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('General Information'));
    }

    protected function _prepareLayout()
    {
        $entity = Mage::getModel('eav/entity_type')->load('news_post', 'entity_type_code');

        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
            ->setAttributeSetFilter($entity->getDefaultAttributeSetId())
            ->setSortOrder()
            ->load();

        foreach ($groupCollection as $group) {
            $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entity->getEntityTypeId())
                ->setAttributeGroupFilter($group->getId());

            $attributes->getSelect()->order('additional_table.position', 'ASC');

            $this->addTab('group_' . $group->getId(), array(
                'label' => $this->__($group->getAttributeGroupName()),
                'content' => $this->getLayout()->createBlock('news/adminhtml_post_edit_tab_' . strtolower($group->getAttributeGroupName()))->setAttributes($attributes)->toHtml()
            ));
        }

        $this->addTab('products', array(
            'label' => $this->__('Associated products'),
            'url' => $this->getUrl('*/*/products', array('_current' => true)),
            'class' => 'ajax'
        ));

        $this->addTab('sliders', array(
            'label' => $this->__('Sliders'),
            'url' => $this->getUrl('*/*/sliders', array('_current' => true)),
            'class' => 'ajax'
        ));

        return parent::_beforeToHtml();
    }

    public function getPost()
    {
        return Mage::registry('post_data');
    }
}