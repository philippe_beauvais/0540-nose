<?php

class Walkwizus_News_Block_Adminhtml_Post_Edit_Tab_Seo extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareLayout()
    {
        Varien_Data_Form::setElementRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );

        Varien_Data_Form::setFieldsetRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')
        );

        Varien_Data_Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock('news/adminhtml_post_renderer_fieldset_element')
        );
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setDataObject(Mage::registry('post_data'));

        $general = $form->addFieldset('general', array(
            'legend' => $this->__('General Informations'),
            'class' => 'fieldset-wide',
        ));

        $attributes = $this->getAttributes();
        foreach ($attributes as $attribute) {
            $attribute->setEntity(Mage::getResourceModel('news/post'));
        }

        $this->_setFieldset($attributes, $general, array());
        $formValues = Mage::registry('post_data')->getData();

        if (!Mage::registry('post_data')->getId()) {
            foreach ($attributes as $attribute) {
                if (!isset($formValues[$attribute->getAttributeCode()])) {
                    $formValues[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                }
            }
        }

        $form->addValues($formValues);
        $form->setFieldNameSuffix('post');
        $this->setForm($form);
    }

    public function getTabLabel()
    {
        return $this->__('General');
    }

    public function getTabTitle()
    {
        return $this->__('General');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}