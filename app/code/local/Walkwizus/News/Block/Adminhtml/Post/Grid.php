<?php

class Walkwizus_News_Block_Adminhtml_Post_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('postGrid');
        $this->setDefaultSort('entity_id');
        $this->setSaveParametersInSession(true);
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('news/post')
            ->getCollection()
            ->addAttributeToSelect('title');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => $this->__('Post ID'),
            'align' => 'left',
            'index' => 'entity_id',
        ));

        $this->addColumn('title', array(
            'header' => $this->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('post');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->__('Are you sure?'),
        ));
    }

    public function getRowUrl($item)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $item->getId()));
    }
}