<?php

class Walkwizus_News_Block_Post extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 7200,
            'cache_tags' => array(Walkwizus_News_Model_Post::CACHE_TAG)
        ));
    }

    public function getCacheKeyInfo()
    {
        return array(
            'name_in_layout' => $this->getNameInLayout(),
            'store' => Mage::app()->getStore()->getId(),
            'design_package' => Mage::getDesign()->getPackageName(),
            'design_theme' => Mage::getDesign()->getTheme('template')
        );
    }

    public function getLastPost($limit)
    {
        $collection = Mage::getModel('news/post')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setOrder('created_at', 'DESC');

        $collection->getSelect()->limit($limit);

        return $collection;
    }
    
    public function getLastHomepagePost($limit = false)
    {
        $collection = Mage::getModel('news/post')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_homepage', 1)
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setOrder('position_post', 'ASC');

        if ($limit) {
            $collection->getSelect()->limit($limit);
        }

        return $collection;
    }
}