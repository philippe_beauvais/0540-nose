<?php

class Walkwizus_News_Adminhtml_PostController extends Mage_Adminhtml_Controller_Action
{
    protected function _construct()
    {
        $this->setUsedModuleName('Walkwizus_News');
    }

    protected function _initPost()
    {
        $this->_title($this->__('News'))
            ->_title($this->__('Manage Posts'));

        $postId  = (int) $this->getRequest()->getParam('entity_id');
        $post    = Mage::getModel('news/post')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if ($postId) {
            $post->load($postId);
        }

        Mage::register('post_data', $post);
        return $post;
    }

    public function indexAction()
    {
        $this->_title($this->__('News'))
            ->_title($this->__('Manage Posts'));

        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $entityId = (int)$this->getRequest()->getParam('entity_id');
        $post = $this->_initPost();

        if ($entityId && !$post->getId()) {
            $this->_getSession()->addError($this->__('This post no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        if ($data = Mage::getSingleton('adminhtml/session')->getPostData(true)) {
            $post->setData($data);
        }

        $this->loadLayout();

        if ($post->getId()) {
            if (!Mage::app()->isSingleStoreMode() && ($storeSwitcher = $this->getLayout()->getBlock('store_switcher'))) {
                $storeSwitcher->setDefaultStoreName($this->__('Default Values'))
                    ->setWebsiteIds($post->getWebsiteIds())
                    ->setSwitchUrl($this->getUrl('*/*/*', array(
                        '_current' => true,
                        'active_tab' => null,
                        'tab' => null,
                        'store' => null,
                    )));
            }
        } else {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }

        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $post = $this->_initPost();
            $postData = $this->getRequest()->getParam('post', array());

            $post->addData($postData);
            $post->setAttributeSetId($post->getDefaultAttributeSetId());

            $products = $this->getRequest()->getPost('products', -1);
            if ($products != -1) {
                $post->setProductsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($products));
            }

            $sliders = $this->getRequest()->getPost('sliders', -1);
            if ($sliders != -1) {
                $post->setSlidersData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($sliders));
            }

            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $post->setData($attributeCode, false);
                }
            }

            try {
                $post->save();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('This post was saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/', array('entity_id', $post->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/', array('id', $this->getRequest()->getParam('id')));
                return;
            }
        }
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('entity_id')) {
            try {
                Mage::getModel('news/post')->load($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('The post has been deleted');
                $this->_redirect('*/*/list');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/list');
                return;
            }
        }
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('post');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select post(s)'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('news/post')->load($id)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function productsAction()
    {
        $this->_initPost();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('post.edit.tab.product')
            ->setPostProducts($this->getRequest()->getPost('post_products', null));
        $this->renderLayout();

    }

    public function slidersAction()
    {
        $this->_initPost();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('post.edit.tab.slider')
            ->setPostSliders($this->getRequest()->getPost('post_sliders', null));
        $this->renderLayout();

    }

    public function productsgridAction()
    {
        $this->_initPost();
        $this->loadLayout();
        $this->getLayout()->getBlock('post.edit.tab.product')
            ->setPostProducts($this->getRequest()->getPost('post_products', null));
        $this->renderLayout();
    }

    public function slidersgridAction()
    {
        $this->_initPost();
        $this->loadLayout();
        $this->getLayout()->getBlock('post.edit.tab.slider')
            ->setPostSliders($this->getRequest()->getPost('post_sliders', null));
        $this->renderLayout();
    }

    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('news/adminhtml_post_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id' => $storeId,
            'store_media_url' => $storeMediaUrl,
        ));

        $this->getResponse()->setBody($content->toHtml());
    }
}