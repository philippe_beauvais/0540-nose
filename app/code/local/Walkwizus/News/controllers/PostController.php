<?php

class Walkwizus_News_PostController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _initPost()
    {
        $id = $this->getRequest()->getParam('id', 0);
        $post = Mage::getModel('news/post')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($id);

        if (!$post->getId()) {
            return false;
        } else if (!$post->getStatus()) {
            return false;
        }

        return $post;
    }

    public function viewAction()
    {
        $post = $this->_initPost();

        if (!$post) {
            $this->_forward('no-route');
            return;
        }

        Mage::register('current_post', $post);

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');

        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('news-post news-post' . $post->getId());
        }

        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $post->getUrl());
            if ($post->getMetaTitle()) {
                $headBlock->setTitle($post->getMetaTitle());
            } else {
                $headBlock->setTitle($post->getTitle());
            }
            $headBlock->setKeywords($post->getMetaKeywords());
            $headBlock->setMetaDescription($post->getMetaDescription());
        }
        $this->renderLayout();
    }
}