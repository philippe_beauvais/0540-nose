<?php

class Walkwizus_News_Model_Resource_Post_Collection extends Mage_Catalog_Model_Resource_Collection_Abstract //Mage_Eav_Model_Entity_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('news/post');
    }
}