<?php

class Walkwizus_News_Model_Resource_Post_Slider_Collection extends Walkwizus_Slider_Model_Resource_Slider_Collection
{
    protected $_joinedFields = false;

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('news/post_slider')),
                'related.slider_id = main_table.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addPostFilter($post)
    {
        if ($post instanceof Walkwizus_News_Model_Post) {
            $post = $post->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.post_id = ?', $post);
        return $this;
    }
}
