<?php

class Walkwizus_News_Model_Resource_Post_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    protected $_joinedFields = false;

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('news/post_product')),
                'related.product_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addPostFilter($post)
    {
        if ($post instanceof Walkwizus_News_Model_Post) {
            $post = $post->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.post_id = ?', $post);
        return $this;
    }
}
