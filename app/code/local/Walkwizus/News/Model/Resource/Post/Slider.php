<?php

class Walkwizus_News_Model_Resource_Post_Slider extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('news/post_slider', 'rel_id');
    }

    public function savePostRelation($post, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('post_id=?', $post->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $sliderId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'post_id' => $post->getId(),
                    'slider_id' => $sliderId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function saveSliderRelation($slider, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('slider_id=?', $slider->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $postId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'post_id' => $postId,
                    'slider_id' => $slider->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
