<?php

class Walkwizus_News_Model_Post_Slider extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('news/post_slider');
    }

    public function savePostRelation($post)
    {
        $data = $post->getSlidersData();
        if (!is_null($data)) {
            $this->_getResource()->savePostRelation($post, $data);
        }
        return $this;
    }

    public function getSliderCollection($post)
    {
        $collection = Mage::getResourceModel('news/post_slider_collection')
            ->addPostFilter($post);
        return $collection;
    }
}
