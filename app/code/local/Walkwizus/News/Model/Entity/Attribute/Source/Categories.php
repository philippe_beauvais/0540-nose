<?php

class Walkwizus_News_Model_Entity_Attribute_Source_Categories extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        return array(
            array(
                'label' => Mage::helper('news')->__('-- Choose Category --'),
                'value' => -1
            ),
            array(
                'label' => Mage::helper('news')->__('News'),
                'value' => 'news'
            ),
            array(
                'label' => Mage::helper('news')->__('Team'),
                'value' => 'team'
            ),
            array(
                'label' => Mage::helper('news')->__('Press'),
                'value' => 'press'
            ),
        );
    }
}