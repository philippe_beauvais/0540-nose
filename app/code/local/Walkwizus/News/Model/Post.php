<?php

class Walkwizus_News_Model_Post extends Mage_Catalog_Model_Abstract
{
    const ENTITY = 'news_post';
    const CACHE_TAG = 'news_post';

    protected $_eventPrefix = 'news_post';
    protected $_eventObject = 'post';
    protected $_productInstance = null;
    protected $_sliderInstance = null;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('news/post');
    }

    public function getDefaultAttributeSetId()
    {
        return $this->getResource()->getEntityType()->getDefaultAttributeSetId();
    }

    public function getAttributeText($attributeCode)
    {
        $text = $this->getResource()
            ->getAttribute($attributeCode)
            ->getSource()
            ->getOptionText($this->getData($attributeCode));
        if (is_array($text)) {
            return implode(', ', $text);
        }
        return $text;
    }

    public function getSelectedProducts()
    {
        if (!$this->hasSelectedProducts()) {
            $products = array();
            foreach ($this->getSelectedProductsCollection() as $product) {
                $products[] = $product;
            }
            $this->setSelectedProducts($products);
        }
        return $this->getData('selected_products');
    }

    public function getSelectedProductsCollection()
    {
        $collection = $this->getProductInstance()->getProductCollection($this);
        return $collection;
    }

    public function getSelectedSliders()
    {
        if (!$this->hasSelectedSliders()) {
            $sliders = array();
            foreach ($this->getSelectedSlidersCollection() as $slider) {
                $sliders[] = $slider;
            }
            $this->setSelectedSliders($sliders);
        }
        return $this->getData('selected_sliders');
    }

    public function getSelectedSlidersCollection()
    {
        $collection = $this->getSliderInstance()->getSliderCollection($this);
        return $collection;
    }

    protected function _afterSave()
    {
        $this->getProductInstance()->savePostRelation($this);
        $this->getSliderInstance()->savePostRelation($this);
        return parent::_afterSave();
    }

    public function getProductInstance()
    {
        if (!$this->_productInstance) {
            $this->_productInstance = Mage::getSingleton('news/post_product');
        }
        return $this->_productInstance;
    }

    public function getSliderInstance()
    {
        if (!$this->_sliderInstance) {
            $this->_sliderInstance = Mage::getSingleton('news/post_slider');
        }
        return $this->_sliderInstance;
    }

    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    protected function _beforeSave()
    {
        $this->cleanCache();
        return parent::_beforeSave();
    }

    public function cleanCache()
    {
        Mage::app()->cleanCache(array(self::CACHE_TAG));
    }

    /**
     * @todo Dynamics Attributes
     */
    public function getAttributes()
    {

    }
}