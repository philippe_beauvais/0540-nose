<?php

$installer = $this;
$this->startSetup();

$installer->removeAttribute($this->getEntityTypeId('news_post'), 'image_desktop');
$installer->removeAttribute($this->getEntityTypeId('news_post'), 'image_mobile');

$installer->endSetup();