<?php

$installer = $this;
$this->startSetup();

$installer->removeAttribute($this->getEntityTypeId('news_post'), 'thumbnail');

$installer->addAttribute('news_post', 'thumbnail_desktop', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Thumbnail News Desktop',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '17',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('news_post', 'thumbnail_mobile', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Thumbnail News Mobile',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '18',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('news_post', 'image_desktop', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Principale News Desktop',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '19',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('news_post', 'image_mobile', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Principale News Mobile',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '20',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->endSetup();