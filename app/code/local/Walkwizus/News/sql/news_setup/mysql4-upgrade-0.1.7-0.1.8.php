<?php

$installer = $this;
$this->startSetup();

$installer->addAttribute('news_post', 'is_homepage', array(
    'group' => 'General',
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Visible on homepage',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '',
    'user_defined' => false,
    'default' => '0',
    'unique' => false,
    'position' => '80',
    'note' => '',
    'visible' => '1',
    'wysiwyg_enabled' => '0',
));

$installer->endSetup();
