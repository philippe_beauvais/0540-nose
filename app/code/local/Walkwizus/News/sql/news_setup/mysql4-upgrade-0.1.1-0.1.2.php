<?php

$installer = $this;
$this->startSetup();

//$installer->removeAttribute($this->getEntityTypeId('news_post'), 'content');

$installer->addAttribute('news_post', 'article', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Article',
    'input' => 'textarea',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '16',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => true,
));

$installer->endSetup();