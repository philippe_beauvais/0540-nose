<?php

$installer = $this;
$this->startSetup();

$installer->addAttribute('news_post', 'category', array(
    'group' => 'General',
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Category',
    'input' => 'select',
    'source' => 'news/entity_attribute_source_categories',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '0',
    'user_defined' => true,
    'default' => '',
    'unique' => false,
    'position' => '40',
    'note' => '',
    'visible' => '1',
    'wysiwyg_enabled' => '0',
));

$installer->endSetup();
