<?php

$installer = $this;
$this->startSetup();

$installer->removeAttribute($this->getEntityTypeId('news_post'), 'thumbnail');

$installer->addAttribute('news_post', 'thumbnail', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Thumbnail',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '17',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$this->endSetup();