<?php

$installer = $this;
$this->startSetup();

$installer->addAttribute('news_post', 'post_position', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Position',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '0',
    'user_defined' => true,
    'default' => '',
    'unique' => false,
    'position' => '20',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->endSetup();