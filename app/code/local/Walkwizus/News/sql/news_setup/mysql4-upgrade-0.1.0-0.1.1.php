<?php

$installer = $this;
$this->startSetup();

$installer->addAttribute('news_post', 'subtitle', array(
    'group' => 'General',
    'type' => 'varchar',
    'backend' => '',
    'frontend' => '',
    'label' => 'Subtitle',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '15',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->endSetup();