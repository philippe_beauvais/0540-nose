<?php

class Walkwizus_News_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    const NEWS_MODULE_NAME = 'news';

    const NEWS_MODEL_POST = 'news/post';

    public function initControllerRouters($observer)
    {
        $front = $observer->getEvent()->getFront();
        $front->addRouter('news', $this);
        return $this;
    }

    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $urlKey = trim($request->getPathInfo(), '/');

        $check = array();
        $check['post'] = new Varien_Object(array(
            'prefix' => Mage::helper('news')->getUrlPrefix(),
            'suffix' => Mage::helper('news')->getUrlSuffix(),
            'list_key' => Mage::helper('news')->getUrlRewriteList(),
            'list_action' => 'index',
            'model' => self::NEWS_MODEL_POST,
            'controller' => 'post',
            'action' => 'view',
            'param' => 'id',
            'check_path' => 0,
        ));

        foreach ($check as $key => $value) {
            if ($value->getListKey()) {
                if ($urlKey == $value->getListKey()) {
                    $request->setModule(self::NEWS_MODULE_NAME)
                        ->setControllerName($value->getController())
                        ->setActionName($value->getListAction());

                    $request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $urlKey);
                    return true;
                }
            }

            if ($value->getPrefix()) {
                $parts = explode('/', $urlKey);
                if ($parts[0] != $value->getPrefix() || count($parts) != 2) {
                    continue;
                }
                $urlKey = $parts[1];
            }

            if ($value->getSuffix()) {
                $urlKey = substr($urlKey, 0, -strlen($value->getSuffix()) -1);
            }
            
            $model = Mage::getModel($value->getModel());
            $id = $model->checkUrlKey($urlKey, Mage::app()->getStore()->getId());

            if ($id) {
                if ($value->getCheckPath() && !$model->load($id)->getStatusPath()) {
                    continue;
                }
                $request->setModuleName(self::NEWS_MODULE_NAME)
                    ->setControllerName($value->getController())
                    ->setActionName($value->getAction())
                    ->setParam($value->getParam(), $id);

                $request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $urlKey);
                return true;
            }
        }

        return false;
    }
}