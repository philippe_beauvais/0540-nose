<?php

class Walkwizus_Sample_Model_Observer
{
    public function onProductAddToCart($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if (Mage::helper('sample')->isSample($product->getSku())) {
            if (!Mage::helper('sample')->checkSamplesMaxQtyInCart()) {
                $this->_removeCartItem($product);

                $link = '<a class="popup" href="' . Mage::getUrl('help/conditions') . '">' . Mage::helper('sample')->__('Nose Terms and Conditions of Sales') . '</a>';
                Mage::throwException(Mage::helper('sample')->__("According to the %s, you aren't allowed to order more than %d samples per web order.", $link, Mage::getStoreConfig('nose/sample/max_qty')));
            }

            if (!Mage::helper('sample')->checkSamplesUniqueQtyInCart($product)) {
                $this->_removeCartItem($product);

                $link = '<a class="popup" href="' . Mage::getUrl('help/conditions') . '">' . Mage::helper('sample')->__('Nose Terms and Conditions of Sales') . '</a>';
                Mage::throwException(Mage::helper('sample')->__("According to the %s, you aren't allowed to order the same sample more than twice per web order.", $link));
            }
        }
    }

    private function _removeCartItem($product)
    {
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $quoteItem = $quote->getItemByProduct($product);
        if ($quoteItem->getQty() > 1) {
            $quoteItem->setQty($quoteItem->getQty() - 1);
        }
        else {
            $quoteItem->delete();
        }
        $quote->save();
    }

    public function onCartShow($observer)
    {
        if (!Mage::helper('sample')->checkSamplesMaxQtyInCart()) {
            $link = Mage::helper('sample')->__('Nose Terms and Conditions of Sales');

            $quote = Mage::getModel('checkout/cart')->getQuote();
            $quote->addErrorInfo(
                'qty',
                'cataloginventory',
                Mage_CatalogInventory_Helper_Data::ERROR_QTY,
                Mage::helper('sample')->__("According to the %s, you aren't allowed to order more than %d samples per web order.", $link, Mage::getStoreConfig('nose/sample/max_qty'))
            );
        }

        if (!Mage::helper('sample')->checkSamplesUniqueQtyInCart()) {
            $link = Mage::helper('sample')->__('Nose Terms and Conditions of Sales');

            $quote = Mage::getModel('checkout/cart')->getQuote();
            $quote->addErrorInfo(
                'qty',
                'cataloginventory',
                Mage_CatalogInventory_Helper_Data::ERROR_QTY,
                Mage::helper('sample')->__("According to the %s, you aren't allowed to order the same sample more than twice per web order.", $link)
            );
        }
    }
}