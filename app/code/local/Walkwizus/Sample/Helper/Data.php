<?php

class Walkwizus_Sample_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_sampleProducts = array(
        '^S-',
    );

    private $_sampleKits = array(
        'D-Diagnostic-Pack',
        'D-Diagnostic-Giftcard'
    );

    public function isSample($sku)
    {
        return $this->isSampleProduct($sku) || $this->isSampleKit($sku);
    }

    public function isSampleProduct($sku)
    {
        foreach ($this->_sampleProducts as $sampleSku) {
            if (preg_match("/$sampleSku/", $sku)) {
                return true;
            }
        }
        return false;
    }

    public function isSampleKit($sku)
    {
        foreach ($this->_sampleKits as $sampleSku) {
            if (preg_match("/$sampleSku/", $sku)) {
                return true;
            }
        }
        return false;
    }

    public function checkSamplesMaxQtyInCart()
    {
        $maxQty = Mage::getStoreConfig('nose/sample/max_qty');
        $qty = 0;

        $quote = Mage::getModel('checkout/cart')->getQuote();
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                if (!$this->isSampleKit($item->getParentItem()->getSku())) {
                    continue;
                }
            }

            if ($this->isSampleProduct($item->getSku())) {
                $qty += $item->getQty();
            }
        }

        return $qty <= $maxQty;
    }

    public function checkSamplesUniqueQtyInCart($product = null)
    {
        $maxQty = Mage::getStoreConfig('nose/sample/max_unique_qty');
        $qties = array();

        $quote = Mage::getModel('checkout/cart')->getQuote();

        $products = array();
        if (!is_null($product)) {
            if ($this->isSampleProduct($product->getSku())) {
                $products[] = $product->getSku();
            }
            if ($this->isSampleKit($product->getSku())) {
                $productItem = $quote->getItemByProduct($product);
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getParentItemId() == $productItem->getId()) {
                        $products[] = $item->getSku();
                    }
                }
            }
        }

        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                if (!$this->isSampleKit($item->getParentItem()->getSku())) {
                    continue;
                }
            }

            if ($this->isSampleProduct($item->getSku())) {
                if (!isset($qties[$item->getSku()])) {
                    $qties[$item->getSku()] = 0;
                }
                $qties[$item->getSku()] += $item->getQty();

                if (empty($products) || in_array($item->getSku(), $products)) {
                    if ($qties[$item->getSku()] > $maxQty) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}