<?php

class Walkwizus_Phonemissing_IndexController extends Mage_Core_Controller_Front_Action
{
    public function saveBillingTelephoneAction()
    {
        $addressId = $this->getRequest()->getParam('addressId');
        $telephone = $this->getRequest()->getParam('telephone');

        $response = array();

        try {
            $address = Mage::getModel('customer/address')->load($addressId);
            $response['eee'] = $address->getFirstname();
            $address->setTelephone($telephone);
            $address->save();

            $response['success'] = true;
            $response['message'] = $this->__('Phone number was saved with success.');
        } catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(Zend_Json::encode($response));
    }
}