<?php

class Walkwizus_Callrequest_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CALLREQUEST_ZENDESK_ENABLED = 'callrequest/zendesk/enabled';
    const XML_PATH_CALLREQUEST_ZENDESK_API_SUBDOMAIN = 'callrequest/zendesk/api_subdomain';
    const XML_PATH_CALLREQUEST_ZENDESK_API_USER = 'callrequest/zendesk/api_user';
    const XML_PATH_CALLREQUEST_ZENDESK_API_TOKEN = 'callrequest/zendesk/api_token';

    private $_customerSession = null;

    public function isZendeskEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_CALLREQUEST_ZENDESK_ENABLED);
    }

    public function getApiSubdomain()
    {
        return Mage::getStoreConfig(self::XML_PATH_CALLREQUEST_ZENDESK_API_SUBDOMAIN);
    }

    public function getApiUser()
    {
        return Mage::getStoreConfig(self::XML_PATH_CALLREQUEST_ZENDESK_API_USER);
    }

    public function getApiToken()
    {
        return Mage::getStoreConfig(self::XML_PATH_CALLREQUEST_ZENDESK_API_TOKEN);
    }

    private function _getSession()
    {
        if ($this->_customerSession == null) {
            $this->_customerSession = Mage::getSingleton('customer/session');
        }

        return $this->_customerSession;
    }

    public function isLoggedIn()
    {
        return $this->_getSession()->isLoggedIn();
    }

    public function getDefaultTelephone()
    {
        $address = $this->_getSession()->getCustomer()->getPrimaryBillingAddress();
        if ($address) {
            return $address->getTelephone();
        }
        return '';
    }
}