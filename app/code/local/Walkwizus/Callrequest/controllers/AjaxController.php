<?php

class Walkwizus_Callrequest_AjaxController extends Mage_Core_Controller_Front_Action
{
    private function _sendCallRequest($customer, $phone)
    {
        if (Mage::helper('callrequest')->isZendeskEnabled()) {
            Mage::getModel('callrequest/zendesk')->createTicket($customer, $phone);
        }

        Mage::getModel('callrequest/zendesk')->sendEmailCallRequest($customer, $phone);
    }

    public function createCustomerAction()
    {
        $data = $this->getRequest()->getParam('phone');

        if ($data['firstname'] && $data['lastname'] && $data['email'] && $data['password'] && $data['phone_number']) {
            try {
                $customer = Mage::getModel('customer/customer');
                $customer->setFirstname($data['firstname']);
                $customer->setLastname($data['lastname']);
                $customer->setEmail($data['email']);
                $customer->setPassword($data['password']);
                $customer->save();

                $loadCustomer = Mage::getModel('customer/customer')->load($customer->getId());

                Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($loadCustomer);

                $this->_sendCallRequest($customer, $data['phone_number']);
                Mage::register('callrequest_phone_number', $data['phone_number']);
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
            }
        }

        $this->loadLayout(false);
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function loginPostAction()
    {
        $data = $this->getRequest()->getParam('login');

        if ($data['username'] && $data['password']) {
            $session = Mage::getSingleton('customer/session');
            try {
                $session->login($data['username'], $data['password']);
                $customer = $session->getCustomer();
                $session->setCustomerAsLoggedIn($customer);
            } catch(Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
            }
        }

        $this->loadLayout(false);
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function confirmCallRequestAction()
    {
        $data = $this->getRequest()->getParam('phone');
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn() && $data['phone_number']) {
            $this->_sendCallRequest($session->getCustomer(), $data['phone_number']);
        }

        Mage::register('callrequest_phone_number', $data['phone_number']);

        $this->loadLayout(false);
        $this->renderLayout();
    }
}