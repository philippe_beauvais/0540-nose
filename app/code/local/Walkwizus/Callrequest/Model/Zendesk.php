<?php

use Zendesk\API\HttpClient as ZendeskAPI;

class Walkwizus_Callrequest_Model_Zendesk extends Mage_Core_Model_Abstract
{
    private $_client = null;

    private function _getClient()
    {
        if ($this->_client == null) {
            $this->_client = new ZendeskAPI(Mage::helper('callrequest')->getApiSubdomain());
            $this->_client->setAuth('basic', [
                'username' => Mage::helper('callrequest')->getApiUser(),
                'token' => Mage::helper('callrequest')->getApiToken(),
            ]);
        }
        return $this->_client;
    }

    public function sendEmailCallRequest($customer, $phone)
    {
        $locale = Mage::getStoreConfig('general/locale/code',$customer->getStoreId());
        $content = 'Chère équipe, le client ' . $customer->getFirstname() . ' ' . $customer->getLastname() . ' a demandé à être rappellé au ' . $phone . ' en langue: ' . $locale;

        $mail = new Zend_Mail('UTF-8');
        $mail->setBodyHtml($content);
        $mail->setFrom($customer->getEmail());
        $mail->addTo('info@nose.fr');
        $mail->setSubject('Demande de rappel');

        $mail->send();
    }

    public function createTicket($customer, $phone)
    {
        $locale = Mage::getStoreConfig('general/locale/code',$customer->getStoreId());
        $content = 'Chère équipe, le client ' . $customer->getFirstname() . ' ' . $customer->getLastname() . ' a demandé à être rappellé au ' . $phone . ' en langue: ' . $locale;

        $client = $this->_getClient();

        $ticket = $client->tickets()->create([
            'subject' => 'Demande de rappel',
            'comment' => [
                'body' => $content
            ],
            'priority' => 'normal'
        ]);

        return $ticket;
    }
}