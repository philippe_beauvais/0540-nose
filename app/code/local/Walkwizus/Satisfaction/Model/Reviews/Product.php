<?php

class Walkwizus_Satisfaction_Model_Reviews_Product extends Partikule_Mynose_Model_Abstract
{
    private $_lang = array(
        'fr_FR' => 'fr',
        'en_US' => 'en'
    );

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mynose_read');
    }

    public function getReviewsByProduct($id)
    {
        $localeCode = Mage::getStoreConfig('general/locale/code', Mage::app()->getStore()->getId());
        $select = $this->getReadConnection()
            ->select()
            ->from(array('arp' => 'av_reviews_product'))
            ->where('arp.id_product = ?', $id)
            ->where('arp.lang = ?', $this->_lang[$localeCode]);

        $query = $select->query();
        $result = $query->fetchAll();

        return $result;
    }
}