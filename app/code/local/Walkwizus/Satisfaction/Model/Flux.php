<?php

class Walkwizus_Satisfaction_Model_Flux extends Partikule_Mynose_Model_Abstract
{
    private $_entities = array('customer_retail', 'customer_online', 'diag_retail', 'sample_buyer');

    private $_idSurvey = array(1, 2, 3, 4);

    private $_preferedLang = array('fr', 'en');

    private $_targetDayData = array();

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mynose_read');

        foreach ($this->_idSurvey as $id) {
            foreach ($this->_preferedLang as $lang) {
                $this->_setReadTargetDayData($id, $lang);
            }
        }
    }

    public function sendFile()
    {
        $exportDir = Mage::getBaseDir('var') . DS . 'export' . DS . 'avisverifies' . DS;

        foreach ($this->_entities as $entityName) {
            $csv = new Varien_File_Csv();
            $csv->setDelimiter(';');
            $csv->setEnclosure('"');

            $entity = Mage::getModel('satisfaction/flux_' . $entityName);

            $dataCsv = $entity->getDataCsv();
            foreach ($dataCsv as $lang => $data) {
                $csv->saveData($exportDir . $entity->getFilename($lang), $data);
                $this->sendToFtp($entity->getRemotePath($lang) . $entity->getFilename($lang), $exportDir . $entity->getFilename($lang));
            }
        }
    }

    public function getFile()
    {
        set_time_limit(0);

        $importDir = Mage::getBaseDir('var') . DS . 'import' . DS . 'avisverifies' . DS;
        $remoteDirectory = DS . 'reviews' . DS;

        $resource = $this->_getFtpResource();
        ftp_pasv($resource, true);
        ftp_chdir($resource, $remoteDirectory);
        $remoteFiles = ftp_nlist($resource, "");

        foreach ($remoteFiles as $remoteFile) {
            $localFile = $importDir . $remoteFile;
            $handle = fopen($localFile, 'w');
            ftp_fget($resource, $handle, $remoteDirectory . $remoteFile, FTP_ASCII, 0);
            fclose($handle);

            if (file_exists($localFile)) {
                if (preg_match("/product/", $remoteFile)) {
                    Mage::getModel('satisfaction/import_product')->saveData($localFile);
                }

                if (preg_match("/site/", $remoteFile)) {
                    Mage::getModel('satisfaction/import_site')->saveData($localFile);
                }

                unlink($localFile);
                ftp_rename($resource, $remoteFile, '../archives/reviews/' . $remoteFile);
            }
        }

        ftp_close($resource);
    }

    private function _getFtpResource()
    {
        $helper = Mage::helper('satisfaction');
        $resource = ftp_connect($helper->getFtpHost());
        ftp_login($resource, $helper->getFtpUsername(), $helper->getFtpPassword());

        return $resource;
    }

    public function sendToFtp($remotePath, $localPath)
    {
        $resource = $this->_getFtpResource();

        ftp_put($resource, $remotePath, $localPath, FTP_BINARY);
        ftp_close($resource);

        return $this;
    }

    protected function _getTargetDayData($idSurvey)
    {
        if (isset($this->_targetDayData[$idSurvey])) {
            return $this->_targetDayData[$idSurvey];
        }
    }

    private function _setReadTargetDayData($id, $lang)
    {
        $select = $this->getReadConnection()
            ->select()
            ->from(array('atd' => 'av_target_day'))
            ->where('atd.id_survey = ?', $id)
            ->where('atd.prefered_lang = ?', $lang);
        $query = $select->query();
        $result = $query->fetchAll();

        $this->_targetDayData[$id][$lang] = $result;
    }

    public function testFlux()
    {
        return $this->_targetDayData;
    }
}