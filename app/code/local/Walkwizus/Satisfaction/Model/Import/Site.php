<?php

class Walkwizus_Satisfaction_Model_Import_Site extends Walkwizus_Satisfaction_Model_Import_Abstract
{
    public function saveData($file)
    {
        $data = $this->getCsvData($file, "\t");
        $write = $this->getWriteConnection();

        foreach ($data as $v) {
            $write->insertIgnore('av_reviews_site', array(
                'action' => $v['action'],
                'review_id' => $v['review_id'],
                'email' => $v['email'],
                'lastname' => $v['lastname'],
                'firstname' => $v['firstname'],
                'review_date' => $this->_formatDate($v['review_date']),
                'review' => $v['review'],
                'rate' => $v['rate'],
                'order_ref' => $v['order_ref'],
                'nb_comments' => $v['nb_comments'],
            ));
        }
    }
}