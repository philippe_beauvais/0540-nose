<?php

class Walkwizus_Satisfaction_Model_Import_Abstract extends Partikule_Mynose_Model_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mynose_write');
    }

    public function getCsvData($file, $delimiter = ",")
    {
        $datas = array();
        if (($handle = fopen($file, 'r')) !== false) {
            $i = 0;
            while (($line = fgetcsv($handle, 0, $delimiter)) !== false) {
                $i++;

                if ($i == 1) {
                    $columns = $line;
                    continue;
                }

                $data = array();
                foreach ($line as $k => $v) {
                    $data[$columns[$k]] = $v;
                }

                $datas[] = $data;
            }
        }

        return $datas;
    }

    protected function _formatDate($date)
    {
        $datetime = new DateTime($date);
        return (string)$datetime->format('Y-m-d H:i:s');
    }

}