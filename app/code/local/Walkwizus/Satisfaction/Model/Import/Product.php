<?php

class Walkwizus_Satisfaction_Model_Import_Product extends Walkwizus_Satisfaction_Model_Import_Abstract
{
    public function saveData($file)
    {
        $data = $this->getCsvData($file, "\t");
        $write = $this->getWriteConnection();

        foreach ($data as $v) {
            $write->insertIgnore('av_reviews_product', array(
                'action' => $v['action'],
                'review_id' => $v['review_id'],
                'product_review_id' => $v['product_review_id'],
                'order_ref' => $v['order_ref'],
                'id_product' => $v['id_product'],
                'review_date' => $this->_formatDate($v['review_date']),
                'review' => $v['review'],
                'rate' => $v['rate'],
                'lastname' => $v['lastname'],
                'firstname' => $v['firstname'],
                'email' => $v['email'],
                'nb_comments' => $v['nb_comments'],
            ));
        }
    }
}