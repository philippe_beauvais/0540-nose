<?php

interface Walkwizus_Satisfaction_Model_Flux_Interface
{
    public function getFilename($lang);

    public function getDataCsv();

    public function getRemotePath($lang);
}