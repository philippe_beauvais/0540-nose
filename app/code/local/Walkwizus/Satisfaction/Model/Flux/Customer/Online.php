<?php

class Walkwizus_Satisfaction_Model_Flux_Customer_Online extends Walkwizus_Satisfaction_Model_Flux implements Walkwizus_Satisfaction_Model_Flux_Interface
{
    private $_idSurvey = 2;

    public function getFilename($lang)
    {
        return 'customer_online_' . $lang . '_' . date('Ymd') . '.csv';
    }

    public function getRemotePath($lang)
    {
        return DS . 'orders' . DS . 'public' . DS . $lang . DS;
    }

    public function getDataCsv()
    {
        $targetData = $this->_getTargetDayData($this->_idSurvey);
        $dataCsv = array();

        foreach ($targetData as $lang => $datas) {
            $dataCsv[$lang][] = array(
                'id_survey',
                'email',
                'order_ref',
                'order_date',
                'delay',
                'langue',
                'civilite',
                'lastname',
                'firstname',
                'id_product',
                'name_product',
                'url_product',
                'url_image_product',
                'gtin_ean',
                'sku'
            );

            foreach ($datas as $data) {
                $addData = json_decode($data['add_data']);

                if (is_object($addData)) {
                    foreach ($addData->master as $key => $idProduct) {
                        $product = Mage::getModel('catalog/product')
                            ->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('type_id', 'configurable')
                            ->addAttributeToFilter('master_id', $idProduct)
                            ->getFirstItem();

                        if ($product->getId()) {
                            $dataCsv[$lang][] = array(
                                'id_survey' => $this->_idSurvey,
                                'email' => $addData->email,
                                'order_ref' => $addData->order_ref,
                                'order_date' => $addData->order_date,
                                'delay' => 0,
                                'langue' => $addData->langue,
                                'civilite' => $addData->civilite,
                                'lastname' => $addData->last_name,
                                'firstname' => $addData->first_name,
                                'id_product' => $idProduct,
                                'name_product' => $product->getName(),
                                'url_product' => Mage::helper('satisfaction/product')->getCategoryUrlByProduct($product),
                                'url_image_product' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage(),
                                'gtin_ean' => $product->getGtinEan(),
                                'sku' => $product->getSku(),
                            );
                        }
                    }
                }
            }
        }

        return $dataCsv;
    }
}