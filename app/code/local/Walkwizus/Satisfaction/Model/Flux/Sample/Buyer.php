<?php

class Walkwizus_Satisfaction_Model_Flux_Sample_Buyer extends Walkwizus_Satisfaction_Model_Flux implements Walkwizus_Satisfaction_Model_Flux_Interface
{
    private $_idSurvey = 3;

    public function getFilename($lang)
    {
        return 'sample_kit_' . $lang . '_' . date('Ymd') . '.csv';
    }

    public function getRemotePath($lang)
    {
        return DS . 'orders' . DS . 'off' . DS . $lang . DS;
    }

    public function getDataCsv()
    {
        $targetData = $this->_getTargetDayData($this->_idSurvey);
        $dataCsv = array();

        foreach ($targetData as $lang => $datas) {
            $dataCsv[$lang][] = array(
                'id_survey',
                'email',
                'order_ref',
                'order_date',
                'delay',
                'langue',
                'civilite',
                'lastname',
                'firstname',
            );

            foreach ($datas as $data) {
                $addData = json_decode($data['add_data']);

                if (is_object($addData)) {
                    $dataCsv[$lang][] = array(
                        'id_survey' => $this->_idSurvey,
                        'email' => $addData->email,
                        'order_ref' => $addData->order_ref,
                        'order_date' => $addData->order_date,
                        'delay' => 0,
                        'langue' => $addData->langue,
                        'civilite' => $addData->civilite,
                        'lastname' => $addData->last_name,
                        'firstname' => $addData->first_name,
                    );
                }
            }
        }

        return $dataCsv;
    }
}