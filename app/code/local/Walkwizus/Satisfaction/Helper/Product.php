<?php

class Walkwizus_Satisfaction_Helper_Product extends Mage_Core_Helper_Abstract
{
    public function getCategoryUrlByProduct($product)
    {
        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('entity_id', $product->getCategoryIds())
            ->addAttributeToFilter('is_active', 1);

        $collection->getSelect()->order('level DESC')->limit(1);

        $registeredCategory = Mage::registry('current_category');
        Mage::unregister('current_category');
        Mage::register('current_category', $collection->getFirstItem());

        $url = $product->getProductUrl();
        Mage::unregister('current_category');

        if ($registeredCategory) {
            Mage::register('current_category', registeredCategory);
        }

        return $url;
    }
}