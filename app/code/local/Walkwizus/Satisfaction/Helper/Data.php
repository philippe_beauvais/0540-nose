<?php

class Walkwizus_Satisfaction_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SATISFACTION_FTP_HOST = 'satisfaction/general/ftp_host';

    const XML_PATH_SATISFACTION_FTP_USERNAME = 'satisfaction/general/ftp_user';

    const XML_PATH_SATISFACTION_FTP_PASSWORD = 'satisfaction/general/ftp_password';

    public function getFtpHost()
    {
        return Mage::getStoreConfig(self::XML_PATH_SATISFACTION_FTP_HOST);
    }

    public function getFtpUsername()
    {
        return Mage::getStoreConfig(self::XML_PATH_SATISFACTION_FTP_USERNAME);
    }

    public function getFtpPassword()
    {
        return Mage::getStoreConfig(self::XML_PATH_SATISFACTION_FTP_PASSWORD);
    }
}