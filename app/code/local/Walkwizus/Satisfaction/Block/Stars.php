<?php

class Walkwizus_Satisfaction_Block_Stars extends Walkwizus_Satisfaction_Block_Review
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('catalog/product/list/stars.phtml');
    }
}