<?php

class Walkwizus_Satisfaction_Block_Review extends Mage_Core_Block_Template
{
    public function getReviews()
    {
        $product = Mage::registry('product');
        if ($product) {
            return Mage::getModel('satisfaction/reviews_product')->getReviewsByProduct($product->getMasterId());
        }

        return false;
    }

    public function getReviewByProduct($product)
    {
        if ($product) {
            return Mage::getModel('satisfaction/reviews_product')->getReviewsByProduct($product->getMasterId());
        }

        return false;
    }

    public function getAvgReviews($product)
    {
        $reviews = $this->getReviewByProduct($product);

        if (count($reviews) > 0) {
            $total = 0;
            foreach ($reviews as $review) {
                $total += $review['rate'];
            }

            return $total / count($reviews);
        }
        return false;
    }
}