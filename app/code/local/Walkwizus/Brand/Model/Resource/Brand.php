<?php

class Walkwizus_Brand_Model_Resource_Brand extends Mage_Catalog_Model_Resource_Abstract
{
    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('brand')
            ->setConnection(
                $resource->getConnection('brand_read'),
                $resource->getConnection('brand_write')
            );
    }

    public function getMainTable()
    {
        return $this->getEntityTable();
    }
}