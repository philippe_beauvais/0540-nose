<?php

class Walkwizus_Brand_Model_Resource_Brand_Post extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('brand/brand_post', 'rel_id');
    }

    public function saveBrandRelation($brand, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('brand_id=?', $brand->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $postId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brand->getId(),
                    'post_id' => $postId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function savePostRelation($post, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('post_id=?', $post->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $brandId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brandId,
                    'post_id' => $post->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
