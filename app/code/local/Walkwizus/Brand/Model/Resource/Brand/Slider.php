<?php

class Walkwizus_Brand_Model_Resource_Brand_Slider extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('brand/brand_slider', 'rel_id');
    }

    public function saveBrandRelation($brand, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('brand_id=?', $brand->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $sliderId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brand->getId(),
                    'slider_id' => $sliderId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function saveSliderRelation($slider, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('slider_id=?', $slider->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $brandId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'brand_id' => $brandId,
                    'slider_id' => $slider->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
