<?php

class Walkwizus_Brand_Model_Resource_Brand_Slider_Collection extends Walkwizus_Slider_Model_Resource_Slider_Collection
{
    protected $_joinedFields = false;

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('brand/brand_slider')),
                'related.slider_id = main_table.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addBrandFilter($brand)
    {
        if ($brand instanceof Walkwizus_Brand_Model_Brand) {
            $brand = $brand->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.brand_id = ?', $brand);
        return $this;
    }
}
