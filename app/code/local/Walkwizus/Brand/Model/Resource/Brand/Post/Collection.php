<?php

class Walkwizus_Brand_Model_Resource_Brand_Post_Collection extends Walkwizus_News_Model_Resource_Post_Collection
{
    protected $_joinedFields = false;

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('brand/brand_post')),
                'related.post_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addBrandFilter($brand)
    {
        if ($brand instanceof Walkwizus_Brand_Model_Brand) {
            $brand = $brand->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.brand_id = ?', $brand);
        return $this;
    }
}
