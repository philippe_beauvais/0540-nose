<?php

class Walkwizus_Brand_Model_Resource_Brand_Collection extends Mage_Catalog_Model_Resource_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('brand/brand');
    }
}