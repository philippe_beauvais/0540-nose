<?php

class Walkwizus_Brand_Model_Resource_Setup extends Mage_Catalog_Model_Resource_Setup
{
    public function getDefaultEntities()
    {

        $entities = array(
            'brand' => array(
                'entity_model' => 'brand/brand',
                'attribute_model' => 'brand/resource_eav_attribute',
                'table' => 'brand/entity',
                'additional_attribute_table' => 'brand/eav_attribute',
                'entity_attribute_collection' => 'brand/brand_attribute_collection',
                'attributes' => array(
                    'title' => array(
                        'group' => 'General',
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Title',
                        'input' => 'text',
                        'source' => '',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required' => '1',
                        'user_defined' => false,
                        'default' => '',
                        'unique' => false,
                        'position' => 10,
                        'note' => '',
                        'visible' => '1',
                        'wysiwyg_enabled' => '0',
                    ),
                    'top_content' => array(
                        'group' => 'General',
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Top Content',
                        'input' => 'textarea',
                        'source' => '',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required' => '',
                        'user_defined' => false,
                        'default' => '',
                        'unique' => false,
                        'position' => 20,
                        'note' => '',
                        'visible' => '1',
                        'wysiwyg_enabled' => '0',
                    ),
                    'image' => array(
                        'group' => 'General',
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Image',
                        'input' => 'mediachooser',
                        'source' => '',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required' => '0',
                        'user_defined' => false,
                        'default' => '',
                        'unique' => false,
                        'position' => '17',
                        'note' => '',
                        'visible' => true,
                        'wysiwyg_enabled' => false,
                    ),
                    'bottom_content' => array(
                        'group' => 'General',
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Bottom Content',
                        'input' => 'textarea',
                        'source' => '',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required' => '',
                        'user_defined' => false,
                        'default' => '',
                        'unique' => false,
                        'position' => 20,
                        'note' => '',
                        'visible' => '1',
                        'wysiwyg_enabled' => '0',
                    )
                ),
            ),
        );

        return $entities;
    }
}