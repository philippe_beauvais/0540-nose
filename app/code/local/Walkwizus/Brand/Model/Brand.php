<?php

class Walkwizus_Brand_Model_Brand extends Mage_Catalog_Model_Abstract
{
    const ENTITY = 'brand';
    const CACHE_TAG = 'brand';

    protected $_eventPrefix = 'brand_brand';
    protected $_eventObject = 'brand';
    protected $_postInstance = null;
    protected $_sliderInstance = null;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('brand/brand');
    }

    public function getDefaultAttributeSetId()
    {
        return $this->getResource()->getEntityType()->getDefaultAttributeSetId();
    }

    public function getSelectedPosts()
    {
        if (!$this->hasSelectedPosts()) {
            $posts = array();
            foreach ($this->getSelectedPostsCollection() as $post) {
                $posts[] = $post;
            }
            $this->setSelectedPosts($posts);
        }
        return $this->getData('selected_posts');
    }

    public function getSelectedPostsCollection()
    {
        $collection = $this->getPostInstance()->getPostCollection($this);
        return $collection;
    }

    public function getSelectedSliders()
    {
        if (!$this->hasSelectedSliders()) {
            $sliders = array();
            foreach ($this->getSelectedSlidersCollection() as $slider) {
                $sliders[] = $slider;
            }
            $this->setSelectedSliders($sliders);
        }
        return $this->getData('selected_sliders');
    }

    public function getSelectedSlidersCollection()
    {
        $collection = $this->getSliderInstance()->getSliderCollection($this);
        return $collection;
    }

    protected function _afterSave()
    {
        $this->getPostInstance()->saveBrandRelation($this);
        $this->getSliderInstance()->saveBrandRelation($this);
        return parent::_afterSave();
    }

    public function getPostInstance()
    {
        if (!$this->_postInstance) {
            $this->_postInstance = Mage::getSingleton('brand/brand_post');
        }
        return $this->_postInstance;
    }

    public function getSliderInstance()
    {
        if (!$this->_sliderInstance) {
            $this->_sliderInstance= Mage::getSingleton('brand/brand_slider');
        }
        return $this->_sliderInstance;
    }
}