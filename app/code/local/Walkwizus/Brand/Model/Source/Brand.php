<?php

class Walkwizus_Brand_Model_Source_Brand extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        $collection = Mage::getModel('brand/brand')
            ->getCollection()
            ->addAttributeToSelect('title');

        $options = array(-1 => 'Select brand');
        foreach ($collection as $brand) {
            $options[$brand->getId()] = $brand->getTitle();
        }

        return $options;
    }
}