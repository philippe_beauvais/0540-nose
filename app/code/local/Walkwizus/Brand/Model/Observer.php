<?php

class Walkwizus_Brand_Model_Observer
{
    public function addBrandHandle($observer)
    {
        if ($observer->getAction()->getFullActionName() == 'catalog_category_view') {
            $brand = Mage::registry('current_category')->getBrand() ;
            if ($brand && $brand != -1) {
                $layout = $observer->getLayout();
                $layout->getUpdate()->addHandle('category_brand_view');
            }
        }
    }
}