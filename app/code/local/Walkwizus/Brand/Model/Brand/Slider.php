<?php

class Walkwizus_Brand_Model_Brand_Slider extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brand/brand_slider');
    }

    public function saveBrandRelation($brand)
    {
        $data = $brand->getSlidersData();
        if (!is_null($data)) {
            $this->_getResource()->saveBrandRelation($brand, $data);
        }
        return $this;
    }

    public function getSliderCollection($brand)
    {
        $collection = Mage::getResourceModel('brand/brand_slider_collection')
            ->addBrandFilter($brand);
        return $collection;
    }
}
