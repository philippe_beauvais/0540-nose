<?php

class Walkwizus_Brand_Model_Brand_Post extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brand/brand_post');
    }

    public function saveBrandRelation($brand)
    {
        $data = $brand->getPostsData();
        if (!is_null($data)) {
            $this->_getResource()->saveBrandRelation($brand, $data);
        }
        return $this;
    }

    public function getPostCollection($brand)
    {
        $collection = Mage::getResourceModel('brand/brand_post_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addBrandFilter($brand);
        $collection->getSelect()->order('position ASC');
        return $collection;
    }
}
