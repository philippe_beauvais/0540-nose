<?php

class Walkwizus_Brand_Adminhtml_BrandController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Brands'))
            ->_title($this->__('Manage Brands'));

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _initBrand()
    {
        $this->_title($this->__('Brands'))
            ->_title($this->__('Manage Brands'));

        $brandId = (int) $this->getRequest()->getParam('entity_id');
        $brand = Mage::getModel('brand/brand')->setStoreId($this->getRequest()->getParam('store', 0));

        if ($brandId) {
            $brand->load($brandId);
        }

        Mage::register('brand_data', $brand);
        return $brand;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $entityId = (int)$this->getRequest()->getParam('entity_id');
        $brand = $this->_initBrand();

        if ($entityId && !$brand->getId()) {
            $this->_getSession()->addError($this->__('This brand no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        if ($data = Mage::getSingleton('adminhtml/session')->getPostData(true)) {
            $brand->setData($data);
        }

        $this->loadLayout();

        if ($brand->getId()) {
            if (!Mage::app()->isSingleStoreMode() && ($storeSwitcher = $this->getLayout()->getBlock('store_switcher'))) {
                $storeSwitcher->setDefaultStoreName($this->__('Default Values'))
                    ->setWebsiteIds($brand->getWebsiteIds())
                    ->setSwitchUrl($this->getUrl('*/*/*', array(
                        '_current' => true,
                        'active_tab' => null,
                        'tab' => null,
                        'store' => null,
                    )));
            }
        } else {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }

        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $brand = $this->_initBrand();
            $brandData = $this->getRequest()->getParam('brand', array());

            $brand->addData($brandData);
            $brand->setAttributeSetId($brand->getDefaultAttributeSetId());

            $posts = $this->getRequest()->getPost('posts', -1);
            if ($posts != -1) {
                $brand->setPostsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($posts));
            }

            $sliders = $this->getRequest()->getPost('sliders', -1);
            if ($sliders != -1) {
                $brand->setSlidersData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($sliders));
            }

            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $brand->setData($attributeCode, false);
                }
            }

            try {
                $brand->save();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('This brand was saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/', array('entity_id', $brand->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/', array('id', $this->getRequest()->getParam('id')));
                return;
            }
        }
    }

    public function postsAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('brand.edit.tab.post')
            ->setBrandPosts($this->getRequest()->getPost('brand_posts', null));
        $this->renderLayout();

    }

    public function postsgridAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.post')
            ->setBrandPosts($this->getRequest()->getPost('brand_posts', null));
        $this->renderLayout();
    }

    public function slidersAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('brand.edit.tab.slider')
            ->setBrandSliders($this->getRequest()->getPost('brand_sliders', null));
        $this->renderLayout();

    }

    public function slidersgridAction()
    {
        $this->_initBrand();
        $this->loadLayout();
        $this->getLayout()->getBlock('brand.edit.tab.slider')
            ->setBrandSliders($this->getRequest()->getPost('brand_sliders', null));
        $this->renderLayout();
    }

    public function deleteAction()
    {
        $brandId = $this->getRequest()->getParam('entity_id');

        if ($brandId) {
            try {
                Mage::getModel('brand/brand')->load($brandId)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Brand was successfully deleted'));
                $this->_redirect('*/*/');
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('entity_id' => $brandId));
            }
        }
    }

    public function massDeleteAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                Mage::getModel('brand/brand')->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Brand(s) was successfully removed'));
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}