<?php

$installer = $this;
$this->startSetup();

$installer->removeAttribute($this->getEntityTypeId('brand'), 'top_content');
$installer->removeAttribute($this->getEntityTypeId('brand'), 'bottom_content');

$installer->addAttribute('brand', 'top_content', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Top Content',
    'input' => 'textarea',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => 20,
    'note' => '',
    'visible' => '1',
    'wysiwyg_enabled' => '0',
));

$installer->addAttribute('brand', 'bottom_content', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Bottom Content',
    'input' => 'textarea',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => '',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => 20,
    'note' => '',
    'visible' => '1',
    'wysiwyg_enabled' => '0',
));

$installer->endSetup();