<?php

$installer = $this;
$this->startSetup();

$installer->removeAttribute($this->getEntityTypeId('brand'), 'image');

$installer->addAttribute('brand', 'image_desktop', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Brand Desktop',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '17',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('brand', 'image_mobile', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Brand Mobile',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '17',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->endSetup();