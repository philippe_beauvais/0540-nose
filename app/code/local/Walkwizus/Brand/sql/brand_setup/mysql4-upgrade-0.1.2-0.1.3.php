<?php


$installer = $this;
$this->startSetup();

$installer->addAttribute('brand', 'image_universe1', array(
    'group' => 'Universe',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Universe 1',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '10',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('brand', 'image_universe2', array(
    'group' => 'Universe',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Universe 2',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '20',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('brand', 'image_universe3', array(
    'group' => 'Universe',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Universe 3',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '30',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('brand', 'image_universe4', array(
    'group' => 'Universe',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Image Universe 4',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '40',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$this->endSetup();