<?php
$this->startSetup();
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'brand', array(
    'group' => 'General Information',
    'input' => 'select',
    'type' => 'text',
    'label' => 'Brand',
    'backend' => '',
    'source' => 'brand/source_brand',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'user_defined' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$this->endSetup();