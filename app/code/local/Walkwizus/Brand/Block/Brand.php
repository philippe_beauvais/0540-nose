<?php

class Walkwizus_Brand_Block_Brand extends Mage_Core_Block_Template
{
    public function getSubCategories()
    {
        return array(
            array(
                'filter' => 'perfume',
                'label' => $this->__('Perfumes')
            ),
            array(
                'filter' => 'cosmetics',
                'label' => $this->__('Cosmetics')
            ),
            array(
                'filter' => 'home',
                'label' => $this->__('Home')
            ),
        );
    }

    public function getBrands()
    {
        $brandCatId = Mage::helper('brand')->getBrandCategoryId();

        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('parent_id', $brandCatId)
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToSort('name', 'ASC');

        return $collection;
    }

    public function getBrandsLetters()
    {
        return array(
            'A' => false,
            'B' => false,
            'C' => false,
            'D' => false,
            'E' => false,
            'F' => false,
            'G' => false,
            'H' => false,
            'I' => false,
            'J' => false,
            'K' => false,
            'L' => false,
            'M' => false,
            'N' => false,
            'O' => false,
            'P' => false,
            'Q' => false,
            'R' => false,
            'S' => false,
            'T' => false,
            'U' => false,
            'V' => false,
            'W' => false,
            'X' => false,
            'Y' => false,
            'Z' => false
        );
    }
}