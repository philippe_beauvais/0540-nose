<?php

class Walkwizus_Brand_Block_Adminhtml_Brand_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'brand';
        $this->_controller = 'adminhtml_brand';
        parent::__construct();
    }

    public function getHeaderText()
    {

    }
}