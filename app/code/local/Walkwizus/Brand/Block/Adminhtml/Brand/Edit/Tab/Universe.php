<?php

class Walkwizus_Brand_Block_Adminhtml_Brand_Edit_Tab_Universe extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareLayout()
    {
        Varien_Data_Form::setElementRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );

        Varien_Data_Form::setFieldsetRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')
        );

        Varien_Data_Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock('news/adminhtml_post_renderer_fieldset_element')
        );
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setDataObject(Mage::registry('brand_data'));

        $general = $form->addFieldset('general', array(
            'legend' => $this->__('General Informations'),
            'class' => 'fieldset-wide',
        ));

        $general->addType('mediachooser','AntoineK_MediaChooserField_Data_Form_Element_Mediachooser');

        $attributes = $this->getAttributes();
        foreach ($attributes as $attribute) {
            $attribute->setEntity(Mage::getResourceModel('brand/brand'));
        }

        $this->_setFieldset($attributes, $general, array());
        $formValues = Mage::registry('brand_data')->getData();

        if (!Mage::registry('brand_data')->getId()) {
            foreach ($attributes as $attribute) {
                if (!isset($formValues[$attribute->getAttributeCode()])) {
                    $formValues[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                }
            }
        }

        $form->addValues($formValues);
        $form->setFieldNameSuffix('brand');
        $this->setForm($form);
    }

    public function getTabLabel()
    {
        return $this->__('General');
    }

    public function getTabTitle()
    {
        return $this->__('General');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}