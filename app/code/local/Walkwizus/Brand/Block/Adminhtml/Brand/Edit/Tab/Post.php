<?php

class Walkwizus_Brand_Block_Adminhtml_Brand_Edit_Tab_Post extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('post_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getBrand()->getId()) {
            $this->setDefaultFilter(array('in_posts' => 1));
        }
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('news/post_collection');
        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $collection->joinAttribute('post_title', 'news_post/title', 'entity_id', null, 'left', $adminStore);

        if ($this->getBrand()->getId()) {
            $constraint = '{{table}}.brand_id=' . $this->getBrand()->getId();
        } else {
            $constraint = '{{table}}.brand_id=0';
        }

        $collection->joinField(
            'position',
            'brand/brand_post',
            'position',
            'post_id=entity_id',
            $constraint,
            'left'
        );

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareMassaction()
    {
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_posts', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_posts',
            'values' => $this->_getSelectedPosts(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('post_title', array(
            'header' => Mage::helper('catalog')->__('Title'),
            'align' => 'left',
            'index' => 'post_title',
            'renderer' => 'brand/adminhtml_news_post_renderer_relation',
            'params' => array(
                'entity_id' => 'getId'
            ),
            'base_link' => 'adminhtml/post/edit',
        ));

        $this->addColumn('position', array(
            'header' => Mage::helper('brand')->__('Position'),
            'name' => 'position',
            'width' => 60,
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'editable' => true,
        ));
    }

    protected function _getSelectedPosts()
    {
        $posts = $this->getBrandPosts();

        if (!is_array($posts)) {
            $posts = array_keys($this->getSelectedPosts());
        }

        return $posts;
    }

    public function getSelectedPosts()
    {
        $posts = array();
        $selected = Mage::registry('brand_data')->getSelectedPosts();

        if (!is_array($selected)) {
            $selected = array();
        }

        foreach ($selected as $post) {
            $posts[$post->getId()] = array('position' => $post->getPosition());
        }

        return $posts;
    }

    public function getRowUrl($item)
    {
        return '#';
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/postsGrid', array(
            'id' => $this->getBrand()->getId()
        ));
    }

    public function getBrand()
    {
        return Mage::registry('brand_data');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_posts') {
            $postIds = $this->_getSelectedPosts();
            if (empty($postIds)) {
                $postIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $postIds));
            } else {
                if ($postIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $postIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }
}
