<?php

class Walkwizus_Brand_Block_Adminhtml_Brand_Edit_Tab_Slider extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getBrand()->getId()) {
            $this->setDefaultFilter(array('in_sliders' => 1));
        }
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('slider/slider_collection');

        if ($this->getBrand()->getId()) {
            $constraint = "bs.brand_id=" . $this->getBrand()->getId();
        } else {
            $constraint = "bs.brand_id=0";
        }

        $collection->getSelect()->joinLeft(array('bs' => $collection->getTable('brand/brand_slider')), "bs.slider_id=main_table.entity_id AND $constraint", array('position'));

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareMassaction()
    {
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_sliders', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_sliders',
            'values' => $this->_getSelectedSliders(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'align' => 'left',
            'index' => 'name',
            'renderer' => 'brand/adminhtml_slider_slider_renderer_relation',
            'params' => array(
                'id' => 'getId'
            ),
            'base_link' => 'adminhtml/slider/edit',
        ));

        $this->addColumn('position', array(
            'header' => Mage::helper('brand')->__('Position'),
            'name' => 'position',
            'width' => 60,
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'editable' => true,
        ));
    }

    protected function _getSelectedSliders()
    {
        $sliders = $this->getBrandSliders();

        if (!is_array($sliders)) {
            $sliders = array_keys($this->getSelectedSliders());
        }

        return $sliders;
    }

    public function getSelectedSliders()
    {
        $sliders = array();
        $selected = Mage::registry('brand_data')->getSelectedSliders();

        if (!is_array($selected)) {
            $selected = array();
        }

        foreach ($selected as $slider) {
            $sliders[$slider->getId()] = array('position' => $slider->getPosition());
        }

        return $sliders;
    }

    public function getRowUrl($item)
    {
        return '#';
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/slidersGrid', array(
            'id' => $this->getBrand()->getId()
        ));
    }

    public function getBrand()
    {
        return Mage::registry('brand_data');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_sliders') {
            $sliderIds = $this->_getSelectedSliders();
            if (empty($sliderIds)) {
                $sliderIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $sliderIds));
            } else {
                if ($sliderIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $sliderIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }
}
