<?php

class Walkwizus_Brand_Block_Adminhtml_Brand extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_brand';
        $this->_blockGroup = 'brand';

        parent::__construct();

        $this->_headerText = $this->__('Manage Brand');
        $this->_updateButton('add', 'label', $this->__('Add Brand'));

        $this->setTemplate('brand/grid.phtml');
    }
}