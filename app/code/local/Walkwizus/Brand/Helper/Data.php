<?php

class Walkwizus_Brand_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NOSE_CATEGORIES_BRANDS = 'nose/categories/brands';

    public function getThumbnailImageUrl($img)
    {
        return Mage::getBaseUrl('media') . 'catalog' . DS . 'category'. DS . $img;
    }

    public function isBrandCategory($category)
    {
        $brandCatId = Mage::getStoreConfig('nose/categories/brands');
        if ($brandCatId == $category->getId()) {
            return false;
        } else {
            $path = $category->getPath();
            $ids = explode('/', $path);
            if ($ids[2] == $brandCatId) {
                return true;
            }
        }
        return false;
    }

    public function getPersonUrl($person)
    {
        return $this->getUrlPrefix() . '/' . $person->getUrlKey() . '/' . $this->getUrlSuffix();
    }

    public function getBrandCategoryId()
    {
        return Mage::getStoreConfig(self::XML_PATH_NOSE_CATEGORIES_BRANDS);
    }
}