<?php

class Walkwizus_CanonicalProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getProductCanonicalUrl($product)
    {
        $category = Mage::helper('nose/product')->getProductFirstCategory($product);
        if ($category) {
            return $category->getUrl() . '/' . $product->getUrlKey();
        }

        return $product->getUrlModel()->getUrl($product, array('_ignore_category' => true));
    }
}