<?php

class Walkwizus_Nez_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    const NEZ_MODULE_NAME = 'nez';

    const NEZ_MODEL_PERSON = 'nez/person';

    public function initControllerRouters($observer)
    {
        $front = $observer->getEvent()->getFront();
        $front->addRouter(self::NEZ_MODULE_NAME, $this);
        return $this;
    }

    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $urlKey = trim($request->getPathInfo(), '/');

        $check = array();
        $check['nez'] = new Varien_Object(array(
            'prefix' => Mage::helper(self::NEZ_MODULE_NAME)->getUrlPrefix(),
            'suffix' => Mage::helper(self::NEZ_MODULE_NAME)->getUrlSuffix(),
            'list_key' => Mage::helper(self::NEZ_MODULE_NAME)->getUrlRewriteList(),
            'list_action' => 'list',
            'model' => self::NEZ_MODEL_PERSON,
            'controller' => 'person',
            'action' => 'view',
            'param' => 'id',
            'check_path' => 0,
        ));

        foreach ($check as $key => $value) {
            if ($value->getListKey()) {
                if ($urlKey == $value->getListKey()) {
                    $request->setModuleName(self::NEZ_MODULE_NAME)
                        ->setControllerName($value->getController())
                        ->setActionName($value->getListAction());

                    $request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $urlKey);
                    return true;
                }
            }

            if ($value->getPrefix()) {
                $parts = explode('/', $urlKey);
                if ($parts[0] != $value->getPrefix() || count($parts) != 2) {
                    continue;
                }
                $urlKey = $parts[1];
            }

            if ($value->getSuffix()) {
                $urlKey = substr($urlKey, 0, -strlen($value->getSuffix()) -1);
            }

            $model = Mage::getModel($value->getModel());
            $id = $model->checkUrlKey($urlKey, Mage::app()->getStore()->getId());

            if ($id) {
                if ($value->getCheckPath() && !$model->load($id)->getStatusPath()) {
                    continue;
                }
                $request->setModuleName(self::NEZ_MODULE_NAME)
                    ->setControllerName($value->getController())
                    ->setActionName($value->getAction())
                    ->setParam($value->getParam(), $id);

                $request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $urlKey);
                return true;
            }
        }

        return false;
    }
}