<?php

$installer = $this;
$this->startSetup();

$installer->addAttribute('nez', 'thumbnail_desktop', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Thumbnail Desktop',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '20',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->addAttribute('nez', 'thumbnail_mobile', array(
    'group' => 'General',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Thumbnail Mobile',
    'input' => 'mediachooser',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => '0',
    'user_defined' => false,
    'default' => '',
    'unique' => false,
    'position' => '20',
    'note' => '',
    'visible' => true,
    'wysiwyg_enabled' => false,
));

$installer->endSetup();