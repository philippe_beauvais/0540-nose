<?php

class Walkwizus_Nez_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NEZ_PERSON_URL_PREFIX = 'nez/person/url_prefix';

    const XML_PATH_NEZ_PERSON_URL_SUFFIX = 'nez/person/url_suffix';

    const XML_PATH_NEZ_PERSON_URL_REWRITE_LIST = 'nez/person/url_rewrite_list';

    public function getUrlPrefix()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEZ_PERSON_URL_PREFIX);
    }

    public function getUrlSuffix()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEZ_PERSON_URL_SUFFIX);
    }

    public function getUrlRewriteList()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEZ_PERSON_URL_REWRITE_LIST);
    }

    public function getThumbnailImageUrl($nose, $type = 'Desktop')
    {
        $method = 'getThumbnail' . $type;
        return Mage::getBaseUrl('media') . $nose->$method();
    }

    public function getNoseUrl($nose)
    {
        return $this->getUrlPrefix() . '/' . $nose->getUrlKey() . $this->getUrlSuffix();
    }
}