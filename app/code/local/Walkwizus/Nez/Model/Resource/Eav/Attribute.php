<?php

class Walkwizus_Nez_Model_Resource_Eav_Attribute extends Mage_Eav_Model_Entity_Attribute
{
    const MODULE_NAME = 'Walkwizus_Nez';
    const ENTITY = 'nez_person_eav_attribute';

    protected $_eventPrefix = 'nez_entity_attribute';
    protected $_eventObject = 'attribute';

    static protected $_labels = null;

    protected function _construct()
    {
        $this->_init('nez/attribute');
    }

    public function isScopeStore()
    {
        return $this->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE;
    }

    public function isScopeWebsite()
    {
        return $this->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE;
    }

    public function isScopeGlobal()
    {
        return (!$this->isScopeStore() && !$this->isScopeWebsite());
    }

    public function getBackendTypeByInput($type)
    {
        switch ($type) {
            case 'file':
            case 'image':
                return 'varchar';
                break;
            case 'multiselect':
                return 'text';
                break;
            default:
                return parent::getBackendTypeByInput($type);
                break;
        }
    }

    protected function _beforeDelete()
    {
        if (!$this->getIsUserDefined()) {
            throw new Mage_Core_Exception($this->__('This attribute is not deletable'));
        }
        return parent::_beforeDelete();
    }
}