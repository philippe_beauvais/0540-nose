<?php

class Walkwizus_Nez_Model_Resource_Person_Collection extends Mage_Catalog_Model_Resource_Collection_Abstract
{
    protected $_joinedFields = false;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('nez/person');
    }

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('nez/person_product')),
                'related.person_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addProductFilter($product)
    {
        if (!$this->_joinedFields) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.product_id = ?', $product->getId());
        return $this;
    }
}