<?php

class Walkwizus_Nez_Model_Resource_Person_Post extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('nez/person_post', 'rel_id');
    }

    public function savePersonRelation($person, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('person_id=?', $person->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $postId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $person->getId(),
                    'post_id' => $postId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function savePostRelation($post, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('post_id=?', $post->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $personId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $personId,
                    'post_id' => $post->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
