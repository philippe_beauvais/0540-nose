<?php

class Walkwizus_Nez_Model_Resource_Person_Slider extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('nez/person_slider', 'rel_id');
    }

    public function savePersonRelation($person, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('person_id=?', $person->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $sliderId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $person->getId(),
                    'slider_id' => $sliderId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function saveSliderRelation($slider, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('slider_id=?', $slider->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $personId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $personId,
                    'slider_id' => $slider->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
