<?php

class Walkwizus_Nez_Model_Resource_Person_Product extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function  _construct()
    {
        $this->_init('nez/person_product', 'rel_id');
    }

    public function savePersonRelation($person, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('person_id=?', $person->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $productId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $person->getId(),
                    'product_id' => $productId,
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }

    public function saveProductRelation($product, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('product_id=?', $product->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $personId => $info) {
            $this->_getWriteAdapter()->insert(
                $this->getMainTable(),
                array(
                    'person_id' => $personId,
                    'product_id' => $product->getId(),
                    'position' => @$info['position']
                )
            );
        }
        return $this;
    }
}
