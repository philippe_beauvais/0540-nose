<?php

class Walkwizus_Nez_Model_Resource_Person_Post_Collection extends Walkwizus_News_Model_Resource_Post_Collection
{
    protected $_joinedFields = false;

    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('nez/person_post')),
                'related.post_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addPersonFilter($person)
    {
        if ($person instanceof Walkwizus_Nez_Model_Person) {
            $person = $person->getId();
        }
        if (!$this->_joinedFields ) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.person_id = ?', $person);
        return $this;
    }
}
