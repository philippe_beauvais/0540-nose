<?php

class Walkwizus_Nez_Model_Person_Slider extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('nez/person_slider');
    }

    public function savePersonRelation($person)
    {
        $data = $person->getSlidersData();
        if (!is_null($data)) {
            $this->_getResource()->savePersonRelation($person, $data);
        }
        return $this;
    }

    public function getSliderCollection($person)
    {
        $collection = Mage::getResourceModel('nez/person_slider_collection')
            ->addPersonFilter($person);
        return $collection;
    }
}
