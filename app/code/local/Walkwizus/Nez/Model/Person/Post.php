<?php

class Walkwizus_Nez_Model_Person_Post extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('nez/person_post');
    }

    public function savePersonRelation($person)
    {
        $data = $person->getPostData();
        if (!is_null($data)) {
            $this->_getResource()->savePersonRelation($person, $data);
        }
        return $this;
    }

    public function getPostCollection($person)
    {
        $collection = Mage::getResourceModel('nez/person_post_collection')
            ->addPersonFilter($person);
        return $collection;
    }
}
