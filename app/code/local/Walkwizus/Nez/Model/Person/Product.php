<?php

class Walkwizus_Nez_Model_Person_Product extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('nez/person_product');
    }

    public function savePersonRelation($post)
    {
        $data = $post->getProductsData();
        if (!is_null($data)) {
            $this->_getResource()->savePersonRelation($post, $data);
        }
        return $this;
    }

    public function getProductCollection($post)
    {
        $collection = Mage::getResourceModel('nez/person_product_collection')
            ->addPersonFilter($post);
        return $collection;
    }
}
