<?php

class Walkwizus_Nez_Model_Person extends Mage_Catalog_Model_Abstract
{
    const ENTITY = 'person';
    const CACHE_TAG = 'nez_person';

    protected $_eventPrefix = 'nez_person';
    protected $_eventObject = 'person';

    protected $_productInstance = null;
    protected $_sliderInstance = null;
    protected $_postInstance = null;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('nez/person');
    }

    public function getDefaultAttributeSetId()
    {
        return $this->getResource()->getEntityType()->getDefaultAttributeSetId();
    }

    public function getAttributeText($attributeCode)
    {
        $text = $this->getResource()
            ->getAttribute($attributeCode)
            ->getSource()
            ->getOptionText($this->getData($attributeCode));
        if (is_array($text)) {
            return implode(', ', $text);
        }
        return $text;
    }

    public function getSelectedProducts()
    {
        if (!$this->hasSelectedProducts()) {
            $products = array();
            foreach ($this->getSelectedProductsCollection() as $product) {
                $products[] = $product;
            }
            $this->setSelectedProducts($products);
        }
        return $this->getData('selected_products');
    }

    public function getSelectedProductsCollection()
    {
        $collection = $this->getProductInstance()->getProductCollection($this);
        return $collection;
    }

    public function getSelectedSliders()
    {
        if (!$this->hasSelectedSliders()) {
            $sliders = array();
            foreach ($this->getSelectedSlidersCollection() as $slider) {
                $sliders[] = $slider;
            }
            $this->setSelectedSliders($sliders);
        }
        return $this->getData('selected_sliders');
    }

    public function getSelectedSlidersCollection()
    {
        $collection = $this->getSliderInstance()->getSliderCollection($this);
        return $collection;
    }

    public function getSelectedPosts()
    {
        if (!$this->hasSelectedPosts()) {
            $posts = array();
            foreach ($this->getSelectedPostsCollection() as $post) {
                $posts[] = $post;
            }
            $this->setSelectedPosts($posts);
        }
        return $this->getData('selected_posts');
    }

    public function getSelectedPostsCollection()
    {
        $collection = $this->getPostInstance()->getPostCollection($this);
        return $collection;
    }

    protected function _afterSave()
    {
        $this->getProductInstance()->savePersonRelation($this);
        $this->getSliderInstance()->savePersonRelation($this);
        $this->getPostInstance()->savePersonRelation($this);

        return parent::_afterSave();
    }

    public function getProductInstance()
    {
        if (!$this->_productInstance) {
            $this->_productInstance = Mage::getSingleton('nez/person_product');
        }
        return $this->_productInstance;
    }

    public function getSliderInstance()
    {
        if (!$this->_sliderInstance) {
            $this->_sliderInstance = Mage::getSingleton('nez/person_slider');
        }
        return $this->_sliderInstance;
    }

    public function getPostInstance()
    {
        if (!$this->_postInstance) {
            $this->_postInstance = Mage::getSingleton('nez/person_post');
        }
        return $this->_postInstance;
    }

    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    /**
     * @todo Dynamics Attributes
     */
    public function getAttributes()
    {

    }
}