<?php

class Walkwizus_Nez_Block_Person_Catalog_Product_View extends Mage_Core_Block_Template
{
    public function getAssociatedPerson()
    {
        $product = Mage::registry('current_product');

        $collection = Mage::getModel('nez/person')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addProductFilter($product);

        return $collection;
    }
}