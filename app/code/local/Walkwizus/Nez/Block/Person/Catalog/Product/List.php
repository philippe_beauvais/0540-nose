<?php

class Walkwizus_Nez_Block_Person_Catalog_Product_List extends Mage_Core_Block_Template
{
    public function getProductCollection()
    {
        $collection = $this->getPerson()->getSelectedProductsCollection();
        $collection->addAttributeToSelect('name');
        $collection->addUrlRewrite();
        $collection->getSelect()->order('related.position');

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $collection;
    }

    public function getPerson()
    {
        return Mage::registry('current_nose');
    }
}