<?php

class Walkwizus_Nez_Block_Person_List extends Mage_Core_Block_Template
{
    public function getNosesCollection()
    {
        $collection = Mage::getModel('nez/person')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', 1)
            ->addAttributeToSort('name', 'ASC');

        return $collection;
    }

    public function getNoseLetters()
    {
        return array(
            'A' => false,
            'B' => false,
            'C' => false,
            'D' => false,
            'E' => false,
            'F' => false,
            'G' => false,
            'H' => false,
            'I' => false,
            'J' => false,
            'K' => false,
            'L' => false,
            'M' => false,
            'N' => false,
            'O' => false,
            'P' => false,
            'Q' => false,
            'R' => false,
            'S' => false,
            'T' => false,
            'U' => false,
            'V' => false,
            'W' => false,
            'X' => false,
            'Y' => false,
            'Z' => false
        );
    }
}