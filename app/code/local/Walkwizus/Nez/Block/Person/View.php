<?php

class Walkwizus_Nez_Block_Person_View extends Mage_Core_Block_Template
{
    public function getCurrentNose()
    {
        return Mage::registry('current_nose');
    }
}