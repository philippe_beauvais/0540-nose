<?php

class Walkwizus_Nez_Block_Adminhtml_Person_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('personGrid');
        $this->setDefaultSort('entity_id');
        $this->setSaveParametersInSession(true);
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('nez/person')
            ->getCollection()
            ->addAttributeToSelect('name');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => $this->__('Person ID'),
            'align' => 'left',
            'index' => 'entity_id',
        ));

        $this->addColumn('name', array(
            'header' => $this->__('name'),
            'align' => 'left',
            'index' => 'name',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('person');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->__('Are you sure?'),
        ));
    }

    public function getRowUrl($item)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $item->getId()));
    }
}