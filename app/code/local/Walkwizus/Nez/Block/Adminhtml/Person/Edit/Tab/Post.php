<?php

class Walkwizus_Nez_Block_Adminhtml_Person_Edit_Tab_Post extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('post_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getPerson()->getId()) {
            $this->setDefaultFilter(array('in_post' => 1));
        }
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('news/post_collection')
            ->addAttributeToSelect('*');

        $tableName = Mage::getSingleton('core/resource')->getTableName('nez/person_post');
        if ($this->getPerson()->getId()) {
            $constraint = 'ps.person_id=' . $this->getPerson()->getId();
        } else {
            $constraint = 'ps.person_id=0';
        }

        $collection->getSelect()->joinLeft(array('ps' => $tableName), 'e.entity_id=ps.post_id AND ' . $constraint, array('position'));

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareMassaction()
    {
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_post', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_post',
            'values' => $this->_getSelectedPost(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('slider')->__('Name'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('position', array(
            'header' => Mage::helper('slider')->__('Position'),
            'name' => 'position',
            'width' => 60,
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'editable' => true,
        ));
    }

    protected function _getSelectedPost()
    {
        $post = $this->getPersonPost();

        if (!is_array($post)) {
            $post = array_keys($this->getSelectedPost());
        }

        return $post;
    }

    public function getSelectedPost()
    {
        $post = array();
        $selected = Mage::registry('person_data')->getSelectedPosts();

        if (!is_array($selected)) {
            $selected = array();
        }

        foreach ($selected as $p) {
            $post[$p->getId()] = array('position' => $p->getPosition());
        }

        return $post;
    }

    public function getRowUrl($item)
    {
        return '#';
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/postGrid', array(
            'id' => $this->getPerson()->getId()
        ));
    }

    public function getPerson()
    {
        return Mage::registry('person_data');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_post') {
            $postIds = $this->_getSelectedPost();
            if (empty($postIds)) {
                $postIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $postIds));
            } else {
                if ($postIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $postIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }
}
