<?php

class Walkwizus_Nez_Block_Adminhtml_Person_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'nez';
        $this->_controller = 'adminhtml_person';

        parent::__construct();
    }

    public function getHeaderText()
    {

    }
}