<?php

class Walkwizus_Nez_Block_Adminhtml_Person extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_person';
        $this->_blockGroup = 'nez';

        parent::__construct();

        $this->_headerText = $this->__('Manage Person');
        $this->_updateButton('add', 'label', $this->__('Add Person'));

        $this->setTemplate('nez/grid.phtml');
    }
}