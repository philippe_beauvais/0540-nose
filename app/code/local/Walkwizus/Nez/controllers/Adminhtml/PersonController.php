<?php

class Walkwizus_Nez_Adminhtml_PersonController extends Mage_Adminhtml_Controller_Action
{
    protected function _construct()
    {
        $this->setUsedModuleName('Walkwizus_Nez');
    }

    protected function _initPerson()
    {
        $this->_title($this->__('Person'))
            ->_title($this->__('Manage Person'));

        $personId = (int) $this->getRequest()->getParam('entity_id');
        $person = Mage::getModel('nez/person')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if ($personId) {
            $person->load($personId);
        }

        Mage::register('person_data', $person);
        return $person;
    }

    public function indexAction()
    {
        $this->_title($this->__('Nose'))
            ->_title($this->__('Manage Nose'));

        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $entityId = (int)$this->getRequest()->getParam('entity_id');
        $person = $this->_initPerson();

        if ($entityId && !$person->getId()) {
            $this->_getSession()->addError($this->__('This person no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        if ($data = Mage::getSingleton('adminhtml/session')->getPersonData(true)) {
            $person->setData($data);
        }

        $this->loadLayout();

        if ($person->getId()) {
            if (!Mage::app()->isSingleStoreMode() && ($storeSwitcher = $this->getLayout()->getBlock('store_switcher'))) {
                $storeSwitcher->setDefaultStoreName($this->__('Default Values'))
                    ->setWebsiteIds($person->getWebsiteIds())
                    ->setSwitchUrl($this->getUrl('*/*/*', array(
                        '_current' => true,
                        'active_tab' => null,
                        'tab' => null,
                        'store' => null,
                    )));
            }
        } else {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }

        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $person = $this->_initPerson();
            $personData = $this->getRequest()->getParam('person', array());

            $person->addData($personData);
            $person->setAttributeSetId($person->getDefaultAttributeSetId());

            $products = $this->getRequest()->getPost('products', -1);
            if ($products != -1) {
                $person->setProductsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($products));
            }

            $sliders = $this->getRequest()->getPost('sliders', -1);
            if ($sliders != -1) {
                $person->setSlidersData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($sliders));
            }

            $post = $this->getRequest()->getPost('post', -1);
            if ($post != -1) {
                $person->setPostData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($post));
            }

            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $person->setData($attributeCode, false);
                }
            }

            try {
                $person->save();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('This person was saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/', array('entity_id', $person->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/', array('id', $this->getRequest()->getParam('id')));
                return;
            }
        }
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('entity_id')) {
            try {
                Mage::getModel('nez/person')->load($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('The person has been deleted');
                $this->_redirect('*/*/index');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/index');
                return;
            }
        }
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('person');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select person(s)'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('nez/person')->load($id)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function productsAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('person.edit.tab.product')
            ->setPersonProducts($this->getRequest()->getPost('person_products', null));
        $this->renderLayout();
    }

    public function slidersAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('person.edit.tab.slider')
            ->setPersonSliders($this->getRequest()->getPost('person_sliders', null));
        $this->renderLayout();

    }

    public function postAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()
            ->getBlock('person.edit.tab.post')
            ->setPersonPost($this->getRequest()->getPost('person_post', null));
        $this->renderLayout();

    }

    public function productsgridAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()->getBlock('person.edit.tab.product')
            ->setPersonProducts($this->getRequest()->getPost('person_products', null));
        $this->renderLayout();
    }

    public function slidersgridAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()->getBlock('person.edit.tab.slider')
            ->setPersonSliders($this->getRequest()->getPost('person_sliders', null));
        $this->renderLayout();
    }

    public function postgridAction()
    {
        $this->_initPerson();
        $this->loadLayout();
        $this->getLayout()->getBlock('person.edit.tab.post')
            ->setPersonPost($this->getRequest()->getPost('person_post', null));
        $this->renderLayout();
    }

    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('nez/adminhtml_person_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id' => $storeId,
            'store_media_url' => $storeMediaUrl,
        ));

        $this->getResponse()->setBody($content->toHtml());
    }
}