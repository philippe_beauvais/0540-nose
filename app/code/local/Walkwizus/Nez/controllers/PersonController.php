<?php

class Walkwizus_Nez_PersonController extends Mage_Core_Controller_Front_Action
{
    protected function _initPerson()
    {
        $id = $this->getRequest()->getParam('id', 0);
        $person = Mage::getModel('nez/person')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($id);

        if (!$person->getId()) {
            return false;
        } else if (!$person->getStatus()) {
            return false;
        }

        return $person;
    }
    
    public function listAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $person = $this->_initPerson();

        if (!$person) {
            $this->_forward('no-route');
            return;
        }

        Mage::register('current_nose', $person);

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');

        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('nez-person nose-person-' . $person->getId());
        }

        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $person->getUrl());
            if ($person->getMetaTitle()) {
                $headBlock->setTitle($person->getMetaTitle());
            } else {
                $headBlock->setTitle($person->getTitle());
            }
            $headBlock->setKeywords($person->getMetaKeywords());
            $headBlock->setMetaDescription($person->getMetaDescription());
        }
        $this->renderLayout();
    }
}