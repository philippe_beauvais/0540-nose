<?php

$installer = $this;

$table = $installer->getConnection()->newTable($installer->getTable('freesample/birthdayoffer'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID'
);

$table->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Customer ID');

$table->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Order ID');

$table->addColumn('year', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Year');

$installer->getConnection()->createTable($table);

$installer->endSetup();

