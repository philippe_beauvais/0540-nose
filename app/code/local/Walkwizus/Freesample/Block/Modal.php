<?php

class Walkwizus_Freesample_Block_Modal extends Mage_Core_Block_Template
{
    public function getBrands()
    {
        $options = Mage::helper('freesample')->getBundleOptions();
        $options = Mage::helper('core')->decorateArray($options);

        $bundleItems = array();
        foreach($options as $option) {
            foreach ($option->getSelections() as $selection) {
                $bundleItems[] = $selection->getId();
            }
            break;
        }

        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect(array('manufacturer'))
            ->addAttributeToFilter('entity_id', array('in' => $bundleItems));

        $results = array();
        foreach ($products as $product) {
            $results[$product->getManufacturer()] = array(
                'id' => $product->getManufacturer(),
                'label' => $product->getAttributeText('manufacturer'),
                'value' => $product->getAttributeText('manufacturer')
            );
        }

        return array_values($results);
    }
}