<?php

class Walkwizus_Freesample_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function getSelectAction()
    {
        $manufacturerId = $this->getRequest()->getPost('manufacturer_id');
        $optionIndex = $this->getRequest()->getPost('option_id');

        $html = Mage::helper('freesample')->getSelectOptions($manufacturerId, $optionIndex);

        $this->getResponse()->setBody($html);
    }
}