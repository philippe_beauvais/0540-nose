<?php

class Walkwizus_Freesample_Model_Resource_Birthdayoffer_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('freesample/birthdayoffer');
    }
}