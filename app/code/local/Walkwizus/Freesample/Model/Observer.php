<?php

class Walkwizus_Freesample_Model_Observer
{
    public function checkFreesampleEligibility()
    {
        $cartSubTotal = Mage::helper('nose/cart')->getCartSubtotal();
        $fsa = Mage::getStoreConfig('nose/free_shipping/amount');

        if ($cartSubTotal < $fsa) {
            $item = Mage::helper('freesample')->isBundleInCart();
            if ($item) {
                Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
            }
        }
    }

    public function checkFreesampleOverride($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if (Mage::helper('freesample')->isFreesampleBundle($product)) {
            $quote = Mage::getModel('checkout/cart')->getQuote();
            foreach ($quote->getAllItems() as $item) {
                if ($item->getId() && Mage::helper('freesample')->isFreesampleBundle($item->getProduct())) {
                    Mage::getSingleton('checkout/cart')->removeItem($item->getId());
                }
            }
            Mage::getSingleton('checkout/cart')->save();
        }
    }

    public function checkBirthdayOfferUse($observer)
    {
        $order = $observer->getEvent()->getOrder();

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $bundleId = Mage::getStoreConfig('nose/freesample/bundle5_id');
            if (!empty($bundleId)) {
                $items = $order->getAllItems();
                foreach ($items as $item) {
                    if ($item->getProduct()->getId() == $bundleId) {
                        $customer = Mage::getSingleton('customer/session')->getCustomer();
                        Mage::getModel('freesample/birthdayoffer')
                            ->setData('customer_id', $customer->getId())
                            ->setData('order_id', $order->getId())
                            ->setData('year', date('Y'))
                            ->save();
                    }
                }
            }
        }
    }
}