<?php

class Walkwizus_Freesample_Model_Birthdayoffer extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('freesample/birthdayoffer');
    }

    public function isAlreadyUsed()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $result = $this->getCollection()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('year', date('Y'));
        return $result->count() == 1;
    }
}