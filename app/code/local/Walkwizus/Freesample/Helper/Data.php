<?php

class Walkwizus_Freesample_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_offer = null;
    private $_bundle = null;
    private $_options = null;
    private $_selections = null;

    public function getOffer()
    {
        if (is_null($this->_offer)) {
            $this->_offer = 3;
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                if (!Mage::getModel('freesample/birthdayoffer')->isAlreadyUsed()) {
                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    $dob = $customer->getDob();
                    if (!empty($dob)) {
                        $dateNow = new DateTime();

                        $dateStart = new DateTime($dob);
                        $dateStart->setDate($dateNow->format('Y'), $dateStart->format('m'), $dateStart->format('d'));
                        $dateStart->modify('-1 month');
                        $dateStart->setTime(0, 0, 0);

                        $dateEnd = new DateTime($dob);
                        $dateEnd->setDate($dateNow->format('Y'), $dateEnd->format('m'), $dateEnd->format('d'));
                        $dateEnd->setTime(23, 59, 59);

                        if ($dateStart <= $dateNow && $dateNow <= $dateEnd) {
                            $this->_offer = 5;
                        }
                    }
                }
            }
        }
        return $this->_offer;
    }

    public function getBundle()
    {
        if (is_null($this->_bundle)) {
            $this->_bundle = false;
            $bundleId = Mage::getStoreConfig('nose/freesample/bundle' . $this->getOffer() . '_id');
            if (!empty($bundleId)) {
                $this->_bundle = Mage::getModel('catalog/product')->load($bundleId);
            }
        }

        return $this->_bundle;
    }

    public function isFreesampleBundle($product)
    {
        return in_array($product->getId(), array(
            Mage::getStoreConfig('nose/freesample/bundle3_id'),
            Mage::getStoreConfig('nose/freesample/bundle5_id')
        ));
    }

    public function isBundleInCart()
    {
        $bundle = $this->getBundle();
        if ($bundle) {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();

            foreach ($items as $item) {
                if ($item->getProduct()->getId() == $bundle->getId()) {
                    return $item;
                }
            }
        }

        return false;
    }

    public function getBundleOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array();

            $bundle = $this->getBundle();

            if ($bundle) {
                $typeInstance = $bundle->getTypeInstance(true);
                $typeInstance->setStoreFilter($bundle->getStoreId(), $bundle);

                $optionCollection = $typeInstance->getOptionsCollection($bundle);

                $selectionCollection = $this->getBundleSelections();

                $this->_options = $optionCollection->appendSelections($selectionCollection, false,
                    Mage::helper('catalog/product')->getSkipSaleableCheck()
                );
            }
        }
        return $this->_options;
    }

    public function getBundleSelections()
    {
        if (is_null($this->_selections)) {
            $this->_selections = array();

            $bundle = $this->getBundle();

            if ($bundle) {
                $typeInstance = $bundle->getTypeInstance(true);
                $typeInstance->setStoreFilter($bundle->getStoreId(), $bundle);

                $this->_selections = $typeInstance->getSelectionsCollection(
                    $typeInstance->getOptionsIds($bundle),
                    $bundle
                );
            }
        }
        return $this->_selections;
    }

    public function getBundleSelectedOptions()
    {
        $options = array();

        if ($item = $this->isBundleInCart()) {
            $bundle = $item->getProduct();
            $orderOptions = Mage::getModel('bundle/product_type')->getOrderOptions($bundle);

            if (!empty($orderOptions['info_buyRequest']['bundle_option'])) {
                $bundleOptions = $orderOptions['info_buyRequest']['bundle_option'];

                $selectionCollection = $this->getBundleSelections();

                foreach ($bundleOptions as $optionId => $selectedOptionId) {
                    $product = $selectionCollection->getItemByColumnValue('selection_id', $selectedOptionId);

                    if ($product) {
                        $parentProduct = Mage::helper('nose/product')->getParentProductFromChildSku($product->getSku());

                        if (!$parentProduct) {
                            $options[$optionId] = array(
                                'selection_id' => $selectedOptionId,
                                'manufacturer_id' => $product->getManufacturer(),
                                'manufacturer_label' => $product->getAttributeText('manufacturer')
                            );
                        } else {
                            $options[$optionId] = array(
                                'selection_id' => $selectedOptionId,
                                'manufacturer_id' => $parentProduct->getManufacturer(),
                                'manufacturer_label' => $parentProduct->getAttributeText('manufacturer')
                            );
                        }
                    }
                }
            }
        }

        return $options;
    }

    public function getSelectOptions($manufacturerId, $optionId, $selectionId = null)
    {
        $options = $this->getBundleOptions();
        $options = Mage::helper('core')->decorateArray($options);
        $productResource = Mage::getSingleton('catalog/product')->getResource();

        $html = '';
        foreach($options as $option) {
            if ($option->getId() == $optionId) {
                foreach ($option->getSelections() as $selection) {
                    $manufacturer = $productResource->getAttributeRawValue($selection->getId(), 'manufacturer', Mage::app()->getStore());
                    if ($manufacturer == $manufacturerId) {
                        $arrName = explode(',', $selection->getName());
                        $html .= '<option value="' . $selection->getSelectionId() . '"' . ($selection->getSelectionId() == $selectionId ? ' selected="selected"' : '') . ' data-product-id="' . $selection->getId() . '">' . $arrName[0] . '</option>';
                    }
                }
                break;
            }
        }

        return $html;
    }
}