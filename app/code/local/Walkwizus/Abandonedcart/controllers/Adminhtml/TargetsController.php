<?php

class Walkwizus_Abandonedcart_Adminhtml_TargetsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('customer');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('abandonedcart/adminhtml_targets'))
            ->renderLayout();
    }

    public function viewAction()
    {
        $targetId = $this->getRequest()->getParam('id');
        $target = Mage::getModel('abandonedcart/target')->load($targetId);

        Mage::register('target_data', $target);

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('abandonedcart/adminhtml_targets_view'))
            ->renderLayout();
    }
}