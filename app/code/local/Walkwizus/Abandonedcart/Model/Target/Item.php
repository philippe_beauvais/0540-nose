<?php

class Walkwizus_Abandonedcart_Model_Target_Item extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('abandonedcart/target_item');
    }
}