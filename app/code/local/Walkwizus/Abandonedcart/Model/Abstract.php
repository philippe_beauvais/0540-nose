<?php

use \Mailjet\Resources;

class Walkwizus_Abandonedcart_Model_Abstract
{
    protected function _sendEmail($customer, $emailTemplate, $emailTemplateVariables)
    {
        $messageId = null;

        if (Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_ENABLED)) {
            $mj = new \Mailjet\Client(
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_LOGIN),
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_PASSWORD)
            );

            $html = $emailTemplate->getProcessedTemplate($emailTemplateVariables, true);
            $subject = $emailTemplate->getProcessedTemplateSubject($emailTemplateVariables);

            $body = array(
                'FromEmail' => Mage::getStoreConfig('trans_email/ident_general/email'),
                'FromName' => Mage::getStoreConfig('trans_email/ident_general/name'),
                'Subject' => $subject,
                'Html-part' => $html,
                'Recipients' => array(
                    //array('Email' => $customer->getEmail())
                    array('Email' => 'ac@nose.fr')
                )
            );

            $response = $mj->post(Resources::$Email, array('body' => $body));
            $body = $response->getBody();

            return $body['Sent'][0]['MessageID'];
        }
        else {
            $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
            $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
            $emailTemplate->setReplyTo(Mage::getStoreConfig('trans_email/ident_custom1/email'));

            //$recipientEmail = $customer->getEmail();
            //$recipientName = $customer->getEmail();
            $recipientEmail = 'ac@nose.fr';
            $recipientName = 'ac@nose.fr';

            $emailTemplate->send($recipientEmail, $recipientName, $emailTemplateVariables);
        }

        return $messageId;
    }

    public function _isTargetableByRecentNewsletter()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('nose_read');
        $sql = "SELECT COUNT(*) AS recent_newsletter FROM
                (SELECT c.id_campaign, MAX(date_send) AS date_send, COUNT(*) c
                FROM crm_campaign c INNER JOIN crm_lk_user_campaign ct
                ON c.id_campaign=ct.id_campaign
                WHERE `type` < 2 AND date_add(sysdate(), INTERVAL -3 DAY) <= date_send
                GROUP BY c.id_campaign
                HAVING COUNT(*) > 10) a";
        $result = $read->fetchAll($sql);

        return !$result['recent_newsletter'];
    }

    protected function _isTargetableBySubscription($customer)
    {
        $userModel = Mage::getModel('mynose/user');
        $noseUser = $userModel->getNoseUser($customer);

        $table = Mage::getSingleton('core/resource')->getTableName('nose/lk_user_distributor');
        $select = $userModel->getReadConnection()->select()
            ->from( array('lud' => $table))
            ->where('id_user = ?', $noseUser['id_user'])
            ->where('distributor_id = ?', 1);
        $query = $select->query();
        $result = $query->fetch();

        return !$result['fg_unsubscribed'];
    }

    protected function _isTargetableByLastOrderDate($lastOrderDate)
    {
        if (!is_null($lastOrderDate)) {
            $dateNow = new DateTime();
            $dateOrder = new DateTime($lastOrderDate);

            $diff = $dateNow->diff($dateOrder);
            if ($diff->format('%a') <= Mage::getStoreConfig('nose/abandonedcart/last_order_delay')) {
                return false;
            }
        }

        return true;
    }

    protected function _isTargetableByLastTargetDate($lastTargetDate)
    {
        if (!is_null($lastTargetDate)) {
            $dateNow = new DateTime();
            $dateTarget = new DateTime($lastTargetDate);

            $diff = $dateNow->diff($dateTarget);
            if ($diff->format('%a') <= Mage::getStoreConfig('nose/abandonedcart/last_target_delay')) {
                return false;
            }
        }

        return true;
    }

    protected function _isTargetableQuote($quote)
    {
        $updatedAt = $quote->getUpdatedAt();
        if (!is_null($updatedAt)) {
            $dateQuote = strtotime($updatedAt);

            $delay = Mage::getStoreConfig('nose/abandonedcart/abandoned_cart_delay');

            $now = new DateTimeImmutable();
            $dateFrom = $now->modify("-$delay days")->setTime(0, 0, 0)->getTimestamp();
            $dateTo = $now->modify("-$delay days")->setTime(23, 59, 59)->getTimestamp();

            if ($dateFrom <= $dateQuote && $dateQuote <= $dateTo) {
                return true;
            }
        }

        return false;
    }

    protected function _getTargetableProductsForRecentlyViewed($products)
    {
        $newProducts = Mage::helper('abandonedcart')->getNewProducts();
        $futureProducts = Mage::helper('abandonedcart')->getFutureProducts();

        $productsToSkip = array_merge($newProducts, $futureProducts);

        $targetableProducts = array();
        foreach ($products as $productId) {
            if (!in_array($productId, $productsToSkip)) {
                $targetableProducts[] = Mage::getModel('catalog/product')->load($productId);
            }
        }

        return array_slice($targetableProducts, 0, 3);
    }

    public function _getTargetableProductsForAbandonedCart($quote)
    {
        $excludedProducts = Mage::helper('abandonedcart')->getExcludedProducts();

        $targetableProducts = array();
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) continue;

            $sku = $item->getSku();
            foreach ($excludedProducts as $excludedProduct) {
                if (preg_match("/$excludedProduct/", $sku)) {
                    continue 2;
                }
            }

            $targetableProducts[] = Mage::getModel('catalog/product')->load($item->getProductId());
        }

        return $targetableProducts;
    }

    protected function _log()
    {
        $args = func_get_args();
        $message = array_shift($args);
        Mage::log(vsprintf($message, $args), null, 'abandonedart.log');
    }
}