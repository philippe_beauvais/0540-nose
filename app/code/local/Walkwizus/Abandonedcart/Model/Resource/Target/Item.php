<?php

class Walkwizus_Abandonedcart_Model_Resource_Target_Item extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('abandonedcart/target_item', 'entity_id');
    }
}