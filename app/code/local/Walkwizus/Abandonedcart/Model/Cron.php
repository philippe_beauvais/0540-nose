<?php

class Walkwizus_Abandonedcart_Model_Cron
{
    public function processRecentlyViewed()
    {
        Mage::getModel('abandonedcart/recentlyviewed')->checkRecentlyViewed();
    }

    public function processAbandonedCart()
    {
        Mage::getModel('abandonedcart/abandonedcart')->checkAbandonedCart();
    }
}