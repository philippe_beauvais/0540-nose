<?php

class Walkwizus_Abandonedcart_Model_Target extends Mage_Core_Model_Abstract
{
    const TYPE_ABANDONED_CART = 'abandoned_cart';
    const TYPE_RECENTLY_VIEWED = 'recently_viewed';

    protected $_items = null;

    public function _construct()
    {
        $this->_init('abandonedcart/target');
    }

    public function getTypeOptions()
    {
        return array(
            self::TYPE_ABANDONED_CART => Mage::helper('abandonedcart')->__('Panier abandonné'),
            self::TYPE_RECENTLY_VIEWED => Mage::helper('abandonedcart')->__('Vous y pensez encore')
        );
    }

    public function getTypeLabel($type)
    {
        $options = $this->getTypeOptions();
        return isset($options[$type]) ?  $options[$type] : $type;
    }

    public function getItems()
    {
        if (is_null($this->_items)) {
            $this->_items = Mage::getResourceModel('abandonedcart/target_item_collection')
                ->addFieldToFilter('target_id', $this->getId());
        }
        return $this->_items;
    }

    public function getLastTargetDate($customer)
    {
        $results = $this->getCollection()
            ->addFieldToSelect(array('targeted_at'))
            ->addFieldToFilter('customer_id', $customer->getId())
            ->setOrder('targeted_at', 'DESC')
            ->setPageSize(1);
        return $results->getFirstItem()->getId()
            ? $results->getFirstItem()->getTargetedAt()
            : null;
    }
}