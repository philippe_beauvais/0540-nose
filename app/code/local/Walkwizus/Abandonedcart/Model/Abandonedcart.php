<?php

class Walkwizus_Abandonedcart_Model_Abandonedcart extends Walkwizus_Abandonedcart_Model_Abstract
{
    public function checkAbandonedCart()
    {
        $this->_log('Abandoned cart - start');

        $delay = Mage::getStoreConfig('nose/abandonedcart/abandoned_cart_delay');

        $now = new DateTimeImmutable();
        $dateFrom = $now->modify("-$delay days")->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $dateTo = $now->modify("-$delay days")->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        $quotes = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('customer_id', array('notnull' => true))
            ->addFieldToFilter('updated_at', array('from' => $dateFrom, 'to' => $dateTo));

        $this->_log('Abandoned cart - %d customers', $quotes->count());

        foreach ($quotes as $quote) {
            $customer = $quote->getCustomer();

            $this->_log('Abandoned cart - %s', $customer->getEmail());

            if (!$this->_isTargetableBySubscription($customer)) {
                $this->_log('Abandoned cart %d - skip by unsubscribed - %s', $quote->getId(), $customer->getEmail());
                continue;
            }

            $lastOrderDate = Mage::helper('abandonedcart')->getLastOrderDate($customer);
            if (!$this->_isTargetableByLastOrderDate($lastOrderDate)) {
                $this->_log('Abandoned cart %d - skip by last order date %s - %s', $quote->getId(), $lastOrderDate, $customer->getEmail());
                continue;
            }

            $lastTargetDate = Mage::getModel('abandonedcart/target')->getLastTargetDate($customer);
            if (!$this->_isTargetableByLastTargetDate($lastTargetDate)) {
                $this->_log('Abandoned cart %d - skip by last target date %s - %s', $quote->getId(), $lastTargetDate, $customer->getEmail());
                continue;
            }

            $targetableProducts = $this->_getTargetableProductsForAbandonedCart($quote);
            if (empty($targetableProducts)) {
                $this->_log('Abandoned cart %d - skip by empty products - %s', $quote->getId(), $customer->getEmail());
                continue;
            }

            $this->sendAbandonedCart($customer, $targetableProducts, $quote);
            $this->_log('Abandoned cart %d - sent - %s', $quote->getId(), $customer->getEmail());
        }

        $this->_log('Abandoned cart - end');
    }

    public function sendAbandonedCart($customer, $products, $quote)
    {
        $storeId = $customer->getStoreId();

        Mage::app()->getTranslator()->init('frontend', true);

        $locale = Mage::getStoreConfig('general/locale/code', $storeId);
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('abandonedcart_abandonedcart', $locale);

        $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId));

        $customer = Mage::getModel('customer/customer')->loadByEmail($customer->getEmail());
        $customer->setIsFemale($customer->getGender() == 2);

        $emailTemplateVariables = array();
        $emailTemplateVariables['products'] = $products;
        $emailTemplateVariables['customer'] = $customer;
        $emailTemplateVariables['quote'] = $quote;
        $emailTemplateVariables['unsubscribe_url'] = Mage::helper('abandonedcart')->getUnsubscribeUrl($customer);

        $messageId = $this->_sendEmail($customer, $emailTemplate, $emailTemplateVariables);

        $target = Mage::getModel('abandonedcart/target')
            ->setData('customer_id', $customer->getId())
            ->setData('targeted_at', date('Y-m-d H:i:s'))
            ->setData('type', Walkwizus_Abandonedcart_Model_Target::TYPE_ABANDONED_CART);
        if (!is_null($messageId)) {
            $target->setData('message_id', $messageId);

            $userModel = Mage::getModel('mynose/user');
            $noseUser = $userModel->getNoseUser($customer);

            Mage::getModel('reporting/message')
                ->setMessageId($messageId)
                ->setCampaignAlt(Walkwizus_Abandonedcart_Model_Target::TYPE_ABANDONED_CART)
                ->setContactId($noseUser['id_user'])
                ->setToEmail($customer->getEmail())
                ->save();
        }
        $target->save();

        foreach ($products as $product) {
            Mage::getModel('abandonedcart/target_item')
                ->setData('target_id', $target->getId())
                ->setData('product_id', $product->getId())
                ->setData('sku', $product->getSku())
                ->setData('name', $product->getName())
                ->save();
        }
    }
}