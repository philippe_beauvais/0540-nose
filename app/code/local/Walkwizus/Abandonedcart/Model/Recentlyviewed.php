<?php

class Walkwizus_Abandonedcart_Model_Recentlyviewed extends Walkwizus_Abandonedcart_Model_Abstract
{
    public function checkRecentlyViewed()
    {
        $this->_log('Recently viewed - start');

        if (!$this->_isTargetableByRecentNewsletter()) {
            $this->_log('Recently viewed - skip by recent newsletter');
            $this->_log('Recently viewed - end');
            return;
        }

        $delay = Mage::getStoreConfig('nose/abandonedcart/recently_viewed_delay');

        $now = new DateTimeImmutable();
        $dateFrom = $now->modify("-$delay days")->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $dateTo = $now->modify("-$delay days")->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        $connection = Mage::getModel('core/resource')->getConnection('core_read');

        $query = $connection->select()->from($connection->getTableName('report_viewed_product_index'))
            ->where('added_at >= ?', $dateFrom)
            ->where('added_at <= ?', $dateTo)
            ->where('customer_id IS NOT NULL');
        $results = $connection->fetchAll($query);

        $recentlyViewed = array();
        foreach ($results as $result) {
            $customerId = $result['customer_id'];
            $productId = $result['product_id'];

            if (!isset($recentlyViewed[$customerId])) {
                $recentlyViewed[$customerId] = array();
            }

            $recentlyViewed[$customerId][] = $productId;
        }

        $this->_log('Recently viewed - %d customers', count($recentlyViewed));

        foreach ($recentlyViewed as $customerId => $products) {
            $customer = Mage::getModel('customer/customer')->load($customerId);

            $this->_log('Recently viewed - %s', $customer->getEmail());

            if (!$this->_isTargetableBySubscription($customer)) {
                $this->_log('Recently viewed - skip by unsubscribed - %s', $customer->getEmail());
                continue;
            }

            $lastOrderDate = Mage::helper('abandonedcart')->getLastOrderDate($customer);
            if (!$this->_isTargetableByLastOrderDate($lastOrderDate)) {
                $this->_log('Recently viewed - skip by last order date %s - %s', $lastOrderDate, $customer->getEmail());
                continue;
            }

            $lastTargetDate = Mage::getModel('abandonedcart/target')->getLastTargetDate($customer);
            if (!$this->_isTargetableByLastTargetDate($lastTargetDate)) {
                $this->_log('Recently viewed - skip by last target date %s - %s', $lastTargetDate, $customer->getEmail());
                continue;
            }

            $quote = Mage::getModel('sales/quote')->loadByCustomer($customer);
            if ($this->_isTargetableQuote($quote)) {
                $this->_log('Recently viewed - targetable quote %d - %s', $quote->getId(), $customer->getEmail());
                $targetableProducts = $this->_getTargetableProductsForAbandonedCart($quote);
                if (!empty($targetableProducts)) {
                    $this->_log('Recently viewed - abandoned cart sent %d - %s', $quote->getId(), $customer->getEmail());
                    Mage::getModel('abandonedcart/abandonedcart')->sendAbandonedCart($customer, $targetableProducts, $quote);
                    continue;
                }
            }

            $targetableProducts = $this->_getTargetableProductsForRecentlyViewed($products);
            if (empty($targetableProducts)) {
                $this->_log('Recently viewed - skip by empty products - %s', $customer->getEmail());
                continue;
            }

            $this->sendRecentlyViewed($customer, $targetableProducts);
            $this->_log('Recently viewed - sent - %s', $customer->getEmail());
        }

        $this->_log('Recently viewed - end');
    }

    public function sendRecentlyViewed($customer, $products)
    {
        $storeId = $customer->getStoreId();

        Mage::app()->getTranslator()->init('frontend', true);

        $locale = Mage::getStoreConfig('general/locale/code', $storeId);
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('abandonedcart_recentlyviewed', $locale);

        $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId));

        $customer = Mage::getModel('customer/customer')->loadByEmail($customer->getEmail());
        $customer->setIsFemale($customer->getGender() == 2);

        $emailTemplateVariables = array();
        $emailTemplateVariables['products'] = $products;
        $emailTemplateVariables['product'] = $products[0];
        $emailTemplateVariables['customer'] = $customer;
        $emailTemplateVariables['unsubscribe_url'] = Mage::helper('abandonedcart')->getUnsubscribeUrl($customer);

        $messageId = $this->_sendEmail($customer, $emailTemplate, $emailTemplateVariables);

        $target = Mage::getModel('abandonedcart/target')
            ->setData('customer_id', $customer->getId())
            ->setData('targeted_at', date('Y-m-d H:i:s'))
            ->setData('type', Walkwizus_Abandonedcart_Model_Target::TYPE_RECENTLY_VIEWED);
        if (!is_null($messageId)) {
            $target->setData('message_id', $messageId);

            $userModel = Mage::getModel('mynose/user');
            $noseUser = $userModel->getNoseUser($customer);

            Mage::getModel('reporting/message')
                ->setMessageId($messageId)
                ->setCampaignAlt(Walkwizus_Abandonedcart_Model_Target::TYPE_RECENTLY_VIEWED)
                ->setContactId($noseUser['id_user'])
                ->setToEmail($customer->getEmail())
                ->save();
        }
        $target->save();

        foreach ($products as $product) {
            Mage::getModel('abandonedcart/target_item')
                ->setData('target_id', $target->getId())
                ->setData('product_id', $product->getId())
                ->setData('sku', $product->getSku())
                ->setData('name', $product->getName())
                ->save();
        }
    }
}