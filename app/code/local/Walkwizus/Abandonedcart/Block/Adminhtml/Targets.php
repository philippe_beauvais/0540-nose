<?php

class Walkwizus_Abandonedcart_Block_Adminhtml_Targets extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_targets';
        $this->_blockGroup = 'abandonedcart';

        $this->_headerText = $this->__('Targets');
        parent::__construct();
        $this->_removeButton('add');
    }
}