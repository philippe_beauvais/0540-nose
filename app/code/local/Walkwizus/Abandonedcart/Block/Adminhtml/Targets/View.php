<?php

class Walkwizus_Abandonedcart_Block_Adminhtml_Targets_View extends Mage_Adminhtml_Block_Widget_Container
{
    private $_target = null;
    private $_customer = null;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('abandonedcart/targets/view.phtml');
        $this->_target = Mage::registry('target_data');
    }

    public function getTarget()
    {
        return $this->_target;
    }

    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            $this->_customer = Mage::getModel('customer/customer')->load($this->getTarget()->getCustomerId());
        }
        return $this->_customer;
    }
}