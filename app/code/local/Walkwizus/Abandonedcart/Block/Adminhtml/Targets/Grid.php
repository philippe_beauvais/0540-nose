<?php

class Walkwizus_Abandonedcart_Block_Adminhtml_Targets_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('targetsGrid');
        $this->setDefaultSort('targeted_at');
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('abandonedcart/target_collection');

        $firstName = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastName = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

        $collection->getSelect()
            ->join('customer_entity', 'main_table.customer_id = customer_entity.entity_id', array('email'))
            ->join(array('ce1' => 'customer_entity_varchar'), 'ce1.entity_id = main_table.customer_id', array('firstname' => 'value'))
            ->where('ce1.attribute_id=' . $firstName->getAttributeId())
            ->join(array('ce2' => 'customer_entity_varchar'), 'ce2.entity_id = main_table.customer_id', array('lastname' => 'value'))
            ->where('ce2.attribute_id=' . $lastName->getAttributeId())
            ->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS customer_name"));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('customer_name', array(
            'header' => $this->__('Customer'),
            'align' => 'left',
            'index' => 'customer_name',
            'filter_condition_callback' => array($this, '_customerNameFilter')
        ));

        $this->addColumn('email', array(
            'header' => $this->__('Email'),
            'align' => 'left',
            'index' => 'email'
        ));

        $this->addColumn('type', array(
            'header' => $this->__('Type'),
            'align' => 'left',
            'index' => 'type',
            'type' => 'options',
            'options' => Mage::getModel('abandonedcart/target')->getTypeOptions(),
            'frame_callback' => array($this, 'decorateType')
        ));

        $this->addColumn('targeted_at', array(
            'header' => $this->__('Targeted At'),
            'align' => 'left',
            'index' => 'targeted_at',
            'type' => 'datetime'
        ));

        return parent::_prepareColumns();
    }

    public function decorateType($value, $row, $column, $isExport)
    {
        return Mage::getModel('abandonedcart/target')->getTypeLabel($row->getType());
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }
}