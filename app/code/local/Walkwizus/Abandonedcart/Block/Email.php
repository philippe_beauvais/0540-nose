<?php

class Walkwizus_Abandonedcart_Block_Email extends Mage_Core_Block_Template
{
    private $_gender = array(
        1 => 165,
        2 => 166
    );
    private $_genderUnisex = 167;
    private $_productIds = null;

    public function getProductIds()
    {
        if (is_null($this->_productIds)) {
            $this->_productIds = array();
            foreach ($this->getProducts() as $product) {
                $this->_productIds[] = $product->getId();
            }
        }
        return $this->_productIds;
    }

    public function getCartUrl()
    {
        $params = array();

        $customer = $this->getCustomer();
        if ($customer) {
            $params[Mage::getStoreConfig('customer/autologin/urlparam')] = $customer->getLoginHash();
        }

        return $this->getUrl('checkout/cart', $params);
    }

    public function getAddtocartUrl($product)
    {
        $params = array('product' => $product->getId());

        $customer = $this->getCustomer();
        if ($customer) {
            $params[Mage::getStoreConfig('customer/autologin/urlparam')] = $customer->getLoginHash();
        }

        return $this->getUrl('abandonedcart/email/addtocart', $params);
    }

    public function getCategory($product)
    {
        return Mage::helper('abandonedcart')->getProductCategory($product);
    }

    public function getRangeProductCollection()
    {
        $products = $this->getProducts();

        $masterIds = array();
        foreach ($products as $product) {
            $masterIds[] = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);
        }

        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('nose_read');
        $sql = "SELECT pa.perfume_id_2, MAX(pa.index_quality) AS idx
                FROM perfume_associations_model pa
                INNER JOIN lk_master_store lk ON pa.perfume_id_2 = lk.master_id AND lk.store_id = 1 AND lk.is_active = 1
                WHERE perfume_id_1  IN (" . implode(', ', $masterIds) . ")
                GROUP BY pa.perfume_id_2
                ORDER BY idx DESC";
        $results = $read->fetchAll($sql);

        $rangeProducts = array();
        foreach ($results as $result) {
            $masterId = $result['perfume_id_2'];

            $product = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('type_id', 'configurable')
                ->addAttributeToFilter('master_id', $masterId)
                ->getFirstItem();

            if ($product->getId()) {
                $saleable = false;
                $simpleProducts = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                foreach ($simpleProducts as $simpleProduct) {
                    if ($simpleProduct->isSaleable()) {
                        $saleable = true;
                        break;
                    }
                }
                if ($saleable) {
                    $rangeProducts[] = Mage::getModel('catalog/product')->load($product->getId());
                    if (count($rangeProducts) == 5) {
                        break;
                    }
                }
            }
        }

        return $rangeProducts;
    }

    public function getAlsoLikedProductCollection()
    {
        $products = $this->getProducts();

        $masterIds = array();
        foreach ($products as $product) {
            $masterIds[] = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);
        }

        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('nose_read');
        $sql = "SELECT aos.*
                FROM asso_input_sales aos
                WHERE input_perfume_id IN (" . implode(', ', $masterIds) . ") AND nose = 1
                ORDER BY aos.score DESC";
        $results = $read->fetchAll($sql);

        $alsoLikedProducts = array();
        foreach ($results as $result) {
            $masterId = $result['sale_perfume_id'];

            $product = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('type_id', 'configurable')
                ->addAttributeToFilter('master_id', $masterId)
                ->getFirstItem();

            if ($product->getId()) {
                $saleable = false;
                $simpleProducts = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                foreach ($simpleProducts as $simpleProduct) {
                    if ($simpleProduct->isSaleable()) {
                        $saleable = true;
                        break;
                    }
                }
                if ($saleable) {
                    $alsoLikedProducts[] = Mage::getModel('catalog/product')->load($product->getId());
                    if (count($alsoLikedProducts) == 5) {
                        break;
                    }
                }
            }
        }

        return $alsoLikedProducts;
    }

    public function getNewProductCollection()
    {
        $products = $this->getProducts();

        $masterIds = array();
        foreach ($products as $product) {
            $masterIds[] = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);
        }

        $customer = $this->getCustomer();

        $userModel = Mage::getModel('mynose/user');
        $noseUser = $userModel->getNoseUser($customer);

        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('nose_read');
        $sql = "SELECT anp.*
                FROM appetence_new_product anp
                WHERE anp.id_user = " . $noseUser['id_user'] . "
                AND anp.master_id NOT IN (" . implode(', ', $masterIds) . ")
                ORDER BY anp.indice DESC";
        $results = $read->fetchAll($sql);

        $newProducts = array();
        foreach ($results as $result) {
            $masterId = $result['master_id'];

            $product = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('type_id', 'configurable')
                ->addAttributeToFilter('master_id', $masterId)
                ->getFirstItem();

            if ($product->getId()) {
                $saleable = false;
                $simpleProducts = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                foreach ($simpleProducts as $simpleProduct) {
                    if ($simpleProduct->isSaleable()) {
                        $saleable = true;
                        break;
                    }
                }
                if ($saleable) {
                    $newProduct = Mage::getModel('catalog/product')->load($product->getId());
                    $newProduct->setIndice(round($result['indice']));
                    $newProducts[] = $newProduct;
                    if (count($newProducts) == 5) {
                        break;
                    }
                }
            }
        }

        return $newProducts;
    }

    public function getStarUrl($indice)
    {
        $eNoseUrl = Mage::getStoreConfig('nose/abandonedcart/enoseme_url');
        return $eNoseUrl . "files/_email/nose/common/etoiles.$indice.jpg";
    }
}