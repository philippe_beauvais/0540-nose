<?php

class Walkwizus_Abandonedcart_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getNewProducts()
    {
        $newProductsExclusion = Mage::getStoreConfig('nose/abandonedcart/new_products_exclusion');

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('news_from_date', array('notnull' => true))
            ->addAttributeToFilter('news_from_date', array('to' => date('Y-m-d')))
            ->addAttributeToSort('news_from_date', 'DESC')
            ->setPageSize($newProductsExclusion)
            ->setCurPage(1);

        $ids = array();
        foreach ($collection as $item) {
            $ids[] = $item->getId();
        }

        return $ids;
    }

    public function getFutureProducts()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('news_from_date', array('notnull' => true))
            ->addAttributeToFilter('news_from_date', array('from' => date('Y-m-d')));

        $ids = array();
        foreach ($collection as $item) {
            $ids[] = $item->getId();
        }

        return $ids;
    }

    public function getExcludedProducts()
    {
        return array(
            '^S-',
            'F-Freesample5-Pack',
            'F-Freesample3-Pack',
            'D-Diagnostic-Pack',
            'D-Diagnostic-Giftcard'
        );
    }

    public function getLastOrderDate($customer)
    {
        $userModel = Mage::getModel('mynose/user');
        $noseUser = $userModel->getNoseUser($customer);

        $select = $userModel->getReadConnection()->select()
            ->from( array('ls' => 'log_sales'), 'MAX(item_date_creation) AS order_date')
            ->where('order_customer_id = ? AND source = 1', $noseUser['magento_id']);
        if (!empty($noseUser['lightspeed_id'])) {
            $select->orWhere('order_customer_id = ? AND source = 2', $noseUser['lightspeed_id']);
        }
        $query = $select->query();
        $result = $query->fetch();

        return !empty($result['order_date']) ? $result['order_date'] : null;
    }

    public function getProductCategory($product)
    {
        $brandsId = Mage::getStoreConfig('nose/categories/brands');
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('entity_id', array('in' => $product->getCategoryIds()))
            ->addAttributeToFilter('path', array('like' => '%/'.$brandsId.'/%'))
            ->addAttributeToSelect('name');

        return $categories->count() > 0 ? $categories->getFirstItem() : null;
    }

    public function getUnsubscribeUrl($customer)
    {
        $eNoseUrl = Mage::getStoreConfig('nose/abandonedcart/enoseme_url');
        $locale = substr(Mage::getStoreConfig('general/locale/code', $customer->getStoreId()), 0, 2);
        $eNoseEncryptionKey = Mage::getStoreConfig('nose/abandonedcart/enoseme_encryption_key');
        $eNoseDistributorId = Mage::getStoreConfig('nose/abandonedcart/enoseme_distributor_id');
        $hash = md5($customer->getEmail() . $eNoseDistributorId . $eNoseEncryptionKey);
        return $eNoseUrl . $locale . '/unsubscribe/ask/' . $customer->getEmail() . '/' . $eNoseDistributorId . '/' . $hash;
    }
}