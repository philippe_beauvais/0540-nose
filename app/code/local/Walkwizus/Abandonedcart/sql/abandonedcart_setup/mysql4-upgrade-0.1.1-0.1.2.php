<?php

$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('abandonedcart/target')}
  ADD COLUMN `message_id` TEXT NULL COMMENT 'Message ID' AFTER `type`;
");

$installer->endSetup();