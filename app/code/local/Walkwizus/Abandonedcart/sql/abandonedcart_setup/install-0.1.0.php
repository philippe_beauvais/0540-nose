<?php

$installer = $this;

$table = $installer->getConnection()->newTable($installer->getTable('abandonedcart/target'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID'
);

$table->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Customer ID');

$table->addColumn('targeted_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
    'nullable' => false,
), 'Targeted At');

$table->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Type');

$table->addColumn('products', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Products');

$installer->getConnection()->createTable($table);

$installer->endSetup();

