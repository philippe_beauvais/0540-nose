<?php

$installer = $this;

$installer->run("
  ALTER TABLE {$this->getTable('abandonedcart/target')}
  DROP COLUMN `products`;
");

$table = $installer->getConnection()->newTable($installer->getTable('abandonedcart/target_item'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID'
);

$table->addColumn('target_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Target ID'
);

$table->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Product ID');

$table->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Product Sku');

$table->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
    'nullable' => false,
), 'Product Name');

$installer->getConnection()->createTable($table);

$installer->endSetup();

