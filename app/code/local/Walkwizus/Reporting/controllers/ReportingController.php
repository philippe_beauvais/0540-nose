<?php

class Walkwizus_Reporting_ReportingController extends Mage_Core_Controller_Front_Action
{
    private $_token = 'hL84WEh9Srf9nGT8V6am3';

    public function payloadAction()
    {
        if ($this->getRequest()->getParam('t') == $this->_token) {
            $response = $this->_getApiResponse();
            Mage::getModel('reporting/reporting')->updateMessage($response);
        }
    }

    private function _getApiResponse()
    {
        return json_decode(file_get_contents('php://input'));
    }
}