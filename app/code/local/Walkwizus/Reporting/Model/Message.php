<?php

class Walkwizus_Reporting_Model_Message extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('reporting/message');
    }
}