<?php

class Walkwizus_Reporting_Model_Resource_Message extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('reporting/message', 'entity_id');
    }
}