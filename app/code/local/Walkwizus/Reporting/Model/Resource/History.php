<?php

class Walkwizus_Reporting_Model_Resource_History extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('reporting/history', 'entity_id');
    }
}