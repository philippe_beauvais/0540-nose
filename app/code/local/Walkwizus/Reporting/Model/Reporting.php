<?php

use \Mailjet\Resources;

class Walkwizus_Reporting_Model_Reporting extends Mage_Core_Model_Abstract
{
    private $_payloadResponse;

    private $_mjClient = null;

    protected function _construct()
    {
        if ($this->_mjClient === null) {
            $this->_mjClient = new \Mailjet\Client(
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_LOGIN),
                Mage::getStoreConfig(MJ_Customsmtp_Helper_Config::XML_PATH_SMTP_PASSWORD)
            );
        }
    }

    public function updateMessage($response)
    {
        $this->_payloadResponse = $response;

        $messageModel = $this->_getMessageById($this->_payloadResponse->MessageID);

        if ($messageModel) {
            $infos = $this->getMessageStatInfo();

            $messageModel
                ->setArrivedAt($this->_formatDate($infos[0]['ArrivalTs']))
                ->setBlocked($infos[0]['Blocked'] ? $infos[0]['Blocked'] : 0)
                ->setBounce($infos[0]['Bounce'] ? $infos[0]['Bounce'] : 0)
                ->setBounceReason($infos[0]['BounceReason'] ? $infos[0]['BounceReason'] : null)
                ->setClick($infos[0]['Click'] ? $infos[0]['Click'] : 0)
                ->setComplaintDate($infos[0]['ComplaintDate'] ? $this->_formatDate($infos[0]['ComplaintDate']) : null)
                ->setOpen($infos[0]['Open'] ? $infos[0]['Open'] : 0)
                ->setSent($infos[0]['Sent'] ? $infos[0]['Sent'] : 0)
                ->setSpam($infos[0]['Spam'] ? $infos[0]['Spam'] : 0)
                ->setUnsub($infos[0]['Unsub'] ? $infos[0]['Unsub'] : 0)
                ->save();

            Mage::getModel('reporting/history')
                ->setMessageId($this->_payloadResponse->MessageID)
                ->setEventAt($this->_payloadResponse->time)
                ->setEventType($this->_payloadResponse->event)
                ->setComments(null)
                ->setUseragent(isset($this->_payloadResponse->agent) ? $this->_payloadResponse->agent : null)
                ->save();
        }
    }

    private function _getMessageById($id)
    {
        $collection = Mage::getModel('reporting/message')
            ->getCollection()
            ->addFieldToFilter('message_id', array('eq' => $id));

        return ($collection->count() > 0) ? $collection->getFirstItem() : false;
    }

    private function _formatDate($date)
    {
        $datetime = new DateTime($date);
        return (string)$datetime->format('Y-m-d H:i:s');
    }

    public function getMessageStatInfo()
    {
        $response = $this->_mjClient->get(Resources::$Messagesentstatistics, ['id' => $this->_payloadResponse->MessageID]);
        return $response->getData();
    }
}