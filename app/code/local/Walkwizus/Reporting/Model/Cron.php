<?php

class Walkwizus_Reporting_Model_Cron
{
    public function histoSkuStock()
    {
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('sku', array('like' => 'P-%'))
            ->addAttributeToFilter('status', 1);

        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('reporting_write');

        $dateTracking = date('Y-m-d');

        foreach ($products as $product) {
            $masterId = Mage::helper('nose/Perfume')->getMasterIdFromSku($product);

            if ($masterId) {
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

                $write->query("UPDATE histo_sku_stock SET fg_last_tracking = 0 WHERE sku = '" . $product->getSku() . "';");
                $write->query("REPLACE INTO histo_sku_stock (sku, date_tracking, master_id, qty, fg_last_tracking) VALUES (
                    '" . $product->getSku() . "',
                    '" . $dateTracking . "',
                    " . $masterId . ",
                    " . intval($stock->getQty()) . ",
                    1
                );");
            }
        }
    }
}