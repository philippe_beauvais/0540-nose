<?php

class Walkwizus_Slider_Adminhtml_SliderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('nose');
        return $this;
    }

    public function indexAction()
    {
        $this->_forward('list');
    }

    public function listAction()
    {
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('slider/adminhtml_slider'))
            ->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $sliderId = $this->getRequest()->getParam('id');

        if ($sliderId) {
            $slider = Mage::getModel('slider/slider')->getCollection()
                ->addFieldToFilter('entity_id', $sliderId)
                ->getFirstItem();
            Mage::register('slider', $slider);
        }

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('slider/adminhtml_slider_edit'))->_addLeft($this->getLayout()->createBlock('slider/adminhtml_slider_edit_tabs'))
            ->renderLayout();
    }

    public function saveAction()
    {
        $sliderId = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getPost();

        $slider = Mage::getModel('slider/slider');

        try {
            $slider->addData($data);

            if ($sliderId) {
                $slider->setId($sliderId);
            }

            $slider->save();

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slider was successfully saved'));
            Mage::getSingleton('adminhtml/session')->setSliderData(false);
        }
        catch(Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setSliderData($data);
        }

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $slider->getId()));
        }
        else {
            $this->_redirect('*/*/');
        }
    }

    public function deleteAction()
    {
        $sliderId = $this->getRequest()->getParam('id');

        if ($sliderId) {
            try {
                Mage::getModel('slider/slider')->load($sliderId)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slider was successfully deleted'));
                $this->_redirect('*/*/');
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $sliderId));
            }
        }
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                Mage::getModel('slider/slider')->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slider(s) was successfully removed'));
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}