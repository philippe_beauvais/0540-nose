<?php

class Walkwizus_Slider_Adminhtml_Slider_ItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('nose');
        return $this;
    }

    public function indexAction()
    {
        $this->_forward('list');
    }

    public function listAction()
    {
        $sliderId = $this->getRequest()->getParam('id');

        $this->_initAction()
            ->getLayout()->getBlock('slider.item.grid')->setSliderId($sliderId);
        $this->renderLayout();
    }

    public function newAction()
    {
        $sliderId = $this->getRequest()->getParam('slider_id');

        if ($sliderId) {
            $slider = Mage::getModel('slider/slider')->getCollection()
                ->addFieldToFilter('entity_id', $sliderId)
                ->getFirstItem();
            Mage::register('slider', $slider);
        }

        $this->_forward('edit');
    }

    public function editAction()
    {
        $itemId = $this->getRequest()->getParam('id');

        if ($itemId) {
            $item = Mage::getModel('slider/slider_item')->getCollection()
                ->addFieldToFilter('entity_id', $itemId)
                ->getFirstItem();
            Mage::register('slider_item', $item);
        }

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('slider/adminhtml_slider_item_edit'))->_addLeft($this->getLayout()->createBlock('slider/adminhtml_slider_item_edit_tabs'))
            ->renderLayout();
    }

    public function saveAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $sliderId = $this->getRequest()->getParam('slider_id');
        $data = $this->getRequest()->getPost();

        $item = Mage::getModel('slider/slider_item');

        try {
            $item->addData($data);

            if ($itemId) {
                $item->setId($itemId);
            }
            elseif ($sliderId) {
                $item->setSliderId($sliderId);
            }

            $item->save();

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slide was successfully saved'));
            Mage::getSingleton('adminhtml/session')->setSliderItemData(false);
        }
        catch(Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setSliderItemData($data);
        }

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $item->getId()));
        }
        else {
            $this->_redirect('*/slider/edit', array('id' => $item->getSliderId()));
        }
    }

    public function deleteAction()
    {
        $itemId = $this->getRequest()->getParam('id');

        if ($itemId) {
            try {
                $item = Mage::getModel('slider/slider_item')->load($itemId);
                $sliderId = $item->getSliderId();
                $item->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slide was successfully deleted'));
                $this->_redirect('*/slider/edit', array('id' => $sliderId));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $itemId));
            }
        }
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $item = Mage::getModel('slider/slider_item')->load($id);
                $sliderId = $item->getId();
                $item->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slide(s) was successfully removed'));
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/slider/edit', array('id' => $sliderId));
    }
}