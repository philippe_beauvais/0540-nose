<?php

$installer = $this;

$installer->startSetup();

// Slider
$table = $installer->getConnection()->newTable($installer->getTable('slider/slider'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Id'
);

$table->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
    'nullable' => false,
), 'Slider Name');

$table->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Is Active');

$installer->getConnection()->createTable($table);

// Slider Item
$table = $installer->getConnection()->newTable($installer->getTable('slider/slider_item'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Id'
);

$table->addColumn('slider_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Slider Id');

$table->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
    'nullable' => false,
), 'Item Name');

$table->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
    'nullable' => false,
), 'Image');

$table->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'nullable' => false,
), 'Position');

$installer->getConnection()->createTable($table);

$installer->endSetup();