<?php

$installer = $this;

$this->startSetup();

$table = $installer->getTable('slider/slider_item');

$installer->getConnection()->addColumn($table, 'cinemagraphe', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'default' => null,
    'comment' => 'Field cinemagraphe'
));

$installer->getConnection()->addColumn($table, 'video', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'default' => null,
    'comment' => 'Field video'
));

$installer->run("ALTER TABLE {$this->getTable('slider/slider_item')} CHANGE `image_desktop` `image_desktop` TEXT NULL DEFAULT NULL COMMENT 'Field Image Desktop'");
$installer->run("ALTER TABLE {$this->getTable('slider/slider_item')} CHANGE `image_mobile` `image_mobile` TEXT NULL DEFAULT NULL COMMENT 'Field Image Mobile'");


$this->endSetup();