<?php

$installer = $this;

$this->startSetup();

$table = $installer->getTable('slider/slider_item');

$installer->getConnection()->addColumn($table, 'image_desktop', "TEXT COMMENT 'Image Desktop'");
$installer->getConnection()->addColumn($table, 'image_mobile', "TEXT COMMENT 'Image Mobile'");
$installer->getConnection()->dropColumn($table, 'image');

$this->endSetup();