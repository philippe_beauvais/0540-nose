<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addForeignKey(
        $installer->getFkName(
            'slider/slider_item',
            'slider_id',
            'slider/slider',
            'entity_id'
        ),
        $installer->getTable('slider/slider_item'),
        'slider_id',
        $installer->getTable('slider/slider'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
