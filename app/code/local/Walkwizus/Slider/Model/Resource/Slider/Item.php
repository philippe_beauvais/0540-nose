<?php

class Walkwizus_Slider_Model_Resource_Slider_Item extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('slider/slider_item', 'entity_id');
    }
}