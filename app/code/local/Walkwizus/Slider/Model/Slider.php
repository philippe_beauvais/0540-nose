<?php

class Walkwizus_Slider_Model_Slider extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('slider/slider');
    }

    public function getItemsCollection()
    {
        return Mage::getResourceModel('slider/slider_item_collection')->addFieldToFilter('slider_id', $this->getId());
    }
}