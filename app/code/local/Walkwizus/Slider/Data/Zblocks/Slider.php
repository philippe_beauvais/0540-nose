<?php

class Walkwizus_Slider_Data_Zblocks_Slider extends Varien_Data_Form_Element_Select
{
    public function getOptions()
    {
        $collection = Mage::getModel('slider/slider')
            ->getCollection();

        $data = array();
        $data[-1] = '-- Select Slider --';
        foreach ($collection as $slider) {
            $data[$slider->getId()] = $slider->getName();
        }
        
        return $data;
    }
}