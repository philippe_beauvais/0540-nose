<?php

class Walkwizus_Slider_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getImagePath($slide)
    {
        return Mage::getBaseDir('media') . $slide->getImageDesktop();
    }

    public function getImageUrl($slide)
    {
        return Mage::getBaseUrl('media') . $slide->getImageDesktop();
    }

    public function getImageMobile($slide)
    {
        return Mage::getBaseDir('media') . $slide->getImageMobile();
    }

    public function getImageDesktop($slide)
    {
        return Mage::getBaseDir('media') . $slide->getImageDesktop();
    }
}