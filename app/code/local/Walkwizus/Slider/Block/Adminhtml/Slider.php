<?php

class Walkwizus_Slider_Block_Adminhtml_Slider extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_slider';
        $this->_blockGroup = 'slider';

        $this->_headerText = $this->__('Manage Sliders');
        parent::__construct();
    }
}