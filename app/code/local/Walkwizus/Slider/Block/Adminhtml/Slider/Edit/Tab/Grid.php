<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Edit_Tab_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('sliderItemGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('slider/slider_item')->getCollection()
            ->addFieldToFilter('slider_id', $this->getSliderId());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('item_id', array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'index' => 'entity_id',
        ));

        $this->addColumn('item_name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('item_image', array(
            'header' => $this->__('Image'),
            'align' => 'left',
            'index' => 'image_desktop',
            'frame_callback' => array($this, 'decorateImage'),
        ));

        $this->addColumn('item_position', array(
            'header' => $this->__('Position'),
            'align' => 'left',
            'index' => 'position',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/slider_item/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove', array(
            'label' => $this->__('Remove'),
            'url' => $this->getUrl('*/*/massRemove'),
            'confirm' => $this->__('Are you sure?')
        ));
        return $this;
    }

    protected function getAdditionalJavascript()
    {
        return 'window.sliderItemGrid_massactionJsObject = sliderItemGrid_massactionJsObject;';
    }

    public function decorateImage($value, $row, $column, $isExport)
    {
        return '<img src="' . Mage::helper('slider')->getImageUrl($row) . '" width="100" />';
    }
}