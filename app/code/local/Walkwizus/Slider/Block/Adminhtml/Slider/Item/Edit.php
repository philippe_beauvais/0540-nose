<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_slider_item';
        $this->_blockGroup = 'slider';

        parent::__construct();

        $this->_addButton('save_and_continue', array(
            'label' => $this->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save'
        ), -100);

        $this->_formScripts[] = "
             function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
             }
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('slider_item')) {
            return $this->__('Edit slide : %s', $this->htmlEscape(Mage::registry('slider_item')->getName()));
        } elseif (Mage::registry('slider')) {
            return $this->__('Add slide : %s', $this->htmlEscape(Mage::registry('slider')->getName()));
        }
    }

    public function getBackUrl()
    {
        if (Mage::registry('slider_item')) {
            return $this->getUrl('*/slider/edit', array('id' => Mage::registry('slider_item')->getSliderId()));
        } elseif (Mage::registry('slider')) {
            return $this->getUrl('*/slider/edit', array('id' => Mage::registry('slider')->getId()));
        } else {
            return $this->getUrl('*/slider/index');
        }
    }
}