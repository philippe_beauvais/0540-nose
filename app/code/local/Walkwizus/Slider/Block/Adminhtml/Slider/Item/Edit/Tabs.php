<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Item_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_item_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Slide Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('slider/adminhtml_slider_item_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}