<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_slider';
        $this->_blockGroup = 'slider';

        parent::__construct();

        $this->_addButton('save_and_continue', array(
            'label' => $this->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save'
        ), -100);

        $this->_formScripts[] = "
             function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
             }
        ";

        if (Mage::registry('slider')) {
            $this->_addButton('add', array(
                'label' => $this->__('Add slide'),
                'onclick' => "setLocation('{$this->getUrl('*/slider_item/new', array('slider_id' => Mage::registry('slider')->getId()))}')",
                'class' => 'add'
            ));
        }
    }

    public function getHeaderText()
    {
        if (Mage::registry('slider')) {
            return $this->__('Edit slider : %s', $this->htmlEscape(Mage::registry('slider')->getName()));
        } else {
            return $this->__('Add slider');
        }
    }
}