<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('sliderGrid');
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('slider/slider')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'index' => 'entity_id',
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('is_active', array(
            'header' => $this->__('Status'),
            'align' => 'left',
            'index' => 'is_active',
            'type' => 'options',
            'options' => self::getStatusOption(),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove', array(
            'label' => $this->__('Remove'),
            'url' => $this->getUrl('*/*/massRemove'),
            'confirm' => $this->__('Are you sure?')
        ));
        return $this;
    }

    static public function getStatusOption()
    {
        return array(
            0 => 'Disable',
            1 => 'Enable'
        );
    }
}