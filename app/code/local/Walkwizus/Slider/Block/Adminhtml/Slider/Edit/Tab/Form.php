<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('slider_general', array(
            'legend' => $this->__('General Information'),
            'class' => 'fieldset-wide',
        ));

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => $this->__('Name'),
            'title' => $this->__('Name'),
            'required' => true
        ));

        $fieldset->addField('is_active', 'select', array(
            'name' => 'is_active',
            'label' => $this->__('Status'),
            'title' => $this->__('Status'),
            'required' => true,
            'values' => array(
                array('value' => 0, 'label' => 'Disable'),
                array('value' => 1, 'label' => 'Enable')
            )
        ));

        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getSliderData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getSliderData());
            Mage::getSingleton('adminhtml/session')->setSliderData(false);
        }
        elseif (Mage::registry('slider')) {
            $form->setValues(Mage::registry('slider')->getData());
        }

        return parent::_prepareForm();
    }
}
