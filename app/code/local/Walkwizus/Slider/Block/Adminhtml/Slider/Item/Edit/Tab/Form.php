<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('slider_general', array(
            'legend' => $this->__('General Information'),
            'class' => 'fieldset-wide',
        ));

        $fieldset->addType('mediachooser', 'AntoineK_MediaChooserField_Data_Form_Element_Mediachooser');

        $fieldset->addField('slider_id', 'hidden', array(
            'name' => 'slider_id'
        ));

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => $this->__('Name'),
            'title' => $this->__('Name'),
            'required' => true
        ));

        $fieldset->addField('image_desktop', 'mediachooser', array(
            'name' => 'image_desktop',
            'label' => $this->__('Image Desktop'),
            'title' => $this->__('Image Desktop'),
            'required' => false
        ));

        $fieldset->addField('image_mobile', 'mediachooser', array(
            'name' => 'image_mobile',
            'label' => $this->__('Image Mobile'),
            'title' => $this->__('Image Mobile'),
            'required' => false
        ));

        $fieldset->addField('cinemagraphe', 'text', array(
            'name' => 'cinemagraphe',
            'label' => $this->__('Cinemagraphe (URL Vimeo)'),
            'title' => $this->__('Cinémagraphe (URL Vimeo)'),
            'required' => false
        ));

        $fieldset->addField('video', 'text', array(
            'name' => 'video',
            'label' => $this->__('Video (ID YouTube)'),
            'title' => $this->__('Video (ID YouTube)'),
            'required' => false
        ));

        $fieldset->addField('position', 'text', array(
            'name' => 'position',
            'label' => $this->__('Position'),
            'title' => $this->__('Position'),
            'required' => true
        ));

        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getSlideItemData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getSlideItemData());
            Mage::getSingleton('adminhtml/session')->setSlideItemData(false);
        }
        elseif (Mage::registry('slider_item')) {
            $form->setValues(Mage::registry('slider_item')->getData());
        }

        return parent::_prepareForm();
    }
}
