<?php

class Walkwizus_Slider_Block_Adminhtml_Slider_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Slider Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('slider/adminhtml_slider_edit_tab_form')->toHtml(),
        ));
        if (Mage::registry('slider')) {
            $this->addTab('items_section', array(
                'label' => $this->__('Slides'),
                'title' => $this->__('Slides'),
                'url' => $this->getUrl('*/slider_item/list', array('id' => Mage::registry('slider')->getId(), '_current' => true)),
                'class' => 'ajax',
            ));
        }
        return parent::_beforeToHtml();
    }
}