<?php

class Walkwizus_Prehome_Block_Popin extends Mage_Core_Block_Template
{
    public function _construct()
    {
        $code = false;
        $customerSession = Mage::getSingleton('customer/session');

        if ($customerSession->isLoggedIn())
        {
            $user = Mage::getModel('mynose/user')->getNoseUserFromMagentoId($customerSession->getCustomer()->getId());

            if ($user['term_of_use'] == 1)
            {
                $customer = Mage::getSingleton('customer/session')->getCustomer();

                $status = Mage::getModel('prehome/status')->getUserStatus($customer);

                $code = Mage::helper('prehome')->getPopinCodeByStatus($status);
            }
        }

        $this->setData('popin_code', $code);
    }
}