<?php

class Walkwizus_Prehome_Model_Status extends Partikule_Mynose_Model_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setConnection('mynose_read', 'mynose_write');
    }

    public function getUserStatus($customer)
    {
        $userTable = Mage::getSingleton('core/resource')->getTableName('mynose/users');
        $lkDistributorTable = Mage::getSingleton('core/resource')->getTableName('prehome/lk_user_distributor');
        $userCycleStatusTable = Mage::getSingleton('core/resource')->getTableName('prehome/crm_lk_user_cycle_status');
        $crmCycleStatusTable = Mage::getSingleton('core/resource')->getTableName('prehome/crm_cycle_status');

        $noseUser = Mage::getModel('mynose/user')->getNoseUserFromCustomer($customer);

        // Get Diagnostic coupon usage
        if ( ! empty($noseUser['coupon_code']))
        {
            $coupon = Mage::getModel('salesrule/coupon');
            $coupon->load($noseUser['coupon_code'], 'code');

            if ($coupon->getTimesUsed == 0)
            {
                Mage::register('coupon_code', $coupon->getCode());

                return 'use_kit_coupon';
            }
        }

        // Second pass : Get the status from eNose
        $select = $this->getReadConnection()->select()
            ->from(array('u' => $userTable), array('id_user'))
            ->join(array('lkud' => $lkDistributorTable), "u.id_user = lkud.id_user AND lkud.distributor_id = 1", array())
            ->join(array('lkucs' => $userCycleStatusTable), "u.id_user = lkucs.id_user AND lkucs.dt_end IS NULL", array('dt_start'))
            ->join(array('cs' => $crmCycleStatusTable), "lkucs.id_cycle_status = cs.id_cycle_status AND cs.id_cycle = 1", array('status_name'))
            ->where('u.id_user = ?', $noseUser['id_user']);

        $query = $select->query();
        $result = $query->fetchAll();

        return $result[0]['status_name'];
    }
}