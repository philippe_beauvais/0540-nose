<?php

class Walkwizus_Prehome_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_crmStatus = array(
        '3rd_kit_buyer' => false,
        'customer_discretion' => false,
        'evaluation_pending' => 'evaluate_your_perfumes',
        'follower' => 'do_the_diagnosis',
        'invited' => 'do_the_diagnosis',
        'lost' => 'neutral',
        'loyal_online_customer' => false,
        'online_customer' => false,
        'online_customer_without_diagnostic' => 'do_the_diagnosis',
        'online_diagnosed' => 'order_your_sample_kit',
        'prospect' => 'do_the_diagnosis',
        'really_unsatisfied_evaluator' => 'neutral',
        'recalled_for_2nd_evaluations_pending' => 'evaluate_your_perfumes',
        'recalled_for_evaluations_pending' => 'evaluate_your_perfumes',
        'retail_big_customer' => false,
        'retail_customer' => false,
        'retail_customer_without_diagnostic' => 'do_the_diagnosis',
        'retail_diagnosed_discretion' => 'order_your_sample_kit',
        'retail_diagnosed_gp' => 'order_your_sample_kit',
        'retail_diagnosed_ngp' => 'order_your_sample_kit',
        'sample_buyer' => false,
        'satisfied_evaluator' => false,
        'unsatisfied_evaluator' => 'neutral',
        'use_kit_coupon' => 'use_kit_coupon'
    );

    public function getPopinCodeByStatus($status)
    {
        $sanitize = strtolower(str_replace(' ', '_', $status));
        if (isset($this->_crmStatus[$sanitize])) {
            return $this->_crmStatus[$sanitize];
        } else {
            return false;
        }
    }
}