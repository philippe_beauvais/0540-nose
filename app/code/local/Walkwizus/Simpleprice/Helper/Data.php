<?php

class Walkwizus_Simpleprice_Helper_Data extends Mage_Core_Helper_Data
{
    public function getCheapestProductByConfigurable($product)
    {
        $select = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $select->fetchOne("
            SELECT cpe.entity_id, cpip.final_price
            FROM catalog_product_entity cpe
            INNER JOIN catalog_product_relation cpr ON cpe.entity_id = cpr.child_id
            INNER JOIN catalog_product_entity cpe2 ON cpr.parent_id = cpe2.entity_id AND cpe2.sku = '{$product->getSku()}'
            INNER JOIN catalog_product_index_price as cpip ON cpe.entity_id = cpip.entity_id AND customer_group_id = 0
            WHERE cpe.sku NOT LIKE 'S-%'
            ORDER BY cpip.final_price ASC
            LIMIT 0, 1
        ");

        return Mage::getModel('catalog/product')->load($result);
    }
}