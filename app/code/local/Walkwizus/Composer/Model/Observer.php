<?php

class Walkwizus_Composer_Model_Observer
{
    public function onControllerFrontInitBefore()
    {
        self::requireComposerAutoload();
    }

    static function requireComposerAutoload()
    {
        $autoload = Mage::getBaseDir() . DS . 'vendor' . DS . 'autoload.php';
        if (is_file($autoload)) {
            require_once($autoload);
        }
    }
}