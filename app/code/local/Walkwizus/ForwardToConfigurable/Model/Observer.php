<?php

class Walkwizus_ForwardToConfigurable_Model_Observer extends Mage_Core_Model_Abstract
{
    public function forwardToConfigurable($observer)
    {
        $controller = $observer->getControllerAction();
        $productId = (int)$controller->getRequest()->getParam('id');

        $product = Mage::getModel('catalog/product')->load($productId);

        if ($product->getTypeId() == 'simple') {
            $baseUrl = Mage::getBaseUrl();

            // Redirect with parent product
            $parentProduct = Mage::helper('nose/product')->getParentProduct($product);
            if ($parentProduct) {
                $url = $baseUrl;

                $category = Mage::helper('nose/product')->getProductFirstCategory($parentProduct);
                if ($category) {
                    $url = $category->getUrl() . '/';
                }

                if ($parentProduct->isVisibleInCatalog()) {
                    $url .= $parentProduct->getUrlKey();
                }

                Mage::app()->getResponse()->setRedirect($url);
                return;
            }

            // Redirect with category
            $category = Mage::helper('nose/product')->getProductFirstCategory($product);
            if ($category) {
                $url = $category->getUrl();

                Mage::app()->getResponse()->setRedirect($url);
                return;
            }

            // Redirect to homepage
            Mage::app()->getResponse()->setRedirect($baseUrl);
        }
    }

}