<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
/**
 * Create table 'soon_zblocks/content'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('soon_zblocks/content'))
    ->addColumn(
        'content_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ),
        'Content Id'
    )
    ->addColumn(
        'aw_content_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'unsigned' => true,
            'default' => '0',
        ),
        'AW_Zblocks Content Id'
    )
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(), 'Content')
    ->addForeignKey(
        $installer->getFkName('soon_zblocks/content', 'aw_content_id', 'zblocks/content', 'block_id'),
        'aw_content_id',
        $installer->getTable('zblocks/content'),
        'block_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Tags Info');

$installer->getConnection()->createTable($table);

$installer->endSetup();
