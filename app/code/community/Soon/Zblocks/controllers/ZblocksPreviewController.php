<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Adminhtml_ZblocksPreviewController Controller
 * @package Soon_Zblocks
 */
class Soon_Zblocks_ZblocksPreviewController extends Mage_Core_Controller_Front_Action
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    /**
     * Preview zblock
     */
    public function winAction()
    {
        $this->loadLayout();

        $zblock = Mage::getModel('zblocks/zblocks')
            ->load($this->getRequest()->getParam('id'));

        $this->getLayout()
            ->getBlock('soon_zblocks.preview')
            ->setZblock($zblock);

        $this->renderLayout();
    }

// Agence Soon Tag NEW_METHOD

    /**
     * Is allowed?
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

}