<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@agence-soon.fr> <@herveguetin>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Renderer_Sample Block
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Block_Renderer_Sample extends Mage_Core_Block_Template
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    public function getBlockTitle()
    {
        return 'Some custom logic';
    }

// Agence Soon Tag NEW_METHOD

}