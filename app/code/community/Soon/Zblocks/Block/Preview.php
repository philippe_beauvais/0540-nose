<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Preview Block
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Block_Preview extends Mage_Core_Block_Template
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    protected function _toHtml()
    {
        $zblock = $this->getZblock();

        $blocks = array();
        $contentBlocks = Mage::getResourceModel('zblocks/content_collection')
            ->addFieldToFilter('zblock_id', array('eq' => $zblock->getZblockId()))
            ->addOrder('sort_order', 'ASC');

        foreach ($contentBlocks as $contentBlock) {
            $blocks[] = $contentBlock->getContent();
        }

        $processor = Mage::getModel('core/email_template_filter');

        foreach ($blocks as $k => $content) {
            $blocks[$k] = $processor->filter($content);
            $cssClass = "zblock zblock-" . $zblock->getBlockPositionCustom();
            $blocks[$k] = '<div class="' . $cssClass . '">' . $blocks[$k] . '</div>';
        }

        return implode('', $blocks);
    }

// Agence Soon Tag NEW_METHOD

}