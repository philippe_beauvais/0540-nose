<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Adminhtml_Rewrites_Content_Edit_Form Block
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Block_Adminhtml_Rewrites_Content_Edit_Form extends AW_Zblocks_Block_Adminhtml_Zblocks_Edit_Tab_Content_Edit_Form
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR
    /**
     * @var Soon_Zblocks_Model_Content_Types
     */
    private $_contentTypes;
    /**
     * @var array
     */
    private $_contentFields;

    /**
     * OVERRIDE in order to add some things...
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        /* @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('zblocks_content');

        // Remove content field as we do not want to use it
        $fieldset->removeField('content');

        // Add custom input types from config
        $this->_addCustomInputTypes($fieldset);

        // Retrieve all our custom content types
        $this->_contentTypes = Mage::getModel('soon_zblocks/content_types');

        // Inject all available content fields into HTML
        $this->_contentFields = $this->_contentTypes->getFields();

        if (empty($this->_contentFields)) {
            return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
        }

        // Let's user select which content type he•she wants
        $fieldset->addField('soon_type', 'select', array(
            'label' => $this->__('Type'),
            'title' => $this->__('Type'),
            'name' => 'soon_type',
            'options' => $this->_contentTypes->getFormTypeOptions(),
        ), 'title');

        // Add fields
        $this->_populateFields($fieldset);

        // Create a form dependency block that shows only relevant fields depending on which option is selected in "soon_type" select
        $this->_buildDependencies();

        // Populate additional data from our custom content
        $this->_populateData();

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }

    /**
     * Add custom input types to our fieldset
     *
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     */
    private function _addCustomInputTypes($fieldset)
    {
        $inputTypes = Mage::getConfig()->getNode('soon_zblocks/custom_input_types')->asArray();
        array_map(function ($code, $class) use ($fieldset) {
            $fieldset->addType($code, $class);
        }, array_keys($inputTypes), $inputTypes);
    }

    /**
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     * @return string
     */
    protected function _populateFields($fieldset)
    {
        foreach ($this->_contentFields as $fieldName => $field) {
            $fieldConfig = array(
                'label' => $field['label'],
                'title' => $field['label'],
                'name' => $fieldName,
                'field_type' => $field['field_type'],
            );
            if (isset($field['note'])) {
                $fieldConfig['note'] = $field['note'];
            }

            $fieldConfig = array_merge($fieldConfig, $this->_makeFieldConfig($field));

            /**
             * By default we are using text field.
             */
            $fieldset->addField($fieldName, $fieldConfig['field_type'], $fieldConfig, 'soon_type');
        }
    }

    /**
     * Use specific logic for populating input additional config
     *
     * @param array $field
     * @return array mixed
     */
    private function _makeFieldConfig($field)
    {
        $builderClassFactory = (string)Mage::getConfig()->getNode('soon_zblocks/field_type_builders/' . $field['field_type']);

        $fieldConfig = isset($field['element_data']) ? $field['element_data'] : array();

        if (!$builderClassFactory) {
            return $fieldConfig;
        }

        /** @var Soon_Zblocks_Model_Content_Field_Types_Interface $builder */
        $builder = Mage::getModel($builderClassFactory, array('soon_blocks_element_data' => $fieldConfig));

        return $builder->makeConfig();
    }


    /**
     * Build form dependencies
     */
    protected function _buildDependencies()
    {
        $formDependBlock = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');
        $formDependBlock->addFieldMap('page_soon_type', 'page_soon_type_depend');

        foreach ($this->_contentFields as $fieldName => $field) {
            $formDependBlock->addFieldMap('page_' . $fieldName, $fieldName . '_depend');
            $typeOptionArr = explode('__', $fieldName);
            $typeCode = $typeOptionArr[1];
            $formDependBlock->addFieldDependence($fieldName . '_depend', 'page_soon_type_depend', $typeCode);
        }

        $this->setChild('form_after', $formDependBlock);
    }

    /**
     * Populate the form with all the required data
     */
    protected function _populateData()
    {
        if (Mage::registry('zblocks_content')) {
            $data = Mage::registry('zblocks_content');

            $configData = Mage::getModel('soon_zblocks/content_types')->getFields();
            foreach ($configData as $k => $i) {
                if (isset($i['element_data']['value'])) {
                    $data[$k] = $i['element_data']['value'];
                }
            }

            if (isset($data['block_id'])) {
                $soonContent = Mage::getModel('soon_zblocks/content')->load($data['block_id'], 'aw_content_id');
                if ($soonContent->getId()) {
                    $data = array_merge($data, $soonContent->getContent()->getData());
                }

                $this->getForm()->setValues($data);
            }
        }
    }

// Agence Soon Tag NEW_METHOD

}