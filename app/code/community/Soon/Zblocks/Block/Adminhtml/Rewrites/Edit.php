<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Adminhtml_Rewrites_Edit Block
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Block_Adminhtml_Rewrites_Edit extends AW_Zblocks_Block_Adminhtml_Zblocks_Edit
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    public function __construct()
    {
        parent::__construct();

        $this->_addButton('preview', array(
            'label' => Mage::helper('soon_zblocks')->__('Preview'),
            'onclick' => 'preview()',
            'class' => 'go',
        ), -100);

        $popWinUrl = Mage::app()->getDefaultStoreView()
            ->getUrl('soon_zblocks/zblocksPreview/win', array('id' => $this->getRequest()->getParam('id')));

        $this->_formScripts[] = "
            function preview(){
                popWin('" . $popWinUrl . "', 'soon_zblocks_preview');
            }
        ";
    }

// Agence Soon Tag NEW_METHOD

}