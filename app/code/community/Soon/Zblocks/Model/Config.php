<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@agence-soon.fr> <@herveguetin>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Config Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Config extends Mage_Core_Model_Abstract
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    /**
     * @var bool
     */
    private $_isConfigLoaded = false;
    /**
     * @var Mage_Core_Model_Config
     */
    private $_mageConfig;
    /**
     * @var Mage_Core_Helper_Abstract[]
     */
    private $_helpers = array();

    protected function _construct()
    {
        $this->_mageConfig = Mage::getConfig();

        parent::_construct();
    }


    /**
     * Load additional configs into main config
     */
    public function loadConfig()
    {
        if (!$this->_isConfigLoaded) {
            $modules = $this->_mageConfig->getNode('modules')->children();
            foreach ($modules as $modName => $module) {
                if ($module->is('active')) {
                    // Content types
                    $files = glob($this->_mageConfig->getModuleDir('etc',
                            $modName) . DS . 'soon_zblocks_content_types' . DS . '*.xml');
                    if (!empty($files)) {
                        $this->_loadConfigFiles($files);
                    }
                }
            }
            $this->_translateConfig();

            $this->_isConfigLoaded = true;
        }
    }

    /**
     * Inject config files contents into main config
     *
     * @param array $files
     */
    private function _loadConfigFiles($files)
    {
        foreach ($files as $file) {
            $fileArr = explode(DS . 'etc' . DS, $file);
            $fileName = end($fileArr);

            if ($fileName == 'config.xml') {
                continue;
            }

            $this->_mageConfig->loadModulesConfiguration($fileName, $this->_mageConfig);
        }
    }

    /**
     * Translate config nodes
     *
     * @param array $nodes
     * @param string $pathPrefix
     */
    private function _translateConfig($nodes = array(), $pathPrefix = '')
    {
        $nodes = (empty($nodes)) ? $this->_mageConfig->getNode('soon_zblocks/content_types')->asArray() : $nodes;

        if (isset($nodes['@']['translate']) && isset($nodes['@']['module'])) {
            $this->_translateConfigNode($nodes, $pathPrefix);
        }

        foreach ($nodes as $path => $node) {
            if (isset($node['@']['translate']) && isset($node['@']['module'])) {
                $fullPath = empty($pathPrefix) ? $path : $pathPrefix . '/' . $path;
                $this->_translateConfigNode($node, $fullPath);
            }
            if (is_array($node)) {
                foreach ($node as $subPath => $subNode) {
                    if (is_array($subNode) && $subPath != '@') {
                        $subPathPrefix = empty($pathPrefix) ? '' : $pathPrefix . '/';
                        $this->_translateConfig($subNode, $subPathPrefix . $path . '/' . $subPath);
                    }
                }
            }
        }
    }

    /**
     * Translate config node
     *
     * @param array $node
     * @param string $path
     */
    private function _translateConfigNode($node, $path)
    {
        $fieldsToTranslate = explode(',', str_replace(' ', '', $node['@']['translate']));
        $moduleHelper = $node['@']['module'];
        if (!isset($this->_helpers[$moduleHelper])) {
            $this->_helpers[$moduleHelper] = Mage::helper($moduleHelper);
        }
        array_map(function ($fieldToTranslate) use ($path, $moduleHelper) {
            $configPath = 'soon_zblocks/content_types/' . $path . '/' . $fieldToTranslate;
            $value = (string)$this->_mageConfig->getNode($configPath);
            $this->_mageConfig->setNode($configPath, $this->_helpers[$moduleHelper]->__($value));
        }, $fieldsToTranslate);
    }

// Agence Soon Tag NEW_METHOD

}