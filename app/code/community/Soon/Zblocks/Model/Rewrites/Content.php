<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Rewrites_Content Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Rewrites_Content extends AW_Zblocks_Model_Content
{

// Agence Soon Tag NEW_CONST

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'aw_zblocks_content';

    /**
     * Parameter name in event
     * In observe method you can use $observer->getEvent()->getObject() in this case
     * @var string
     */
    protected $_eventObject = 'content';

    /**
     * OVERRIDE base Varien_Object getter in order to use our custom renderers
     *
     * @return string
     */
    public function getContent()
    {
        $soonContent = Mage::getModel('soon_zblocks/content')->load($this->getBlockId(), 'aw_content_id');
        $processor = Mage::helper('cms')->getBlockTemplateProcessor();

        if ($soonContent->getId()) {
            $contentTypes = Mage::getModel('soon_zblocks/content_types')->getContentTypes();
            $usedContentType = $soonContent->getContent()->getSoonType();

            if (isset($contentTypes[$usedContentType])) {
                $rendererUri = $contentTypes[$usedContentType]['frontend_renderer']['class'];
                $contentConfig = $soonContent->getContentTypeConfig()->getData();
                $contentConfig['soon_type'] = $usedContentType;
                $contentConfig['template'] = $contentTypes[$usedContentType]['frontend_renderer']['template'];

                foreach ($contentConfig as $k => $v) {
                    if (is_string($v)) {
                        $contentConfig[$k] = $processor->filter($v);
                    }
                }

                $contentBlock = Mage::app()->getLayout()->createBlock($rendererUri, null, $contentConfig);

                $html = sprintf(
                    '<div class="%s">%s</div>',
                    str_replace('_', '-', $usedContentType),
                    $contentBlock->toHtml()
                );

                return $html;
            }
        }

        return $this->getData('content');
    }

// Agence Soon Tag NEW_VAR

// Agence Soon Tag NEW_METHOD

}