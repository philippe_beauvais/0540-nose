<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Resource Model of Content
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Resource_Content extends Mage_Core_Model_Resource_Db_Abstract
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    /**
     * Content Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('soon_zblocks/content', 'content_id');
    }

// Agence Soon Tag NEW_METHOD

}