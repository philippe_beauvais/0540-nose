<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Content_Types Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Content_Types extends Mage_Core_Model_Abstract
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    protected function _construct()
    {
        Mage::getSingleton('soon_zblocks/config')->loadConfig();

        parent::_construct();
    }

    /**
     * Retrieve content types from config
     *
     * @return array
     */
    public function getContentTypes()
    {
        $contentTypes = Mage::getConfig()->getNode('soon_zblocks/content_types')->asArray();
        foreach ($contentTypes as $k => $type) {
            if (!$type['active']) {
                unset($contentTypes[$k]);
            }
        }
        return $contentTypes;
    }

    /**
     * Retrieve content types as options array
     *
     * @return array
     */
    public function getFormTypeOptions()
    {
        $options = array();
        $contentTypes = $this->getContentTypes();
        foreach ($contentTypes as $code => $config) {
            $options[$code] = $config['label'];
        }

        return $options;
    }

    /**
     * Get fields from content types
     *
     * @return array
     */
    public function getFields()
    {
        $fields = array();
        $contentTypes = $this->getContentTypes();

        foreach ($contentTypes as $contentCode => $config) {
            if (isset($config['fields'])) {
                foreach ($config['fields'] as $fieldCode => $field) {
                    $fields['soon_zblocks__' . $contentCode . '__' . $fieldCode] = $field;
                }
            }
        }

        return $fields;
    }

// Agence Soon Tag NEW_METHOD

}