<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@agence-soon.fr> <@herveguetin>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Content_Field_Types_Multiselect Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Content_Field_Types_Multiselect  extends Soon_Zblocks_Model_Content_Field_Types_Abstract implements Soon_Zblocks_Model_Content_Field_Types_Interface
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    /**
     * Make field type config from XML config
     *
     * @return array
     * @throws Exception
     */
    public function makeConfig()
    {
        $config = $this->_getFieldConfig();

        if (!isset($config['values'])) {
            throw new Exception('<values> node must be declared.');
        }

        $values = [];
        foreach ($config['values'] as $value) {
            $values[] = array('value' => $value['value'], 'label' => $value['label']);
        }

        return array('values' => $values);
    }

// Agence Soon Tag NEW_METHOD
}