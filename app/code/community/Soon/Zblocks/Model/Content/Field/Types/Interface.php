<?php
/**
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Content_Field_Types_Select Model
 * @package Soon_Zblocks
 */
interface Soon_Zblocks_Model_Content_Field_Types_Interface
{
    /**
     * Make field type config from XML config
     *
     * @return array
     */
    public function makeConfig();
}