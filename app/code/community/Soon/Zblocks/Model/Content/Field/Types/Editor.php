<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@agence-soon.fr> <@herveguetin>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Content_Field_Types_Editor Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Content_Field_Types_Editor extends Soon_Zblocks_Model_Content_Field_Types_Abstract implements Soon_Zblocks_Model_Content_Field_Types_Interface
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

// Agence Soon Tag NEW_METHOD

    /**
     * Make field type config from XML config
     *
     * @return array
     */
    public function makeConfig()
    {
        return array(
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
        );
    }
}