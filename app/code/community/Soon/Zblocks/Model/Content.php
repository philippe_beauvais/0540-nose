<?php
/**
 * This file is part of Soon_Zblocks for Magento.
 *
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr> <@attweek>
 * @category Soon
 * @package Soon_Zblocks
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

/**
 * Content Model
 * @package Soon_Zblocks
 */
class Soon_Zblocks_Model_Content extends Mage_Core_Model_Abstract
{

// Agence Soon Tag NEW_CONST

// Agence Soon Tag NEW_VAR

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'soon_zblocks_content';

    /**
     * Parameter name in event
     * In observe method you can use $observer->getEvent()->getObject() in this case
     * @var string
     */
    protected $_eventObject = 'content';
    /**
     * @var bool
     */
    private $_isBeingDuplicated = false;
    /**
     * @var int
     */
    private $_duplicatedContentId;

    /**
     * Content Constructor
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('soon_zblocks/content');
    }

    /**
     * Save content based on AW content
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function saveFromAwContent(Varien_Event_Observer $observer)
    {
        if (!$observer->getContent()->hasSoonType()) {
            return;
        }

        if ($this->_isBeingDuplicated) {
            $this->duplicateSoonContent($observer);
            return;
        }

        $awContent = $observer->getContent();
        if ($awContent->hasFormKey()) {
            $awContent->unsFormKey();
        }

        $this->load($awContent->getBlockId(), 'aw_content_id');

        $this->setAwContentId($awContent->getBlockId());

        $usedData = array();
        foreach ($awContent->getData() as $k => $v) {
            if ($k == 'soon_type' || strpos($k, 'soon_zblocks__') !== false) {
                $usedData[$k] = $v;
            }
        }

        $this->setContent(serialize($usedData));

        $this->save();
    }

    /**
     * Store duplicated block ID
     */
    public function isBeingDuplicated()
    {
        $this->_isBeingDuplicated = true;
    }

    /**
     * Store duplicated content Id for further use
     *
     * @param Varien_Event_Observer $observer
     */
    public function storeDuplicatedContentId(Varien_Event_Observer $observer)
    {
        if ($observer->getContent() instanceof AW_Zblocks_Model_Content && $this->_isBeingDuplicated) {
            $this->_duplicatedContentId = $observer->getContent()->getId();
        }
    }

    /**
     * Duplicate Soon content
     *
     * @param Varien_Event_Observer $observer
     */
    public function duplicateSoonContent(Varien_Event_Observer $observer)
    {
        $this->load($this->_duplicatedContentId, 'aw_content_id');
        $this->setId(null)
            ->setAwContentId($observer->getContent()->getId())
            ->setContent(serialize($this->getContent()->getData()))
            ->save();
    }

    /**
     * Get config specific to content type
     *
     * @return Varien_Object
     */
    public function getContentTypeConfig()
    {
        $config = array();
        $contentData = $this->getContent()->getData();
        $soonTypeFlag = 'soon_zblocks__' . $contentData['soon_type'] . '__';

        foreach ($contentData as $k => $v) {
            if (strpos($k, $soonTypeFlag) !== false) {
                $config[str_replace($soonTypeFlag, '', $k)] = $v;
            }
        }

        return new Varien_Object($config);
    }

    protected function _afterLoad()
    {
        $this->setContent(new Varien_Object(unserialize($this->getContent())));
        return parent::_afterLoad();
    }

// Agence Soon Tag NEW_METHOD

}