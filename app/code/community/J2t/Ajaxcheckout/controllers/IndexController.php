<?php

/**
 * J2T-DESIGN.
 *
 * @category   J2t
 * @package    J2t_Ajaxcheckout
 * @copyright  Copyright (c) 2003-2009 J2T DESIGN. (http://www.j2t-design.com)
 * @license    OSL
 */

class J2t_Ajaxcheckout_IndexController extends /*Mage_Checkout_CartController*/ Mage_Core_Controller_Front_Action
{

    const CONFIGURABLE_PRODUCT_IMAGE= 'checkout/cart/configurable_product_image';
    const USE_PARENT_IMAGE          = 'parent';

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function cartdeleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                Mage::getSingleton('checkout/cart')->removeItem($id)
                  ->save();
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addError($this->__('Cannot remove item'));
            }
        }
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');

        $this->renderLayout();
    }

    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }
    
    
    protected function _massConfAdd($cart, $params)
    {
        $productId = (int) $this->getRequest()->getParam('product');
        

        try {
            if (isset($params['qty_super_attribute'])){
                $hasbeenprocessed = FALSE;
                foreach($params['qty_super_attribute'] as $key_att_mixed => $qty_att){
                    if ($qty_att > 0){
                        $key_att_mixed_array = explode('|', $key_att_mixed);
                        foreach($key_att_mixed_array as $key_att){
                            $hasbeenprocessed = TRUE;
                            $sup_att_array = explode('_',$key_att);
                            if ($params['super_attribute'] == array()){
                                $params['super_attribute'] = array($sup_att_array[0] => $sup_att_array[1]);
                            } else {
                                $params['super_attribute'][$sup_att_array[0]] = $sup_att_array[1];
                            }
                        }

                        $filter = new Zend_Filter_LocalizedToNormalized(
                            array('locale' => Mage::app()->getLocale()->getLocaleCode())
                        );
                        $params['qty'] = $filter->filter($qty_att);
                        $related = $this->getRequest()->getParam('related_product');
                        $params_processed = $params;
                        unset($params_processed['qty_super_attribute']);
                        
                        if ($productId) {
                            $product = Mage::getModel('catalog/product')
                                ->setStoreId(Mage::app()->getStore()->getId())
                                ->load($productId);

                        }


                        $cart->addProduct($product, $params_processed);
                        if (!empty($related)) {
                            $cart->addProductsByIds(explode(',', $related));
                        }
                    }
                }
                if (!$hasbeenprocessed){
                    $message = $this->__('Please specify product quantity.');
                    $this->_getSession()->addError($message);
                    return;
                }
                $cart->save();
            } else {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }

                $product = $this->_initProduct();
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    $this->_goBack();
                    return;
                }

                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();
            }

            $this->_getSession()->setCartWasUpdated(TRUE);

            
            $img = '';
            Mage::dispatchEvent('checkout_cart_add_product_complete', array('product'=>$product, 'request'=>$this->getRequest()));

            $photo_arr = explode("x",Mage::getStoreConfig('j2tajaxcheckout/default/j2t_ajax_cart_image_size', Mage::app()->getStore()->getId()));

            $prod_img = $product;
            if($product->isConfigurable() && isset($params['super_attribute'])){
                $attribute = $params['super_attribute'];
                if (Mage::getStoreConfig(self::CONFIGURABLE_PRODUCT_IMAGE) != self::USE_PARENT_IMAGE) {
                    $prod_img_temp = Mage::getModel("catalog/product_type_configurable")->getProductByAttributes($attribute, $product);
                    if ($prod_img_temp->getImage() != 'no_selection' && $prod_img_temp->getImage()){
                        $prod_img = $prod_img_temp;
                    }
                }
            }
            $img = '<img src="'.Mage::helper('catalog/image')->init($prod_img, 'thumbnail')->resize($photo_arr[0],$photo_arr[1]).'" width="'.$photo_arr[0].'" height="'.$photo_arr[1].'" />';
            $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());

            Mage::getSingleton('checkout/session')->addSuccess('<div class="j2tajax-checkout-img">'.$img.'</div><div class="j2tajax-checkout-txt">'.$message.'</div>');
        }
        catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(TRUE)) {
                $this->_getSession()->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError($message);
                }
            }
        }
        catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
        }
    }
    
    protected function _getRemainAmountFromFreeShipping()
	{
		// Get all product and prices :
		$addon = '';
		$fsa = Mage::getStoreConfig('nose/free_shipping/amount');
		$cartSubTotal = Mage::helper('nose/cart')->getCartSubtotal();

		if ($cartSubTotal < $fsa)
		{
			$diff = $fsa - $cartSubTotal;
			$addon = '<b style="color:#b00">'.
				$this->__('Only %s', $diff.' '.Mage::app()->getStore()->getCurrentCurrencyCode()).
				' !</b><br/>'.
				$this->__('from free shipping and %d free samples', Mage::helper('freesample')->getOffer());
		}
		else{
            $offer = Mage::helper('freesample')->getOffer();
            $addon  = '<b>';
            if ($offer == 5) {
                $addon .= $this->__('Happy birthday !') . '<br/>';
            }
            $addon .= $this->__('Free shipping and %d samples !', $offer) . '<br/>';
            $addon .= $this->__('Choose your samples and place your order.');
            $addon .= '</b>';
		}
		$addon = '<div class="mt20 center">'.$addon.'</div>';
		return $addon;
	}

    public function cartAction()
    {
        if ($this->getRequest()->getParam('cart')){
            if ($this->getRequest()->getParam('cart') == "delete"){
                $id = $this->getRequest()->getParam('id');
                if ($id) {
                    try {
                        Mage::getSingleton('checkout/cart')->removeItem($id)
                          ->save();
                    } catch (Exception $e) {
                        Mage::getSingleton('checkout/session')->addError($this->__('Cannot remove item'));
                    }
                }
            }
        }

		/*
		 * Partikule : Handle of the Diagnostic Bundle
		 * 2012.11.30
		 *
		 */
		if ($this->getRequest()->getParam('cart') && $this->getRequest()->getParam('cart') == 'bundle')
		{
			$bundle_id = $this->getRequest()->getParam('bundle');
			$diag_product_ids = explode(',', $this->getRequest()->getParam('products'));
			$cart_bundle_option = array();

			// $bundle = Mage::getModel('catalog/product');
			$bundle = new Mage_Catalog_Model_Product();;
			$bundle->load($bundle_id);

			try
			{
				// We can't  continue
				if ($bundle->getTypeId() != 'bundle')
				{
					Mage::throwException($this->__('Can not add item to shopping cart'));
				}
				else
				{
					$optionCollection = $bundle->getTypeInstance(TRUE)->getOptionsCollection($bundle);

					// Get all products linked to all options of the bundle
					$selectionCollection = $bundle->getTypeInstance(TRUE)->getSelectionsCollection(
						$bundle->getTypeInstance(TRUE)->getOptionsIds($bundle),
						$bundle
					);
					foreach($optionCollection as $option)
					{
						// trace('<h2>' . $option->getId() . '</h2>');
						$diag_product_id = array_shift($diag_product_ids);

						$optionSelectionCollection = $selectionCollection->getItemsByColumnValue('option_id', $option->getId());

						// Loop into all Option products
						foreach($optionSelectionCollection as $option_product)
						{
							// If the asked sample is in the list : OK, add it
							if ($option_product->getId() == $diag_product_id)
							{
								$cart_bundle_option[$option->getId()] = $option_product->getSelectionId();
								break;
							}
						}
					}
					$params = array(
						'product' => $bundle_id,
						'related_product' => NULL,
						'bundle_option' => $cart_bundle_option,
						'qty' => 1,
					);

					$cart = Mage::getSingleton('checkout/cart');

					$cart->addProduct($bundle, $params);
					$cart->save();

					// Bon, ça, on le prend comme c'est hein...
					Mage::getSingleton('checkout/session')->setCartWasUpdated(TRUE);
					Mage::getSingleton('checkout/session')->setCartInsertedItem($bundle->getId());

					$img = '';
					Mage::dispatchEvent('checkout_cart_add_product_complete', array('product'=>$bundle, 'request'=>$this->getRequest()));

					$photo_arr = explode("x",Mage::getStoreConfig('j2tajaxcheckout/default/j2t_ajax_cart_image_size', Mage::app()->getStore()->getId()));

					$prod_img = $bundle;
					if($bundle->isConfigurable() && isset($params['super_attribute'])){
						$attribute = $params['super_attribute'];
						if (Mage::getStoreConfig(self::CONFIGURABLE_PRODUCT_IMAGE) != self::USE_PARENT_IMAGE) {
							$prod_img_temp = Mage::getModel("catalog/product_type_configurable")->getProductByAttributes($attribute, $bundle);
							if ($prod_img_temp->getImage() != 'no_selection' && $prod_img_temp->getImage()){
								$prod_img = $prod_img_temp;
							}
						}
					}
					$img = '<img src="'.Mage::helper('catalog/image')->init($prod_img, 'thumbnail')->resize($photo_arr[0],$photo_arr[1]).'" width="'.$photo_arr[0].'" height="'.$photo_arr[1].'" />';
					$message = $this->__('%s was successfully added to your shopping cart.', $bundle->getName());

					Mage::getSingleton('checkout/session')->addSuccess('<div class="j2tajax-checkout-img">'.$img.'</div><div class="j2tajax-checkout-txt">'.$message.'</div>');


				}
			}
			catch (Exception $e) {
				Mage::getSingleton('checkout/session')->addException($e, $e->getMessage());
			}
		}


		/*
		 * Partikule : Handle of multi products
		 *
		 */
        if ($this->getRequest()->getParam('cart') && $this->getRequest()->getParam('cart') == 'addset')
        {
			$products = explode(',', $this->getRequest()->getParam('products'));

			$cart = Mage::getModel('checkout/cart');
			$cart->init();
			
			$imgs = array();
            $photo_arr = explode("x",Mage::getStoreConfig('j2tajaxcheckout/default/j2t_ajax_cart_image_size', Mage::app()->getStore()->getId()));

            try
            {
				foreach ($products as $productId)
				{
					if ($productId == '') 
						continue;
		 
	                $product = Mage::getModel('catalog/product')
	                    ->setStoreId(Mage::app()->getStore()->getId())
	                    ->load($productId);
					if (
						$product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
						OR
						$product->getTypeId() == Webtex_Giftcards_Model_Product_Type::TYPE_GIFTCARDS_PRODUCT

					)
					{
						try	{
							$cart->addProduct($product, array('qty' => '1'));
	
		                    $imgs[] = '<img src="'.Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(55).'" width="55" height="55" />';
		                    
							// Should that be added for each product ?
							// Mage::dispatchEvent('checkout_cart_add_product_complete', array('product'=>$product, 'request'=>$this->getRequest()));
						}
						catch (Exception $e) {
							continue;
						}
					}
		 
				}
				$cart->save();

				$message = $this->__('The sample set was successfully added to your shopping cart.');
				
                Mage::getSingleton('checkout/session')->addSuccess(
					'<div class="j2tajax-checkout-img-multiple">'.implode('', $imgs).
					'</div><div class="clearfix"></div><div class="j2tajax-checkout-txt-multiple">'.
					$message.
					'</div>'
				);

			}

            catch (Mage_Core_Exception $e) {
                if (Mage::getSingleton('checkout/session')->getUseNotice(TRUE)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError($message);
                    }
                }
            }
            catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
            }
        }
		/*
		 * /Partikule
		 *
		 */

		/*
		 * Partikule : Handle of Gifts
		 *
		 */

		if ($this->getRequest()->getParam('cart') && $this->getRequest()->getParam('cart') == 'gift')
		{
			$products = explode(',', $this->getRequest()->getParam('products'));

			$cart = Mage::getModel('checkout/cart');
			$cart->init();

			$imgs = array();
			$photo_arr = explode("x",Mage::getStoreConfig('j2tajaxcheckout/default/j2t_ajax_cart_image_size', Mage::app()->getStore()->getId()));

			try
			{
				foreach ($products as $productId)
				{
					if ($productId == '')
						continue;

					$product = Mage::getModel('catalog/product')
						->setStoreId(Mage::app()->getStore()->getId())
						->load($productId);

					if ($product->getTypeId() == Webtex_Giftcards_Model_Product_Type::TYPE_GIFTCARDS_PRODUCT)
					{
						try	{
							$params = $this->getRequest()->getParams();
							unset($params['cart']);
							unset($params['products']);
							unset($params['product_amount_'.$product->getId()]);
							$params['qty'] = '1';
							$params['card_amount'] = $this->getRequest()->getParam('product_amount_'.$product->getId());

							$cart->addProduct(
								$product,
								$params
							);

							$imgs[] = '<img src="'.Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(55).'" width="55" height="55" />';

							// Should that be added for each product ?
							// Mage::dispatchEvent('checkout_cart_add_product_complete', array('product'=>$product, 'request'=>$this->getRequest()));
						}
						catch (Exception $e) {
							continue;
						}
					}

				}
				$cart->save();

				$message = $this->__('The products were successfully added to your cart');

				// Get remaining amount from free shipping
				$addon = $this->_getRemainAmountFromFreeShipping();

				Mage::getSingleton('checkout/session')->addSuccess(
					'<div class="j2tajax-checkout-img-multiple">'
						.implode('', $imgs)
					.'</div><div class="clearfix"></div><div class="j2tajax-checkout-txt-multiple">'
					.$message
					.'</div>'
					.$addon
				);
			}

			catch (Mage_Core_Exception $e) {
				if (Mage::getSingleton('checkout/session')->getUseNotice(TRUE)) {
					Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						Mage::getSingleton('checkout/session')->addError($message);
					}
				}
			}
			catch (Exception $e) {
				Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
			}
		}

		/*
		 * Only one product : Default behaviour
		 *
		 */
		if ($this->getRequest()->getParam('product')) {

            $cart   = Mage::getSingleton('checkout/cart');
            $params = $this->getRequest()->getParams();
            $related = $this->getRequest()->getParam('related_product');
            
            //J2T check massconf update
            if(Mage::getConfig()->getModuleConfig('J2t_Massconf')->is('active', 'true') && isset($params['qty_super_attribute'])){
                $this->_massConfAdd($cart, $params);
                $this->loadLayout();
                $this->_initLayoutMessages('checkout/session');
                $this->renderLayout();
                return;
            }
            //J2T check massconf update

            $productId = (int) $this->getRequest()->getParam('product');
            if ($productId) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($productId);
                try {

                    if (!isset($params['qty'])) {
                        $params['qty'] = 1;
                    }

                    $cart->addProduct($product, $params);
                    if (!empty($related)) {
                        $cart->addProductsByIds(explode(',', $related));
                    }
                    $cart->save();
                    $this->_getSession()->setCartWasUpdated(TRUE);


                    Mage::getSingleton('checkout/session')->setCartWasUpdated(TRUE);
                    Mage::getSingleton('checkout/session')->setCartInsertedItem($product->getId());

                    Mage::dispatchEvent('checkout_cart_add_product_complete', array('product'=>$product, 'request'=>$this->getRequest()));

                    $photo_arr = explode("x",Mage::getStoreConfig('j2tajaxcheckout/default/j2t_ajax_cart_image_size', Mage::app()->getStore()->getId()));

                    $prod_img = $product;
                    if($product->isConfigurable() && isset($params['super_attribute'])){
                        $attribute = $params['super_attribute'];
                        if (Mage::getStoreConfig(self::CONFIGURABLE_PRODUCT_IMAGE) != self::USE_PARENT_IMAGE) {
                            $prod_img_temp = Mage::getModel("catalog/product_type_configurable")->getProductByAttributes($attribute, $product);
                            if ($prod_img_temp->getImage() != 'no_selection' && $prod_img_temp->getImage()){
                                $prod_img = $prod_img_temp;
                            }
                        }
                    }
                    $img = '<img src="'.Mage::helper('catalog/image')->init($prod_img, 'thumbnail')->resize($photo_arr[0],$photo_arr[1]).'" width="'.$photo_arr[0].'" height="'.$photo_arr[1].'" />';
                    $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());

					// Get remaining amount from free shipping
					$addon = $this->_getRemainAmountFromFreeShipping();

					Mage::getSingleton('checkout/session')->addSuccess(
						'<div class="j2tajax-checkout-img">'.
						$img.'</div><div class="j2tajax-checkout-txt">'.
						$message.
						'</div>'.
						$addon
					);
                }
                catch (Mage_Core_Exception $e) {
                    if (Mage::getSingleton('checkout/session')->getUseNotice(TRUE)) {
                        Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                    } else {
                        $messages = array_unique(explode("\n", $e->getMessage()));
                        foreach ($messages as $message) {
                            Mage::getSingleton('checkout/session')->addError($message);
                        }
                    }
                }
                catch (Exception $e) {
                    Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
                }

            }
        }
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');

        $this->renderLayout();
    }


    public function productoptionAction()
    {
        //getProductUrlSuffix
        echo 'test';
        die;
    }
    
    public function productcheckAction()
    {
        $params = $this->getRequest()->getParams();
        
        $productTypes = array(
            Mage_Catalog_Model_Product_Type::TYPE_GROUPED,
            Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
            Mage_Catalog_Model_Product_Type::TYPE_BUNDLE,
        );
        
        $storeId = Mage::app()->getStore()->getId();
        
        if($product_id = $params['product']){
            
            $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($product_id);
            if ($product->getId()){
                if($product->getHasOptions() || in_array($product->getTypeId(), $productTypes)){
                    //echo get product url
                    echo $product->getProductUrl();
                    die;
                }
            }
        }
        echo 0;
        die;
    }

    public function addtocartAction()
    {
        $this->indexAction();
    }



    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
    }


}