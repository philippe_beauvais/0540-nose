<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zendesk\\API\\' => array($vendorDir . '/zendesk/zendesk_api_client_php/src'),
    'Mailjet' => array($vendorDir . '/mailjet/mailjet-apiv3-php/src'),
    'Inflect' => array($vendorDir . '/mmucklo/inflect/src'),
);
