<div id="maincolumn">

	<h2 class="main" style="background:url(<?php echo base_url(); ?>modules/Nose/assets/images/icon_48_module.png) no-repeat top left;"><?php echo config_item('module_nose_name'); ?></h2>

	<div class="subtitle">

		<!-- About this module -->
		<p class="lite">
			<?php echo lang('module_nose_about'); ?>
		</p>

	</div>
	
	<!-- Tabs -->
	<div id="moduleNoseTab" class="mainTabs">
		
		<ul class="tab-menu">

			<li><a><?= lang('module_nose_tab_creators') ?></a></li>
<!--			<li class="right"><a><?= lang('module_nose_tab_settings') ?></a></li>-->

		</ul>
		<div class="clear"></div>
	
	</div>

	<div id="moduleNoseTabContent">
		<div class="tabcontent" id="moduleNoseCreatorsContainer">
		</div>
		<div class="tabcontent">
		</div>
	</div>
	
</div>

<script type="text/javascript">
	
	/**
	 * Panel toolbox
	 * Init the panel toolbox is mandatory !!! 
	 *
	 */
	ION.initToolbox('empty_toolbox');

	new TabSwapper({tabsContainer: 'moduleNoseTab', sectionsContainer: 'moduleNoseTabContent', selectedClass: 'selected', deselectedClass: '', tabs: 'li', clickers: 'li a', sections: 'div.tabcontent', cookieName: 'moduleNoseTab' });


	ION.HTML('module/nose/creator/get_list', {}, {'update':'moduleNoseCreatorsContainer'});


</script>
