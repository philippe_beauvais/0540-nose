
<h3><?= lang('module_nose_title_pages_linked_to_creator') ?></h3>


<?php if( ! empty($pages)) :?>


	<ul class="sortable-container mr20" id="noseCreatorPageList">

		<?php foreach($pages as $page) :?>
		
			<?php 
				$title = '';
			?>
		
			<li class="sortme">
	
				<!-- Unlink icon -->
				<a class="icon unlink right" data-id="<?= $page['id_page'] ?>"></a>
		
				<!-- Title -->
				<span class="left title" title="<?= $page['title'] ?>">
				
					<?php foreach($page['breadcrumb'] as $idx => $breadcrumb) :?>

						<?php if ( $idx > 0 ) :?>> <?php endif ;?>
						<?= $breadcrumb['title'] ?>

					<?php endforeach ;?>
				
				</span>
	
			</li>
			
		<?php endforeach ;?>

	</ul>

	<script type="text/javascript">
		
		$$('#noseCreatorPageList li .unlink').each(function(item)
		{
			ION.initRequestEvent(item, 
				'<?= admin_url() ?>/module/nose/creator/remove_link', 
				{
					'parent': 'page',
					'id_parent': item.getProperty('data-id'),
					'creator_id':'<?= $creator_id ?>',
					'update':'noseCreatorPageLinkContainer'
				}
			);
		});
		
		
	</script>
	
<?php else :?>

	<textarea id="link" data-id="<?= $creator_id ?>" class="inputtext w200 h30 droppable empty nofocus dropPageToNoseCreator" alt="<?= lang('module_nose_label_drop_page_here') ?>"></textarea>

	<script type="text/javascript">
		
		
		// Load the Module JS / CSS
		var noseScript = Asset.javascript('<?= base_url() ?>modules/Nose/assets/javascript/nose.js', {
			id: 'noseScript',
			onLoad: function()
			{
				Asset.css('<?= base_url() ?>modules/Nose/assets/css/nose.css');
	
			}
		});

		ION.initDroppable();

		/**
		 * Make each page draggable
		 *
		 */
		$$('.treeContainer .page a.title').each(function(item, idx)
		{
			ION.addDragDrop(item, '.dropPageToNoseCreator', 'NOSE.dropPageToNoseCreator');
		});	
	
		$$('.treeContainer').each(function(tree, idx)
		{
			tree.retrieve('tree').addEvent('get', function()
			{
				$$('.treeContainer .page a.title').each(function(item, idx)
				{
					ION.addDragDrop(item, '.dropPageToNoseCreator', 'NOSE.dropPageToNoseCreator');
				});	
			});
		});	

	</script>


<?php endif ;?>



