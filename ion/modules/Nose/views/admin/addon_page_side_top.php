
<!-- Options -->
<h3 class="toggler"><?= lang('module_nose_title_creator') ?></h3>

<div class="element">


	<!-- Linked Creator -->
	<?php if ($page['id_page'] != '') :?>
	
		<div id="noseCreatorLinkContainer"></div>
		
	<?php endif ;?>

	
	<!-- Btn to Creators list window -->
	<dl class="small compact">
		<dt><label></label></dt>
		
		<dd>
			<button id="btnModuleNoseLinkCreator" type="button" class="light-button plus"><?= lang('module_nose_button_link_creator') ?></button>
		</dd>
	</dl>


</div>

<script type="text/javascript">

	// Loads JS & CSS
	var noseScript = Asset.javascript('<?= base_url() ?>modules/Nose/assets/javascript/nose.js', {
		id: 'noseScript',
		onLoad: function()
		{
			Asset.css('<?= base_url() ?>modules/Nose/assets/css/nose.css');
		}
	});


	// Creators List window
	$('btnModuleNoseLinkCreator').addEvent('click', function()
	{
		ION.dataWindow('noseCreatorList', 'module_nose_title_creator_list', ION.adminUrl + 'module/nose/creator/', {'width':400, 'height':450});
	
		return false;
	});
	
	// Link to Creators
	if ($('noseCreatorLinkContainer'))
	{
		ION.HTML(admin_url + 'module/nose/creator/get_link', {
			'parent':'page', 
			'id_parent': '<?= $page['id_page'] ?>'
		},
		{'update': 'noseCreatorLinkContainer'});
	}

</script>