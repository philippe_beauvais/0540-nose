
<?php if( ! empty($creator)) :?>

	<dl class="small mb0">
		<dt>
			<label title="<?= lang('module_nose_label_drop_creator_here') ?>"><?= lang('module_nose_label_creator') ?></label>
			<br/>
		</dt>
		<dd>
			<ul class="sortable-container mr20" id="noseCreatorlinkedList">
			
				<li class="sortme">
		
					<!-- Unlink icon -->
					<a class="icon unlink right"></a>
			
					<!-- Title -->
					<a style="overflow:hidden;height:16px;display:block;" class="pl5 pr10 title" title="<?= $creator['creator_name'] ?>"><?= $creator['creator_name'] ?></a>
		
				</li>
		
			</ul>
		</dd>
	</dl>

	<script type="text/javascript">
		
		$$('#noseCreatorlinkedList li .unlink').each(function(item)
		{
			ION.initRequestEvent(item, 
				'<?= admin_url() ?>/module/nose/creator/remove_link', {
					'parent':'<?= $parent ?>',
					'id_parent':'<?= $id_parent ?>', 
					'creator_id':'<?= $creator['creator_id']?>',
					'update':'noseCreatorLinkContainer'
				});
		});
		
		$$('#noseCreatorlinkedList li .title').each(function(item)
		{
			item.addEvent('click', function()
			{
				ION.dataWindow('noseCreatorList', 'module_nose_title_creator_list', ION.adminUrl + 'module/nose/creator/', {'width':400, 'height':450}, {'creator_id': '<?= $creator['creator_id']?>'});
			});
		});
		
	</script>
	
<?php else :?>

	<dl class="small dropNoseCreator" rel="<?= $parent ?>.<?= $id_parent ?>" data-type="page" data-id="<?= $id_parent ?>">
		<dt>
			<label for="link"><?= lang('module_nose_label_creator') ?></label>
			<br/>
		</dt>
		<dd>
			<textarea id="link" class="inputtext w140 h30 droppable empty nofocus" alt="<?= lang('module_nose_label_drop_creator_here') ?>"></textarea>
		</dd>
	</dl>

	<script type="text/javascript">
		
		ION.initDroppable();

	</script>


<?php endif ;?>



