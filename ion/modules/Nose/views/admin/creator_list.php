
<!-- Filter Form -->
<form id="panelCreatorFilterForm" name="panelCreatorFilterForm">
	<input id="panelCreatorFilter" name="panelCreatorFilter" class="inputtext w150" type="text" value="" />
	<button id="panelCreatorFilterReset" type="button" class="toolbar-button" value=""><?= lang('ionize_button_reset_filter') ?></button>
</form>


<ul id="creatorPanelList" class="list mb20 mt10">

<?php foreach($creators as $creator) :?>

	<?php
		$id = $creator['creator_id'];
	?>

	<li class="creator<?= $id ?> pointer" id="creator_<?= $id ?>" rel="<?= $id ?>" data-id="<?= $id ?>">
		<a class="icon drag left"></a>
		<a class="left pl5 edit title" data-id="<?= $id ?>"><?= $creator['creator_name'] ?></a>
	</li>
	
<?php endforeach ;?>

</ul>
<script type="text/javascript">
	

	// Click Event to display the details of one creator
	$$('#creatorPanelList li').each(function(item, idx)
	{
		var id = item.getProperty('data-id');
		var a = item.getElement('a.title');

		a.addEvent('click', function(e)
		{
			ION.HTML('module/nose/creator/get_detail/' + id, {}, {'update': 'moduleNoseCreatorsContainer' });		
		});

		ION.addDragDrop(a, '.dropNoseCreator', 'NOSE.dropNoseCreatorOnParent');
	});
	
	// Filter
	var creatorListFilter = new ION.ListFilter($('creatorPanelList'), 
	{
		'items': '.title',
		'form': $('panelCreatorFilterForm'), 
		'input': $('panelCreatorFilter'),
		'reset': $('panelCreatorFilterReset')
	});

	
</script>