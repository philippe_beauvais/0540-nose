





<?php

	$id = $creator_id;

// trace($this->_ci_cached_vars);

	if ( ! empty($medias))
	{
		$picture = $medias[0];
	}

	$class = ' pl0';
	$background = '';
	
	if (  ! empty($picture) )
	{
		$class = ' pictured';
		$thumb_size = (Settings::get('media_thumb_size') != '') ? Settings::get('media_thumb_size') : '120';
		$background = 'background:url(' . config_item('module_nose_base_url') . 'files/.thumbs/'. substr($picture['path'],6).'?'.time() . ') no-repeat top left';
	}
	
?>

<!--<h2 class="main creator"><?= $creator_name ?></h2>-->


<h2 class="main<?= $class ?>" id="creator_title<?= $id ?>" style="<?= $background ?>"><?= $creator_name ?></h2>

<?php if( ! empty($id) && TRUE == FALSE) :?>
	<span class="main subtitle<?= $class ?>"><a class="graylink italic fmButton" id="changeTitlePicture"><?= lang('app_label_change_picture') ?></a></span>
<?php endif ;?>

<!-- Back To List button -->
<p>
	<button id="creatorDetailBackButton" class="light-button back"><?= lang('module_nose_button_back_to_creator_list') ?></button>
</p>


<!-- Linked Page -->
<div id="noseCreatorPageLinkContainer<?= $id ?>"></div>


<h3 class="mt20"><?= lang('module_nose_creator_description') ?></h3>
<p class="lite mt10 ml10">
	<?= lang('module_nose_help_creator_detail') ?>
</p>

<form name="creatorForm<?= $id ?>" id="creatorForm<?= $id ?>" action="<?= admin_url() ?>module/nose/creator/save">

	<!-- Hidden fields -->
	<input id="creator_id<?= $id ?>" name="creator_id" type="hidden" value="<?= $id ?>" />
	<input id="creator_name<?= $id ?>" name="creator_name" type="hidden" value="<?= $creator_name ?>" />


	<fieldset>

		<!-- Tabs -->
		<div id="creatorTab<?= $UNIQ ?>" class="mainTabs">
			<ul class="tab-menu">
				<?php foreach($languages as $l) :?>
					<li class="tab_edit_creator<?= $UNIQ ?><?php if($l['def'] == '1') :?> dl<?php endif ;?>"><a><span><?= ucfirst($l['name']) ?></span></a></li>
				<?php endforeach ;?>
			</ul>
			<div class="clear"></div>
		</div>
	
	
		<div id="creatorTabContent<?= $UNIQ ?>">
			
			<!-- Text block -->
			<?php foreach($languages as $language) :?>
		
				<?php $lang = $language['lang']; ?>
				
				<div class="tabcontent<?= $UNIQ ?>">
		
					<!-- description -->
					<textarea id="description_<?= $lang ?><?= $UNIQ ?>" name="description_<?= $lang ?>" class="tinyCreator w320 h120" rel="<?= $lang ?>"><?= ${$lang}['description'] ?></textarea>
		
				</div>
				
			<?php endforeach ;?>
		
		</div>
	
	</fieldset>

</form>

<!-- Save / Cancel buttons
	 Must be named bSave[windows_id] where 'window_id' is the used ID for the window opening through MUI.formWindow()
--> 
<div class="buttons">
	<button id="bSaveCreator<?= $UNIQ ?>" type="button" class="button yes right mr40"><?= lang('ionize_button_update') ?></button>
</div>





<script type="text/javascript">

	/** 
	 * Back button
	 *
	 */
	if ($('creatorDetailBackButton'))
	{
		$('creatorDetailBackButton').addEvent('click', function(el)
		{
			ION.HTML('module/nose/creator/get_list', {}, {'update': 'moduleNoseCreatorsContainer' });
		});
	}

	/** 
	 * Tabs init
	 *
	 */
	new TabSwapper({tabsContainer: 'creatorTab<?= $UNIQ ?>', sectionsContainer: 'creatorTabContent<?= $UNIQ ?>', selectedClass: 'selected', deselectedClass: '', tabs: 'li', clickers: 'li a', sections: 'div.tabcontent<?= $UNIQ ?>' });


	/**
	 * TinyEditors
	 * Must be called after tabs init.
	 *
	 */
	ION.initTinyEditors('.tab_edit_creator<?= $UNIQ ?>', '#creatorTabContent<?= $UNIQ ?> .tinyCreator', 'small', {'height':120});


	// Update Form submit
	$('bSaveCreator<?= $UNIQ ?>').addEvent('click', function(e) {
		e.stop();
		ION.sendData('module/nose/creator/save_detail', $('creatorForm<?= $id ?>'));
	});

	// Linked to Page
	if ($('noseCreatorPageLinkContainer<?= $id ?>'))
	{
		ION.HTML(admin_url + 'module/nose/creator/get_linked_pages', {'creator_id': '<?= $id ?>'}, {'update': 'noseCreatorPageLinkContainer<?= $id ?>'});
	}





</script>