<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Nose admin controller
*
* @author	Partikule Studio
*
*/
class Nose extends Module_Admin 
{
	/**
	* Constructor
	*
	* @access	public
	* @return	void
	*/
	function construct()
	{
	}

	/**
	* Admin panel
	* Called from the modules list.
	*
	* @access	public
	* @return	parsed view
	*/
	function index()
	{
		$this->output('admin/nose');
	}

	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Adds "Addons" to core panels
	 * When set, this function will be automatically called for each core panel.
	 *
	 * One addon is a view from the module which will be displayed in a core panel,
	 * to add some interaction with the current edited element (page, article)
	 *
	 * 
	 * Core Panels which accepts addons :
	 *  - article : Article Edition Panel
	 *  - page : Page Edition Panel
	 *  - media : Media Edition Panel
	 *
	 * Placeholders :
	 * In each "Core Panel", some placeholder are defined :
	 *  - 'side_top' : Side Column, Top
	 *  - 'side_bottom' : Side Column, Bottom
	 *  - 'main_top' : Main Column, Top
	 *  - 'main_bottom' : Main Column, Top
	 *  - 'toolbar' : Top toolbar
	 * 
	 *
	 * @param	Array	The current edited object.
	 *
	 */
	function _addons($object)
	{
		$CI =& get_instance();
		
		if (isset($object['id_page']))
		{
			
			// Send Page ID to view
			$data['page'] = $object;
			
			// Side Top Page Addon
			$CI->load_addon_view('nose', 'page', 'side_top', 'admin/addon_page_side_top', $data);
		}
	}	


}

/* End of file magento.php */
/* Location: /modules/Magento/controllers/admin/magento.php */
