<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Noses Admin controller
*
* @author	Partikule Studio
*
*/
class Creator extends Module_Admin 
{
	/**
	 * Fields on wich the htmlspecialchars function will not be used before saving
	 * 
	 * @var array
	 */
	protected $no_htmlspecialchars = array('family_name');
	

	/**
	 * Fields on wich no XSS filtering is done
	 * 
	 * @var array
	 */
	protected $no_xss_filter = array();


	/*
	 * Array of data
	 *
	 */
	protected $data = array();
	
	
	// ------------------------------------------------------------------------
	

	/**
	* Constructor
	*
	* @access	public
	* @return	void
	*/
	function construct()
	{
		$this->load->model('nose_creator_model', '', true);
		$this->load->model('page_model', '', true);
	}
	

	// ------------------------------------------------------------------------
	
	
	/**
	* Admin panel
	* Called from the modules list.
	*
	* @access	public
	* @return	parsed view
	*/
	function index()
	{
		// If one creator's ID is received, the view will load the detail panel
		$this->template['creator_id'] = $this->input->post('creator_id');
	
		$this->output('admin/creator_window');
	}
	

	// ------------------------------------------------------------------------
	

	function get_list()
	{
		$this->template['creators'] = $this->nose_creator_model->get_list(array('order_by' => 'creator_name ASC'));
	
		$this->output('admin/creator_list');
	}
	

	// ------------------------------------------------------------------------
	

	function get_detail($id)
	{
//		$this->template['creator_lang'] = $this->nose_creator_model->get_lang(array('creator_id' => $id));
//		$this->template['creator'] = $this->nose_creator_model->get(array('creator_id' => $id));

		$this->template = $this->nose_creator_model->get(array('creator_id' => $id));

		$this->nose_creator_model->feed_lang_template($id, $this->template);

		$this->template['languages'] = $this->nose_creator_model->get_languages();
	
		$this->output('admin/creator_detail');
	}
	

	// ------------------------------------------------------------------------
	

	/**
	 * Gets parent linked Creator
	 *
	 */
	function get_link()
	{
		// Parent
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		
		$this->template = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'creator' => $this->nose_creator_model->get_linked_creator($parent, $id_parent),
		);

		$this->output('admin/creator_link');
	
	}

	// ------------------------------------------------------------------------
	

	/**
	 * Gets the creator's linked page list
	 *
	 */
	function get_linked_pages()
	{
		$creator_id = 	$this->input->post('creator_id');

		$pages = $this->nose_creator_model->get_linked_pages($creator_id);
		
		if ( ! empty($pages))
		{
			// Add breadcrumb (array of pages) to each page
			foreach($pages as &$page)
			{
				$page['breadcrumb'] = $this->page_model->get_parent_array(array('id_page' => $page['id_page']), array(), Settings::get_lang('default'));
			}
		}

		$this->template = array(
			'creator_id' => $creator_id,
			'pages' => $pages
		);

		$this->output('admin/creator_linked_pages');
	}

	// ------------------------------------------------------------------------
	

	/**
	 * Adds parent linked Creator
	 *
	 */
	function add_link()
	{
		// Parent
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		$creator_id = 	$this->input->post('creator_id');
		
		$creator = $this->nose_creator_model->link_creator_to_parent($parent, $id_parent, $creator_id);
		
		$this->updateDomContainer($parent, $id_parent, $creator_id);
	}
	
	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Removes parent linked Creator
	 *
	 */
	function remove_link()
	{
		// Parent
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		$creator_id = 	$this->input->post('creator_id');

		$creator = 	$this->nose_creator_model->unlink_creator_from_parent($parent, $id_parent, $creator_id);
		
		$this->updateDomContainer($parent, $id_parent, $creator_id);
	}
	
	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Saves one Nose details
	 *
	 */
	function save_detail()
	{
		// Prepare data before saving
		$this->_prepare_data();

		// Saves to DB
		$this->id = $this->nose_creator_model->save($this->data, $this->lang_data);

		$this->update[] = array(
			'element' => 'moduleNoseCreatorsContainer', 
			'url' => admin_url() . 'module/nose/creator/get_detail/' . $this->id
		);

		$this->success(lang('ionize_message_operation_ok'));
	}
	
	
	// ------------------------------------------------------------------------

	protected function updateDomContainer($parent, $id_parent, $creator_id)
	{
		$this->callback = array(
			array(
				'fn' => 'ION.HTML',
				'args' => array('module/nose/creator/get_linked_pages', array('creator_id' => $creator_id), array('update' => 'noseCreatorPageLinkContainer'.$creator_id))
			),
			array(
				'fn' => 'ION.HTML',
				'args' => array('module/nose/creator/get_link', array('parent' => $parent,'id_parent' => $id_parent), array('update' => 'noseCreatorLinkContainer'))
			)			
		);

		$this->response();

	}	
	
	// ------------------------------------------------------------------------


	/** 
	 * Prepare data before saving
	 *
	 */
	function _prepare_data() 
	{
		// Standard fields
		$fields = $this->nose_creator_model->list_fields('creator');
		
		// Set the data to the posted value.
		foreach ($fields as $field)
			$this->data[$field] = $this->input->post($field);

		$this->lang_data = array();

		$fields = $this->nose_creator_model->list_fields('creator_lang');

		foreach($this->nose_creator_model->get_languages() as $language)
		{
			foreach ($fields as $field)
			{
				if ($this->input->post($field.'_'.$language['lang']) !== false)
				{
					// Avoid or not security XSS filter
					if ( ! in_array($field, $this->no_xss_filter))
						$content = $this->input->post($field.'_'.$language['lang']);
					else
					{
						$content = stripslashes($_REQUEST[$field.'_'.$language['lang']]);
					}
	
					// Convert HTML special char only on other fields than these defined in $no_htmlspecialchars
					if ( ! in_array($field, $this->no_htmlspecialchars))
						$content = htmlspecialchars($content, ENT_QUOTES, 'utf-8');
						
					$this->lang_data[$language['lang']][$field] = $content;
				}
			}
		}

	}
	
}
