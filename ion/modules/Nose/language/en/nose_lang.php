<?php

$lang['module_nose_about'] = "eNose connexion module";

$lang['module_nose_title_creator'] = "eNose";
$lang['module_nose_title_creator_list'] = "Noses List";

$lang['module_nose_button_link_creator'] = "Noses List";
$lang['module_nose_button_back_to_creator_list'] = "back to Noses list";

$lang['module_nose_help_creator_detail'] = "Editing the Nose description through this window will :<br/> 
											- Update the Creator information in eNose<br/>
											- Make the description available on the online Store";


$lang['module_nose_label_drop_creator_here'] = "Drop one Nose here";
$lang['module_nose_label_creator'] = "Nose";

$lang['module_nose_creator_description'] = "Nose description";

$lang['module_nose_tab_creators'] = "Creators";
$lang['module_nose_tab_settings'] = "Settings";
$lang['module_nose_label_drop_page_here'] = "Drop one page here...";


$lang['module_nose_title_pages_linked_to_creator'] = "Linked pages";
