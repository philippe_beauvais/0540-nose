<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Magento Module Main Model
 *
 */

class Nose_model extends Base_model 
{
	// Magento Store ID
	protected $_storeId = 0;
	
	// Nose DB Connection
	protected $_NDB;
	
	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		$this->_NDB = $this->load->database(config_item('enose_db_group'), TRUE); 
	
		parent::__construct();
	}




}