<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Creator (Noses) Model
 *
 */

class Nose_creator_model extends Base_model 
{
	// Link table between Ionize parent (article, page) and Nose Creators
	protected $_creators_table = 'module_nose_creators';
	
	
	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		// Set the DB Group
		$this->db_group = config_item('enose_db_group');
		$this->_NDB = $this->load->database(config_item('enose_db_group'), TRUE); 
		
		$this->set_table('creator');
		$this->set_lang_table('creator_lang');
		$this->set_pk_name('creator_id');
		
		$this->set_with_media_table();
		
		parent::__construct();
	}
	
	
	public function get_linked_creator($parent, $id_parent)
	{
		$creators_ids = $this->_get_linked_creators_ids($parent, $id_parent);

		if ( ! empty($creators_ids) )
		{
			$creators = $this->_get_nose_creators_from_ids($creators_ids);

			// Check DB integrity
			$this->_delete_non_existing_creators($parent, $id_parent, $creators_ids, $creators);

			if ( ! empty($creators))
				return $creators[0];
		}
		
		return array();
	}

/**
 * To Finish
 *
 */
	public function get_linked_pages($creator_id)
	{
		$this->db->where(array(
			'creator_id' => $creator_id,
			'parent' => 'page',
			'lang' => Settings::get_lang('default')
		));
		$this->db->from($this->_creators_table);
		$this->db->join('page_lang', 'page_lang.id_page = '.$this->_creators_table.'.id_parent', 'inner');
		
		$query = $this->db->get();
		
		$pages = $query->result_array();
		
		return $pages;
		
	}

	
	
	public function link_creator_to_parent($parent, $id_parent, $creator_id)
	{
		$data = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'creator_id' => $creator_id		
		);
		$this->db->where($data);
		
		$query = $this->db->get($this->_creators_table);

		if ($query->num_rows() == 0)
		{
			$this->db->insert($this->_creators_table, $data);
			return TRUE;
		}
		return FALSE;
	}
	
	
	
	public function unlink_creator_from_parent($parent, $id_parent, $creator_id)
	{
		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'creator_id' => $creator_id		
		);
		
		return $this->db->delete($this->_creators_table, $where);
	}
	
	
	
	/**
	 * Returns an array of Nose Creators
	 * from eNose Creator table
	 * usage of $this->db_group to connect to eNose DB.
	 * config. in database.php
	 * @param	array	Array of Nose Creators
	 *
	 */
	protected function _get_nose_creators_from_ids($ids)
	{
		$creators = array();
	
		$this->_NDB->where_in('creator_id', $ids);

		$query = $this->_NDB->get('creator');
		
		if ($query->num_rows() > 0)
		{
			$creators = $query->result_array();
		}
		return $creators;
	}

	

	protected function _get_linked_creators_ids($parent, $id_parent)
	{
		$result = array();

		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent
		);

		$this->db->where($where);
		
		$this->db->select('creator_id');
		
		$query = $this->db->get($this->_creators_table);
		
		if ($query->num_rows() > 0)
		{
			foreach($query->result_array() as $creator)
			{
				$result[] = $creator['creator_id'];
			}
			
			return $result;
		}
		
		return $result;
	}
	
	
	protected function _delete_non_existing_creators($parent, $id_parent, $creators_ids, $creators)
	{
		$ids_to_delete = array();
		
		foreach($creators_ids as $id)
		{
			$found = FALSE;
			foreach($creators as $creator)
			{
				if ($creator['creator_id'] == $id)
					$found = TRUE;
			}
			if ($found == FALSE) $ids_to_delete[] = $id;
		}

		if (!empty ($ids_to_delete))
		{
			$this->db
				->where('id_parent', $id_parent)
				->where('parent', $parent)
				->where_in('entity_id', $ids_to_delete)
				->delete($this->_creators_table);
		}
	}


}