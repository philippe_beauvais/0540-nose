<?php 

$config['module_nose_name'] = "eNose";

/*
|--------------------------------------------------------------------------
| eNose DB group
| + eNose Base URL (with trailing slash)
|--------------------------------------------------------------------------
| DB access groups are defined in /application/config/database.php
|
*/
$config['enose_db_group'] = 'enose.prod';

switch (SITE_IDENTIFIER)
{
	case 'prod.live': 
		
		$config['module_nose_base_url'] = "http://enose2.nose.fr/";
		break;
	
	case 'preprod.live': 
		$config['module_nose_base_url'] = "http://preprod.enose.nose.nbs-test.com/";
		break;
	
	case 'prod.local': 
		$config['module_nose_base_url'] = "http://enose3.home/";
		break;

	default:
		$config['module_nose_base_url'] = "http://preprod.enose.nose.nbs-test.com/";
		break;
}


