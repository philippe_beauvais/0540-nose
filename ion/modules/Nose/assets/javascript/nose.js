/**
 *  Ionize Nose Module
 * 
 *  @author	Partikule Studio
 *  @since	0.9.8
 * 
 */
var NOSE = (NOSE || {});

NOSE.append = function(hash){
	Object.append(NOSE, hash);
}.bind(NOSE);

NOSE.append({
	
	baseUrl: base_url,
	adminUrl: admin_url,
	moduleUrl: admin_url + 'module/nose/',
	
	/**
	 * on drop of one creator to a parent
	 *
	 */
	dropNoseCreatorOnParent: function(element, droppable, event)
	{
		ION.JSON(admin_url + 'module/nose/creator/add_link', {
			'parent': droppable.getProperty('data-type'), 
			'id_parent': droppable.getProperty('data-id'), 
			'creator_id': element.getProperty('data-id'),
			'update':'noseCreatorLinkContainer'
		});
	},
	
	dropPageToNoseCreator: function(element, droppable, event)
	{
		ION.JSON(admin_url + 'module/nose/creator/add_link', {
			'parent': element.getProperty('data-type'), 
			'id_parent': element.getProperty('data-id'), 
			'creator_id': droppable.getProperty('data-id'),
			'update':'noseCreatorPageLinkContainer'
		});
	}


});
