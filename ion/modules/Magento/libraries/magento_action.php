<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Magento_Action
{
	/*
	 * We need to invoke process_data once this class is created.
	 * But we can't use the constructor, as the tag won't be available there.
	 * So we need this pseudo_construct in order not to leak memory.
	 */
	private $pseudo_constructed = false;

	function __construct()
    {
		$ci =  &get_instance();

		if (!isset($ci->form_validation))
			$ci->load->library('form_validation');
    }

	
	public function pseudo_construct($tag)
	{
		if ($this->pseudo_constructed)
			return FALSE;
		
		$this->pseudo_constructed = true;

//		$this->_set_error_messages();

		return $this->process_data($tag);
	}


	/*
	 * Tags
	 */
	public function help($tag)
	{
		$ci =  &get_instance();
		$output = $tag->parse_as_nested(file_get_contents(MODPATH.'Magento/views/tag_help'.EXT));
		return $output;
	}


	/*
	 * This function is called every time Simpleform is used. It checks whether there is something to save or something to do
	 * All this is done at the first place in order to make every tag render the right thing.
	 * If this wasn't used like that, some tags would display wrong data, cause things would be displayed, before they're saved.
	 *
	 */
	public function process_data($tag)
	{
	}
	
	
}
