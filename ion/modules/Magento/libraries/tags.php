<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Magento tags
*
* @author	Partikule Studio
*/
class Magento_Tags
{
	/**
	* Index
	*
	* Returns the requested action.
	*
	* @access	public
	* @return	mixed
	*/
	public static function index(FTL_Binding $tag)
	{
		$ci = &get_instance();
		if( ! isset($ci->magento_action))
			$ci->load->library('magento_action');

		if( ! isset($ci->magento))
			$ci->load->library('magento');

		$ci->magento->getMage();

		$processed = $ci->magento_action->pseudo_construct($tag);



		return $tag->expand();
	}
	
	
	/*
	 *
	 * @usage : 	<ion:magento:connection />
	 *
	 */	
	public static function connection(FTL_Binding $tag)
	{
		$ci = &get_instance();
	
		$ci->load->library('xmlrpc');
		
//$ci->xmlrpc->set_debug(TRUE);

		$ci->xmlrpc->server(config_item('magento_url'), 80);
		$ci->xmlrpc->method('login');
		
		$request = array(config_item('magento_user'), config_item('magento_password'));

		$ci->xmlrpc->request($request);

		$answer = $ci->xmlrpc->send_request();

//trace($answer);	

		
		if ( ! $answer)
		{
			trace($answer);
			trace($ci->xmlrpc->display_error());
		}
		else
		{
			trace($answer);
		}
		
	
	}

	public static function products(FTL_Binding $tag)
	{
	}
	
	
	public static function best_sellers(FTL_Binding $tag)
	{
		$ci = &get_instance();
		
		
	}
	
	
	
	/**
	* Validation tag
	*
	* @access	public
	* @usage	<ion:quote:validation 	[attr="has_errors"] [form_name="<form_name>"] [is_like="<1/0>"] // Container-tag. If true, the inner html/tags will be shown. If form_name is set, it'll only check for errors for the specified form.
	* 									[attr="error_string"] // Returns an error string if has_errors is true
	* 									[attr="has_notices"] [form_name="<form_name>"] [is_like="<1/0>"] // Container-tag. If true, the inner html/tags will be shown. If form_name is set, it'll only check for errors for the specified form.
	* 									[attr="notice_string"] // Returns a notice string if has_notices is true
	* 									[attr="has_success"] [form_name="<form_name>"] [is_like="<1/0>"] // Container-tag. If true, the inner html/tags will be shown. If form_name is set, it'll only check for errors for the specified form. Is true if data was saved.
	* 									[attr="success_string"] // Returns a success string if has_success is true
	*			/>
	* @return	mixed
	*/
	public static function validation(FTL_Binding $tag)
	{
		$ci = &get_instance();
		
		if( ! isset($ci->simpleform_validation))
			$ci->load->library('simpleform_validation');

		switch($tag->attr['attr'])
		{
			case 'has_errors':
				return $ci->simpleform_validation->has_errors($tag);
				break;
			case 'error_string':
				return $ci->simpleform_validation->error_string($tag);
				break;
			case 'has_notices':
				return $ci->simpleform_validation->has_notices($tag);
				break;
			case 'notice_string':
				return $ci->simpleform_validation->notice_string($tag);
				break;
			case 'has_success':
				return $ci->simpleform_validation->has_success($tag);
				break;
			case 'success_string':
				return $ci->simpleform_validation->success_string($tag);
				break;
			default:
				return $tag->expand();
		}
	}
	
	/**
	 * Returns a field value from a form
	 *
	 * @usage	<ion:simpleform:field [attr="<field_name>"]	[from_post_data="<form_name>"]		// The wished field from the given form
	 *
	 */
	public static function field(FTL_Binding $tag)
	{
		$ret = '';
		
		$ci = &get_instance();

		include MODPATH . 'Simpleform/config/config.php';
		
		if(isset($tag->attr['from_post_data']) && $ci->input->post('form_name') === $tag->attr['from_post_data'])
		{
			$ret = ! ($ci->input->post($tag->attr['name']) === FALSE) ? $ci->input->post($tag->attr['name']) : $ret;
		}		
		
		// If only the post data is requested
		if( ! isset($tag->attr['is_like']))
			return $ret;
		
		// If the post data is compared to is_like
		else
			return $tag->attr['is_like'] === $ret ? $tag->expand() : "";	
	}
}

/* End of file tags.php */
/* Location: /modules/Magento/libraries/tags.php */
