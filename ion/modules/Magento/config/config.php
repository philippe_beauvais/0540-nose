<?php 

$config['module_magento_name'] = "Magento";


/*
|--------------------------------------------------------------------------
| Magento path
|--------------------------------------------------------------------------
| Must be the absolute real path to your Magento installation
| Needed to load the Mage class
|
*/
switch (SITE_IDENTIFIER)
{
	case 'prod.live': 
		$config['magento_path'] = '/data/www/client/prod/nose/';
		break;
	
	case 'preprod.live':
		$config['magento_path'] = '/data/www/client/preprod/nose/';
		break;
	
	case 'prod.local': 
		$config['magento_path'] = '/var/www/html/nose/';
		break;

	default:
		$config['magento_path'] = $_SERVER['DOCUMENT_ROOT'].'/';
		break;
}


/*
|--------------------------------------------------------------------------
| Magento API Credentials
|--------------------------------------------------------------------------
| http://www.localhost.com/magento/api/?wsdl
|
*/

/*
$config['magento_url'] = 'http://www.localhost.com/magento/api/xmlrpc/';
$config['magento_user'] = 'nose';
$config['magento_password'] = '123456';
*/

/*
|--------------------------------------------------------------------------
| Magento templates path
|--------------------------------------------------------------------------
| Folder which contains the Magento PHTML templates to link with Ionize pages
| Relative to the Ionize installation path.
| with trailing slash
|
*/

$config['magento_templates_path'] = '../app/design/frontend/nose/default/template/ionize/page/';




