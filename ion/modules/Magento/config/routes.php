<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$route['default_controller'] = "magento";
$route['(.*)'] = "magento/index/$1";
$route[''] = 'magento/index';

