<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Magento Category admin controller
*
* @author	Partikule Studio
*
*/
class Category extends Module_Admin 
{
	protected $_storeId = 0;

	/**
	 * Constructor
	 *
	 */
	function construct()
	{
//		$root_cat_id = config_item('magento_root_category_id');

		$this->load->library('magento');
		$this->load->model('magento_category_model', '', true);
		$this->load->model('page_model', '', true);
	}


	// ------------------------------------------------------------------------
	
	
	/**
	 * Gets one parent page / menu tree
	 *
	 */
	function get_category_tree()
	{
		if ($this->magento->mage())
		{
			// Parent
			$parent_id = $this->input->post('parent_id');
			
			// Products array
			$products = $this->magento_category_model->get_products_from_category($parent_id);
			
			$categories = $this->magento_category_model->get_sub_categories($parent_id);
			
			$this->response( array( 'categories' => $categories, 'products' => $products ) );
		}
		else
		{
			$this->output('admin/error');
		}
	}
	
	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Gets all Magento categories
	 *
	 */
	function get_categories()
	{
		if ($this->magento->mage())
		{
			$min_level = 1;
			$categories = $this->magento_category_model->get_all_categories($min_level);
			
			// Array of categories IDs
			$categories_ids = array();
			foreach($categories as $category)
				$categories_ids[] = $category->getId();
			
			foreach($categories as $key => $category)
			{
				// Magento keeps categories with no parents in DB...
				// so this check is mandatory to not get them.
				if ( in_array($category->getParentId(), $categories_ids) )
				{
					$pages = $this->magento_category_model->get_linked_elements('page', $category->getId() );
					
					if ( ! empty($pages))
					{
						// Add breadcrumb (array of pages) to each page
						foreach($pages as &$page)
						{
							$page['breadcrumb'] = $this->page_model->get_parent_array(array('id_page' => $page['id_page']), array(), Settings::get_lang('default'));
						}
					
						$category->setPages($pages);
					}
				}
				else
				{
					unset($categories[$key]);
				}
			}
			
			$this->template['categories'] = $categories;
			
			$this->output('admin/categories_table');
			
/*			
			foreach($categories as $category)
			{
				trace($category->getLevel() .'::'.$category->getId() .'::'. $category->getName());
			}
*/			
//			$this->response( array( 'categories' => $categories, 'products' => $products ) );
		}
		else
		{
//			$this->output('admin/error');
		}
	}
	
	

	// ------------------------------------------------------------------------
	
	
	/**
	 * Gets parent linked Category
	 *
	 */
	function get_link()
	{
		if ($this->magento->mage())
		{
			// Parent
			$parent = 		$this->input->post('parent');
			$id_parent = 	$this->input->post('id_parent');
			
			$this->template = array(
				'parent' => $parent,
				'id_parent' => $id_parent,
				'category' => $this->magento_category_model->get_linked_category($parent, $id_parent),
			);
	
			$this->output('admin/category_link');
		}
		else
		{
			$this->output('admin/error');
		}
	}
	

	// ------------------------------------------------------------------------
	
	
	/**
	 * Adds parent linked Category
	 *
	 */
	function add_link()
	{
		// Parent
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		$entity_id = 	$this->input->post('entity_id');
		
		$category = $this->magento_category_model->link_category_to_parent($parent, $id_parent, $entity_id);
		
		// If only one category is linked to the parent, set it as "Master"
		$parents = $this->magento_category_model->get_linked_elements($parent, $entity_id);

		if ( count($parents) == 1 )
		{
			$this->set_master_link();
		}
		
		// Update DOM container
		$this->updateCategoriesContainer($parent, $id_parent);
	}
	

	// ------------------------------------------------------------------------
	
	
	/**
	 * Removes parent linked Category
	 *
	 */
	function remove_link()
	{
		// Parent
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		$entity_id = 	$this->input->post('entity_id');

		$category = 	$this->magento_category_model->unlink_category_from_parent($parent, $id_parent, $entity_id);
		
		// Update DOM container
		$this->updateCategoriesContainer($parent, $id_parent);
	}
	
	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Set / Unset Category Link as master
	 * When one category link is master, this category refers to the linked page
	 *
	 */
	function set_master_link()
	{
		$parent = 		$this->input->post('parent');
		$id_parent = 	$this->input->post('id_parent');
		$entity_id = 	$this->input->post('entity_id');

		// Where
		$where = array(
			'parent' => 	$parent,
			'entity_id' => 	$this->input->post('entity_id')
		);
		
		// Unset all masters for this category
		$this->magento_category_model->update($where, array('master'=>'0'), 'module_magento_categories');

		// 
		$master = $this->input->post('master');
		if ( ! $master)
		{
			$where['id_parent'] = $id_parent;
			$this->magento_category_model->update($where, array('master'=>'1'), 'module_magento_categories');
		}
		
		// Update DOM container
		$this->updateCategoriesContainer($parent, $id_parent);
	}
	
	
	// ------------------------------------------------------------------------
	

	/**
	 * Updates the categories DOM display
	 *
	 */
	public function updateCategoriesContainer($parent, $id_parent)
	{
		switch($this->input->post('update'))
		{
			// Category in the Page view
			case 'magentoCategoryLinkContainer':
				$this->updateCategoryLinkContainer($parent, $id_parent);
				break;
			
			// Table of categories in the Magento module view
			case 'magentoCategoryTableContainer':
				$this->updateCategoryTableContainer();
				break;
		}
	}
	
	
	// ------------------------------------------------------------------------
	

	/**
	 * Send the update command to the view
	 *
	 */
	public function updateCategoryLinkContainer($parent, $id_parent)
	{
		$this->callback = array(
			array(
				'fn' => 'ION.HTML',
				'args' => array('module/magento/category/get_link', array('parent' => $parent,'id_parent' => $id_parent), array('update' => 'magentoCategoryLinkContainer'))
			)
		);

		$this->response();
	}
	
	
	// ------------------------------------------------------------------------
	

	/**
	 * Send the update command to the view
	 *
	 */
	public function updateCategoryTableContainer()
	{
		$this->callback = array(
			array(
				'fn' => 'ION.HTML',
				'args' => array('module/magento/category/get_categories', FALSE, array('update' => 'moduleMagentoTabContentCategories'))
			)
		);

		$this->response();
	}
	
	
}