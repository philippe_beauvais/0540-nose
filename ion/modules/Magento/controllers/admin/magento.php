<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Magento admin controller
*
* @author	Partikule Studio
*
*/
class Magento extends Module_Admin 
{
	/**
	* Constructor
	*
	* @access	public
	* @return	void
	*/
	function construct()
	{
		$this->load->model('magento_model', '', true);
	}

	/**
	* Admin panel
	* Called from the modules list.
	*
	* @access	public
	* @return	parsed view
	*/
	function index()
	{
		$this->output('admin/magento');
	}

	
	// ------------------------------------------------------------------------

	
	/**
	 * Saves one Magento link to a page
	 *
	 */
	public function save_page_template()
	{
		$data = array(
			'id_page' => $this->input->post('id_page'),
			'magento_template' => $this->input->post('magento_template')
		);
		
		$this->magento_model->save_template($data);
		
		$this->success(lang('module_magento_message_template_saved'));
	}	
	
	// ------------------------------------------------------------------------
	
	
	/**
	 * Adds "Addons" to core panels
	 * When set, this function will be automatically called for each core panel.
	 *
	 * One addon is a view from the module which will be displayed in a core panel,
	 * to add some interaction with the current edited element (page, article)
	 *
	 * 
	 * Core Panels which accepts addons :
	 *  - article : Article Edition Panel
	 *  - page : Page Edition Panel
	 *  - media : Media Edition Panel
	 *
	 * Placeholders :
	 * In each "Core Panel", some placeholder are defined :
	 *  - 'side_top' : Side Column, Top
	 *  - 'side_bottom' : Side Column, Bottom
	 *  - 'main_top' : Main Column, Top
	 *  - 'main_bottom' : Main Column, Top
	 *  - 'toolbar' : Top toolbar
	 * 
	 *
	 * @param	Array	The current edited object.
	 *
	 */
	function _addons($object)
	{
		$CI =& get_instance();
		$CI->load->model('magento_model', '', true);

		if (isset($object['id_page']))
		{
			// Path to the templates to use
			$template_path = FCPATH.config_item('magento_templates_path');

			$data = array();
///			$template_files = glob(FCPATH.'../app/design/frontend/nose/default/template/ionize/page/*.phtml', GLOB_BRACE);
			$template_files = glob($template_path.'*.phtml', GLOB_BRACE);
	
			foreach($template_files as $file)
				$data['templates'][] = array_pop(explode('/', $file));
	
			// Current Magent page template linked to this page
			$data['template'] = $CI->magento_model->get(array('id_page' => $object['id_page']));
			
			// Send Page ID to view
			$data['page'] = $object;
			
			// Side Top Page Addon
			$CI->load_addon_view('magento', 'page', 'side_top', 'admin/addon_page_side_top', $data);
		}
		
		if (isset($object['id_article']))
		{
			// Send Page ID to view
			$data['article'] = $object;
			
			// Side Top Page Addon
			$CI->load_addon_view('magento', 'article', 'side_top', 'admin/addon_article_side_top', $data);
		}
	}	


}

/* End of file magento.php */
/* Location: /modules/Magento/controllers/admin/magento.php */
