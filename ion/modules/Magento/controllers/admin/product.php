<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Magento Products admin controller
*
* @author	Partikule Studio
*
*/
class Product extends Module_Admin 
{
	/**
	* Constructor
	*
	* @access	public
	* @return	void
	*/
	function construct()
	{
		$this->load->library('magento');
		
		$this->load->model('magento_product_model', '', true);
	}
	
	
	/**
	* Admin panel
	* Called from the modules list.
	*
	* @access	public
	* @return	parsed view
	*/
	function index()
	{
		$this->output('admin/products');
	}
	
	
	/**
	 * Links one product to one parent
	 * 
	 */
	public function link_to_parent()
	{
		$parent = $this->input->post('parent');
		$id_parent = $this->input->post('parent_id');
		$product_id = $this->input->post('product_id');
		
		if ($this->magento_product_model->link_product_to_parent($parent, $id_parent, $product_id))
		{			
			// Update the panels
			$this->callback = array(
				array(
					'fn' => 'MAGE.getProductList',
					'args' => array(
						$parent,
						$id_parent
					)
				),
				array(
					'fn' => 'ION.notification',
					'args' => array(
						'success', 
						lang('module_magento_message_product_linked')
					)
				)
			);
			
			$this->response();
		}
		else
		{
			$this->error(lang('module_magento_message_product_already_linked'));
		}
	}
	
	
	/**
	 * Unlinks one product to one parent
	 * 
	 */
	public function unlink_from_parent()
	{
		$parent = $this->input->post('parent');
		$id_parent = $this->input->post('parent_id');
		$product_id = $this->input->post('product_id');
		
		if ($this->magento_product_model->unlink_product_from_parent($parent, $id_parent, $product_id))
		{			
			// Update the panels
			$this->callback = array(
				array(
					'fn' => 'MAGE.getProductList',
					'args' => array(
						$parent,
						$id_parent
					)
				),
				array(
					'fn' => 'ION.notification',
					'args' => array(
						'success', 
						lang('module_magento_message_product_unlinked')
					)
				)
			);
			
			$this->response();
		}
		else
		{
			$this->error(lang('module_magento_message_product_not_linked'));
		}
	}
	
	
	/**
	 * Returns the linked products to one parent (article, page)
	 * Called through XHR
	 *
	 * @returns		Sting		HTML list of linked products.
	 *
	 */
	public function get_linked_list()
	{
		if ($this->magento->mage())
		{
			$parent = $this->input->post('parent');
			$id_parent = $this->input->post('id_parent');
			
			$products = $this->magento_product_model->get_linked_products($parent, $id_parent);
	
			$this->template['products'] = $products;
			$this->template['id_parent'] = $id_parent;
			$this->template['parent'] = $parent;
			
			$this->output('admin/product_linked_list');
		}
		else
		{
			$this->output('admin/error');	
		}
	}
	
	
	/**
	 *
	 *
	 */
	public function save_ordering($parent, $id_parent)
	{
		$order = $this->input->post('order');

		if( $order !== FALSE )
		{
			// Saves the new ordering
			$this->magento_product_model->save_ordering($order, $parent, $id_parent);
			
			// Answer
			$this->success(lang('ionize_message_element_ordered'));
		}
		else 
		{
			// Answer send
			$this->error(lang('ionize_message_operation_nok'));
		}
	
	}
	
}
