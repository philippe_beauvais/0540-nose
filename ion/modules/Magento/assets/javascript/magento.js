/**
 *  Ionize Magento Module
 * 
 *  @author	Partikule Studio
 *  @since	0.9.8
 * 
 */
var MAGE = (MAGE || {});

MAGE.append = function(hash){
	Object.append(MAGE, hash);
}.bind(MAGE);

MAGE.append({
	baseUrl: base_url,
	adminUrl: admin_url,
	moduleUrl: admin_url + 'module/magento/',
	
	getProductList: function(parent, id_parent)
	{
		// tabSwapper elements
		var tabSwapper = $('desktop').retrieve('tabSwapper');
		var tabs = 		tabSwapper.tabs;
		
		// DOM elements
		var tabsContainer = $(tabSwapper.options.tabsContainer);
		var sectionsContainer = $(tabSwapper.options.sectionsContainer);
		var ul = tabsContainer.getElement('ul');

		var r = new Request.HTML(
		{
			url: MAGE.moduleUrl + 'product/get_linked_list/', 
			method: 'post',
			loadMethod: 'xhr',
			data:
			{
				'parent': parent,
				'id_parent': id_parent
			},
			onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript)
			{
				var index = 0;
				
				if (responseHTML != '')
				{
					var title = 'Products';
					var id = 'Products';
					
					index = tabs.length;
					
					var tabContent = $('tabcontent' + id);
					
					// No tab found ? Build it !
					if (typeOf(tabContent) == 'null')
					{
						// Tab
						var a = new Element('a').set('html', title);
						
						var li = new Element('li', {'id':'tab' + id, 'class': 'tab' + id}).adopt(a);
						li.inject(ul, 'bottom');
						
						// Section
						var tabContent = new Element('div', {'id':'tabcontent' + id, 'class': 'tabcontent tabcontent' + id}).inject(sectionsContainer, 'bottom');
						
						tabSwapper.addTab(li, tabContent, a, index);
					}

					// Set the content
					tabContent.set('html', responseHTML);
					
					// Remind the opened tab
					if (tabSwapper.options.cookieName && tabSwapper.recall()) tabSwapper.show(tabSwapper.recall().toInt());
					
					// Exec the JS
					Browser.exec(responseJavaScript);
					
					// Update the number of items
					ION.updateTabNumber('tab' + id, 'tabcontent' + id);
				}
			}
		}).send();
	},
	
	
	dropCategoryOnParent: function(element, droppable, event)
	{
		var entity_id = element.getProperty('rel');
		var parentData = (droppable.getProperty('rel')).split('.');
		var parent = parentData[0];
		var id_parent = parentData[1];
		
		ION.JSON(admin_url + 'module/magento/category/add_link', 
		{
			'parent': parent, 
			'id_parent': id_parent, 
			'entity_id': entity_id,
			'update':'magentoCategoryLinkContainer'			
		});
	},
	
	dropParentOnCategory: function(element, droppable, event)
	{
		var entity_id = droppable.getProperty('data-id');
		var id_parent = element.getProperty('data-id');
		var parent = element.getProperty('data-type');
		
		ION.JSON(admin_url + 'module/magento/category/add_link', 
		{
			'parent': parent, 
			'id_parent': id_parent, 
			'entity_id': entity_id,
			'update':'magentoCategoryTableContainer'
		});
	}
	
	
});


/**
 * MAGE Product Tree
 * Displays the Magento categories tree
 *
 */
MAGE.ProductTree = new Class({

	Implements: [Events, Options],
	
	options: MAGE.options,
	
	initialize: function(container, options)
	{
		// Timer for double click handler
		this.click_timer = null;

		this.setOptions(options);
		this.container = $(container);
		this.container.empty();
		
//		this.controller = admin_url + 'module/magento/' + this.options.controller, 

		
		// Array of itemManagers
		this.itemManagers = {'category': new Array(), 'product': new Array()};
		
		this.elementIcon_Model = new Element('div', {'class': 'shoptree-img drag'});
		this.plusMinus_Model = new Element('div', {'class': 'shoptree-img plus'});
		this.lineNode_Model = new Element('div', {'class': 'shoptree-img line node'});
		this.treeLine_Model = new Element('div', {'class': 'shoptree-img'});

		this.action_Model = new Element('span', {'class': 'action', 'styles': { 'display':'none' }});
		this.icon_Model = new Element('span', {'class': 'icon'});
		this.span_Model = new Element('span');
		this.title_Model = new Element('a', {'class': 'title'});

		this.opened = new Array();
		if (Cookie.read('mageProductTree')) this.opened = (Cookie.read('mageProductTree')).split(',');
		
		// Root Category in Magento : 1 
		this.get(1);
	},

	
	/**
	 * Gets one tree
	 *
	 */
	get: function(parent_id)
	{
		var self = this;
		
		// Get childs pages and articles
		new Request.JSON({
			url: admin_url + 'module/magento/category/get_category_tree', 
			method: 'post',
			loadMethod: 'xhr',
			data: {
				'parent_id': parent_id
			},
			onFailure: function(xhr) 
			{
				ION.notification('error', xhr.responseJSON);
			},
			onSuccess: function(responseJSON, responseText)
			{
				var categories = responseJSON.categories;
				var products = responseJSON.products;

				var categoryContainer = self.injectContainer('category', parent_id, true);
				var productContainer = self.injectContainer('product', parent_id, true);
				
				// Build tree
				categories.each(function(category) {
					self.insertElement(category, 'category');
				});
				
				products.each(function(product) {
					self.insertElement(product, 'product');
				});
				
				var parentContainer = self.getParentContainer(parent_id);

				self.itemManagers['category'][categoryContainer.id] = new ION.ItemManager({'container': categoryContainer.id, 'controller':'module/magento/category', 'sortable':true });
				self.itemManagers['product'][productContainer.id] = new ION.ItemManager({ 'container': productContainer.id, 'controller':'module/magento/product', 'sortable':true});

				// Stores that the content is loaded
				parentContainer.store('loaded', true);
		
				// Opens the folder
				if (parent_id != 1)
					self.updateOpenClose(parentContainer);

				// Fires the event
				self.fireEvent('get', self);
			}
		}).send();
	},


	/**
	 * Inject or return an existing container for elements (categories / product)
	 *
	 */
	injectContainer: function(type, parent_id, erase)
	{
		var parentContainer = this.getParentContainer(parent_id);

		var container = parentContainer.getFirst('ul.' + type + 'Container');

		if (typeOf(container) == 'null')
		{
			container = new Element('ul', {'rel': parent_id});				
			
			// Root class
			if (parent_id == '1')
			{
				container.addClass('tree');
				container.id = type + 'ContainerTree' + this.id_menu;
			}
			else
			{
				container.id = type + 'Container' + parent_id;
			}
			container.addClass(type + 'Container').inject(parentContainer, 'bottom');
			
			// Hide the parentContainer if it should be, but not for the root.
			if (parent_id != 1)
				if ( ! (parentContainer.hasClass('f-open'))) { container.setStyle('display', 'none');}
		}
		else
		{
			if (erase == true)
			{
				container.empty();
				if (typeOf(this.itemManagers[type][container.id]) != 'null')
					delete this.itemManagers[type][container.id];
			}

		}

		return container;
	},
	
	
	/**
	 * Inserts one element (page / article) in a container (UL)
	 *
	 */
	insertElement: function(element, type)
	{
		// Inject or get the container
		var self = this;
		var id = element.entity_id;
		var parent_id = element.parent_id;
		
		var title = (element.name !='') ? element.name : element.entity_id;
		
		var container = this.injectContainer(type, parent_id)

		var li = new Element('li').setProperty('id', type + '_' + id).addClass('online').addClass(type + id).setProperty('rel', id);
		li.store('loaded', false);
		li.store('entity_id', id);
		
		// Title element 
		var link = this.span_Model.clone().addClass('title');
		var a = this.title_Model.clone().addClass(type + id).addClass('title').setProperty('rel', id);
		
		
		// Icon
		var icon = this.elementIcon_Model.clone();
		icon.inject(li, 'top');
		
		// Category
		if (type == 'category')
		{
			// Title
			a.setProperty('title', title).set('text', String.htmlspecialchars_decode(title + ' (' + element.product_count + ')'));
			
/*
 * HERE 
 * HERE
 *
 */

			// Link to page / article icon
//			var linkCat = new Element('a', {'class': 'icon plus left'});
			// this.icon_Model.clone().addClass('plus').addClass('left').setProperty('rel', id);
			
//			li.adopt(linkCat);
			
			// Drag'n'Drop Category on Parent			
			ION.addDragDrop(a, '.dropMagentoCategory', 'MAGE.dropCategoryOnParent');

			
			li.addClass('folder').addClass('category');
			
			// Folder icon
			icon.addClass('folder');

			// plus / minus icon
			var pm = this.plusMinus_Model.clone().addEvent('click', this.openclose.bind(this)).inject(li, 'top');
		}
		// Product
		else
		{
//			a.setProperty('title', title).set('text', String.htmlspecialchars_decode(title));

			li.addClass('file').addClass(type + id);
			
			// File icon
			icon.addClass('sticky');
			
			// Simple ?
			if (element.type_id == 'simple')
			{
				icon.addClass('simple');
				a.setProperty('title', element.sku).set('text', String.htmlspecialchars_decode(element.sku));
			}
			else
			{
				a.setProperty('title', title).set('text', String.htmlspecialchars_decode(title));
			}
			
			// Item node line
			this.treeLine_Model.clone().inject(li, 'top').addClass('line').addClass('node');
		}

		// Injection
		link.adopt(a);
		li.adopt(link);
		li.inject(container, 'bottom');

		// Edit
		this.addClickEvents(li, type);
		
		// Get the parent : Build tree lines (nodes)
		li.getParents('li').each(function(parent){
			self.treeLine_Model.clone().inject(li, 'top');
		});

		// Makes the folder sortable (on folder icon)
		if (typeOf(container.retrieve('sortables')) != 'null')
			(container.retrieve('sortables')).addItems(li);

		// The element was dynamically inserted through XHR
		if (typeOf(element.inserted) != 'null')
		{
			if (typeOf(this.itemManagers[type][container.id]) != 'null')
				(this.itemManagers[type][container.id]).init();
		}

		// Mouse over effect : Show / Hide actions
		this.addMouseOver(li);
		
		// Open the folder if cookie says...
		if (type == 'category' && this.opened.contains(id))
		{
			this.get(id);
		}
	},
	
	
	/**
	 * Plus / Minus folder icon click event	
	 *
	 */
	openclose:function(e)
	{
		if (typeOf(e.stop) == 'function') e.stop();
		el = e.target;

		// Folder (LI)
		var li = el.getParent('li');
		
		// Update content : XHR
		if (li.retrieve('loaded') == false)
			this.get(li.retrieve('entity_id'));
		else
			this.updateOpenClose(li);
	},
	
	
	/**
	 * Displays / Hides content, updates Plus / Minus icons
	 *
	 *
	 */
	updateOpenClose: function(folder)
	{
		// All childrens UL
		var folderContents = folder.getChildren('ul');
		var folderIcon = folder.getChildren('div.folder');
		
		// Is the folder Open ? Yes ? Close it (Hide the content)
		if (folder.hasClass('f-open'))
		{
			var pmIcon = folder.getFirst('div.shoptree-img.minus');
			pmIcon.addClass('plus').removeClass('minus');
			
			folderIcon.removeClass('open');
			folderContents.each(function(ul){ ul.setStyle('display', 'none');});
			folder.removeClass('f-open');
			
			ION.listDelFromCookie('mageProductTree', folder.retrieve('entity_id'));
		}
		else
		{
			var pmIcon = folder.getFirst('div.shoptree-img.plus');
	
			pmIcon.addClass('minus').removeClass('plus');
			
			folderIcon.addClass('open');
			folderContents.each(function(ul){ ul.setStyle('display', 'block'); });
			folder.addClass('f-open');
			
			ION.listAddToCookie('mageProductTree', folder.retrieve('entity_id'));
			
			$('btnStructureExpand').store('status', 'expand');
			$('btnStructureExpand').value = Lang.get('ionize_label_collapse_all');
		}
	},
	
	
	/**
	 * Adds a link to a tree LI DOM element
	 *
	 * @param	DOMElement		tree LI
	 * @param	String			logical tree element type. "page" or "article"
	 *
	 */
	addClickEvents: function(el, type)
	{
		var a = el.getElement('a.title');
		var self = this;
		
		a.addEvents(
		{
			'click': function(e)
			{
				clearTimeout(self.click_timer);
				self.click_timer = self.relaySingleOrDoubleClick.delay(700, self, [e, self, a, type, 1]);		
			},
			'dblclick': function(e)
			{
				clearTimeout(self.click_timer);
				self.click_timer = self.relaySingleOrDoubleClick.delay(0, self, [e, self, a, type, 2]);		
			}
		});		
	},

	relaySingleOrDoubleClick: function(e, self, el, type, clicks)
	{
		// IE7 / IE8 event problem
		if( ! Browser.ie)
			if (e) e.stop();
		
		// Open page
		if (clicks === 2 && type == 'category')
		{
			self.openclose(e);
		}
		// Link one product to one Page or Article
		else if (clicks === 2 && type == 'product')
		{
			var parent = $('element').value;
			
			var data = {
				'parent' : 		parent,
				'parent_id' :	$('id_' + parent).value,
				'product_id' :	el.getParent('li').retrieve('entity_id')
			}
			
			ION.JSON(MAGE.moduleUrl + 'product/link_to_parent', data);
		}	
	},
	
	
	

	getParentContainer: function(parent_id)
	{
		// Parent DOM Element (usually a folder LI)
		var parentEl = 'category_' + parent_id;

		if (typeOf($(parentEl)) == 'null')
			parentEl = this.container;
		else
			parentEl = $(parentEl);
			
		return parentEl;
	},


	addMouseOver: function(node)
	{
		node.addEvent('mouseover', function(ev){
			ev.stopPropagation();
			ev.stop();
			this.addClass('highlight');
//			this.getParent().getParent().getChildren('.action').setStyle('display', 'none');
//			this.getChildren('.action').setStyle('display', 'block');
		});
		node.addEvent('mouseout', function(ev){
			this.removeClass('highlight');
		});
		node.addEvent('mouseleave', function(e)
		{
//			this.getChildren('.action').setStyle('display', 'none');
		});
	}
});






