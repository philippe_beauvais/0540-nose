

<table class="list" id="magentoCategoriesTable">

	<thead>
		<tr>
			<th><?= lang('module_magento_table_head_category') ?></th>
			<th axis="string"><?= lang('module_magento_table_head_pages') ?></th>
		</tr>
	</thead>

	<tbody>
	
	<?php foreach ($categories as $category) :?>
		
		<?php
			$title = $category->getName();
			$id = $category->getId();
			$level = $category->getLevel();
			
			$padding_left = 'pl' . 10 * $level;
		?>

		<tr class="dropPageToMagentoCategory droppable" data-id="<?= $id ?>">
			<td style="overflow:hidden;" class="title">
				<span class="<?= $padding_left ?>"><strong><?php echo($title) ;?></strong></span>
			</td>
			
			<td>
				<ul class="list mb0 ">
				<?php foreach($category->getPages() as $page) :?>
					<li>
						<?php if ( $page['master'] == '1' ) :?>
							<a class="icon left arrow-right mr10" title="<?= lang('module_magento_help_master_category_page') ?>"></a>
						<?php endif ;?>

						<a class="title left">
							<?php foreach($page['breadcrumb'] as $idx => $breadcrumb) :?>
								<?php if ( $idx > 0 ) :?>
								> 
								<?php endif ;?>
								<?= $breadcrumb['title'] ?>
							<?php endforeach ;?>
						</a>
						<a class="icon unlink right" data-id="<?= $id ?>" data-id-page="<?= $page['id_page'] ?>"></a>
						<?php if ( $page['master'] != '1' ) :?>
							<a class="setmaster right mr10" data-id="<?= $id ?>" data-id-page="<?= $page['id_page'] ?>"><?= lang('module_magento_label_set_master') ?></a>					
						<?php endif ;?>
					</li>
				<?php endforeach ;?>
				</ul>
			</td>
			
		</tr>

	<?php endforeach ;?>
	
	</tbody>

</table>

<script type="text/javascript">

	// Set master action
	$$('#magentoCategoriesTable ul li a.setmaster').each(function(item)
	{
		ION.initRequestEvent(item, 
			'<?= admin_url() ?>/module/magento/category/set_master_link', 
			{
				'parent':'page',
				'id_parent':item.getProperty('data-id-page'), 
				'entity_id':item.getProperty('data-id'), 
				'update':'magentoCategoryTableContainer'
			}
		);
	});

	// Unlink action
	$$('#magentoCategoriesTable ul li a.unlink').each(function(item)
	{
		ION.initRequestEvent(item, 
			'<?= admin_url() ?>/module/magento/category/remove_link', 
			{
				'parent':'page',
				'id_parent':item.getProperty('data-id-page'), 
				'entity_id':item.getProperty('data-id'), 
				'update':'magentoCategoryTableContainer'
			}
		);
	});

/*
	ION.initDroppable();
*/
	
	
	/**
	 * Make each page draggable
	 *
	 */
	$$('.treeContainer .page a.title').each(function(item, idx)
	{
		ION.addDragDrop(item, '.dropPageToMagentoCategory', 'MAGE.dropParentOnCategory');
	});	

	$$('.treeContainer').each(function(tree, idx)
	{
		tree.retrieve('tree').addEvent('get', function()
		{
			$$('.treeContainer .page a.title').each(function(item, idx)
			{
				ION.addDragDrop(item, '.dropPageToMagentoCategory', 'MAGE.dropParentOnCategory');
			});	
		});
	});	

	// Load the Module JS / CSS
	var magentoScript = Asset.javascript('<?= base_url() ?>modules/Magento/assets/javascript/magento.js', {
		id: 'magentoScript',
		onLoad: function()
		{
			Asset.css('<?= base_url() ?>modules/Magento/assets/css/magento.css');

		}
	});

</script>
