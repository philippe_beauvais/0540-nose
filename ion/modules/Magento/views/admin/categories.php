
<h2>Magento Categories</h2>

<div class="element">
	<div class="mageCategoryTreeContainer" id="mageCategoryTree"></div>	
</div>


<script type="text/javascript">

	
	/** 
	 * Assets & JS
	 *
	 */
	var shopScript = Asset.javascript('<?= base_url() ?>modules/Magento/assets/javascript/magento.js', {
    	id: 'magentoScript',
    	onLoad: function()
    	{
			Asset.css('<?= base_url() ?>modules/Magento/assets/css/magento.css');
			
			// Categories / Products Tree
			var mageCategoryTree = new MAGE.CategoryTree('mageCategoryTree');
    	}
	});


</script>