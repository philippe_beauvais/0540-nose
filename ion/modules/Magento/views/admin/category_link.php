
<?php if( ! empty($category)) :?>

	<dl class="small">
		<dt><label for="link"><?= lang('module_magento_label_category') ?></label></dt>
		<dd>
			<ul class="sortable-container" id="linkList">
			
				<li class="sortme">
		
					<!-- Unlink icon -->
					<a class="icon unlink right"></a>
					
					<!-- Master link ? -->
					<?php if($category['master'] == '1') :?>
						<a class="icon flag green left mr5"></a>
					<?php endif ;?>
		
					<!-- Title -->
					<a id="moduleMagentoCategoryLinkTitle" style="overflow:hidden;height:16px;display:block;" class="pl5 pr10" title="<?= $category['name'] ?>"><?= $category['name'] ?></a>
		
				</li>
		
			</ul>
		</dd>
	</dl>

<!--
	<dl class="small mt10">
		<dt>
			<label title="<?= lang('module_magento_label_drop_category_here') ?>"><?= lang('module_magento_label_category') ?></label>
			<br/>
		</dt>
		<dd>
		</dd>
	</dl>
-->
	<script type="text/javascript">

		var val = '<?= $category['master'] ?>';
		var btnToggleMaster = $('moduleMagentoCategoryLinkTitle');

		ION.initRequestEvent(btnToggleMaster, 
			'<?= admin_url() ?>/module/magento/category/set_master_link', 
			{
				'parent':'<?= $parent ?>',
				'id_parent':'<?= $id_parent ?>', 
				'entity_id':'<?= $category['entity_id']?>', 
				'master':val, 
				'update':'magentoCategoryLinkContainer'
			}
		);

		$$('#linkList li .unlink').each(function(item)
		{
			ION.initRequestEvent(item, 
				'<?= admin_url() ?>/module/magento/category/remove_link', 
				{
					'parent':'<?= $parent ?>',
					'id_parent':'<?= $id_parent ?>', 
					'entity_id':'<?= $category['entity_id']?>',
					'update':'magentoCategoryLinkContainer'
				}
			);
		});
		
	</script>
	
<?php else :?>

	<dl class="small">
		<dt><label for="link"><?= lang('module_magento_label_category') ?></label></dt>
		<dd>
			<textarea id="link" class="dropMagentoCategory inputtext h30 droppable empty nofocus" rel="<?= $parent ?>.<?= $id_parent ?>" alt="<?= lang('module_magento_label_drop_category_here') ?>"></textarea>
		</dd>
	
	</dl>
<!--
	<dl class="small dropMagentoCategory mt10" rel="<?= $parent ?>.<?= $id_parent ?>">
		<dt>
			<label for="link"><?= lang('module_magento_label_category') ?></label>
			<br/>
		</dt>
		<dd>
			<textarea id="link" class="inputtext w140 h30 droppable empty nofocus" alt="<?= lang('module_magento_label_drop_category_here') ?>"></textarea>
		</dd>
	</dl>
-->
	<script type="text/javascript">
		
		
		ION.initDroppable();

	</script>


<?php endif ;?>



