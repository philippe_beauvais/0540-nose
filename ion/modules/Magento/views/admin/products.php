
<h2>Magento Products List</h2>

<p>Drag / Drop one category to link it to this page.</p>
<p>Double-click on one product to add it in the product list of this page.</p>

<div class="element">
	<div class="mageProductTreeContainer" id="mageProductTree"></div>	
</div>


<script type="text/javascript">

	
	/** 
	 * Assets & JS
	 *
	 */
	var shopScript = Asset.javascript('<?= base_url() ?>modules/Magento/assets/javascript/magento.js', {
    	id: 'magentoScript',
    	onLoad: function()
    	{
			Asset.css('<?= base_url() ?>modules/Magento/assets/css/magento.css');
			
			// Categories / Products Tree
			var mageProductTree = new MAGE.ProductTree(
				'mageProductTree'
			);
    	}
	});


</script>