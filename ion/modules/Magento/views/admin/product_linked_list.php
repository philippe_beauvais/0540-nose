
<ul id="moduleMagentoLinkedProducts" class="sortable-container">

<?php foreach($products as $product) :?>

	<?php
	
	$id = $product['entity_id'];
	
	$name = ($product['type_id'] == 'simple') ? $product['sku'] : $product['name'];
	
	/*
	 * Identify the first field of each element
	 * $i = 0 : first element, has the link to the edit window
	 * $i = 1 : all childs will be wrapped into a toggler content div
	 *
	 */ 
	$i = 0;
	
	?>

	<li id="product<?= $id ?>" class="sortme nohover" rel="<?= $id ?>">
		<a class="icon unlink right" rel="<?= $id_parent . '.' . $id ?>"></a>
		<img class="icon left drag pr10" src="<?= theme_url() ?>images/icon_16_ordering.png" />
		<span class="left lite pr10"><?= $id ?></span>
		<a class="icon mr10 left shoptree-img sticky <?= $product['type_id'] ?>"></a>
		<a class="edit name left" rel="<?= $id ?>"><?= $name ?></a>
	</li>

<?php endforeach ;?>

</ul>


<script type="text/javascript">


	$$('#moduleMagentoLinkedProducts li .unlink').each(function(item)
	{
		var rel = item.getProperty('rel').split('.');
		
		ION.initRequestEvent(item, admin_url + 'module/Magento/product/unlink_from_parent', {'parent':'<?= $parent ?>', 'parent_id': rel[0], 'product_id': rel[1]} );
	});


	/**
	 * itemManager
	 *
	 */
	var moduleMagentoProductList = new ION.ItemManager({
		'container': 'moduleMagentoLinkedProducts', 
		'controller': 'module/Magento/product', 
		'parent_element': '<?= $parent ?>', 
		'id_parent':'<?= $id_parent ?>',
		'sortable': true
	});

	

</script>