
<!-- Options -->
<h3 class="toggler"><?= lang('module_magento_title_magento') ?></h3>

<div class="element">

	<dl class="small">
		<dt><label for="selectModuleMagentoTemplate"><?= lang('module_magento_label_template') ?></label></dt>
		<dd>
			<select name="selectModuleMagentoTemplate" id="selectModuleMagentoTemplate" class="select">
				<option value="">-- --</option>
			
				<?php foreach($templates as $file_name) :?>
				<?php
					$selected = '';
					if (isset($template['magento_template']) && $template['magento_template'] == $file_name)
						$selected = ' selected="selected"';
				?>
				
				<option<?=$selected?> value="<?= $file_name ?>"><?= $file_name ?></option>
				<?php endforeach ;?>
						
			</select>
		</dd>
	</dl>
					
<?php /*
	<label><?= lang('module_magento_label_template') ?></label><br/>
	<select id="selectModuleMagentoTemplate" name="magento_template" class="select">
	
		<option value="">-- --</option>
	
		<?php foreach($templates as $file_name) :?>
		
			<?php
				$selected = '';
				if (isset($template['magento_template']) && $template['magento_template'] == $file_name)
					$selected = ' selected="selected"';
			?>
		
			<option<?=$selected?> value="<?= $file_name ?>"><?= $file_name ?></option>
		
		<?php endforeach ;?>
	
	</select>
*/
?>
<!--
	<dl class="small compact mt5">
	
		<dt></dt>
		<dd>
		</dd>		
	</dl>
-->	
	<!-- Category ? -->
	<?php if ($page['id_page'] != '') :?>
	
		<div id="magentoCategoryLinkContainer"></div>
		
	<?php endif ;?>


	<dl class="small">
		<dt></dt>
		<dd>
			<button id="btnModuleMagentoLinkProduct" type="button" class="light-button plus mb10"><?= lang('module_magento_button_link_product_category') ?></button>
		</dd>
	</dl>

	
</div>

<script type="text/javascript">

	// Magento template select
	$('selectModuleMagentoTemplate').addEvents(
	{
		'change': function(e)
		{
			ION.JSON(
				admin_url + 'module/magento/magento/save_page_template', 
				{
					'id_page': $('id_page').value,
					'magento_template': this.value
				}
			);
		}
	});
	
	// Link Magento products
	$('btnModuleMagentoLinkProduct').addEvent('click', function()
	{
		ION.dataWindow('magentoProductsTree', 'module_magento_title_link', ION.adminUrl + 'module/magento/product/', {'width':400, 'height':450});
	
		return false;
	});
	

	// Get existing products list
	var magentoScript = Asset.javascript('<?= base_url() ?>modules/Magento/assets/javascript/magento.js', {
		id: 'magentoScript',
		onLoad: function()
		{
			Asset.css('<?= base_url() ?>modules/Magento/assets/css/magento.css');
			MAGE.getProductList('page', '<?= $page['id_page'] ?>');
		}
	});


	// Link to Category
	if ($('magentoCategoryLinkContainer'))
	{
		ION.HTML(admin_url + 'module/magento/category/get_link', {'parent':'page', 'id_parent': '<?= $page['id_page'] ?>'}, {'update': 'magentoCategoryLinkContainer'});
	}


</script>