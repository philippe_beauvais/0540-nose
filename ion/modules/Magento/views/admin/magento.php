<div id="maincolumn">

	<h2 class="main" style="background:url(<?php echo base_url(); ?>modules/Magento/assets/images/icon_48_module.png) no-repeat top left;"><?php echo config_item('module_magento_name'); ?></h2>

	<div class="subtitle">

		<!-- About this module -->
		<p class="lite">
			<?php echo lang('module_magento_about'); ?>
		</p>

	</div>


	<!-- Tabs -->
	<div id="moduleMagentoTab" class="mainTabs">
		
		<ul class="tab-menu">

			<li><a><?= lang('module_magento_tab_categories') ?></a></li>
			<li class="right"><a><?= lang('module_magento_tab_settings') ?></a></li>

		</ul>
		<div class="clear"></div>
	
	</div>

	<div id="moduleMagentoTabContent">
		<div class="tabcontent" id="moduleMagentoTabContentCategories">
		</div>
		<div class="tabcontent">
		</div>
	</div>
	
</div>

<script type="text/javascript">

	new TabSwapper({tabsContainer: 'moduleMagentoTab', sectionsContainer: 'moduleMagentoTabContent', selectedClass: 'selected', deselectedClass: '', tabs: 'li', clickers: 'li a', sections: 'div.tabcontent', cookieName: 'moduleMagentoTab' });


	ION.HTML('module/magento/category/get_categories', {}, {'update':'moduleMagentoTabContentCategories'});


</script>
