
<!-- Options -->
<h3 class="toggler toggler-options"><?= lang('module_magento_title_magento') ?></h3>

<div class="element element-options">


	<!-- Category -->
	<?php if ($article['id_article'] != '') :?>
	
		<div id="magentoCategoryLinkContainer"></div>
		
	<?php endif ;?>
	
	<!-- Product list ? -->
	<dl class="small">
		<dt></dt>
		<dd>
			<button id="btnModuleMagentoLinkProduct" type="button" class="light-button plus mb10"><?= lang('module_magento_button_link_product_category') ?></button>
		</dd>
	</dl>

</div>

<script type="text/javascript">

	
	// Link Magento products
	$('btnModuleMagentoLinkProduct').addEvent('click', function()
	{
		ION.dataWindow('magentoProductsTree', 'module_magento_title_link', ION.adminUrl + 'module/magento/product/', {'width':400, 'height':450});
	
		return false;
	});
	

	// Get existing products list
	var magentoScript = Asset.javascript('<?= base_url() ?>modules/Magento/assets/javascript/magento.js', {
		id: 'magentoScript',
		onLoad: function()
		{
			Asset.css('<?= base_url() ?>modules/Magento/assets/css/magento.css');
			MAGE.getProductList('article', '<?= $article['id_article'] ?>');
		}
	});


	// Link to Category
	if ($('magentoCategoryLinkContainer'))
	{
		ION.HTML(admin_url + 'module/magento/category/get_link', {'parent':'article', 'id_parent': '<?= $article['id_article'] ?>'}, {'update': 'magentoCategoryLinkContainer'});
	}


</script>