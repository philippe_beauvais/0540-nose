<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Magento Module Main Model
 *
 */

class Magento_model extends Base_model 
{
	// Magento Store ID
	protected $_storeId = 0;

	
	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();

		$this->set_table('module_magento_template');
	}

	
	/**
	 * Saves Magento template link for one page
	 *
	 */
	public function save_template($data)
	{
		$this->db->where(array('id_page' => $data['id_page']))->delete($this->table);
		
		if ($data['magento_template'] != '')
			return parent::save($data);

		return;
	}
	
	
	

	

	

}