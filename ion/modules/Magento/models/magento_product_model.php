<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Magento Module Product Model
 *
 */

class Magento_product_model extends Base_model 
{
	// Magento Store ID
	protected $_storeId = 0;
	
	// Link table between Ionize parent (article, page) and Magento products
	protected $_products_table = 'module_magento_products';


	/**
	 * Links one product to one page / article
	 *
	 *
	 */
	public function link_product_to_parent($parent, $id_parent, $entity_id)
	{
		// Get the ordering value, regarding to the type
		$this->db->select_max($this->_products_table.'.ordering');
		$this->db->join($parent, $parent.'.id_'.$parent.' = '.$this->_products_table.'.id_parent');
		$this->db->where($parent.'.id_'.$parent, $id_parent);
		$this->db->where($this->_products_table.'.parent', $parent);

		$query = $this->db->get($this->_products_table);

		if ($query->num_rows() > 0)
		{	
			$row =		$query->row();
			$ordering =	$row->ordering;
		}
		else 
		{
			$ordering = 0;
		}
		
		$this->db->set('ordering', $ordering += 1);

		$data = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'entity_id' => $entity_id		
		);
		$this->db->where($data);
		
		$query = $this->db->get($this->_products_table);

		if ($query->num_rows() == 0)
		{
			$this->db->insert($this->_products_table, $data);
			return TRUE;
		}
		return FALSE;
	}
	
	
	/**
	 * Unlinks one product to one page / article
	 *
	 */
	public function unlink_product_from_parent($parent, $id_parent, $entity_id)
	{
		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'entity_id' => $entity_id		
		);
		
		return $this->db->delete($this->_products_table, $where);
	}
	
	
	/**
	 * Returns the Magento linked products
	 *
	 * @param	String		Parent type. 'page', 'article'
	 * @param	int			ID parent
	 *
	 * @return	Array		Associative array of products
	 *
	 */
	public function get_linked_products($parent, $id_parent)
	{
		$product_ids = $this->_get_linked_products_ids($parent, $id_parent);
		
		$magento_products = $this->_get_magento_products_from_ids($product_ids);
		
		// Check DB integrity
		$this->_delete_non_existing_products($parent, $id_parent, $product_ids, $magento_products);
		
		return $magento_products;
	}
	
	
	public function save_ordering($ordering, $parent, $id_parent)
	{
		if ( ! is_array($ordering))
		{
			$ordering = explode(',', $ordering);
		}
		$new_order = '';
		$i = 1;
		
		while (list ($rank, $id) = each ($ordering))	
		{
			$this->db->where('entity_id', $id);
			$this->db->where('parent', $parent);
			$this->db->where('id_parent', $id_parent);
			
			$this->db->set('ordering', $i++);
			
			$this->db->update($this->_products_table);
					
			$new_order .= $id.",";
		}
		
		return substr($new_order, 0, -1);
	}
	
	
	
	/**
	 * Returns the linked products IDs
	 *
	 */
	protected function _get_linked_products_ids($parent, $id_parent)
	{
		$result = array();

		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent
		);

		$this->db->where($where);
		$this->db->order_by('ordering', 'ASC');
		
		$this->db->select('entity_id');
		
		$query = $this->db->get($this->_products_table);
		
		if ($query->num_rows() > 0)
		{
			foreach($query->result_array() as $product)
			{
				$result[] = $product['entity_id'];
			}
			
			return $result;
		}
		
		return $result;
	}
	
		
	/**
	 * Returns one Magento Products collection based on the given products IDs
	 *
	 * @param	array	Array of product IDs
	 *
	 */
	protected function _get_magento_products_from_ids($product_ids)
	{
		$this->load->library('magento');
		
		// Magento Product Model
		$productModel = Mage::getModel('catalog/product'); 

		// Returned Products data array
		$products_data = array();

		$productCollection = Mage::getResourceModel('catalog/product_collection')
								->addAttributeToFilter('entity_id', array('in' => $product_ids))
								->load();

/*
		$productCollection = Mage::getModel('catalog/product')->getCollection()
								->addAttributeToFilter('entity_id', array('in' => $product_ids))
								->load();
*/		

		// Loop into $product_ids to keep the Ionize's ordering
		foreach ($product_ids as $product_id)
		{
			foreach ($productCollection as $product)
			{
				if ($product_id == $product->getId())
				{
					$_product = $productModel->load($product->getId());
					$products_data[] = $_product->getData();
				}
			}
		}
		
		return $products_data;
	}
	
	
	protected function _delete_non_existing_products($parent, $id_parent, $product_ids, $magento_products)
	{
		$ids_to_delete = array();
		
		foreach($product_ids as $id)
		{
			$found = FALSE;
			foreach($magento_products as $mage_product)
			{
				if ($mage_product['entity_id'] == $id)
					$found = TRUE;
			}
			if ($found == FALSE) $ids_to_delete[] = $id;
		}

		if (!empty ($ids_to_delete))
		{
			$this->db->where('id_parent', $id_parent);
			$this->db->where('parent', $parent);
			$this->db->where_in('entity_id', $ids_to_delete);
			
			$this->db->delete($this->_products_table);
		}
	}
	
}