<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Magento Module Category Model
 *
 */

class Magento_category_model extends Base_model 
{
	// Magento Store ID
	protected $_storeId = 0;

	// Link table between Ionize parent (article, page) and Magento categories
	protected $_categories_table = 'module_magento_categories';

	// 
	protected $_page_table = 'page';
	
	protected $_article_table = 'article';


	public function link_category_to_parent($parent, $id_parent, $entity_id)
	{
		$data = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'entity_id' => $entity_id		
		);
		$this->db->where($data);
		
		$query = $this->db->get($this->_categories_table);

		if ($query->num_rows() == 0)
		{
			$this->db->insert($this->_categories_table, $data);
			return TRUE;
		}
		return FALSE;
	}
	
	
	public function unlink_category_from_parent($parent, $id_parent, $entity_id)
	{
		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent,
			'entity_id' => $entity_id		
		);
		
		return $this->db->delete($this->_categories_table, $where);
	}
	
	
	/**
	 * Returns the first found Magento linked category
	 *
	 */
	public function get_linked_category($parent, $id_parent)
	{
		// $categories_ids = $this->_get_linked_categories_ids($parent, $id_parent);
		
		$categories = $this->_get_linked_categories($parent, $id_parent);

		if ( ! empty($categories) )
		{
			$categories_ids = array();
			
			foreach($categories as $category)
				$categories_ids[] = $category['entity_id'];

			$magento_categories = $this->_get_magento_categories_from_ids($categories_ids);
			
			// Check DB integrity
			$this->_delete_non_existing_categories($parent, $id_parent, $categories_ids, $magento_categories);

			foreach($magento_categories as &$mage_cat)
			{
				foreach($categories as $link_cat)
				{
					if ($mage_cat['entity_id'] == $link_cat['entity_id'])
						$mage_cat['master'] = $link_cat['master'];
				}
			}
			
			if ( ! empty($magento_categories))
				return $magento_categories[0];
		}
		
		return array();
	}
	
	
	/**
	 * Returns the array of Magento linked categories
	 *
	 */
	public function get_linked_categories($parent, $id_parent)
	{
		// $categories_ids = $this->_get_linked_categories_ids($parent, $id_parent);
		
		$categories = $this->_get_linked_categories($parent, $id_parent);

		if ( ! empty($categories) )
		{
			$categories_ids = array();
			
			foreach($categories as $category)
				$categories_ids[] = $category['entity_id'];

			$magento_categories = $this->_get_magento_categories_from_ids($categories_ids);

			// Check DB integrity
			$this->_delete_non_existing_categories($parent, $id_parent, $categories_ids, $magento_categories);

			foreach($magento_categories as &$mage_cat)
			{
				foreach($categories as $link_cat)
				{
					if ($mage_cat['entity_id'] == $link_cat['entity_id'])
						$mage_cat['master'] = $link_cat['master'];
				}
			}
			
			if ( ! empty($magento_categories))
				return $magento_categories;
		}
		
		return array();
	}


	
	/**
	 * Returns the array of Ionize Elements linked to the category
	 *
	 * @param 	string		Parent type. 'article', 'page'
	 * @return 	array		Array of linked parent element
	 *
	 */
	public function get_linked_elements($parent, $id_category)
	{
		$result = array();

		$parent_table = $parent.'_lang';
		$parent_pk = $parent_table.'.id_'.$parent;

		$where = array(
			'parent' => $parent,
			'entity_id' => $id_category,
			$parent_table.'.lang' => Settings::get_lang('default')
		);

		$this->db->where($where);
		$this->db->join($parent_table, $parent_pk .' = ' . $this->_categories_table.'.id_parent', 'inner');			
		
		$query = $this->db->get($this->_categories_table);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result_array();
		}
		
		return $result;

		
	}
	
	/**
	 * Returns all categories data from a parent category ID
	 *
	 * @param	int		Parent Category ID
	 * @return	array	Array of categories
	 *
	 */
	public function get_sub_categories($parent_id = 0)
	{
		$this->load->library('magento');
		
		// Magento Model
		$categoryModel = Mage::getModel('catalog/category'); 

		$categories_data = array();

		// Categories
		$categories = $categoryModel->getTreeModel()->load(); 
		
		$categoriesCollection = $categories->getCollection()
			->addFilter('parent_id', $parent_id)
			->addAttributeToSort('name', 'ASC');
				
		foreach ($categoriesCollection as $category)
		{
			$category->setStoreId($this->_storeId);
			$productCollection = $category->getProductCollection();
			$category->setData('product_count', $productCollection->count());
			$category->setData('type', 'category');
			
			$categories_data[] = $category->getData();
		}
		
		return $categories_data;
	}
	
	
	/**
	 * Returns all Magento Categories
	 * Only adds categories sup. to the given level.
	 *
	 * @param 	Int				Min. wished category level
	 * @return 	Collection		Collection of categories
	 *
	 */
	public function get_all_categories($min_level = 0)
	{
		$this->load->library('magento');
		
		// Magento Model
		$categoryModel = Mage::getModel('catalog/category'); 

		$tree = $categoryModel->getTreeModel();
		$tree->load();

		$ids = $tree->getCollection()->getAllIds();

		$arr = array();

		if ($ids)
		{
			foreach ($ids as $id)
			{
				$cat = Mage::getModel('catalog/category');
				$cat->load($id);
	
				if ($cat->getLevel() >= $min_level)
					array_push($arr, $cat);
			}
		}

		return $arr;
	}
	
	
	
	/**
	 * Returns all products data from a category
	 *
	 * @param	int		Category ID
	 * @return	array	Array of products
	 *
	 */
	public function get_products_from_category($category_id = 0)
	{
		$this->load->library('magento');

		// Magento Models
		$categoryModel = Mage::getModel('catalog/category'); 
		$productModel = Mage::getModel('catalog/product');
		
		$products_data = array();

		$category = NULL;

		// Category
		try
		{
			// Be sure the catalog_category_flat table is managed by Magento
			// To manage categories through the flat table : 
			// 1. System > Configuration > Catalog > Frontend Use flat catalog to manage categories : No
			$category = $categoryModel->setStoreId($this->_storeId)->load($category_id);		
		}
		catch(Exception $e)
		{
			log_message('error', $e->getMessage());
		}
		/*
		// Other method to get the product collection
		
		$productCollection = Mage::getResourceModel('catalog/product_collection')
									->addCategoryFilter($category)
									->load();
		*/


/*
$attributes = Mage::getModel('eav/entity_attribute')->getCollection()->load();

foreach ($attributes as $at)
{
//	az($at->getData());
}
*/
		if ( ! is_null($category))
		{
			// Products Collection
			$productCollection = $category->getProductCollection()
									->addAttributeToFilter('type_id', array('eq' => 'configurable'));
									
			foreach ($productCollection as $product)
			{
				$_product = $productModel->load($product->getId());
				
	/*
	 * HERE : 	Get the configurable attributes labels and values
	 * 			to display them in the name of the simple product if needed
	 *
				$configurableSet = array();
				
				if ($product->isConfigurable() == false)
	            {
	            	$configurable_product = Mage::getModel('catalog/product_type_configurable');
	            	$parentIdArray = $configurable_product->getParentIdsByChild($product->getId());
					
					if (!empty($parentIdArray))
	                {
	                    //get configurable attributes of the parent
	                    $hasParent = true;
	                    $tempParent = $productModel->load($parentIdArray[0]);
	                    $tempattr = $tempParent->getTypeInstance()->getConfigurableAttributes();
	                    foreach ($tempattr as $attr)
	                    {    
	                        $configurableSet[] = $attr->getProductAttribute()->getId();                    
	                    }
	                }                  
	            }

	            $attributes = $product->getAttributes();
	            
	            foreach ($attributes as $attribute)
	            {
	                if (in_array($attribute->getId(), $configurableSet))
	                {
	                    $label = $attribute->getFrontend()->getLabel(); 
	// Doesn't work : 
						$value = $attribute->getFrontend()->getValue($_product);

	foreach($attribute->getSource()->getAllOptions(false) as $option){
	}

	                }
	            }
	            
	*/            

				$_product->setData('parent_id', $category_id);
				
				$products_data[] = $_product->getData();
			}
		}

		return $products_data;
	}




	protected function _get_linked_categories_ids($parent, $id_parent)
	{
		$result = array();

		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent
		);

		$this->db->where($where);
		
		$this->db->select('entity_id');
		
		$query = $this->db->get($this->_categories_table);
		
		if ($query->num_rows() > 0)
		{
			foreach($query->result_array() as $category)
			{
				$result[] = $category['entity_id'];
			}
			
			return $result;
		}
		
		return $result;
	
	}

	protected function _get_linked_categories($parent, $id_parent)
	{
		$result = array();

		$where = array(
			'parent' => $parent,
			'id_parent' => $id_parent
		);

		$this->db->where($where);
		
		$query = $this->db->get($this->_categories_table);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result_array();
		}
		
		return $result;
	
	}


	/**
	 * Returns an array of Magento Categories
	 *
	 * @param	array	Array of Magento categories
	 *
	 */
	protected function _get_magento_categories_from_ids($ids)
	{
		$magento_categories = array();
		
		$this->load->library('magento');
		
		foreach($ids as $category_id)
		{
			$category = Mage::getModel('catalog/category')
				->load($category_id)
				->getData(); 
			
			if ( ! empty($category))
				$magento_categories[] = $category;
		}
		
		return $magento_categories;
	}


	/**
	 * Returns one Magento Category on the given ID
	 *
	 * Note : Not used at this time
	 *
	 */
	protected function _get_magento_category_from_id($category_id)
	{
		$this->load->library('magento');

		// Magento Product Model
		$category = Mage::getModel('catalog/category')
			->load($category_id)
			->getData(); 

		return $category;
	}



	protected function _delete_non_existing_categories($parent, $id_parent, $categories_ids, $magento_categories)
	{
		$ids_to_delete = array();
		
		foreach($categories_ids as $id)
		{
			$found = FALSE;
			foreach($magento_categories as $mage_category)
			{
				if ($mage_category['entity_id'] == $id)
					$found = TRUE;
			}
			if ($found == FALSE) $ids_to_delete[] = $id;
		}

		if (!empty ($ids_to_delete))
		{
			$this->db->where('id_parent', $id_parent);
			$this->db->where('parent', $parent);
			$this->db->where_in('entity_id', $ids_to_delete);
			
			$this->db->delete($this->_categories_table);
		}
	}



	
}