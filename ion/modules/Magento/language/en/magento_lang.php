<?php

$lang['module_magento_about'] = "This module connects Ionize to Magento";


$lang['module_magento_title_magento'] = "Magento";
$lang['module_magento_label_template'] = "Template";

$lang['module_magento_message_template_saved'] = "Magento Template linked to page.";

/*
$lang['module_magento_button_link_product'] = "Link Products";
$lang['module_magento_title_link_product'] = "Link Products";

$lang['module_magento_button_link_category'] = "Link Category";
$lang['module_magento_title_link_category'] = "Link Category";
*/

$lang['module_magento_title_link'] = "Categories and Products";
$lang['module_magento_button_link_product_category'] = "Products / Categories";

$lang['module_magento_label_category'] = "Category";
$lang['module_magento_label_drop_category_here'] = "Drop a category here...";


$lang['module_magento_message_product_linked'] = "Product linked";
$lang['module_magento_message_product_unlinked'] = "Product unlinked";
$lang['module_magento_message_product_already_linked'] = "Product already linked";
$lang['module_magento_message_product_not_linked'] = "The product isn't linked";


$lang['module_magento_tab_categories'] = "Categories";
$lang['module_magento_tab_settings'] = "Settings";

$lang['module_magento_table_head_category'] = "Magento Category";
$lang['module_magento_table_head_pages'] = "CMS Pages";
$lang['module_magento_label_set_master'] = "Set Master";
$lang['module_magento_help_master_category_page'] = "The Magento category links by default to this CMS page";

