<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * eNose
 *
 * @package		eNose
 * @author		Partikule
 * @link		http://www.partikule.net
 * @since		Version 1.0
 */

// ------------------------------------------------------------------------

/**
 * eNose Perfume Model
 *
 *
 */

class Perfume_model extends Base_model 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();


		$this->set_table('perfume');
		$this->set_pk_name('perfume_id');
		$this->set_lang_table('perfume_lang');

		$this->brand_table = 'brand';
		$this->creator_table = 'creator';
		
		
		$this->lk_family = 'lk_perfume_family';
		$this->lk_creator = 'lk_perfume_creator';
		$this->lk_note = 'lk_perfume_note';

	}

	
	// ------------------------------------------------------------------------


	/**
	 * Get one perfume
	 * 
	 */
	function get($where, $lang = NULL) 
	{
		$data = parent::get($where, $lang);
		return $data;
	}
	
	
	// ------------------------------------------------------------------------

	
	/** 
	 * Get perfume list with additional data (brand, etc.)
	 *
	 */
	function get_lang_list($where=FALSE, $lang=NULL)
	{
		$data = array();
		
		// Perform conditions from the $where array
		foreach(array('limit', 'offset', 'order_by', 'like') as $key)
		{
			if(isset($where[$key]))
			{
				call_user_func(array($this->db, $key), $where[$key]);
				unset($where[$key]);
			}
		}
		
		// Filter on some ambigious columns
		$ambiguous = array('brand_id' => 'brand.brand_id', 'perfume_id'=>'perfume.perfume_id');
		
		function rename_ambiguous($array, $ambiguous)
		{
			if (is_array($array))
			{
				foreach ($array as $key => $val)
				{
					if (in_array($key, array_keys($ambiguous)))
					{
						unset($array[$key]);
						$key = $ambiguous[$key];
						$array[$key] = $val;
					}
				}
				return $array;
			}
		}
		
		$where = rename_ambiguous($where, $ambiguous);
		
		if ($where) $this->db->where($where);
		

		// Make sure we have only one time each element
		$this->db->distinct();

		// Lang data
		if ( ! is_null($lang) || ! $lang == false)
		{
			$this->db->select($this->lang_table.'.*');
			$this->db->join($this->lang_table, $this->lang_table.'.'.$this->pk_name.' = ' .$this->table.'.'.$this->pk_name, 'inner');			
			$this->db->where($this->lang_table.'.lang', $lang);
		}


		// Main data select						
		$this->db->select($this->table.'.*', false);

		// Brand table join
		$this->db->select($this->brand_table.'.brand_name', false);
		$this->db->join($this->brand_table, $this->brand_table.'.brand_id = ' .$this->table.'.brand_id', 'inner');

		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			$query->free_result();

			// Add Creators
			$this->add_creators($data);
			
			// Get users  : updates informations
			$users = $this->get_all('users');
			
			// Add Creators informations
			foreach ($data as &$perfume)
			{
				$perfume['user'] = array_shift(array_filter($users, (create_function('$u', 'return $u["id_user"] == '.$perfume['updater'].';'))));
	
				$perfume['creators'] = array_map(create_function('$c', 'return $c["creator_name"];'), $perfume['creators']);
			}
		}

		return $data;
	}

	
	function get_perfume_select($where=FALSE)
	{
		$data = array();
		
		// Perform conditions from the $where array
		foreach(array('limit', 'offset', 'order_by', 'like') as $key)
		{
			if(isset($where[$key]))
			{
				call_user_func(array($this->db, $key), $where[$key]);
				unset($where[$key]);
			}
		}
		
		foreach($where as $key => $cond)
		{
			if ($key == 'sex')
			{
				if ($cond != FALSE)
					$this->db->where('(sex='.$cond.' OR sex=3)');
			}
			else
			{
				$this->db->where($key, $cond);
			}
		}
		
		// Make sure we have only one time each element
		$this->db->distinct();

		// Main data select						
		$this->db->select($this->table.'.*', false);

		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			$query->free_result();
		}

		return $data;
	}
		
		
	// ------------------------------------------------------------------------


	/**
	 * Saves one note for this perfume
	 *
	 */
	public function save_note_link($perfume_id, $note_id, $note_type, $id_user='0')
	{
		$data = array(
			'perfume_id' => $perfume_id,
			'note_id' => $note_id,
			'note' => $note_type
		);
	
		if (FALSE == $this->exists($data, $this->lk_note) )
		{
			$data['id_user'] = $id_user;
			$data['updated'] = date('Y-m-d H:i:s');
			
			$this->db->insert($this->lk_note, $data);
			
			return TRUE;
		}
		
		return FALSE;
	}

	
	
	// ------------------------------------------------------------------------

	/**
	 * Used by front-end to save user's associations
	 *
	 */
	public function save_user_association($user, $data, $perfumes_array)
	{
		$id_user = FALSE;
	
		// Get the user
		$user_db = array();
		$this->db->where('email', $user['email']);
		$query = $this->db->get('users');
		
		if ( $query->num_rows() > 0 )
			$user_db = $query->row_array();
		
		// Create the user if he doesn't exists
		if ( empty($user_db))
		{
			$user['id_group'] = '6';
			$user['join_date'] = date('Y-m-d H:i:s');
			$user['last_visit'] = date('Y-m-d H:i:s');
			
			$this->db->insert('users', $user);
			$id_user = $this->db->insert_id();
		}
		else
		{
			$user = $user_db;
			$user['last_visit'] = date('Y-m-d H:i:s');
			$this->db->where('id_user', $user['id_user']);
			$this->db->update('users', $user);
			
			$id_user = $user['id_user'];
		}
		
		for ($i = 1; $i<6; $i++)
		{
			if ( ! empty($perfumes_array[$i -1]))
			{
				$data['perfume_id_' . $i] = $perfumes_array[$i -1];
			}
			else
			{
				$data['perfume_id_' . $i] = '';
			}
		}
				
		// Save input diag
		if ($id_user)
		{
			$data['id_user'] = $id_user;
			$data['test_date'] = date('Y-m-d H:i:s');
			
			// Delete the current user diag
			$this->db->delete('input_diag', array('id_user' => $id_user));

			$this->db->insert('input_diag', $data);
		}
	}
	
	// ------------------------------------------------------------------------
	
	public function get_user_diag($where, $limit=6)
	{
		$data = array();

		$this->db->where($where);

		$this->db->where('perfume.nose', '1');
		$this->db->limit($limit);


		$this->db->select('
			perfume.perfume_id,
			perfume.perfume_name, 
			perfume.family_id, 
			perfume.subfamily_id, 
			perfume.specifer_id, 
			diag_user.index_quality, 
			diag_user.score, 
			brand.brand_name
		');
		$this->db->join('diag_user', 'diag_user.perfume_id_2 = perfume.perfume_id');
		$this->db->join('brand', 'perfume.brand_id = brand.brand_id');


		$this->db->order_by('diag_user.index_quality DESC, diag_user.score DESC');
		
		$query = $this->db->get('perfume');

// trace($this->db->last_query());
		
		if ( $query->num_rows() > 0 )
			$data = $query->result_array();
	
		return $data;

	}

	
	// ------------------------------------------------------------------------


	public function get_association($where)
	{
		$data = array();
	
		$this->db->where($where);
		
		$this->db->select('perfume.perfume_name, perfume_associations.*, users.screen_name');
		$this->db->join('perfume', 'perfume.perfume_id = perfume_associations.perfume_id_2');
		$this->db->join('users', 'perfume_associations.author = users.id_user', 'left outer');

		$query = $this->db->get('perfume_associations');

		if ( $query->num_rows() > 0 )
			$data = $query->row_array();
	
		return $data;
	}
	
	
	// ------------------------------------------------------------------------

	
	public function get_input_diags($where)
	{
		$data = array();

		$this->db->select('input_diag_view.*, perfume.*, brand.*,perfume_associations.number,perfume_associations.index_quality,perfume_associations.index_quality_nose');
		$this->db->where($where);
		$this->db->order_by('score DESC');
		$this->db->join('perfume', 'input_diag_view.perfume_id_2=perfume.perfume_id');
		$this->db->join('brand', 'perfume.brand_id = brand.brand_id');
		$this->db->join('perfume_associations', 'input_diag_view.perfume_id_1 = perfume_associations.perfume_id_1 and input_diag_view.perfume_id_2 = perfume_associations.perfume_id_2', 'left outer');


		$this->db->limit('30');
		
		$query = $this->db->get('input_diag_view');
				
		if ( $query->num_rows() > 0 )
			$data = $query->result_array();

		return $data;
	}
	
	// ------------------------------------------------------------------------

	public function get_notes($where, $lang)
	{
		$data = array();
	
		$this->db->select('lk_perfume_note.*, note.validation, note.in_user_box, note_lang.*, users.*');
		
		$this->db->join('note', 'note.note_id = lk_perfume_note.note_id');
		$this->db->join('note_lang', 'note_lang.note_id = note.note_id');
		$this->db->join('users', 'lk_perfume_note.id_user = users.id_user', 'left outer');
		
		$this->db->where('note_lang.lang', $lang);
		
		// Perform conditions from the $where array
		foreach(array('limit', 'offset', 'order_by', 'like') as $key)
		{
			if(isset($where[$key]))
			{
				call_user_func(array($this->db, $key), $where[$key]);
				unset($where[$key]);
			}
		}

		$this->db->where($where);
	
		$query = $this->db->get('lk_perfume_note');
	
		if ( $query->num_rows() > 0 )
			$data = $query->result_array();
	
		return $data;
	}




	public function get_associations($where, $limit=FALSE)
	{
		$data = array();
	
		$this->db->select('perfume.perfume_name,perfume.perfume_id,perfume.nose, brand.brand_name, perfume_id_1, perfume_id_2, index_quality, number, index_quality_nose, number_nose, author, perfume_associations.updated, trash, users.screen_name');
		
		$this->db->join('perfume', 'perfume.perfume_id = perfume_associations.perfume_id_2');
		$this->db->join('brand', 'perfume.brand_id = brand.brand_id');
		$this->db->join('users', 'perfume_associations.author = users.id_user', 'left outer');
		
		// Perform conditions from the $where array
		foreach(array('limit', 'offset', 'order_by', 'like') as $key)
		{
			if(isset($where[$key]))
			{
				call_user_func(array($this->db, $key), $where[$key]);
				unset($where[$key]);
			}
		}

		$this->db->where($where);
		
		$this->db->order_by('number_nose');
		
		if ($limit != FALSE)
		{
			$this->db->limit($limit);
		}

		$query = $this->db->get('perfume_associations');

		
		if ( $query->num_rows() > 0 )
			$data = $query->result_array();
	
		return $data;
	}

	

	// ------------------------------------------------------------------------


	function save_top10_ordering($ordering, $id_parent)
	{
		if ( ! is_array($ordering))
		{
			$ordering = explode(',', $ordering);
		}
		$new_order = '';
		$i = 1;
		
		while (list ($rank, $id) = each ($ordering))	
		{
			$this->db->where('perfume_associations.perfume_id_2', $id);
			$this->db->where('perfume_associations.perfume_id_1', $id_parent);

			$this->db->set('number_nose', $i++);
			$this->db->update('perfume_associations');
										
			$new_order .= $id.",";
		}
		return substr($new_order, 0, -1);
	}

	
	// ------------------------------------------------------------------------


	public function rate_association($perfume_id_1, $perfume_id_2, $rate, $id_user)
	{
		$this->db->where('perfume_associations.perfume_id_2', $perfume_id_2);
		$this->db->where('perfume_associations.perfume_id_1', $perfume_id_1);

		$this->db->set('index_quality_nose', $rate);
		$this->db->set('author', $id_user);
		$this->db->set('updated', date('Y-m-d H:i:s'));

		return $this->db->update('perfume_associations');
	}
	
	
	// ------------------------------------------------------------------------





	public function insert_association($data = NULL)
	{
		if ( is_array($data) )
		{
			$this->insert($data, 'perfume_associations');
			
			return TRUE;
		}
		return FALSE;
	}
	
	
	
	public function update_association($where = NULL, $data = NULL)
	{
		if ( is_array($where) )
		{
			$this->update($where, $data, 'perfume_associations');

			return TRUE;
		}
		
		return FALSE;
	}
	
	// ------------------------------------------------------------------------


	public function delete_association($where = NULL)
	{
		if ( is_array($where) )
		{
			if (TRUE == ($this->exists($where, 'perfume_associations')))
			{
				$this->db->where($where);
				$this->db->delete('perfume_associations', $where);
			}

			return (int) $this->db->affected_rows();
		}
		
		return FALSE;
	}
	

	// ------------------------------------------------------------------------


	public function count_perfumes($cond = array())
	{
		if(isset($cond['like']))
		{
			$this->db->like($cond['like']);
			unset($cond['like']);
		}
		
		unset($cond['order_by']);
		unset($cond['limit']);
		unset($cond['offset']);
		
		$this->db->where($cond);
		
		$this->db->from($this->table);
		
		return $this->db->count_all_results();
	}

	
	// ------------------------------------------------------------------------


	/**
	 * Adds to each element the creators
	 *
	 * @param	array	By ref. The array to add the urls datas
	 *
	 */
	protected function add_creators(&$data)
	{
		$this->db->select($this->creator_table.'.creator_name,'.$this->lk_creator.'.perfume_id', false);
		$this->db->join($this->lk_creator, $this->creator_table.'.creator_id = ' .$this->lk_creator.'.creator_id', 'inner');

		$query = $this->db->get($this->creator_table);

		// Feed each media array

		if($query->num_rows() > 0)
		{
			$result = $query->result_array();
			
			$query->free_result();

			$c1 = count($data);
			$c2 = count($result);
			$i = 0;

			$z = 0;


			/*
			 * Perf. testing
			 *
			function getDiff($start, $end) {
			    $s = explode(' ', $start);
			    $stot = $s[1] + $s[0];
			    $e = explode(' ', $end);
			    $etot = $e[1] + $e[0];
			    return $etot - $stot;
			}
			
			$start = microtime();
	
			while($i<$c1)
			{
				$data[$i]['creators'] = array();
				$j = 0;
				while($j<$c2)
				{
					if ($data[$i][$this->pk_name] == $result[$j][$this->pk_name])
					{
						$data[$i]['creators'][] = $result[$j];
					}
					$j++;
				}
				$i++;
				
				$z = $i * $j;
			}
	
			$end = microtime();
			trace(getDiff($start, $end));
			
			// Second test
			$start = microtime();
	
			foreach($data as $key => $el)
			{
				$data[$key]['creators'] = array();
				
				foreach($result as $creator)
				{
					if ($el[$this->pk_name] == $creator[$this->pk_name])
					{
						$data[$key]['creators'][] = $creator;
					}
				}
			}
			
			$end = microtime();
			
			trace(getDiff($start, $end));		
			
			*/
	
			foreach($data as $key => $el)
			{
				$creators = array_values(array_filter($result, create_function('$row','return ($row["'.$this->pk_name.'"] == "'. $el[$this->pk_name] .'");')));
				$data[$key]['creators'] = $creators;
			}
		}
	}
}
