<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Application config
|--------------------------------------------------------------------------
|
| SITE_IDENTIFIER : Define precisely on which host is installed the application
|					String.
|
| SITE_LIVE : 		Is the site in live or not ?
|					Boolean
|					Useful for including Google Analytics or such things.
|
*/
switch ($_SERVER['HTTP_HOST'])
{
	// Prod
	case "nose.fr":
	case "www.nose.fr":
		define('SITE_IDENTIFIER', 'prod.live', TRUE);
		break;
	
	// Preprod
	case "preprod.nose.nose.nbs-test.com":
	case "nose.walkwizus.fr":
		define('SITE_IDENTIFIER', 'preprod.live', TRUE);
		break;

	// Dev
	case "nose.partikule.com":
		define('SITE_IDENTIFIER', 'dev.live', TRUE);
		break;


	// Dev or Local
	case "192.168.1.20":
	case "nose.home":
	case "nose3.macbook":
	case "localhost":
	case "www.nose.dev":
		define('SITE_IDENTIFIER', 'prod.local', TRUE);
		break;
	
	default:
		define('SITE_IDENTIFIER', 'prod.local', TRUE);
		break;

}


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */