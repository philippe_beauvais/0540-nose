<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;
$db['default'] = array();
$db['enose.prod'] = array();

switch (SITE_IDENTIFIER)
{
	/*
	|--------------------------------------------------------------------------
	| Prod
	|--------------------------------------------------------------------------
	*/
	case 'prod.live': 

		$db['default']['hostname'] = '10.1.198.1';
		$db['default']['username'] = 'prod';
		$db['default']['password'] = 'Titulnertug3';
		$db['default']['database'] = 'nose_cms_prod';
		$db['default']['dbdriver'] = 'mysql';
		$db['default']['dbprefix'] = '';
		$db['default']['swap_pre'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_unicode_ci';

/*		$db['enose.prod']['hostname'] = '10.1.17.90';
		$db['enose.prod']['username'] = 'nose';
		$db['enose.prod']['password'] = 'voodPhefoic9';
		$db['enose.prod']['database'] = 'nose_enose_prod';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';*/

		$db['enose.prod']['hostname'] = '5.39.22.1';
		$db['enose.prod']['username'] = 'nose';
		$db['enose.prod']['password'] = 'voodPhefoic9';
		$db['enose.prod']['database'] = 'enose';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';

		break;
	
	/*
	|--------------------------------------------------------------------------
	| Pre Prod
	|--------------------------------------------------------------------------
	*/
	case 'preprod.live':

		$db['default']['hostname'] = 'mysql001.walkwizus.com';
		$db['default']['username'] = 'nose';
		$db['default']['password'] = 'Kf3yeMXxdGTAAU9X';
		$db['default']['database'] = 'nose_cms';
		$db['default']['dbdriver'] = 'mysql';
		$db['default']['dbprefix'] = '';
		$db['default']['swap_pre'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_unicode_ci';

		$db['enose.prod']['hostname'] = 'mysql001.walkwizus.com';
		$db['enose.prod']['username'] = 'nose';
		$db['enose.prod']['password'] = 'Kf3yeMXxdGTAAU9X';
		$db['enose.prod']['database'] = 'nose_enose';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';

		break;
	
	/*
	|--------------------------------------------------------------------------
	| Partikule Dev
	|--------------------------------------------------------------------------
	*/
	case 'dev.live':

		$db['default']['hostname'] = '127.0.0.1';
		$db['default']['username'] = 'dev';
		$db['default']['password'] = 'dev459459';
		$db['default']['database'] = 'nose_cms';
		$db['default']['dbdriver'] = 'mysql';
		$db['default']['dbprefix'] = '';
		$db['default']['swap_pre'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_unicode_ci';

		$db['enose.prod']['hostname'] = '5.39.22.0';
		$db['enose.prod']['username'] = 'nose';
		$db['enose.prod']['password'] = 'voodPhefoic9';
		$db['enose.prod']['database'] = 'enose';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';

		break;

	/*
	|--------------------------------------------------------------------------
	| Local Dev
	|--------------------------------------------------------------------------
	*/
	case 'prod.local': 

		$db['default']['hostname'] = '127.0.0.1';
		$db['default']['username'] = 'root';
		$db['default']['password'] = 'root';
		$db['default']['database'] = 'nose19_cms';
		$db['default']['dbdriver'] = 'mysql';
		$db['default']['dbprefix'] = '';
		$db['default']['swap_pre'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_unicode_ci';

		$db['enose.prod']['hostname'] = '127.0.0.1';
		$db['enose.prod']['username'] = 'root';
		$db['enose.prod']['password'] = 'root';
		$db['enose.prod']['database'] = 'enoseme19';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';
		break;

	case 'preprod.walkwizus':

		$db['default']['hostname'] = 'mysql001.walkwizus.com';
		$db['default']['username'] = 'nose';
		$db['default']['password'] = 'Kf3yeMXxdGTAAU9X';
		$db['default']['database'] = 'nose_cms';
		$db['default']['dbdriver'] = 'mysql';
		$db['default']['dbprefix'] = '';
		$db['default']['swap_pre'] = '';
		$db['default']['pconnect'] = FALSE;
		$db['default']['db_debug'] = TRUE;
		$db['default']['cache_on'] = FALSE;
		$db['default']['cachedir'] = '';
		$db['default']['char_set'] = 'utf8';
		$db['default']['dbcollat'] = 'utf8_unicode_ci';

		$db['enose.prod']['hostname'] = 'mysql001.walkwizus.com';
		$db['enose.prod']['username'] = 'nose';
		$db['enose.prod']['password'] = 'Kf3yeMXxdGTAAU9X';
		$db['enose.prod']['database'] = 'nose_enose';
		$db['enose.prod']['dbdriver'] = 'mysql';
		$db['enose.prod']['dbprefix'] = '';
		$db['enose.prod']['swap_pre'] = '';
		$db['enose.prod']['pconnect'] = FALSE;
		$db['enose.prod']['db_debug'] = TRUE;
		$db['enose.prod']['cache_on'] = FALSE;
		$db['enose.prod']['cachedir'] = '';
		$db['enose.prod']['char_set'] = 'utf8';
		$db['enose.prod']['dbcollat'] = 'utf8_unicode_ci';
		break;
}






/* End of file database.php */
/* Auto generated by Installer on 2011.09.28 16:02:56 */
/* Location: ./application/config/database.php */
