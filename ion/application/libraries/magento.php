<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Magento
{

	private $pseudo_constructed = false;

	function __construct()
    {
		$ci =  &get_instance();
		return $this->initialize();
    }

	
	public function initialize()
	{
		if(class_exists('Mage'))
			return TRUE;

		$magento_config = config_item('magento_path') . 'includes/config.php';
		
		if (file_exists($magento_config))
			include($magento_config);
		else
		{
			log_message('error', 'Magento Module : Cannot find the Magento config file. Check your path in modules/Magento/config/config.php !');	
			return FALSE;
		}
 
		$mageFilename = config_item('magento_path') . 'app/Mage.php';

		if (file_exists($mageFilename))
			require_once $mageFilename;
 		else
 		{
			log_message('error', 'Magento Module : Mage class file not found');	
 			return FALSE;
 		}

		//	Mage::app('default');
		//	Mage::app('base');
		//	Mage::app('admin');
		Mage::app(); // initialiser Magento avec le magasin Admin

		if(class_exists('Mage'))
		{
			return TRUE;
		}
		
		log_message('error', 'Magento Module : Could not init Mage class');	
		return FALSE;

		// Mage::getSingleton('core/session', array('name' => 'frontend'));

	}
	
	public function mage()
	{
		if(class_exists('Mage'))
			return TRUE;
		
		return FALSE;
	}


}