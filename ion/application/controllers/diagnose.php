<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diagnose extends CI_Controller {


	private $data = array();	// Included view data array;


	public function __construct()
	{
		parent::__construct();

//		$this->load->database();

		// Models
//		$this->load->model('base_model', '', true);
//		$this->load->model('diagnose/perfume_model', '', true);

		$this->load->helper(array('array_helper', 'form', 'url'));

		$this->load->library('magento');

		
	}


	function index()
	{
echo('index');

		if ($this->magento->mage())
		{

		echo('diagnose Index from ionize <br/>');
		
		

		

		if (isset($_COOKIE['frontend'])) {

			Zend_Debug::dump($_COOKIE['frontend']);

			$session_id = $_COOKIE['frontend'];
			
//			session_id($session_id);
//			session_start();
			
			$coreSession = Mage::getSingleton('core/session', array('name'=>'frontend'));
			$coreSession->setSessionId($sessionId);
// $coreSession->start();
			
			$customerSession = Mage::getSingleton("customer/session", array('name'=>'frontend'));
			$myData = $customerSession->isLoggedIn();

			Zend_Debug::dump($_SESSION);


			
			Zend_Debug::dump($myData);
			Zend_Debug::dump($coreSession->toArray());
			Zend_Debug::dump($customerSession->toArray());

		}


//$customer = $session->getCustomer();
//Mage::getSingleton('customer/session')->getCustomer();


		}		
		
		// $this->output('index');
		
		
	///	echo(Theme::get_theme());
		
	}
	
	function diagnose()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		// Validation rules
		$config = array(
			array('field'   => 'first_name','label'   => 'First Name','rules'   => 'trim|required'),
			array('field'   => 'last_name','label'   => 'Last Name','rules'   => ''),
			array('field'   => 'email','label'   => 'Email','rules'   => 'trim|required|valid_email'),
			array('field'   => 'age','label'   => 'Age','rules'   => ''),
			array('field'   => 'sex','label'   => 'Sex','rules'   => ''),
			array('field'   => 'style_id','label'   => 'Style','rules'   => ''),
			array('field'   => 'flower_scent','label'   => 'Flower Scent','rules'   => ''),
			array('field'   => 'family_id_1','label'   => 'Family 1','rules'   => ''),
			array('field'   => 'family_id_2','label'   => 'Family 2','rules'   => ''),
			array('field'   => 'brand_1','label' => 'Brand 1','rules' => ''),
			array('field'   => 'brand_2','label' => 'Brand 2','rules' => ''),
			array('field'   => 'brand_3','label' => 'Brand 3','rules' => ''),
			array('field'   => 'brand_4','label' => 'Brand 4','rules' => ''),
			array('field'   => 'brand_5','label' => 'Brand 5','rules' => ''),
			array('field'   => 'brand_name_1','label' => 'Brand Name','rules' => ''),
			array('field'   => 'brand_name_2','label' => 'Brand Name','rules' => ''),
			array('field'   => 'brand_name_3','label' => 'Brand Name','rules' => ''),
			array('field'   => 'brand_name_4','label' => 'Brand Name','rules' => ''),
			array('field'   => 'brand_name_5','label' => 'Brand Name','rules' => ''),
			array('field'   => 'perfume_id_1','label' => 'Perfume ID','rules' => ''),
			array('field'   => 'perfume_id_2','label' => 'Perfume ID','rules' => ''),
			array('field'   => 'perfume_id_3','label' => 'Perfume ID','rules' => ''),
			array('field'   => 'perfume_id_4','label' => 'Perfume ID','rules' => ''),
			array('field'   => 'perfume_id_5','label' => 'Perfume ID','rules' => ''),

			array('field'   => 'never','label'   => 'Never','rules'   => '')
        );
		
		for ($i = 1; $i<6; $i++)
		{
			if (isset($_POST['brand_name_'.$i]) && $_POST['brand_name_'.$i] == '')
			{
				$_POST['brand_'.$i] = '';
			}
		}
		
		$this->form_validation->set_rules($config);

		
		
		if ($this->form_validation->run() == FALSE)
		{
			$families = $this->family_model->get_lang_list(FALSE, Settings::get_lang('default'));


			$families = $this->perfume_model->get_lang_items_select('family', 'family_name', Settings::get_lang('default'), '-- Select --', 'family_name ASC');
			$this->template['family_id_1'] = form_dropdown('family_id_1', $families, $this->input->post('family_id_1'), 'id="family_id_1" class="inputtext w140"');
			$this->template['family_id_2'] = form_dropdown('family_id_2', $families, $this->input->post('family_id_2'), 'id="family_id_2" class="inputtext w140"');
			$this->template['family_id_3'] = form_dropdown('family_id_3', $families, $this->input->post('family_id_3'), 'id="family_id_3" class="inputtext w140"');

			// Filter families by allowed family_types and create associate select dropdown
//			$config_families = config_item('family_types');
//			$family = array_shift($config_families);
				
//			$families_filter = array_values(array_filter($families, create_function('$row','return ($row["family_type"] == "'. $family .'");')));
			
//			$families_select = array('' => '-- Select --');
//			foreach($families_filter as $family)
//			{
//				$families_select[$family['family_id']] = $family['family_name'];
//			}
	
			// Drop Down
//			$this->template['family_id_1'] = form_dropdown('family_id_1', $families_select, $current = $this->input->post('family_id_1'), 'id="family_id_1" class="inputtext w140"');
//			$this->template['family_id_2'] = form_dropdown('family_id_2', $families_select, $current = $this->input->post('family_id_2'), 'id="family_id_2" class="inputtext w140"');
//			$this->template['family_id_3'] = form_dropdown('family_id_3', $families_select, $current = $this->input->post('family_id_3'), 'id="family_id_3" class="inputtext w140"');
	
	
			$styles = $this->style_model->get_lang_list(FALSE, Settings::get_lang());
			$styles_select = array('' => '-- Select --');
			foreach($styles as $style)
			{
				$styles_select[$style['style_id']] = $style['style_name'];
			}
	
			$this->template['style_id'] = form_dropdown('style_id', $styles_select, $current = $this->input->post('style_id'), 'id="style_id" class="inputtext w140"');
			

			// Floral Notes
			$flower_scent = $this->note_model->get_lang_list(array('in_user_box' => '1'), Settings::get_lang('default'));
			$flower_scent_select = array(''=>'-- Select / Empty --');
			foreach($flower_scent as $note)
			{
				$flower_scent_select[$note['note_id']] = $note['note_name'];
			}
			$this->template['note_id'] = form_dropdown('note_id', $flower_scent_select, $current = $this->input->post('note_id'), 'id="note_id" class="inputtext w140"');


			$this->output('diagnose');
		}
		else
		{
			$this->result();
		}

	}



	
	function result()
	{
		$this->load->model('connect_model', '', true);
		
		if ($this->input->post('form_name') == FALSE)
		{
			redirect('diagnostic/diagnose');
		}
		
		
		/**
		 * Data save
		 *
		 *
		 */

		// Temp : We get the perfume ID not from the "perfume_id_x" field, which should be dedicated to the form repopulation... in the future....
		$perfume_id_1 = $this->input->post('perfume_1');
		$perfume_id_2 = $this->input->post('perfume_2');
		$perfume_id_3 = $this->input->post('perfume_3');
		$perfume_id_4 = $this->input->post('perfume_4');
		$perfume_id_5 = $this->input->post('perfume_5');

		// Perfumes array : for save
		$data = array(
			'style_id' => $this->input->post('style_id'),
			'note_id' => $this->input->post('note_id'),
			'family_id_1' => $this->input->post('family_id_1'),
			'family_id_2' => $this->input->post('family_id_2')
		);
		
		//  Add perfume IDs to data array
		$perfumes_array = array();
		for ($i = 1; $i<6; $i++)
		{
			if ($this->input->post('perfume_' . $i) != FALSE)
			{
				$perfumes_array[] = $this->input->post('perfume_' . $i);
			}
		}
		
		// Save perfumes associations
		if (count($perfumes_array) > 0 )
		{
			$user = array(
				'username' => $this->input->post('email'),
				'screen_name' => $this->input->post('first_name') . ' ' . $this->input->post('last_name') ,
				'email' => $this->input->post('email'),
				'sex' => $this->input->post('sex'),
				'age' => $this->input->post('age')
			);
			
			// Save the user and the association
			$this->perfume_model->save_user_association($user, $data, $perfumes_array);
		}	
		
		// Get the user
		$user = $this->connect_model->find_user(array('email' => $this->input->post('email')));
		
		if ( ! empty($user))
		{
			// Get the user Diagnostic 
//			trace($perfumes_array);
			
			$where = array('diag_user.id_user' => $user['id_user']);
			
			$perfumes = $this->perfume_model->get_user_diag($where);
			
//			trace($perfumes);
			$this->template['recommandations'] = $perfumes;

		
			if ( ! empty($perfumes))
			{
				$product = $perfumes[0];
				
				$product = array_merge($product, array_shift($this->perfume_model->get_lang($product['perfume_id'], Settings::get_lang('default') )));

				$this->template['media'] = $this->perfume_model->get_linked_items('perfume', 'media', array('perfume_id'=> $product['perfume_id']) );
				if ( ! empty($this->template['media']))
				{
					$this->template['media'] = array_shift($this->template['media']);
				}
				

/*
				$this->template['media'] = $this->perfume_model->get_linked_lang_items('perfume', 'media', array('perfume_id'=>$product['perfume_id']), Settings::get_lang('default'));
	
				if ( ! empty($this->template['media']))
				{
					$this->template['media'] = array_shift($this->template['media']);
				}
*/	
				
				$notes = $this->perfume_model->get_linked_lang_items('perfume', 'note', array('perfume_id'=> $perfume_id_1), Settings::get_lang('default'), $prefix = 'lk_');
	
				$this->template['notes']['TETE'] = $this->template['notes']['COEUR'] = $this->template['notes']['FOND'] = array();
				foreach($notes as $note)
				{
					if ($note['note'] == 'TETE') $this->template['notes']['TETE'][] = $note['note_name'];
					if ($note['note'] == 'COEUR') $this->template['notes']['COEUR'][] = $note['note_name'];
					if ($note['note'] == 'FOND') $this->template['notes']['FOND'][] = $note['note_name'];
				}
				
				// Get the first perfume scent.
	//			$this->template['perfume_notes'] = $this->perfume_model->get_linked_lang_items('perfume', 'note', array('perfume_id'=> $perfume_id_1), Settings::get_lang('default'), $prefix = 'lk_');
	
				// Get the first perfume families.
	/*
				$this->template['perfume_families']['Family'] = $this->template['perfume_families']['Sub Family'] = $this->template['Specifer']['Family'] = array();
				$perfume_families = $this->perfume_model->get_linked_lang_items('perfume', 'family', array('perfume_id'=> $perfume_id_1), Settings::get_lang('default'), $prefix = 'lk_');
				foreach($perfume_families as $pf)
				{
					if ($pf['family_type'] == 'Family') $this->template['perfume_families']['Family'][] = $pf['family_name'];
					if ($pf['family_type'] == 'Sub Family') $this->template['perfume_families']['Sub Family'][] = $pf['family_name'];
					if ($pf['family_type'] == 'Specifer') $this->template['perfume_families']['Specifer'][] = $pf['family_name'];
				}
	*/

				$this->template['family'] = $this->family_model->get($product['family_id'], Settings::get_lang('default'));
				$this->template['subfamily'] = $this->subfamily_model->get($product['subfamily_id'], Settings::get_lang('default'));
				$this->template['specifer'] = $this->specifer_model->get($product['specifer_id'], Settings::get_lang('default'));
				
				// Send product to view
				$this->template['product'] = $product;
			}

			// User associations : Based on first perfume
			$where = array (
				'input_diag_view.perfume_id_1' => $perfume_id_1,
				'perfume.nose' => '1'
			);
			$perfumes = $this->perfume_model->get_input_diags($where);
	
			$this->template['users_perfumes'] = $perfumes;

		}

		$this->output('diagnose_result');
	}
	
	
	function get_product()
	{
		$perfume_id = $this->input->post('perfume_id');
		
		$product  = $this->perfume_model->get(array('perfume_id'=>$perfume_id));
		$product = array_merge($product, array_shift($this->perfume_model->get_lang($perfume_id, Settings::get_lang('default') )));

		$brand = $this->brand_model->get(array('brand_id' => $product['brand_id'] ));

		$product['brand_name'] = $brand['brand_name'];
	

		$this->template['media'] = $this->perfume_model->get_linked_items('perfume', 'media', array('perfume_id'=> $perfume_id) );
		if ( ! empty($this->template['media']))
		{
			$this->template['media'] = array_shift($this->template['media']);
		}

		$notes = $this->perfume_model->get_linked_lang_items('perfume', 'note', array('perfume_id'=> $perfume_id), Settings::get_lang('default'), $prefix = 'lk_');

		foreach($notes as $note)
		{
			if ($note['note'] == 'TETE') $this->template['notes']['TETE'][] = $note['note_name'];
			if ($note['note'] == 'COEUR') $this->template['notes']['COEUR'][] = $note['note_name'];
			if ($note['note'] == 'FOND') $this->template['notes']['FOND'][] = $note['note_name'];
		}

		
		// Get the first perfume scent.
		$this->template['perfume_notes'] = $this->perfume_model->get_linked_lang_items('perfume', 'note', array('perfume_id'=> $perfume_id), Settings::get_lang('default'), $prefix = 'lk_');

		
		$this->template['product'] = $product;
		
		$this->output('product');
		
	}
	
/*	
	function folders()
	{
		$path = FCPATH.'files/';

		$perfumes = $this->perfume_model->get_lang_list(array(), Settings::get_lang('default'));		
		
		foreach($perfumes as $perfume)
		{
			$medias = $this->perfume_model->get_linked_items('perfume', 'media', array('perfume_id'=> $perfume['perfume_id']) );
			
			if ( ! empty($medias))
			{
				$picture = $medias[0];
				$brand = url_title($perfume['brand_name'], 'underscore');
					
				if ( ! is_dir($path.$brand))
				{
					mkdir($path.$brand, 0777);
				}
				if (rename(FCPATH.'files/'.$picture['file_name'], $path.$brand.'/'.$picture['file_name']) == TRUE)
				{
					$this->db->where(array('id_media'=> $picture['id_media']));
					$this->db->update('media', array('path' => 'files/'.$brand.'/'.$picture['file_name'], 'base_path' => 'files/'.$brand));
					
					trace($path.$picture['file_name'].' :: OK');
					flush();
//					trace(url_title($perfume['brand_name'], 'underscore').'::'.$perfume['perfume_name'].'::'.$medias[0]['file_name']);
					
				}
			
			}
		
		}
		
		
	}
*/	
	
}

/* End of file diagnostic.php */
/* Location: ./application/controllers/diagnostic.php */