<?php
/**
 * Ionize
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.0
 *
 */

/**
 * Default Ionize Controller
 * Displays all pages
 *
 */

class Page extends CI_Controller
{
	function index()
	{
		// Init the Page TagManager
		// TagManager_Page::init();
		
		$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		$base_url .= "://".$_SERVER['HTTP_HOST'];		

		redirect($base_url, 301);
	}
}


/* End of file page.php */
/* Location: ./application/controllers/page.php */