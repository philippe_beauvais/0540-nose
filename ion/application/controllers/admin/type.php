<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ionize, creative CMS
 *
 * @package		Ionize
 * @author		Ionize Dev Team
 * @license		http://ionizecms.com/doc-license
 * @link		http://ionizecms.com
 * @since		Version 0.9.8
 */

// ------------------------------------------------------------------------

/**
 * Ionize Article Type Controller
 *
 * @package		Ionize
 * @subpackage	Controllers
 * @category	Content Type
 * @author		Ionize Dev Team
 *
 */

class Type extends MY_admin 
{
	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('type_model', '', TRUE);
	}


	// ------------------------------------------------------------------------


	/**
	 * Index
	 *
	 */
	function index()
	{
		$this->output('admin/type');
	}


	// ------------------------------------------------------------------------


}


/* End of file type.php */
/* Location: ./application/controllers/admin/type.php */