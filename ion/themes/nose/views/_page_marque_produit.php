<ion:partial view="header" />

<!-- Breadcrump -->
<div class="span-24 last">
	
	<p class="breadcrumb mt8">
		<a href="<ion:base_url />">Home</a> / <ion:breadcrumb separator=" / " />
	</p>

</div>


<div class="container">
	<div class="span-24">
		
		<ion:articles>
			
			<a href="<ion:url />">
			<ion:medias type="picture" limit="1">
				<img src="<ion:src />" />
			</ion:medias>
			</a>
	
		</ion:articles>
	
	</div>
	
	<div class="clear"></div>
	
	<div class="span-24 mt20 white">
	
		<img src="<ion:theme_url />assets/images/_tmp_bottom_newsletter.jpg" />
	
	</div>

</div>





<!-- Partial : Footer -->
<ion:partial view="footer" />