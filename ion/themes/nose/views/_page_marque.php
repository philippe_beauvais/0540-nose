<ion:partial view="header" />

<!-- Breadcrump -->
<div class="span-24 last">
	
	<p class="breadcrumb mt8">
		<a href="<ion:base_url />">Home</a> / <ion:breadcrumb separator=" / " />
	</p>

</div>


<!-- Title -->
<div class="span-24 last mb20 center">

	<p class="mb10" style="margin-top:-18px;"><em>BIENVENUE CHEZ :</em></p>

	<ion:title tag="h2" class="brand title" />
	<ion:subtitle tag="p" class="brand subtitle" />

</div>

<div class="span-24">

	<!-- Articles Slider -->
	<div class="brand-scroller">
		
		<!-- Scroller -->
		<div class="scroller" >
			
			<!-- Nav -->
			<ul class="scroller-nav">
				<ion:articles type="">
					<li>
						<a href="#panel<ion:id_article />">
							<ion:title />
						</a>
					</li>
				</ion:articles>
			</ul>
	
			<!-- Content -->
			<div class="scroller-container">
				<div class="scroller-content">
		
					<ion:articles type="">
		
						<div id="panel<ion:id_article />" class="scroller-panel" style="background:url(<ion:medias type="picture" limit="1"><ion:src folder="660" /></ion:medias>) no-repeat top left;)">
							
							<img src="<ion:medias type="picture" limit="1"><ion:src folder="660" /></ion:medias>" />
							
							<?php if (true == false) :?>
							<div class="scroller-panel-content">
								<div class="bg"></div>
								<div class="text">
									<ion:subtitle tag="h3" />
									<ion:content />					
								</div>
							</div>
							<?php endif ;?>
	
						</div>
					</ion:articles>
				</div>
			</div>
			
		</div><!-- / Scroller -->
	
	</div>
	
	<div class="from-blogs span-7 last">
	
		<ion:articles type="blog">
			
			<ion:medias type="picture" limit="1">
				<img src="<ion:src />" />
			</ion:medias>

		</ion:articles>
	
			<?
			/*
			
			http://www.auparfum.com/spip.php?page=backend
			http://dr-jicky-and-mister-phoebus.blogspot.com/
			http://www.auparfum.com/spip.php?page=actu_sites_parfums
			http://graindemusc.blogspot.com/feeds/posts/default
			*/
			
			?>
		
		</div>
	
	</div>

</div>

<div class="container">
	<div class="span-24">
		
		<ion:articles type="boutique">
			
			<a href="<ion:url />">
			<ion:medias type="picture" limit="1">
				<img src="<ion:src />" />
			</ion:medias>
			</a>
	
		</ion:articles>
	
	</div>
	
	<div class="clear"></div>
	
	<div class="span-24 mt20 white">
	
		<img src="<ion:theme_url />assets/images/_tmp_bottom_newsletter.jpg" />
	
	</div>

</div>





<!-- Partial : Footer -->
<ion:partial view="footer" />