<ion:partial view="header" />

<!-- Title -->
<div class="span-24 last mt20 center">

	<ion:title tag="h2" class="title" />

</div>

<!-- Sub menu -->
<div class="span-24 last mt10 center">

	<ion:medias type="picture" limit="1">
		<a href="<ion:link />">
			<img src="<ion:src />" />
		</a>
	</ion:medias>


</div>

<ion:articles type="">
		
	<ion:article/>		
		
</ion:articles>

<div class="clear"></div>

<div class="span-24 mt20 white">

	<img src="<ion:theme_url />assets/images/_tmp_bottom_newsletter.jpg" />

</div>

<!-- Partial : Footer -->
<ion:partial view="footer" />