<ion:partial view="header" />

<!-- Breadcrump -->
<div class="span-24 last">
	
	<p class="breadcrumb mt8">
		<a href="<ion:base_url />">Home</a> / <ion:breadcrumb separator=" / " />
	</p>

</div>


<!-- Title -->
<div class="span-24 last mb20 center">

	<p class="mb10" style="margin-top:-18px;"><em>BIENVENUE CHEZ :</em></p>

	<ion:title tag="h2" class="brand title" />
	<ion:subtitle tag="p" class="brand subtitle" />

</div>

<div class="span-24">

	<!-- Articles Slider -->
	<div class="brand-scroller">
		
		<!-- Scroller -->
		<div class="scroller" >
			
			<!-- Nav -->
			<ul class="scroller-nav">
				<ion:articles type="">
					<li>
						<a href="#panel<ion:id_article />">
							<ion:title />
						</a>
					</li>
				</ion:articles>
			</ul>
	
			<!-- Content -->
			<div class="scroller-container">
				<div class="scroller-content">
		
					<ion:articles type="">
		
						<div id="panel<ion:id_article />" class="scroller-panel" style="background:url(<ion:medias type="picture" limit="1"><ion:src folder="660" /></ion:medias>) no-repeat top left;)">
							
							<?php if ('<ion:subtitle />' != '') :?>
							<div class="scroller-panel-content">
								<div class="bg"></div>
								<div class="text">
									<ion:subtitle tag="h3" />
									<ion:content />					
								</div>
							</div>
							<?php endif ;?>
	
						</div>
					</ion:articles>
				</div>
			</div>
			
		</div><!-- / Scroller -->
	
	</div>
	
	<div class="from-blogs span-7 last">
	
		<ion:articles type="blog">
			
			<ion:medias type="picture" limit="1">
				<img src="<ion:src />" />
			</ion:medias>

		</ion:articles>
	
			<?
			/*
			
			http://www.auparfum.com/spip.php?page=backend
			http://dr-jicky-and-mister-phoebus.blogspot.com/
			http://www.auparfum.com/spip.php?page=actu_sites_parfums
			http://graindemusc.blogspot.com/feeds/posts/default
			*/
			
			?>
		
		</div>
	
	</div>

</div>


<div class="span-24 white">
	
	<ion:articles type="boutique">
			
		<ion:medias type="picture" limit="1">
			<img src="<ion:src />" />
		</ion:medias>

	</ion:articles>

	
	<h3 class="title mb5">Boutique <ion:title /></h3>	
	
	<div class="clear"></div>
	
	<ul class="subnav3 mb15">
	
		<li><a class="selected">Les nouveautés</a></li>
		<li><a>Les best-sellers</a></li>
		<li><a>parfums</a></li>
		<li><a>cosmétiques</a></li>
		<li><a>maison</a></li>
	
	</ul>
		
	<div class="clear"></div>

	<ion:nose:perfumes rand="true" limit="6">
	
		<div class="span-33 bloc-brand-product last <ion:if field="index" condition="%3==0"> last</ion:if>" style="background-color:5df;cursor:pointer;">
			
			<span class="icon24 sex<ion:perfume attr="sex" />"></span>


			<div class="picture">
				<?php if('<ion:medias type="picture" limit="1"><ion:path /></ion:medias>' == '') :?>
					<img src="http://prod.enoseme.com/files/diptyque/diptyque.oyedo.edt.jpg" style="height:160px;"/>
				<?php endif ;?>
				
				<ion:medias type="picture" limit="1">
					<img src="http://prod.enoseme.com/<ion:path />" style="height:160px;"/>
				</ion:medias>
			</div>
			
			<div class="desc">
				<h4>"<ion:perfume attr="perfume_name" />"</h4>
				<h5><ion:perfume attr="brand_name" /></h5>
				
				<?php
					echo (word_limiter("<ion:perfume attr='description'  />", 15));
				?>
				
				<p><a class="button">Voir le produit >></a></p>
				
			</div>				
			
		</div>
	
	</ion:nose:perfumes>
	
	<div class="clear"></div>

</div>


<div class="clear"></div>

<div class="span-24 mt20 white">

	<img src="<ion:theme_url />assets/images/_tmp_bottom_newsletter.jpg" />

</div>


<script type="text/javascript">

	$('.bloc-brand-product').css('cursor', 'pointer').hover(function()
	{
		$(this).addClass('hover')
		$('a', this).addClass('hover');
	},function()
	{
		$(this).removeClass('hover');
		$('a', this).removeClass('hover');
	});


</script>



<!-- Partial : Footer -->
<ion:partial view="footer" />