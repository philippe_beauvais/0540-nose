<ion:partial view="header" />

<!-- Home Picture Slider -->
<div class="clear"></div>
<h3 class="title">Diagnose</h3>	

<div class="span-24">

	<div class="span-165 last">
		
		<div id="home-slider" >
			<ion:articles from="slider-home">
				<ion:medias type="picture" limit="1" >
					<img src="<ion:src folder="660" />" alt="" />
				</ion:medias>
			</ion:articles>
		</div>
	
	</div>
	<!--
	<div class="span-17 last">
		
		<div id="home-slider" >
			<div id="slideshow">
				
				<div id="s3slider">
					<ul id="s3sliderContent">
					
						<ion:articles from="slider-home">
	
							<li class="s3sliderImage">
							
								<ion:medias type="picture" limit="1" >
									<img src="<ion:src folder="660" />" alt="" />
								</ion:medias>
								
								<div class="bottom">
									
									<div>
										<h2><ion:title /></h2>
										<ion:content />
									</div>
									
									
								</div>
		
							</li>
	
						</ion:articles>
						<div class="clear s3sliderImage"></div>
					</ul>
				</div>
				
			</div>
		</div>
	
	</div>
	-->
	
	<div class="span-7 last">
	
		<div class="home-blocs span-7 last mb10"><img src="<ion:base_url />/files/home_blocs/bloc_surprise.jpg" /></div>
		<div class="home-blocs span-7 last"><img src="<ion:base_url />/files/home_blocs/bloc_love_machine.jpg" /></div>
	
	</div>

</div>


<div class="clear"></div>
<h3 class="title">Magazine</h3>	

<div class="span-24 mb10">

	<ion:articles type="home-third-bloc">
	
		<div class="span-33<ion:if field="index" condition="%3==0"> last</ion:if>">
			<div class="bloc pad home-third-bloc" style="background:url(<ion:medias type="picture" limit="1"><ion:src /></ion:medias>) no-repeat top left;">
				<h4 class="title"><ion:title /></h4>
			</div>
		</div>
	
	</ion:articles>


</div>

<!-- Top 10 -->

<div class="clear"></div>
<h3 class="title">Boutique</h3>	

<div class="span-24">
	<div class="pad home-top10">
		
		<h4 class="title">Best seller</h4>
		
		<div class="clear"></div>
		
		<ion:nose:perfumes rand="true" limit="8">
		
			<div class="span-3<ion:if field="index" condition="%8==0"> last</ion:if>" style="background-color:5df;cursor:pointer;">
				
				<div style="width:110px;height:110px;background-color:#fff;">
					<ion:medias type="picture" limit="1">
						<img src="http://prod.enoseme.com/<ion:path />" style="height:110px;"/>
					</ion:medias>
				</div>

				
				<h5><ion:perfume attr="brand_name" /></h5>
				<h6><ion:perfume attr="perfume_name" /></h6>
				
			</div>
		
		</ion:nose:perfumes>
		
		<div class="clear"></div>
		
		
	</div>
</div>

<div class="clear"></div>

<div class="span-24 mt20 white">

	<img src="<ion:theme_url />assets/images/_tmp_bottom_newsletter.jpg" />

</div>




<!-- Partial : Footer -->
<ion:partial view="footer" />