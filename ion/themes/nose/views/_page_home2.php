<ion:partial view="header" />

<!-- Home Picture Slider -->
<div class="clear"></div>

<div class="span-24">
	<img src="<ion:theme_url />assets/images/_tmp_home_title.jpg" />
</div>
<div class="clear"></div>

<div class="span-24">

	<!-- Articles Slider -->
	<div class="brand-scroller">
		
		<!-- Scroller -->
		<div class="scroller" >
			
			<!-- Nav -->
			<ul class="scroller-nav">
				<ion:articles type="">
					<li>
						<a href="#panel<ion:id_article />">
							<ion:title />
						</a>
					</li>
				</ion:articles>
			</ul>
	
			<!-- Content -->
			<div class="scroller-container">
				<div class="scroller-content">
		
					<ion:articles type="slider">
		
						<div id="panel<ion:id_article />" class="scroller-panel" style="background:url(<ion:medias type="picture" limit="1"><ion:src folder="660" /></ion:medias>) no-repeat top left;)">
							
							<?php if ('<ion:subtitle />' != '') :?>
							<div class="scroller-panel-content">
								<div class="bg"></div>
								<div class="text">
									<ion:subtitle tag="h3" />
									<ion:content />					
								</div>
							</div>
							<?php endif ;?>
	
						</div>
					</ion:articles>
				</div>
			</div>
			
		</div><!-- / Scroller -->
	
	</div>
	
	<div class="span-7 last">
	
		<div class="home-blocs span-7 last mb10"><img src="<ion:base_url />/files/home_blocs/bloc_surprise.jpg" /></div>
		<div class="home-blocs span-7 last"><img src="<ion:base_url />/files/home_blocs/bloc_love_machine.jpg" /></div>
	
	</div>

</div>

<div class="clear"></div>


<div class="span-24">


	
	
</div>

<div class="span-24">
	<ion:medias type="picture" limit="1" >
		<img src="<ion:src />" alt="" />
	</ion:medias>
</div>





<!-- Partial : Footer -->
<ion:partial view="footer" />