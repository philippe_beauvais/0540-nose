<?php

	$i = 0;
?>

<?php foreach($posts as $post) :?>

	<?
	$i++;
//	trace($post);
	
	$picture = FALSE;

	foreach($post['enclosure']->attributes() as $key => $value)
	{
		if ($key == 'url')
		{
			$picture = $value;
			break;
		}
	}
	?>
	
	<div class="from-blogs-rss<?php if($i == $nb) :?> last<?php endif;?>">
		<div class="from-blogs-rss-post">
			<div class="thumb">
				<?php if($picture) :?>
					<img class="thumb" src="<?= $picture?>" style="width:115px;"/>
				<?php endif ;?>
			</div>
			<div class="rss-content">
				<p class="rss-date"></p>
				<h4><a href="<?= $post['link'] ?>"><?= $post['title'] ?></a></h4>
				<p><?= word_limiter($post['description'],3) ?></p>
			</div>
		</div>
	</div>
	
<?php endforeach ;?>

<script type="text/javascript">

	$('.from-blogs-rss').css('cursor', 'pointer').hover(function()
	{
		$(this).addClass('hover');
	},function()
	{
		$(this).removeClass('hover');
		
	});

</script>