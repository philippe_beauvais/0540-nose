Product.Config.prototype.reloadPrice = Product.Config.prototype.reloadPrice.wrap(
    function(reloadPrice) {
        if (this.config.disablePriceReload) {
            return;
        }

        var price = 0;
        var oldPrice = 0;

        for(var i=this.settings.length-1;i>=0;i--) {
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if(selected.config){
                price    += parseFloat(selected.config.price);
                oldPrice += parseFloat(selected.config.oldPrice);
            }
        }

        optionsPrice.reload();

        var existingProducts = new Object();

        for(var i=this.settings.length-1;i>=0;i--) {
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if(selected.config) {
                for(var iproducts=0;iproducts<selected.config.products.length;iproducts++) {
                    var usedAsKey = selected.config.products[iproducts] + "";
                    if(existingProducts[usedAsKey] == undefined) {
                        existingProducts[usedAsKey] = 1;
                    } else {
                        existingProducts[usedAsKey] = existingProducts[usedAsKey]+1;
                    }
                }
            }
        }

        for (var keyValue in existingProducts) {
            for ( var keyValueInner in existingProducts) {
                if(Number(existingProducts[keyValueInner]) < Number(existingProducts[keyValue])) {
                    delete existingProducts[keyValueInner];
                }
            }
        }

        var sizeOfExistingProducts = 0;
        var currentSimpleProductId = "";

        for(var keyValue in existingProducts) {
            currentSimpleProductId = keyValue;
            sizeOfExistingProducts = sizeOfExistingProducts + 1
        }

        if(sizeOfExistingProducts == 1) {
            if($('product-price-' + this.config.productId)) {
                $('product-price-' + this.config.productId).innerHTML = $("simple_" + currentSimpleProductId).innerHTML;
            }
        }

        return price;

        if($('product-price-' + this.config.productId)) {
            $('product-price-' + this.config.productId).innerHTML = price;
        }
        this.reloadOldPrice();
    }
);