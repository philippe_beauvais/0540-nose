Event.observe(window, 'load', function() {
    $$('.super-attribute-select').each(function(el){
        Event.observe(el, 'change',  applyOutofstock);
    });
});

function applyOutofstock() {
    var simpleProductId = spConfig.selectedProductId();
    $$('.outofstock-container').each(function(e) {
        e.hide();
    });
    var message = $('outofstock-message-' + simpleProductId)
    if (message) {
        message.show();
    }
    var link = $('outofstock-link-' + simpleProductId)
    if (link) {
        link.show();
    }
}