/**
 * Partikule
 * Used for checkout, as the navigation items were put out from the sections.
 * (Because of adding the "Cart" navigation item)
 *
 */
NoseAccordion = Class.create();
NoseAccordion.prototype = {
    initialize: function(elem, clickableEntity, checkAllow) {
        this.container = $(elem);
        this.checkAllow = checkAllow || false;
        this.disallowAccessToNextSections = false;
        this.sections = $$('#' + elem + ' .section');
        this.currentSection = false;
        var headers = $$('#' + elem + ' ' + clickableEntity);
        this.headers = headers;
        headers.each(function(header) {
            Event.observe(header,'click',this.sectionClicked.bindAsEventListener(this));
        }.bind(this));
    },

    sectionClicked: function(event) {
		var section_id = $(Event.element(event)).up('li').readAttribute('data-id');
		for (var i=0; i<this.sections.length; i++) {
			if ((this.sections[i]).readAttribute('data-id') == section_id)
			{
				this.openSection(this.sections[i]);
				break;
			}	
		}
        Event.stop(event);
    },

    openSection: function(section) {
        var section = $(section);

        // Check allow
        if (this.checkAllow && !Element.hasClassName(section, 'allow')){
            return;
        }

        if(section.id != this.currentSection) {
            this.closeExistingSection();
            this.currentSection = section.id;
            $(this.currentSection).addClassName('active');
            this.headers.each(function(header) {
            	header.removeClassName('active');
			});
			for (var i=0; i<this.headers.length; i++) {
				if ((this.headers[i]).readAttribute('data-id') == section.readAttribute('data-id'))
				{
					(this.headers[i]).addClassName('active').addClassName('allow');
					break;
				}
            }
            var contents = Element.select(section, '.a-item');
            contents[0].show();
            //Effect.SlideDown(contents[0], {duration:.2});

            if (this.disallowAccessToNextSections) {
                var pastCurrentSection = false;
                for (var i=0; i<this.sections.length; i++) {
                    if (pastCurrentSection) {
                        Element.removeClassName(this.sections[i], 'allow');
                        Element.removeClassName(this.headers[i], 'allow');
                    }
                    if (this.sections[i].id==section.id) {
                        pastCurrentSection = true;
                    }
                }
            }
        }
    },

    closeSection: function(section) {
        $(section).removeClassName('active');
        var contents = Element.select(section, '.a-item');
        contents[0].hide();
        //Effect.SlideUp(contents[0]);
    },

    openNextSection: function(setAllow){
        for (section in this.sections) {
            var nextIndex = parseInt(section)+1;
            if (this.sections[section].id == this.currentSection && this.sections[nextIndex]){
                if (setAllow) {
                    Element.addClassName(this.sections[nextIndex], 'allow')
                    Element.addClassName(this.headers[nextIndex], 'allow')
                }
                this.openSection(this.sections[nextIndex]);
                return;
            }
        }
    },

    openPrevSection: function(setAllow){
        for (section in this.sections) {
            var prevIndex = parseInt(section)-1;
            if (this.sections[section].id == this.currentSection && this.sections[prevIndex]){
                if (setAllow) {
                    Element.addClassName(this.sections[prevIndex], 'allow')
                    Element.addClassName(this.headers[prevIndex], 'allow')
                }
                this.openSection(this.sections[prevIndex]);
                return;
            }
        }
    },

    closeExistingSection: function() {
        if(this.currentSection) {
            this.closeSection(this.currentSection);
        }
    }
}
