$j.fn.spin = function(opts) {
  this.each(function() {
    var self = $j(this),
        data = self.data();

    if (data.spinner) {
      data.spinner.stop();
      delete data.spinner;
    }
    if (opts !== false) {
      data.spinner = new Spinner($j.extend({color: self.css('color')}, opts)).spin(this);
    }
  });
  return this;
};