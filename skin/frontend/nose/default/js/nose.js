
/*
 * Standard function extension
 *
 */
function nosePopWin(url) {
    var win = window.open(url,'Nose', 'scrollbars=yes,width=770,height=655,resizable=yes,toolbar=0');
    win.focus();
}


/*
 * jQuery
 *
 */

var $j = jQuery.noConflict();

/*
 * Blocks Links
 * Takes the first A in each element which has .link as class
 *
 */
jQuery(document).ready(function()
{
	$j('.link').click(function(event)
	{
		a = $j('a:first', this).attr('href');
		if (a)
		{
			window.location.href = a;
		}
	});
});

/*
 * Product view titles togglers
 *
 */
var initToggler = function()
{
	$j('.toggler').each(function(idx)
	{
		if($j(this).hasClass('open')) $j('.accontent',$j(this).parent()).stop(true,true).slideDown(500);
	});

	$j(document).ready(function()
	{
		// accordion toggle click
		$j('.toggler').click(function() {
			if($j(this).hasClass('open')) {
				$j('.accontent',$j(this).parent()).stop(true,true).slideUp(500);
				$j(this).removeClass('open');
			} else {
				$j('.accontent',$j(this).parent()).stop(true,true).slideDown(500);
				$j(this).addClass('open');
			}
		});
	});
};

/*
 * Sub menu block content togglers
 *
 */
var removeBlockTogglerCookie = function()
{
	$j('.block-title').each(function()
	{
		jQuery.cookie($j(this).parent().attr('id'), null, { path: '/'});
	});
};

var isCurrentUrlInBlockLinks = function(id)
{
	if (id)
	{
		var links = document.getElementById(id).getElementsByTagName('a'),
			url = (window.location.href).replace(/\/$/, '') ,
			is = false;

		$j(links).each(function()
		{
			if ($j(this).attr('href').replace(/\/$/, '') == url) is = true;
		});

		return is;
	} else {
		return false;
	}
};


var displayMenuBlock = function(el)
{
	$j('.block-content.visible').removeClass('visible');
	$j('.block-title.open').removeClass('open');
	$j(el).addClass('open');
	$j('.block-content',$j(el).parent()).addClass('visible');
};

/*
var addMenuBlockToCookie = function(el)
{
	removeBlockTogglerCookie();
	jQuery.cookie($j(el).parent().attr('id'), 'open', { path: '/'});
};
*/

var initBlockToggler = function()
{
	$j(document).ready(function()
	{
		// Initial opened block
		$j('.block-title').each(function()
		{
			// if ($j(this).hasClass('preserve-link') ==  false)
			// {
				// accordion toggle click
			/*
				$j(this).click(function(e) {
					e.preventDefault();
					displayMenuBlock($j(this));
				});
		*/
			// }

			if (isCurrentUrlInBlockLinks($j(this).parent().attr('id')))
			{
				displayMenuBlock($j(this));
			}
		});
	});
};

/*
 * Sets one block open
 *
 */
var setOpenBlock = function(blockId)
{
	if( ! $j(blockId + ' .block-title').hasClass('open')) {
		$j(blockId + ' .block-content').stop(true,true).slideDown(500);
		$j(blockId + ' .block-title').addClass('open');
	}
};




/**
 * Slides up / down one div and empty it.
 *
 */
var slideEmpty = function(element, direction)
{
	if (direction == 'up')
		$j(element).slideUp(500, function(){ $j(element).empty(); });

	if (direction == 'down')
		$j(element).slideDown(500, function(){ $j(element).empty(); });
};

var slide = function(element, direction)
{
	if (direction == 'up')
		$j(element).slideUp(500);

	if (direction == 'down')
		$j(element).slideDown(500);
};

/**
 * Desaturate
 *
 */
var paircount = 0;

var desevent = function(event)
{
	if (event.type == 'mouseenter')
		$j(".cloned.color", this).fadeIn(250);

	if (event.type == 'mouseleave')
		$j(".cloned.color", this).fadeOut(250);
};

var desaturate = function(img)
{
	var parent = img.closest("li");
	var cloned = img.clone().attr('id', '');
	cloned.insertAfter(img).addClass("color").addClass('cloned').hide();
	img.pixastic('desaturate');
	parent.bind("mouseenter mouseleave", desevent).data('processed', true);
};


// Home Scroller
var initHomeScroller = function(noautostart)
{
	var $panels = $j('#home-scroller .scroller-content > div.scroller-panel');
	var $container = $j('#home-scroller .scroller-content');
	var forceStart = ( noautostart == true) ? false : true;
	var horizontal = true;

	// float the panels left if we're going horizontal
	if (horizontal && $panels[0]) {
		// calculate a new width for the container (so it holds all panels)
		$container.css('width', $panels[0].offsetWidth * $panels.length);
	}
	// handle nav selection
	function selectHomeNav() {
		$j(this)
			.parents('ul:first')
			.find('a')
			.removeClass('selected')
			.end()
			.end()
			.addClass('selected');
	}
	$j('#home-scroller .scroller-nav').find('a').click(function(el)
	{
		selectHomeNav();
		$j('#home-scroller .scroller-container').trigger('stop');
	});

	// go find the navigation link that has this target and select the nav
	function triggerHome(data) {
		var el = $j('#home-scroller .scroller-nav').find('a[href$="' + data.attr('id') + '"]').get(0);
		selectHomeNav.call(el);
	}

	var scrollOptions =
	{
		target: $j('#home-scroller .scroller-container'),	// element that has the overflow
		items: $panels,									// relative to the target
		navigation: '#home-scroller .scroller-nav a',
		prev: 'a.left',									// selectors NOT relative to document
		next: 'a.right',
		axis: 'xy',										// both directions
		force: forceStart,
		cycle: true,
		interval: 12000,
		offset: parseInt((horizontal ? $container.css('paddingTop') : $container.css('paddingLeft')) || 0) * -1,
		duration: 1000,
		easing: 'easeInOutQuart',
		onBefore: function(e, elem, $pane, $items, pos) {
			e.preventDefault();
			triggerHome($j(elem));
			if (this.blur) this.blur();
		}
	};

	$j('#home-scroller').serialScroll(scrollOptions);

	if (window.location.hash) {
		triggerHome({ id : window.location.hash.substr(1) });
	} else {
		var el = $j('#home-scroller .scroller-nav').find('a').get(0);
		selectHomeNav.call(el);
	}

	// localScroll to hook any other arbitrary links to trigger the effect
	$j.localScroll(scrollOptions);

	// finally, if the URL has a hash, move the slider in to position,
	// setting the duration to 1 because I don't want it to scroll in the
	scrollOptions.duration = 1;
	$j.localScroll.hash(scrollOptions);
};


// Top Scroller
var initTopScroller = function(noautostart)
{
	var $panels = $j('#scroller .scroller-content > div.scroller-panel');
	var $container = $j('#scroller .scroller-content');
	var forceStart = ( noautostart == true) ? false : true;
	var horizontal = true;

	// float the panels left if we're going horizontal
	if (horizontal && $panels[0]) {
	  // calculate a new width for the container (so it holds all panels)
	  $container.css('width', $panels[0].offsetWidth * $panels.length);
	}

	// handle nav selection
	function selectNav() {
	  $j(this)
	    .parents('ul:first')
	      .find('a')
	        .removeClass('selected')
	      .end()
	    .end()
	    .addClass('selected');
	}
	$j('#scroller .scroller-nav').find('a').click(function(el)
	{
		selectNav();
		$j('#scroller .scroller-container').trigger('stop');
	});

	// go find the navigation link that has this target and select the nav
	function trigger(data) {
		var el = $j('#scroller .scroller-nav').find('a[href$="' + data.attr('id') + '"]').get(0);
		selectNav.call(el);
	}

	var scrollOptions =
	{
		target: $j('#scroller .scroller-container'),	// element that has the overflow
		items: $panels,									// relative to the target
		navigation: '#scroller .scroller-nav a',
		prev: 'a.left',									// selectors NOT relative to document
		next: 'a.right',
		axis: 'xy',										// both directions
		force: forceStart,
		cycle: true,
		interval: 6000,
		offset: parseInt((horizontal ? $container.css('paddingTop') : $container.css('paddingLeft')) || 0) * -1,
		duration: 1000,
		easing: 'easeInOutQuart', // http://gsgd.co.uk/sandbox/jquery/easing/
		onBefore: function(e, elem, $pane, $items, pos) {
            e.preventDefault();
			trigger($j(elem));
		    if (this.blur) this.blur();
		},
		onAfter:function( elem, obj )
		{
/*
			if ($j(elem).index() == ($panels.length -1))
			{
				obj.target.trigger('stop');
			}
*/
		}
	};

	$j('#scroller').serialScroll(scrollOptions);

	if (window.location.hash) {
		trigger({ id : window.location.hash.substr(1) });
	} else {
		//		$j('ul.scroller-nav a:first').click();
		var el = $j('#scroller .scroller-nav').find('a').get(0);
		selectNav.call(el);
	}


	// localScroll to hook any other arbitrary links to trigger the effect
	$j.localScroll(scrollOptions);

	// finally, if the URL has a hash, move the slider in to position,
	// setting the duration to 1 because I don't want it to scroll in the
	scrollOptions.duration = 1;
	$j.localScroll.hash(scrollOptions);
};



var showAjxMessage = function(el, type, msg)
{
	$j(el).removeClass('error').removeClass('success').addClass(type);
	$j('.ajx-message-content', $j(el)).html(msg);
	clearTimeout($j(el).data('timeout'));
	$j(el).slideDown( 500 );
	$j(el).data('timeout', setTimeout( hideAjxMessage, (4 * 1000), $j(el) ));
};

var hideAjxMessage = function(el)
{
	clearTimeout($j(el).data('timeout'));
	$j(el).slideUp( 500 );
};

var enhanceCheckbox = function(selectors)
{
	$j(selectors).checkBox();
};

/*
 *
 *
 */
jQuery(document).ready(function()
{
	// Inputs enhancement
	$j('input').checkBox();
	// https://github.com/dcneiner/In-Field-Labels-jQuery-Plugin/
	$j('label.inline').inFieldLabels({'fadeOpacity':0, 'fadeDuration':250});

	// Left menus
	initBlockToggler();

	// Hides messages if any
	if ($j('.messages'))
		setTimeout( hideAjxMessage, (12000), $j('.messages') );

	// Init Helper blocks
	$j('.helper-bloc').each(function(idx)
	{
		if ($j(this).attr('data-target'))
		{
			$j(this).click(function()
			{
				var t = $j($j(this).attr('data-target'));

				t.toggle();
			});
		}
	});
});


/*
 * Spinner
 *
var opts = {
  lines: 10, // The number of lines to draw
  length: 6, // The length of each line
  width: 4, // The line thickness
  radius: 12, // The radius of the inner circle
  color: '#000', // #rgb or #rrggbb
  speed: 1.3, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false // Whether to render a shadow
};
 */

$j.fn.spin = function() {

	var opts = {
		lines: 10,
		length: 9,
		width: 2,
		radius: 7,
		color: '#000',
		speed: 1.3,
		trail: 60,
		shadow: false
	};

	this.each(function() {
	var self = $j(this),
	    data = self.data();

	if (data.spinner) {
	  data.spinner.stop();
	  delete data.spinner;
	  return this;
	}
	if (opts !== false) {
		data.spinner = new Spinner($j.extend({color: self.css('color')}, opts)).spin(this);
	}
	});
	return this;
};


/**
 * Boutique Products details : Lowest price product infos
 *
 */
var getLowestProductInfos = function(url, selector, pids)
{
	$j.ajax({
		'url': url,
		type: 'POST',
		data: {'pids' : pids},
		success: function(data){
			var items = $j.parseJSON( data );
			$j(items).each(function(idx, item)
			{
				var detail = $j(selector + '[data-id=' + item.parentId +']');
				detail.removeClass('spinner');
				detail.html(item.price);
			});
		}
	});
};

var getLowestMasterIds = function(selector, start, end){

	var items = $j(selector).slice(start, end);
	pids =[];
	pids = $j.makeArray(items.map(function(){
		return $j(this).attr("data-id");
	}));
	return pids;
};


