/**
 * Nose Prototype JS Code
 *
 */

var NoseEvaluation = Class.create({

	initialize: function(url)
	{
		this.url  = url;
	},

	initEvaluationRanking: function(ul)
	{
		var self = this;


		$$( ul + '.open li').each(function(a)
		{
			var parent = a.up('ul');
			// var tr = a.up('tr');

			var data = {
				'pid': a.readAttribute('data-id'),
				'rank': a.readAttribute('data-rank')
//				'dig': a.readAttribute('data-dig')
			};

			// Add active class if class rank is data rank
			if( parent.hasClassName('n' + a.readAttribute('data-rank')))
			{
				a.addClassName('active');
			}

			a.observe('click', function(event)
			{
				if( ! parent.hasClassName('n' + a.readAttribute('data-rank')))
				{
					self.removeRankClasses(parent);

					parent.addClassName('n' + a.readAttribute('data-rank'));

					a.siblings().each(function(el) {
						el.removeClassName('active');
					});
					a.addClassName('active');

					// data
					self.updateEvaluationRanking(data);
				}
			});
		});

	},

	removeRankClasses: function(ul)
	{
		ul.removeClassName('waiting');
		for(var i=1; i<=10; i++)
		{
			ul.removeClassName('n' + i);
		}
	},

	updateEvaluationRanking: function(data)
	{
		new Ajax.Request(this.url,
		{
			'method': 'post',
			'parameters': data,
			'evalScripts' : true,
			'onSuccess': function(transport) {
//				console.log('great');
			},
			'onFailure': function(response) {
//				console.log(response.status);
//				window.location.reload();
			}
		});

	}
});




var NoseProductLoader = Class.create({

	listItem: null,
	update: null,
	updates: null,
	hideHandler: false,
	loadingSpinner: null,

	initialize: function(url, options)
	{
		this.url  = url;
		this.handlers = options.handlers;

		if (options.update)	this.update = options.update;
		if (options.updates) this.updates = options.updates;
		if (options.hideHandler) this.hideHandler = options.hideHandler;
		if (options.loadingSpinner) this.loadingSpinner = options.loadingSpinner;

		this.selectedClass = 'active';

		this.elements = $A();
	},

	init: function(elements)
	{
		var self = this;

		$$(this.handlers).each(function(handler)
		{
			var pid = handler.readAttribute('data-pid');

			self.elements[pid] = handler;
			handler.observe('click', function(event)
			{
				self.load(pid, handler);
			});
			handler.setStyle({cursor:'pointer'});
		});
	},

	load: function(pid, handler)
	{
		var self = this;
		var update = this.getUpdate(pid);

		if ( this.updates != null)
		{
			$$(this.handlers).each(function(h){
				h.show();
			});

			if (this.hideHandler) {	handler.hide();	}

			this.showSpinner(pid);

			$$('.' + this.updates).each(function(element)
			{
/*
				if (element.innerHTML != '')
					slideEmpty(element, 'up');
*/
				element.update();
			});
		}

		new Ajax.Updater(
			update,
			this.url,
			{
				'parameters': {'pid' : pid},
				'method': 'post',
				'evalScripts' : true,
				'onSuccess': function(response)
				{
					self.onLoad(self.elements[pid]).bind(self);
/*
					try{
				        FB.XFBML.parse();
				    }catch(ex){}
*/

				//	self.hideSpinner(pid).bind(self);
				}
			}
		);
	},

	showSpinner: function(pid)
	{
		// Multiple updates : means multiple spinner, use of the pid
		if ( this.updates != null)
		{
			if (this.loadingSpinner != null)
			{
				$(this.loadingSpinner + pid).show();
			}
		}
	},

	hideSpinner: function(pid)
	{
		// Multiple updates : means multiple spinner, use of the pid
		if ( this.updates != null)
		{
			if (this.loadingSpinner != null)
			{
				$(this.loadingSpinner + pid).hide();
			}
		}
	},

	getUpdate: function(pid)
	{
		if ( ! (this.update == null))
			return this.update;

		var update = this.updates + pid;

		return update;
	},

	onLoad: function(element)
	{
		var self = this;
		var pid = element.readAttribute('data-pid');

		self.hideSpinner(pid);

		$$(this.handlers).each(function(el)
		{
			el.removeClassName(self.selectedClass);
		})

		$$(this.handlers + pid).each(function(el)
		{
			el.addClassName(self.selectedClass);
		});


/*
			this.elements.each(function(el){
				el.removeClassName(self.selectedClass);
			});
*/


		/*

		if (this.listItem != null)
		{
			var parent = element.up(this.listItem);
			this.elements.each(function(el){
				(el.up(self.listItem)).removeClassName(self.selectedClass);
			});

			parent.addClassName(this.selectedClass);
		}
		else
		{
			this.elements.each(function(el){
				el.removeClassName(self.selectedClass);
			});

			element.addClassName(self.selectedClass);
		}
		*/
	}

});

var NoseEvalProductLoader = Class.create({

	initialize: function(url, options)
	{
		this.url  = url;
		this.containers = options.containers;	// class selector
		this.handlers = options.handlers;

		this.selectedClass = 'active';

		this.elements = $A();
	},

	init: function(elements)
	{
		var self = this;

		$$(this.handlers).each(function(handler)
		{
			var pid = handler.readAttribute('data-pid')
			self.elements[pid] = handler;
			handler.observe('click', function(event)
			{
				self.load(pid);
			});
			handler.setStyle({cursor:'pointer'});
		});
	},

	load: function(pid)
	{
		var self = this;

		new Ajax.Updater(
			this.update,
			this.url,
			{
				'parameters': {'pid' : pid},
				'method': 'post',
				'evalScripts' : true,
				'onSuccess': function(response)
				{
					self.onLoad(self.elements[pid]).bind(self);
				}
			}
		);
	},

	onLoad: function(element)
	{
		var self = this;

		$$(this.handlers).each(function(el)
		{
			el.removeClassName(self.selectedClass);
		})

		$$(this.handlers + element.readAttribute('data-pid')).each(function(el)
		{
			el.addClassName(self.selectedClass);
		});
	}

});

var NosePanels = Class.create({

	activeClass: 'active',

	displayAtStart: 0,

	initialize: function(options)
	{
		this.nav  = options.nav;
		this.panels  = options.panels;
		this.navElements = $(this.nav).childElements();
		this.panelElements = $(this.panels).childElements();

		if (options.activeClass) this.activeClass = options.activeClass;

		if (options.displayAtStart) this.displayAtStart = options.displayAtStart;

		this.init();
	},

	init: function()
	{
		var self = this;

		this.navElements.each(function(element, idx) {
			element.observe('click', function(event) {
				self.display(idx);
			});
		});

		this.hidePanels().display(this.displayAtStart);
	},

	hidePanels: function()
	{
		this.panelElements.each(function(element) {
			element.hide();
		});
		return this;
	},

	setActiveMenuItem: function(panelId)
	{
		var self = this;

		this.navElements.each(function(element)
		{
			element.removeClassName(self.activeClass);
		});

		this.navElements[panelId].addClassName(self.activeClass);

		return this;
	},

	display: function(panelId)
	{
    	this.hidePanels();
    	if (this.panelElements[panelId])
    	{
    		this.panelElements[panelId].show();

		    // Bug correction in prod only : last panel mask stays at 0px height.
		    // Done in jQuery.
		    var mask = $j(this.panelElements[panelId]).find('div.mask');
		    if (mask)
		    {
			    var ul = $j(mask).find('ul');
			    var maskHeight = ul.outerHeight(true);
			    if (maskHeight)
			    {
			        $j(mask).height(maskHeight);
			    }
		    }
    		this.setActiveMenuItem(panelId);
    	}
	}
});


/**
 * Contact Create Form Validation
 * Surprise / Surprise form Validation
 *
 */
var BasicForm = Class.create();

BasicForm.prototype = {
    initialize: function(form, saveUrl){
        this.form = form;
        this.loadWaiting = false;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.saveUrl = saveUrl;
    },

    _disableEnableAll: function(element, isDisabled) {
        var descendants = element.descendants();
        for (var k in descendants) {
            descendants[k].disabled = isDisabled;
        }
        element.disabled = isDisabled;
    },

    setLoadWaiting: function(step, keepDisabled) {
        if (step) {
            if (this.loadWaiting) {
                this.setLoadWaiting(false);
            }
            var container = $(step+'-buttons-container');
            container.addClassName('disabled');
            container.setStyle({opacity:.5});
            this._disableEnableAll(container, true);
            Element.show(step+'-please-wait');
        } else {
            if (this.loadWaiting) {
                var container = $(this.loadWaiting+'-buttons-container');
                var isDisabled = (keepDisabled ? true : false);
                if (!isDisabled) {
                    container.removeClassName('disabled');
                    container.setStyle({opacity:1});
                }
                this._disableEnableAll(container, isDisabled);
                Element.hide(this.loadWaiting+'-please-wait');
            }
        }
        this.loadWaiting = step;
    },

    save: function()
    {
        var validator = new Validation(this.form);

        if (validator.validate())
        {
            var container = $('contact-buttons-container');
            container.addClassName('disabled');
            container.setStyle({opacity:.5});
            this._disableEnableAll(container, true);

            // this.setLoadWaiting('contact');
			$(this.form).submit();
			/*
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
					// onComplete: this.onComplete,
					// onSuccess: this.onSave,
					// onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
			*/
        }
    }
}








/**
 * Validation Class enhancement : Email confirmation
 * @notice : 	Do not forget to add the following translation in your templates :
 * 				Translator.add('Please make sure your email match.','<?php echo $this->__('Please make sure your email match.')?>');
 *
 *
 */
if (Validation)
{
	Validation.addAllThese(
	[
		['validate-cemail', 'Please make sure your email match.', function(v,r){
			var conf = $('email_confirmation') ? $('email_confirmation') : $$('.validate-cemail')[0];
			var email = false;
			if ($('email_address')) {
				email = $('email_address');
			}
			return (email.value == conf.value);
		}]
	]);
}



/**
 * Select the first combination of one configurable product
 *
 */

/**
 * Returns the version of Internet Explorer or a -1
 * (indicating the use of another browser).
 *
 */
function getInternetExplorerVersion()
{
	var rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer')
	{
		var ua = navigator.userAgent;
		var re = new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');
		if (re.exec(ua) != null)
		rv = parseFloat( RegExp.$1 );
	}
	return rv;
};
// var ver = getInternetExplorerVersion();

function fireEvent(element, event)
{
	ver = getInternetExplorerVersion();
	if (ver == -1 || ver > 8.0) {
		// dispatch for firefox, IE9 + others
		var evt = document.createEvent('HTMLEvents');
		evt.initEvent(event, true, true );
		return !element.dispatchEvent(evt);
	}
	else {
		// dispatch for IE <9
		var evt = document.createEventObject();
		return element.fireEvent('on'+event,evt);
	}
};

function setDefaultConfigOptions()
{
	if (spConfigIndex >= spConfig.settings.length)
	{
		return; // stop
	}

	spConfig.settings[spConfigIndex].selectedIndex = 1;
//	spConfig.settings[spConfigIndex].selectedIndex = 0;
	var obj = spConfig.settings[spConfigIndex];
	++spConfigIndex;

	Event.observe(obj,'change',function(){});
	fireEvent(obj,'change');
	window.setTimeout('setDefaultConfigOptions()', 0); // Add a small delay before moving onto the next option
};

/*
function setQuantityChangeEvent()
{
	$$('.qty_change').each(function(item)
	{
    	item.observe('click', function(e)
    	{
			if (this.hasClassName('sub'))
			{
				if($('qty').getValue() > 1) {
					$('qty').setValue(parseInt($('qty').getValue())-1);
				}
			}
			else {
				$('qty').setValue(parseInt($('qty').getValue())+1);
			}
		});
//		fireEvent(obj,'change');
//		window.setTimeout('setDefaultConfigOptions()', 0); // Add a small delay before moving onto the next option

	});
}
*/

/* 	Event.observe(window, 'load', function() {
		setQuantityChangeEvent();
	});
*/
