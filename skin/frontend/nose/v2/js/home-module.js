// Media queries
function detectDevice(device) {
  device.mobile = window.matchMedia( "(max-width: 550px)" );
  device.mobileH = window.matchMedia( "(max-width: 700px)" );
  device.tablet = window.matchMedia( "(max-width: 1000px)" );
  device.desktop = window.matchMedia( "(min-width: 1001px)" );
  device.largeDesktop = window.matchMedia( "(min-width: 1600px)" );
}

var device = {};
detectDevice(device);

(function($) {

  jQuery.fn.videoItem = function() {
    var that = $(this);

    var player;

    function onStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        that.find('.player .controls .progress').addClass('visible');
        that.find('.player').addClass('playing');
      } else if(event.data == YT.PlayerState.PAUSED) {
        that.find('.player').removeClass('playing');
      }
    }

    function displayTime(time) {
      var minutes = Math.floor(time / 60);
      var seconds = Math.floor(time - minutes * 60);
      seconds = seconds > 9 ? seconds : '0' + seconds;
      that.find('.controls .time').html(minutes + ':' + seconds);
    }

    function displayProgress(time, duration) {
      var progress = (time / duration) * 100;
      that.find('.controls .progress .bar').css({
        width: progress + '%'
      });
    }

    function setProgress(e) {
      var max = that.find('.controls .progress').width()
      var position = e.offsetX;
      var progress = position / max;
      var duration = player.getDuration();
      var seekTime = progress * duration;
      player.seekTo(seekTime, true);
      displayTime(seekTime);
      displayProgress(seekTime, duration);
    }

    function playPause(e) {
      e.stopPropagation();
      if (player.getPlayerState() != 1) {
        player.playVideo();
      } else {
        player.pauseVideo();
      }
    }

    function toggleMute() {
      if (player.isMuted()) {
        unMute();
      } else {
        mute();
      }
    }

    function mute() {
      that.find('.player').addClass('mute');
      player.mute();
    }

    function unMute() {
      that.find('.player').removeClass('mute');
      player.unMute();
    }

    player = new YT.Player('player' + that.data('id'), {
      height: '520',
      width: '920',
      videoId: that.data('youtube'),
      playerVars: { 'autoplay': 0, 'controls': 0, 'rel': 0, 'showinfo': 0, 'iv_load_policy': 3 },
      events: {
        'onReady': function(event) {
          unMute();
          setInterval(function() {
            displayTime(event.target.getCurrentTime());
            displayProgress(event.target.getCurrentTime(), event.target.getDuration());
          }, 1000);
        },
        'onStateChange': onStateChange
      }
    });

    that.data('playerObject', player);

    // Events
    that.find('.player .controls .playpause').click(playPause);
    that.find('.player .overlay').click(playPause);
    that.find('.player .controls .progress').click(setProgress);
    that.find('.player .controls .sound').click(toggleMute);
  };

  jQuery.fn.homeModule = function() {
    var $that = $(this);

    var items = [];

    var activeItemIndex = 0;

    // Create array from DOM elements with data attributes
    $that.find('.zblock-item').each(function(index) {
      if ($(this).find('.video-item').length) {
        items.push({
          id: index,
          type: 'video',
          title: $(this).find('.video-item').data('title'),
          poster: $(this).find('.video-item').data('poster'),
          youtube: $(this).find('.video-item').data('youtube')
        });
      } else if ($(this).find('.pictures-item').length) {
        pictures = $(this).find('.pictures-item .picture').map(function() {
          if (device.mobile.matches) {
            return $(this).data('src-mobile');
          } else {
            return $(this).data('src');
          }
        }).get();
        items.push({
          id: index,
          type: 'pictures',
          title: $(this).find('.pictures-item').data('title'),
          pictures: pictures,
          link: $(this).find('.pictures-item').data('link')
        });
      }
    });

    // Empty DOM node
    $that.empty();

    // fill with HTML
    var $videoGallery = $('<div class="video-gallery"></div>');
    var $videoGrid = $('<div class="video-grid"></div>');
    items.forEach(function(item, index) {
      if (item.type == 'video') {
        // video item
        var playerId = Math.floor(Math.random() * 1000);
        var $videoZone = $('<div class="video-zone gallery-item" data-youtube="' + item.youtube + '" data-id="' + playerId + '"></div>');
        $videoZone.append($('<div class="poster" style="background-image: url(' + item.poster + ');"><h3>' + item.title + '</h3></div>'));
        $videoZone.append($('<div class="player"><div id="player' + playerId + '"></div><div class="overlay"></div><div class="controls"><div class="playpause"></div><div class="time">0:00</div><div class="sound"></div><div class="progress"><div class="bar"></div></div></div></div>'));
        $videoGallery.append($videoZone);
        $videoGrid.append('<div class="thumb" style="background-image: url(' + item.poster + ')"></div>');
      } else if (item.type == 'pictures') {
        // picture item
        var $picturesZone = $('<div class="pictures-zone gallery-item"></div>');
        if (index == 0) {
          $picturesZone.addClass('first');
        }
        item.pictures.forEach(function(picture, index) {
          var $picture = $('<div class="picture" style="background-image: url(' + picture + ');"></div>');
          if (index == 0) {
            $picture.addClass('active');
          }
          $picturesZone.append($picture);
        });
        if (item.link != "") {
          $picturesZone.append('<a class="pictures-link" href="' + item.link + '"></a>');
        }
        if (item.title != "") {
          $picturesZone.append('<h3 class="pictures-title">' + item.title + '</h3>');
        }
        $videoGallery.append($picturesZone);
        $videoGrid.append('<div class="thumb" style="background-image: url(' + item.pictures[0] + ')"></div>');
      }
    });
    var galleryItems = $videoGallery.find('.gallery-item');

    // Append everything
    $that.append($videoGallery);
    $videoGrid.append('<div class="icon"></div>');
    $videoGrid.find('.icon').click(function() {
      $videoGrid.toggleClass('open');
    })
    $that.append($videoGrid);

    // Build Youtube players
    galleryItems.filter('.video-zone').videoItem();

    // methods
    function activateItem(index) {
      index = Math.max(0, Math.min(galleryItems.length - 1, index));

      var activeItem = galleryItems.filter('.active');

      var newItem = galleryItems.eq(index);

      if (activeItem.hasClass('video-zone')) {
        activeItem.find('.player').removeClass('active');
        activeItem.data('playerObject').pauseVideo();
      }

      if (newItem.hasClass('video-zone')) {
        newItem.data('playerObject').playVideo();
        newItem.find('.player').addClass('active');
        newItem.find('.poster').addClass('not-visible')
      }
      galleryItems.removeClass('first');
      activeItem.removeClass('active');
      newItem.addClass('active');

      var offset = index * 960 + 480;
      if (device.mobile.matches) {
        offset = index * $(window).width() + ($(window).width() / 2);
      }
      $videoGallery.css({
        transform: 'translateX(-' + offset + 'px)'
      });

      activeItemIndex = index;
    }
    function next() {
      activateItem(activeItemIndex + 1);
    }
    function previous() {
      activateItem(activeItemIndex - 1);
    }

    // events
    galleryItems.filter('.video-zone').find('.poster').click(function(e) {
      $(this).addClass('not-visible');
      $(this).closest('.video-zone').find('.player').addClass('active');
      $(this).closest('.video-zone').data('playerObject').playVideo();
    });
    galleryItems.not('.active').click(function() {
      if (!$(this).hasClass('active')) {
        var index = $(this).index();
        activateItem(index);
      }
    });
    $videoGrid.find('.thumb').click(function() {
      var index = $(this).index();
      activateItem(index);
    })

    galleryItems.eq(0).addClass('active');

    if (device.mobile.matches) {
      $that.bind('swiperight', previous);
      $that.bind('swipeleft', next);
    }

    return this;
  }

}(jQuery));

// if (device.desktop.matches) {
  if (true) {

    function onYouTubeIframeAPIReady() {
      jQuery('.zblock-home_module_top').homeModule();
      jQuery('.zblock-home_module_bottom').homeModule();
      jQuery('.video.slide').videoItem();
    }

    jQuery(document).ready(function($) {

    // Load youtube API
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  });

  }