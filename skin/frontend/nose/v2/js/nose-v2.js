jQuery(document).ready(function($) {

  var BROWSER_HEIGHT = $(window).height();
  var BROWSER_WIDTH = $(window).width();
  var DOCUMENT_HEIGHT = $(document).height();
  var HEADER_HEIGHT = $('header').outerHeight();
  var TRANSITION_SPEED = 500;

  $(window).resize(function() {
    BROWSER_HEIGHT = $(window).height();
    BROWSER_WIDTH = $(window).width();
    DOCUMENT_HEIGHT = $(document).height();
    HEADER_HEIGHT = $('header').outerHeight();
    fitFullwidthItems();
  });

  // Media queries
  function detectDevice(device) {
    device.mobile = window.matchMedia( "(max-width: 550px)" );
    device.mobileH = window.matchMedia( "(max-width: 700px)" );
    device.tablet = window.matchMedia( "(max-width: 1000px)" );
    device.desktop = window.matchMedia( "(min-width: 1001px)" );
    device.largeDesktop = window.matchMedia( "(min-width: 1600px)" );
  }

  var device = {};
  detectDevice(device);

  // Fit window height objects
  function fitFullwidthItems() {
    $('.fit-window').each(function() {
      if ($(this).hasClass('show-edge')) {
        $(this).height( Math.min( BROWSER_HEIGHT - HEADER_HEIGHT - 40 - 80, (BROWSER_WIDTH * 9/16), 1080 ) );
      } else {
        $(this).height( Math.min( BROWSER_HEIGHT - HEADER_HEIGHT - 40, (BROWSER_WIDTH * 9/16), 1080 ) );
      }
    });
  }
  fitFullwidthItems();

  // Reorder language flags
  if ($('header').hasClass('en')) {
    $('.form-language').find('a.fr').closest('li').prependTo($('.form-language ul'));
  } else if ($('header').hasClass('fr')) {
    $('.form-language').find('a.en').closest('li').prependTo($('.form-language ul'));
  }

  // Open Search container
  $('#searchLink').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('#searchContainer').addClass('visible');
    $('#searchContainer #searchField').focus();
  });

  // Close Search container
  $('#searchContainer .close-search').click(function() {
    $('#searchContainer').removeClass('visible');
  });
  $('#searchContainer').click(function(e) {
    e.stopPropagation();
  });
  $('body').click(function() {
    $('#searchContainer').removeClass('visible');
  });

  // Actual search
  $('#searchContainer #searchField').on('keyup', _.debounce(function(e) {
    _.delay(function() {
      $('#searchResultsList').removeClass('no-result');
      $('#searchResultsList').find('.result').remove();
      $('#searchResultsList').append('<li class="result"><a href="#" title="Search result #1">Search result #1</a></li>');
      $('#searchResultsList').append('<li class="result"><a href="#" title="Search result #2">Search result #2</a></li>');
    }, 2000);
  }, 300));

  // Open Login modal
  $('.top-link-account').click(function(e) {
    if (window.isUserLoggedIn == 0) {
      e.preventDefault();
      $('#loginModal').addClass('visible');
    }
  });

  // Open Newsletter Modal
  $('#openNewsletterModal').click(function(e) {
    e.preventDefault();
    $('#newsletterModal').addClass('visible');
  });

  // Close all modals
  $('.close-modal').click(function() {
    $(this).closest('.modal-wrapper').removeClass('visible');
  });

  // Show badge on cart link
  function updateCartLinkBadge() {
    if ($('.top-link-cart').length) {
      var link = $('.top-link-cart');
      var li = link.closest('li');
      var regExp = /\(([^)]+)\)/;
      var number = regExp.exec(link.html());
      if (number) {
        if (li.find('.number').length) {
          li.find('.number').text(number[1]);
        } else {
          var html = $('<span class="number">' + number[1] + '</span>');
          li.append(html).addClass('with-badge with-number-badge');
        }
      }
    }
  }
  updateCartLinkBadge();
  $('.top-link-cart').bind('DOMSubtreeModified', function() {
    updateCartLinkBadge();
  });

  // Sliders plugin
  $.fn.brandBanner = function() {

    var that = this;
    var slides = that.find('.slide'),
    prevArrow = that.find('.arrow.left'),
    nextArrow = that.find('.arrow.right');

    var currentIndex = 0;
    var maxIndex = slides.length - 1;

    function activateSlide(index) {
      if (index > maxIndex) { index = 0; }
      if (index < 0) { index = maxIndex; }
      var oldSlide = slides.filter('.active').first();
      oldSlide.removeClass('active');
      if (oldSlide.hasClass('video')) {
        oldSlide.data('playerObject').pauseVideo();
      }
      var newSlide = slides.eq(index);
      newSlide.addClass('active');
      if (newSlide.hasClass('video')) {
        newSlide.data('playerObject').playVideo();
      }
      currentIndex = index + 0;
    }

    function nextSlide(e) {
      e.preventDefault();
      activateSlide(currentIndex + 1);
    }

    function prevSlide(e) {
      e.preventDefault();
      activateSlide(currentIndex - 1);
    }

    if (slides.length == 1) {
      prevArrow.remove();
      nextArrow.remove();
    }
    nextArrow.click(nextSlide);
    prevArrow.click(prevSlide);
    if (device.mobile.matches) {
      that.on( "swipeleft", prevSlide );
      that.on( "swiperight", nextSlide );
    }

  };

  // All things related to top menu / nav

  $('#navLink').click(function(e) {
    e.preventDefault();
    $('#nav').toggleClass('visible');
    $('#navOverlay').toggleClass('visible');
  });

  $('#navClose').click(function(e) {
    $('#nav').removeClass('visible');
    $('#navOverlay').removeClass('visible');
  });

  $('#navOverlay').click(function(e) {
    $('#nav').removeClass('visible');
    $('#navOverlay').removeClass('visible');
  });


  $('#nav > ul > li:first-of-type').addClass('over');

  $('#nav > ul > li.parent > a').click(function(e) {
    e.preventDefault();
    var $li = $(this).closest('li');
    $li.siblings().removeClass('over');
    $li.toggleClass('over');
  });

  // Move image with mouse on A to Z page
  if ($('.moveWithMouse').length) {
    $(window).mousemove(function(e) {
      $('.moveWithMouse').css({
        top: e.pageY,
        left: e.pageX + 40
      });
    });
  }

  // All sliders
  if ($('.brand-banner').length) {
    $('.brand-banner').brandBanner();
  }

  // 360°
  $.fn.roundabout = function() {
    // Responsive events
    var respMouseDown = 'mousedown',
    respMouseUp   = 'mouseup',
    respMouseMove = 'mousemove';

    if (Modernizr.touch === true) {
      respMouseDown = 'touchstart';
      respMouseUp   = 'touchend';
      respMouseMove = 'touchmove';
    }

    var that = $(this);
    var PATH = that.data('path');
    var WIDTH = that.width();
    var startOffsetIndex = 1;
    var moveIndex = 1;
    var videoIndex = 1;
    var frontIndex = that.data('front') == null ? 18 : that.data('front');
    var MAX_INDEX = 72;
    var SPEED = 100;
    var loadedImages = 0;
    var isMouseDown = false;
    // load all images in cache
    for (var i = 1; i <= MAX_INDEX; i++) {
      var image = new Image();
      image.src = PATH + 'image' + i + '.jpg';
      image.onload = function() {
        loadedImages++;
        if (loadedImages == MAX_INDEX) {
          that.removeClass('loading');
          that.closest('.loading').removeClass('loading');
          startAnimation();
        }
      }
    }

    function startAnimation() {
      var videoInterval = setInterval(function() {
        videoIndex = videoIndex >= MAX_INDEX ? 1 : videoIndex + 1;
        that.attr('src', PATH + 'image' + videoIndex + '.jpg');
        if (videoIndex == frontIndex) {
          clearInterval(videoInterval);
        }
      }, SPEED);
    }

    function onMouseMove(e) {
      if (isMouseDown) {
        var newMoveIndex = videoIndex + cursorIndex(e) - startOffsetIndex;
        if (newMoveIndex != moveIndex) {
          moveIndex = newMoveIndex + 0;
          if (moveIndex > MAX_INDEX) { moveIndex = moveIndex - MAX_INDEX; }
          if (moveIndex < 1) { moveIndex = moveIndex + MAX_INDEX; }
          that.attr('src', PATH + 'image' + moveIndex + '.jpg');
        }
      }
    }

    function cursorIndex(e) {
      var parentOffset = that.offset();
      if (Modernizr.touch === true) {
        var relX = e.originalEvent.touches[0].pageX - parentOffset.left;
      } else {
        var relX = e.pageX - parentOffset.left;
      }
      return Math.max(Math.floor((relX / WIDTH) * MAX_INDEX), 1);
    }

    that.on('dragstart', function(event) { event.preventDefault(); });

    that.bind(respMouseDown, function(e) {
      isMouseDown = true;
      startOffsetIndex = cursorIndex(e);
      that.addClass('mousedown');
    });

    that.bind(respMouseUp, function(e) {
      isMouseDown = false;
      videoIndex = moveIndex + 0;
      that.removeClass('mousedown');
    });

    that.bind(respMouseMove, onMouseMove.throttle(SPEED));
  };

  // News or products carousel
  $.fn.noseCarousel = function() {
    var that = $(this);

    var $wrapper = that.find('.wrapper');
    var $container = that.find('.carousel-container');
    var wrapperWidth = $wrapper.width();
    var $items = $wrapper.find('.item');
    var itemsWidth = $items.eq(2).outerWidth(true);
    var itemsNumber = $items.length;
    var totalWidth = itemsWidth * itemsNumber;

    var LEFT = 0;

    that.find('.arrow.right').click(function(e) {
      e.preventDefault();
      LEFT = Math.min(totalWidth - wrapperWidth, LEFT + itemsWidth);
      $container.css('transform', 'translateX(-' + LEFT + 'px)');
    });

    that.find('.arrow.left').click(function(e) {
      e.preventDefault();
      LEFT = Math.max(0, LEFT - itemsWidth);
      $container.css('transform', 'translateX(-' + LEFT + 'px)');
    });
  }

  // products photos slider
  $.fn.productPhotoSlider = function() {

    var that = this;
    var photos = that.find('.product-photo');

    // init
    dots = $('<div class="dots"></div>')
    photos.each(function(index) {
      var photo = $(this);
      var dot = $('<div class="dot"></div>');
      var offset = index * 100;
      if (index == 0) { dot.addClass('active'); }
      dot.click(function() {
        dots.find('.dot').removeClass('active').eq(index).addClass('active');
        that.css({
          transform: 'translateX(-' + offset + '%)'
        });
        // deal with 360°
        if (photo.hasClass('roundabout') && photo.hasClass('loading')) {
          photo.roundabout();
        }
      });
      dots.append(dot);
    });
    that.parent().append(dots);

    if (photos.first().hasClass('roundabout') && photos.first().hasClass('loading')) {
      photos.first().roundabout();
    }

  }

  if ($('.product-photos').length) {
    if ($('.product-photos').find('.product-photo').length > 1) {
      $('.product-photos').productPhotoSlider();
    }
  }

  // Cosmetic products panels
  $('.panels .panel h3').click(function() {
    // $(this).closest('.panels').find('.panel').removeClass('active');
    $(this).closest('.panel').toggleClass('active');
  });

  // Move sort by dropdown
  if ($('#sort-by').length) {
    $('#sort-by').detach().appendTo($('.shop-navbar .col-main'));
  }

  // toggle product filters
  if ($('.filters-toggle').length) {
    $('.filters-toggle').click(function() {
      $(this).toggleClass('open');
      $('.filters').toggleClass('open');
    });
  }

  // Product wave
  $.fn.removeWaving = function() {
    var that = $(this);
    timeoutId = setTimeout(function() {
      if (that.data('isHover')) {
        that.removeWaving();
      } else {
        that.removeClass('waving');
      }
    }, 1200);
    that.data('timeoutId', timeoutId);
  }
  if ($('.product-item').length) {
    $('.product-item').hover(function() {
      var that = $(this);
      that.data('isHover', true);
      clearTimeout(that.data('timeoutId'));
      that.addClass('waving');
      that.removeWaving();
    }, function() {
      $(this).data('isHover', false);
    });
  }

  // Scroll end extension:
  $.fn.scrollStartEnd = function(startCallback, endCallback, timeout) {
    $(this).scroll(function(){
      startCallback()
      var $this = $(this);
      if ($this.data('scrollTimeout')) {
        clearTimeout($this.data('scrollTimeout'));
      }
      $this.data('scrollTimeout', setTimeout(endCallback, timeout));
    });
  };

  // Inertia effect
  $.fn.inertiaEffect = function() {
    var that = this;
    var lastScrollTop = 0;
    $(window).scrollStartEnd(function() {
      var st = $(window).scrollTop();
      if (st > lastScrollTop){
        that.addClass('scrolling-down');
      } else {
        that.addClass('scrolling-up');
      }
      lastScrollTop = st;
    }, function(){
      that.removeClass('scrolling-down scrolling-up');
    }, 100);
  };

  if (device.desktop.matches) {
    $('.inertia').inertiaEffect();
  }

  // Responsive images
  $('.responsive-image-bg').each(function() {
    if (device.mobile.matches) {
      var src = $(this).data('src-mobile') != "" ? $(this).data('src-mobile') : $(this).data('src');
    } else {
      var src = $(this).data('src');
    }
    $(this).css({ 'background-image': 'url(' + src + ')' });
  });

  // Elements get position fixed when scrolling to it
  $.fn.fixedOnScroll = function(offset) {
    var that = this;
    var elTop, elLeft, elWidth;
    var elTop = that.offset().top - offset;

    function onScroll() {
      var scrollPosition = $(window).scrollTop();
      var translateOffset = Math.max(scrollPosition + HEADER_HEIGHT + offset - elTop, 0);

      that.css({transform: 'translateY(' + translateOffset + 'px)'});
    }

    $(window).resize(function() {
      elTop = that.offset().top - offset;
    });

    $(window).scroll(onScroll);
  }

  if ($('.fixed-on-scroll').length) {
    $('.fixed-on-scroll').fixedOnScroll(20);
  }

  // Mobile Home
  $.fn.mobileIconSlider = function() {
    var that = this;

    var items = that.find('.zblock-item');
    var currentIndex = 0;
    var maxIndex = items.length - 1;

    // add dots
    var dots = $('<div class="dots"></div>');
    that.find('.zblock-item').each(function(index) {
      var dot = $('<div class="dot"></div>')
      dots.append(dot);
    });
    that.append(dots);

    function activate(index) {
      currentIndex = index;
      items.css({
        transform: 'translateX(-' + index * 100 + '%)'
      });
      dots.find('.dot').removeClass('active').eq(index).addClass('active');
    }

    function next() {
      activate(Math.min(currentIndex + 1, maxIndex));
    }

    function previous() {
      activate(Math.max(currentIndex - 1, 0));
    }

    // init
    activate(0);

    // events
    dots.find('.dot').click(function() {
      activate($(this).index());
    });
    that.on('swipeleft', next);
    that.on('swiperight', previous);
  }
  if (device.tablet.matches) {
    $('.zblock-homepage_icons_top').mobileIconSlider();
    $('.zblock-homepage_icons_middle').mobileIconSlider();
    $('.zblock-home_links').mobileIconSlider();
  }

});

jQuery(window).on('load', function() {
  // Show website
  jQuery('.wrapper').removeClass('loading');
  jQuery('.loading-wrapper').fadeOut(500, function() {
    jQuery(this).remove();
  });
});