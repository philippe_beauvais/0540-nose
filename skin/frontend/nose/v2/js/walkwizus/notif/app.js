(function($) {

    $(document).ready(function() {

        // Tabs popin
        $(document).on('click', '.tab-link', function() {
            $(this).closest('.modal').find('.subnav li.active').removeClass('active');
            $(this).parent('li').addClass('active');
            $(this).closest('.modal').find('.tab').removeClass('active').filter('#'+$(this).data('tab')).addClass('active');
        });

        if (hasMessageNotSeen()) {
            $('#notificationsIcon').parent().addClass('with-badge');
        }

        if ($('.notification-messages').length) {
            $('.notification-messages').detach().appendTo('#notificationsIcon');
        }


        $('#notificationsIcon').click(function(e) {
            e.stopPropagation();
            if (window.isUserLoggedIn == 0) {
                e.preventDefault();
                $('#do_the_diagnosis').addClass('visible');
            } else {
                $(this).toggleClass('open');
            }
      });

        $('body').click(function() {
            $('#notificationsIcon').removeClass('open');
        });

        // Mark all notification as seen
        $('#notificationsIcon').click(function() {
            if ($(this).hasClass('open')) {
                return false;
            }

            var messages = getAllMessages();
            $.ajax(notificationUrl.open, {
                type: 'POST',
                data: {
                    messages: messages
                },
                beforeSend: function() {
                    // Show Loader ?
                },
                success: function(r) {

                }
            });
        });

        // Mark all notifications as read
        $('.notification-mark-all-read').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.notification-messages').find('.notification-message').removeClass('state-not-seen').addClass('state-read');
            var messages = getAllMessages();
            $.ajax(notificationUrl.allRead, {
                type: 'POST',
                data: {
                    messages: messages
                },
                beforeSend: function() {

                },
                success: function(r) {

                }
            });
        });

        // Delete notification
        $('.notification-delete').click(function(e) {
            e.stopPropagation();
            $(this).closest('.notification-message').fadeOut(400, function() {
                $(this).remove();
                if ($('.notification-message').length == 0) {
                    $('.notification-messages ul').addClass('empty');
                }
            });
            $.ajax(notificationUrl.delete, {
                type: 'POST',
                data: {
                    messageId: $(this).data('message-id')
                },
                beforeSend: function() {
                    // Show Loader ?
                },
                success: function(r) {
                    updateHtmlContent(r);
                }
            });
        });

        // Mark notification as read
        $('.notification-mark-as-read').click(function(e) {
            e.stopPropagation();
            $(this).closest('.notification-message').removeClass('state-not-seen').addClass('state-read');
            $.ajax(notificationUrl.markAsRead, {
                type: 'POST',
                data: {
                    messageId: $(this).data('message-id')
                },
                beforeSend: function() {
                    // Show Loader ?
                },
                success: function(r) {
                    updateHtmlContent(r);
                }
            });
        });

        // Turn off notification
        $('#turn-off-notification').click(function(e) {
            e.stopPropagation();
            $(this).closest('.notification-messages').fadeOut(function() {
                $(this).remove();
            });
            $.ajax(notificationUrl.turnOff, {
                type: 'POST',
                beforeSend: function() {

                },
                success: function(r) {

                }
            });
        });
    });

    function changeMessageState(url, state) {

    }

    function getAllMessages() {
        var messages = [];

        $('.notification-message').each(function(i, v) {
            messages.push($(v).data('message-id'));
        });

        return messages;
    }

    function hasMessageNotSeen() {
        return $('.notification-message[data-message-state="not-seen"]').length > 0;
    }

    function updateHtmlContent(content) {

    }
}(jQuery));