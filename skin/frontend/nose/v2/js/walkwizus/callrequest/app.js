(function($) {
    $(document).ready(function() {
        $('a[href^="#callrequest"]').click(function(e) {
            e.preventDefault();
            $('#phoneModal').addClass('visible');
        });

        $(document).on('submit', '.callrequest-ajax-form', function(e) {
            $(this).find('button[type="submit"]').attr('disabled');
            e.preventDefault();
            $.ajax($(this).attr('action'), {
                type: 'POST',
                data: $(this).serialize(),
                success: function(r) {
                    var html = $(r).html();
                    console.log(html);
                    $('#phoneModal').html(html);
                },
                error: function(r) {
                    console.log("Error in callback form: ", r);
                }
            });
        });
    });
}(jQuery));