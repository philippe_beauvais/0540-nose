<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// BRANDS
$collection = Mage::getModel('brand/brand')->getCollection()
    ->addAttributeToSelect('*');

$data = array();
foreach ($collection as $item) {
    $data[] = array(
        'id' => $item->getId(),
        'data_fr' => getModelData($item, 1),
        'data_en' => getModelData($item, 2),
        'news' => getLinkedEntities($item->getSelectedPostsCollection(), 'entity_id'),
        'sliders' => getLinkedEntities($item->getSelectedSlidersCollection(), 'entity_id'),
    );
}
setDatas('data/cms/brands.csv', $data);

// NEWS
$collection = Mage::getModel('news/post')->getCollection()
    ->addAttributeToSelect('*');

$data = array();
foreach ($collection as $item) {
    $data[] = array(
        'id' => $item->getId(),
        'data_fr' => getModelData($item, 1),
        'data_en' => getModelData($item, 2),
        'products' => getLinkedEntities($item->getSelectedProductsCollection(), 'sku'),
        'sliders' => getLinkedEntities($item->getSelectedSlidersCollection(), 'entity_id'),
    );
}
setDatas('data/cms/news.csv', $data);

// NEZ
$collection = Mage::getModel('nez/person')->getCollection()
    ->addAttributeToSelect('*');

$data = array();
foreach ($collection as $item) {
    $data[] = array(
        'id' => $item->getId(),
        'data_fr' => getModelData($item, 1),
        'data_en' => getModelData($item, 2),
        'products' => getLinkedEntities($item->getSelectedProductsCollection(), 'sku'),
        'sliders' => getLinkedEntities($item->getSelectedSlidersCollection(), 'entity_id'),
        'news' => getLinkedEntities($item->getSelectedPostsCollection(), 'entity_id'),
    );
}
setDatas('data/cms/nez.csv', $data);

// CATEGORIES
$collection = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect(array('name', 'brand'))
    ->addAttributeToFilter('parent_id', 670);
$data = array();
foreach ($collection as $item) {
    $data[] = array(
        'name' => $item->getName(),
        'brand' => $item->getBrand(),
    );
}
setDatas('data/cms/categories.csv', $data);

function getModelData($model, $storeId) {
    $data = $model->setStoreId($storeId)->load()->getData();
    unset($data['entity_id']);
    unset($data['entity_type_id']);
    unset($data['attribute_set_id']);
    unset($data['created_at']);
    unset($data['updated_at']);
    unset($data['store_id']);
    return json_encode($data);
}

function getLinkedEntities($collection, $field) {
    $data = array();
    foreach ($collection as $item) {
        $data[$item->getData($field)] = array(
            'position' => $item->getPosition()
        );
    }
    return json_encode($data);
}

function setDatas($file, $datas) {
    $fp = fopen($file, 'w');

    $i = 0;
    foreach ($datas as $data) {
        $i++;

        if ($i == 1) {
            fputcsv($fp, array_keys($data), ';');
        }

        fputcsv($fp, $data, ';');
    }
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle, 0, ';')) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                case 'key':
                    $datas[$data[$structData]] = $data;
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}