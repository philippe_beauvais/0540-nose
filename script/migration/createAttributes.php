<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'hair_length', array(
    'group'           => 'Nose',
    'label'           => 'Hair Length',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'hair_thickness', array(
    'group'           => 'Nose',
    'label'           => 'Hair Thickness',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'texture', array(
    'group'           => 'Nose',
    'label'           => 'Texture',
    'input'           => 'select',
    'type'            => 'int',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'frequency', array(
    'group'           => 'Nose',
    'label'           => 'Frequency',
    'input'           => 'select',
    'type'            => 'int',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'routine', array(
    'group'           => 'Nose',
    'label'           => 'Routine',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'preoccupations_body', array(
    'group'           => 'Nose',
    'label'           => 'Body Preoccupation',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'preoccupations_face', array(
    'group'           => 'Nose',
    'label'           => 'Face Preoccupation',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'preoccupations_hair', array(
    'group'           => 'Nose',
    'label'           => 'Hair Preoccupation',
    'input'           => 'multiselect',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'key_ingredients', array(
    'group'           => 'Nose',
    'label'           => 'Key Ingredients',
    'input'           => 'textarea',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'skin_effects', array(
    'group'           => 'Nose',
    'label'           => 'Skin Effects',
    'input'           => 'textarea',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'actives_principles', array(
    'group'           => 'Nose',
    'label'           => 'Actives Principles',
    'input'           => 'textarea',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 1,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'note'            => '',
));
$installer->endSetup();