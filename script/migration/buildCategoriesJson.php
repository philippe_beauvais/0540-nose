<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once('../../app/Mage.php');
Mage::app();


$categories = getDatas('data/cosmetics/category.csv');

$tree = array(
    'id' => '',
    'name_en' => 'Shop',
    'name_fr' => 'Boutique',
    'children' => array()
);

foreach ($categories as $category) {
    $names = array_values(array_filter(explode('|', $category['name_en'])));
    $namesFr = array_values(array_filter(explode('|', $category['name_fr'])));

    $currentNode = &$tree;
    foreach ($names as $k => $name) {
        if (!isset($currentNode['children'])) {
            $currentNode['children'] = array();
        }
        if (!isset($currentNode['children'][$name])) {
            $currentNode['children'][$name] = array(
                'id' => '',
                'name_en' => $name,
                'name_fr' => $namesFr[$k],
            );
        }
        $currentNode = &$currentNode['children'][$name];
    }
}

$tree = cleanTree($tree);

print_r($tree);
file_put_contents('data/cosmetics/category.json', json_encode($tree, JSON_UNESCAPED_UNICODE));

function cleanTree($tree) {
    if (isset($tree['children'])) {
        $tree['children'] = array_values($tree['children']);
        foreach ($tree['children'] as &$children) {
            $children = cleanTree($children);
        }
    }
    return $tree;
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle)) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}