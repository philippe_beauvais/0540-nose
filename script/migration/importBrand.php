<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$data = getDatas('data/cms/import_brands.csv');

$i = 0;
foreach ($data as $item) {
    Mage::log($item['brand'], null, 'jeremie.log');

    $brand = Mage::getModel('brand/brand')->loadByAttribute('title', $item['brand']);

    if (!$brand) {
        $brand = Mage::getModel('brand/brand');
        $brand->setAttributeSetId($brand->getDefaultAttributeSetId());
    }

    $brand
        ->setStoreId(0)
        ->setData('title', $item['brand'])
        ->setData('top_content', $item['about content top en'])
        ->setData('bottom_content', $item['about content bottom en'])
        ->save()
        ->setStoreId(1)
        ->setData('title', false)
        ->setData('top_content', $item['about content top fr'])
        ->setData('bottom_content', $item['about content bottom fr'])
        ->save()
    ;

    $i++;
    if ($i >= 5) {
        break;
    }
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle)) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}