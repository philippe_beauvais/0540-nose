<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once('../../app/Mage.php');
Mage::app();

$products = getDatas('data/cosmetics/cosmetics_orig.csv');

$attributes = array();
foreach ($products as $k => $product) {
    if (empty($product['sku'])) {
        unset($products[$k]);
        continue;
    }

    foreach ($product as $name => $value) {
        $products[$k][$name] = trim($products[$k][$name], " \t\n\r\0\x0B|");
    }

    if (!isset($products[$k]['admin'])) {
        $products[$k] = array_merge(array('store' => 'admin'), $products[$k]);
    }

    // RANGE
    $products[$k]['product_range'] = str_replace(array(
        'Vit Mineral'
    ), array(
        'Vita Mineral'
    ), $products[$k]['product_range']);

    // CATEGORY
    /*$products[$k]['category'] = str_replace(array(
        '||',
        ' |',
        '| ',
        'cosmetic',
        'category|',
        'Category|',
        'Categories|',
        'Cosmetics|Shower & Bath|Shower Gel',
        'Cosmetics|Unisex|Travel Kit',
        'Cosmetics|Body Care|Deodorant',
    ), array(
        '|',
        '|',
        '|',
        'Cosmetics',
        '',
        '',
        '',
        'Cosmetics|Body|Shower & Bath|Shower Gel',
        'Cosmetics|Body|Unisex|Travel Kit',
        'Cosmetics|Body|Body Care|Deodorant',
    ), $products[$k]['category']);*/

    // HAIR LENGTH
    $products[$k]['hair_length'] = '';

    // HAIR THICKNESS
    /*$hairThickness = array();
    foreach (explode(PHP_EOL, $products[$k]['hair_thickness']) as $value) {
        if (!empty($value)) {
            $hairThickness[] = array_pop(explode('|', $value));
        }
    }
    sort($hairThickness);
    $products[$k]['hair_thickness'] = implode(',', $hairThickness);
    $products[$k]['hair_thickness'] = str_replace('All', 'Normal,Thick,Thin', $products[$k]['hair_thickness']);*/

    $hairThickness = array();
    foreach (explode(',', $products[$k]['hair_thickness']) as $value) {
        if (!empty($value) && $value != 'All') {
            $hairThickness[] = $value;
        }
    }
    sort($hairThickness);
    $products[$k]['hair_thickness'] = implode(',', $hairThickness);

    // PREOCCUPATIONS
    /*$preoccupations = array();
    foreach (explode(PHP_EOL, $products[$k]['preoccupations']) as $value) {
        $value = trim($value, " \t\n\r\0\x0B|");
        if (!empty($value)) {
            $value = str_replace(array(
                '||',
                '| ',
                ' |',
            ), array(
                '|',
                '|',
                '|',
            ), $value);

            $value = explode('|', $value);
            if (in_array('cosmetic', $value)) {
                $value = array_slice($value, 3);
            }

            $value = implode('|', $value);
            if (strpos($value, 'All') === false) {
                $preoccupations[] = $value;
            }
        }
    }
    unset($products[$k]['preoccupations']);
    $products[$k]['preoccupations_body'] = '';
    $products[$k]['preoccupations_face'] = '';
    $products[$k]['preoccupations_hair'] = '';
    $arrCategories = explode(PHP_EOL, $products[$k]['category']);
    $firstCategory = explode('|', $arrCategories[0]);
    foreach ($preoccupations as &$preoccupation) {
        $preoccupation = str_replace('|', ' - ', $preoccupation);
    }
    $products[$k]['preoccupations_' . strtolower($firstCategory[1])] = implode(',', $preoccupations);*/

    $preoccupations = array();
    foreach (explode(',', $products[$k]['preoccupations_body']) as $value) {
        if (!empty($value) && $value != 'All') {
            $preoccupations[] = $value;
        }
    }
    sort($preoccupations);
    $products[$k]['preoccupations_body'] = implode(',', $preoccupations);

    $preoccupations = array();
    foreach (explode(',', $products[$k]['preoccupations_face']) as $value) {
        if (!empty($value) && $value != 'All') {
            $preoccupations[] = $value;
        }
    }
    sort($preoccupations);
    $products[$k]['preoccupations_face'] = implode(',', $preoccupations);

    $preoccupations = array();
    foreach (explode(',', $products[$k]['preoccupations_hair']) as $value) {
        if (!empty($value) && $value != 'All') {
            $preoccupations[] = $value;
        }
    }
    sort($preoccupations);
    $products[$k]['preoccupations_hair'] = implode(',', $preoccupations);

    // MANUFACTURER
    $products[$k]['manufacturer'] = str_replace(array(
        'Rosebud',
        'Jorgobé Skin Care - Copenhagen',
    ), array(
        'Rosebud perfume co',
        'JorgObé',
    ), $products[$k]['manufacturer']);

    if (!checkManufacturer($products[$k]['manufacturer'])) {
        //var_dump($products[$k]['manufacturer']);
    }

    // SEX
    $products[$k]['sex'] = str_replace(array(
        'Unisex',
        'Men',
        'Women',
    ), array(
        'unisex',
        'male',
        'female',
    ), $products[$k]['sex']);

    // FREQUENCY
    $products[$k]['frequency'] = str_replace(array(
        'quotidien',
        'Quotidien',
        'daily',
    ), array(
        'Daily',
        'Daily',
        'Daily',
    ), $products[$k]['frequency']);

    // ROUTINE
    /*$products[$k]['routine'] = str_replace(array(
        'Morning & Night',
    ), array(
        'Morning,Night',
    ), $products[$k]['routine']);*/


    // ATTRIBUTES
    foreach ($products[$k] as $field => $value) {
        if (in_array($field, array(
                'category',
                'hair_length',
                'hair_thickness',
                'sex',
                'texture',
                'frequency',
                'routine',
                'preoccupations_body',
                'preoccupations_face',
                'preoccupations_hair',
            )) && !empty($value)) {
            if (!isset($attributes[$field])) {
                $attributes[$field] = array();
            }
            $attributes[$field] = array_unique(array_merge($attributes[$field], explode($field == 'category' ? PHP_EOL : ',', $value)));
        }
    }
}

$products = array_values($products);

$data = array();
foreach ($products as $product) {
    $productFr = $product;
    $productFr['store'] = 'fr';
    foreach ($productFr as $field => $value) {
        if (!in_array($field, array('store', 'sku'))) {
            $productFr[$field] = '__MAGMI_IGNORE__';
        }
    }
    foreach ($product as $field => $value) {
        if (strpos($field, '_fr') !== false) {
            $arrField = explode('_', $field);
            array_pop($arrField);
            $fieldName = implode('_', $arrField);
            $productFr[$fieldName] = $value;
        }
    }

    /*$arrSku = explode('-', $product['sku']);
    $skuConf = 'M-' . $arrSku[1];
    $productConf = $product;
    $productConf['sku'] = $skuConf;
    $productConfFr = $productFr;
    $productConfFr['sku'] = $skuConf;*/

    $data[] = $product;
    $data[] = $productFr;
    /*$data[] = $productConf;
    $data[] = $productConfFr;*/
}

setDatas('data/cosmetics/cosmetics.csv', $data);

print_r($attributes);

foreach ($attributes as $field => $values) {
    sort($values);

    $data = array();
    foreach ($values as $value) {
        $data[] = array(
            'name_en' => $value,
            'name_fr' => ''
        );
    }
    setDatas("data/cosmetics/$field.csv", $data);
}

function checkManufacturer($label) {
    $categories = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToFilter('parent_id', 587)
        ->addAttributeToFilter('name', $label);
    return $categories->count() > 0;
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle)) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}

function setDatas($file, $datas) {
    $fp = fopen($file, 'w');

    $i = 0;
    foreach ($datas as $data) {
        $i++;

        if ($i == 1) {
            fputcsv($fp, array_keys($data));
        }

        fputcsv($fp, $data);
    }
}