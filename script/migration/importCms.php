<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// BRANDS
$data = getDatas('data/cms/brands.csv');
foreach ($data as &$item) {
    $model = Mage::getModel('brand/brand');
    if (!empty($item['id2'])) {
        $model->load($item['id2']);
    }
    $model->setAttributeSetId($model->getDefaultAttributeSetId());
    $model
        ->setStoreId(0)->addData(json_decode($item['data_en'], true))->save()
        ->setStoreId(1)->addData(json_decode($item['data_fr'], true))->save()
    ;
    $item['id2'] = $model->getId();
}
setDatas('data/cms/brands.csv', $data);

// NEWS
$data = getDatas('data/cms/news.csv');
foreach ($data as &$item) {
    $model = Mage::getModel('news/post');
    if (!empty($item['id2'])) {
        $model->load($item['id2']);
    }
    $model->setAttributeSetId($model->getDefaultAttributeSetId());
    $model
        ->setStoreId(0)->addData(json_decode($item['data_en'], true))->save()
        ->setStoreId(1)->addData(json_decode($item['data_fr'], true))->save()
    ;
    $item['id2'] = $model->getId();
}
setDatas('data/cms/news.csv', $data);

// NEZ
$data = getDatas('data/cms/nez.csv');
foreach ($data as &$item) {
    $model = Mage::getModel('nez/person');
    if (!empty($item['id2'])) {
        $model->load($item['id2']);
    }
    $model->setAttributeSetId($model->getDefaultAttributeSetId());
    $model
        ->setStoreId(0)->addData(json_decode($item['data_en'], true))->save()
        ->setStoreId(1)->addData(json_decode($item['data_fr'], true))->save()
    ;
    $item['id2'] = $model->getId();
}
setDatas('data/cms/nez.csv', $data);

// LINK
$brands = getDatas('data/cms/brands.csv', 'key', 'id');
$news = getDatas('data/cms/news.csv', 'key', 'id');
$nez = getDatas('data/cms/nez.csv', 'key', 'id');

// BRANDS
foreach ($brands as $item) {
    $model = Mage::getModel('brand/brand')->load($item['id2']);

    // NEWS
    $data = array();
    foreach (json_decode($item['news'], true) as $id => $position) {
        $data[$news[$id]['id2']] = $position;
    }
    $model->setPostsData($data);
    $model->getPostInstance()->saveBrandRelation($model);

    // SLIDERS
    $model->setSlidersData(json_decode($item['sliders'], true));
    $model->getSliderInstance()->saveBrandRelation($model);
}

// NEWS
foreach ($news as $item) {
    $model = Mage::getModel('news/post')->load($item['id2']);

    // PRODUCTS
    $data = array();
    foreach (json_decode($item['products'], true) as $id => $position) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $id);
        if ($product) {
            $data[$product->getId()] = $position;
        }
    }
    $model->setProductsData($data);
    $model->getProductInstance()->savePostRelation($model);

    // SLIDERS
    $model->setSlidersData(json_decode($item['sliders'], true));
    $model->getSliderInstance()->savePostRelation($model);
}

// NEZ
foreach ($nez as $item) {
    $model = Mage::getModel('nez/person')->load($item['id2']);

    // PRODUCTS
    $data = array();
    foreach (json_decode($item['products'], true) as $id => $position) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $id);
        if ($product) {
            $data[$product->getId()] = $position;
        }
    }
    $model->setProductsData($data);
    $model->getProductInstance()->savePersonRelation($model);

    // SLIDERS
    $model->setSlidersData(json_decode($item['sliders'], true));
    $model->getSliderInstance()->savePersonRelation($model);

    // NEWS
    $data = array();
    foreach (json_decode($item['news'], true) as $id => $position) {
        $data[$news[$id]['id2']] = $position;
    }
    $model->setPostsData($data);
    $model->getPostInstance()->savePersonRelation($model);
}

// CATEGORY
$categories = getDatas('data/cms/categories.csv', 'mappage', array('name', 'brand'));
foreach ($categories as $category => $brand) {
    if (!empty($brand)) {
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('parent_id', 670)
            ->addAttributeToFilter('name', $category);
        if ($collection->count()) {
            $model = $collection->getFirstItem();
            $model->setBrand($brands[$brand]['id2']);
            $model->save();
        }
    }
}

function setDatas($file, $datas) {
    $fp = fopen($file, 'w');

    $i = 0;
    foreach ($datas as $data) {
        $i++;

        if ($i == 1) {
            fputcsv($fp, array_keys($data), ';');
        }

        fputcsv($fp, $data, ';');
    }
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle, 0, ';')) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                case 'key':
                    $datas[$data[$structData]] = $data;
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}