<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$categories = json_decode(file_get_contents('data/cosmetics/category.json'));

createCategory($categories);

file_put_contents('data/cosmetics/category.json', json_encode($categories));

function createCategory(&$category, $parentCategory = null) {
    Mage::log($category->name_en, null, 'jeremie.log');

    if (!empty($category->id)) {
        $modelCategory = Mage::getModel('catalog/category')->load($category->id);
    }
    else {
        $collectionCategory = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('name', $category->name_en)
            ->addAttributeToFilter('parent_id', $parentCategory->getId());
        if ($collectionCategory->count() == 1) {
            $modelCategory = $collectionCategory->getFirstItem();
        }
        else {
            $modelCategory = Mage::getModel('catalog/category');
            $modelCategory->setPath($parentCategory->getPath());
            $modelCategory->setName($category->name_en);
            $modelCategory->setIsActive(1);
            $modelCategory->setIsAnchor(1);
            $modelCategory->setIncludeInMenu(0);
            $modelCategory->save();
        }
    }

    if (!empty($category->name_fr)) {
        $modelCategory->setStoreId(1);
        $modelCategory->setName($category->name_fr);
        $modelCategory->setIsActive(false);
        $modelCategory->setIsAnchor(false);
        $modelCategory->setIncludeInMenu(false);
        $modelCategory->save();
    }

    $category->id = $modelCategory->getId();

    if (isset($category->children)) {
        foreach ($category->children as &$child) {
            createCategory($child, $modelCategory);
        }
    }
}