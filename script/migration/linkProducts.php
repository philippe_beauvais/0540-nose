<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$root = json_decode(file_get_contents('data/cosmetics/category.json'));

$products = getDatas('data/cosmetics/cosmetics.csv');

$association = array();
foreach ($products as $product) {
    Mage::log($product['sku'], null, 'jeremie.log');

    $paths = explode(PHP_EOL, $product['category']);

    $modelProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $product['sku']);

    if (!$modelProduct || !$modelProduct->getId()) {
        continue;
    }

    $categoryIds = $modelProduct->getCategoryIds();
    foreach ($paths as $path) {
        $categories = explode('|', $path);

        $categoryId = null;
        $tree = $root;
        foreach ($categories as $category) {
            foreach ($tree->children as $child) {
                if ($category == $child->name_en) {
                    $categoryId = $child->id;
                    $tree = $child;
                    break;
                }
            }
        }

        $categoryIds[] = $categoryId;
    }

    $modelProduct->setCategoryIds($categoryIds);
    $modelProduct->save();
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle)) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}