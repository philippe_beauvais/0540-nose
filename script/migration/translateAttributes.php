<?php

die('die');

echo "<pre>";

error_reporting(E_ALL);
ini_set('display_errors', true);
set_time_limit(0);

require_once('../../app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$attributes = array(
    'frequency',
    'hair_thickness',
    'preoccupations_body',
    'preoccupations_face',
    'preoccupations_hair',
    'routine',
    'texture',
);

foreach ($attributes as $attributeName) {
    $translations = getDatas("data/cosmetics/$attributeName.csv", 'mappage', array('name_en', 'name_fr'));

    $attributeModel = Mage::getModel('eav/entity_attribute');
    $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');

    $attributeId = $attributeModel->getIdByCode('catalog_product', $attributeName);
    $attribute = $attributeModel->load($attributeId);

    $attributeTable = $attributeOptionsModel->setAttribute($attribute);
    $options = $attributeOptionsModel->getAllOptions(false);

    $data = array();
    $values = array();
    foreach ($options as $option) {
        $values[$option['value']] = array(
            0 => $option['label'],
            1 => $translations[$option['label']]
        );
    }

    $data['option']['value'] = $values;
    $attributeModel->addData($data);
    $attributeModel->save();
}

function getDatas($file, $structName = null, $structData = null) {
    $datas = array();
    if (($handle = fopen($file, 'r')) !== false) {
        $i = 0;
        while (($line = fgetcsv($handle)) !== false) {
            $i++;

            if ($i == 1) {
                $columns = $line;
                if ($structName == 'columns') {
                    return $columns;
                }
                continue;
            }

            $data = array();
            foreach ($line as $k => $v) {
                $data[$columns[$k]] = $v;
            }
            switch ($structName) {
                case 'mappage':
                    if (!empty($data[$structData[0]])) {
                        $datas[$data[$structData[0]]] = $data[$structData[1]];
                    }
                    break;
                case 'onefield':
                    $datas[] = $data[$structData];
                    break;
                default:
                    $datas[] = $data;
                    break;
            }
        }
        fclose($handle);
    }
    return $datas;
}