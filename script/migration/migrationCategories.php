<?php

die('die');

echo "<pre>";

require '../../app/Mage.php';
Mage::app();

error_reporting(E_ALL);
ini_set('display_errors', true);

$brandPageId = 11;

//$testCategoryId = 632;

$shopCategoryId = 669;
$oldBrandCategoryId = 587;
$newBrandCategoryId = 670;
/*$shopCategoryId = 677;
$oldBrandCategoryId = 587;
$newBrandCategoryId = 669;*/

$familySubCategories = array(
    'homefragrance' => 'Home',
    'perfume' => 'Perfumes' ,
    'cosmetic' => 'Cosmetics'
);

$context = null;
/*$context = stream_context_create(array(
    'http' => array(
        'header' => 'Authorization: Basic ' . base64_encode("nosemaster:NoseIsTheBest654")
    )
));*/

$pages = Mage::getModel('ionize/page')->getPages();
foreach($pages as $page) {
    if ($page->getId() == $brandPageId) {
        $fromPage = $page;
        break;
    }
}
$images = array();
if (isset($fromPage)) {
    $articles = Mage::getModel('ionize/article')->getArticles($fromPage);
    foreach ($articles as $article) {
        $images[cleanCode($article->getTitle())] = $article->getMedias()->getFirstItem()->getSource();
    }
}

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$shopCategory = Mage::getModel('catalog/category')->load($shopCategoryId);

$oldBrandCategories = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('parent_id', $oldBrandCategoryId);

if (isset($testCategoryId)) {
    $oldBrandCategories->addAttributeToFilter('entity_id', $testCategoryId);
}

$newBrandCategory = Mage::getModel('catalog/category')->load($newBrandCategoryId);

$attributeModel = Mage::getModel('eav/entity_attribute');
$manufacturerAttributeCode = $attributeModel->getIdByCode('catalog_product', 'manufacturer');
$manufacturerAttribute = $attributeModel->load($manufacturerAttributeCode);

$shopSubCategories = array();
$brandOptions = array();

foreach ($oldBrandCategories as $oldBrandCategory) {
    Magelog('CAT => ' . $oldBrandCategory->getName());

    $collectionCategory = Mage::getModel('catalog/category')
        ->getCollection()
        ->addAttributeToFilter('name', $oldBrandCategory->getName())
        ->addAttributeToFilter('parent_id', $newBrandCategory->getId());
    if ($collectionCategory->count() == 1) {
        $brandCategory = $collectionCategory->getFirstItem();
    }
    else {
        $brandCategory = Mage::getModel('catalog/category')
            ->setName($oldBrandCategory->getName())
            ->setIsActive(1)
            ->setUrlKey($oldBrandCategory->getUrlKey())
            ->setIncludeInMenu(1)
            ->setIsAnchor(1)
            ->setPath($newBrandCategory->getPath())
            ->save();
    }

    if (!$brandCategory->getThumbnail()) {
        if (isset($images[cleanCode($brandCategory->getName())])) {
            $url = $images[cleanCode($brandCategory->getName())];
            $filename = array_pop(explode('/', $url));
            $thumbnail = file_get_contents($url, null, $context);
            file_put_contents(Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category' . DS . $filename, $thumbnail);
            $brandCategory->setThumbnail($filename);
            $brandCategory->save();
        }
        else {
            $brandCategory->setIsActive(0);
            $brandCategory->save();
        }
    }

    if (!isset($brandOptions[$brandCategory->getName()])) {
        $brandOptions[$brandCategory->getName()] = getOptionId($brandCategory->getName());
        if (!$brandOptions[$brandCategory->getName()]) {
            $manufacturerAttribute->setData('option', array(
                'value' => array(
                    'option' => array(
                        $brandCategory->getName()
                    )
                )
            ));
            $manufacturerAttribute->save();

            $brandOptions[$brandCategory->getName()] = getOptionId($brandCategory->getName());
        }
    }

    $subCategories = array();

    $productCollection = $oldBrandCategory->getProductCollection()
        ->addAttributeToSelect('*');
    foreach ($productCollection as $product) {
        $save = false;

        // Set de l'attribut manufacturer
        if (!$product->getManufacturer()) {
            $product->setManufacturer($brandOptions[$brandCategory->getName()]);
            $save = true;
        }

        $type = $product->getAttributeText('boutique_product_type');

        // Liaison à la sous catégorie shop
        if (!empty($type) && $type != 'cosmetic') {
            if (!isset($shopSubCategories[$type])) {
                $collectionCategory = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToFilter('name', $familySubCategories[$type])
                    ->addAttributeToFilter('parent_id', $shopCategory->getId());
                if ($collectionCategory->count() == 1) {
                    $subCategory = $collectionCategory->getFirstItem();
                } else {
                    $subCategory = Mage::getModel('catalog/category')
                        ->setName($familySubCategories[$type])
                        ->setIsActive(1)
                        ->setPath($shopCategory->getPath())
                        ->save();
                }
                $shopSubCategories[$type] = $subCategory->getId();
            }

            $categoryIds = $product->getCategoryIds();
            if (!in_array($shopSubCategories[$type], $categoryIds)) {
                $categoryIds[] = $shopSubCategories[$type];
                $product->setCategoryIds($categoryIds);
                $save = true;
            }
        }

        // Liaison à la sous catégorie brand
        if (!empty($type)) {
            if (!isset($subCategories[$type])) {
                $collectionCategory = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToFilter('name', $familySubCategories[$type])
                    ->addAttributeToFilter('parent_id', $brandCategory->getId());
                if ($collectionCategory->count() == 1) {
                    $subCategory = $collectionCategory->getFirstItem();
                } else {
                    $subCategory = Mage::getModel('catalog/category')
                        ->setName($familySubCategories[$type])
                        ->setIsActive(1)
                        ->setPath($brandCategory->getPath())
                        ->setIncludeInMenu(0)
                        ->save();
                }
                $subCategories[$type] = $subCategory->getId();
            }

            $categoryIds = $product->getCategoryIds();
            if (!in_array($subCategories[$type], $categoryIds)) {
                $categoryIds[] = $subCategories[$type];
                $product->setCategoryIds($categoryIds);
                $save = true;
            }
        }

        if ($save) {
            Magelog('PRD => ' . $product->getName());
            $product->save();
        }
    }
}

function getOptionId($label) {
    $attributeModel = Mage::getModel('eav/entity_attribute');
    $manufacturerAttributeCode = $attributeModel->getIdByCode('catalog_product', 'manufacturer');
    $manufacturerAttribute = $attributeModel->load($manufacturerAttributeCode);

    $attributeOptionsModel = Mage::getModel('eav/entity_attribute_source_table');
    $attributeOptionsModel->setAttribute($manufacturerAttribute);
    $manufacturerOptions = $attributeOptionsModel->getAllOptions(false);

    foreach($manufacturerOptions as $option)
    {
        if ($option['label'] == $label) {
            return $option['value'];
        }
    }
    return false;
}

function cleanCode($value, $strtolower = true) {
    $code = trim($value);

    if ($strtolower) {
        $code = strtolower($code);
    }

    $code = preg_replace('/[^A-Za-z0-9]+/', '_', str_replace(
        array('(', ')', ',', '"', '&',  'à', 'è', 'é', 'ê', 'ï', 'ô', 'û'),
        array('',  '',  '',  '',  'et', 'a', 'e', 'e', 'e', 'i', 'o', 'u'),
        $code
    ));

    return $code;
}

function Magelog($message) {
    Mage::log($message, null, 'jeremie.log');
}